<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="usr" uri="/WEB-INF/tld/login-user.tld"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>人工客户服务</title>
<!-- 加载extjs4.2.1 -->
<script type="text/javascript" src="<c:url value="/ext4/ext-all.js"/>"></script>
<link rel="stylesheet" type="text/css" href="<c:url value="/ext4/ext-all.css"/>" />
<!-- 加载extjs4.2.1结束 -->
<link rel="stylesheet" type="text/css" href="css/common.css" />
<script type="text/javascript">
var csName='<usr:name />';
</script>
<script type="text/javascript" src="js/newMsgShow.js"></script>
</head>
<body>
<script type="text/javascript" src="js/transport.js"></script>
<script type="text/javascript" src="js/csApp.js"></script>
</body>
</html>