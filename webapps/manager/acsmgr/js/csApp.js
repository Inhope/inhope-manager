var state = 1;
Ext.onReady(function() {
			Ext.create(
							'Ext.container.Viewport',
							{
								layout : 'border',
								items : [
										{
											region : 'north',
											border : false,
											items : [ {
												border : false,
												xtype : 'toolbar',
												height : 35,
												items : [stateRadioGroup,
														{
															margins : '0 0 0 100',
															id : 'isConnSuccess',
															border : false,
															xtype : 'tbtext',
															text : '坐席未连接',
															style : 'font-size:14px;font-weight:bold;color:red'
														}
												]
											} ]
										}, {
											region : 'west',
											collapsible : true,
											title : '等待处理的访客',
											width : 300,
											height : '100%',
											layout : 'fit',
											items : gridPanel
										}, {
											region : 'center',
											xtype : 'tabpanel', // TabPanel
											id : 'csServerWindow',
											maxTabWidth : 120
										} ]
							});
			csUserInfo = [ csName ];
			Connection.getConnection(onTransportReady);
		});
		
var stateRadioGroup=Ext.form.RadioGroup({
			margins : '0 0 0 6',
			labelWidth : 60,
			xtype : 'radiogroup',
			columns : 2,
			labelSeparator : ' ',
			fieldLabel : '客服状态',
			labelStyle : 'font-size:13px;font-weight:bold;color:#0000AA;',
			items : [
					{
						boxLabel : '在线',
						name : 'state',
						inputValue : '1',
						checked : true
					},
					{
						boxLabel : '离开',
						name : 'state',
						inputValue : '2'
					} ],
			listeners : {
				change : function(
						self,
						newValue,
						oldValue,
						eOpts) {
					if ("2" == oldValue.state
							&& "1" == newValue.state) {
						state = 1;
					} else if ("2" == newValue.state) {
						state = 2;
					}
				}
			}
});

var store = Ext.create('Ext.data.Store', {
	fields : [ 'id', 'name', 'platform' ],
	proxy : {
		type : 'memory',
		reader : {
			type : 'json',
			root : 'items'
		}
	}
});

var seleMode = new Ext.selection.Model({
	singleSelect : true
});
var gridPanel = Ext
		.create(
				'Ext.grid.Panel',
				{
					height : '100%',
					store : store,
					sm : seleMode,
					columnLines : true,
					sortableColumns : false,
					enableColumnHide : false,
					columns : [
							{
								text : 'id',
								dataIndex : 'id',
								hidden : true
							},
							{
								text : '昵称',
								dataIndex : 'name'
							},
							{
								text : '用户平台',
								dataIndex : 'platform'
							},
							{
								text : '操作',
								renderer : function(v, m, r) {
									return "<a style='cursor:pointer;text-decoration:none' href='javascript:void(0);' onclick='javscript:accept(\""
											+ r.data.id
											+ "\");'>接受</a>&nbsp;&nbsp;&nbsp;&nbsp;"
											+ "<a style='cursor:pointer;text-decoration:none' href='javascript:void(0);' onclick='javscript:decline(\""
											+ r.data.id + "\");'>拒绝</a>";
								}
							} ],
					forceFit : true
				});

function decline(sessionId) {
	var model = gridPanel.getSelectionModel().getSelection();
	gridPanel.getStore().remove(model);
	var responseBody = {};
	responseBody["state"] = CsServer.appEvent.DECLINE;
	responseBody["sessionId"] = sessionId;
	connection.sendMessage(responseBody);
}
function toolBar(id) {
	return Ext.create('Ext.toolbar.Toolbar', {
		width : '100%',
		height : 30
	});
}

function inputTextarea(sessionId, userId) {
	if (!Ext.getCmp(userId + "textArea")) {
		return Ext.create('Ext.form.field.TextArea', {
			id : userId + "textArea",
			width : '90%',
			height : 80,
			name : 'response',
			emptyText : '在这里输入你要回复的内容',
			enterIsSpecial : true,
			listeners : {
				specialkey : function(field, event) {
					if (event.getKey() == event.ENTER) {
						submitContent(sessionId, userId);
						event.stopEvent();
					}
				}
			}
		});
	}
}
function submitButton(sessionId, userId) {
	return Ext.create('Ext.Button', {
		text : '发送',
		width : 70,
		height : 70,
		handler : function() {
			submitContent(sessionId, userId);
		}
	});
}

function submitContent(sessionId, userId) {
	var responseContent = Ext.getCmp(userId + 'textArea').value;
	Ext.getCmp(userId + 'textArea').setValue(null);
	Ext.getCmp(userId + 'textArea').setRawValue(null);
	if (responseContent) {
		var responseBody = {};
		responseBody["state"] = CsServer.appEvent.DELIVER;
		responseBody["sessionId"] = sessionId;
		responseBody["message"] = responseContent;
		connection.sendMessage(responseBody);
		addGenerateDiv(userId, "坐席", responseContent);
	}
}

function addGenerateDiv(userId, name, content) {
	if (name) {
		var nameDiv = document.createElement("div");
		nameDiv.style.fontSize = '14px';
		nameDiv.style.fontWeight = 'bold';
		nameDiv.innerHTML = name + '：';
		document.getElementById(userId + 'showDialog').appendChild(nameDiv);
	}
	if (content) {
		var contentDiv = document.createElement("div");
		contentDiv.style.wordWrap = 'break-word';
		contentDiv.style.overflow = 'hidden';
		contentDiv.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + content;
		document.getElementById(userId + 'showDialog').appendChild(contentDiv);
	}
	var t = document.getElementById(userId + 'showDialog');
	t.scrollTop = t.scrollHeight;
}

function accept(sessionId) {
	var baseTabPanel = Ext.getCmp('csServerWindow');
	if (baseTabPanel.items.length >= 8) {
		Ext.Msg.alert('提示', '您最多只能接入8个客户！');
		return;
	}
	var responseBody = {};
	responseBody["state"] = CsServer.appEvent.ACCEPT;
	responseBody["sessionId"] = sessionId;
	connection.sendMessage(responseBody);
}

function showTab(sessionId, userId, title, platform, userQA) {// 添加新的tabPanel
	var baseTabPanel = Ext.getCmp('csServerWindow');
	var tabPanel = baseTabPanel.getComponent(userId);
	if (!tabPanel) {
		var tabPanel = baseTabPanel;
		tabPanel
				.add(
						{
							id : userId,
							title : title,
							bodyStyle : 'padding:0 0 30px 5px;',
							closable : true,
							iconCls : 'icon-cs-tab',
							layout : 'border',
							items : [
									{
										border : false,
										region : 'center',
										layout : 'fit',
										html : '<div id="'
												+ userId
												+ "showDialog"
												+ '" style="width:100%;height:100%;border:1px solid #99bbe8;overFlow-y:scroll;overFlow-x:hidden;font-size:12px;"></div>'
									},
									{
										region : 'south',
										border : false,
										items : [
												{
													border : false,
													region : 'north',
													items : new toolBar()
												},
												{
													width : '100%',
													height : 80,
													layout : 'table',
													xtype : 'form',
													items : [
															{
																layout : 'fit',
																items : inputTextarea(
																		sessionId,
																		userId)
															},
															{
																border : false,
																bodyStyle : 'margin:0 40px 0 0;',
																items : submitButton(
																		sessionId,
																		userId)
															} ]
												} ]
									} ],
							listeners : {
								beforeclose : function(panel, eOpts) {
									if (!panel.isClosing) {
										Ext.MessageBox
												.confirm(
														'提示',
														"你是否要关闭当前会话？",
														function(btn) {
															if (btn == 'yes') {
																panel.isClosing = true;
																responseBody = {};
																responseBody["state"] = CsServer.appEvent.CLOSE;
																responseBody["sessionId"] = sessionId;
																if (connection) {
																	connection
																			.sendMessage(responseBody);
																}
																panel.close();
															}
														});
										return false;
									}
								},
								show : function() {
									var tabPanel = Ext.getCmp(userId);
									tabPanel.tab.removeCls('header-point-tab');
									tabPanel.tab.removeCls('header-leave-tab');
									tabPanel.tab.addCls('header-default-tab');
									tabPanel.tab.el
											.select(
													'.x-tab-tl,.x-tab-tc,.x-tab-tr,.x-tab-ml,.x-tab-mc,.x-tab-mr',
													false).removeCls(
													'header-point-tab');
									tabPanel.tab.el
											.select(
													'.x-tab-tl,.x-tab-tc,.x-tab-tr,.x-tab-ml,.x-tab-mc,.x-tab-mr',
													false).removeCls(
													'header-leave-tab');
								}
							}
						}).show();
		var model = gridPanel.getSelectionModel().getSelection();
		gridPanel.getStore().remove(model);
		if (userQA) {
			var qas = decodeURIComponent(userQA).split("QA。");
			if (qas.length > 0) {
				for ( var i = 0; i < qas.length; i++) {
					var qa = qas[i].split("A:");
					addGenerateDiv(userId, "用户", qa[0]);
					addGenerateDiv(userId, "机器人", qa[1]);
				}
				addGenerateDiv(
						userId,
						null,
						"              ----------------------------------以上是机器人对话消息-----------------------------------------------");
			}
		}
		tabPanel.setActiveTab(userId);
	} else {
		Ext.getCmp(userId + "textArea").setDisabled(false);
		addGenerateDiv(
				userId,
				null,
				"              ----------------------------------以上是转人工历史消息-----------------------------------------------");
		if (userQA) {
			var qas = decodeURIComponent(userQA).split("QA。");
			if (qas.length > 0) {
				for ( var i = 0; i < qas.length; i++) {
					var qa = qas[i].split("A:");
					addGenerateDiv(userId, "用户", qa[0]);
					addGenerateDiv(userId, "机器人", qa[1]);
				}
				addGenerateDiv(
						userId,
						null,
						"              ----------------------------------以上是机器人对话消息-----------------------------------------------");
			}
		}
		tabPanel.tab.removeCls('header-point-tab');
		tabPanel.tab.removeCls('header-leave-tab');
		tabPanel.tab.el.select(
				'.x-tab-tl,.x-tab-tc,.x-tab-tr,.x-tab-ml,.x-tab-mc,.x-tab-mr',
				false).removeCls('header-point-tab');
		tabPanel.tab.el.select(
				'.x-tab-tl,.x-tab-tc,.x-tab-tr,.x-tab-ml,.x-tab-mc,.x-tab-mr',
				false).removeCls('header-leave-tab');
		tabPanel.tab.addCls('header-default-tab');
		tabPanel.show();
	}
}

/**
 * 处理用户发送的消息
 * 
 * @param msg
 */
function processMsg(msg) {
	if (CsServer.appEvent.ACCEPTED == msg.type) {
		var last = msg.userQA ? encodeURIComponent(msg.userQA) : "";
		showTab(msg.sessionId, msg.userId, msg.title, msg.platform, last);
		return;
	} else if (CsServer.appEvent.REPEAT == msg.type) { // 重复
		connection.disconnect();
		Ext.Msg.alert('提示', '一个用户只能登录一个坐席');
		return;
	} else if (CsServer.appEvent.READY == msg.type) { // 等待处理的
		var model = store.getById(msg.sessionId);
		if (!model) {
			store.add({
				id : msg.sessionId,
				name : msg.userName ? msg.userName : msg.userId,
				platform : msg.platform
			});
			titleRemind();
		}
	} else if (CsServer.appEvent.DELIVER == msg.type) { // 通话中
		if (msg.message) {
			addGenerateDiv(msg.userId,
					msg.userName ? msg.userName : msg.userId, msg.message);
			var tabPanel = Ext.getCmp('csServerWindow');
			var selectTap = Ext.getCmp(msg.userId);
			if (tabPanel.getActiveTab() != selectTap) {
				selectTap.tab.removeCls('header-default-tab');
				selectTap.tab.removeCls('header-leave-tab');
				selectTap.tab.addCls('header-point-tab');
				selectTap.tab.el
						.select(
								'.x-tab-tl,.x-tab-tc,.x-tab-tr,.x-tab-ml,.x-tab-mc,.x-tab-mr',
								false).addCls('header-point-tab');
			}
			if (state == 2) {
				var responseBody = {};
				responseBody["state"] = CsServer.appEvent.LEAVE;
				responseBody["sessionId"] = msg.sessionId;
				connection.sendMessage(responseBody);
			}
		}
		titleRemind();
	} else if (CsServer.appEvent.CANCEL == msg.type) { // 主动取消转人工申请
		var model = store.getById(msg.sessionId);
		if (model)
			gridPanel.getStore().remove(model);
	} else if (CsServer.appEvent.BYE == msg.type) { // 主动断开的访客
		addGenerateDiv(msg.userId, "提示消息", "客户已经离开");
		var tabPanel = Ext.getCmp(msg.userId);
		tabPanel.tab.removeCls('header-default-tab');
		tabPanel.tab.removeCls('header-point-tab');
		tabPanel.tab.addCls('header-leave-tab');
		tabPanel.tab.el.select(
				'.x-tab-tl,.x-tab-tc,.x-tab-tr,.x-tab-ml,.x-tab-mc,.x-tab-mr',
				false).addCls('header-leave-tab');
		Ext.getCmp(msg.userId + "textArea").setDisabled(true);
	} else if (CsServer.appEvent.SYNC == msg.type) {
		Ext.Msg.alert('提示', '同步操作，其它坐席对该用户已经操作！');
		return;
	}
}

function connState(isConnected) {
	if (isConnected)
		Ext.getCmp('isConnSuccess').setText('坐席连接成功');
	else {
		Ext.getCmp('isConnSuccess').setText('坐席未连接');
	}
}