function isFrameChild() {
	if (self != getTopWindow()) {
		return true;
	}else{
		return false;
	}
}
function titleRemind() {
	// 当窗口效果为最小化，或者没焦点状态下才闪动
	if (isMinStatus() || !window.focus) {
		newMsgCount();
	} else {
		if (!isFrameChild) {
			document.title = '人工客户服务';
		} else {
			parent.document.title = '人工客户服务';// 窗口没有消息的时候默认的title内容
			window.clearInterval();
		}
	}
}
var flag = false;
function newMsgCount() {
	if (flag) {
		flag = false;
		if (!isFrameChild) {
			document.title = '【新消息提示！】';
		} else {
			getTopWindow().document.title = '【新消息提示！】';
		}
	} else {
		flag = true;
		if (!isFrameChild) {
			document.title = '【　　　】';
		} else {
			getTopWindow().document.title = '【　　　】';
		}

	}
	window.setTimeout('titleRemind(0)', 400);
}
// 判断窗口是否最小化
// 在Opera中还不能显示
var isMin = false;
function isMinStatus() {
	// 除了Internet Explorer浏览器，其他主流浏览器均支持Window outerHeight 和outerWidth 属性
	if (window.outerWidth != undefined && window.outerHeight != undefined) {
		isMin = window.outerWidth <= 160 && window.outerHeight <= 27;
	} else {
		isMin = window.outerWidth <= 160 && window.outerHeight <= 27;
	}
	// 除了Internet Explorer浏览器，其他主流浏览器均支持Window screenY 和screenX 属性
	if (window.screenY != undefined && window.screenX != undefined) {
		isMin = window.screenY < -30000 && window.screenX < -30000;// FF Chrome
	} else {
		isMin = window.screenTop < -30000 && window.screenLeft < -30000;// IE
	}
	return isMin;
}
