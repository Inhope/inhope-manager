var FLASHSERVER_PORT = 8444;
var appBaseURI = '';
function FlashTransport() {
	var test = function() {
		if (FlashTransport.oFlash)
			return true;
		var swfURL = 'js/transport.swf';
		var version = 0;
		var swfHTML;
		if (navigator.plugins && navigator.mimeTypes.length) {
			var x = navigator.plugins["Shockwave Flash"];
			if (x && x.description) {
				version = parseInt(x.description.replace(/([a-zA-Z]|\s)+/, "")
						.replace(/(\s+r|\s+b[0-9]+)/, ".").split(".")[0]);
			}
			swfHTML = '<embed style="position:absolute;left:-1024px;top:-1024px;" id="_transport_flash" height=1 width=1 type="application/x-shockwave-flash" src="'
					+ swfURL + '" allowScriptAccess="always"/>';
		} else {
			try {
				var axo = new ActiveXObject("ShockwaveFlash.ShockwaveFlash.7");
			} catch (e) {
				try {
					var axo = new ActiveXObject(
							"ShockwaveFlash.ShockwaveFlash.6");
					version = 6;
					try {
						axo = new ActiveXObject("ShockwaveFlash.ShockwaveFlash");
					} catch (e) {
					}
				} catch (e) {
				}
			}
			if (axo != null) {
				version = parseInt(axo.GetVariable("$version").split(" ")[1]
						.split(",")[0]);
			}
			swfHTML = '<object id="_transport_flash" allowScriptAccess="always" style="position:absolute;left:-1024px;top:-1024px;" height=1 width=1 classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"><param name="movie" value="'
					+ swfURL
					+ '"/><param name="allowScriptAccess" value="always" /></object>';
		}
		var result = (version >= 8);
		if (result) {
			// if (navigator.plugins && navigator.mimeTypes.length) {
			if (false) {
				var storeFrame = document.createElement("iframe");
				storeFrame.style.width = '0px';
				storeFrame.style.height = '0px';
				storeFrame.scrolling = "no";
				storeFrame.frameBorder = "no";
				storeFrame.style.position = 'absolute';
				storeFrame.style.left = '-1024px';
				document.body.appendChild(storeFrame);
				doc = storeFrame.contentWindow.document;
				doc.open();
				doc.write("<html><body>" + swfHTML + "</body></html>");
				doc.close();
				FlashTransport.oFlash = doc.getElementById('_transport_flash');
			} else {
				document.write(swfHTML);
				FlashTransport.oFlash = document
						.getElementById('_transport_flash');
			}
		} else {
			throw new Error("Flash player N/A.");
		}
		return result;
	};

	if (!test() && name)
		throw new Error("not support flash transport");

	if (!name)
		return;
}

JSON = {
	toJson : function(obj) {
		var m = {
			'\b' : '\\b',
			'\t' : '\\t',
			'\n' : '\\n',
			'\f' : '\\f',
			'\r' : '\\r',
			'"' : '\\"',
			'\\' : '\\\\'
		}, s = {
			array : function(x) {
				var a = [ '[' ], b, f, i, l = x.length, v;
				for (i = 0; i < l; i += 1) {
					v = x[i];
					f = s[typeof v];
					if (f) {
						v = f(v);
						if (typeof v == 'string') {
							if (b) {
								a[a.length] = ',';
							}
							a[a.length] = v;
							b = true;
						}
					}
				}
				a[a.length] = ']';
				return a.join('');
			},
			'boolean' : function(x) {
				return String(x);
			},
			'null' : function(x) {
				return null;
			},
			number : function(x) {
				return isFinite(x) ? String(x) : 'null';
			},
			object : function(x) {
				if (x) {
					if (x instanceof Array) {
						return s.array(x);
					}
					var a = [ '{' ], b, f, i, v;
					for (i in x) {
						v = x[i];
						f = s[typeof v];
						if (f) {
							v = f(v);
							if (typeof v == 'string') {
								if (b) {
									a[a.length] = ',';
								}
								a.push(s.string(i), ':', v);
								b = true;
							}
						}
					}
					a[a.length] = '}';
					return a.join('');
				}
				return null;
			},
			string : function(x) {
				if (/["\\\x00-\x1f]/.test(x)) {
					x = x.replace(/([\x00-\x1f\\"])/g, function(a, b) {
						var c = m[b];
						if (c) {
							return c;
						}
						c = b.charCodeAt();
						return '\\u00' + Math.floor(c / 16).toString(16)
								+ (c % 16).toString(16);
					});
				}
				return '"' + x + '"';
			}
		};
		if (typeof obj == "array") {
			return s.array(obj);
		} else {
			return s.object(obj);
		}
	},
	fromJson : function(s) {
		var r;
		eval("r=" + s);
		return r;
	}
};

CsServer = {
	mode : {
		FLASH : 'flash',
		SERVLET : 'servlet',
		JSONP : 'jsonp'
	},
	msgRegistry : {
		OPEN : 'open',
		SESSIONOPEN : 'sessionopen',
		TXT : "txt",
		NUDGE : "nudge",
		ACTION : "action"
	},
	appEvent : {
		ACCEPT : 'accept',
		DECLINE : 'decline',
		READY : 'ready',
		CLOSE : 'close',
		DELIVER : 'deliver',
		BYE : 'bye',
		CANCEL : 'cancel',
		LEAVE : 'leave',
		REPEAT : 'repeat',
		SYNC : 'syncRuntime',
		ACCEPTED : 'accepted'
	}
};
// >>>Utils>>>
var Utils = {
	fromJson : function(jsonString, instance) {
		jsonString = jsonString.replace(/\r\n/ig, '\\n');
		jsonString = jsonString.replace(/\n/ig, '\\n');
		jsonString = jsonString.replace(/\r/ig, '\\n');
		var obj;
		try {
			obj = eval('(' + jsonString + ')');
		} catch (e) {
			alert('mal-format transport data!\r\n' + jsonString);
		}
		if (!obj)
			return;
		if (!instance)
			return obj;

		if (typeof instance == 'function') {
			instance = new instance();
		}
		for ( var key in obj) {
			instance[key] = obj[key];
		}
	},
	toJson : function(obj) {
		return JSON.toJson(obj);
	}
};
function $d(obj, des) {
	if (window.console && window.console.debug) {
		console.debug("::" + (des ? des : ''), obj);
	} else {
		// alert("::" + (des ? des : '') + "|" + obj);
	}
}
// <<<Utils<<<
// >>> protocol >>>
function Packet(type) {
	this.type = type;
	this.sequence = -1;
	this.callId = -1;
	this.payload = null;
}

Packet.TYPE_AUTH = 0;
Packet.TYPE_MESSAGE = 1;
Packet.TYPE_REQUEST = 2;
Packet.TYPE_RESPONSE = 3;
Packet.TYPE_ERROR = 4;
Packet.TYPE_KEEPALIVE = 5;
function ConnectionEventHandler() {
	this.onMessage = function(body) {
	};

	this.onError = function(body) {
	};

	this.onConnected = function() {
	};

	this.onDisconnected = function() {
	};
	this.onRequest = function(body) {
	};
	this.onResponse = function(body) {
	};
}
function Connection(transport) {
	var _conn = this;
	/**
	 * @type ConnectionEventHandler
	 */
	var handler;
	/**
	 * 
	 * @param {ConnectionEventHandler}
	 *            h
	 */
	this.setEventHandler = function(h) {
		handler = h;
	};

	this.state = null;
	var getTransport = function() {
		return transport;
	};

	this.configConnection = function(host, port) {
		getTransport().configConnection({
			host : host,
			port : port
		});
	};

	this.setCompressThreshold = function(t) {
		getTransport().setCompressThreshold(t);
	};

	this.connect = function() {
		this.disconnByClient = null;
		if (this.state != Connection.STA_CONNECTED) {
			getTransport().connect();
		}
	};
	this.disconnByClient = null;
	this.disconnect = function() {
		this.disconnByClient = 1;
		if (this.state == Connection.STA_CONNECTED) {
			getTransport().disconnect();
		}
		handler.onDisconnected();
	};

	var seq = 1;
	/**
	 * 
	 * @param p{Packet}
	 */
	var sendPacket = function(p) {
		if (p.type != Packet.TYPE_KEEPALIVE) {
			p.sequence = seq++;
		}
		p.payload = Utils.toJson(p.payload);
		// console.debug(p)
		if (_conn.state == Connection.STA_CONNECTED) {
			getTransport().sendPacket(p);
		}
	};
	this.authenticate = function(bodyData, callback) {
		if (callback == null) {
			throw new Error('no callback for "authenticate" method');
		}
		handler.onRequest(bodyData);
		if (!reqQueueTimer) {
			startReqQueueTimer();
		}
		var requestEntry = new RequestEntry();
		requestEntry.callId = 0;
		requestEntry.cb = callback;
		requestQuene.push(requestEntry);
		var p = new Packet(Packet.TYPE_AUTH);
		p.payload = bodyData;
		sendPacket(p);
	};
	/**
	 * send asynchorous message
	 * 
	 * @param bodyData{Object}
	 * 
	 */
	this.sendMessage = function(bodyData) {
		var p = new Packet(Packet.TYPE_MESSAGE);
		p.payload = bodyData;
		sendPacket(p);
	};

	var requestQuene = [];
	function RequestEntry() {
		this.callId = null;
		this.cb = null;
		this.startTime = new Date().getTime();
	}
	var callId = 1;
	var reqQueueTimer;
	function startReqQueueTimer() {
		reqQueueTimer = setInterval(function() {
			var now = new Date().getTime();
			for ( var i = requestQuene.length - 1; i >= 0; i--) {
				var entry = requestQuene[i];
				if (now - entry.startTime > entry.timeout) {
					requestQuene.splice(i, 1);
					if (entry.cb)
						entry.cb(false);
				}
			}
		}, 10000);
	}
	/**
	 * send synchorous message
	 * 
	 * @param bodyData{Object}
	 * @param callback{Function}
	 * 
	 */
	this.callService = function(bodyData, callback) {
		if (callback == null) {
			throw new Error('no callback for "callService" method');
		}
		handler.onRequest(bodyData);
		if (!reqQueueTimer) {
			startReqQueueTimer();
		}
		if (callId == 32767) {
			callId = 0;
		} else {
			callId++;
		}
		var requestEntry = new RequestEntry();
		requestEntry.callId = callId;
		requestEntry.cb = callback;
		requestQuene.push(requestEntry);
		var p = new Packet(Packet.TYPE_REQUEST);
		p.callId = callId;
		p.payload = bodyData;
		sendPacket(p);
	};
	var keepaliveTimer;
	this.startKeepAlive = function() {
		if (keepaliveTimer) {
			clearInterval(keepaliveTimer);
		}
		keepaliveTimer = setInterval(function() {
			try {
				sendPacket(new Packet(Packet.TYPE_KEEPALIVE));
			} catch (e) {
			}
		}, 60000);
	};

	// global call backs >>>
	this.onConnected = function() {
		this.state = Connection.STA_CONNECTED;
		this.startKeepAlive();
		handler.onConnected();
	};

	this.onDisconnected = function() {
		if (this.state != Connection.STA_DISCONNECTED) {
			this.state = Connection.STA_DISCONNECTED;
			handler.onDisconnected();
		}
		connState(false);
	};

	this.onPacketReceived = function(packet) {
		var bodyData;
		if (packet.payload != null) {
			bodyData = Utils.fromJson(packet.payload);
		}
		switch (packet.type) {
		case Packet.TYPE_MESSAGE:
			handler.onMessage(bodyData);
			break;
		case Packet.TYPE_AUTH:
			packet.callId = 0;
		case Packet.TYPE_RESPONSE:
			handler.onResponse(bodyData);
			for ( var i = requestQuene.length - 1; i >= 0; i--) {
				var entry = requestQuene[i];
				if (entry.callId == packet.callId) {
					requestQuene.splice(i, 1);
					entry.cb(true, bodyData);
					// debug mode
					// setTimeout(function(){entry.cb(message);},0);
					break;
				}
			}
			break;
		case Packet.TYPE_ERROR:
			handler.onError(bodyData);
			break;
		}
	};
	var conn = this;
	// CONNECTION EVENTS
	window.flapxtrans_onConnected = function() {
		conn.onConnected();
	};
	window.flapxtrans_onPacketReceived = function(packet) {
		conn.onPacketReceived(packet);
	};
	window.flapxtrans_onClosed = function() {
		conn.onDisconnected();
	};
}
Connection.STA_CONNECTED = 'CONNECTED';
Connection.STA_DISCONNECTED = 'DISCONNECTED';
var NA = new Object();
var TransportUtil = {
	getTransport : function(cb) {
		if (this.trans == null) {
			this.cb = cb;
		} else {
			cb(this.trans == NA ? null : this.trans);
		}
	},
	onReady : function(trans) {
		this.trans = trans;
		if (this.cb) {
			this.cb(this.trans);
		}
	}
};
// write flash transport
try {
	window.flapxtrans_ready = function() {
		TransportUtil.onReady(FlashTransport.oFlash);
	};
	new FlashTransport();
} catch (e) {
	TransportUtil.onReady(NA);
}
/**
 * 
 * @param {Function(connection)}
 *            cb
 */
Connection.getConnection = function(cb) {
	TransportUtil.getTransport(function(transport) {
		if (transport) {
			$d(transport.connect, 'transport.connect');
			if (transport && transport.connect) {
				var connection = new Connection(transport);
				cb(connection);
			}
		} else {
			cb();
		}
	});
};
var csUserInfo;
var onTransportReady = function(conn) {
	var flapxHandler = new ConnectionEventHandler();
	flapxHandler.onConnected = function() {
		if (NA == conn) {
			alert('坐席连接失败，可能是你的flash版本过低，或者flash被关闭！');
			return;
		} else if (conn) {
			/**
			 * flapx's authenticate protocol, can be used to confirm if the
			 * connection is established and then trigger sessionopened.
			 */
			var data = null;
			if (csUserInfo && csUserInfo.length > 0) {
				data = {
					csName : csUserInfo[0],
					state : 'register'
				};
			}
			conn.authenticate(data, function(success, data) {
				if (success)
					connState(success);
				// processOpenResponse(data, conn);
			});
		}
	};
	connection = conn;
	flashConnect(conn, flapxHandler);
};
var connection;
var flashConnect = function(conn, flapxHandler) {
	var flashConn = conn;
	flapxHandler.onDisconnected = function() {

	};
	flapxHandler.onError = function() {
		fireEvent('exceptioncaught', session);
	};
	flapxHandler.onMessage = function(msg) {
		processMsg(msg);
	};
	flashConn.setEventHandler(flapxHandler);
	var _h = window.location.href;
	var host = getHostName(_h);
	var port = FLASHSERVER_PORT;
	flashConn.configConnection(host, port);
	flashConn.connect();
};

function getHostName(str) {
	var re = new RegExp('^(?:f|ht)tp(?:s)?\://([^/^:]+)', 'im');
	return str.match(re)[1].toString();
}
