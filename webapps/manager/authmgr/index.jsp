<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="res" uri="/WEB-INF/tld/import-res.tld"%>
<%@taglib prefix="usr" uri="/WEB-INF/tld/login-user.tld"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>用户/权限管理</title>

<%@include file="/commons/extjsvable.jsp"%>
<link rel="stylesheet" type="text/css" href="custom.css" />
<res:import dir="script" recursive="true" include=".*\.js"
	exclude="chart/.*" />
<usr:checkPerm jsExportVar="auth_check_perm_result"
	permissionNames="auth" dump="true" />
<usr:name jsExportVar="auth_username" />
</head>

<body>
	<script type="text/javascript">
		if (parent.Ext.getCmp('authmgr'))
	  parent.Ext.getCmp('authmgr').getEl().unmask();
	</script>
</body>
</html>
