<%@page import="java.net.URLEncoder"%>
<%@page import="com.eastrobot.commonsapi.ibotcluster.ClusterAppConfig"%>
<%@page import="com.eastrobot.commonsapi.ibotcluster.DoInClusterCallback"%>
<%@page import="com.incesoft.xmas.commons.Simp2TranUtils"%>
<%@page import="com.eastrobot.commonsapi.ibotcluster.ClusterConfigServiceHelper"%>
<%@page import="com.incesoft.xmas.authmgr.web.AuthAction"%>
<%@page import="com.incesoft.xmas.commons.LicenceValidator"%>
<%@page import="org.springframework.web.context.WebApplicationContext"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:directive.page import="org.springframework.web.context.support.WebApplicationContextUtils"/>
<jsp:directive.page import="org.springframework.beans.factory.config.PropertyPlaceholderConfigurer"/>
<jsp:directive.page import="com.incesoft.xmas.commons.PropertyPlaceholderRecorder"/>
<%@taglib prefix="mdl" uri="/WEB-INF/tld/mdl-ft.tld"%>
<%
	LicenceValidator licenceValidator = null;
	WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(application);
	if(ctx != null) licenceValidator = ctx.getBean(LicenceValidator.class);
	if (ClusterConfigServiceHelper.isClusterEnabled() && ClusterConfigServiceHelper.getAppCFG() == null){
		final PropertyPlaceholderRecorder placeHolder = (PropertyPlaceholderRecorder) ctx.getBean(PropertyPlaceholderRecorder.class);
		if ("true".equals(placeHolder.getProperty("sys.labs"))){
			response.sendRedirect("../unimgr/labs.jsp");
			return;	
		}
		response.sendRedirect("../clustermgr/login.jsp");
		return;	
	}
	else if (licenceValidator != null && licenceValidator.isVersionStandard0()){
		response.sendRedirect("./login_standard.jsp");
		return;	
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<title><mdl:dump name="systemName" jsExportVar="#out" unique="true" type="props"/></title>
<base target="_self"/>
<%@include file="/commons/extjsvable.jsp" %>
<link rel="stylesheet" type="text/css" href="css/xiaoi_houtai.css"/>
<link rel="shortcut icon" type="image/x-icon" href="../favicon.ico"/>
<script>
(function(){
	if (getTopWindow() != window){
		getTopWindow().location = window.location
		return;
	}
	var error_code = '<%=URLEncoder.encode(request.getParameter("error_code")!=null?request.getParameter("error_code"):"")%>'
	if (error_code == '1'){
		window.location = 'error.jsp?msg='+encodeURIComponent('请求的资源未授权给当前登陆用户,请联系管理员');
	}
})();
var markInvalid = function(msg, target) {
	target = target||'username'
	Ext.getBody().unmask();
	Ext.each(['username','password','captcha'],function(t){
		if (Ext.get(t +"InvalidIcon")){
			Ext.get(t +"InvalidIcon")[t!=target?'hide':'show']();
			if (t==target) {
				Ext.getDom(t +"InvalidIcon").title= msg;
			}
		}
	})
	Ext.get(target).focus();
	if (Ext.getDom('captchaImg'))
		reloadImg(Ext.getDom('captchaImg'))
}

var authCallback = function(response, options) {
	var host = options.host;
	var result = Ext.decode(response.responseText);
	if (result.success) {
		Ext.getBody().mask('登录成功，正在为您转到目标页面', 'x-mask-loading');
		window.location = result.data;
	} else {
		if (result.message != 'probe')
			markInvalid(result.message, result.data);
		Ext.getBody().unmask();
	}
}
var doAuth = function(probe) {
	
	var username = Ext.getDom('username').value
	var password = Ext.getDom('password').value
	var captcha = Ext.getDom('captcha')?Ext.getDom('captcha').value:''
	
	if (!probe) {
		if(!username) {
			markInvalid('缺少用户名','username');
			return;
		}
		if(!password) {
			markInvalid('缺少密码','password');
			return;
		}
	}
	
	Ext.getBody().mask('登录中...', 'x-mask-loading');
	Ext.Ajax.request({
				url : 'auth!login.action',
				params : {
					username : username,
					password : username == 'INCE.SUPERVISOR'?password:CryptoJS.SHA1(password).toString(),
					captcha : captcha,
					probe : probe
				},
				success : authCallback,
				failure : markInvalid
			});
}
Ext.onReady(function() {
	Ext.get('username').on('keydown',function(e){
		if(e.getKey()==13)
			doAuth();
	});
	Ext.get('password').on('keydown',function(e){
		if(e.getKey()==13)
			doAuth();
	});
	if (Ext.get('captcha'))
		Ext.get('captcha').on('keydown',function(e){
			if(e.getKey()==13)
				doAuth();
		});
	Ext.get('loginButton').on('click',function(e){
		doAuth();
	});
	Ext.get('resetButton').on('click',function(e){
		Ext.getDom('username').value = window.initUsername
		Ext.getDom('password').value = window.initPassword
		Ext.get('username').focus();
	});
	window.initUsername = Ext.getDom('username').value
	window.initPassword = Ext.getDom('password').value
	doAuth(true)
	Ext.get('username').focus();
})
function reloadImg(img){
	if (img) img.src = img.src.replace(/\?.*/,'')+'?'+new Date().getTime()
}
</script>
</head>

<body>
<%
pageContext.setAttribute("utfVersion", Simp2TranUtils.UI_SIMP2TRAN_ENABLED);
%>
<!--=============================Author:lvzhaohua=========================-->
<mdl:dump name="sys.kbase" jsExportVar="#page.kbaseEnabled" unique="true" type="props"/>
<div class="warp"  style="${kbaseEnabled?'background-image: url(images/content_bg_kbase.jpg)':(utfVersion?'background-image: url(images/content_bg_utf.jpg)':'')}">

<div class="top_content"><%=((PropertyPlaceholderRecorder)WebApplicationContextUtils.getWebApplicationContext(application).getBean(PropertyPlaceholderRecorder.class)).getProperty("projectName")%></div>
<div class="middle_content">
<table width="336" border="0" cellspacing="0" cellpadding="0">
<tbody>
  <tr>
    <td width="25%" height="36" align="right" valign="middle"><b>用户名：</b></td>
    <td width="75%" height="36" align="left" valign="middle"><input type="text" name="textfield" id="username" class="input_cn1" /><div id="usernameInvalidIcon"></div></td>
  </tr>
  <tr>
    <td height="36" align="right" valign="middle"><b>密&nbsp;&nbsp;码：</b></td>
    <td height="36" align="left" valign="middle"><input type="password" name="textfield" id="password" class="input_cn1" /><div id="passwordInvalidIcon"></div></td>
  </tr>
  <% if (AuthAction.isCaptchaEnabled()){ %>
	  <tr>
	    <td height="36" align="right" valign="middle"><b>验证码：</b></td>
	    <td height="36" align="left" valign="middle"><input name="textfield" id="captcha" class="input_cn1" style="width:115px;"/><img alt="看不清点击换一张" style="margin:1px 0 0 3px;height:24px;width:54px;cursor:pointer;float:left" id="captchaImg" src='auth!captcha.action' onclick="reloadImg(this)"><div id="captchaInvalidIcon"></div></td>
	  </tr>
  <% } %>
  <tr>
    <td height="40">&nbsp;</td>
    <td height="40" align="left" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="0"><tbody>
      <tr>
        <td width="33%" height="40" align="left" valign="middle"><a id="loginButton" href="#" class="${!utfVersion?'login_bt1':'login_bt1_utf'}"></a></td>
        <td width="67%" height="40" align="left" valign="middle"><a id="resetButton" href="#" class="${!utfVersion?'login_bt2':'login_bt2_utf'}"></a></td>
      </tr>
    </tbody></table></td>
  </tr>
</tbody>
</table>
</div>
<div class="copyright_content">Copyright © 2013 <a href="http://www.xiaoi.com" target="_blank">www.xiaoi.com</a> 版权所有</div>
</div>
<!--=============================Author:lvzhaohua=========================-->
</body>
</html>
