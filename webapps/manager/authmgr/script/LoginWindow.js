/*
 * ! Ext JS Library 3.1.0 Copyright(c) 2006-2009 Ext JS, LLC licensing@extjs.com
 * http://www.extjs.com/license
 */
LoginWindow = function() {
	Ext.QuickTips.init();
	var self = this;
	var submitByEnter = {
		specialkey : function(field, e) {
			if (e.getKey() == Ext.EventObject.ENTER) {
				self.doAuth();
			}
		}
	};
	this.inputUsername = new Ext.form.TextField({
				id : 'input-login-username',
				fieldLabel : '用户名',
				msgTarget : 'side',
				mode : 'local',
				allowBlank : false,
				blankText : '请填写用户名',
				listeners : submitByEnter
			});

	this.inputPassword = new Ext.form.TextField({
				id : 'input-login-password',
				fieldLabel : '密码',
				msgTarget : 'side',
				inputType : 'password',
				allowBlank : false,
				blankText : '请填写密码',
				listeners : submitByEnter
			});

	this.formItems = [this.inputUsername, this.inputPassword];
	this.form = new Ext.FormPanel({
				labelAlign : 'left',
				items : this.formItems,
				border : false,
				bodyStyle : 'background:transparent;padding:10px;',
				buttonAlign : 'center',
				buttons : [{
							text : '登录',
							handler : function() {
								this.doAuth();
							},
							scope : this
						}, {
							text : '重置',
							handler : function() {
								this.form.getForm().reset()
							},
							scope : this
						}]
			});
	this.addEvents('userLogin');

	LoginWindow.superclass.constructor.call(this, {
				title : '登录',
				iconCls : 'feed-icon',
				id : 'login-win',
				autoHeight : true,
				width : 300,
				resizable : false,
				plain : true,
				modal : true,
				y : 100,
				autoScroll : false,
				closeAction : 'hide',
				items : this.form,
				listeners : {
					beforeshow : function() {
						this.form.getForm().reset();
					},
					scope : this
				}
			});
}

Ext.extend(LoginWindow, Ext.Window, {
			show : function(btn) {
				LoginWindow.superclass.show.apply(this, [btn]);
			},
			probe : function() {
				this.doAuth(true)
			},
			logout : function() {
				Ext.Ajax.request({
							url : 'auth!logout.action',
							success : function() {
							},
							scope : this
						});
			},
			doAuth : function(probe) {
				if (!probe && !this.form.getForm().isValid()) {
					return
				}
				this.el.mask('登录中...', 'x-mask-loading');
				var username = this.inputUsername.getValue();
				var password = this.inputPassword.getValue();

				Ext.Ajax.request({
							url : 'auth!login.action',
							params : {
								username : username,
								password : password,
								probe : probe
							},
							success : this.authCallback,
							failure : this.markInvalid,
							scope : this
						});
			},
			markInvalid : function(msg, target) {
				if (target) {
					target = Ext.getCmp('input-login-' + target);
				}
				if (!target) {
					target = this.formItems[this.formItems.length - 1];
				}
				target.markInvalid(msg && typeof msg == 'string'
						? msg
						: '登陆失败（连接服务器出错）');
				this.el.unmask();
			},

			authCallback : function(response, options) {
				var host = options.host;
				var result = Ext.decode(response.responseText);
				if (result.success) {
					this.el.mask('登录成功，正在为您转到目标页面', 'x-mask-loading');
					window.location = result.data;
				} else {
					if (result.message != 'probe')
						this.markInvalid(result.message, result.data);
					this.el.unmask();
				}

				return this.fireEvent('userLogin', result.obj);
			}
		});
