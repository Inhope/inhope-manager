NavPanel = function() {
	var self = this;
	this.data = {};
	var menu = Dashboard.u().filterAllowed([{
				id : 'auth.usr',
				name : '用户管理',
				module : '3',
				icon : 'icon-toolbox',
				panelClass : auth.user.ListPanel
			}, {
				id : 'auth.role',
				authName : 'auth.role',
				name : '角色管理',
				module : '1',
				icon : 'icon-toolbox',
				panelClass : auth.role.MainPanel
			}, {
				id : 'auth.perm',
				authName : 'auth.perm',
				name : '资源管理',
				module : '2',
				icon : 'icon-toolbox',
				panelClass : auth.PermissionTree
			}, {
				id : 'auth.op',
				authName : 'auth.op',
				name : '权限管理',
				module : '4',
				icon : 'icon-toolbox',
				panelClass : auth.OperationTree
			}, {
				id : 'auth.rapi',
				authName : 'auth.rapi',
				name : '接口管理',
				module : '5',
				icon : 'icon-toolbox',
				panelClass : auth.rapi.AddressRestrictPanel
			}]);
	var rootNode = new Ext.tree.TreeNode({
				id : '0',
				text : 'root'
			});
	var _nodeMap = {};
	for (var i = 0; i < menu.length; i++) {
		var _item = menu[i];
		var _node = new Ext.tree.TreeNode({
					id : _item.id,
					text : _item.name,
					iconCls : _item.icon,
					module : _item.module,
					panelClass : _item.panelClass
				});
		_nodeMap[_item.id] = _node;
		if (!_item.parent)
			rootNode.appendChild(_node);
		else
			_nodeMap[_item.parent].appendChild(_node);
	}
	var config = {
		title : '授权管理',
		border : false,
		autoScroll : true,
		rootVisible : false,
		lines : false,
		root : rootNode
	};
	NavPanel.superclass.constructor.call(this, config);

	this.modulePanels = {};
	this.on('click', function(n) {
				if (!n)
					return false;
				var module = n.attributes.module;
				var panelClass = n.attributes.panelClass;
				var p = self.showTab(panelClass, module + 'Tab', n.text,
						n.attributes.iconCls, true);
			});
}

Ext.extend(NavPanel, Ext.tree.TreePanel, {});

Dashboard.registerNav(NavPanel, 1);