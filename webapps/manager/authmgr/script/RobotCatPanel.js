RobotCatPanel = function() {
	var self = this;
	var pageSize = 50;
	var grid = this;
	this.robotcat = {};

	var Task = Ext.data.Record.create([{
				name : 'id',
				type : 'string'
			}, {
				name : 'taskName',
				type : 'string'
			}, {
				name : 'adminName',
				type : 'string'
			}, {
				name : 'createTime',
				type : 'string',
				dateFormat : 'Y-m-d H:i:s'
			},
			// {
			// name : 'taskUrl',
			// type : 'string'
			// },
			{
				name : 'location',
				type : 'string'
			}, {
				name : 'platform',
				type : 'string'
			}, {
				name : 'itemType',
				type : 'string'
			}, {
				name : 'executeTime',
				type : 'date',
				dateFormat : 'Y-m-d H:i:s'
			}, {
				name : 'timer',
				type : 'string'
			}, {
				name : 'timeInterval',
				type : 'string'
			}, {
				name : 'fileId',
				type : 'string'
			}, {
				name : 'state',
				type : 'string'
			}, {
				name : 'emails',
				type : 'string'
			}]);

	var reader = new Ext.data.JsonReader({
				idProperty : 'id',
				root : 'data',
				messageProperty : 'message',
				totalProperty : 'total'
			}, Task);

	var writer = new Ext.data.JsonWriter({
				writeAllFields : true
			});

	var proxy = new Ext.data.HttpProxy({
				api : {
					read : 'robot-cat!list.action',
					// create : 'robot-cat!save.action',
					// update : 'robot-cat!save.action',
					destroy : 'robot-cat!del.action'
				},
				listeners : {
					write : function(proxy, action, result, res, rs) {
						if (res.message) {
							Ext.Msg.alert("提示", res.message);
						}
					}
				}
			});

	var store = new Ext.data.Store({
				proxy : proxy,
				reader : reader,
				writer : writer,
				autoLoad : {
					params : {
						start : 0,
						limit : pageSize
					}
				},
				autoSave : true
			});

	var uploadWindow = new Ext.task.ExcelUploadWindow({
				url : 'robot-cat!_import.action',
				title : '上传测试用例，可上传07,03版本的excel。',
				updatePath : function(data) {
					if (data.success) {
						// editor.getFieldByName('fileId').setValue(data.message);
						self.robotcat.fileId = data.message;
					} else {
						// alert("错误: "+data.message);
						Ext.Msg.alert("提示", data.message);
					}
				}
			});

	var _platComboStore = new Ext.data.JsonStore({
				url : 'robot-cat!listPlatform.action',
				fields : ['tag', 'name'],
				autoLoad : true
			});
	var _locationComboStore = new Ext.data.JsonStore({
				url : 'robot-cat!listLocations.action',
				fields : ['tag', 'name'],
				autoLoad : true
			});
	var taskName = new Ext.form.TextField({
				fieldLabel : '任务名',
				name : 'taskName',
				anchor : '80%',
				allowBlank : false
			});
	var emails = new Ext.form.TextField({
				fieldLabel : '邮箱地址(多个请用半角逗号分割)',
				name : 'emails',
				anchor : '80%',
				allowBlank : true
			});
	var locationCombo = new Ext.form.ComboBox({
				name : 'location',
				fieldLabel : '地市',
				allowBlank : true,
				store : _locationComboStore,
				displayField : 'name',
				valueField : 'tag',
				anchor : '90%',
				triggerAction : 'all'
			});
	var platformCombo = new Ext.form.ComboBox({
				fieldLabel : '平台',
				name : 'platform',
				store : _platComboStore,
				displayField : 'name',
				valueField : 'tag',
				anchor : '90%',
				triggerAction : 'all'
			});
	var itemTypeCombo = new Ext.form.ComboBox({
				fieldLabel : '项目类型',
				name : 'itemType',
				width : 110,
				value : 'enterprize',
				store : new Ext.data.SimpleStore({
							fields : ['value', 'text'],
							data : [['enterprize', '企业'], ['weixin', '微信接口']]
						}),
				displayField : 'text',
				valueField : 'value',
				triggerAction : 'all',
				mode : 'local'
			});
	var executeTime = new ClearableDateTimeField({
				fieldLabel : "执行时间",
				name : "executeTime",
				editable : false,
				validator : function(v) {
					if (v) {
						if (!v) {
							Ext.Msg.alert("提示", '执行时间不能为空!');
							return false;
						}
						return true;
					}
					return true;
				},
				width : 160
			});
	var timerCombo = new Ext.form.ComboBox({
				fieldLabel : '监控类型',
				name : 'timer',
				store : new Ext.data.SimpleStore({
							fields : ['value', 'text'],
							data : [['promptly', '立即'], ['everysec', '按秒监控'],
									['everymin', '按分监控'], ['everyday', '每天'],
									['everymonth', '每月']]
						}),
				displayField : 'text',
				valueField : 'value',
				triggerAction : 'all',
				width : 100,
				mode : 'local'
			});
	var timeInterval = new Ext.form.TextField({
				name : 'timeInterval',
				fieldLabel : '间隔时长（秒）',
				readOnly : false,
				allowBlank : true,
				validator : function(v) {
					if (v) {
						if (isNaN(v)) {
							Ext.Msg.alert("提示", '间隔时长必须为数字!');
							return false;
						}
						return true;
					}
					return true;
				},
				width : 100
			});

	var editor = new Ext.task.FormRowEditor({
				title : '任务信息',
				width : 600,
				height : 280,
				store : store,
				items : [taskName, emails, {
							layout : 'column',
							items : [{
										columnWidth : .3,
										layout : 'form',
										items : locationCombo
									}, {
										columnWidth : .3,
										layout : 'form',
										items : platformCombo
									}, {
										columnWidth : .2,
										layout : 'form',
										items : itemTypeCombo
									}]
						}, {
							layout : 'column',
							items : [{
										columnWidth : .3,
										layout : 'form',
										items : executeTime
									}, {
										columnWidth : .2,
										layout : 'form',
										items : timerCombo

									}, {
										columnWidth : .2,
										layout : 'form',
										items : timeInterval
									}, {
										columnWidth : .3,
										layout : 'form',
										items : {
											xtype : 'button',
											align : 'right',
											style : 'margin-top:20px',
											text : '导入测试用例',
											handler : function() {
												uploadWindow.show();
											}
										}
									}, {
										layout : 'form',
										items : {
											xtype : 'hidden',
											id : 'relaseTestfileId',
											name : 'fileId'
										}
									}, {
										layout : 'form',
										items : {
											xtype : 'hidden',
											id : 'robotCatId',
											name : 'id'
										}
									}]
						}],
				buttons : [{
					text : '保存',
					handler : function() {
						if (taskName.getValue())
							self.robotcat.taskName = taskName.getValue();
						else {
							Ext.Msg.alert('提示', '任务名称不能为空');
							return;
						}
						if (emails.getValue())
							self.robotcat.emails = emails.getValue();
						else {
							Ext.Msg.alert('提示', '邮箱地址不能为空');
							return;
						}
						self.robotcat.location = locationCombo.getValue();
						if (platformCombo.getValue())
							self.robotcat.platform = platformCombo.getValue();
						else {
							Ext.Msg.alert('提示', '平台不能为空');
							return;
						}
						self.robotcat.itemType = itemTypeCombo.getValue();
						if (executeTime.getValue())
							self.robotcat.executeTime = executeTime.getValue();
						else {
							Ext.Msg.alert('提示', '执行时间不能为空');
							return;
						}
						if (timerCombo.getValue())
							self.robotcat.timer = timerCombo.getValue();
						else {
							Ext.Msg.alert('提示', '监控类型不能为空');
							return;
						}
						if (timeInterval.getValue())
							self.robotcat.timeInterval = timeInterval
									.getValue();
						else {
							Ext.Msg.alert('提示', '间隔时间不能为空');
							return;
						}
						if (Ext.getCmp("relaseTestfileId").getValue()) {
							self.robotcat.fileId = Ext
									.getCmp("relaseTestfileId").getValue();
						}
						self.robotcat.id = Ext.getCmp("robotCatId").getValue();
						Ext.Ajax.request({
									url : 'robot-cat!save.action',
									params : {
										'data' : Ext.encode(ux.util.resetEmptyString(self.robotcat),
												"Y-m-d H:i:s")
									},
									success : function(resp) {
										var obj = Ext.util.JSON
												.decode(resp.responseText);
										if (obj.success){
											Dashboard.setAlert("任务更新成功！");
											store.load();
										}else
											Ext.Msg.alert('异常', obj.message);
									},
									failure : function(resp) {
										Ext.Msg.alert('错误', resp.responseText);
									}
								});
					}
				}, {
					text : '取消',
					handler : function() {
						editor.hide();
					}
				}]
			});

	this.getSelectionModel().on('selectionchange', function(sm) {
				grid.removeBtn.setDisabled(sm.getCount() < 1);
				grid.taskResultBtn.setDisabled(sm.getCount() < 1);
			});

	this.tbar = [{
				iconCls : 'icon-user-add',
				text : '添加任务信息',
				handler : function() {
					editor.record = new Task({
								createTime : new Date().format('Y-m-d H:i:s')
							});
					self.robotcat.fileId = "";
					editor.show();
				}
			}, '-', {
				ref : '../removeBtn',
				iconCls : 'icon-user-delete',
				text : '删除任务信息',
				disabled : true,
				handler : function() {
					var s = grid.getSelectionModel().getSelections();
					Ext.MessageBox.confirm('删除任务信息确认', '您确认删除选中的任务信息？',
							function(btn, text) {
								if (btn == 'yes') {
									for (var i = 0, r; r = s[i]; i++) {
										store.remove(r);
									}
								}
							});

				}
			}, '-', {
				ref : '../taskResultBtn',
				iconCls : 'icon-grid',
				text : '测试结果',
				disabled : true,
				handler : function() {
					var s = grid.getSelectionModel().getSelections();
					for (var i = 0, r; r = s[i]; i++) {
						var cfg = {
							taskId : r.get('id'),
							taskName : r.get('taskName')
						};
						var knowledgeResultGrid = function() {
							return new KnowledgeResultGrid(cfg);
						};
						self.navPanel.showTab(knowledgeResultGrid,
								'taskResult_' + cfg.taskId, cfg.taskName
										+ '->测试结果', '', true)
					}
				}
			}];
	this.columns = [{
				header : '任务名',
				dataIndex : 'taskName',
				width : 150,
				sortable : true
			}, {
				header : '管理员',
				dataIndex : 'adminName',
				width : 150,
				sortable : true
			}, {
				header : '执行时间',
				dataIndex : 'executeTime',
				width : 150,
				sortable : true,
				xtype : 'datecolumn',
				format : 'Y-m-d H:i:s'
			}, {
				header : '测试周期',
				dataIndex : 'timer',
				width : 150,
				sortable : true,
				renderer : function(v) {
					if (v == 'promptly') {
						return '立即';
					} else if (v == 'everysec') {
						return '按秒监控';
					} else if (v == 'everymin') {
						return '按分监控';
					} else if (v == 'everyday') {
						return '每天';
					} else if (v == 'everymonth') {
						return '每月';
					}
				}
			}, {
				header : '状态',
				dataIndex : 'state',
				width : 150,
				sortable : true,
				renderer : function(v) {
					if (v) {
						return "<font color='red'>" + v + "</font>";
					}
					return "";
				}
			}, {
				header : '创建时间',
				dataIndex : 'createTime',
				width : 150,
				sortable : true,
				xtype : 'datecolumn',
				format : 'Y-m-d H:i:s'
			}];

	var pagingToolbar = new Ext.PagingToolbar({
				store : store,
				displayInfo : true,
				pageSize : pageSize,
				prependButtons : true,
				beforePageText : '第',
				afterPageText : '页，共{0}页',
				displayMsg : '第{0}到{1}条记录，共{2}条',
				emptyMsg : "没有记录"
			});

	RobotCatPanel.superclass.constructor.call(this, {
				store : store,
				region : 'center',
				border : false,
				viewConfig : {
					forceFit : true,
					markDirty : true
				},
				bbar : pagingToolbar,
				listeners : {
					rowdblclick : function(grid, index, e) {
						editor.record = grid.getSelectionModel().getSelected();
						editor.show();
					}
				}
			});
}

Ext.extend(RobotCatPanel, Ext.grid.GridPanel, {
			loadData : function() {
				this.getStore().load();
			}
		});