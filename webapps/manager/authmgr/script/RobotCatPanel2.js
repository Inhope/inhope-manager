RobotCatPanel2 = function() {
	
	var taskGrid = new TaskGrid();
	taskGrid.navPanel = this.navPanel;
	var cfg = {
			layout : 'fit',
		    title: '任务菜单',
		    region:' center',
		    margins: '5 0 0 0',
		    cmargins: '5 5 0 0',
		    items : [taskGrid]
	};
	
	RobotCatPanel2.superclass.constructor.call(this, cfg);
	
};

Ext.extend(RobotCatPanel2, Ext.Panel, {
	
});