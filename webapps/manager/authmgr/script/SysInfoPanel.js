SysInfoPanel = function(_cfg) {
	var self = this;
	var chartResponseText = "";
	var _refreshTime = 5 * 60;
	var _currRefreshTime = _refreshTime;
	var _sm = new Ext.grid.RowSelectionModel({
				singleSelect : true
			});
	// var _selectNode = "";
	// var _selectType = "";
	var _selectLine = 0;
	var _rowData;
	var _reader = new Ext.data.JsonReader({
				idProperty : 'id',
				root : 'data',
				fields : [{
							name : 'node',
							type : 'string'
						}, {
							name : 'type',
							type : 'string'
						},, {
							name : "startTime",
							type : 'long'
						}, {
							name : "cpuUtilization",
							type : 'int'
						}, {
							name : "sysUsedMemory",
							type : 'int',
							convert : self.formatByte
						}, {
							name : "sysFreeMemory",
							type : 'int',
							convert : self.formatByte
						}, {
							name : "sysTotalMemory",
							type : 'int',
							convert : self.formatByte
						}, {
							name : "sysUsedMomoryRate",
							type : 'int'
						}, {
							name : "jvmUsedMemory",
							type : 'int',
							convert : self.formatByte
						}, {
							name : "jvmFreeMemory",
							type : 'int',
							convert : self.formatByte
						}, {
							name : "jvmTotalMemory",
							type : 'int',
							convert : self.formatByte
						}, {
							name : "jvmUsedMomoryRate",
							type : 'int'
						}, {
							name : "createDate",
							type : 'date',
							convert : function(v, r) {
								return new Date(r.createDate);
							}
						}, {
							name : "osName",
							type : 'string'
						}, {
							name : "osVersion",
							type : 'string'
						}, {
							name : "osArch",
							type : 'string'
						}, {
							name : "javaVersion",
							type : 'string'
						}, {
							name : "javaVendor",
							type : 'string'
						}, {
							name : "typeVersion",
							type : 'string'
						}, {
							name : "online",
							type : 'boolean'
						}]
			});
	var myExpander = new Ext.grid.RowExpander({
		tpl : new Ext.XTemplate('<p style="color:#999;margin:2px 0px 2px 48px;">{["&nbsp os.name:"+values.osName+" &nbsp os.version:"+values.osVersion+" &nbsp os.arch:"+values.osArch+" &nbsp java.version:"+values.javaVersion+" &nbsp java.vendor:"+values.javaVendor+"<br/> &nbsp 系统使用内存:"+values.sysUsedMemory+" &nbsp 系统空闲内存:"+values.sysFreeMemory+" &nbsp 系统总内存:"+values.sysTotalMemory+" &nbsp 系统运行时间:"+this.formatDate(values.startTime,values.createDate)]}</p>',{
			formatDate : function (nS,createDate) {
				var date1=new Date(parseInt(nS));  //开始时间  
				var date2=createDate; //结束时间  
				var date3=date2.getTime()-date1.getTime()  //时间差的毫秒数  
				//计算出相差天数  
				var days=Math.floor(date3/(24*3600*1000))  
				//计算出小时数  
				var leave1=date3%(24*3600*1000)    //计算天数后剩余的毫秒数  
				var hours=Math.floor(leave1/(3600*1000))  
				//计算相差分钟数  
				var leave2=leave1%(3600*1000)        //计算小时数后剩余的毫秒数  
				var minutes=Math.floor(leave2/(60*1000))  
				//计算相差秒数  
				var leave3=leave2%(60*1000)      //计算分钟数后剩余的毫秒数  
				var seconds=Math.round(leave3/1000)  
				return  ""+days+"天"+hours+"小时"+minutes+"分钟"+seconds+"秒";
			}
		})
	});
	var _store = new Ext.data.GroupingStore({
				proxy : new Ext.data.HttpProxy({
							url : 'log-sys-info!list.action'
						}),
				autoLoad : true,
				reader : _reader,
				groupField : 'type'
			});
	_store.on('load', function() {
				if (this.getCount() > 0) {
//					self.openSysInfoChart();
					self.grid.getSelectionModel().selectRow(_selectLine, true);
				}
			});
	var _tbarEx = new Ext.Toolbar({
		enableOverflow : true,
		items : ['自动刷新时间:', {
					xtype : 'radiogroup',
					fieldLabel : '自动刷新时间',
					width : 180,
					items : [{
								boxLabel : '1分钟  ',
								name : 'refreshTime',
								inputValue : 1
							}, {
								boxLabel : '5分钟  ',
								name : 'refreshTime',
								inputValue : 5,
								checked : true
							}, {
								boxLabel : '10分钟  ',
								name : 'refreshTime',
								inputValue : 10
							}],
					listeners : {
						'change' : function(checkbox, checked) {
							_refreshTime = checked.inputValue * 60;
							_currRefreshTime = _refreshTime;
						}
					}
				}, '-', {
					id : 'refreshBtn',
					iconCls : 'icon-refresh',
					text : '刷新',
					handler : function() {
						_store.reload();
						self.openSysInfoChart();
					}
				}, '->', '使用率:', '<p style="color:#FF0000">红色：大于80%&nbsp</p>',
				'<p style="color:#0000FF">蓝色：30-80%&nbsp</p>',
				'<p style="color:green">绿色：小于30%&nbsp</p>']//', '-',双击可查看系统信息曲线图'
	});
	var config = {
		id : 'sysInfoPanel',
		store : _store,
		region : 'center',
		columnLines : true,
		border : false,
		style : 'border-right: 1px solid ' + sys_bdcolor + ' border-bottom: 1px solid ' + sys_bdcolor,
		sm : _sm,
		loadMask : true,
		tbar : _tbarEx,
		view : new Ext.grid.GroupingView({
					forceFit : true,
					groupTextTpl : '{text} 节点数:{[values.rs.length]}'
				}),
		colModel : new Ext.grid.ColumnModel({
					columns : [new Ext.grid.RowNumberer(), myExpander, {
								header : 'IP',
								width : 100,
								dataIndex : 'node'
							}, {
								header : '平台类型',
								width : 80,
								dataIndex : 'type',
								hidden : true
							}, {
								header : '进程CPU使用率(%)',
								width : 100,
								dataIndex : 'cpuUtilization',
								renderer : self.formatTate
							}, {
								header : '系统内存使用率(%)',
								width : 100,
								dataIndex : 'sysUsedMomoryRate',
								renderer : self.formatTate
							}, {
								header : 'JVM内存使用率(%)',
								width : 100,
								dataIndex : 'jvmUsedMomoryRate',
								renderer : self.formatTate
							}, {
								header : '使用的内存',
								width : 100,
								dataIndex : 'sysUsedMemory',
								hidden : true
							}, {
								header : '空闲的内存',
								width : 100,
								dataIndex : 'sysFreeMemory',
								hidden : true
							}, {
								header : '总内存',
								width : 100,
								dataIndex : 'sysTotalMemory',
								hidden : true
							}, {
								header : 'JVM使用的内存',
								width : 100,
								dataIndex : 'jvmUsedMemory'
							}, {
								header : 'JVM空闲的内存',
								width : 100,
								dataIndex : 'jvmFreeMemory'
							}, {
								header : 'JVM总内存',
								width : 100,
								dataIndex : 'jvmTotalMemory'
							}, {
								header : '是否在线',
								width : 100,
								dataIndex : 'online',
								renderer : self.formatOnline
							}]
				}),
		plugins : [myExpander]
	};
	var _grid = new Ext.grid.GridPanel(config);
	this.grid = _grid;
	this.sm = _sm;

	var cfg = {
		layout : 'border',
		border : false,
		items : [_grid, {
					region : 'south',
					header : false,
//					ref : 'southPanel',
					html : '<iframe src="script/chart/index.jsp" frameborder=0 width="100%" height="100%"></iframe>',
					split : true,
					collapsible : true,
					collapseMode : 'mini',
					frame : false,
					border : false,
					height : 430
				}]
	};
	SysInfoPanel.superclass.constructor.call(this, cfg);

	this._chartStores = new Map();
	this._hostTypeColor = new Map();
	this._colorArray = new Array("#6C3365","#336666","#613030","#844200","#424200","#006000","#003E3E","#000079","#5E005E","#4D0000")
//	this.newSysInfoChart = new ss(title,key,self)
	this.newSysInfoChart = function(title,key) {
		alert(title+","+key)
		var resStore = self.grid.getStore();
		var fields = new Array();
		var seriess = new Array();
		var index = 0 ;
		resStore.each(function(){
			var resStoreThis = this;
			var color = "";
			if(self._hostTypeColor.containsKey(resStoreThis.id)){
				color = self._hostTypeColor.get(resStoreThis.id);
			}else{
				color = getRandomColor();
				self._hostTypeColor.put(resStoreThis.id,color);
			}
			color = self._colorArray[index];
			index++;
			seriess.push(new Ext.chart.Series(
				{
					type : 'line',
					displayName : resStoreThis.data.node,
					yField : (resStoreThis.data.type+''+resStoreThis.data.node+''+key).replace(/\./g, "").replace(":", ""),
					xField : (resStoreThis.data.type+''+resStoreThis.data.node+'time').replace(/\./g, "").replace(":", ""),
					style : {
						color : color,
						size : 2
					}
				}
			));
			
			fields.push((resStoreThis.data.type+''+resStoreThis.data.node+'time').replace(/\./g, "").replace(":", ""));
			fields.push((resStoreThis.data.type+''+resStoreThis.data.node+''+key).replace(/\./g, "").replace(":", ""));
		});
		var chartReader = new Ext.data.JsonReader({
			idProperty : 'id',
			root : 'data',
			fields : fields
		});
		
		var yAxis = function(){
			var title = "";
			if(key == 'cpuUtilization'){
				title = '使用率（%）';
				return new Ext.chart.NumericAxis({
						//title : title,
						maximum: 100,
		                minimum: 0
		                //orientation:'vertical',
		                //position: 'right',
				});
			}else if(key == 'sysUsedMemory'){
				title = '系统内存';
				return new Ext.chart.NumericAxis({
					//title : title,
					labelRenderer  : self.formatByte
				});
			}else if(key == 'jvmUsedMemory'){
				title = 'JVM内存';
				return new Ext.chart.NumericAxis({
					//title : title,
					labelRenderer  : self.formatByte 
			});
			}
		};
		
		if(!this._chartStores.containsKey(key)){
			this._chartStores.put(key,new Ext.data.Store({
				proxy : new Ext.data.HttpProxy({
					url : 'log-sys-info!dayListAll.action'
				}),
				autoLoad : true,
				reader : chartReader
			}));
		}
		var useChartStores = this._chartStores.get(key);
		return new Ext.chart.LineChart({
					title : title,
					store : useChartStores,
					url : '../ext/resources/charts.swf',
					animate : true,
					shadow : true,
					series : seriess,
					height : 400,
					isopen : false,
					tipRenderer : function(chart, record, index, series) {
						var text = '';
						if (key == 'cpuUtilization') {
							text += series.displayName + ' 当前使用率为： ';
							text += record.data[series.yField] + "%";
						} else if (key == 'sysUsedMemory') {
							text += series.displayName + ' 使用量： ';
							text += self.formatByte(record.data[series.yField]);
						} else if (key == 'jvmUsedMemory') {
							text += series.displayName + ' 使用量： ';
							text += self.formatByte(record.data[series.yField]);
						}
						return text;
					},
					extraStyle : {
						legend : {
							display : 'bottom',
							padding : 5,
							font : {
								family : 'Tahoma',
								size : 10
							}
						}
					},
					dataTip : {
						padding : 5,
						border : {
							color : 0x99bbe8,
							size : 1
						},
						background : {
							color : 0xDAE7F6,
							alpha : 0.9
						},
						font : {
							name : 'Tahoma',
							color : 0x15428B,
							size : 10,
							bold : true
						}
					},
					xAxis: new Ext.chart.NumericAxis({
						title : '时间',
		                maximum: 24,
		                minimum: 0,
		                adjustMinimumByMajorUnit: 1
		            }),
					yAxis : yAxis()
				});
	};
	var getRandomColor = function(){
//		return '#' +
//			(function(color){
//				return (color +='0123456789abcdef'[Math.floor(Math.random()*16)])
//				&& (color.length == 6) ? color : arguments.callee(color);
//		})('');
		return '#'+('00000'+(Math.random()*0x1000000<<0).toString(16)).slice(-6);
	};

	this.grid.addListener('rowdblclick', function() {
		if (_sm.getSelected().data.online) {
			_rowData = _sm.getSelected().data;
//					self.openSysInfoChart(_rowData.node, _rowData.type);
			_selectLine = _grid.getSelectionModel().lastActive;
		} else {
			Ext.MessageBox.alert('提示', '设备暂不在线，不能查看', function(btn) {
						self.grid.getSelectionModel().selectRow(
								_selectLine, true);
					});
		}
	});

	Ext.TaskMgr.start({
			run : function() {
				if (_currRefreshTime <= 0) {
					_currRefreshTime = _refreshTime;
					_store.reload();
					self.openSysInfoChart();
				} else {
					if(Ext.getCmp('refreshBtn')){
						Ext.getCmp('refreshBtn').setText('刷新(还有'+ _currRefreshTime + '秒)');
						_currRefreshTime--;
					}
				}
			},
			scope : this,
			interval : 1000
		});
};

Ext.extend(SysInfoPanel, Ext.Panel, {
	activeTab : 0,
	loadData : function() {
		this.grid.getStore().load();
	},
	exportData : function() {

	},
	formatTate : function(val) {
		if (val >= 0 && val < 30) {
			return '<span style="color:green;">' + val + '%</span>';
		} else if (val >= 30 && val < 80) {
			return '<span style="color:blue;">' + val + '%</span>';
		} else if (val <= 100){
			return '<span style="color:red;">' + val + '%</span>';
		} else {
			return '<span>未知</span>';
		}
		return val;
	},
	formatOnline : function(val, w, m) {
		if (val) {
			return '<span style="color:green;">在线</span>';
		} else {
			return '<span style="color:red;">离线</span>'
		}
	},
	formatByte : function(val) {
		if (val < 0) {
			return '未知';
		} else if (val >= 0 && val < 1024 * 1024) {
			return (val / 1024).toFixed(2) + "K";
		} else if (val >= 1024 * 1024 && val < 1024 * 1024 * 1024) {
			return (val / 1024 / 1024).toFixed(2) + "M"
		} else if (val >= 1024 * 1024 * 1024 && val < 1024 * 1024 * 1024 * 1024) {
			return (val / 1024 / 1024 / 1024).toFixed(2) + "G"
		}
		return val;
	},
	openSysInfoChart : function() {
		var self = this;
		Ext.Ajax.request({
			url : 'log-sys-info!dayListAll.action',
			params : Ext.urlEncode({
//						node : node,
//						type : type
					}),
			success : function(response) {
				self.chartResponseText = response.responseText;
				var result = Ext.util.JSON
						.decode(response.responseText);
				if (result.success) {
					//获取MAP中所有VALUE的数组（ARRAY）
					var array = this._chartStores.values();
					for (var i = 0; i < array.length; i++) {
						array[i].loadData(result);
			        }
				} else {
					var array = this._chartStores.values()
					for (var i = 0; i < array.length; i++) {
						array[i].loadData({
							data : {}
						});
					}
				}
			},
			failure : function(response) {
				if (response.responseText)
					Ext.Msg.alert('出错了', response.responseText.replace(
									/\r\n/ig, '<br>'));
			},
			scope : this
		});
		var tabpanel = new Ext.TabPanel({
	        activeTab: self.activeTab,
	        //shadow : true,
	        //plain:true,
	        //footer:true,
	        border : false,
	        //height : 400,
	        tabPosition : 'top',
	        defaults:{autoScroll: true},
	        items:[
//	        	self.newSysInfoChart("进程CPU","cpuUtilization"),
//	        	self.newSysInfoChart("系统内存","sysUsedMemory"),
//	        	self.newSysInfoChart("JVM内存","jvmUsedMemory")
//	        	new Chart4("进程CPU","cpuUtilization",self),
//	        	new Chart4("系统内存","sysUsedMemory",self),
//	        	new Chart4("JVM内存","jvmUsedMemory",self)
				{
					xtype:"panel",
					height : 550,
					title: 'Hello1',
					width: 200,
					html: '<p>World!</p>'
				},new authmgr.chart.MonitorPanelChart4(),{
	        		title:'系统内存',
	        		panelClass : authmgr.chart.MonitorChart4(this._chartStores,this._hostTypeColor,this._colorArray,"sysUsedMemory",self.grid.getStore())
	        	},
	        	authmgr.chart.MonitorChart4(this._chartStores,this._hostTypeColor,this._colorArray,"cpuUtilization",self.grid.getStore())
//	        	,{
//	        		title:'系统内存',
//	        		panelClass : authmgr.chart.MonitorChart4(this._chartStores,this._hostTypeColor,this._colorArray,"sysUsedMemory",self.grid.getStore())
//	        	},{
//	        		title:'JVM内存',
//	        		panelClass : authmgr.chart.MonitorChart4(this._chartStores,this._hostTypeColor,this._colorArray,"jvmUsedMemory",self.grid.getStore())
//	        	}
//	        	authmgr.chart.MonitorChart4
			],
	        listeners: {
	        	'tabchange':function(tabs,tab){
	        		if(tab.title == "系统内存"){
	        			self.activeTab = 1;
	        			if(tab.isopen){
	        				tab.destroy();
		        			tabs.insert(1,self.newSysInfoChart("系统内存","sysUsedMemory")).show();
	        			}else{
	        				tab.isopen = true;
	        			}
	        		}else if(tab.title == "JVM内存"){
	        			self.activeTab = 2;
	        			if(tab.isopen){
	        				tab.destroy();
		        			tabs.add(self.newSysInfoChart("JVM内存","jvmUsedMemory")).show();
	        			}else{
	        				tab.isopen = true;
	        			}
	        		}
	        	}
	        }
	    })
//		this.southPanel.add(tabpanel);
//		this.southPanel.add(new Ext.Panel({
//			border : false,
//			layout : 'fit',
//			html : '<iframe src="http://www.baidu.com" frameborder=0 width="100%" height="100%"></iframe>'
//		}));
		this.doLayout();
		// this.sysInfoChartWindow.show();
	},
	gridRowSelected : function(rowIndow) {
		this.grid.selModel.selectRow(rowIndex, false);
	},
	testObj : function(obj) {
		var PropertyList = '';
		var PropertyCount = 0;
		for (i in obj) {
			if (obj.i != null)
				PropertyList = PropertyList + i + '属性：' + obj.i + '\r\n';
			else
				PropertyList = PropertyList + i + '方法\r\n';
		}
		alert(PropertyList);
	}
	
	
});