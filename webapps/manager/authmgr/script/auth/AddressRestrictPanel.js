Ext.namespace('auth.rapi');
auth.rapi.AddressRestrictPanel = function() {

	var self = this;

	var store = new Ext.data.Store({
				proxy : new Ext.data.HttpProxy({
							api : {
								read : 'address-restrict!list.action',
								create : 'address-restrict!save.action',
								update : 'address-restrict!save.action',
								destroy : 'address-restrict!delete.action'
							}
						}),
				reader : new Ext.data.JsonReader({
							idProperty : 'id',
							root : 'data',
							fields : [{
										name : 'pattern',
										type : 'string'
									}, {
										name : 'type',
										type : 'int'
									}]
						}),
				writer : new Ext.data.JsonWriter({
							writeAllFields : true
						}),
				autoLoad : true,
				listeners : {
					beforewrite : function(store, data, rs) {
						if (!rs.get('pattern'))
							return false;
					}
				}
			});
	var editor = new Ext.ux.grid.RowEditor({
				saveText : '保存',
				cancelText : '取消',
				errorSummary : false
			});
	editor.on('canceledit', function(rowEditor, changes, record, rowIndex) {
				var r, i = 0;
				while ((r = store.getAt(i++))) {
					if (r.phantom) {
						store.remove(r);
					}
				}
			});
	var sm = new Ext.grid.CheckboxSelectionModel();
	var typeData = [[1, 'WebService'], [2, 'WEB']];
	var typeMap = {};
	for (var i = 0; i < typeData.length; i++)
		typeMap[typeData[i][0]] = typeData[i][1];
	var config = {
		id : 'addressRestrictPanel',
		store : store,
		margins : '0 5 5 5',
		border : false,
		autoExpandColumn : 'pattern_id',
		plugins : [editor],
		tbar : [{
					iconCls : 'icon-add',
					text : '添加允许访问IP规则',
					handler : function() {
						var fix = new store.recordType({
									name : '',
									type : 1
								});
						editor.stopEditing();
						store.insert(0, fix);
						self.getSelectionModel().selectRow(0);
						editor.startEditing(0, 1);
					}
				}, {
					iconCls : 'icon-delete',
					text : '删除规则',
					handler : function() {
						editor.stopEditing();
						var rows = self.getSelectionModel().getSelections();
						if (rows.length == 0) {
							Ext.Msg.alert('提示', '请至少选择一条记录！');
							return false;
						}
						Ext.Msg.confirm('提示', '确认删除这' + rows.length + '条记录？',
								function(btn) {
									if (btn == 'yes') {
										var ids = [];
										Ext.each(rows, function() {
													ids.push(this.id)
												});
										Ext.Ajax.request({
											url : 'address-restrict!delete.action',
											params : {
												ids : ids.join(',')
											},
											success : function(resp) {
												Ext.Msg.alert('提示', '删除成功！',
														function() {
															store.reload();
														});
											},
											failure : function(resp) {
												Ext.Msg.alert('错误',
														resp.responseText);
											}
										})
									}
								});
					}
				}, {
					iconCls : 'icon-refresh',
					text : '刷新',
					handler : function() {
						store.reload();
					}
				}, {
					iconCls : 'icon-status-ok',
					text : '应用',
					handler : function() {
						Ext.Ajax.request({
									url : 'address-restrict!apply.action',
									success : function(resp) {
										Ext.Msg.alert('提示', '应用成功！');
									},
									failure : function(resp) {
										Ext.Msg.alert('错误', resp.responseText);
									}
								})
					}
				}, '-', '类型:', new Ext.form.ComboBox({
							id : 's_type_suf',
							triggerAction : 'all',
							width : 100,
							editable : false,
							value : '',
							mode : 'local',
							store : new Ext.data.ArrayStore({
										fields : ['type', 'displayText'],
										data : [['', '全部']].concat(typeData)
									}),
							valueField : 'type',
							displayField : 'displayText'
						}), ' ', {
					text : '搜索',
					iconCls : 'icon-search',
					handler : function() {
						self.search();
					}
				}, ' ', {
					text : '全部',
					iconCls : 'icon-search',
					handler : function() {
						self.search(true);
					}
				}],
		sm : sm,
		loadMask : true,
		colModel : new Ext.grid.ColumnModel({
					columns : [{
								id : 'pattern_id',
								header : '允许访问IP规则',
								dataIndex : 'pattern',
								editor : {
									xtype : 'textfield',
									allowBlank : false,
									blankText : '请输入规则'
								}
							}, {
								header : '类型',
								dataIndex : 'type',
								width : 200,
								renderer : function(value, metaData, record,
										rowIndex, colIndex, store) {
									return typeMap[value];
								},
								editor : {
									xtype : 'combo',
									allowBlank : false,
									editable : false,
									triggerAction : 'all',
									mode : 'local',
									store : new Ext.data.ArrayStore({
												fields : ['id', 'displayText'],
												data : typeData
											}),
									valueField : 'id',
									displayField : 'displayText'
								}
							}]
				})
	};
	auth.rapi.AddressRestrictPanel.superclass.constructor.call(this, config);
}

Ext.extend(auth.rapi.AddressRestrictPanel, Ext.grid.GridPanel, {
			loadData : function() {
				this.getStore().load()
			},
			search : function(showAll) {
				var tbar = this.getTopToolbar();
				var _typeCombo = tbar.findById('s_type_suf');
				if (showAll) {
					_typeCombo.setValue('');
				}
				var store = this.getStore();
				delete store.baseParams['type'];
				var _type = _typeCombo.getValue();
				if (_type)
					store.setBaseParam('type', _type);
				store.load();
			}
		});
