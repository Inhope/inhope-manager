Ext.namespace('auth')
auth.PermTreeField = Ext.extend(Ext.Panel, {
			layout : 'fit',
			getTreecomboClass : function() {
				return this.treecomboClass
						? this.treecomboClass
						: auth.PermTreeSelector;
			},
			initComponent : function() {
				var combo = new (this.getTreecomboClass())({
							treeConfig : this.treeConfig
						});
				combo.initList();
				this.combo = combo
				this.items = combo.tree
				this.tree = combo.tree
				if (this.onValueChange)
					combo.onValueChange = this.onValueChange

				auth.PermTreeField.superclass.initComponent.call(this)
				var layoutOnceL = function() {
					this.el.mask('加载中...');
					this.removeListener('afterlayout', layoutOnceL, this)
				};
				this.on('afterlayout', layoutOnceL, this)
				combo.loader.on('beforeload', function() {
							this.el.mask('加载中...')
						}, this)
				combo.on('allload', function() {
							if (this.el)
								this.el.unmask()
						}, this)
			},
			getTree : function() {
				return this.tree
			},
			setValue : function(v) {
				this.combo.setValue(v)
			},
			getValue : function() {
				return this.combo.getValue()
			},
			reset : function() {
				this.combo.reset();
			}
		})
auth.PermTreeSelector = Ext.extend(TreeComboCheck, {
	border : false,
	autoScroll : true,
	dataUrl : 'permission!listAllTree.action',
	setValue : function(v) {
		auth.PermTreeSelector.superclass.setValue.call(this, v);
		if (this.tree._loaded) {
			this.expandCheckedNode(this.tree)
		}
	},
	expandCheckedAfterRender : false,
	initComponent : function() {
		this.treeConfig = this.treeConfig || {};
		this.treeConfig = Ext.apply(this.treeConfig, {
					// empty listeners to disable 'checkchange'syncing
					// underlining values
					listeners : {},
					lines : true,
					border : false,
					tbar : new Ext.Toolbar({
								items : [{
											text : '清空',
											iconCls : 'icon-refresh',
											handler : function() {
												this.reset()
											}.dg(this)
										}, {
											text : '刷新',
											iconCls : 'icon-refresh',
											handler : function() {
												this.tree.root.reload();
											}.dg(this)
										}, {
											text : '展开所有节点',
											iconCls : 'icon-status-add',
											handler : function() {
												this.tree.expandAll();
											}.dg(this)
										}, {
											text : '折叠所有节点',
											iconCls : 'icon-status-cancel',
											handler : function() {
												this.tree.getRootNode()
														.collapse(true);
											}.dg(this)
										}]
							})
				})
		auth.PermTreeSelector.superclass.initComponent.call(this);
		this._buildEvents();
	},
	_buildTbar : function() {
	},
	// @override
	valueTransformer : function(node) {
		var d = {
			id : node.id
		}
		if (this.isCate(node)) {
			d.cateId = node.id
		} else {
			if (this.isPerm(node)) {
				d.permId = node.id;
			} else {
				d.cateId = node.id.split('.')[0];
				d.permId = node.id.split('.')[1];
			}
		}
		return d
	},
	initList : function() {
		auth.PermTreeSelector.superclass.initList.call(this);

		this._nodeToLoad = {};
		this.tree.on('load', function(node) {
					if (node.text == 'root') {
						completeChecker.start(function() {
									// defer for async-load expanding
									this.expandCheckedNode.defer(100, this,
											[this.tree])
									this.tree._loaded = true;
									this.fireEvent('allload')
								}.dg(this))
					}
					Ext.each(node.childNodes, function(n) {
								if (n.isLoaded) {
									completeChecker.pushFlag(n.text)
								}
							}, this)
					completeChecker.removeFlag(node.text)
				}, this)
		var completeChecker = {
			_c : {},
			pushFlag : function(f) {
				this._c[f] = true;
			},
			removeFlag : function(f) {
				delete this._c[f];

				var empty = true;
				for (var key in this._c) {
					empty = false;
					break;
				}
				if (empty && this._cb)
					this._cb()
			},
			checkComplete : function() {

			},
			reset : function() {
				delete this._c
				delete this._cb
				this._c = {}
			},
			start : function(cb) {
				this.reset()
				this._cb = cb
			}
		}
		this.tree.on('expandnode', function(node) {
					// console.log('exp', node)
					if (node.text == 'root') {
						// root expanded means the whole tree reloaded
						this.tree._loaded = false;
						Ext.each(node.childNodes, function(n) {
									n.expand(true, false)
								})
					}
				}, this)
		// check cascades
		this.tree.on('checkchange', function(node, checked) {
			if (!this._checkcascades) {
				if (this.isCate(node)) {
					this._checkcascades = true
					if (node.childNodes.length) {
						node.traverse(function(n) {
									if (node != n
											&& n.getUI().isChecked() != checked)
										n.getUI().toggleCheck(checked);
								})
					}
					delete this._checkcascades
				}
				if (checked == false) {
					var pnode = node;
					this._checkcascades = true
					while (pnode = pnode.parentNode) {
						if (pnode.getUI().isChecked())
							pnode.getUI().toggleCheck(false)
					}
					delete this._checkcascades
				}
				var root = this.tree.getRootNode();
				var vs = [];
				root.traverse(function(n) {
							if (root != n) {
								if (n.getUI().isChecked()) {
									var v = this.valueTransformer(n);
									if (v)
										vs.push(v)
									if (this.isCate(n)
											&& !this.addSubValueWhenCateSelected) {
										// ALL PERM,so just
										// ignore
										// the sub-ones
										return false;
									}
								}
							}
						}.dg(this));
				this.value = vs;
			}
		}, this)
	},
	_buildEvents : function() {

	},
	// data operation
	isCate : function(n) {
		return !n.attributes.attachment || !n.attributes.attachment.cateId
	},
	isPerm : function(n, virtual) {
		return !this.isCate(n)
				&& ((n.id.indexOf('.') != -1 ? 1 : 0) == (virtual ? 1 : 0))
	},
	getCategoryPerms : function(node) {
		var ret = []
		if (node.childNodes) {
			Ext.each(node.childNodes, function(n) {
						if (this.isPerm(n)) {
							ret.push(n)
						}
					}, this)
		}
		return ret
	},
	refreshNode : function() {
		var node = this.getSelectedNode();
		if (!node)
			this.refresh(this.getRootNode());
		else {
			this.refresh(node);
		}
	}

});
auth.GeneralPermTreeSelector = Ext.extend(auth.PermTreeSelector, {
			dataUrl : 'permission!list.action?generalCate=true',
			addSubValueWhenCateSelected : true,
			initList : function() {
				auth.GeneralPermTreeSelector.superclass.initList.call(this);
				this.tree.on('load', function(node) {
							this.fireEvent('allload')
						}, this)
			},
			initComponent : function() {
				auth.GeneralPermTreeSelector.superclass.initComponent
						.call(this)
			},
			valueTransformer : function(node) {
				if (this.isCate(node))
					return null
				return {
					id : node.id
				}
			}
		})