Ext.namespace('auth')
auth.RoleTreeSelector = Ext.extend(Ext.Panel, {
			layout : 'fit',
			initComponent : function() {
				var combo = new auth.RoleSelectTreeCombo();
				combo.initList();
				this.combo = combo
				this.items = combo.tree
				this.tree = combo.tree
				if (this.onValueChange)
					combo.onValueChange = this.onValueChange

				auth.RoleTreeSelector.superclass.initComponent.call(this)
			},
			getTree : function() {
				return this.tree
			},
			setValue : function(v) {
				this.combo.setValue(v)
			},
			getValue : function() {
				return this.combo.getValue()
			},
			reset : function() {
				this.combo.reset();
			}
		})
auth.RoleSelectTreeCombo = Ext.extend(TreeComboCheck, {
			border : false,
			autoScroll : true,
			dataUrl : 'role!listAllTree.action',
			data : {},
			setValue : function(v) {
				auth.RoleSelectTreeCombo.superclass.setValue.call(this, v);
				if (this.tree._loaded) {
					this.expandCheckedNode(this.tree)
				}
			},
			initComponent : function() {
				this.root = {
					id : '0',
					text : 'root',
					iconCls : 'icon-ontology-root'
				};
				this.treeConfig = {
					lines : true,
					border : false,
					tbar : new Ext.Toolbar({
								items : [{
											text : '清空',
											iconCls : 'icon-refresh',
											handler : function() {
												this.reset()
											}.dg(this)
										}, {
											text : '刷新',
											iconCls : 'icon-refresh',
											handler : function() {
												this.tree.root.reload();
											}.dg(this)
										}]
							})
				}
				auth.RoleSelectTreeCombo.superclass.initComponent.call(this);
				this._buildEvents();
			},
			_buildTbar : function() {
			},
			// @override
			valueTransformer : function(node) {
				var d = {
					id : node.id
				}
				return d
			},
			initList : function() {
				auth.RoleSelectTreeCombo.superclass.initList.call(this);
				this.tree.on('expandnode', function(node) {
							if (this.tree.getRootNode() == node) {
								this.tree._loaded = true;
								this.expandCheckedNode(this.tree)
							}
						}, this)
			},
			_buildEvents : function() {

			}
		});
