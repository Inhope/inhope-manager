Ext.namespace('auth')
auth.OperationTree = Ext.extend(Ext.tree.TreePanel, {
	rootVisible : false,
	enableDD : true,
	lines : false,
	border : false,
	autoScroll : true,
	dataUrl : 'permission!list.action?generalCate=true',
	data : {},
	initComponent : function() {
		this.root = {
			id : '0',
			text : 'root',
			iconCls : 'icon-ontology-root'
		};
		this._buildFormWinPerm();
		this._buildFormWinCate();
		this._buildContextMenu();
		this.tbar = this._buildTbar()
		auth.OperationTree.superclass.initComponent.call(this);
		this._buildEvents();
	},
	_buildTbar : function() {
		return new Ext.Toolbar({
					enableOverflow : true,
					height : '20',
					items : [{
								text : '新建权限组',
								iconCls : 'icon-wordclass-root-add',
								width : 50,
								handler : this.addCate.dg(this, [])
							}, '-', {
								text : '刷新',
								iconCls : 'icon-refresh',
								width : 50,
								handler : this.refresh.dg(this, [])
							}]

				})

	},
	_buildContextMenu : function() {
		var categoryMenu = new Ext.menu.Menu({
					items : [{
								text : '刷新',
								iconCls : 'icon-refresh',
								width : 50,
								handler : this.refreshNode.dg(this, [])
							}, {
								text : '删除权限组',
								iconCls : 'icon-ontology-cate-del',
								width : 50,
								handler : function() {
									this.deleteNode(categoryMenu.currentNode);
								}.dg(this)
							}, {
								text : '修改权限组',
								iconCls : 'icon-ontology-cate-edit',
								width : 50,
								handler : function() {
									this.modifyCate(categoryMenu.currentNode);
								}.dg(this)
							}, '-', {
								text : '新建操作',
								iconCls : 'icon-ontology-object-add',
								width : 50,
								handler : function() {
									this.addPerm(categoryMenu.currentNode)
								}.dg(this)
							}]
				});
		var permMenu = new Ext.menu.Menu({
					items : [{
								text : '修改操作',
								iconCls : 'icon-ontology-cate-edit',
								width : 50,
								handler : function() {
									this.modifyPerm(permMenu.currentNode);
								}.dg(this)
							}, {
								text : '删除操作',
								iconCls : 'icon-ontology-object-del',
								width : 50,
								handler : this._deletePermission.dg(this)
							}]
				});

		this.on('contextmenu', function(node, event) {
					event.preventDefault();
					node.select();
					if (node.attributes.attachment.cateId) {
						permMenu.currentNode = node;
						permMenu.showAt(event.getXY());
					} else {
						categoryMenu.currentNode = node;
						categoryMenu.showAt(event.getXY());
					}
				});
	},
	_buildEvents : function() {
		this.on('click', function(n) {
				}, this)
		this.on('load', function(node, refNode) {
					if (node.childNodes) {
						Ext.each(node.childNodes, function(n) {
									n.attributes.draggable = true
									n.draggable = true
								})
					}
				});
		this.on('beforenodedrop', function(e) {
					var node = e.dropNode;
					var target = e.target.attributes.objectId
							? e.target.parentNode
							: e.target;
					var source = node.parentNode

					var c = Ext.Msg.confirm('提示', '确定要移动到&nbsp&nbsp'
									+ target.getPath('text').replace('/root',
											'') + '&nbsp&nbsp吗', function(btn,
									text) {
								if (btn == 'yes') {
									Ext.Ajax.request({
												url : 'ontology-object!move.action',
												params : {
													objectId : node.id,
													categoryId : node.parentNode.id
												},
												success : function(form, action) {
													Dashboard.setAlert('移动成功')
												},
												failure : function(response) {
													source.appendChild(node);
													source.expand();
													Ext.Msg
															.alert(
																	"错误",
																	response.responseText);
												}
											});
								} else {
									source.appendChild(node);
									source.expand();
								}
							});
				});
	},
	// data operation
	_deletePermission : function() {
		var selNode = this.getSelectedNode();
		if (!selNode) {
			Ext.Msg.alert('提示', '请先选择需要删除的节点');
			return;
		}
		Ext.Msg.confirm('确定框', '确定删除记录' + selNode.text + '吗?',
				function(result) {
					if (result == 'yes') {
						Ext.Ajax.request({
									url : 'permission!deletePermission.action',
									params : {
										"id" : selNode.id
									},
									success : function(response) {
										var result = Ext.util.JSON
												.decode(response.responseText);
										Dashboard.setAlert(result.message);
										selNode.parentNode.removeChild(selNode,
												true);
									},
									failure : function(response) {
										Ext.Msg.alert("错误",
												response.responseText);
									}
								});
					}
				});
	},
	updateAttr : function(data, cateId, tabId) {
		var node = this.getNodeById(data.id);
		if (node) {
			node.setText(data.text);
		} else {
			var parentNode = this.getNodeById(cateId);
			node = new Ext.tree.TreeNode(data);
			if (tabId)
				node.tabId = tabId;
			parentNode.appendChild(node);
		}
		this.showTab(obj.detail.MainPanel, (node.tabId
						? node.tabId
						: 'object-tab-' + node.id), node.text,
				'icon-ontology-object', true);
	},
	showObjectTab : function(n, dimValueId) {
		var objectPanel = this.showTab(obj.detail.MainPanel, (n.tabId
						? n.tabId
						: 'object-tab-' + n.id), n.text,
				'icon-ontology-object', true);
		objectPanel.objectChanged(new obj.detail.ObjectType({
							objectId : n.id ? n.id : null
						}), null, dimValueId)
	},
	saveCate : function(cb) {
		this.saveNode0(cb, this.formWinCate, 'permission!saveCategory.action')
	},
	savePerm : function(cb) {
		this
				.saveNode0(cb, this.formWinPerm,
						'permission!savePermission.action')
	},
	saveNode0 : function(cb, formWin, url) {
		if (!formWin.fmP.getForm().isValid()) {
			Dashboard.setAlert('验证不通过，请检查您填写的数据', 'error')
			cb(false)
			return;
		}
		var values = formWin.fmP.getForm().getFieldValues();
		var self = this;
		Ext.Ajax.request({
					url : url,
					params : {
						data : Ext.encode(ux.util.resetEmptyString(values))
					},
					success : function(form, action) {
						Dashboard.setAlert("保存成功!");
						self.updateNode(Ext.decode(form.responseText), formWin
										.getParentId());
						cb(true)
					},
					failure : function(response) {
						Ext.Msg.alert("错误", response.responseText);
						cb(false)
					}
				})
	},

	deleteNode : function(node) {
		var self = this;
		if (node.isLoaded && !node.isLoaded() || node.childNodes.length) {
			Ext.Msg.alert('警告', '只能删除叶子权限组！');
			return;
		}
		Ext.Msg.confirm('提示', '确定要删除&nbsp&nbsp'
						+ node.getPath('text').replace('/root', '')
						+ '&nbsp&nbsp吗', function(btn, text) {
					if (btn == 'yes') {
						Ext.Ajax.request({
									url : 'permission!deleteCategory.action',
									params : {
										id : node.id
									},
									success : function(form, action) {
										node.parentNode.removeChild(node);
									},
									failure : function(response) {
										Ext.Msg.alert("错误",
												response.responseText);
									}
								});
					}
				});
	},
	// category
	_buildFormWinCate : function() {
		var cateRecordType = Ext.data.Record.create(['id', 'name', 'descName',
				'parentId', 'bh'])
		var closeEditWin = function() {
			formWinCate.hide();
			formWinCate.fmP.getForm().reset();
		}
		var _formPanel = new Ext.form.FormPanel({
					ref : 'fmP',
					frame : false,
					border : false,
					labelWidth : 75,
					height : 150,
					waitMsgTarget : true,
					bodyStyle : 'padding : 5px',
					defaults : {
						blankText : '',
						invalidText : '',
						anchor : '100%',
						xtype : 'textfield'
					},
					items : [{
								name : 'id',
								hidden : true
							}, {
								name : 'parentId',
								hidden : true
							}, {
								name : 'bh',
								hidden : true
							}, {
								fieldLabel : '权限组名称',
								name : 'descName',
								allowBlank : false
							}, {
								fieldLabel : '权限组代号',
								name : 'name'
							}, {
								name : 'type',
								value : 1,
								hidden : true
							}],
					buttons : [{
								text : '保存',
								handler : function(b) {
									b.disable()
									this.saveCate(function(success) {
												b.enable()
												if (success)
													closeEditWin();
											}, this.formWinCate)
								}.dg(this)
							}, {
								text : '关闭',
								handler : closeEditWin.dg(this)
							}]
				});
		var formWinCate = this.formWinCate = new Ext.Window({
					width : 495,
					title : '类型管理',
					defaults : {
						border : false
					},
					modal : true,
					plain : true,
					shim : true,
					closable : false,
					closeAction : 'hide',
					collapsible : true,
					resizable : false,
					draggable : true,
					animCollapse : true,
					constrainHeader : true,
					items : [_formPanel],
					// custom
					loadData : function(d) {
						_formPanel.getForm().loadRecord(new cateRecordType(d))
					},
					getParentId : function() {
						return _formPanel.getForm().getFieldValues()['parentId'];
					}
				});
	},
	addCate : function(node) {
		node = node || this.getRootNode();
		this.modifyCate0({}, {
					id : node.id,
					path : node.getPath('text').replace('/root', '')
				})
	},
	modifyCate : function(node) {
		var d = node.attributes.attachment;
		this.modifyCate0(d, {
					id : node.parentNode.id,
					path : node.parentNode.getPath('text').replace('/root', '')
				})
	},
	modifyCate0 : function(d, parentNodeData) {
		this.formWinCate.show();
		if (!parentNodeData.path) {
			parentNodeData.path = '/';
		}
		d.parentId = parentNodeData.id == 0 ? null : parentNodeData.id
		this.formWinCate.loadData(d);
	},
	refresh : function(node) {
		node = node || this.getRootNode();
		if (node.isExpanded())
			node.reload();
		else
			node.expand();

	},
	// permission
	_buildFormWinPerm : function() {
		var permRecordType = Ext.data.Record.create(['id', 'name', 'descName',
				'refId', 'cateId'])
		var closeEditWin = function() {
			formWinPerm.hide();
			_formPanel.getForm().reset();
		}
		var _formPanel = new Ext.form.FormPanel({
					ref : 'fmP',
					frame : false,
					border : false,
					labelWidth : 75,
					height : 200,
					waitMsgTarget : true,
					bodyStyle : 'padding : 5px',
					defaults : {
						blankText : '',
						invalidText : '',
						anchor : '100%',
						xtype : 'textfield'
					},
					items : [{
								name : 'id',
								hidden : true
							}, {
								name : 'cateId',
								hidden : true
							}, {
								fieldLabel : '操作名称',
								name : 'descName',
								allowBlank : false
							}, {
								fieldLabel : '操作代号',
								name : 'name'
							}, {
								fieldLabel : '引用ID',
								name : 'refId'
							}],
					buttons : [{
								text : '保存',
								handler : function(b) {
									b.disable()
									this.savePerm(function(success) {
												b.enable()
												if (success)
													closeEditWin();
											})
								}.dg(this)
							}, {
								text : '关闭',
								handler : closeEditWin.dg(this)
							}]
				});
		var formWinPerm = this.formWinPerm = new Ext.Window({
					width : 495,
					title : '操作管理',
					defaults : {
						border : false
					},
					modal : true,
					plain : true,
					shim : true,
					closable : false,
					closeAction : 'hide',
					collapsible : true,
					resizable : false,
					draggable : true,
					animCollapse : true,
					constrainHeader : true,
					items : [_formPanel],
					// custom
					loadData : function(d) {
						_formPanel.getForm().loadRecord(new permRecordType(d))
					},
					getParentId : function() {
						return _formPanel.getForm().getFieldValues()['cateId'];
					}
				});
	},
	addPerm : function(node) {
		this.modifyPerm0({}, {
					id : node.id,
					path : node.getPath('text').replace('/root', '')
				})
	},
	modifyPerm : function(node) {
		var d = node.attributes.attachment;
		this.modifyPerm0(d, {
					id : node.parentNode.id,
					path : node.parentNode.getPath('text').replace('/root', '')
				})
	},
	modifyPerm0 : function(d, parentNodeData) {
		this.formWinPerm.show();
		if (!parentNodeData.path) {
			parentNodeData.path = '/';
		}
		if (!d.cateId)
			d.cateId = parentNodeData.id == '0' ? null : parentNodeData.id
		this.formWinPerm.loadData(d)
	},
	// miscs
	getSelectedNode : function() {
		return this.getSelectionModel().getSelectedNode();
	},
	updateNode : function(data, parentId) {
		var parentCateId = parentId;
		var parentNode;
		if (!parentCateId) {
			parentNode = this.getRootNode();
		} else
			parentNode = this.getNodeById(parentCateId);
		if (!data.saveOrUpdate) {
			var selNode = this.getSelectedNode();
			selNode.setText(data.text);
			Ext.apply(selNode.attributes.attachment, data.attachment);
			if (selNode.parentNode != parentNode) {
				if (parentNode) {
					selNode.leaf = data.leaf;
					this.setNodeIconCls(selNode, data.iconCls);
					parentNode.appendChild(selNode);
				} else
					selNode.parentNode.removeChild(selNode);
			}
		} else {
			if (parentNode) {
				var node = new Ext.tree.TreeNode({
							id : data.id,
							iconCls : data.iconCls,
							leaf : true,
							text : data.text,
							attachment : data.attachment
						});
				if (parentNode.leaf)
					parentNode.leaf = false
				parentNode.appendChild(node);
				parentNode.expand();
				node.select();
			}
		}
	},
	setNodeIconCls : function(node, clsName) {
		var iel = node.getUI().getIconEl();
		if (iel) {
			var el = Ext.get(iel);
			if (el) {
				if (clsName.indexOf('-root') > 0)
					el.removeClass('icon-wordclass-category');
				else
					el.removeClass('icon-wordclass-root');
				el.addClass(clsName);
			}
		}
	},
	expandNode : function(node, bh) {
		var self = this;
		if (node.attributes.bh == bh || node.leaf) {
			this.getSelectionModel().select(node);
			return;
		}
		node.expand(false, true, function() {
					var childs = node.childNodes;
					for (var i = 0; i < childs.length; i++) {
						if (bh.indexOf(childs[i].attributes.bh) != -1)
							self.expandNode(childs[i], bh);
					}
				});
	},
	refreshNode : function() {
		var node = this.getSelectedNode();
		if (!node)
			this.refresh(this.getRootNode());
		else {
			this.refresh(node);
		}
	}

});