Ext.namespace('auth')
auth.PermissionTree = Ext.extend(Ext.ux.tree.TreeGrid, {
	rootVisible : false,
	lines : false,
	border : false,
	autoScroll : true,
	dataUrl : 'permission!listAllTree.action',
	data : {},
	enableSort : false,// treegrid
	getParentNamePath : function(attrs) {
		var path = '';
		var n = this.getNodeById(attrs.id);
		var rn = this.getRootNode();
		do {
			path = n.attributes.attachment.name + '.' + path
		} while ((n = n.parentNode) && n != rn);
		return path.slice(0, -1);
	},
	initComponent : function() {
		this.columns = [{
					header : '资源树',
					dataIndex : 'text',
					width : 350
				}, {
					header : '授权路径',
					width : 300,
					tpl : new Ext.XTemplate('{.:this.renderer}', {
						renderer : function(v) {
							return '<div unselectable="off" style="-moz-user-select:all;" >'
									+ this.getParentNamePath(v) + '</div>'
						}.dg(this)
					})

				}]
		this.root = new Ext.tree.AsyncTreeNode({
					id : '0',
					text : 'root',
					iconCls : 'icon-ontology-root'
				});
		this._buildContextMenu();
		this.tbar = this._buildTbar()
		auth.PermissionTree.superclass.initComponent.call(this);
		this._buildEvents();
	},
	_buildTbar : function() {
		return new Ext.Toolbar({
					enableOverflow : true,
					height : '20',
					items : [{
								text : '新建',
								iconCls : 'icon-wordclass-root-add',
								width : 50,
								handler : this.addCate.dg(this, [])
							}, '-', {
								text : '刷新',
								iconCls : 'icon-refresh',
								width : 50,
								handler : this.refresh.dg(this, [])
							}, '-', {
								text : '展开所有节点',
								iconCls : 'icon-status-add',
								handler : function() {
									this.expandAll();
								}.dg(this)
							}, {
								text : '折叠所有节点',
								iconCls : 'icon-status-cancel',
								handler : function() {
									this.getRootNode().collapse(true);
								}.dg(this)
							}
					// , '-', {
					// ref : 'searchField',
					// xtype : 'textfield',
					// width : 100
					// }, {
					// xtype : 'button',
					// text : '搜索',
					// iconCls : 'icon-search',
					// handler : function() {
					// var t = this.getTopToolbar().searchField.getValue();
					// if (t) {
					// this.expandAll(function() {
					// }, this);
					// }
					// }.dg(this)
					// }
					]

				})

	},
	_buildContextMenu : function() {
		var categoryMenu = new Ext.menu.Menu({
					items : [{
								text : '刷新',
								iconCls : 'icon-refresh',
								width : 50,
								handler : this.refreshNode.dg(this, [])
							}, {
								text : '新建子资源',
								iconCls : 'icon-ontology-cate-add',
								width : 50,
								handler : function() {
									this.addCate(categoryMenu.currentNode);
								}.dg(this)
							}, {
								text : '删除资源',
								iconCls : 'icon-ontology-cate-del',
								width : 50,
								handler : function() {
									this.deleteCate(categoryMenu.currentNode);
								}.dg(this)
							}, {
								text : '修改资源',
								iconCls : 'icon-ontology-cate-edit',
								width : 50,
								handler : function() {
									this.modifyCate(categoryMenu.currentNode);
								}.dg(this)
							}, '-', {
								text : '新建自定义权限',
								iconCls : 'icon-ontology-object-add',
								width : 50,
								handler : function() {
									this.addPerm(categoryMenu.currentNode)
								}.dg(this)
							}]
				});
		var permMenu = new Ext.menu.Menu({
					items : [{
								text : '修改自定义权限',
								iconCls : 'icon-ontology-cate-edit',
								width : 50,
								handler : function() {
									this.modifyPerm(permMenu.currentNode);
								}.dg(this)
							}, {
								text : '删除自定义权限',
								iconCls : 'icon-ontology-object-del',
								width : 50,
								handler : this._deletePermission.dg(this)
							}]
				});

		this.on('contextmenu', function(node, event) {
					event.preventDefault();
					node.select();
					if (node.attributes.attachment.cateId) {// perm
						if (node.attributes.id.indexOf('.') == -1) {
							permMenu.currentNode = node;
							permMenu.showAt(event.getXY());
						}
					} else {// cate
						categoryMenu.currentNode = node;
						categoryMenu.showAt(event.getXY());
					}
				});
	},
	_buildEvents : function() {
		this.on('click', function(n) {
				}, this)
		this.on('load', function(node, refNode) {
					if (node == this.getRootNode()) {
						if (this.lastSelectedNodePath) {
							this.expandPath(this.lastSelectedNodePath, '',
									function(suc, node) {
										node.select();
									});
							delete this.lastSelectedNodePath
						}
					}
				});
		this.on('click', function(node, e) {
					this.lastSelectedNodePath = node.getPath();
				})
	},
	// data operation
	_deletePermission : function() {
		var selNode = this.getSelectedNode();
		if (!selNode) {
			Ext.Msg.alert('提示', '请先选择需要删除的节点');
			return;
		}
		Ext.Msg.confirm('确定框', '确定删除记录' + selNode.text + '吗?',
				function(result) {
					if (result == 'yes') {
						Ext.Ajax.request({
									url : 'permission!deletePermission.action',
									params : {
										"id" : selNode.id
									},
									success : function(response) {
										var result = Ext.util.JSON
												.decode(response.responseText);
										Dashboard.setAlert('删除成功')
										selNode.parentNode.removeChild(selNode,
												true);
									},
									failure : function(response) {
										Ext.Msg.alert("错误",
												response.responseText);
									}
								});
					}
				});
	},
	updateAttr : function(data, cateId, tabId) {
		var node = this.getNodeById(data.id);
		if (node) {
			node.setText(data.text);
		} else {
			var parentNode = this.getNodeById(cateId);
			node = new Ext.tree.TreeNode(data);
			if (tabId)
				node.tabId = tabId;
			parentNode.appendChild(node);
		}
		this.showTab(obj.detail.MainPanel, (node.tabId
						? node.tabId
						: 'object-tab-' + node.id), node.text,
				'icon-ontology-object', true);
	},
	showObjectTab : function(n, dimValueId) {
		var objectPanel = this.showTab(obj.detail.MainPanel, (n.tabId
						? n.tabId
						: 'object-tab-' + n.id), n.text,
				'icon-ontology-object', true);
		objectPanel.objectChanged(new obj.detail.ObjectType({
							objectId : n.id ? n.id : null
						}), null, dimValueId)
	},
	saveCate : function(cb) {
		this.saveNode0(cb, this.getFormWinCate(),
				'permission!saveCategory.action')
	},
	savePerm : function(cb) {
		this.saveNode0(cb, this.getFormWinPerm(),
				'permission!savePermission.action')
	},
	saveNode0 : function(cb, formWin, url) {
		if (!formWin.fmP.getForm().isValid()) {
			Dashboard.setAlert('验证不通过，请检查您填写的数据', 'error')
			cb(false)
			return;
		}
		var values = formWin.fmP.getForm().getFieldValues();
		var self = this;
		Ext.Ajax.request({
					url : url,
					params : {
						data : Ext.encode(ux.util.resetEmptyString(values))
					},
					success : function(form, action) {
						Dashboard.setAlert("保存成功!");
						self.updateNode(Ext.decode(form.responseText), formWin
										.getParentId());
						cb(true)
					},
					failure : function(response) {
						Ext.Msg.alert("错误", response.responseText);
						cb(false)
					}
				})
	},

	deleteCate : function(node, force) {
		var self = this;
		Ext.Msg.confirm('提示', '确定要删除&nbsp&nbsp'
						+ node.getPath('text').replace('/root', '')
						+ '&nbsp&nbsp吗', function(btn, text) {
					if (btn == 'yes') {
						if (force) {
							Ext.Ajax.request({
										url : 'permission!deleteCategory.action',
										params : {
											id : node.id
										},
										success : function(form, action) {
											node.parentNode.removeChild(node);
										},
										failure : function(response) {
											Ext.Msg.alert("错误",
													response.responseText);
										}
									});
						} else {
							this.deleteCate(node, true)
						}
					}
				}, this);
	},
	// category
	getFormWinCate : function() {
		if (!this.formWinCate)
			this._buildFormWinCate();
		return this.formWinCate;
	},
	_buildFormWinCate : function() {
		var cateRecordType = Ext.data.Record.create(['id', 'name', 'descName',
				'parentId', 'bh', 'generalPerms'])
		var closeEditWin = function() {
			formWinCate.hide();
			formWinCate.fmP.getForm().reset();
		}
		var _formPanel = new Ext.form.FormPanel({
			ref : 'fmP',
			frame : false,
			border : false,
			labelWidth : 75,
			waitMsgTarget : true,
			bodyStyle : 'padding : 5px',
			defaults : {
				blankText : '',
				invalidText : '',
				anchor : '100%',
				xtype : 'textfield'
			},
			items : [{
						name : 'id',
						hidden : true
					}, {
						name : 'parentId',
						hidden : true
					}, {
						name : 'bh',
						hidden : true
					}, {
						fieldLabel : '资源名称',
						name : 'descName',
						allowBlank : false
					}, {
						fieldLabel : '资源代号',
						name : 'name'
					}, {
						name : 'generalPerms',
						hidden : true,
						setValue : function(v) {
							_formPanel.generalPermsTree.setValue(v)
						}.dg(this),
						getValue : function() {
							return _formPanel.generalPermsTree.getValue()
						}.dg(this),
						reset : function() {
							_formPanel.generalPermsTree.reset()
						}.dg(this)
					}, {
						xtype : 'fieldset',
						style : 'padding:2px',
						border : false,
						labelAlign : 'top',
						layout : 'fit',
						fieldLabel : '默认权限',
						anchor : '100% -50',
						items : new auth.PermTreeField({
									ref : '../generalPermsTree',
									treecomboClass : auth.GeneralPermTreeSelector
								})
					}],
			buttons : [{
						text : '保存',
						handler : function(b) {
							b.disable()
							this.saveCate(function(success) {
										b.enable()
										if (success)
											closeEditWin();
									}, this.formWinCate)
						}.dg(this)
					}, {
						text : '关闭',
						handler : closeEditWin.dg(this)
					}]
		});
		var formWinCate = this.formWinCate = new Ext.Window({
					width : 495,
					height : 300,
					layout : 'fit',
					title : '资源编辑界面',
					defaults : {
						border : false
					},
					modal : true,
					plain : true,
					shim : true,
					closable : false,
					closeAction : 'hide',
					collapsible : true,
					animCollapse : true,
					constrainHeader : true,
					items : [_formPanel],
					// custom
					loadData : function(d) {
						_formPanel.getForm().loadRecord(new cateRecordType(d))
					},
					getParentId : function() {
						return _formPanel.getForm().getFieldValues()['parentId'];
					}
				});
	},
	addCate : function(node) {
		node = node || this.getRootNode();
		this.modifyCate0({}, {
					id : node.id,
					path : node.getPath('text').replace('/root', '')
				})
	},
	modifyCate : function(node) {
		var d = node.attributes.attachment;
		this.modifyCate0(d, {
					id : node.parentNode.id,
					path : node.parentNode.getPath('text').replace('/root', '')
				})
	},
	modifyCate0 : function(d, parentNodeData) {
		this.getFormWinCate().show();
		if (!parentNodeData.path) {
			parentNodeData.path = '/';
		}
		d.parentId = parentNodeData.id == 0 ? null : parentNodeData.id
		this.getFormWinCate().loadData(d);
	},
	refresh : function(node) {
		node = node || this.getRootNode();
		if (node.isExpanded())
			node.reload();
		else
			node.expand();

	},
	// permission
	getFormWinPerm : function() {
		if (!this.formWinPerm)
			this._buildFormWinPerm();
		return this.formWinPerm;
	},
	_buildFormWinPerm : function() {
		var permRecordType = Ext.data.Record.create(['id', 'name', 'descName',
				'refId', 'cateId'])
		var closeEditWin = function() {
			formWinPerm.hide();
			_formPanel.getForm().reset();
		}
		var _formPanel = new Ext.form.FormPanel({
					ref : 'fmP',
					frame : false,
					border : false,
					labelWidth : 75,
					waitMsgTarget : true,
					bodyStyle : 'padding : 5px',
					defaults : {
						blankText : '',
						invalidText : '',
						anchor : '100%',
						xtype : 'textfield'
					},
					items : [{
								name : 'id',
								hidden : true
							}, {
								name : 'cateId',
								hidden : true
							}, {
								fieldLabel : '权限名称',
								name : 'descName',
								allowBlank : false
							}, {
								fieldLabel : '权限代号',
								name : 'name'
							}],
					buttons : [{
								text : '保存',
								handler : function(b) {
									b.disable()
									this.savePerm(function(success) {
												b.enable()
												if (success)
													closeEditWin();
											})
								}.dg(this)
							}, {
								text : '关闭',
								handler : closeEditWin.dg(this)
							}]
				});
		var formWinPerm = this.formWinPerm = new Ext.Window({
					width : 495,
					title : '权限编辑界面',
					height : 300,
					layout : 'fit',
					defaults : {
						border : false
					},
					modal : true,
					plain : true,
					shim : true,
					closable : false,
					closeAction : 'hide',
					collapsible : true,
					animCollapse : true,
					constrainHeader : true,
					items : [_formPanel],
					// custom
					loadData : function(d) {
						_formPanel.getForm().loadRecord(new permRecordType(d))
					},
					getParentId : function() {
						return _formPanel.getForm().getFieldValues()['cateId'];
					}
				});
	},
	addPerm : function(node) {
		this.modifyPerm0({}, {
					id : node.id,
					path : node.getPath('text').replace('/root', '')
				})
	},
	modifyPerm : function(node) {
		var d = node.attributes.attachment;
		this.modifyPerm0(d, {
					id : node.parentNode.id,
					path : node.parentNode.getPath('text').replace('/root', '')
				})
	},
	modifyPerm0 : function(d, parentNodeData) {
		this.getFormWinPerm().show();
		if (!parentNodeData.path) {
			parentNodeData.path = '/';
		}
		if (!d.cateId)
			d.cateId = parentNodeData.id == '0' ? null : parentNodeData.id
		this.getFormWinPerm().loadData(d)
	},
	// miscs
	getSelectedNode : function() {
		return this.getSelectionModel().getSelectedNode();
	},
	updateNode : function(data, parentId) {
		var parentCateId = parentId;
		var parentNode;
		if (!parentCateId) {
			parentNode = this.getRootNode();
		} else
			parentNode = this.getNodeById(parentCateId);
		if (!data.saveOrUpdate) {
			var selNode = this.getSelectedNode();
			selNode.setText(data.text);
			Ext.apply(selNode.attributes.attachment, data.attachment);
			if (selNode.parentNode != parentNode) {
				if (parentNode) {
					selNode.leaf = data.leaf;
					this.setNodeIconCls(selNode, data.iconCls);
					parentNode.appendChild(selNode);
				} else
					selNode.parentNode.removeChild(selNode);
			}
		} else {
			if (parentNode) {
				var node = new Ext.tree.TreeNode({
							id : data.id,
							iconCls : data.iconCls,
							leaf : true,
							text : data.text,
							attachment : data.attachment
						});
				if (parentNode.leaf)
					parentNode.leaf = false
				parentNode.appendChild(node);
				parentNode.expand();
				node.select();
			}
		}
	},
	setNodeIconCls : function(node, clsName) {
		var iel = node.getUI().getIconEl();
		if (iel) {
			var el = Ext.get(iel);
			if (el) {
				if (clsName.indexOf('-root') > 0)
					el.removeClass('icon-wordclass-category');
				else
					el.removeClass('icon-wordclass-root');
				el.addClass(clsName);
			}
		}
	},
	expandNode : function(node, bh) {
		var self = this;
		if (node.attributes.bh == bh || node.leaf) {
			this.getSelectionModel().select(node);
			return;
		}
		node.expand(false, true, function() {
					var childs = node.childNodes;
					for (var i = 0; i < childs.length; i++) {
						if (bh.indexOf(childs[i].attributes.bh) != -1)
							self.expandNode(childs[i], bh);
					}
				});
	},
	refreshNode : function() {
		var node = this.getSelectedNode();
		if (!node)
			this.refresh(this.getRootNode());
		else {
			this.refresh(node);
		}
	}

});