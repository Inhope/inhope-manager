Ext.namespace('auth.role')
auth.role.EditForm = Ext.extend(Ext.FormPanel, {
			initComponent : function() {
				this.items = [{
							name : 'id',
							anchor : '100%',
							hidden : true
						}, {
							name : 'name',
							fieldLabel : '角色名称',
							xtype : 'textfield',
							anchor : '100%',
							allowBlank : false,
							blankText : '角色名称是必填项，请填写后再保存'
						}, {
							name : 'descContent',
							fieldLabel : '描述内容',
							xtype : 'textarea',
							anchor : '100%'
						}, {
							name : 'rolePermissions',
							hidden : true,
							setValue : function(v) {
								this.rolePermissionTree.setValue(v)
							}.dg(this),
							getValue : function() {
								return this.rolePermissionTree.getValue()
							}.dg(this),
							reset : function() {
								this.rolePermissionTree.reset()
							}.dg(this)
						}, {
							xtype : 'fieldset',
							style : 'padding:2px',
							border : false,
							labelAlign : 'top',
							layout : 'fit',
							fieldLabel : '权限',
							anchor : '100% -90',
							items : new auth.PermTreeField({
										ref : '../rolePermissionTree'
									})
						}]
				this.tbar = [{
							text : '保存',
							iconCls : 'icon-table-save',
							width : 50,
							handler : function(btn) {
								btn.disable()
								this.saveData(function() {
											btn.enable()
										})
							}.dg(this)
						}]
				auth.role.EditForm.superclass.initComponent.call(this);
			},
			loadData : function(r) {
				this.getForm().reset()
				if (r) {
					var rps = r.get('rolePermissions');
					Ext.each(rps, function(rp) {
								var id = '';
								if (rp.cateId)
									id += rp.cateId
								if (rp.permId) {
									id += (id ? '.' : '') + rp.permId;
								}
								rp.id = id
							})
					this.getForm().loadRecord(r);
				}
			},
			saveData : function(cb) {
				if (!this.getForm().isValid()) {
					Ext.Msg.alert('消息', '表单有必填项未填写，请重填后再提交')
					cb()
					return;
				}
				var d = this.getForm().getFieldValues();
				if (d.rolePermissions && d.rolePermissions.length){
					Ext.each(d.rolePermissions,function(rp){
						if (rp.id) delete rp.id
					})
				}
				this.getEl().mask('保存...')
				Ext.Ajax.request({
							url : 'role!save.action',
							params : {
								"data" : Ext
										.encode(ux.util.resetEmptyString(d))
							},
							success : function(response) {
								this.getEl().unmask()
								if (cb)
									cb(true)
								var r = new (auth.role.Type.getRecordType())(Ext
										.decode(response.responseText).data)
								this.loadData(r)
								this.fireEvent('dataSaved', r);
								Dashboard.setAlert("保存成功!");
							},
							failure : function(response) {
								this.getEl().unmask()
								Ext.Msg.alert("错误", response.responseText);
								if (cb)
									cb(false)
							},
							scope : this
						});
			},
			collapseMode : 'mini',
			margins : '0',
			border : true,
			bodyStyle : 'background-color:' + sys_bgcolor + ' padding : 3px 10px;',
			labelWidth : 55,
			defaults : {
				xtype : 'textfield'
			}
		})
auth.role.EditWin = Ext.extend(Ext.Window, {
			/**
			 * @type {auth.role.EditForm}
			 */
			fmP : null,
			initComponent : function() {
				this.fmP = new auth.role.EditForm();
				this.items = [this.fmP];
				auth.role.EditWin.superclass.initComponent.call(this)
				this.relayEvents(this.fmP, ['dataSaved']);
			},
			loadData : function(r) {
				this.fmP.loadData(r);
			},
			closeAction : 'hide',
			width : 500,
			height : 400,
			plain : true,
			maximizable : true,
			collapsible : true,
			layout : 'fit',
			border : false
		})
