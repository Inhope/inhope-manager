Ext.namespace('auth.role')
auth.role.Type = {
	fields : ['id', 'name', 'descContent', 'rolePermissions'],
	emptyRecord : function() {
		var d = {}
		Ext.each(this.fields, function(f) {
					d[f] = ''
				});
		return new (this.getRecordType())(d);
	},
	getRecordType : function() {
		if (!this.rt)
			this.rt = Ext.data.Record.create(this.fields);
		return this.rt;
	},
	createStore : function(cfg) {
		cfg = cfg || {}
		return new Ext.data.JsonStore(Ext.apply({
					remoteSort : false,
					idProperty : 'id',
					fields : this.getRecordType()
				}, cfg));
	}
}
auth.role.ListPanel = Ext.extend(Ext.grid.GridPanel, {
			border : false,
			initComponent : function() {
				this.store = auth.role.Type.createStore({
							url : 'role!list.action?fetchPermission=true',
							root : 'data',
							autoLoad : true
						})
				this.columns = [{
							header : '角色名',
							dataIndex : 'name',
							width : 150
						}, {
							header : '描述',
							dataIndex : 'descContent',
							width : 150
						}]
				this.selModel = new Ext.grid.RowSelectionModel();

				this.tbar = [[{
							text : '添加角色',
							iconCls : 'icon-add',
							handler : this.addRole.dg(this, [])
						}, '', {
							text : '修改角色',
							iconCls : 'icon-wordclass-edit',
							handler : this.modifyRole.dg(this, [])
						}, '', {
							text : '删除角色',
							iconCls : 'icon-delete',
							handler : this.deleteRole.dg(this, [])
						}, '', {
							text : '刷新',
							iconCls : 'icon-refresh',
							handler : this.reloadRoles.dg(this, [])
						}]]

				auth.role.ListPanel.superclass.initComponent.call(this)
				this.on('rowclick', function() {
							this.modifyRole()
						})
				// TODO:debug
				this.getView().on('refresh', function() {
							this.getSelectionModel().selectRow(0);
							this.fireEvent('rowclick', [this, 0]);
						}, this)
			},
			// data operation
			deleteRole : function() {
				if (!this.getSelectedR()) {
					Ext.Msg.alert('提示', '请先选中需要删除的行！');
					return;
				}
				var r = this.getSelectedR()
				Ext.Msg.confirm('提示', '确定要删除 ' + r.get('name') + ' 吗',
						function(btn, text) {
							if (btn == 'yes') {
								Ext.Ajax.request({
											url : 'role!delete.action',
											params : {
												id : r.get('id')
											},
											success : function(form, action) {
												Dashboard.setAlert('删除成功')
												this.reloadRoles();
											},
											failure : function(response) {
												Ext.Msg.alert("错误",
														response.responseText);
											},
											scope : this
										});
							}
						}, this);
			},
			editRole : function(r) {
				if (!this.edForm._init) {
					this.edForm._init = true;
					this.edForm.on('dataSaved', function(r) {
								var pos = this.getStore().find('id',
										r.get('id'))
								if (pos != -1) {
									var oldR = this.getStore().getAt(pos)
									oldR.beginEdit();
									for (var key in r.data)
										oldR.set(key, r.data[key]);
									oldR.endEdit();
									oldR.commit()
								} else {
									this.getStore().add(r.copy())
								}
							}, this)
				}
				var r = r.copy();
				var rps = []
				Ext.each(r.data.rolePermissions, function(rp) {
							rps.push(Ext.apply({}, rp))
						});
				r.data.rolePermissions = rps;
				this.edForm.loadData(r);
			},
			addRole : function() {
				this.editRole(auth.role.Type.emptyRecord());
			},
			modifyRole : function(add) {
				this.editRole(this.getSelectedR())
			},
			reloadRoles : function() {
				this.store.reload();
			},
			// utils
			getSelectedR : function() {
				return this.getSelectionModel().getSelected();
			}
		})
auth.role.MainPanel = Ext.extend(Ext.Panel, {
			layout : 'border',
			border : false,
			initComponent : function() {
				var edFm = new auth.role.EditForm({
							region : 'east',
							width : 400,
							split : true,
							border : false,
							style : 'border-left: 1px solid ' + sys_bdcolor,
							collapseMode : 'mini'
						})
				var lsP = new auth.role.ListPanel({
							region : 'center',
							style : 'border-right: 1px solid ' + sys_bdcolor,
							edForm : edFm
						})
				this.items = [edFm, lsP]
				auth.role.MainPanel.superclass.initComponent.call(this);
			}
		})
