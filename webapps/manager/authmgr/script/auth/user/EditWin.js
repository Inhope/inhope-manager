Ext.namespace('auth.user')
auth.user.EditForm = Ext.extend(Ext.FormPanel, {
			initComponent : function() {
				this.items = [{
							name : 'id',
							anchor : '100%',
							hidden : true
						}, {
							name : 'username',
							fieldLabel : '用户名',
							xtype : 'textfield',
							anchor : '100%',
							allowBlank : false
						}, {
							name : 'password',
							fieldLabel : '密码',
							inputType : 'password',
							xtype : 'textfield',
							anchor : '100%',
							allowBlank : false
						}, {
							name : 'password_raw',
							xtype : 'textfield',
							hidden : true
						}, {
							name : 'roles',
							hidden : true,
							setValue : function(v) {
								this.roleTree.setValue(v)
							}.dg(this),
							getValue : function() {
								return this.roleTree.getValue()
							}.dg(this),
							reset : function() {
								this.roleTree.reset()
							}.dg(this)
						}, {
							xtype : 'fieldset',
							style : 'padding:2px',
							border : false,
							labelAlign : 'top',
							layout : 'fit',
							fieldLabel : '权限',
							anchor : '100% -40',
							items : new auth.RoleTreeSelector({
										ref : '../roleTree'
									})
						}]
				this.tbar = [{
							text : '保存',
							iconCls : 'icon-table-save',
							width : 50,
							handler : function(btn) {
								btn.disable()
								this.saveData(function() {
											btn.enable()
										})
							}.dg(this)
						}]
				auth.user.EditForm.superclass.initComponent.call(this);
			},
			loadData : function(r) {
				this.getForm().reset()
				r.data.password_raw = r.data.password
				if (r.data.password){
					r.data.password = '******'
				}
				this.getForm().loadRecord(r);
				var editMyPwd = !Dashboard.u().allow('auth.usr')
				var readonly = r && (r.get('username') == 'INCE.SUPERVISOR' || editMyPwd) ? true : false;
				this.roleTree[readonly ? 'disable' : 'enable']()
				this.getForm().findField('username').setReadOnly(readonly)
			},
			saveData : function(cb) {
				if (!this.getForm().isValid()) {
					Ext.Msg.alert('消息', '表单有必填项未填写，请重填后再提交')
					cb()
					return;
				}
				var d = this.getForm().getFieldValues();
				var rawpwd 
				if (d.password == '******'){
					d.password = d.password_raw
				}else{
					rawpwd = d.password
					d.password = CryptoJS.SHA1(d.password).toString()
					if (d.password == d.password_raw){
						Ext.Msg.alert('消息', '密码不能和原密码一样')
						cb()
						return
					}
				}
				if (d.password_raw)
					delete d.password_raw
				if (!window._validate_user){
					var validator = http_get('property!get.action?key=authmgr.pwd_validator')
					if (!validator || !validator.trim())
						validator = '' 
					validator = 'window._validate_user = function(user){'+validator+'}';
						try{
							eval(validator);
						}catch(e){
							Ext.Msg.alert('错误','登陆用户名密码验证规则错误，请在系统参数管理中重新修改。')
							cb()
							throw e;
						}
				}
				var errmsg;
				if (rawpwd && (errmsg=window._validate_user({username:d.username,password:rawpwd}))){
					Ext.Msg.alert('消息', '用户名或者密码格式不正确：'+errmsg)
					cb()
					return
				}
				this.getEl().mask('保存...')
				Ext.Ajax.request({
							url : 'user!save.action',
							params : {
								"data" : Ext.encode(ux.util.resetEmptyString(d))
							},
							success : function(response) {
								this.getEl().unmask()
								if (cb)
									cb(true)
								var r = new (auth.user.Type.getRecordType())(Ext.decode(response.responseText).data)
								this.loadData(r)
								this.fireEvent('dataSaved', r);
								Dashboard.setAlert("保存成功!");
							},
							failure : function(response) {
								this.getEl().unmask()
								Ext.Msg.alert("错误", response.responseText);
								if (cb)
									cb(false)
							},
							scope : this
						});
			},
			collapseMode : 'mini',
			margins : '0',
			border : true,
			bodyStyle : 'background-color:' + sys_bgcolor + ' padding : 3px 10px;',
			labelWidth : 55,
			defaults : {
				xtype : 'textfield'
			}
		})
auth.user.EditWin = Ext.extend(Ext.Window, {
			/**
			 * @type {auth.user.EditForm}
			 */
			fmP : null,
			initComponent : function() {
				this.fmP = new auth.user.EditForm();
				this.items = [this.fmP];
				auth.user.EditWin.superclass.initComponent.call(this)
				this.relayEvents(this.fmP, ['dataSaved']);
			},
			loadData : function(r) {
				this.fmP.getForm().reset();
				this.fmP.loadData(r);
			},
			closeAction : 'hide',
			width : 500,
			height : 400,
			plain : true,
			maximizable : true,
			collapsible : true,
			layout : 'fit',
			border : false
		})
