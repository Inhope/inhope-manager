Ext.namespace('auth.user')
auth.user.Type = {
	fields : ['id', 'username', 'password', 'roles'],
	emptyRecord : function() {
		var d = {}
		Ext.each(this.fields, function(f) {
			  d[f] = ''
		  });
		return new (this.getRecordType())(d);
	},
	getRecordType : function() {
		if (!this.rt)
			this.rt = Ext.data.Record.create(this.fields);
		return this.rt;
	},
	createStore : function(cfg) {
		cfg = cfg || {}
		return new Ext.data.JsonStore(Ext.apply({
			  remoteSort : false,
			  idProperty : 'id',
			  fields : this.getRecordType()
		  }, cfg));
	}
}
auth.user.ListPanel = Ext.extend(Ext.grid.GridPanel, {
	  border : false,
	  initComponent : function() {
		  this.store = auth.user.Type.createStore({
			    url : 'user!list.action?fetchRoles=true',
			    root : 'data',
			    autoLoad : true
		    })
		  this.columns = [{
			    header : '用户名',
			    dataIndex : 'username',
			    width : 150
		    }, {
			    header : '密码',
			    dataIndex : 'password',
			    width : 150,
			    renderer : function(v) {
				    return '******';
			    }
		    }, {
			    header : '角色',
			    width : 150,
			    dataIndex : 'roles',
			    renderer : function(v) {
				    if (!v)
					    return ''
				    var ret = ''
				    Ext.each(v, function(role) {
					      ret += role.name + ','
				      })
				    return ret.length ? ret.slice(0, -1) : ''
			    }
		    }]
		  this.selModel = new Ext.grid.RowSelectionModel();

		  var self = this;

		  this.tbar = [{
			    text : '添加用户',
			    iconCls : 'icon-add',
			    disabled : !Dashboard.u().allow('auth'),
			    handler : this.addUser.dg(this, [])
		    }, '', {
			    text : '修改用户',
			    iconCls : 'icon-wordclass-edit',
			    handler : this.modifyUser.dg(this, [])
		    }, '', {
			    text : '删除用户',
			    disabled : !Dashboard.u().allow('auth'),
			    iconCls : 'icon-delete',
			    handler : this.deleteUser.dg(this, [])
		    }, '', {
			    text : '刷新',
			    iconCls : 'icon-refresh',
			    handler : this.reloadUsers.dg(this, [])
		    }, '-', {
			    ref : 'userNameSearch',
			    xtype : 'textfield',
			    width : 200,
			    emptyText : '输入用户名搜索...',
			    listeners : {
				    specialkey : function(field, e) {
					    if (e.getKey() == Ext.EventObject.ENTER) {
						    self.searchUser(self);
					    }
				    }
			    }
		    }, {
			    text : '搜索',
			    iconCls : 'icon-search',
			    handler : this.searchUser.dg(this, [])
		    }]

		  auth.user.ListPanel.superclass.initComponent.call(this)
		  this.on('rowdblclick', function() {
			    this.modifyUser()
		    })
		  // TODO:debug
		  this.getView().on('refresh', function() {
			    this.getSelectionModel().selectRow(0);
		    }, this)
	  },
	  // data operation
	  deleteUser : function() {
		  if (!this.getSelectedR()) {
			  Ext.Msg.alert('提示', '请先选中需要删除的行！');
			  return;
		  }
		  var r = this.getSelectedR()
		  Ext.Msg.confirm('提示', '确定要删除 ' + r.get('username') + ' 吗', function(btn, text) {
			    if (btn == 'yes') {
				    Ext.Ajax.request({
					      url : 'user!delete.action',
					      params : {
						      id : r.data.id
					      },
					      success : function(form, action) {
						      Dashboard.setAlert('删除成功')
						      this.reloadUsers();
					      },
					      failure : function(response) {
						      Ext.Msg.alert("错误", response.responseText);
					      },
					      scope : this
				      });
			    }
		    }, this);
	  },
	  showEditWin : function(r) {
		  if (!this._edWin) {
			  this._edWin = new auth.user.EditWin();
			  this._edWin.on('dataSaved', function(r) {
				    var pos = this.getStore().find('id', r.get('id'))
				    if (pos != -1) {
					    var oldR = this.getStore().getAt(pos)
					    oldR.beginEdit();
					    for (var key in r.data)
						    oldR.set(key, r.data[key]);
					    oldR.endEdit();
					    oldR.commit()
				    } else {
					    this.getStore().add(r.copy())
				    }
			    }, this)
		  }
		  this._edWin.loadData(r);
		  this._edWin.show();
		  return this._edWin;
	  },
	  addUser : function() {
		  this.showEditWin(auth.user.Type.emptyRecord()).setTitle('新建用户')
	  },
	  modifyUser : function() {
		  var r = this.getSelectionModel().getSelected()
		  if (r)
			  this.showEditWin(r).setTitle('编辑用户 - ' + r.get('username'))
	  },
	  reloadUsers : function() {
	  	  var userName = this.topToolbar.userNameSearch.getValue();
		  this.store.setBaseParam('userName', userName);
		  this.store.reload();
	  },
	  // utils
	  getSelectedR : function() {
		  return this.getSelectionModel().getSelected();
	  },
	  searchUser : function() {
		  var userName = this.topToolbar.userNameSearch.getValue();
		  this.store.setBaseParam('userName', userName);
		  this.store.load();
	  }
  })
