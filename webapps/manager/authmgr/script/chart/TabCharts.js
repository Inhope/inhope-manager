//Ext.tip.QuickTipManager.init();
//Ext.Loader.setPath('Ext.app', '.');
//Ext.Loader.setPath('Ext.tab', '.');
Ext.chart.randomTheme = function() {
	var arr = [ 'Base', 'Green', 'Sky', 'Red', 'Purple', 'Blue', 'Yellow', 'Category1', 'Category2', 'Category3', 'Category4', 'Category5',
			'Category6' ];
	//return arr[Math.floor((Math.random() * arr.length) + 1)];
	return arr[5];
};

Ext.define('Ext.app.TabChart', {
	extend : 'Ext.tab.Panel',
	initComponent : function() {
		var self = this;
		var getListeners = function() {
			return {
				'render' : function() {
					this.setLoading('正在加载数据...');
				}
			};
		};
		
		var intr = setInterval(function() {
			self.getActiveTab().getStore().reload();
	    }, 60000);
		
		this._chartStores = new ChartMap();
		this._hostTypeColor = new ChartMap();
		this._colorArray = new Array("#6C3365","#336666","#613030","#844200","#424200","#006000","#003E3E","#000079","#5E005E","#4D0000")
		
		var formatByte = function(val) {
			if (val < 0) {
				return '未知';
			} else if (val >= 0 && val < 1024 * 1024) {
				return (val / 1024).toFixed(2) + "K";
			} else if (val >= 1024 * 1024 && val < 1024 * 1024 * 1024) {
				return (val / 1024 / 1024).toFixed(2) + "M"
			} else if (val >= 1024 * 1024 * 1024 && val < 1024 * 1024 * 1024 * 1024) {
				return (val / 1024 / 1024 / 1024).toFixed(2) + "G"
			}
			return val;
		};
		
		//平台节点信息store
		var node_store = new Ext.data.JsonStore({
			autoLoad : true,
			proxy : {
				type : 'ajax',
				url : 'log-sys-info!list.action',
				reader : {
					type : 'json',
					root : 'data'
				}
			},
			model : 'PLATFORM_NODE.MODE',
			writer : new Ext.data.JsonWriter()
		});
		
		
		node_store.on('load', function(st, records) {
			if (this.getCount() > 0) {
				self.add(self.createChart("进程CPU","cpuUtilization"));
				self.add(self.createChart("系统内存","sysUsedMemory"));
				self.add(self.createChart("JVM内存","jvmUsedMemory"));
				self.setActiveTab(0);
			}
		});
		
		var getRandomColor = function(){
			return '#'+('00000'+(Math.random()*0x1000000<<0).toString(16)).slice(-6);
		};
		
		var _createChart = function(title,key){
			var fields = new Array();
			var fields_x = new Array();
			var fields_y = new Array();
			var seriess = new Array();
			var index = 0 ;
			node_store.each(function(){
				var resStoreThis = this;
				var color = "";
				if(self._hostTypeColor.containsKey(resStoreThis.id)){
					color = self._hostTypeColor.get(resStoreThis.id);
				}else{
					color = getRandomColor();
					self._hostTypeColor.put(resStoreThis.id,color);
				}
				color = self._colorArray[index];
				index++;
				seriess.push({	
						type : 'line',
						title : resStoreThis.data.node,
						label: resStoreThis.data.node,
						yField : (resStoreThis.data.type+''+resStoreThis.data.node+''+key).replace(/\./g, "").replace(":", ""),
						xField : (resStoreThis.data.type+''+resStoreThis.data.node+'time').replace(/\./g, "").replace(":", ""),
						style: {
							'stroke-width': 2,
							fill: 'none'
			            },
						highlight: {
		                    size: 4,
		                    radius: 4
		                },
						markerConfig: {
		                    type: 'circle',
		                    size: 6,
		                    radius: 0,
		                    'stroke-width': 0
		                },
		                tips: {
		                	trackMouse: true,
		                	width: 130,
		                	height: 25,
		                	renderer: function(storeItem, item) {
		                		var text = storeItem.get((resStoreThis.data.type+''+resStoreThis.data.node+''+key).replace(/\./g, "").replace(":", ""));
		                		if(key == 'cpuUtilization'){
		                			text += "%";
		                		}else{
		                			text = formatByte(text);
		                		}
		                		this.setTitle(title + ': ' + text);
		                	}
						},
						smooth: true
				});
				var x = (resStoreThis.data.type+''+resStoreThis.data.node+'time').replace(/\./g, "").replace(":", "");
				fields_x.push(x);
				fields.push(x);
				
				var y = (resStoreThis.data.type+''+resStoreThis.data.node+''+key).replace(/\./g, "").replace(":", "");
				fields_y.push(y);
				fields.push(y);
			});
			
			if(!this._chartStores.containsKey(key)){
				this._chartStores.put(key,new Ext.data.JsonStore({
					autoLoad : true,
					proxy : {
						type : 'ajax',
						url : 'log-sys-info!dayListAll.action',
						reader : {
							type : 'json',
							root : 'data'
						}
					},
					fields : fields
				}));
			}
			
			var yAxis = function(){
				if(key == 'cpuUtilization'){
					return {
		                type: 'Numeric',
		                position: 'left',
		                fields: fields_y,
		                title: '使用率（%）',
		                grid: true,
		                decimals: 0,
		                minimum: 0,
		                maximum: 100
		            };
				}else if(key == 'sysUsedMemory'){
					return {
						type: 'Numeric',
						position: 'left',
						title: '系统内存',
						fields: fields_y,
						grid: true,
						label: {
							renderer  : formatByte
						}
					};
				}else if(key == 'jvmUsedMemory'){
					return {
						type: 'Numeric',
						position: 'left',
						title: 'JVM内存',
						fields: fields_y,
						grid: true,
						label: {
							renderer  : formatByte
						}
					};
				}
			};
			
			var useChartStores = this._chartStores.get(key);
			return new Ext.create('Ext.chart.Chart', {
				title : title,
				style: 'background:#fff',
	            animate: true,
	            store: useChartStores,
	            shadow: false,
	            theme: 'Category2',
	            height : 400,
	            legend: {
	                position: 'bottom'
	            },
	            axes: [yAxis(),{
	                type: 'Numeric',
	                position: 'bottom',
	                fields: fields_x,
	                maximum: 24,
	                minimum: 0
	            }],
	            series: seriess
	            
			});
		};
		self.createChart = _createChart;
		
		Ext.apply(this,{
			renderTo : document.body,
			layout : 'fit',
	        activeTab: 1,
	        defaults :{
	            bodyPadding: 10
	        }
//			items: [createChart("进程CPU","cpuUtilization"),{
//                title: '系统内存',
//                html: "My content was added during construction."
//            },{
//                title: 'JVM内存',
//                html: "My content was added during construction."
//            }]
		});
		this.callParent();
	}
});

/**
 * 平台节点信息MODE
 */
Ext.define('PLATFORM_NODE.MODE', {
	extend : 'Ext.data.Model',
	fields : [{
		name : 'node',
		type : 'string'
	}, {
		name : 'type',
		type : 'string'
	}]
});

Ext.require([ 'ChartMap','Ext.tab.*','Ext.data.*','Ext.chart.*']);

Ext.onReady(function() {
	Ext.getBody().addCls(Ext.baseCSSPrefix + 'theme-neptune');
	Ext.create('Ext.app.TabChart');
});