<%@page import="com.incesoft.xmas.commons.LabsUtil"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>

	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title></title>
	<% 
		String theme = "neptune"; 
		if (LabsUtil.getExtTheme() != null)
			theme = LabsUtil.getExtTheme();
		pageContext.setAttribute("theme", theme);
	%>
	<link rel="stylesheet" type="text/css" href="<c:url value="/ext4/resources/css/ext-all-${theme}.css"/>" />
	<script src="<c:url value="/ext4/ext-all.js"/>" type="text/javascript"></script>
	<script src="TabCharts.js" type="text/javascript"></script>
	</head>
	
	<body></body>

</html>