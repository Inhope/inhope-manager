/**
 * 
 * @include "NavPanel.js"
 */

Dashboard = {
	u : function() {
		return this._usr ? this._usr : this._usr = new LoginUser('auth')
	},
	setAlert : function(msg, status, delay) {
		if (!status)
			status = 'ok'
		if (!delay)
			delay = 3
		if (!this.msgCt) {
			this.msgCt = Ext.DomHelper.insertFirst(document.body, {
				id : 'msg-div'
			}, true);
			this.msgCt.setStyle('position', 'absolute');
			this.msgCt.setStyle('z-index', 9999);
			this.msgCt.setWidth(300);
		}
		this.msgCt.alignTo(document, 't-t');
		var html = [ '<div class="app-msg">', '<div class="x-box-tl"><div class="x-box-tr"><div class="x-box-tc"></div></div></div>',
				'<div class="x-box-ml"><div class="x-box-mr"><div class="x-box-mc"><h3 class="x-icon-text icon-status-' + status + '">', msg,
				'</h3>', '</div></div></div>', '<div class="x-box-bl"><div class="x-box-br"><div class="x-box-bc"></div></div></div>', '</div>' ]
				.join('');
		Ext.DomHelper.append(this.msgCt, {
			'html' : html
		}, true).slideIn('t').pause(delay).ghost("t", {
			remove : true
		});
	},
	navRegistry : [],
	registerNav : function(navClass, sort) {
		this.navRegistry.push({
			'createNav' : function(id) {
				return new navClass({
					'id' : id
				})
			},
			'sort' : sort
		})
	},
	isCloudEmbed : function() {
		return getTopWindow().location.toString().indexOf('clustermgr') != -1
	}
};

Ext.onReady(function() {
	Ext.QuickTips.init();

	var mainPanel = new Ext.TabPanel({
		region : 'center',
		activeTab : 0,
		border : false,
		style : 'border-left: 1px solid ' + sys_bdcolor,
		resizeTabs : true,
		tabWidth : 150,
		minTabWidth : 120,
		enableTabScroll : true
	});

	// var r = Dashboard.navRegistry.sort(function(nav1, nav2) {
	// return nav1.sort - nav2.sort
	// });
	//
	// var tabRegistry = {};
	//
	// var navItems = [];
	//
	// for (var i = 0; i < r.length; i++) {
	// var nav = r[i].createNav();
	// nav.moduleIndex = i;
	// nav.showTab = function(panelClass, tabId, title, iconCls, closable) {
	// if (!panelClass)
	// return false;
	// var tab = mainPanel.get(tabId);
	// if (!tab) {
	// var tabPanel = new panelClass();
	// tabPanel.navPanel = this;
	// tabPanel.tabId = tabId;
	// tab = mainPanel.add({
	// id : tabId,
	// layout : 'fit',
	// title : title,
	// items : tabPanel,
	// iconCls : iconCls,
	// closable : closable
	// });
	// tabRegistry[tabId] = this.moduleIndex;
	// tab.show();
	// } else {
	// mainPanel.activate(tabId);
	// if (title)
	// tab.setTitle(title)
	// if (iconCls)
	// tab.setIconClass(iconCls)
	// }
	// return tab.get(0);
	// };
	// nav.closeTab = function(tabId) {
	// mainPanel.remove(tabId)
	// };
	//
	// if (i == 0) {
	// nav.on('afterrender', function() {
	// (function () {
	// if (this.getRootNode().childNodes[0])
	// this.getRootNode().childNodes[0].fireEvent('click',
	// this.getRootNode().childNodes[0])
	// }).defer(100, this)
	// })
	// } else {
	// nav.on('expand', function() {
	// this.renderRootNode();
	// })
	// }
	// navItems.push(nav);
	// }
	//
	// var navPanel = new Ext.Panel({
	// region : 'west',
	// width : 200,
	// minSize : 175,
	// maxSize : 400,
	// split : true,
	// collapsible : true,
	// collapseMode : 'mini',
	// border : false,
	// style : 'border-right: 1px solid ' + sys_bdcolor,
	// layout : 'accordion',
	// titleCollapse : false,
	// title : '控制台',
	// items : navItems
	// });
	var childNav = Dashboard.u().filterAllowed([ {
		id : 'auth.usr',
		text : '用户管理',
		leaf : true,
		iconCls : 'icon-toolbox',
		panelClass : auth.user.ListPanel
	}, {
		id : 'auth.role',
		authName : 'auth.role',
		text : '角色管理',
		leaf : true,
		iconCls : 'icon-toolbox',
		panelClass : auth.role.MainPanel
	}, {
		id : 'auth.perm',
		authName : 'auth.perm',
		text : '资源管理',
		leaf : true,
		iconCls : 'icon-toolbox',
		panelClass : auth.PermissionTree
	}, {
		id : 'auth.op',
		authName : 'auth.op',
		text : '权限管理',
		leaf : true,
		iconCls : 'icon-toolbox',
		panelClass : auth.OperationTree
	} ]);

	var navs = Dashboard.u().filterAllowed([ {
		id : 'auth',
		text : '授权管理',
		iconCls : 'icon-toolbox',
		children : childNav
	} ]);
	if (!Dashboard.isCloudEmbed()) {
		navs = navs.concat(Dashboard.u().filterAllowed([ {
			id : 'auth.rapi',
			authName : 'auth.rapi',
			text : '接口权限管理',
			leaf : true,
			iconCls : 'icon-rapi',
			panelClass : auth.rapi.AddressRestrictPanel
		}, {
			id : 'auth.sysprop',
			leaf : true,
			authName : 'auth.sysp',
			text : '系统参数管理',
			iconCls : 'icon-sysprop',
			panelClass : SysPropPanel
		}, {
			id : 'auth.sysinfo',
			leaf : true,
			authName : 'auth.sysinfo',
			text : '系统监控管理',
			iconCls : 'icon-notice',
			panelClass : SysInfoPanel
		}, {
			id : 'auth.robotcat',
			leaf : true,
			authName : 'auth.robotcat',
			text : '系统业务监控',
			iconCls : 'icon-notice',
			panelClass : RobotCatPanel2
		} ]));
		if (isVersionStandard()) {
			navs.splice(navs.length - 1, 1);
			navs = navs.concat([ {
				leaf : true,
				text : '服务参数配置',
				iconCls : 'icon-message-resource',
				panelClass : {
					creator : function() {
						return new IFramePanel({
							frameURL : '../mktmgr/std-msgresource.jsp',
							style : 'border-width:0px;border-right-width:1px',
							border : false
						});
					}
				}
			}, {
				leaf : true,
				text : '特殊欢迎语配置',
				panelClass : {
					creator : function() {
						return new IFramePanel({
							frameURL : '../mktmgr/std-welcome.jsp',
							style : 'border-width:0px;border-right-width:1px',
							border : false
						});
					}
				}
			}, {
				leaf : true,
				text : '对话预处理',
				panelClass : {
					creator : function() {
						return new IFramePanel({
							frameURL : '../mktmgr/std-simpledlg.jsp',
							style : 'border-width:0px;border-right-width:1px',
							border : false
						});
					}
				}
			} ]);
		}
	}
	var navPanel = new Ext.tree.TreePanel({
		region : 'west',
		width : 200,
		minSize : 175,
		maxSize : 400,
		split : true,
		collapsible : true,
		collapseMode : 'mini',
		border : false,
		style : 'border-right: 1px solid ' + sys_bdcolor,
		title : '控制台',
		autoScroll : true,
		rootVisible : false,
		lines : false,
		root : {
			id : '0',
			text : 'root',
			children : navs
		}
	});
	Dashboard.navPanel = mainPanel;
	navPanel.showTab = function(panelClassOrCreateFn, tabId, title, iconCls, closable) {
		if (!panelClassOrCreateFn)
			return false;
		var tab = mainPanel.get(tabId);
		if (!tab) {
			var tabPanel = !panelClassOrCreateFn.creator ? new panelClassOrCreateFn() : panelClassOrCreateFn.creator();
			tabPanel.navPanel = this;
			tabPanel.tabId = tabId;
			tab = mainPanel.add({
				id : tabId,
				layout : 'fit',
				title : title,
				items : tabPanel,
				iconCls : iconCls,
				closable : closable
			});
			tab.contentPanel = tabPanel;
			tab.show();
		} else {
			mainPanel.activate(tabId);
			if (title)
				tab.setTitle(title)
			if (iconCls)
				tab.setIconClass(iconCls)
		}
		return tab.get(0);
	};
	navPanel.closeTab = function(tabId) {
		mainPanel.remove(tabId)
	};
	navPanel.on('click', function(n) {
		if (!n)
			return false;
		var panelClass = n.attributes.panelClass;
		if (panelClass) {
			var p = this.showTab(panelClass, n.id + 'Tab', n.text, n.attributes.iconCls, true);
			if (p && p.init)
				p.init();
		}
	});
	navPanel.on('expandnode', function(node) {
		if (node.firstChild && node.firstChild.id == 'auth') {
			node.firstChild.expand();
			this.fireEvent('click', node.firstChild.firstChild);
		}
	}, navPanel);

	var viewport = new Ext.Viewport({
		layout : 'border',
		items : [ navPanel, mainPanel ]
	});

	// mainPanel.on('tabchange', function(tabPanel, tab) {
	// if (!tab)
	// return;
	// var navIndex = tabRegistry[tab.id];
	// if (typeof navIndex == 'undefined')
	// return;
	// navPanel.layout.setActiveItem(navIndex);
	// })
});