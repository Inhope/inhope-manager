
Ext.ns('Ext.task');


Ext.task.ExcelUploadWindow = function(cfg) {
	
	var self = this;
	
	var formPanel = new Ext.FormPanel({
			title:"请按照指定格式",
	        fileUpload: true,
	        width: 500,
	        frame: true,
	        autoHeight: true,
	        bodyStyle: 'padding: 10px 10px 0 10px;',
	        labelWidth: 80,
	        defaults: {
	            anchor: '95%',
	            allowBlank: false,
	            msgTarget: 'side'
	        },
	        items: [{
	            xtype: 'hidden',
	            name: 'baseDir',
	            value: cfg.baseDir?cfg.baseDir:'/files'
	        },{
	            xtype: 'fileuploadfield',
	            emptyText: cfg.emptyText?cfg.emptyText:'选择文件',
	            fieldLabel: cfg.fieldLabel?cfg.fieldLabel:'Excel',
	            name: 'sheetFile',
	            buttonText: '',
	            buttonCfg: {
	                iconCls: 'icon-image-add'
	            }
	        }],
	        buttonAlign : 'center',
	        buttons: [{
	            text: '上传',
	            handler: function(){
	                if(formPanel.getForm().isValid()){
		                formPanel.getForm().submit({
		                    url: cfg.url,
		                    waitMsg: '正在上传...',
		                    success: function(formPanel, o){
		                    	cfg.updatePath(o.result);
		                        self.hide();
		                    }
		                });
	                }
	            }
	        }]
    	});

	Ext.task.ExcelUploadWindow.superclass.constructor.call(this, {
		title : cfg.title,
		iconCls : 'icon-image-add',
		closeAction : 'hide',
		autoHeight : true,
		width : 507,
		resizable : false,
		plain : true,
		modal : true,
		autoScroll : false,
		items : formPanel,
		listeners : {
			beforeshow : function() {
				eachField(formPanel,function(item){
					item.reset();
				}); 
			}
		}
	});
}
Ext.extend(Ext.task.ExcelUploadWindow, Ext.Window, {
	
});