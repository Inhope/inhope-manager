Ext.ns('Ext.task');

Ext.task.FormRowEditor = function(cfg) {

	var editor = this;
	var store = cfg.store;

	this.record = cfg.record;

	Ext.task.FormRowEditor.superclass.constructor.call(this, {
		title : cfg.title,
		iconCls : 'icon-user-login',
		height : cfg.height,
		width : cfg.width,
		resizable : true,
		plain : true,
		modal : true,
		autoScroll : false,
		closeAction : 'hide',
		layout : 'fit',
		items : new Ext.FormPanel({
			labelAlign : cfg.labelAlign ? cfg.labelAlign : 'top',
			frame : true,
			items : cfg.items,
			buttonAlign : 'center',
			buttons : cfg.buttons ? cfg.buttons : [{
						text : '保存',
						handler : function() {
							if (cfg.handler) {
								cfg.handler();
								return;
							}
							if (store.getById(editor.record.id)) {
								editor.record.beginEdit();
							}
							var valid = eachField(editor, function(item) {
										if (!item.validate()) {
											return false;
										}
										var value = item.getValue();
										if (value) {
											editor.record.set(item.name, value);
										}
										return true;
									});
							if (!valid) {
								editor.record.cancelEdit();
								return;
							}

							if (store.getById(editor.record.id)) {
								editor.record.endEdit();
								editor.record.commit();
							} else {
								store.insert(0, editor.record);
							}
							editor.hide();
						}
					}, {
						text : '取消',
						handler : function() {
							editor.hide();
						}
					}]
		}),
		listeners : {
			beforeshow : function() {
				eachField(editor, function(item) {
							item.reset();
							var v;
							if (editor.record.get)
								v = editor.record.get(item.name);
							else
								v = editor.record[item.name]
							if (v)
								item.setValue(v);
							return true;
						});
				if (cfg.beforeshow) {
					cfg.beforeshow();
				}
			}
		}
	});
}
Ext.extend(Ext.task.FormRowEditor, Ext.Window, {
			getFieldByName : function(name) {
				var targetField = null;
				eachField(this, function(field) {
							if (field.getName() == name) {
								targetField = field;
								return false;
							}
						});
				return targetField;
			}
		});