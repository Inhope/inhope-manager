KnowledgeResultGrid = function(cfg) {
	var self = this;
	var pageSize = 50;
	var grid = this;
	var taskId = cfg.taskId;
	this.taskId = taskId;
	var TaskResult = Ext.data.Record.create([/*
												 * { name : 'taskId', type :
												 * 'string' },{ name : 'tkId',
												 * type : 'string' },
												 */{
				name : 'tQuestion',
				type : 'string'
			}, {
				name : 'tAnswer',
				type : 'string'
			}, {
				name : 'tAnswerType',
				type : 'string'
			}, {
				name : 'isRight',
				type : 'string'
			}, {
				name : 'isContainQuestion',
				type : 'string'
			}, {
				name : 'taskDate',
				type : 'string'
			}, {
				name : 'question',
				type : 'string'
			}, {
				name : 'testQuestion',
				type : 'string'
			}, {
				name : 'answer',
				type : 'string'
			}]);

	var reader = new Ext.data.JsonReader({
				idProperty : 'id',
				root : 'data',
				messageProperty : 'message',
				totalProperty : 'total',
				remoteSort : true
			}, TaskResult);

	var writer = new Ext.data.JsonWriter({
				writeAllFields : true
			});

	var proxy = new Ext.data.HttpProxy({
				api : {
					read : 'robot-cat!results.action?taskId=' + cfg.taskId
				},
				listeners : {
					write : function(proxy, action, result, res, rs) {
						if (res.message) {
							App.setAlert(true, res.message);
						}
					},
					exception : function(proxy, type, action, options, res) {
						if (res.responseText) {
							var r = eval("s=" + res.responseText);
							if (r.message && r.message == 'logout') {
								redirectLogin();
							}
							App.setAlert(false, r.message);
						} else {
							if (res.message && res.message == 'logout') {
								redirectLogin();
							}
							App.setAlert(false, res.message);
						}
					}
				}
			});

	var store = new Ext.data.Store({
				proxy : proxy,
				reader : reader,
				writer : writer,
				autoLoad : {
					params : {
						start : 0,
						limit : pageSize
					}
				},
				autoSave : true
			});
	// store.setDefaultSort('isRight', 'desc');
	store.remoteSort = true;

	this.getSelectionModel().on('selectionchange', function(sm) {
				// grid.removeBtn.setDisabled(sm.getCount() < 1);
			});

	var taskDateField = new Ext.form.DateField({
				name : "taskDate",
				xtype : 'datefield',
				editable : false,
				format : 'Y-m-d',
				selectOnFocus : true,
				editable : false,
				value : new Date(),
				validator : function(v) {
					if (v) {
						if (v.length < 1) {
							App.setAlert(true, '时间不能为空!');
							return false;
						}
						return true;
					}
					return false;
				},
				width : 100
			});
	this.taskDateField = taskDateField;
	this.tbar = ['选择时间: ', ' ', taskDateField, {
				text : '搜 索',
				iconCls : 'icon-grid',
				handler : function() {
					self.search();
				}
			}, '-', {
				iconCls : 'icon-grid',
				text : '导出03版',
				handler : function() {
					self.exportData('xls');
				}
			}, '-', {
				iconCls : 'icon-grid',
				text : '导出07版',
				handler : function() {
					self.exportData('xlsx');
				}
			}];

	this.columns = [{
				header : '标准问',
				dataIndex : 'question',
				width : 150
			}, {
				header : '测试问题',
				dataIndex : 'testQuestion',
				width : 150
			}, {
				header : '测试答案',
				dataIndex : 'answer',
				renderer : function(answer) {
					if (answer) {
						answer = answer.replace(new RegExp("<", "gm"), "&lt;");
						answer = answer.replace(new RegExp(">", "gm"), "&gt;");
						answer = answer
								.replace(new RegExp("\n", "gm"), "<br/>");
						return answer;
					}
					return "";
				}
			}, {
				header : '触发问题',
				dataIndex : 'tQuestion',
				width : 150
			}, {
				header : '实际答案',
				dataIndex : 'tAnswer',
				renderer : function(v) {
					if (v) {
						v = v.replace(new RegExp("<", "gm"), "&lt;");
						v = v.replace(new RegExp(">", "gm"), "&gt;");
						v = v.replace(new RegExp("\n", "gm"), "<br/>");
						return v;
					}
					return "";
				}
			}, {
				header : '回答类型',
				dataIndex : 'tAnswerType',
				width : 50,
				renderer : function(v) {
					var m = '';
					if (v) {
						if (v == '-1') {
							m = '前端回复';
						} else if (v == '0') {
							m = '无回复';
						} else if (v == '1') {
							m = '标准答案';
						} else if (v == '2') {
							m = '列表';
						} else if (v == '3') {
							m = '其他';
						} else if (v == '4') {
							m = '聊天(通用聊天库)';
						} else if (v == '5') {
							m = '敏感词';
						} else if (v == '6') {
							m = '重复';
						} else if (v == '7') {
							m = '对话预处理的直接回复';
						} else if (v == '8') {
							m = '聊天(定制聊天库)';
						} else if (v == '9') {
							m = '过期';
						} else if (v == '10') {
							m = '拼写错误';
						} else if (v == '11') {
							m = '建议问';
						} else if (v > '100') {
							m = '指令';
						}
					}
					return m;
				}
			}, {
				header : '是否正确',
				dataIndex : 'isRight',
				width : 50,
				renderer : function(v) {
					if (v) {
						if (v == 'true') {
							return '正确';
						} else if (v == 'exception') {
							return '异常';
						}
						return '错误';
					}
					return '';
				},
				sortable : true,
				remoteSort : true
			}, {
				header : '是否包含',
				dataIndex : 'isContainQuestion',
				width : 60,
				renderer : function(v) {
					if (v) {
						if (v == 'true') {
							return '包含';
						} else {
							return '不包含';
						}
					}
					return '';
				},
				sortable : true,
				remoteSort : true
			}, {
				header : '测试日期',
				dataIndex : 'taskDate',
				width : 100,
				sortable : true,
				remoteSort : true
			}];

	var pagingToolbar = new Ext.PagingToolbar({
				store : store,
				displayInfo : true,
				pageSize : pageSize,
				prependButtons : true,
				beforePageText : '第',
				afterPageText : '页，共{0}页',
				displayMsg : '第{0}到{1}条记录，共{2}条',
				emptyMsg : "没有记录"
			});

	var editor = new Ext.task.KnowledgeResultPanel({
				title : '测试结果详情',
				width : 1000,
				height : 550,
				store : store,
				items : [{
							layout : 'column',
							items : [{
										columnWidth : .48,
										layout : 'form',
										style : {
											float : 'left'
										},
										items : {
											xtype : 'textfield',
											name : 'question',
											fieldLabel : '标准问题',
											anchor : '100%',
											readOnly : true
										}
									}]
						}, {
							layout : 'column',
							items : [{
										columnWidth : .48,
										layout : 'form',
										style : {
											float : 'left'
										},
										items : {
											xtype : 'textfield',
											name : 'testQuestion',
											fieldLabel : '测试问题',
											anchor : '100%',
											readOnly : true
										}
									}, {
										columnWidth : .48,
										layout : 'form',
										style : {
											float : 'right'
										},
										items : {
											xtype : 'textfield',
											name : 'tQuestion',
											fieldLabel : '触发问题',
											anchor : '100%',
											readOnly : true
										}
									}]
						}, {
							layout : 'column',
							items : [{
										columnWidth : .48,
										layout : 'form',
										style : {
											float : 'left'
										},
										items : {
											xtype : 'textarea',
											name : 'answer',
											fieldLabel : '标准答案',
											width : '460px',
											height : '300px',
											readOnly : true
										}
									}, {
										columnWidth : .48,
										layout : 'form',
										style : {
											float : 'right'
										},
										items : {
											xtype : 'textarea',
											name : 'tAnswer',
											fieldLabel : '触发答案',
											width : '460px',
											height : '300px',
											readOnly : true
										}
									}]
						}, {
							layout : 'column',
							items : [{
								columnWidth : .3,
								layout : 'form',
								style : {
									float : 'left',
									padding : '0px 0px 0px 5px'
								},
								items : {
									xtype : 'combo',
									name : 'tAnswerType',
									fieldLabel : '答案类型',
									store : new Ext.data.SimpleStore({
												fields : ['value', 'text'],
												data : [['-1', '前端回复'],
														['0', '无回复'],
														['1', '标准答案'],
														['2', '列表'],
														['3', '其他'],
														['4', '聊天(通用聊天库)'],
														['5', '敏感词'],
														['6', '重复'],
														['7', '对话预处理的直接回复'],
														['8', '聊天(定制聊天库)'],
														['9', '过期'],
														['10', '拼写错误'],
														['11', '建议问'],
														['101', '指令']]
											}),
									displayField : 'text',
									valueField : 'value',
									width : 150,
									readOnly : true,
									mode : 'local'
								}
							}, {
								columnWidth : .3,
								layout : 'form',
								style : {
									float : 'left',
									padding : '0px 0px 0px 5px'
								},
								items : {
									xtype : 'combo',
									name : 'isRight',
									fieldLabel : '是否正确',
									store : new Ext.data.SimpleStore({
												fields : ['value', 'text'],
												data : [['true', '正确'],
														['false', '错误'],
														['exception', '异常']]
											}),
									displayField : 'text',
									valueField : 'value',
									width : 150,
									readOnly : true,
									mode : 'local'
								}
							}, {
								columnWidth : .3,
								layout : 'form',
								style : {
									float : 'left',
									padding : '0px 0px 0px 5px'
								},
								items : {
									xtype : 'combo',
									name : 'isContainQuestion',
									fieldLabel : '是否包含标准问',
									store : new Ext.data.SimpleStore({
												fields : ['value', 'text'],
												data : [['true', '包含'],
														['false', '不包含']]
											}),
									displayField : 'text',
									valueField : 'value',
									width : 150,
									readOnly : true,
									mode : 'local'
								}
							}]
						}]
			});

	KnowledgeResultGrid.superclass.constructor.call(this, {
				store : store,
				region : 'center',
				border : false,
				viewConfig : {
					forceFit : true,
					markDirty : true
				},
				bbar : pagingToolbar,
				listeners : {
					rowdblclick : function(grid, index, e) {
						editor.record = grid.getSelectionModel().getSelected();
						editor.show();
					}
				}
			});

	this.getPageSize = function() {
		return pageSize;
	};
}

Ext.extend(KnowledgeResultGrid, Ext.grid.GridPanel, {
	search : function() {
		var taskDate = this.taskDateField.getValue();
		if (!taskDate || taskDate == '') {
			alert("请选择查询时间！");
		} else {
			taskDate = taskDate.format('Y-m-d');
		}
		for (var key in this.getStore().baseParams) {
			if (key && key.indexOf('queryParam.') != -1)
				delete store.baseParams[key];
		}
		this.getStore().setBaseParam('taskDate', taskDate);
		this.getStore().load({
					params : {
						start : 0,
						limit : this.getPageSize(),
						'taskDate' : taskDate
					}
				});
	},
	exportData : function(v) {
		var self = this;
		var taskDate = this.taskDateField.getValue();
		if (!taskDate || taskDate == '') {
			alert("请选择查询时间！");
			return;
		} else {
			taskDate = taskDate.format('Y-m-d');
		}
		// var url = "robot-cat!results.action?taskId=" + this.taskId
		// + "&taskDate=" + taskDate + "&export=export&format=" + v;
		// window.open(url);
		// 开始下载
		Ext.Ajax.request({
			url : 'robot-cat!results.action',
			params : {
				taskId : self.taskId,
				taskDate : taskDate,
				'export' : 'export',
				format : v
			},
			success : function(response) {
				var result = Ext.util.JSON.decode(response.responseText);
				if (result.success) {
					if (!self.downloadIFrame) {
						self.downloadIFrame = self.getEl().createChild({
									tag : 'iframe',
									style : 'display:none;'
								})
					}
					self.downloadIFrame.dom.src = 'robot-cat!downExportFile.action?_t=' + new Date().getTime();
				} else {
					Ext.Msg.alert('错误', '导出失败:' + result.message)
				}
			},
			failure : function(response) {
				Ext.Msg.alert('错误', '导出失败')
			},
			scope : this
		});
	}
});
