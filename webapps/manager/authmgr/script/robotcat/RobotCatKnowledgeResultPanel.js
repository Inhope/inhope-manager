Ext.ns('Ext.task');
function eachField(target, fn) {
	var retFn = true;
	if(target.items) {
    	target.items.each(function(item){
    		if(item instanceof Ext.form.Field) {
    			if(!fn(item)) {
    				retFn = false;
    				return;
    			}
    		}
    		else {
    			if(!eachField(item,fn)) {
    				retFn = false;
    				return;
    			}
    		}
    	},target);
	}
	return retFn;
}

Ext.task.KnowledgeResultPanel = function(cfg) {
	
	var editor = this;
	var store = cfg.store;
	
	this.record = cfg.record;

	Ext.task.KnowledgeResultPanel.superclass.constructor.call(this, {
		title : cfg.title,
		iconCls : 'icon-user-login',
		height : cfg.height,
		width : cfg.width,
		resizable : true,
		plain : true,
		modal : true,
		autoScroll : false,
		closeAction : 'hide',
		layout: 'fit',
		items : new Ext.FormPanel({
	        labelAlign: cfg.labelAlign?cfg.labelAlign:'top',
	        frame:true,
	        items: cfg.items,
			buttonAlign : 'center',
	        buttons: [{
	            text: '关闭',
	            handler: function(){
	                editor.hide();
	            }
	        }]
	    }),
		listeners : {
			beforeshow : function() {
				eachField(editor,function(item){
					item.reset();
					var v;
					if(editor.record.get)v = editor.record.get(item.name);
					else v = editor.record[item.name]
					if(v) item.setValue(v);
					return true;
				}); 
				if(cfg.beforeshow) {
					cfg.beforeshow();
				}
			}
		}
	});
}
Ext.extend(Ext.task.KnowledgeResultPanel, Ext.Window, {
	getFieldByName : function(name) {
		var targetField = null;
		eachField(this,function(field){
			if(field.getName()==name) {
				targetField = field;
				return false;
			}
		});
		return targetField;
	}
});