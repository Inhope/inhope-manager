TaskResultGrid = function(cfg) {
	
	var pageSize = 50;
	var grid = this;
	
	var TaskResult = Ext.data.Record.create([{
				name : 'id',
				type : 'string'
			}, {
				name : 'taskId',
				type : 'string'
			},{
				name : 'tkId',
				type : 'string'
			},{
				name : 'tQuestion',
				type : 'string'
			},{
				name : 'tAnswer',
				type : 'string'
			},{
				name : 'tAnswerType',
				type : 'string'
			},{
				name : 'isRight',
				type : 'string'
			},{
				name : 'isContainQuestion',
				type : 'string'
			},{
				name : 'taskDate',
				type : 'string'
			},{
				name : 'tk',
				type : 'object'
			}]);

	var reader = new Ext.data.JsonReader({
				idProperty : 'id',
				root : 'data',
				messageProperty : 'message',
				totalProperty : 'total'
			}, TaskResult);

	var writer = new Ext.data.JsonWriter({
				writeAllFields : true
			});

	var proxy = new Ext.data.HttpProxy({
				api : {
					read : 'robot-cat!results.action?taskId='+cfg.taskId
				},
				listeners : {
					write : function(proxy, action, result, res, rs) {
						if (res.message) {
							App.setAlert(true, res.message);
						}
					},
					exception : function(proxy, type, action, options, res) {
						if(res.responseText){
							var r = eval("s="+res.responseText);
							App.setAlert(false,r.message);
						}else{
							if(res.message && res.message=='logout'){
								redirectLogin();
							}
							App.setAlert(false, res.message);
						}
					}
				}
			});

	var store = new Ext.data.Store({
				proxy : proxy,
				reader : reader,
				writer : writer,
				autoLoad : {params:{start: 0, limit: pageSize}},
				autoSave : true
			});
	
	this.getSelectionModel().on('selectionchange', function(sm) {
			//grid.removeBtn.setDisabled(sm.getCount() < 1);
	});

	this.columns = [{
			header : '标准问',
			dataIndex : 'tk',
			width : 150,
			renderer : function(v){
				if(v){
					return v.question;
				}
				return "";
			}
		},{
			header : '测试问题',
			dataIndex : 'tk',
			width : 150,
			renderer : function(v){
				if(v){
					return v.testQuestion;
				}
				return "";
			}
		},{
			header : '测试答案',
			dataIndex : 'tk',
			renderer : function(v){
				if(v && v.answer){
					var answer = v.answer;
					answer = answer.replace(new RegExp("<","gm"),"&lt;");
					answer = answer.replace(new RegExp(">","gm"),"&gt;");
					answer = answer.replace(new RegExp("\n","gm"),"<br/>");
					return answer;
				}
				return "";
			}
		},{
			header : '触发问题',
			dataIndex : 'tQuestion',
			width : 150
		},{
			header : '实际答案',
			dataIndex : 'tAnswer',
			renderer : function(v){
				if(v){
					v = v.replace(new RegExp("<","gm"),"&lt;");
					v = v.replace(new RegExp(">","gm"),"&gt;");
					v = v.replace(new RegExp("\n","gm"),"<br/>");
					return v;
				}
				return "";
			}
		},{
			header : '回答类型',
			dataIndex : 'tAnswerType',
			width : 50
		},{
			header : '是否正确',
			dataIndex : 'isRight',
			width : 50,
			sortable : true
		},{
			header : '是否包含',
			dataIndex : 'isContainQuestion',
			width : 60,
			sortable : true
		},{
			header : '测试日期',
			dataIndex : 'taskDate',
			width : 100
		}];
	
	var pagingToolbar = new Ext.PagingToolbar({
			store : store,
			displayInfo : true,
			pageSize : pageSize,
			prependButtons : true,
			beforePageText : '第',
			afterPageText : '页，共{0}页',
			displayMsg : '第{0}到{1}条记录，共{2}条',
			emptyMsg : "没有记录"
		});
		
		
	var editor = new Ext.task.FormRowEditor({
		title : '测试结果详情',
		width: 600,
		height: 550,
		store: store,
        items: [{
            xtype:'textfield',
            fieldLabel: '标准问题',
            name: 'tk[\'question\']',
            anchor:'100%',
            allowBlank : false
        },{
            xtype:'StarHtmleditor',
            fieldLabel: '答案',
            name: 'tAnswer',
            anchor:'100% -250',
            allowBlank : false
        }]
	});
	
	TaskResultGrid.superclass.constructor.call(this, {
		store : store,
		region : 'center',
		border : false,
		viewConfig: {
	        forceFit: true,
	        markDirty: true
		},
		bbar : pagingToolbar,
		listeners : {
			rowdblclick : function(grid, index, e) {
				editor.record = grid.getSelectionModel().getSelected();
				editor.show();
			}
		}
	});
}

Ext.extend(TaskResultGrid, Ext.grid.GridPanel, {
});

		
		