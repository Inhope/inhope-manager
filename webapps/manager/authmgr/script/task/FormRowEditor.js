Ext.ns('Ext.task');

function eachField(target, fn) {
	var retFn = true;
	if (target.items) {
		target.items.each(function(item) {
			if (item instanceof Ext.form.Field) {
				if (!fn(item)) {
					retFn = false;
					return;
				}
			} else {
				if (!eachField(item, fn)) {
					retFn = false;
					return;
				}
			}
		}, target);
	}
	return retFn;
}

var textField =  new Ext.form.TextField({allowBlank : false});
var customPropertiesColumns =  new Ext .grid.ColumnModel([
                    {header: '参数名', width: 50, sortable: true, dataIndex: 'properties_key', editor: textField},
                    {header: '参数值', width: 50, sortable: true, dataIndex: 'properties_value', editor: textField},
                ]);
var writer = new Ext.data.JsonWriter({
    encode: true,
    writeAllFields: false
});

Ext.task.FormRowEditor = function(cfg) {

	var editor = this;
	var store = cfg.store;

	this.record = cfg.record;
	
	
	this.customPropertiesEditor = new Ext.grid.EditorGridPanel({// 创建Grid表格组件
		frame : false,// 渲染表格面板
		autoScroll : true,// 当数据查过表格宽度时，显示滚动条
		viewConfig : {// 自动充满表格
			autoFill : true
		},
		height : 110,
		region : 'center',
		border : true,
		tbar : [{
            text: '添加',
            iconCls : 'icon-add',
            handler: function(btn, ev) {
                var u = new editor.customPropertiesEditor.store.recordType({
                	properties_key : '',
                	properties_value: ''
                });
                editor.record.dirty = true;
                editor.customPropertiesEditor.stopEditing();
                editor.customPropertiesEditor.store.insert(0, u);
                editor.customPropertiesEditor.startEditing(0, 0);
                
//            	if(editor.customPropertiesEditor.store.getCount()<7){
//				    editor.setHeight(editor.getHeight()+20);
//	                editor.customPropertiesEditor.setHeight(editor.customPropertiesEditor.getHeight()+20);
//	                editor.customFieldSet.setHeight(editor.customFieldSet.getHeight()+20);
//	                editor.customFieldSet.items.items[0].setHeight(editor.customFieldSet.items.items[0].getHeight()+20);
//				}
             
                editor.customFieldSet.doLayout();
                editor.customPropertiesEditor.doLayout();
                editor.doLayout();
                
            },
            scope: this
        }, '-', {
        	text: '删除',
            iconCls : 'icon-delete',
            handler: function(btn, ev) {
                var index = editor.customPropertiesEditor.getSelectionModel().getSelectedCell();
                if (!index) {
                    return false;
                }
                editor.record.dirty = true;
                var rec = editor.customPropertiesEditor.store.getAt(index[0]);
                editor.customPropertiesEditor.store.remove(rec);
//                if(editor.customPropertiesEditor.store.getCount()<7 &&editor.customPropertiesEditor.store.getCount()>1){
//                	   editor.setHeight(editor.getHeight()-20);
//                       editor.customPropertiesEditor.setHeight(editor.customPropertiesEditor.getHeight()-20);
//                       editor.customFieldSet.setHeight(editor.customFieldSet.getHeight()-20);
//                       editor.customFieldSet.items.items[0].setHeight(editor.customFieldSet.items.items[0].getHeight()-20);
//                }
             
                editor.customFieldSet.doLayout();
                editor.customPropertiesEditor.doLayout();
                editor.doLayout();
            },
            scope: this
        }],
        cm:customPropertiesColumns,
        store: new Ext.data.JsonStore({
            root: 'propertiesDatas',
            idProperty: 'properties_key',
            fields: ['properties_key','properties_value'],
            listeners : {
				beforewrite : function(store, data, rs) {
					return false;
				}
			},
			writer : writer
        }),
        listeners : {
			afteredit:function(){
	        	editor.record.dirty = true;
	        }
		}
        
	});
	
	
	this.customFieldSet =  new Ext.FormPanel({
		frame : false,
		border:false,
		bodyStyle:'border-width:0;padding:5px;background-color:#d0def0;',
		labelAlign : cfg.labelAlign ? cfg.labelAlign : 'top',
		items : [{
        	xtype : 'fieldset',
            title: '自定义参数',
            bodyStyle:'border-width:0;padding:0px;background-color:#d0def0;',
            height: 150,
            items:[this.customPropertiesEditor]
        }],
		region : 'center',
		height : 200,
		buttonAlign : 'center',
		trackResetOnLoad : true
	});
	
	Ext.task.FormRowEditor.superclass.constructor.call(this, {
		title : cfg.title,
		iconCls : 'icon-user-login',
		height : cfg.height,
		width : cfg.width,
		resizable : true,
		plain : true,
		modal : true,
		autoScroll : false,
		closeAction : 'hide',
		layout : 'fit',
		tbar : [ {
			iconCls : 'icon-status-ok',
			text : '保存',
			handler : function() {
				if (cfg.handler) {
					cfg.handler();
					return;
				}
				// 如果这单条数据存在 就设置这单条数据开始修改
				if (store.getById(editor.record.id)) {
					editor.record.beginEdit();
				}
				var valid = eachField(editor, function(item) {
					if (!item.validate()) {
						return false;
					}
					var value = item.getValue();
					if (value) {
						editor.record.set(item.name, value);
					}
					return true;
				});
				if (!valid) {
					editor.record.cancelEdit();
					return;
				}

				if (store.getById(editor.record.id)) {
					console.log('record', editor.record);
					editor.record.endEdit();
					editor.record.commit();
				} else {
					store.insert(0, editor.record);
				}
				editor.hide();
			},
			scope : this
		}, '-', {
			text : '取消',
			iconCls : 'icon-status-cancel',
			handler : function() {
				editor.hide();
			},
			scope : this
		} ],
		items : [ new Ext.Panel({
			layout : 'border',
			border : false,
			items : [ new Ext.FormPanel({
				frame : false,
				border:false,
				bodyStyle:'border-width:0;padding:5px;background-color:#d0def0;',
				labelAlign : cfg.labelAlign ? cfg.labelAlign : 'top',
				name : 'formpanel',
				items : cfg.items,
				region : 'north',
				height : 390,
				buttonAlign : 'center',
				trackResetOnLoad : true
			}),editor.customFieldSet]
		}) ],
		listeners : {
			beforeshow : function() {
				eachField(editor, function(item) {
					item.reset();
					var v;
					if (editor.record.get)
						v = editor.record.get(item.name);
					else
						v = editor.record[item.name];
					if (v)
						item.setValue(v);
					return true;
				});
				if (cfg.beforeshow) {
					cfg.beforeshow();
				}
			}
		}
	});
};

Ext.extend(Ext.task.FormRowEditor, Ext.Window, {
	getFieldByName : function(name) {
		var targetField = null;
		eachField(this, function(field) {
			if (field.getName() == name) {
				targetField = field;
				return false;
			}
		});
		return targetField;
	}
});