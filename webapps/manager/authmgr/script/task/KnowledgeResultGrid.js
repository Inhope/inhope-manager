
KnowledgeResultGrid = function(cfg) {
	var self = this;
	var pageSize = 50;
	var grid = this;
	var taskId = cfg.taskId;
	this.taskId = taskId;
	
	var lie = [/*{
				name : 'taskId',
				type : 'string'
			},{
				name : 'tkId',
				type : 'string'
			},*/{
				name : 'tQuestion',
				type : 'string'
			},{
				name : 'tAnswer',
				type : 'string'
			},{
				name : 'tAnswerType',
				type : 'string'
			},{
				name : 'isRight',
				type : 'string'
			},{
				name : 'isContainQuestion',
				type : 'string'
			},{
				name : 'taskDate',
				type : 'string'
			},{
				name : 'question',
				type : 'string'
			},{
				name : 'testQuestion',
				type : 'string'
			},{
				name : 'answer',
				type : 'string'
			},{
				name : 'taskTime',
				type : 'string'
			},{
				name : 'resultXml',
				type : 'string'
			},{
				name : 'matchingExpressionOnRuntime',
				type : 'string'
			},{
				name : 'casesColumn',
				type : 'string'
			}];
	
//	if(cfg.cmItems){
//		for(var i=0;i<cfg.cmItems.length;i++){
//			lie.push(
//				{
//					name : cfg.cmItems[i].dataIndex,
//					type : 'string'
//				}
//			);
//		}
//	}
	
	
	
	var TaskResult = Ext.data.Record.create(lie);

	var reader = new Ext.data.JsonReader({
				idProperty : 'id',
				root : 'data',
				messageProperty : 'message',
				totalProperty : 'total',
				remoteSort:true
			}, TaskResult);

	var writer = new Ext.data.JsonWriter({
				writeAllFields : true
			});

	var proxy = new Ext.data.HttpProxy({
				api : {
					read : 'task!results.action?taskId='+cfg.taskId
				},
				listeners : {
					write : function(proxy, action, result, res, rs) {
						
						if (res.message) {
							App.setAlert(true, res.message);
						}
					},
					
					
					exception : function(proxy, type, action, options, res) {
						if(res.responseText){
							var r = eval("s="+res.responseText);
							if(r.message && r.message=='logout'){
								redirectLogin();
							}
							App.setAlert(false,r.message);
						}else{
							if(res.message && res.message=='logout'){
								redirectLogin();
							}
							App.setAlert(false, res.message);
						}
					}
				}
			});

	var store = new Ext.data.Store({
				proxy : proxy,
				reader : reader,
				writer : writer,
				autoLoad : {params:{start: 0, limit: pageSize}},
				autoSave : true
			});
//	store.setDefaultSort('isRight', 'desc');
	store.remoteSort=true; 
		
	this.getSelectionModel().on('selectionchange', function(sm) {
			//grid.removeBtn.setDisabled(sm.getCount() < 1);
	});
	
	var taskDateField = new Ext.form.DateField({
					name : "taskDate",
					xtype:'datefield',
					editable : false,
					format: 'Y-m-d',
					selectOnFocus:true, 
					editable:false,
					value:new Date(),
					validator : function(v) {
						if (v) {
							if (v.length<1){
								App.setAlert(true, '时间不能为空!');
								return false;
							}
							return true;
						}
						return false;
					},
					width : 100
				});
	this.taskDateField = taskDateField;				
	this.tbar = [
			'选择时间: ',' ',taskDateField, 
            {
				text : '搜 索',
				iconCls : 'icon-grid',
				handler : function() {
					self.search();
				}
			},'-',{
				iconCls : 'icon-grid',
				text : '导出03版',
				handler : function() {
					self.exportData('xls');
				}
		},'-',{
				iconCls : 'icon-grid',
				text : '导出07版',
				handler : function() {
					self.exportData('xlsx');
				}
		}
    ];
	
	
	
	var cmItems = [];
	// 固定列部分
	cmItems.push({
		header : '测试问题',
		dataIndex : 'testQuestion',
		width : 150
	});
	
	cmItems.push({
		header : '返回xml',
		dataIndex : 'resultXml',
		width : 150,
		renderer:function(v){
			v = v.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/'/g, "&apos;").replace(/"/g, "&quot;")
			return '<span title="'+v+'">'+ v +'</span>';
		}
	});
	
	cmItems.push({
		header : '执行时匹配表达式',
		dataIndex : 'matchingExpressionOnRuntime',
		width : 100,
		renderer:function(v){
			return '<span title="'+v+'">'+ v +'</span>';
		}
	});
	
	cmItems.push({
		header : '是否正确',
		dataIndex : 'isRight',
		width : 50,
		renderer:function(v){
			if(v){
				if(v=='true'){
					return '正确';
				}else if(v=='exception'){
					return '异常';
				}
				return '错误';
			}
			return '';
		},
		sortable : true,
		remoteSort: true
	});
	cmItems.push({
		header : '测试时间',
		dataIndex : 'taskTime',
		width : 150
	});	

	this.columns = cmItems;
	
	var pagingToolbar = new Ext.PagingToolbar({
		store : store,
		displayInfo : true,
		pageSize : pageSize,
		prependButtons : true,
		beforePageText : '第',
		afterPageText : '页，共{0}页',
		displayMsg : '第{0}到{1}条记录，共{2}条',
		emptyMsg : "没有记录"
	});
	
	var editor = new Ext.task.KnowledgeResultPanel({
		title : '测试结果详情',
		width: 1000,
		height: 550,
		store: store,
        items: [{
        	layout:'column',
            items:[{
            	columnWidth:.48,
            	layout: 'form',
            	style:{                     
            		float:'left'
            	}, 
	            items :  {
	            	xtype : 'textfield',
			        name: 'testQuestion',
			        fieldLabel:'测试问题',
			        anchor:'90%',
			        readOnly : true
			    }
	        },{
            	columnWidth:.48,
            	layout: 'form',
            	style:{                     
            		float:'right'
            	}, 
	            items :  {
	            	xtype : 'textfield',
			        name: 'matchingExpressionOnRuntime',
			        fieldLabel:'执行时匹配表达式',
			        anchor:'90%',
			        readOnly : true
			    }
	        }]
        },{
        	layout:'column',
            items:[{
            	columnWidth:.48,
            	layout: 'form',
            	style:{                     
            		float:'left'
            	}, 
	            items :  {
	            	xtype : 'textarea',
			        name: 'resultXml',
			        fieldLabel:'返回的xml',
			        width:'460px',
			        height:'300px',
			        readOnly : true
			    }
	        },{
            	columnWidth:.48,
            	layout: 'form',
            	style:{                     
            		float:'right'
            	},
	            items :  {
	            	xtype : 'textarea',
			        name: 'casesColumn',
			        fieldLabel:'用例里的列',
			        width:'460px',
			        height:'300px',
			        readOnly : true
			    }
	        }]
        },{
        	layout:'column',
            items:[{
            	columnWidth:.3,
            	layout: 'form',
	            items :  {
	            	xtype : 'combo',
			        name: 'isRight',
			        fieldLabel:'是否正确',
			        store:new Ext.data.SimpleStore({
                       fields: ['value', 'text'],
                       data: [
                       	   ['true','正确'],
                       	   ['false','错误'],
                       	   ['exception','异常']
                       ]
                   }),
                   displayField: 'text',
                   valueField: 'value',
                   width:150,
                   readOnly : true,
                   mode: 'local'
			    }
	        }]
        }]
	});
	
	
	KnowledgeResultGrid.superclass.constructor.call(this, {
		store : store,
		region : 'center',
		border : false,
		viewConfig: {
	        forceFit: true,
	        markDirty: true
		},
		bbar : pagingToolbar,
		listeners : {
			rowdblclick : function(grid, index, e) {
				editor.record = grid.getSelectionModel().getSelected();
				editor.show();
			}
		}
		
		
		
	});
	
	this.getPageSize = function() {
		return pageSize;
	};
}

Ext.extend(KnowledgeResultGrid, Ext.grid.GridPanel, {
search : function(){
	var taskDate = this.taskDateField.getValue();
	if(!taskDate ||taskDate==''){
		alert("请选择查询时间！");
	}else{
		taskDate = taskDate.format('Y-m-d');
	}
	this.getStore().load({
		params : {
			start : 0,
			limit : this.getPageSize(),
			'taskDate' : taskDate
		}
	});
},
exportData : function(v){
	var taskDate = this.taskDateField.getValue();
	if(!taskDate ||taskDate==''){
		alert("请选择查询时间！");
		return;
	}else{
		taskDate = taskDate.format('Y-m-d');
	}
	var url = "task!results.action?taskId="+this.taskId+"&taskDate="+taskDate+"&export=export&format="+v;
	window.open(url);
}

});

		
		