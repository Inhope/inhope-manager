/*
 * ! Ext JS Library 3.1.0 Copyright(c) 2006-2009 Ext JS, LLC licensing@extjs.com
 * http://www.extjs.com/license
 */
NavMenu = function(config) {
	NavMenu.superclass.constructor.call(this, Ext.apply({
						region : 'west',
						split : true,
						width : 160,
						minSize : 175,
						maxSize : 400,
						collapsible : true,
						margins : '0 0 5 5',
						cmargins : '0 5 5 5',
						rootVisible : false,
						lines : false,
						autoScroll : true,
						root : new Ext.tree.TreeNode('rootitem'),
						collapseFirst : false,
						listeners : {
							click : function(node) {
								if (node) {
									this.fireEvent('itemselect',node.attributes.bean);
								}
							}
						}
					}, config));

	this.feeds = this.root;
	this.getSelectionModel().on({
				'beforeselect' : function(sm, node) {
					return node.isLeaf();
				},
				// 'selectionchange' : function(sm, node) {
				// if (node) {
				// this.fireEvent('feedselect', node.attributes.bean);
				// }
				// },
				scope : this
			});

	this.addEvents({
				itemselect : true
			});
};

Ext.extend(NavMenu, Ext.tree.TreePanel, {

			showWindow : function(btn, server) {
				if (!this.win) {
					this.win = new FeedWindow(this);
					this.win.on('validfeed', this.addItem, this);
				}
				this.win.show(btn, server);
			},

			selectFirst : function() {
				if (this.root.childNodes && this.root.childNodes.length > 0) {
					this.root.childNodes[0].select();
				}
			},

			selectById : function(id) {
				if (!id) {
					this.getSelectionModel().clearSelections();
					return;
				}
				if (this.root.childNodes && this.root.childNodes.length > 0) {
					Ext.each(this.root.childNodes, function(node, i) {
								if (node && node.id == id) {
									node.select();
									return false;
								}
							});
				}
			},

			findServerNode : function(id) {
				return this.getNodeById(id);
			},

			addItem : function(attrs, inactive, preventAnim) {
				var exists = this.getNodeById(attrs.id);
				if (exists) {
					exists.attributes.text = attrs.id;
					exists.attributes.bean = attrs;
					exists.setText(exists.attributes.text);
					if (!inactive) {
						exists.select();
						exists.ui.highlight();
					}
					return;
				}
				var rawAttrs = {};
				Ext.apply(rawAttrs, attrs);
				Ext.apply(attrs, {
							iconCls : 'feed-icon',
							leaf : true,
							cls : 'feed',
							id : attrs.id,
							text : attrs.text ? attrs.text : attrs.id,
							bean : rawAttrs
						});
				var node = new Ext.tree.TreeNode(attrs);
				this.feeds.appendChild(node);
				if (!inactive) {
					if (!preventAnim) {
						Ext.fly(node.ui.elNode).slideIn('l', {
									callback : node.select,
									scope : node,
									duration : .4
								});
					} else {
						node.select();
					}
				}
				return node;
			},

			// prevent the default context menu when you miss the node
			afterRender : function() {
				NavMenu.superclass.afterRender.call(this);
				this.el.on('contextmenu', function(e) {
							e.preventDefault();
						});
			}
		});
//延迟创建一个名为 navMenu类型的appNavMenu
Ext.reg('appNavMenu', NavMenu);