
TaskGrid = function() {
	var pageSize = 50;
	var grid = this;
	var task_url = http_get('property!get.action?key=robot.context_path');
	var Task = Ext.data.Record.create([{
				name : 'id',
				type : 'string'
			}, {
				name : 'taskName',
				type : 'string'
			}, {
				name : 'adminName',
				type : 'string'
			}, {
				name : 'createTime',
				type : 'date',
				dateFormat:'Y-m-d H:i:s'
			}, {
				name : 'taskUrl',
				type : 'string'
			}, {
				name : 'location',
				type : 'string'
			}, {
				name : 'platform',
				type : 'string'
			}, {
				name : 'itemType',
				type : 'string'
			}, {
				name : 'executeTime',
				type : 'date',
				dateFormat:'Y-m-d H:i:s'
			}, {
				name : 'timer',
				type : 'string'
			}, {
				name : 'timeInterval',
				type : 'string'
			}, {
				name : 'fileId',
				type : 'string'
			}, {
				name : 'state',
				type : 'string'
			}, {
				name : 'emails',
				type : 'string'
			}, {
				name : 'matchingExpression',
				type : 'string'
			},{
				name : 'brand',
				type : 'string'
			},{
				name : 'customParams',
				type : 'object'
			}]);

	var reader = new Ext.data.JsonReader({
				idProperty : 'id',
				root : 'data',
				messageProperty : 'message',
				totalProperty : 'total'
			}, Task);

	var writer = new Ext.data.JsonWriter({
				writeAllFields : true
			});

	var proxy = new Ext.data.HttpProxy({
				api : {
					read : 'task!list.action',
					create : 'task!add.action',
					update : 'task!update.action',
					destroy : 'task!del.action'
				},
				listeners : {
					beforewrite : function(proxy, action,rs,params) {
						if(action=='update' || action=='create'){
							var customParams = {};
							var datas = rs['data'];
							editor.customPropertiesEditor.store.each(function (record){
								if(record.get('properties_key') && record.get('properties_value')){
									customParams[record.get('properties_key')] = record.get('properties_value');
								}
							});
							var executeTime = editor.getFieldByName('executeTime').getRawValue();
							var dataObj = Ext.decode(params['data']);
							dataObj['customParams'] = customParams;
							if(executeTime == ""){
								dataObj['executeTime'] = null;
							}else{
								dataObj['executeTime'] = executeTime;
							}
							dataObj.createTime = Ext.util.Format.date(dataObj.createTime,'Y-m-d H:i:s');
							params['data'] = Ext.encode(dataObj);
							rs['data']['customParams']=customParams;
						}
					},
					write : function(proxy, action, result, res, rs) {
						if (res.message) {
							Dashboard.setAlert(res.message);
						}
					},
					exception : function(proxy, type, action, options, res) {
						if(res.responseText){
							var r = eval("s="+res.responseText);
							if(r.message && r.message=='logout'){
								redirectLogin();
							}
							Dashboard.setAlert(r.message);
						}else{
							if(res.message && res.message=='logout'){
								redirectLogin();
							}
							Dashboard.setAlert(res.message);
						}
					}
				}
			});

	var store = new Ext.data.Store({
				proxy : proxy,
				reader : reader,
				writer : writer,
				autoLoad : {params:{start: 0, limit: pageSize}},
				autoSave : true
			});
	
			
	var uploadWindow = new Ext.task.ExcelUploadWindow({
		url:'task!_import.action',title:'上传测试用例，可上传07,03版本的excel。',
		updatePath:function(data) {
			if(data.success){
				//将缓存里的id 放入文件fileId
				editor.getFieldByName('fileId').setValue(data.message);
			}else{
				Dashboard.setAlert(data.message);
			}
		}
	});
	
	
	
	var height = 610;
	var editor = new Ext.task.FormRowEditor({
		title : '任务信息',
		width: 600,
		height: height,
		store: store,
		beforeshow : function(){
			var customParams = editor.record.data.customParams;
			editor.customPropertiesEditor.store.removeAll(true);
			if(customParams && customParams !="" && customParams!={}){
				var customPropertieArray = new Array();
				for (var custom in customParams) {
					var cObj = {};
					cObj['properties_key'] = custom;
					cObj['properties_value'] = customParams[custom];
					customPropertieArray.push(cObj);
				}
				var customPropertieDatas = {'propertiesDatas':customPropertieArray};
				editor.customPropertiesEditor.store.loadData(customPropertieDatas);
			}
		    editor.customFieldSet.doLayout();
            editor.customPropertiesEditor.doLayout();
            editor.items.items[0].doLayout();
            editor.doLayout();
		},
        items: [{
        	xtype: 'hidden',
            name:'customParams'
        },{
            xtype:'textfield',
            fieldLabel: '任务名',
            name: 'taskName',
            anchor:'100%',
            allowBlank : false
        },{
        	layout : 'column',
        	border:false,
        	bodyStyle:'background-color:#d0def0;',
        	defaults: {
        		frame : false,
				border:false,
				bodyStyle:'border-width:0;padding:0px;background-color:#d0def0;'},
        	items : [{
        		columnWidth : .65,
            	layout : 'form',
            	items : [{
            		xtype:'textfield',
                    fieldLabel: '测试地址',
                    name: 'taskUrl',
                    anchor:'99%',
                    allowBlank : false,
                    value:task_url
            	}]
        	},{
	        	columnWidth:.35,
            	layout: 'form',
			    items:[{
			       	   xtype:"combo",
	                   fieldLabel: '项目类型',
	                   name: 'itemType',
	                   anchor:'99%',
	                   allowBlank : false,
	                   store: new Ext.data.SimpleStore({
	                       fields: ['value', 'text'],
	                       data: [
	                           ['ask', 'ask接口'],
		                       ['ask.action', 'ask.action接口'],
		                       ['weixin', '微信接口']
	                       ]
	                   }),
					   listeners :{
						   select: function(v) {
								if (v.value=='ask') {
									var taskUrl = editor.getFieldByName('taskUrl').value;
									var task_url = taskUrl.replace('http://', "");
									editor.getFieldByName('taskUrl').setValue("http://"+ task_url.substring(0,task_url.indexOf('/'))+'/robot/ask');
									return true;
								}else if(v.value=='ask.action'){
									var taskUrl = editor.getFieldByName('taskUrl').value;
									var task_url = taskUrl.replace('http://', "");
									editor.getFieldByName('taskUrl').setValue("http://"+ task_url.substring(0,task_url.indexOf('/'))+'/robot/ask.action');
									return true;
								}else if(v.value=='weixin'){
									var taskUrl = editor.getFieldByName('taskUrl').value;
									var task_url = taskUrl.replace('http://', "");
									editor.getFieldByName('taskUrl').setValue("http://"+ task_url.substring(0,task_url.indexOf('/'))+'/robot/weixin/debugger-bot');
									return true;
								}
						   }
					   },
	                   displayField: 'text',
	                   valueField: 'value',
	                   triggerAction:'all',
	                   mode: 'local'
			      }]
        	}]
            
        },{
            xtype:'textfield',
            fieldLabel: '邮箱地址(多个请用半角逗号分割)',
            name: 'emails',
            anchor:'100%',
            allowBlank : true
        },{
            xtype: "textarea",
            fieldLabel: '匹配表达式(多个请用半角逗号分割)',
            name: "matchingExpression",
            allowBlank : false,
            anchor:'100%',
        },{
	         layout:'column',
	         border:false,
	         bodyStyle:'background-color:#d0def0;',
	         defaults: {
	        		frame : false,
					border:false,
					bodyStyle:'border-width:0;padding:0px;background-color:#d0def0;'},
	         items:[{
	            	columnWidth:.25,
	            	layout: 'form',
		            items :  new ClearableDateTimeField({
		            	anchor:'98%',
						fieldLabel:'执行时间',
						name : "executeTime",
						editable : false,
						validator : function(v) {
							if (v=="") {
								editor.record.dirty = true;
							}
							return true;
						},
					})
		        },{
	            	columnWidth:.25,
	            	layout: 'form',
		            items : {
			            	xtype : 'textfield',
					        name: 'timeInterval',
					        anchor:'98%',
					        fieldLabel:'间隔时长',
					        readOnly : false,
					        allowBlank : true,
					        
					        validator : function(v) {
								if (v) {
									if (isNaN(v)){
										Dashboard.setAlert('间隔时长必须为数字!');
										return false;
									}
									return true;
								}
								return true;
							},
				        }
		        },{
	            	columnWidth:.25,
	            	layout: 'form',
			        items:[{
				       	   xtype:"combo",
		                   fieldLabel: '单位类型',
		                   name: 'timer',
		                   anchor : '98%',
		                   store:new Ext.data.SimpleStore({
		                       fields: ['value', 'text'],
		                       data: [
		                           ['everysec','秒'],
		                           ['everymin','分'],
		                           ['everyday','天'],
					        	   ['everymonth','月']
		                       ]
		                   }),
		                   displayField: 'text',
		                   valueField: 'value',
		                   triggerAction:'all',
		                   mode: 'local'
				      }]
		                  
		        },{
	            	columnWidth:.25,
	            	layout: 'form',
		            items : {
			            	xtype:'button',
			            	align:'right',
			            	anchor:'98%',
			            	style : 'margin-top:18px',
				            text: '导入测试用例',
				            handler : function(){
				            	uploadWindow.show();
			            	}
		            	}
		        },{
	            	layout: 'form',
		            items : {
			            	xtype:'hidden',
			            	id:'fileId',
			            	name:'fileId'
		            	}
		     }]
            
        },{
        	xtype : 'fieldset',
            title: '固定参数',
            height: 90,
            items: [{
            	layout:'column',
            	bodyStyle:'background-color:#d0def0;',
            	border : false,
   	         	defaults: {
   	        		frame : false,
   					border:false,
   					bodyStyle:'border-width:0;padding:0px;background-color:#d0def0;'
   				},
                items:[{
                	columnWidth:.333,
                	layout: 'form',
    	            items : [{
    	            	xtype : 'textfield',
    			        name: 'location',
    			        fieldLabel : '城市',
    			        readOnly : false,
    			        allowBlank : true,
    			        anchor : '98%',
    			        tabIndex : 2
    		        }]
    	        },{
                	columnWidth:.333,
                	layout: 'form',
    	            items : [{
    		            	xtype : 'combo',
    				        fieldLabel:'平台',
    				        name: 'platform',
    				        store: new Ext.data.SimpleStore({
    	                       fields: ['value', 'text'],
    	                       data: [
    	                           ['sms', 'SMS'],
    		                       ['web', 'Web'],
    		                       ['fetion', 'Fetion'],
    		                       ['wap', 'Wap'],
    		                       ['tsina', 'TSina'],
    		                       ['gtalk', 'Gtalk'],
    		                       ['msn', 'MSN'],
    		                       ['qq', 'QQ'],
    		                       ['android', 'Android'],
    		                       ['ios', 'iOs'],
    		                       ['tv', 'TV'],
    		                       ['android.oem', 'Android.OEM'],
    		                       ['tv.s', 'TV.S'],
    		                       ['tqq', 'TQQ'],
    		                       ['tv.lge', 'LG_E'],
    		                       ['tv.lga', 'LG_A'],
    		                       ['android.oemd', 'Android.OEMD'],
    		                       ['weixin', 'WeiXin'],
    		                       ['yixin', 'YiXin'],
    		                       ['tv.a', 'TV.A'],
    		                       ['tv.s/52', 'TV.S/52'],
    		                       ['tv.s/k82', 'TV.S/K82'],
    		                       ['wbfan', 'wbfan'],
    		                       ['alipay', 'Alipay']
    	                       ]
    	                   }),
    	                   displayField: 'text',
    	                   valueField: 'value',
    	                   triggerAction:'all',
    	                   mode:'local',
    	                   anchor : '98%'
    			        }]
    	        },{
                	columnWidth:.333,
                	layout: 'form',
    	            items : {
		            	xtype : 'textfield',
				        name: 'brand',
				        fieldLabel:'品牌',
				        readOnly : false,
				        allowBlank : true,
				        anchor : '98%'
			        }
    	        }]
            }]
        }
        ]
	});
	
	this.getSelectionModel().on('selectionchange', function(sm) {
			grid.removeBtn.setDisabled(sm.getCount() < 1);
			grid.taskResultBtn.setDisabled(sm.getCount() < 1);
	});
	
	this.tbar = [{
			iconCls : 'icon-user-add',
			text : '添加任务信息',
			handler : function() {
				editor.record = new Task({createTime:new Date()});
				editor.show();
			}
		},'-', {
			ref : '../removeBtn',
			iconCls : 'icon-user-delete',
			text : '删除任务信息',
			disabled : true,
			handler : function() {
				var s = grid.getSelectionModel().getSelections();
				Ext.MessageBox.confirm('删除任务信息确认','您确认删除选中的任务信息？', function(btn, text){
				    if (btn == 'yes'){
				       for (var i = 0, r; r = s[i]; i++) {
							store.remove(r);
						}
				    }
				});
				
			}
	},'-', {
			ref : '../taskResultBtn',
			iconCls : 'icon-grid',
			text : '测试结果',
			disabled : true,
			handler : function() {
				var s = grid.getSelectionModel().getSelections();
				if(s&&s.length>0){
					var r = s[0];
					var columns = r.get('matchingExpression');
					var cfg = {taskId:r.get('id'),taskName:r.get('taskName')};
					var tabId = 'taskResult_'+cfg.taskId;
					var tab = Dashboard.navPanel.get(tabId);
					var title = cfg.taskName+ '->'+'测试结果';
					if (!tab) {
						var krg = new KnowledgeResultGrid(cfg)
						tab = Dashboard.navPanel.add({
							id : tabId,
							layout : 'fit',
							title : title,
							items : krg,
							closable : true
						});
						tab.contentPanel = krg;
						tab.show();
					} else {
						Dashboard.navPanel.activate(tabId);
						if (title)
							tab.setTitle(title)
					}
				}
			}
	}];
	this.columns = [{
			header : '任务名',
			dataIndex : 'taskName',
			width : 150,
			sortable : true
		}, {
			header : '管理员',
			dataIndex : 'adminName',
			width : 150,
			sortable : true
		}, {
			header : '执行时间',
			dataIndex : 'executeTime',
			width : 150,
			sortable : true,
			xtype : 'datecolumn',
			format : 'Y-m-d H:i:s'
		}, {
			header : '测试周期',
			dataIndex : 'timer',
			width : 150,
			sortable : true,
			renderer:function(v){
				if(v=='promptly'){
					return '只执行一次';
				}else if(v=='everysec'){
					return '按秒监控';
				}else if(v=='everymin'){
					return '按分监控';
				}else if(v=='everyday'){
					return '按天监控';
				}else if(v=='everymonth'){
					return '按月监控';
				}
			}
		}, {
			header : '状态',
			dataIndex : 'state',
			width : 150,
			sortable : true,
			renderer:function(v){
				if(v){
					return "<font color='red'>"+v+"</font>";
				}
				return "";
			}
		}, {
			header : '创建时间',
			dataIndex : 'createTime',
			width : 150,
			sortable : true,
			xtype : 'datecolumn',
			format : 'Y-m-d H:i:s'
		}];
	
	var pagingToolbar = new Ext.PagingToolbar({
			store : store,
			displayInfo : true,
			pageSize : pageSize,
			prependButtons : true,
			beforePageText : '第',
			afterPageText : '页，共{0}页',
			displayMsg : '第{0}到{1}条记录，共{2}条',
			emptyMsg : "没有记录"
		});
	
	TaskGrid.superclass.constructor.call(this, {
		store : store,
		region : 'center',
		border : false,
		viewConfig: {
	        forceFit: true,
	        markDirty: true
		},
		bbar : pagingToolbar,
		listeners : {
			rowdblclick : function(grid, index, e) {
				editor.record = grid.getSelectionModel().getSelected();
				editor.show();
			}
		}
	});
	
};

Ext.extend(TaskGrid, Ext.grid.GridPanel, {
	
});
