DateFieldEx = Ext.extend(Ext.form.DateField, {

			onTriggerClick : function() {

				DateFieldEx.superclass.onTriggerClick.call(this, arguments);

				if (this.format.indexOf('d') == -1) {
					this.menu.picker.monthPicker.slideIn = function() {
						this.show()
					}
					this.menu.picker.hideMonthPicker = function() {
						this.monthPicker.hide();
						var v = this.activeDate
						if (v) {
							this.setValue(new Date(v))
							this.fireEvent('select', this, this.value);
						}
					}
					this.menu.picker.showMonthPicker()
				}
			}
		})
