Ext.namespace('ux')
Ext.form.TextField.prototype.getSelection = function(element) {
	var domElement = element ? element : this.getEl().dom;
	if (Ext.isIE) {
		var sel = document.selection;
		var range = sel.createRange();
		if (range.parentElement() != domElement)
			return null;
		var bookmark = range.getBookmark();
		var selection = domElement.createTextRange();
		selection.moveToBookmark(bookmark);
		var before = domElement.createTextRange();
		before.collapse(true);
		before.setEndPoint("EndToStart", selection);
		var after = domElement.createTextRange();
		after.setEndPoint("StartToEnd", selection);
		return {
			selectionStart : before.text.length,
			selectionEnd : before.text.length + selection.text.length,
			beforeText : before.text,
			text : selection.text,
			afterText : after.text,
			wholeText : before.text + selection.text + after.text
		}
	} else {
		if (domElement.selectionEnd >= 0 && domElement.selectionStart >= 0) {
			if (domElement.selectionEnd >= domElement.selectionStart) {
				return {
					selectionStart : domElement.selectionStart,
					selectionEnd : domElement.selectionEnd,
					beforeText : domElement.value.substr(0,
							domElement.selectionStart),
					text : domElement.value
							.substr(domElement.selectionStart,
									domElement.selectionEnd
											- domElement.selectionStart),
					afterText : domElement.value
							.substr(domElement.selectionEnd),
					wholeText : domElement.value
				};
			}
		}
	}
	return null;
}
Ext.form.HtmlEditor.prototype.getSelection = function() {
	if (Ext.isIE) {
		var range = this.getDoc().selection.createRange();
		domElement = this.getDoc().body
		var bookmark = range.getBookmark();
		var selection = domElement.createTextRange();
		selection.moveToBookmark(bookmark);
		var before = domElement.createTextRange();
		before.collapse(true);
		before.setEndPoint("EndToStart", selection);
		var after = domElement.createTextRange();
		after.setEndPoint("StartToEnd", selection);

		function tripHTML(html) {
			var str = html
			str = str.replace(/<br>/gi, "\n");
			str = str.replace(/<[^>]+>/g, "");
			str = str.replace('&nbsp;', ' ');
			str = str.replace('&lt;', '<');
			str = str.replace('&gt;', '>');
			return str;
		}

		var beforeText = tripHTML(before.htmlText)
		var afterText = tripHTML(after.htmlText)
		var selText = tripHTML(selection.htmlText)
		return {
			selectionStart : beforeText.length,
			selectionEnd : beforeText.length + selText.length,
			beforeText : beforeText,
			text : selText,
			afterText : afterText,
			wholeText : beforeText + selText + afterText
		}
	} else {
		function getNodeText(node) {
			var text = ''
			var node = node.firstChild
			while (node) {
				if (node.nodeType == 3) {
					text = text + node.nodeValue
				} else if (node.nodeType == 1) {
					if (node.nodeName == 'BR') {
						text = text + '\n'
					} else {
						text = text + getNodeText(node)
					}
				}
				node = node.nextSibling;
			}
			return text;
		}
		function getBeforeNodeText(top, node) {
			var text = ''
			while (node != top) {
				while (node.previousSibling != null) {
					node = node.previousSibling;
					if (node.nodeType == 3) {
						text = node.nodeValue + text
					} else if (node.nodeType == 1) {
						if (node.nodeName == 'BR') {
							text = '\n' + text
						} else {
							text = getNodeText(node) + text
						}
					}
				}
				node = node.parentNode
			}
			return text
		}
		function getAfterNodeText(top, node) {
			var text = ''
			while (node != top) {
				while (node.nextSibling != null) {
					node = node.nextSibling;
					if (node.nodeType == 3) {
						text = text + node.nodeValue
					} else if (node.nodeType == 1) {
						if (node.nodeName == 'BR') {
							text = text + '\n'
						} else {
							text = text + getNodeText(node)
						}
					}
				}
				node = node.parentNode
			}
			return text
		}

		var body = this.getDoc().getElementsByTagName('BODY')[0]
		var sel = this.getWin().getSelection();
		var start = sel.anchorOffset
		var end = sel.focusOffset
		var value, selNode = sel.anchorNode
		if (selNode.nodeType == 3) {
			value = selNode.nodeValue
		} else if (selNode.nodeType == 1) {
			selNode = selNode.childNodes[start]
			start = end = 0
			if (selNode.nodeName == 'BR') {
				value = '\n'
			}
		}
		var beforeNodeText = getBeforeNodeText(body, selNode)
		var afterNodeText = getAfterNodeText(body, selNode)
		if (!value || value == '\n')
			value = ''
		return {
			selectionStart : beforeNodeText.length + start,
			selectionEnd : beforeNodeText.length + end,
			beforeText : beforeNodeText + value.substr(0, start),
			text : value.substr(start, end),
			afterText : value.substr(end) + afterNodeText,
			wholeText : beforeNodeText + value + afterNodeText
		}
	}
}

var _ext_panel_init = Ext.Panel.prototype.initComponent;
var _ext_panel_const = Ext.Panel;
Ext.override(Ext.data.Record, {
			_set_method : Ext.data.Record.prototype.set,
			set : function(key, value) {
				if (!key) {
					return;
				}
				return this._set_method.apply(this, arguments)
			}
		})
Ext.override(Ext.Editor, {
			_realign : Ext.Editor.prototype.realign,
			_initComponent : Ext.Editor.prototype.initComponent,
			_setSize : Ext.Editor.prototype.setSize,
			initComponent : function() {
				this.autoSize = true;
				if (this.field && this.field.getCustomEditorSize) {
					this.getCustomEditorSize = this.field.getCustomEditorSize
				}
				this._initComponent.call(this);
			},
			setSize : function(w, h) {
				if (this.getCustomEditorSize) {
					var sz = this.getCustomEditorSize(this);
					if (sz[0])
						w = sz[0];
					if (sz[1])
						h = sz[1]
				}
				this._setSize.apply(this, [w, h]);
			},
			realign : function(autoSize) {
				if (!this.field.getResizeEl())
					autoSize = false;
				var old = this.boundEl
				if (old) {
					var el = old.findParent('td', 5, true)
					if (el) {
						this.boundEl = el
						this._realign.apply(this, [autoSize])
						this.boundEl = old;
						return;
					}
				}
				this._realign.apply(this, [autoSize])
			}
		});
Ext.override(Ext.Panel, {
// split : true,
		// collapsible : true
		// initComponent : function() {
		// if (this.region && this.region != 'center') {
		// if (this.title == '控制台') {
		// Ext.apply(this.initialConfig, {
		//
		// })
		// }
		// }
		// return _ext_panel_init.call(this);
		// }
		})
Ext.override(Ext.tree.TreePanel, {
	// private
	renderRoot : function() {
		if (this.root && this.root.attributes && !this.root.attributes.lazyLoad) {
			this.renderRootInternal();
		}
	},
	renderRootInternal : function() {
		if (this.__rootRendered)
			return false;
		this.__rootRendered = true;
		this.root.render();
		if (!this.rootVisible) {
			this.root.renderChildren();
		}
	},
	renderRootNode : function() {
		this.renderRootInternal();
	}
})
Ext.form.ComboBox.prototype._doQuery = Ext.form.ComboBox.prototype.doQuery
Ext.override(Ext.form.ComboBox, {
	doQuery : function() {
		if (this.getStore().autoLoad === false && !this.getStore().__autoLoaded) {
			this.getStore().__autoLoaded = true;
			this.getStore().load();
		}
		return Ext.form.ComboBox.prototype._doQuery.apply(this, arguments)
	}
})
Ext.override(Ext.form.Field, {
			hideField : function() {
				var field = this;
				if (field) {
					field.disable();// for validation
					field.hide();
					var labelEl = field.getEl();
					if (labelEl) {
						labelEl.up('.x-form-item').setDisplayed(false); // hide
						// label
					}
				}
			},
			showField : function() {
				var field = this;
				if (field) {
					field.enable();
					field.show();
					var labelEl = field.getEl();
					if (labelEl) {
						field.getEl().up('.x-form-item').setDisplayed(true); // hide
						// label
					}
				}
			}
		})
Function.prototype.dg = Function.prototype.createDelegate

ux.ObjectTextField = Ext.extend(Ext.form.Field, {
			fieldPath : '',
			initComponent : function() {
				if (!this.fieldPath) {
					throw new Error('ObjectTextField.fieldPath required')
				}
			},
			reset : function() {
				this.value = null;
				this.setRawValue(null);
			},
			setValue : function(v) {
				if (v) {
					if (typeof v == 'string') {
						if (!this.value)
							this.value = {};
						this.value[this.fieldPath] = v;
					} else {
						this.value = v;
					}
					this.setRawValue(v[this.fieldPath]);
				} else {
					this.value = v;
				}
			},
			getValue : function() {
				var v = this.getRawValue();
				if (v) {
					if (!this.value)
						this.value = {};
					this.value[this.fieldPath] = v;
				} else {
					this.value = null;
				}
				return this.value;
			}
		})

ux.PostEditableEditorGridPanel = Ext.extend(Ext.grid.EditorGridPanel, {
			postEditValue : function(value, startValue, r, field) {
				if (this.onPostEditValue) {
					this.onPostEditValue(value, startValue, r, field)
				}
				return ux.PostEditableEditorGridPanel.superclass.postEditValue
						.apply(this, arguments);
			}
		})
ux.util = {
	resetEmptyString : function(obj) {
		for (var key in obj) {
			var v = obj[key];
			if (!v) {
				delete obj[key];
			} else if (v.push && v.length) {
				for (var i = 0; i < v.length; i++) {
					this.resetEmptyString(v[i]);
				}
			} else if (key.indexOf('id') != -1 && key.indexOf('-') == 0) {
				delete obj[key]// reset negative id as null
			}
		}
		return obj;
	},
	isEmptyRecord : function(r) {
		for (var key in r.data) {
			if (r.data[key]) {
				if (r.data[key] instanceof Array) {
					if (r.data[key].length)
						return false
				} else {
					return false
				}
			}
		}
		return true
	}
}
Ext.override(Ext.tree.TreeNode, {
			traverseNode : function(node, cb) {
				if (node && cb(node) === false)
					return
				if (node.childNodes && node.childNodes.length)
					for (var i = 0; i < node.childNodes.length; i++) {
						this.traverseNode(node.childNodes[i], cb)
					}
			},
			traverseNodeLevelOrder : function(node, cb) {
				var stack = [node];
				while (stack.length) {
					var n = stack.shift();
					cb(n);
					for (var i = 0; i < n.childNodes.length; i++) {
						stack.push(n.childNodes[i])
					}
				}
			},
			/**
			 * 
			 * @param {Function}
			 *            cb - return false to stop the traverse. params - node
			 */
			traverse : function(cb, type) {
				if (type == 'level')
					this.traverseNodeLevelOrder(this, cb);
				else
					this.traverseNode(this, cb);
			},
			findByAttributes : function(attrsmap) {
				var foundNode;
				this.traverse(function(node) {
							for (var key in attrsmap) {
								if (node.attributes[key] != attrsmap[key]) {
									return;
								}
							}
							foundNode = node
							return false;
						});
				return foundNode;
			}
		})