var editMenuItem = function(el) {
	var isRoot = arguments.length > 1 && arguments[1] === true;
	var wrapper = $(el).parent().parent();
	var state = wrapper.attr('state');
	if (!state || state == 'display') {
		$('span', wrapper).hide();
		$('input', wrapper).each(function(idx, el) {
			$(this).show();
			$(this).val($('span', $(this).parent()).attr('title'));
		});
		wrapper.attr('state', 'edit');
	} else {
		var hasErr = false;
		$('input', wrapper)
				.each(
						function() {
							if ($(this).parent().css('display') == 'none')
								return;
							var val = $.trim($(this).val());
							var isLink = val.indexOf('http://') == 0 || val.indexOf('https://') == 0;
							var isNameField = $(this).attr('name') == 'menuName_i';
							var hasChild = false;
							var next = null;
							if (isRoot) {
								while (!hasChild && (next = wrapper.next()).length) {
									if (next.attr('name') == 'rootMenu')
										break;
									hasChild = true;
								}
								if ((hasChild && ((isNameField && charLength(val) > 16) || (isNameField && !val)))
										|| (!hasChild && (!val || (isNameField && charLength(val) > 16)
												|| (!isNameField && !isLink && charLength(val) > 128) || (!isNameField && isLink && charLength(val) > 256)))) {
									$(this).css('border', '2px solid red');
									hasErr = true;
									return false;
								}
							} else {
								if (!val || (isNameField && charLength(val) > 40) || (!isNameField && !isLink && charLength(val) > 128)
										|| (!isNameField && isLink && charLength(val) > 256)) {
									$(this).css('border', '2px solid red');
									hasErr = true;
									return false;
								}
							}
							$(this).css('border', '1px solid #e3e3e3');
						});
		if (hasErr)
			return false;
		$('input', wrapper).hide();
		$('span', wrapper).each(function(idx, el) {
			$(this).show();
			var val = $('input', $(this).parent()).val();
			$(this).attr('title', val).html(val.length > 13 ? val.substring(0, 13) + '...' : val);
		});
		if (!isRoot) {
			if ($('dd', wrapper.parent()).length >= 0)
				$('span[name=menuKey]', $('dt', wrapper.parent())).parent().hide();
		}
		wrapper.attr('state', 'display');
		preview();
	}
};

var delMenuItem = function(el) {
	var ret = window.confirm('确认删除？');
	if (ret) {
		var ddEl = $(el).parent().parent();
		var dtEl = $('dt', ddEl.parent());
		var dlEl = ddEl.parent();
		ddEl.remove();
		if ($('dd', dlEl).length == 0) {
			$('span[name=menuKey]', dtEl).parent().show();
			$('.CM-cta01', dtEl).trigger('click');
		}
		preview();
	}
};

var delRootMenu = function(el) {
	var ret = window.confirm('确认删除？这将一并删除所有子菜单');
	if (ret) {
		var el = $(el).parent().parent();
		var els = [];
		els.push(el);
		while ((tr = el.next()).length) {
			if (tr.attr('name') == 'rootMenu')
				break;
			els.push(tr);
			el = el.next();
		}
		for ( var i = 0; i < els.length; i++)
			els[i].remove();
	}
};

var addMenuItem = function(el) {
	var tr = $(el).parent().parent();
	var target = null, insertBefore = true, count = 0;
	while ((target = tr.next()).length) {
		if (target.attr('name') == 'rootMenu')
			break;
		tr = tr.next();
		count++;
	}
	if (!target || !target.length) {
		target = tr.parent();
		insertBefore = false;
	}
	if (count == 5) {
		alert('子菜单数量不能超过5个');
		return false;
	}
	var item = $('<tr>').attr('name', 'menuItem');
	if (insertBefore)
		target.before(item);
	else
		target.append(item);
	item
			.html('<td width="111" height="30" align="center" valign="middle" class="CMOP-cttd01 CM-cl03"><div><span name="menuName"></span><input name="menuName_i" value="" style="display:none;width:100px;border:1px solid #f8f8f8;"/></div></td>'
					+ '<td width="181" height="30" align="center" valign="middle" class="CMOP-cttd01 CM-cl03"><div><span name="menuKey"></span><input name="menuKey_i" value="" style="display:none;width:150px;border:1px solid #f8f8f8;"/></div></td>'
					+ '<td width="104" height="30" align="right" valign="middle" class="CMOP-cttd03"><a href="#" class="CM-cta05" style="margin-right:15px"></a> <a href="#" class="CM-cta04"></a></td>');
	$('.CM-cta04', item).click(function() {
		if (window.blurTimeout)
			clearTimeout(window.blurTimeout);
		editMenuItem(this);
	});
	$('.CM-cta04', item).trigger('click');
	$('input', item).keyup(function() {
		if ($.trim(this.value))
			this.style.border = '1px solid #e3e3e3';
	}).blur(function(e) {
		var self = this;
		window.blurTimeout = setTimeout(function() {
			editMenuItem($('.CM-cta04', $(self).parent().parent().parent()).get(0));
		}, 100);
	}).focus(function(e) {
		if (window.blurTimeout)
			clearTimeout(window.blurTimeout);
	});
	$('.CM-cta05', item).click(function() {
		delMenuItem(this);
	});
	preview();
};

$(document)
		.ready(
				function() {
					$('#addRootMenuBtn')
							.click(
									function() {
										if ($('tr[name=rootMenu]', $('#jsweb_c4')).length == 3) {
											alert('菜单数量不能超过3个！');
											return false;
										}
										var dl = $('<tr></tr>').attr('name', 'rootMenu');
										dl
												.html('<td width="111" height="30" align="center" valign="middle" bgcolor="#f8f8f8" class="CMOP-cttd01"><div><span name="menuName"></span><input name="menuName_i" value="" style="display:none;width:100px;"/></div></td>'
														+ '<td width="181" height="30" align="center" valign="middle" bgcolor="#f8f8f8" class="CMOP-cttd01"><div><span name="menuKey"></span><input name="menuKey_i" value="" style="display:none;width:150px;"/></div></td>'
														+ '<td width="104" height="30" align="right" valign="middle" bgcolor="#f8f8f8" class="CMOP-cttd03"> <a href="#" class="CM-cta03" style="margin-right:15px"></a> <a href="#" class="CM-cta02"></a> <a href="#" class="CM-cta01"></a></td>');
										$('#menuContainer').append(dl);
										$('.CM-cta01', dl).click(function(e) {
											if (window.blurTimeout)
												clearTimeout(window.blurTimeout);
											editMenuItem(this, true);
										}).trigger('click');
										$('.CM-cta02', dl).click(function() {
											addMenuItem(this);
										});
										$('.CM-cta03', dl).click(function() {
											delRootMenu(this);
										});
										$('input', dl).keyup(function() {
											if ($.trim(this.value))
												this.style.border = '1px solid #e3e3e3';
										}).blur(function(e) {
											var btn = $('.CM-cta01', $(this).parent().parent().parent()).get(0);
											window.blurTimeout = setTimeout(function() {
												editMenuItem(btn, true);
											}, 100);
										}).focus(function(e) {
											if (window.blurTimeout)
												clearTimeout(window.blurTimeout);
										});
									});
					$('.CM-cta04').click(function() {
						if (window.blurTimeout)
							clearTimeout(window.blurTimeout);
						editMenuItem(this);
					});
					$('.CM-cta05').click(function() {
						delMenuItem(this);
					});
					$('.CM-cta01').click(function() {
						if (window.blurTimeout)
							clearTimeout(window.blurTimeout);
						editMenuItem(this, true);
					});
					$('.CM-cta02').click(function() {
						addMenuItem(this);
					});
					$('.CM-cta03').click(function() {
						delRootMenu(this);
					});
					$('input[name$=_i]').keyup(function() {
						if ($.trim(this.value))
							this.style.border = '1px solid #e3e3e3';
					}).blur(function() {
						var self = this;
						window.blurTimeout = setTimeout(function() {
							var tr = $(self).parent().parent().parent();
							if (tr.attr('state') == 'display')
								return false;
							var isRoot = tr.attr('name') == 'rootMenu';
							if (isRoot)
								$('.CM-cta01', tr).trigger('click');
							else
								$('.CM-cta04', tr).trigger('click');
						}, 100);
					}).focus(function(e) {
						if (window.blurTimeout)
							clearTimeout(window.blurTimeout);
					});
					$('#saveMenuBtn').click(function() {
						var dls = $('tr[name=rootMenu]', '#menuContainer');
						var errMsg = '';
						var data = [];
						dls.each(function(idx, dl) {
							dl = $(dl);
							if (dl.attr('state') == 'edit')
								if (editMenuItem($('.CM-cta01', dl), true) === false) {
									errMsg = '您的菜单尚未正确编辑完成，请检查。';
									return false;
								}
							var rootMenu = {};
							rootMenu.type = 'click';
							rootMenu.name = $('span[name=menuName]', dl).attr('title');
							var rootVal = $('span[name=menuKey]', dl).attr('title');
							if (rootVal.indexOf('http://') == 0 || rootVal.indexOf('https://') == 0) {
								rootMenu.url = rootVal;
								rootMenu.type = 'view';
							} else
								rootMenu.key = rootVal;
							var tr = null, sub_button = [];
							while ((tr = dl.next()).length) {
								if (tr.attr('name') == 'rootMenu')
									break;
								if (tr.attr('state') == 'edit')
									if (editMenuItem($('.CM-cta04', tr)) === false) {
										errMsg = '您的菜单尚未正确编辑完成，请检查。';
										return false;
									}
								var sub = {};
								sub.type = 'click';
								sub.name = $('span[name=menuName]', tr).attr('title');
								var subVal = $('span[name=menuKey]', tr).attr('title');
								if (subVal.indexOf('http://') == 0 || subVal.indexOf('https://') == 0) {
									sub.url = subVal;
									sub.type = 'view';
								} else
									sub.key = subVal;
								sub_button.push(sub);
								dl = dl.next();
							}
							if (sub_button.length) {
								delete rootMenu.key;
								delete rootMenu.type;
								rootMenu.sub_button = sub_button;
							} else {
								if (!rootMenu.key && !rootMenu.url) {
									errMsg = '没有子节点的主菜单必须填写内容！';
									$('.CM-cta01', dl).trigger('click');
									return false;
								}
							}
							data.push(rootMenu);
						});
						if (errMsg) {
							alert(errMsg);
							return false;
						}
						var postBody = data.length ? $.toJSON(data) : '';
						$.ajax({
							url : 'robot-account!saveMenu.action',
							type : 'POST',
							data : {
								protocol : window.protocol,
								data : postBody
							},
							success : function(respTxt) {
								if (respTxt == 'error')
									alert('保存失败，请稍后再试！');
								else
									alert('保存成功！');
							}
						});
						return false;
					});
					$('#deployMenuBtn').click(function() {
						var ret = window.confirm('确认发布菜单？');
						if (ret) {
							$.ajax({
								url : 'robot-account!deployMenu.action',
								type : 'POST',
								data : {
									protocol : window.protocol
								},
								success : function(respTxt) {
									if (respTxt == 'no_data')
										alert('您尚未编辑菜单内容。');
									else if (respTxt == 'error')
										alert('发布失败，请稍后再试！');
									else
										alert('发布成功！');
								}
							});
						}
						return false;
					});
					$('span[name^=menu]', $('#menuContainer')).each(function() {
						var val = $(this).html();
						if (val.length > 13)
							$(this).html(val.substring(0, 13) + '...');
					});
					if (!window.noMenuData)
						preview();
				});

function preview() {
	$('#previewRootItems').html('');
	$('#previewCont').hide();
	var dls = $('tr[name=rootMenu]', '#menuContainer');
	dls.each(function(idx, dl) {
		dl = $(dl);
		var rootMenu = $('<li></li>').html('<h6>' + $('span[name=menuName]', $(this)).html() + '</h6><p></p>');
		$('#previewRootItems').append(rootMenu);
		rootMenu.click(function() {
			$('#previewSubItems').html('');
			var tr = null, subMenus = [];
			var dl0 = dl;
			while ((tr = dl0.next()).length) {
				if (tr.attr('name') == 'rootMenu')
					break;
				subMenus.push(tr);
				dl0 = dl0.next();
			}
			if (subMenus.length == 0) {
				$('#previewCont').hide();
			} else {
				subMenus = $(subMenus);
				subMenus.each(function() {
					var val = $('span[name=menuName]', $(this)).attr('title');
					var displayVal = val;
					if (displayVal.length > 5)
						displayVal = displayVal.substring(0, 5) + '..';
					var subMenu = $('<li></li>').html('<h6>' + displayVal + '</h6><p></p>').attr('title', val);
					$('#previewSubItems').append(subMenu);
				});
				var left = idx * 90 + 50 + 'px';
				var left0 = $('#previewCont').css('left');
				if (left == left0 && $('#previewCont').css('display') != 'none')
					$('#previewCont').hide();
				else {
					$('#previewCont').css('left', left);
					$('#previewCont').show();
				}
			}
		});
	});
}

function charLength(input) {
	var len = 0;
	if (input) {
		for ( var i = 0; i < input.length; i++) {
			var char = input.charAt(i);
			len += /[^\x00-\xff]/g.test(char) ? 2 : 1;
		}
	}
	return len;
}