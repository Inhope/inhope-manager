Dashboard = {
	defaultPlatforms : [ [ 'web', 'WEB', [ 'tpl', 'emot' ] ], [ 'msn', 'MSN', [ 'acc', 'emot' ] ], [ 'qqcrm', 'QQ', [ 'acc' ] ],
			[ 'tsina', '新浪微博', [ 'acc' ] ], [ 'wbfan', '新浪微博粉丝', [ 'acc' ] ], [ 'tqq', '腾讯微博', [ 'acc' ] ], [ 'fetion', '飞信', [ 'acc', 'emot' ] ],
			[ 'fetionrobot', '飞信CRM', [ 'acc', 'emot' ] ], [ 'gtalk', 'GTalk', [ 'acc' ] ], [ 'cmpp', 'CMPP', [ 'acc' ] ], [ 'android', '安卓手机', [] ],
			[ 'ios', '苹果手机', [] ], [ 'android.vm', '车载', [] ], [ 'weixin', '微信', [ 'acc', 'friend', 'menu' ] ],
			[ 'yixin', '易信', [ 'acc', 'friend', 'menu' ] ], [ 'mqq', '营销QQ', [ 'acc', 'friend' ] ], [ 'qqhttp', 'QQIMCC', [ 'acc' ] ],
			[ 'alipay', '支付宝服务窗', [ 'acc' ] ], [ 'tv', '电视', [] ], [ 'line', 'Line', [ 'acc' ] ] ],
	u : function() {
		return this._usr ? this._usr : this._usr = new LoginUser('bot');
	},
	setAlert : function(msg, status, delay) {
		if (!status)
			status = 'ok';
		if (!delay)
			delay = 3;
		if (!this.msgCt) {
			this.msgCt = Ext.DomHelper.insertFirst(document.body, {
				id : 'msg-div'
			}, true);
			this.msgCt.setStyle('position', 'absolute');
			this.msgCt.setStyle('z-index', 9999);
			this.msgCt.setWidth(320);
		}
		this.msgCt.alignTo(document, 't-t');
		var html = [ '<div class="app-msg">', '<div class="x-box-tl"><div class="x-box-tr"><div class="x-box-tc"></div></div></div>',
				'<div class="x-box-ml"><div class="x-box-mr"><div class="x-box-mc"><h3 class="x-icon-text icon-status-' + status + '">', msg,
				'</h3>', '</div></div></div>', '<div class="x-box-bl"><div class="x-box-br"><div class="x-box-bc"></div></div></div>', '</div>' ]
				.join('');
		Ext.DomHelper.append(this.msgCt, {
			'html' : html
		}, true).slideIn('t').pause(delay).ghost("t", {
			remove : true
		});
	},
	utils : {
		showFormItems : function(itemsArr) {
			for ( var i = 0; i < itemsArr.length; i++) {
				itemsArr[i].setVisible(true);
				itemsArr[i].container.up('div.x-form-item').dom.style.display = '';
			}
		},
		hideFormItems : function(itemsArr) {
			for ( var i = 0; i < itemsArr.length; i++) {
				itemsArr[i].setVisible(false);
				itemsArr[i].container.up('div.x-form-item').dom.style.display = 'none';
			}
		},
		arrayDataToMap : function(arr) {
			var _all = [], ret = {};
			for ( var i = 0; i < arr.length; i++) {
				_all = _all.concat(arr[i]);
			}
			for ( var i = 0; i < _all.length; i++) {
				ret[_all[i][0]] = _all[i][1];
			}
			return ret;
		}
	},
	createXHR : function() {
		var _xhr = false;
		try {
			_xhr = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				_xhr = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e2) {
				_xhr = false;
			}
		}
		if (!_xhr && window.XMLHttpRequest)
			_xhr = new XMLHttpRequest();
		return _xhr;
	},
	createDimChooser : function(_width) {
		return Ext.create({
			width : _width,
			name : 'dimension',
			xtype : 'dimensionbox',
			clearBtn : true,
			emptyText : '请选择维度',
			menuConfig : {
				labelAlign : 'top'
			},
			valueTransformer : function(value) {
				if (value == 'ALLDIM')
					return '';
				var tags = this.tags;
				var groupTags = {};
				if (tags)
					Ext.each(tags, function(t) {
						groupTags[t.dimId] = t.id;
					});
				return Ext.encode(groupTags);
			}
		});
	},
	navExternal : function(module) {
		var dashboardName = module + '_Dashboard';
		var topwin = getTopWindow()
		var dashboard = topwin[dashboardName];
		var idx = -1;
		if (!dashboard) {
			for ( var i = 0; i < topwin.modules.length; i++) {
				if (topwin.modules[i].id == module) {
					idx = i;
					break;
				}
			}
		} else {
			idx = dashboard.index;
		}
		if (idx == -1)
			return false;
		topwin.vtab.setActiveTab(idx);
		var args = Array.prototype.slice.call(arguments, 1);
		if (!dashboard) {
			topwin[module + '_initcb'] = function() {
				topwin[dashboardName].nav.apply(topwin[dashboardName], args);
				topwin[dashboardName].index = idx;
			};
		} else {
			topwin[module + '_initcb'] = null;
			topwin[dashboardName].nav.apply(topwin[dashboardName], args);
		}
	},
	nav : function() {
		var panelId = arguments[0];
		var node = null;
		if (panelId.indexOf('-') != -1) {
			var pathArr = panelId.split('-');
			node = Dashboard.navPanel.getRootNode().findChild('platform', pathArr[0], true);
			if (node) {
				node.expand(false);
				node.external = true;
				node = node.findChild('module', pathArr[1], true);
			}
		} else {
			node = Dashboard.navPanel.getRootNode().findChild('id', panelId, true);
		}
		if (node) {
			node.external = true;
			Dashboard.navPanel.fireEvent('click', node);
			var tab = Dashboard.mainPanel.get(panelId + 'Tab');
			Dashboard.mainPanel.activate(tab);
			tab.items.items[0].externalLoad.apply(tab.items.items[0], Array.prototype.slice.call(arguments, 1));
			node.external = false;
		}
	}
};

Ext.onReady(function() {
	Ext.QuickTips.init();

	var xhr = Dashboard.createXHR();
	xhr.open('GET', '../licence.action', false);
	xhr.send();
	var licence = Ext.decode(xhr.responseText);
	if (licence)
		Dashboard.supportedPlatforms = licence.supportedPlatforms;
	xhr.open('GET', '../dimension!loadDim.action?dim=platform', false);
	xhr.send();
	var dim = Ext.decode(xhr.responseText);
	var tags = dim.tags;
	Dashboard.platformMap = {};
	for ( var k = 0; k < Dashboard.defaultPlatforms.length; k++) {
		Dashboard.platformMap[Dashboard.defaultPlatforms[k][0]] = Dashboard.defaultPlatforms[k];
	}
	var dynamicPlatforms = [];
	var tag2TagId = {};
	if (tags) {
		for ( var i = 0; i < tags.length; i++) {
			if (Dashboard.supportedPlatforms && Dashboard.supportedPlatforms.indexOf(tags[i].tag) == -1)
				continue;
			tag2TagId[tags[i].tag] = tags[i].id;
			if (!Dashboard.platformMap[tags[i].tag])
				dynamicPlatforms.push([ tags[i].tag, tags[i].name ]);
		}
	}

	var mainPanel = new Ext.TabPanel({
		region : 'center',
		activeTab : 0,
		border : false,
		style : 'border-left: 1px solid ' + sys_bdcolor,
		resizeTabs : true,
		tabWidth : 165,
		minTabWidth : 120,
		enableTabScroll : true,
		layoutOnTabChange : true
	});

	var _getChildren = function(platform, platformName, config, whiteList) {
		whiteList = whiteList || [];
		var variableItems = [ {
			id : 'web-tpl',
			module : 'tpl',
			leaf : true,
			authName : 'bot.tpl',
			text : '模板管理',
			iconCls : 'icon-report',
			panelClass : WebTemplatePanel
		}, {
			id : platform + '-' + 'acc',
			module : 'acc',
			leaf : true,
			authName : 'bot.acc',
			text : '账号列表',
			textPrefix : platformName,
			iconCls : 'icon-robot-acc',
			panelClass : RobotAccountPanel,
			initConfig : config
		}, {
			id : platform + '-' + 'emot',
			module : 'emot',
			authName : 'bot.emot',
			leaf : true,
			text : '自定义表情管理',
			textPrefix : platformName,
			iconCls : 'icon-emot-icon',
			panelClass : EmotIconPanel,
			initConfig : config
		}, {
			id : platform + '-' + 'menu',
			module : 'menu',
			authName : 'bot.menu',
			leaf : true,
			text : '菜单管理',
			textPrefix : platformName,
			iconCls : 'icon-robot-menu',
			panelClass : MenuPanel,
			initConfig : config
		} ];
		for ( var i = variableItems.length - 1; i >= 0; i--) {
			if (whiteList.indexOf(variableItems[i].module) == -1) {
				variableItems.splice(i, 1);
			}
		}
		var ret = Dashboard.u().filterAllowed(variableItems.concat([ {
			id : platform + '-' + 'adv',
			module : 'adv',
			leaf : true,
			authName : 'bot.adv',
			text : '形象套装设置',
			textPrefix : platformName,
			iconCls : 'icon-advertise',
			panelClass : AdvertiseSuitePanel,
			initConfig : config
		}, {
			id : platform + '-' + 'blst',
			module : 'blst',
			authName : 'bot.blst',
			leaf : true,
			text : '黑名单管理',
			textPrefix : platformName,
			iconCls : 'icon-blocklist-mgr',
			panelClass : BlackListPanel,
			initConfig : config
		}, {
			id : platform + '-' + 'friend',
			module : 'friend',
			authName : 'bot.friend',
			leaf : true,
			text : '用户管理',
			textPrefix : platformName,
			iconCls : 'icon-robot-friend',
			panelClass : RobotFriendPanel,
			initConfig : config
		} ]));
		whiteList.length = 0;
		Ext.each(ret, function(d) {
			whiteList.push(d.module);
		});
		return ret;
	};

	var robotNodes = [];
	for ( var i = 0; i < Dashboard.defaultPlatforms.length; i++) {
		var platInfo = Dashboard.defaultPlatforms[i];
		var _initConfig = {
			platform : platInfo[0],
			platformName : platInfo[1]
		};
		var _disabled = tag2TagId[platInfo[0]] ? !Dashboard.u().allow([ 'bot', dim.id, tag2TagId[platInfo[0]] ].join('.')) : true;
		platInfo[3] = _disabled;
		var nodeCfg = {
			disabled : _disabled,
			text : platInfo[1] + '平台',
			iconCls : 'icon-robot-' + platInfo[0],
			initConfig : _initConfig,
			singleClickExpand : true
		};
		if (nodeCfg.disabled) {
			nodeCfg.expanded = true;
			nodeCfg.children = [];
		}

		nodeCfg.platform = platInfo[0];
		nodeCfg.iconCls = 'icon-robot-' + platInfo[0].replace('.', '');
		if (!nodeCfg.disabled)
			nodeCfg.children = _getChildren(platInfo[0], platInfo[1], _initConfig, platInfo[2]);
		robotNodes.push(nodeCfg);
	}
	if (dynamicPlatforms.length > 0) {
		var children_d = [];
		for ( var i = 0; i < dynamicPlatforms.length; i++) {
			var ptag = dynamicPlatforms[i][0];
			var pname = dynamicPlatforms[i][1];
			var _initConfig = {
				platform : ptag,
				platformName : pname
			};
			var dynPNodeCfg = {
				disabled : tag2TagId[ptag] ? !Dashboard.u().allow([ 'bot', dim.id, tag2TagId[ptag] ].join('.')) : true,
				text : pname + '平台',
				iconCls : 'icon-dynamic-platform0',
				initConfig : _initConfig,
				singleClickExpand : true
			};
			dynPNodeCfg.disabled ? dynPNodeCfg.leaf = true : dynPNodeCfg.children = _getChildren(ptag, pname, _initConfig);
			children_d.push(dynPNodeCfg);
		}
		robotNodes.push({
			singleClickExpand : true,
			text : '其他平台',
			iconCls : 'icon-dynamic-platform',
			children : children_d
		});
	}

	var children = isVersionStandard() ? [ {
		id : 'platView',
		authName : 'bot.pv',
		leaf : true,
		text : '支持渠道概览',
		iconCls : 'icon-robot-plat',
		panelClass : RobotAccountView
	} ].concat(robotNodes) : [ {
		id : 'platView',
		authName : 'bot.pv',
		leaf : true,
		text : '支持渠道概览',
		iconCls : 'icon-robot-plat',
		panelClass : RobotAccountView
	} ].concat(robotNodes).concat([ {
		id : 'robotServiceEx',
		authName : 'bot.sdlg',
		leaf : true,
		text : '平台扩展接口',
		iconCls : 'icon-setup',
		panelClass : RobotServiceExPanel
	} ]);

	var navPanel = new Ext.tree.TreePanel({
		region : 'west',
		width : 200,
		minSize : 175,
		maxSize : 400,
		split : true,
		collapsible : true,
		collapseMode : 'mini',
		border : false,
		style : 'border-right: 1px solid ' + sys_bdcolor,
		title : '控制台',
		autoScroll : true,
		rootVisible : false,
		lines : false,
		root : {
			id : '0',
			text : 'root',
			children : Dashboard.u().filterAllowed(children)
		}
	});

	navPanel.showTab = function(panelClass, tabId, title, iconCls, closable, initConfig) {
		if (!panelClass)
			return false;
		var tab = mainPanel.get(tabId);
		if (!tab) {
			var tabPanel = null;
			if (initConfig)
				tabPanel = new panelClass(initConfig);
			else
				tabPanel = new panelClass();
			tabPanel.navPanel = this;
			tabPanel.tabId = tabId;
			tab = mainPanel.add({
				id : tabId,
				layout : 'fit',
				title : title,
				items : tabPanel,
				iconCls : iconCls,
				closable : closable
			});
			tab.contentPanel = tabPanel;
			tab.show();
		} else {
			mainPanel.activate(tabId);
			if (title)
				tab.setTitle(title);
			if (iconCls)
				tab.setIconClass(iconCls);
		}
		return tab.get(0);
	};
	navPanel.closeTab = function(tabId) {
		mainPanel.remove(tabId);
	};
	navPanel.on('expandnode', function(node) {
		if (node.text == 'root' && node.childNodes && node.childNodes.length) {
			this.fireEvent('click', node.firstChild);
			var topwin = getTopWindow()
			if (topwin.botmgr_initcb) {
				topwin.botmgr_initcb.call();
				topwin.botmgr_initcb = null;
			}
		} else if (node.childNodes && node.childNodes.length && !node.external) {
			this.fireEvent('click', node.firstChild);
		}
	}, navPanel);
	navPanel.on('click', function(n) {
		if (!n)
			return false;
		var panelClass = n.attributes.panelClass;
		if (panelClass) {
			var text = n.text;
			if (n.attributes.textPrefix)
				text = n.attributes.textPrefix + text;
			var p = this.showTab(panelClass, n.id + 'Tab', text, n.attributes.iconCls, true, n.attributes.initConfig);
			if (p && p.init && !n.external)
				p.init();
		}
	});
	var viewport = new Ext.Viewport({
		layout : 'border',
		items : [ navPanel, mainPanel ]
	});
	Dashboard.navPanel = navPanel;
	Dashboard.mainPanel = mainPanel;
});

Ext.form.ImageField = Ext.extend(Ext.form.Field, {
	autoCreate : {
		tag : 'img'
	},
	setValue : function(new_value) {
		this.el.dom.src = new_value;
	},
	initValue : function() {
		this.setValue(this.value);
	},
	initComponent : function() {
		Ext.apply(this, {});
		Ext.form.ImageField.superclass.initComponent.apply(this);
	}
});
Ext.reg('imagefield', Ext.form.ImageField);
Ext.copyToClipboard = function(txt) {
	if (window.clipboardData) {
		window.clipboardData.clearData();
		window.clipboardData.setData("Text", txt);
	} else if (navigator.userAgent.indexOf("Opera") != -1) {
		window.location = txt;
	} else if (window.netscape) {
		try {
			netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
		} catch (e) {
			Ext.Msg.alert('提示', "您的firefox安全限制限制您进行剪贴板操作，请打开'about:config'将signed.applets.codebase_principal_support'设置为true'之后重试。");
			return false;
		}
		var clip = Components.classes['@mozilla.org/widget/clipboard;1'].createInstance(Components.interfaces.nsIClipboard);
		if (!clip)
			return;
		var trans = Components.classes['@mozilla.org/widget/transferable;1'].createInstance(Components.interfaces.nsITransferable);
		if (!trans)
			return;
		trans.addDataFlavor('text/unicode');
		var str = new Object();
		var len = new Object();
		var str = Components.classes["@mozilla.org/supports-string;1"].createInstance(Components.interfaces.nsISupportsString);
		var copytext = txt;
		str.data = copytext;
		trans.setTransferData("text/unicode", str, copytext.length * 2);
		var clipid = Components.interfaces.nsIClipboard;
		if (!clip)
			return false;
		clip.setData(trans, null, clipid.kGlobalClipboard);
	}
	Dashboard.setAlert('复制成功！');
}