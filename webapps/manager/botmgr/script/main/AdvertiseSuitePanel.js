AdvertiseSuitePanel = function(initCfg) {
	var self = this;
	this.platform = initCfg.platform;
	this.ACTIVE = 1;
	// var _platStore = new Ext.data.JsonStore({
	// autoLoad : true,
	// url : 'advertise-item!listDimenstion.action',
	// fields : [ 'tag', 'name' ]
	// });
	this.SuiteRecord = Ext.data.Record.create([ 'id', 'name', 'platform', {
		name : 'begin',
		type : 'string',
		convert : function(v) {
			return v.replace('T', ' ');
		}
	}, {
		name : 'expire',
		type : 'string',
		convert : function(v) {
			return v.replace('T', ' ');
		}
	}, 'state', 'userAttrs' ]);
	var _suitePanel = new Ext.grid.GridPanel({
		region : 'west',
		width : 310,
		split : true,
		columnLines : true,
		collapsible : true,
		collapseMode : 'mini',
		style : 'border-right: 1px solid ' + sys_bdcolor,
		border : false,
		title : '形象套装列表',
		store : new Ext.data.Store({
			proxy : new Ext.data.HttpProxy({
				url : 'advertise-suite!list.action'
			}),
			reader : new Ext.data.JsonReader({
				idProperty : 'id',
				root : 'data',
				fields : self.SuiteRecord
			})
		}),
		tbar : [ {
			iconCls : 'icon-add',
			text : '新增套装',
			handler : function() {
				self.addSuite();
			}
		}, {
			iconCls : 'icon-edit',
			text : '修改套装',
			handler : function() {
				var rec = self.hasSelectedSuite();
				if (rec)
					self.editSuite(rec);
			}
		}, {
			iconCls : 'icon-delete',
			text : '删除',
			handler : function() {
				self.deleteFn(self.suitePanel, 'advertise-suite!delete.action');
			}
		}, {
			iconCls : 'icon-refresh',
			text : '刷新',
			handler : function() {
				self.suitePanel.getStore().reload();
			}
		}, {
			iconCls : 'icon-ok',
			text : '应用',
			handler : function() {
				var rec = self.hasSelectedSuite();
				if (rec) {
					Ext.Msg.confirm('提示', '确定应用所选的形象设置？', function(btn) {
						if (btn == 'yes') {
							Ext.Ajax.request({
								url : 'advertise-suite!apply.action',
								params : {
									id : rec.get('id'),
									platform : self.platform
								},
								success : function(resp) {
									Dashboard.setAlert('应用成功！');
									_suitePanel.getStore().reload();
								},
								failure : function(resp) {
									Ext.Msg.alert('错误', resp.responseText);
								}
							});
						}
					});
				}
			}
		} ],
		sm : new Ext.grid.RowSelectionModel({
			singleSelect : true
		}),
		loadMask : true,
		autoExpandColumn : 'nameCol' + this.platform,
		colModel : new Ext.grid.ColumnModel({
			columns : [ new Ext.grid.RowNumberer(), {
				header : '名称',
				id : 'nameCol' + this.platform,
				dataIndex : 'name'
			}, {
				header : '状态',
				width : 80,
				dataIndex : 'state',
				renderer : function(v, meta, rec) {
					if (v == self.ACTIVE)
						return '启用中';
					else
						return '未启用';
				}
			} ]
		})
	});
	_suitePanel.getStore().setBaseParam('platform', this.platform);
	_suitePanel.getStore().on('load', function() {
		this.getSelectionModel().selectRow(0);
	}, _suitePanel)
	_suitePanel.on('rowdblclick', function(g, rowIdx, evt) {
		var rec = g.getStore().getAt(rowIdx);
		self.editSuite(rec);
	}, _suitePanel);
	var _lastSelectedId;
	_suitePanel.getSelectionModel().on('rowselect', function(sm, rowIdx, rec) {
		var sel = sm.getSelected();
		var currId = sel.get('id');
		if (currId === _lastSelectedId)
			return false;
		else
			_lastSelectedId = currId;
		if (rec && !self.fromExternal) {
			_itemPanel.getStore().reload({
				params : {
					'suiteId' : rec.get('id')
				}
			});
		}
	});

	var _now = new Date();
	// var _userAttrStore = new Ext.data.JsonStore({
	// url : 'user-attr!list.action',
	// root : 'data',
	// idProperty : 'id',
	// fields : ['mark', 'name']
	// });
	var _formCfg = {
		frame : false,
		border : false,
		labelWidth : 70,
		autoHeight : true,
		waitMsgTarget : true,
		bodyStyle : 'padding : 3px 10px; background-color:' + sys_bgcolor
	}
	var _suiteForm = new Ext.form.FormPanel(Ext.apply(_formCfg, {
		items : [ new Ext.form.Hidden({
			name : 'id'
		}), new Ext.form.Hidden({
			name : 'state',
			value : 0
		}), new Ext.form.Hidden({
			name : 'platform',
			value : this.platform
		}), {
			xtype : 'textfield',
			fieldLabel : '套装名称',
			maxLength : 30,
			allowBlank : false,
			name : 'name',
			anchor : '98%'
		}, {
			xtype : 'xdatetime',
			allowBlank : false,
			fieldLabel : '起始时间',
			dateFormat : 'Y-m-d',
			timeFormat : 'H:i',
			value : _now,
			name : 'begin',
			anchor : '98%'
		}, {
			xtype : 'xdatetime',
			fieldLabel : '过期时间',
			allowBlank : false,
			dateFormat : 'Y-m-d',
			timeFormat : 'H:i',
			value : new Date().setMonth(_now.getMonth() + 1),
			name : 'expire',
			anchor : '98%'
		}
		// , new Ext.ux.form.MultiSelect({
		// fieldLabel : '用户属性',
		// name : 'userAttrs',
		// height : 128,
		// width : 128,
		// style : 'overflow-y:hidden',
		// store : _userAttrStore,
		// displayField : 'name',
		// valueField : 'mark'
		// })
		],
		tbar : [ {
			text : '保存',
			iconCls : 'icon-add',
			handler : function() {
				this.disable();
				self.saveFn('advertise-suite!save.action', self.suitePanel.getStore(), self.suiteWin, self.suiteWin.editForm, this);
			}
		} ]
	}));

	var _winCfg = {
		defaults : {
			border : false
		},
		modal : true,
		plain : true,
		shim : true,
		closable : true,
		closeAction : 'hide',
		collapsible : true,
		resizable : false,
		draggable : true,
		animCollapse : true,
		constrainHeader : true,
		shadow : false
	}

	var _suiteWin = new Ext.Window(Ext.apply({
		width : 360,
		title : '编辑形象',
		items : [ _suiteForm ],
		listeners : {
			'beforehide' : function(p) {
				p.editForm.getForm().reset();
			}
		}
	}, _winCfg));
	_suiteWin.editForm = _suiteForm;
	this.suiteWin = _suiteWin;

	var _imgPositions = [ [ 4, '头像' ], [ 512, '大头像' ], [ 8, '场景' ] ];
	var _txtPositions = [ [ 1, '昵称' ], [ 2, '欢迎语' ], [ 256, '欢迎语指令' ], [ 16, '个性签名' ], [ 32, '邀请脚注' ], [ 64, '接受邀请脚注' ], [ 128, '拒绝邀请脚注' ] ];
	var _positionsMap = Dashboard.utils.arrayDataToMap([ _imgPositions, _txtPositions ]);
	var isImgItem = function(pos) {
		return pos == 4 || pos == 8 || pos == 512;
	}
	this.ItemRecord = Ext.data.Record.create([ 'id', 'content', 'position', 'createTime', 'platform', 'platformName' ]);
	var _itemPanel = new Ext.grid.GridPanel({
		region : 'center',
		title : '形象设置',
		columnLines : true,
		style : 'border-left: 1px solid ' + sys_bdcolor + ' border-right: 1px solid ' + sys_bdcolor,
		border : false,
		store : new Ext.data.Store({
			proxy : new Ext.data.HttpProxy({
				url : 'advertise-item!list.action'
			}),
			reader : new Ext.data.JsonReader({
				idProperty : 'id',
				root : 'data',
				fields : self.ItemRecord
			})
		}),
		tbar : [ {
			iconCls : 'icon-add',
			text : '新增形象',
			handler : function() {
				self.addItem();
			}
		},
		// '-',
		// new Ext.form.ComboBox({
		// ref : 'platCombo',
		// triggerAction : 'all',
		// width : 120,
		// editable : false,
		// allowBlank : true,
		// emptyText : '请选择平台',
		// store : _platStore,
		// valueField : 'tag',
		// displayField : 'name'
		// }),
		{
			iconCls : 'icon-refresh',
			text : '刷新',
			handler : function() {
				var params = {
					suiteId : _suitePanel.getSelectionModel().getSelected().get('id')
				};
				self.itemPanel.getStore().reload({
					params : params
				});
			}
		}, '-', '<font color="blue">同一平台可添加多项个性签名，其余位置只能添加一项。</font>' ],
		sm : new Ext.grid.RowSelectionModel({
			singleSelect : true
		}),
		loadMask : true,
		autoExpandColumn : 'contentCol' + this.platform,
		colModel : new Ext.grid.ColumnModel({
			columns : [
					new Ext.grid.RowNumberer(),
					{
						header : '位置',
						width : 90,
						dataIndex : 'position',
						renderer : function(v, meta, rec) {
							return _positionsMap[v];
						}
					},
					{
						header : '内容',
						id : 'contentCol' + this.platform,
						dataIndex : 'content',
						renderer : function(v, meta, rec) {
							if (isImgItem(rec.get('position')))
								return '<span style="vertical-align:top;">' + v
										+ '</span><img style="border:none;margin:3px;" src="advertise-item!showImg.action?id=' + rec.get('id')
										+ '" />';
							else
								return v;
						}
					},
					{
						width : 80,
						header : '操作',
						dataIndex : 'operations',
						renderer : function(v, meta, rec) {
							var ret = "<a class='_delButton' href='javascript:void(0);' onclick='javscript:return false;'>删除</a>"
							if (!isImgItem(rec.get('position')))
								ret = "<a class='_viewButton' href='javascript:void(0);' onclick='javscript:return false;'>编辑</a>&nbsp;&nbsp;&nbsp;"
										+ ret;
							return ret;
						}
					} ]
		})
	});
	_itemPanel.getStore().setBaseParam('platform', this.platform);

	// var _platComboCfg = {
	// triggerAction : 'all',
	// fieldLabel : '适用平台',
	// anchor : '60%',
	// editable : false,
	// allowBlank : false,
	// blankText : '请选择平台',
	// store : _platStore,
	// valueField : 'tag',
	// displayField : 'name'
	// }
	// var _platComboImg = new Ext.form.ComboBox(_platComboCfg);
	// var _platComboTxt = new Ext.form.ComboBox(Ext.apply({
	// name : 'platform'
	// }, _platComboCfg));

	_itemPanel.on('cellclick', function(grid, row, col, e) {
		var editBtn = e.getTarget('._viewButton');
		var delBtn = e.getTarget('._delButton');
		var rec = this.getStore().getAt(row);
		if (editBtn) {
			self.itemWin.show();
			// _switchTab.imgTab.setDisabled(true);
			self.itemWin.txtForm.getForm().loadRecord(rec);
		}
		if (delBtn) {
			self.deleteFn(_itemPanel, 'advertise-item!delete.action');
		}
	}, _itemPanel);
	_itemPanel.getStore().on('load', function() {
		this.filterBy(function(r) {
			if (r.get('position') == 1024)
				return false;
			return true;
		});
	}, _itemPanel.getStore());

	var suffixReg = /gif$|png$|jpg$/i;
	var _imgItemForm = new Ext.FormPanel(Ext.apply(_formCfg, {
		fileUpload : true,
		defaults : {
			anchor : '95%',
			allowBlank : false,
			msgTarget : 'side'
		},
		items : [ new Ext.form.Hidden({
			name : 'platform',
			value : this.platform
		}), new Ext.form.ComboBox({
			triggerAction : 'all',
			fieldLabel : '图片位置',
			anchor : '60%',
			hiddenName : 'position',
			name : 'position',
			editable : false,
			allowBlank : false,
			blankText : '请选择位置',
			value : 4,
			mode : 'local',
			store : new Ext.data.ArrayStore({
				fields : [ 'position', 'txt' ],
				data : _imgPositions
			}),
			valueField : 'position',
			displayField : 'txt'
		}),
		// _platComboImg,
		{
			xtype : 'textfield',
			fieldLabel : '图片名称',
			name : 'name',
			allowBlank : true,
			validator : function(v) {
				v = v.trim();
				if (v) {
					v = v.substring(v.indexOf('.') + 1);
					if (suffixReg.test(v))
						return true;
					else
						return '图片名称不正确';
				}
				return true;
			}
		}, {
			xtype : 'fileuploadfield',
			emptyText : '选择一幅图片',
			fieldLabel : '图片路径',
			name : 'img',
			buttonText : '',
			buttonCfg : {
				iconCls : 'icon-upload'
			}
		} ],
		tbar : [ {
			text : '保存',
			iconCls : 'icon-add',
			handler : function() {
				var btn = this;
				btn.disable();
				if (_imgItemForm.getForm().isValid()) {
					_imgItemForm.getForm().submit({
						url : 'advertise-item!saveImg.action',
						waitMsg : '正在上传图片...',
						params : {
							suiteId : _suitePanel.getSelectionModel().getSelected().get('id'),
							platform : self.platform
						},
						success : function(form, act) {
							Ext.Msg.alert('提示', '上传成功', function() {
								btn.enable();
								_itemWin.hide();
								_itemPanel.getStore().reload();
							});
						},
						failure : function(form, act) {
							Ext.Msg.alert('错误', act.result.message);
							btn.enable();
						}
					});
				} else {
					Ext.Msg.alert('错误', '您尚未正确完整的输入表单内容，请检查！', function() {
						btn.enable()
					});
				}
			}
		}, '-', '<font color="blue">GIF,JPG或PNG图片,头像为96*96,大头像98*142,场景小于100K</font>' ]
	}));
	var _txtItemForm = new Ext.form.FormPanel(Ext.apply(_formCfg, {
		defaults : {
			anchor : '92%',
			allowBlank : false,
			msgTarget : 'side'
		},
		items : [ new Ext.form.Hidden({
			name : 'platform',
			value : this.platform
		}), new Ext.form.Hidden({
			name : 'id'
		}), new Ext.form.ComboBox({
			triggerAction : 'all',
			fieldLabel : '应用位置',
			name : 'position',
			editable : false,
			allowBlank : false,
			blankText : '请选择位置',
			value : 1,
			mode : 'local',
			store : new Ext.data.ArrayStore({
				fields : [ 'position', 'txt' ],
				data : _txtPositions
			}),
			valueField : 'position',
			displayField : 'txt'
		}), /** _platComboTxt, */
		{
			xtype : 'textarea',
			fieldLabel : '文字内容',
			name : 'content'
		} ],
		tbar : [ {
			text : '保存',
			iconCls : 'icon-add',
			handler : function() {
				this.disable();
				var rec = self.suitePanel.getSelectionModel().getSelected();
				self.saveFn('advertise-item!saveTxt.action', self.itemPanel.getStore(), self.itemWin, self.itemWin.txtForm, this, {
					suiteId : rec.get('id')
				});
			}
		} ]
	}));
	var _switchTab = new Ext.Panel({
		activeTab : 0,
		border : false,
		resizeTabs : true,
		tabWidth : 100,
		autoHeight : true,
		enableTabScroll : true,
		layoutOnTabChange : true,
		layout : 'fit',
		items : _txtItemForm
	});
	// _switchTab.add({
	// layout : 'fit',
	// title : '文字形象位',
	// autoHeight : true,
	// items : _txtItemForm
	// });
	// _switchTab.add({
	// ref : 'imgTab',
	// layout : 'fit',
	// title : '图片形象位',
	// autoHeight : true,
	// items : _imgItemForm
	// });
	var _itemWin = new Ext.Window(Ext.apply({
		title : '编辑形象位',
		width : 420,
		items : [ _switchTab ],
		listeners : {
			'beforehide' : function(p) {
				// _switchTab.setActiveTab(0);
				// _switchTab.imgTab.setDisabled(false);
				p.imgForm.getForm().reset();
				p.txtForm.getForm().reset();
			}
		}
	}, _winCfg));
	_itemWin.imgForm = _imgItemForm;
	_itemWin.txtForm = _txtItemForm;
	this.itemWin = _itemWin;

	this.suitePanel = _suitePanel;
	this.itemPanel = _itemPanel;

	var config = {
		layout : 'border',
		border : false,
		items : [ _suitePanel, _itemPanel ]
	};

	AdvertiseSuitePanel.superclass.constructor.call(this, config);
}

Ext.extend(AdvertiseSuitePanel, Ext.Panel, {
	init : function() {
		this.suitePanel.getStore().load();
	},
	hasSelectedSuite : function() {
		var rec = this.suitePanel.getSelectionModel().getSelected();
		if (!rec) {
			Ext.Msg.alert('提示', '请先在套装列表里选择一条记录！');
			return false;
		}
		return rec;
	},
	addSuite : function() {
		this.suiteWin.show();
	},
	editSuite : function(rec) {
		this.suiteWin.show();
		var f = this.suiteWin.editForm.getForm().loadRecord(rec);
	},
	afterSave : function(btn, win) {
		btn.enable();
		this.getEl().unmask();
		win.hide();
	},
	addItem : function() {
		if (this.hasSelectedSuite())
			this.itemWin.show();
	},
	saveFn : function(url, st, win, form, btn, extraParams) {
		var self = this;
		var f = form.getForm();
		if (!f.isValid()) {
			Ext.Msg.alert('提示', '您尚未正确完整的输入表单内容，请检查！');
			btn.enable();
			return false;
		}
		this.getEl().mask('保存中...');
		var vals = f.getFieldValues();
		if (!vals.id)
			vals.id = null;
		if (!vals.platform)
			vals.platform = this.platform;
		params = {
			'data' : Ext.encode(vals)
		};
		if (extraParams)
			Ext.apply(params, extraParams);
		Ext.Ajax.request({
			url : url,
			params : params,
			success : function(resp) {
				var rst = Ext.decode(resp.responseText);
				if (rst.message) {
					Ext.Msg.alert('提示', rst.message);
				} else {
					Dashboard.setAlert('保存成功！');
					st.reload();
				}
				self.afterSave(btn, win);
			},
			failure : function(resp) {
				Ext.Msg.alert('错误', resp.responseText);
				self.afterSave(btn, win);
			}
		});
	},
	deleteFn : function(grid, url) {
		var self = this;
		var rows = grid.getSelectionModel().getSelections();
		if (rows.length == 0) {
			Ext.Msg.alert('提示', '请选择一条记录删除！');
			return false;
		}
		Ext.Msg.confirm('提示', '确认删除？', function(btn) {
			if (btn == 'yes') {
				self.getEl().mask('删除中...');
				var ids = [];
				Ext.each(rows, function() {
					ids.push(this.id)
				});
				Ext.Ajax.request({
					url : url,
					params : {
						ids : ids.join(',')
					},
					success : function(resp) {
						self.getEl().unmask();
						Dashboard.setAlert('删除成功！');
						grid.getStore().reload();
					},
					failure : function(resp) {
						self.getEl().unmask();
						Ext.Msg.alert('错误', resp.responseText);
					}
				})
			}
		});
	},
	externalLoad : function() {
		var platform = arguments[0];
		var self = this;
		this.fromExternal = true;
		this.suitePanel.getStore().setBaseParam('platform', platform);
		this.itemPanel.getStore().setBaseParam('platform', platform);
		this.suitePanel.getStore().load({
			callback : function() {
				var rec = self.suitePanel.getSelectionModel().getSelected();
				if (!rec)
					return false;
				var params = {
					suiteId : rec.get('id')
				};
				self.itemPanel.getStore().reload({
					params : params
				});
				this.fromExternal = null;
			}
		});
	}
});