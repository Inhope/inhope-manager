BlackListPanel = function(initCfg) {
	var self = this;
	this.platform = initCfg.platform;

	var store = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({
			url : 'black-list!list.action'
		}),
		reader : new Ext.data.JsonReader({
			idProperty : 'id',
			root : 'data',
			fields : [ {
				name : 'id'
			}, {
				name : 'robotAccount'
			}, {
				name : 'rule'
			}, {
				name : 'ruleType'
			}, {
				name : 'platform'
			}, {
				name : 'platformName'
			}, {
				name : 'state'
			}, {
				name : 'type',
				type : 'int'
			}, {
				name : 'startTime'
			}, {
				name : 'expireTime'
			}, {
				name : 'target'
			} ]
		})
	});
	store.setBaseParam('queryParam.platform', this.platform);
	var typeData = [ [ 1, '临时屏蔽' ], [ 2, '永久屏蔽' ] ];
	var typeMap = Dashboard.utils.arrayDataToMap([ typeData ]);
	var _typeCombo = new Ext.form.ComboBox({
		triggerAction : 'all',
		fieldLabel : '屏蔽方式',
		anchor : '60%',
		name : 'type',
		editable : false,
		allowBlank : false,
		blankText : '请选择平台',
		value : 1,
		mode : 'local',
		store : new Ext.data.ArrayStore({
			fields : [ 'type', 'display' ],
			data : typeData
		}),
		valueField : 'type',
		displayField : 'display'
	});
	var targetData = [ [ 1, '用户账号' ], [ 2, '用户IP' ] ];
	var targetMap = Dashboard.utils.arrayDataToMap([ targetData ]);
	var _targetCombo = new Ext.form.ComboBox({
		triggerAction : 'all',
		fieldLabel : '屏蔽对象',
		anchor : '60%',
		name : 'target',
		editable : false,
		allowBlank : false,
		blankText : '请选择平台',
		value : 1,
		mode : 'local',
		store : new Ext.data.ArrayStore({
			fields : [ 'type', 'display' ],
			data : targetData
		}),
		valueField : 'type',
		displayField : 'display'
	});
	var pagingBar = new Ext.PagingToolbar({
		store : store,
		displayInfo : true,
		pageSize : 20,
		prependButtons : true,
		beforePageText : '第',
		afterPageText : '页，共{0}页',
		displayMsg : '第{0}到{1}条记录，共{2}条',
		emptyMsg : "没有记录"
	});
	this.isAdd = true;
	var rule = [ "无匹配", "全局匹配", "以规则开头", "以规则结尾", "正则匹配" ];
	var state = [ "关闭", "开启" ];
	var config = {
		store : store,
		columnLines : true,
		margins : '0 5 5 5',
		border : false,
		style : 'border-right: 1px solid ' + sys_bdcolor,
		tbar : [
				{
					iconCls : 'icon-add',
					text : '添加',
					handler : function() {
						this.isAdd = true;
						self.formWin.show();
					}
				},
				{
					iconCls : 'icon-edit',
					text : '编辑',
					handler : function() {
						var rows = self.getSelectionModel().getSelections();
						if (rows.length != 1) {
							Ext.Msg.alert('提示', '请选择一条记录进行编辑！');
							return false;
						}
						self.editList(rows[0]);
					}
				},
				{
					iconCls : 'icon-delete',
					text : '删除',
					handler : function() {
						var rows = self.getSelectionModel().getSelections();
						if (rows.length == 0) {
							Ext.Msg.alert('提示', '请至少选择一条记录！');
							return false;
						}
						Ext.Msg.confirm('提示', '确认删除这' + rows.length + '条记录？',
								function(btn) {
									if (btn == 'yes') {
										var ids = [];
										Ext.each(rows, function() {
											ids.push(this.id)
										});
										self.getEl().mask('正在删除...');
										Ext.Ajax.request({
											url : 'black-list!delete.action',
											params : {
												ids : ids.join(',')
											},
											success : function(resp) {
												self.getEl().unmask();
												Dashboard.setAlert('删除成功');
												store.reload();
											},
											failure : function(resp) {
												self.getEl().unmask();
												Ext.Msg.alert('错误',
														resp.responseText);
											}
										})
									}
								});
					}
				},
				{
					iconCls : 'icon-refresh',
					text : '刷新',
					handler : function() {
						store.reload();
					}
				},
				{
					iconCls : 'icon-ok',
					text : '应用',
					handler : function() {
						Ext.Msg.confirm('提示', '确定应用？', function(btn) {
							if (btn == 'yes') {
								Ext.Ajax.request({
									url : 'black-list!apply.action',
									success : function(resp) {
										Dashboard.setAlert('应用成功！');
									},
									failure : function(resp) {
										Ext.Msg.alert('错误', resp.responseText);
									}
								});
							}
						});
					}
				},
				'-',
				{
					iconCls : 'icon-import',
					text : '导入',
					handler : function() {
						self.uploadWin.show();
					}
				},
				{
					iconCls : 'icon-export',
					text : '导出',
					handler : function() {
						Ext.MessageBox
								.show({
									title : '提示',
									msg : '请选择导出excel文件格式',
									buttons : {
										yes : '07',
										no : '03',
										cancel : '取消'
									},
									fn : function(btn) {
										var type;
										if (btn == 'yes')
											type = 'xlsx';
										else if (btn == 'no')
											type = 'xls';
										if (type) {
											if (!self.downloadIFrame) {
												self.downloadIFrame = self
														.getEl()
														.createChild(
																{
																	tag : 'iframe',
																	style : 'display:none;'
																})
											}
											self.downloadIFrame.dom.src = 'black-list!export.action?ts='
													+ new Date().getTime()
													+ '&type=' + type;
										}
									},
									icon : Ext.MessageBox.QUESTION
								});
					}
				} ],
		bbar : pagingBar,
		sm : new Ext.grid.RowSelectionModel(),
		loadMask : true,
		colModel : new Ext.grid.ColumnModel({
			columns : [ new Ext.grid.RowNumberer(), {
				width : 300,
				hidden : true,
				header : '机器人账号',
				dataIndex : 'robotAccount'
			}, {
				width : 250,
				header : '规则样例',
				dataIndex : 'rule'
			}, {
				width : 100,
				header : '匹配规则',
				dataIndex : 'ruleType',
				renderer : function(v, met, r) {
					return rule[v];
				}
			}, {
				width : 100,
				header : '平台',
				hidden : true,
				dataIndex : 'platformName'
			}, {
				width : 100,
				header : '屏蔽对象',
				dataIndex : 'target',
				renderer : function(v, met, r) {
					return targetMap[v];
				}
			}, {
				width : 100,
				header : '屏蔽方式',
				dataIndex : 'type',
				renderer : function(v, met, r) {
					return typeMap[v];
				}
			}, {
				width : 130,
				header : '开始时间',
				dataIndex : 'startTime',
				renderer : function(v, met, r) {
					if (!v)
						v = '-';
					return v;
				}
			}, {
				width : 130,
				header : '过期时间',
				dataIndex : 'expireTime',
				renderer : function(v, met, r) {
					if (!v)
						v = '-';
					return v;
				}
			}, {
				width : 100,
				header : '状态',
				dataIndex : 'state',
				renderer : function(v, met, r) {
					return state[v];
				}
			} ]
		}),
		viewConfig : {
			forceFit : true
		}
	};
	var _store = new Ext.data.SimpleStore({
		fields : [ 'value', 'name' ],
		data : [ [ '0', '无匹配' ], [ '1', '全局匹配' ], [ '2', '以规则开头' ],
				[ '3', '以规则结尾' ], [ '4', '正则匹配' ] ]
	});

	var _stateStore = new Ext.data.SimpleStore({
		fields : [ 'value', 'name' ],
		data : [ [ '1', '开启' ], [ '0', '关闭' ] ]
	});
	this.tbarEx = new Ext.Toolbar({
		items : [ '规则样例:', {
			ref : 's_rule',
			xtype : 'textfield',
			width : 120,
			emptyText : '输入样例搜索'
		}, '屏蔽方式:', new Ext.form.ComboBox({
			ref : 's_type',
			triggerAction : 'all',
			width : 90,
			mode : 'local',
			editable : false,
			emptyText : '请选择方式',
			store : new Ext.data.ArrayStore({
				fields : [ 'type', 'display' ],
				data : [ [ 0, '全部' ] ].concat(typeData)
			}),
			valueField : 'type',
			displayField : 'display'
		}), '屏蔽对象:', new Ext.form.ComboBox({
			ref : 's_target',
			triggerAction : 'all',
			width : 90,
			mode : 'local',
			editable : false,
			emptyText : '请选择对象',
			store : new Ext.data.ArrayStore({
				fields : [ 'type', 'display' ],
				data : [ [ 0, '全部' ] ].concat(targetData)
			}),
			valueField : 'type',
			displayField : 'display'
		}), '匹配规则:', new Ext.form.ComboBox({
			ref : 's_ruleType',
			triggerAction : 'all',
			width : 90,
			mode : 'local',
			editable : false,
			emptyText : '请选择规则',
			store : _store,
			valueField : 'value',
			displayField : 'name'
		}), '状态:', new Ext.form.ComboBox({
			ref : 's_state',
			triggerAction : 'all',
			width : 90,
			mode : 'local',
			editable : false,
			store : _stateStore,
			emptyText : '请选择状态',
			valueField : 'value',
			displayField : 'name'
		}), ' ', {
			text : '搜索',
			iconCls : 'icon-search',
			handler : function() {
				self.search();
			}
		}, ' ', {
			text : '全部',
			iconCls : 'icon-search',
			handler : function() {
				self.search(true);
			}
		} ]
	});

	BlackListPanel.superclass.constructor.call(this, config);

	this.addListener('render', function() {
		this.tbarEx.render(this.tbar);
	}, this);
	this.on('rowdblclick', function(g, rowIdx, evt) {
		var rec = this.getStore().getAt(rowIdx);
		this.editList(rec);
	}, this);

	var _ruleCombo = new Ext.form.ComboBox({
		triggerAction : 'all',
		fieldLabel : '匹配规则',
		anchor : '60%',
		name : 'ruleType',
		editable : false,
		mode : 'local',
		allowBlank : false,
		blankText : '请选择规则',
		store : _store,
		valueField : 'value',
		displayField : 'name'
	});

	_stateStore.on('load', function() {
		stateCombo.setValue('1');
	});

	var stateCombo = new Ext.form.ComboBox({
		triggerAction : 'all',
		fieldLabel : '状态',
		width : 90,
		mode : 'local',
		name : 'state',
		editable : false,
		store : _stateStore,
		value : '1',
		valueField : 'value',
		displayField : 'name'
	});

	var _editForm = new Ext.form.FormPanel({
		frame : false,
		border : false,
		labelWidth : 75,
		autoHeight : true,
		waitMsgTarget : true,
		bodyStyle : 'padding : 3px 10px; background-color:' + sys_bgcolor,
		defaults : {
			anchor : '96%',
			allowBlank : false
		},
		items : [ new Ext.form.Hidden({
			name : 'id'
		}), new Ext.form.Hidden({
			name : 'platform',
			value : self.platform
		}), {
			xtype : 'textfield',
			hidden : true,
			allowBlank : true,
			fieldLabel : '机器人账号',
			name : 'robotAccount',
			blankText : '标识不能为空'
		}, _targetCombo, _typeCombo, {
			xtype : 'xdatetime',
			allowBlank : true,
			fieldLabel : '起始时间',
			dateFormat : 'Y-m-d',
			timeFormat : 'H:i',
			value : new Date(),
			name : 'startTime',
			hideMode : 'display',
			anchor : '80%',
			ref : 'startTime'
		}, {
			xtype : 'xdatetime',
			allowBlank : true,
			fieldLabel : '过期时间',
			dateFormat : 'Y-m-d',
			timeFormat : 'H:i',
			value : new Date().setHours(new Date().getHours() + 72),
			name : 'expireTime',
			hideMode : 'display',
			anchor : '80%',
			ref : 'expireTime'
		}, _ruleCombo, {
			xtype : 'textfield',
			fieldLabel : '规则样例',
			name : 'rule',
			allowBlank : true
		}, stateCombo ],
		tbar : [ {
			text : '保存',
			iconCls : 'icon-add',
			handler : function() {
				this.disable();
				self.saveList(this);
			}
		} ]
	});
	_typeCombo.on('select', function(combo, rec, idx) {
		var type = _typeCombo.getValue();
		var st = _editForm.startTime;
		var et = _editForm.expireTime;
		if (type == 1) {
			if (self.isAdd) {
				var _now = new Date();
				st.setValue(_now);
				et.setValue(_now.setHours(_now.getHours() + 72));
			}
			Dashboard.utils.showFormItems([ st ]);
			Dashboard.utils.showFormItems([ et ]);
		} else {
			Dashboard.utils.hideFormItems([ st ]);
			Dashboard.utils.hideFormItems([ et ]);
		}
	});
	this.typeCombo = _typeCombo;
	var winCfg = {
		defaults : {
			border : false
		},
		modal : true,
		plain : true,
		shim : true,
		closable : true,
		closeAction : 'hide',
		collapsible : true,
		resizable : false,
		draggable : true,
		animCollapse : true,
		constrainHeader : true,
		shadow : false,
		listeners : {
			'beforehide' : function(p) {
				p.form.getForm().reset();
			}
		}
	}
	var _formWin = new Ext.Window(Ext.apply({
		width : 420,
		title : '编辑黑名单',
		items : [ _editForm ]
	}, winCfg));

	_formWin.form = _editForm;
	this.formWin = _formWin;

	var _uploadForm = new Ext.FormPanel(
			{
				fileUpload : true,
				frame : false,
				border : false,
				labelWidth : 70,
				autoHeight : true,
				waitMsgTarget : true,
				bodyStyle : 'padding : 3px 10px; background-color:' + sys_bgcolor,
				defaults : {
					anchor : '95%',
					allowBlank : false,
					msgTarget : 'side'
				},
				items : [ {
					xtype : 'fileuploadfield',
					emptyText : '选择文件',
					fieldLabel : '文件路径',
					name : 'sheetFile',
					buttonText : '',
					buttonCfg : {
						iconCls : 'icon-upload-excel'
					},
					validator : function(v) {
						v = v.trim();
						if (v) {
							if (v.indexOf('.xls') < 0)
								return '文件格式不正确';
							return true;
						}
						return '请选择文件';
					}
				} ],
				tbar : [
						{
							text : '导入',
							iconCls : 'icon-import',
							handler : function() {
								var btn = this;
								btn.disable();
								var params = {};
								if (_uploadForm.getTopToolbar().clearAllChk.checked)
									params.clear = 'true';
								if (_uploadForm.getForm().isValid()) {
									_uploadForm
											.getForm()
											.submit(
													{
														url : 'black-list!_import.action',
														waitMsg : '正在导入黑名单...',
														params : params,
														success : function(
																form, a) {
															var o = Ext
																	.decode(a.response.responseText);
															var result = o.data;
															var msg = '成功导入'
																	+ result.succTotal
																	+ '条记录。';
															if (o.message != '0')
																msg += '有'
																		+ o.message
																		+ '行数据重复，导入失败。';
															Ext.Msg
																	.alert(
																			'导入结果',
																			msg,
																			function() {
																				btn
																						.enable();
																				_uploadWin
																						.hide();
																				self
																						.getStore()
																						.reload();
																			})
														},
														failure : function(
																form, act) {
															Ext.Msg
																	.alert(
																			'错误',
																			Ext
																					.decode(act.response.responseText).message);
															btn.enable();
														}
													});
								} else {
									Ext.Msg.alert('错误', '您尚未正确完整的输入表单内容，请检查！',
											function() {
												btn.enable()
											});
								}
							}
						}, '-', {
							xtype : 'checkbox',
							name : 'clearAll',
							ref : 'clearAllChk',
							boxLabel : '清空导入'
						} ]
			});
	var _uploadWin = new Ext.Window(Ext.apply({
		title : '导入黑名单',
		items : [ _uploadForm ],
		width : 400
	}, winCfg));
	_uploadWin.form = _uploadForm;
	this.uploadWin = _uploadWin;
}

Ext.extend(BlackListPanel, Ext.grid.GridPanel, {
	init : function() {
		this.getStore().load()
	},
	editList : function(rec) {
		this.isAdd = false;
		this.formWin.show();
		this.formWin.form.getForm().loadRecord(rec);
		this.typeCombo.fireEvent('select');
	},
	afterSave : function(btn) {
		btn.enable();
		this.formWin.hide();
	},
	saveList : function(btn) {
		var self = this;
		var f = this.formWin.form.getForm();
		if (!f.isValid()) {
			Ext.Msg.alert('提示', '您尚未正确完整的输入表单内容，请检查！');
			btn.enable();
			return false;
		}
		var vals = f.getFieldValues();
		if (!vals.id)
			delete vals.id;
		if (vals.type == 2) {
			delete vals.startTime;
			delete vals.expireTime;
		}
		Ext.Ajax.request({
			url : 'black-list!save.action',
			params : {
				'data' : Ext.encode(vals)
			},
			success : function(resp) {
				var ret = Ext.decode(resp.responseText);
				if (ret.message && ret.message == 'fail') {
					Ext.Msg.alert('错误', '保存失败，已存在相应的黑名单！');
					btn.enable();
				} else {
					Dashboard.setAlert('保存成功！');
					self.getStore().reload();
					self.afterSave(btn);
				}
			},
			failure : function(resp) {
				Ext.Msg.alert('错误', resp.responseText);
				self.afterSave(btn);
			}
		});
	},
	search : function(showAll) {
		var _ruleField = this.tbarEx.s_rule;
		var _ruleCombo = this.tbarEx.s_ruleType;
		var _stateRadio = this.tbarEx.s_state;
		var _typeCombo = this.tbarEx.s_type;
		var _targetCombo = this.tbarEx.s_target;
		if (showAll) {
			_ruleField.setValue('');
			_ruleCombo.setValue('');
			_stateRadio.setValue('');
			_typeCombo.setValue('');
			_targetCombo.setValue('');
		}
		var store = this.getStore();
		for ( var key in store.baseParams) {
			if (key && key.indexOf('queryParam.') != -1)
				delete store.baseParams[key];
		}
		var _rule = _ruleField.getValue();
		var _ruleType = _ruleCombo.getValue();
		var _state = _stateRadio.getValue();
		var _type = _typeCombo.getValue();
		var _target = _targetCombo.getValue();
		if (_rule)
			store.setBaseParam('queryParam.rule', _rule);
		if (_ruleType)
			store.setBaseParam('queryParam.ruleType', _ruleType);
		if (_type)
			store.setBaseParam('queryParam.type', _type);
		if (_target)
			store.setBaseParam('queryParam.target', _target);
		if (_state)
			store.setBaseParam('queryParam.state', _state);
		this.store.baseParams['queryParam.platform'] = this.platform;
		store.load();
	},
	externalLoad : function(v) {
		var platform = arguments[0];
		this.store.baseParams['queryParam.platform'] = platform;
		this.store.load({
			params : {
				start : 0,
				limit : 20
			}
		});
	}
});