EmotIconPanel = function(initCfg) {

	var self = this;
	this.platform = initCfg.platform;

	var store = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({
			api : {
				read : 'emotion-icon!list.action',
				create : 'emotion-icon!save.action',
				update : 'emotion-icon!update.action',
				destroy : 'emotion-icon!delete.action'
			}
		}),
		reader : new Ext.data.JsonReader({
			idProperty : 'id',
			root : 'data',
			fields : [ {
				name : 'id',
				type : 'string'
			}, {
				name : 'cmd',
				type : 'string'
			}, {
				name : 'name',
				type : 'string'
			}, {
				name : 'platform'
			} ]
		}),
		writer : new Ext.data.JsonWriter({
			writeAllFields : true
		}),
		listeners : {
			beforewrite : function(store, data, rs) {
				if (!rs.get('cmd'))
					return false;
			}
		}
	});
	store.setBaseParam('queryParam.platform', this.platform);

	var editor = new Ext.ux.grid.RowEditor({
		saveText : '保存',
		cancelText : '取消',
		errorSummary : false
	});
	editor.on('canceledit', function(editor, obj, rec, rowIdx) {
		var r, i = 0;
		while ((r = store.getAt(i++))) {
			if (r.phantom) {
				store.remove(r);
			}
		}
	});
	var pagingBar = new Ext.PagingToolbar({
		store : store,
		displayInfo : true,
		pageSize : 20,
		prependButtons : true,
		beforePageText : '第',
		afterPageText : '页，共{0}页',
		displayMsg : '第{0}到{1}条记录，共{2}条',
		emptyMsg : "没有记录"
	});
	var config = {
		store : store,
		columnLines : true,
		border : false,
		margins : '0 5 5 5',
		autoExpandColumn : 'picCol' + this.platform,
		plugins : [ editor ],
		tbar : [
				{
					iconCls : 'icon-add',
					text : '添加表情',
					handler : function() {
						self.addWin.show();
					}
				},
				{
					iconCls : 'icon-delete',
					text : '删除表情',
					handler : function() {
						editor.stopEditing();
						var rows = self.getSelectionModel().getSelections();
						if (rows.length == 0) {
							Ext.Msg.alert('提示', '请至少选择一条记录！');
							return false;
						}
						Ext.Msg.confirm('提示', '确认删除这' + rows.length + '条记录？',
								function(btn) {
									if (btn == 'yes') {
										var ids = [];
										Ext.each(rows, function() {
											ids.push(this.id)
										});
										self.getEl().mask('正在删除...');
										Ext.Ajax.request({
											url : 'emotion-icon!delete.action',
											params : {
												ids : ids.join(',')
											},
											success : function(resp) {
												self.getEl().unmask();
												Dashboard.setAlert('删除成功！');
												store.reload();
											},
											failure : function(resp) {
												self.getEl().unmask();
												Ext.Msg.alert('错误',
														resp.responseText);
											}
										})
									}
								});
					}
				}, {
					iconCls : 'icon-refresh',
					text : '刷新',
					handler : function() {
						store.reload();
					}
				}, {
					iconCls : 'icon-ok',
					text : '应用',
					handler : function() {
						Ext.Msg.confirm('提示', '确定应用？', function(btn) {
							if (btn == 'yes') {
								Ext.Ajax.request({
									url : 'emotion-icon!apply.action',
									success : function(resp) {
										Dashboard.setAlert('应用成功！');
									},
									failure : function(resp) {
										Ext.Msg.alert('错误', resp.responseText);
									}
								});
							}
						});
					}
				}, '-', {
					iconCls : 'icon-import',
					text : '导入',
					handler : function() {
						self.uploadWin.show();
					}
				}, {
					iconCls : 'icon-export',
					text : '导出',
					handler : function() {
						var rows = self.getSelectionModel().getSelections();
						if (rows.length == 0) {
							Ext.Msg.alert('提示', '请选择你要导出的表情！');
							return false;
						}
						self.exportEmotions(rows);
					}
				}, {
					iconCls : 'icon-export',
					text : '导出全部',
					handler : function() {
						self.exportEmotions();
					}
				} ],
		bbar : pagingBar,
		sm : new Ext.grid.RowSelectionModel(),
		loadMask : true,
		colModel : new Ext.grid.ColumnModel(
				{
					columns : [
							new Ext.grid.RowNumberer(),
							{
								header : '命令',
								dataIndex : 'cmd',
								width : 150,
								editor : {
									xtype : 'textfield',
									allowBlank : false,
									blankText : '请输入命令'
								}
							},
							{
								header : '名称',
								dataIndex : 'name',
								width : 150,
								editor : {
									xtype : 'textfield',
									allowBlank : false,
									blankText : '请输入名称'
								}
							},
							{
								header : '图片',
								dataIndex : 'pic',
								id : 'picCol' + this.platform,
								renderer : function(v, meta, rec) {
									return '<img style="border:none;margin:3px;" src="emotion-icon!showImg.action?id='
											+ rec.get('id') + '" />';
								}
							} ]
				})
	};
	this.tbarEx = new Ext.Toolbar({
		items : [ '表情命令:', {
			ref : 's_cmd',
			xtype : 'textfield',
			width : 120,
			emptyText : '输入关键字搜索...'
		}, '表情名称:', {
			ref : 's_name',
			xtype : 'textfield',
			width : 120,
			emptyText : '输入关键字搜索...'
		}, ' ', {
			text : '搜索',
			iconCls : 'icon-search',
			handler : function() {
				self.search();
			}
		}, ' ', {
			text : '全部',
			iconCls : 'icon-search',
			handler : function() {
				self.search(true);
			}
		} ]
	});

	EmotIconPanel.superclass.constructor.call(this, config);

	this.addListener('render', function() {
		this.tbarEx.render(this.tbar);
	}, this);

	var winCfg = {
		defaults : {
			border : false
		},
		modal : true,
		plain : true,
		shim : true,
		closable : true,
		closeAction : 'hide',
		collapsible : true,
		resizable : false,
		animCollapse : true,
		constrainHeader : true,
		shadow : false,
		width : 400,
		listeners : {
			'beforehide' : function(p) {
				p.form.getForm().reset();
			}
		}
	}
	var formCfg = {
		fileUpload : true,
		frame : false,
		border : false,
		labelWidth : 70,
		autoHeight : true,
		waitMsgTarget : true,
		bodyStyle : 'padding : 3px 10px; background-color:' + sys_bgcolor,
		defaults : {
			anchor : '95%',
			allowBlank : false,
			msgTarget : 'side'
		}
	};
	var suffixReg = /gif$|png$|jpg$/i;
	var _imgForm = new Ext.FormPanel(Ext.apply({
		items : [ new Ext.form.Hidden({
			name : 'platform',
			value : this.platform
		}), {
			xtype : 'textfield',
			fieldLabel : '表情命令',
			name : 'cmd',
			allowBlank : false,
			blankText : '请输入表情命令'
		}, {
			xtype : 'textfield',
			fieldLabel : '重命名',
			name : 'name',
			allowBlank : true,
			validator : function(v) {
				v = v.trim();
				if (v) {
					if (suffixReg.test(v))
						return true;
					else
						return '图片名称不正确';
				}
				return true;
			}
		}, {
			xtype : 'fileuploadfield',
			emptyText : '选择表情文件',
			fieldLabel : '图片路径',
			name : 'img',
			buttonText : '',
			buttonCfg : {
				iconCls : 'icon-upload'
			},
			validator : function(v) {
				v = v.trim();
				if (v) {
					if (suffixReg.test(v))
						return true;
					else
						return '图片格式不正确';
				}
				return true;
			}
		} ],
		tbar : [ {
			text : '保存',
			iconCls : 'icon-add',
			handler : function() {
				var btn = this;
				btn.disable();
				if (_imgForm.getForm().isValid()) {
					_imgForm.getForm().submit({
						url : 'emotion-icon!save.action',
						waitMsg : '正在上传图片...',
						success : function(form, act) {
							Dashboard.setAlert('保存成功！');
							btn.enable();
							_addWin.hide();
							self.getStore().reload();
						},
						failure : function(form, act) {
							Ext.Msg.alert('错误', act.result.message);
							btn.enable();
						}
					});
				} else {
					Ext.Msg.alert('错误', '您尚未正确完整的输入表单内容，请检查！', function() {
						btn.enable()
					});
				}
			}
		}, '-', '<font color="blue">支持png,gif和jpg格式，图片不可超过500K。</font>' ]
	}, formCfg));
	var _addWin = new Ext.Window(Ext.apply({
		title : '添加表情',
		items : [ _imgForm ]
	}, winCfg));
	_addWin.form = _imgForm;
	this.addWin = _addWin;

	var zipReg = /zip/i;
	var _uploadZipForm = new Ext.FormPanel(Ext.apply({
		items : [ new Ext.form.Hidden({
			name : 'platform',
			value : this.platform
		}), {
			xtype : 'fileuploadfield',
			emptyText : '选择zip文件',
			fieldLabel : '文件路径',
			name : 'zip',
			buttonText : '',
			buttonCfg : {
				iconCls : 'icon-upload'
			},
			validator : function(v) {
				if (v) {
					var suffix = v.substring(v.length - 3, v.length);
					if (!zipReg.test(suffix))
						return '请上传zip包文件';
					return true;
				}
				return '请选择文件';
			}
		} ],
		tbar : [ {
			text : '导入',
			iconCls : 'icon-import',
			handler : function() {
				var btn = this;
				btn.disable();
				if (_uploadZipForm.getForm().isValid()) {
					_uploadZipForm.getForm().submit({
						url : 'emotion-icon!_import.action',
						waitMsg : '正在上传图片zip包...',
						success : function(form, act) {
							btn.enable();
							_uploadWin.hide();
							var timer = new ProgressTimer({
								initData : act.result.data,
								progressId : 'emotIconImport',
								boxConfig : {
									title : '导入表情'
								},
								finish : function() {
									this.getStore().reload();
								},
								scope : self
							});
							timer.start();
						}
					});
				} else {
					Ext.Msg.alert('错误', '您尚未正确完整的输入表单内容，请检查！', function() {
						btn.enable()
					});
				}
			}
		}, '-', '<font color="blue">*仅支持zip表情包</font>' ]
	}, formCfg));
	var _uploadWin = new Ext.Window(Ext.apply({
		title : '批量导入表情',
		items : [ _uploadZipForm ]
	}, winCfg));
	_uploadWin.form = _uploadZipForm;
	this.uploadWin = _uploadWin;
}

Ext.extend(EmotIconPanel, Ext.grid.GridPanel, {
	init : function() {
		this.getStore().load()
	},
	search : function(showAll) {
		var _nameField = this.tbarEx.s_name;
		var _cmdField = this.tbarEx.s_cmd;
		if (showAll) {
			_nameField.setValue('');
			_cmdField.setValue('');
		}
		var store = this.getStore();
		for ( var key in store.baseParams) {
			if (key && key.indexOf('queryParam.') != -1)
				delete store.baseParams[key];
		}
		var _name = _nameField.getValue().trim();
		var _cmd = _cmdField.getValue().trim();
		if (_name)
			store.setBaseParam('queryParam.name', _name);
		if (_cmd)
			store.setBaseParam('queryParam.cmd', _cmd);
		store.load();
	},
	exportEmotions : function(rows) {
		var self = this;
		var hint = rows ? '所选表情' : '所有表情';
		Ext.Msg.confirm('提示', '确认导出' + hint + '？', function(btn) {
			if (btn == 'yes') {
				if (!self.downloadIFrame) {
					self.downloadIFrame = self.getEl().createChild({
						tag : 'iframe',
						style : 'display:none;'
					})
				}
				var ids = [];
				if (rows)
					Ext.each(rows, function() {
						ids.push(this.get('id'))
					});
				else
					ids.push('all');
				self.downloadIFrame.dom.src = 'emotion-icon!export.action?ids='
						+ ids.join(',') + '&platform=' + self.platform + '&ts='
						+ new Date().getTime();
			}
		})
	}
});
