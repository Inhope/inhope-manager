MenuPanel = function(_cfg) {
	_cfg.url = 'robot-account!menu.action?protocol=' + (_cfg.platform);
	_cfg.border = false;
	_cfg.html = '<iframe src="' + _cfg.url + '" frameborder=0 scrolling=auto width="100%" height="100%"></iframe>';
	MenuPanel.superclass.constructor.call(this, _cfg);
};

Ext.extend(MenuPanel, Ext.Panel, {
	loadData : function() {
	},
	externalLoad : function() {
	}
});