RobotAccountPanel = function(initCfg) {

	var self = this;
	var _platform = initCfg.platform;
	var _hideFriendLimit = 'mqq weixin yixin tsina tqq line'.indexOf(_platform) == -1 ? false : true;
	this.wxstylePlatforms = [ 'mqq', 'weixin', 'yixin' ];
	var _weixinStyle = this.wxstylePlatforms.indexOf(_platform) != -1 ? true : false;
	var _hideMsnRelated = 'msn' == _platform ? false : true;
	this.hideMsnRelated = _hideMsnRelated;
	this.platform = _platform;

	this.RobotAccRecord = Ext.data.Record.create([ 'id', 'account', 'password', 'protocol', 'enable', 'prime', 'provision', 'maxFriend', 'status',
			'node', 'miscData', 'ex0', 'ex1' ]);
	this.statusMap = {
		'0' : '未注册',
		'1' : '已经注册',
		'2' : '登录中',
		'3' : '运行中',
		'4' : '登录失败',
		'5' : '需要验证'
	};
	this.protocolData = [ [ _platform, initCfg.platformName ] ];
	this.protocolMap = Dashboard.utils.arrayDataToMap([ this.protocolData ]);
	var store = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({
			url : 'robot-account!list.action'
		}),
		reader : new Ext.data.JsonReader({
			idProperty : 'id',
			root : 'data',
			fields : this.RobotAccRecord
		})
	});
	var _yesOrNo = function(bval) {
		if (bval)
			return '是';
		return '否';
	};

	var tbarConf = [ {
		iconCls : 'icon-add',
		text : '添加账号',
		handler : function() {
			self.accTextArea.setReadOnly(false);
			self.formWin.show();
		}
	}, {
		iconCls : 'icon-edit',
		text : '编辑账号',
		handler : function() {
			var rec = self.getSelected();
			if (rec)
				self.editAccount(rec);
		}
	}, {
		iconCls : 'icon-delete',
		text : '删除账号',
		handler : function() {
			var rec = self.getSelected();
			if (rec) {
				Ext.Msg.confirm('提示', '确认删除这个账号？', function(btn) {
					if (btn == 'yes') {
						self.getEl().mask('正在删除...');
						Ext.Ajax.request({
							url : 'robot-account!delete.action',
							params : {
								id : rec.get('id')
							},
							success : function(resp) {
								self.getEl().unmask();
								Dashboard.setAlert('删除成功');
								self.getStore().reload();
							},
							failure : function(resp) {
								self.getEl().unmask();
								Ext.Msg.alert('错误', resp.responseText);
							}
						});
					}
				});
			}
		}
	}, {
		iconCls : 'icon-refresh',
		text : '刷新',
		handler : function() {
			store.reload();
		}
	} ];
	if (!_weixinStyle) {
		tbarConf.push([ {
			iconCls : 'icon-start',
			text : '启动账号',
			handler : function() {
				var rec = self.getSelected();
				if (rec)
					self.actAfterConfirm('robot-account!controlSelected.action', '启动账号' + rec.get('account'), {
						protocol : rec.get('protocol'),
						account : rec.get('account'),
						action : 'start'
					});
			}
		}, {
			iconCls : 'icon-stop',
			text : '停止账号',
			handler : function() {
				var rec = self.getSelected();
				if (rec)
					self.actAfterConfirm('robot-account!controlSelected.action', '停止账号' + rec.get('account'), {
						protocol : rec.get('protocol'),
						account : rec.get('account'),
						action : 'stop'
					});
			}
		}, '-', {
			iconCls : 'icon-start-all',
			text : '启动所有账号',
			handler : function() {
				self.actAfterConfirm('robot-account!controlAll.action', '启动所有账号', {
					action : 'start',
					protocol : _platform
				});
			}
		}, {
			iconCls : 'icon-stop-all',
			text : '停止所有账号',
			handler : function() {
				self.actAfterConfirm('robot-account!controlAll.action', '停止所有账号', {
					action : 'stop',
					protocol : _platform
				});
			}
		} ]);
	}

	var config = {
		store : store,
		columnLines : true,
		border : false,
		margins : '0 5 5 5',
		autoExpandColumn : 'accountCol' + _platform,
		tbar : tbarConf,
		sm : new Ext.grid.CheckboxSelectionModel({
			singleSelect : true
		}),
		loadMask : true,
		colModel : new Ext.grid.ColumnModel({
			columns : [ new Ext.grid.RowNumberer(), {
				header : '账号',
				dataIndex : 'account',
				id : 'accountCol' + _platform
			}, {
				header : '协议',
				width : 60,
				hidden : true,
				dataIndex : 'protocol',
				renderer : function(v) {
					return self.protocolMap[v];
				}
			}, {
				header : '是否主账号',
				hidden : _hideMsnRelated,
				dataIndex : 'prime',
				width : 100,
				renderer : function(v) {
					return _yesOrNo(v);
				}
			}, {
				header : '是否无限好友',
				hidden : _hideMsnRelated,
				width : 100,
				dataIndex : 'provision',
				renderer : function(v) {
					return _yesOrNo(v);
				}
			}, {
				hidden : _hideFriendLimit,
				header : '最大好友数',
				dataIndex : 'maxFriend',
				width : 100,
				renderer : function(v, meta, rec) {
					if (rec.get('provision'))
						return '-';
					return v;
				}
			}, {
				header : '节点',
				width : 150,
				dataIndex : 'node'
			}, {
				header : '是否启用',
				dataIndex : 'enable',
				width : 100,
				renderer : function(v) {
					return _yesOrNo(v);
				}
			}, {
				header : '帐号状态',
				dataIndex : 'status',
				width : 100,
				renderer : function(status) {
					var ret = self.statusMap[status];
					if (status == 5) { // need verify
						ret += " <a class='_verifyButton' href='javascript:void(0);' onclick='javscript:return false;'>填写</a>";
					}
					return ret;
				}
			} ]
		})
	};

	RobotAccountPanel.superclass.constructor.call(this, config);
	this.on('rowdblclick', function(g, rowIdx, evt) {
		var rec = this.getStore().getAt(rowIdx);
		this.editAccount(rec);
	}, this);
	store.on('load', function() {
		store.filter([ {
			fn : function(record) {
				return record.get('protocol') == _platform;
			}
		} ]);
	});

	var verifyImg = new Ext.form.ImageField({
		anchor : '50%',
		name : 'codeimg',
		fieldLabel : '图片'
	});
	var verifyForm = new Ext.form.FormPanel({
		frame : false,
		border : false,
		labelWidth : 70,
		autoHeight : true,
		waitMsgTarget : true,
		bodyStyle : 'padding : 3px 10px; background-color:' + sys_bgcolor,
		items : [ new Ext.form.Hidden({
			name : 'protocol'
		}), new Ext.form.Hidden({
			name : 'account'
		}), verifyImg, {
			xtype : 'textfield',
			fieldLabel : '验证码',
			maxLength : 30,
			allowBlank : false,
			name : 'code',
			anchor : '50%'
		} ],
		tbar : [ {
			text : '确定',
			iconCls : 'icon-add',
			handler : function() {
				this.disable();
				var btn = this;
				if (!verifyForm.getForm().isValid()) {
					Ext.Msg.alert('提示', '您尚未正确完整的输入表单内容，请检查！');
					btn.enable();
					return false;
				}
				Ext.Ajax.request({
					url : 'robot-account!verify.action',
					params : verifyForm.getForm().getFieldValues(),
					success : function(resp) {
						setTimeout(function() {
							Dashboard.setAlert('验证码已提交，请稍后刷新查看帐号状态！');
							verifyWin.hide();
							btn.enable();
							self.getStore().reload();
						}, 800);
					},
					failure : function(resp) {
						Ext.Msg.alert('错误', resp.responseText);
						verifyWin.hide();
						elf.enable();
					}
				});
			}
		} ]
	});
	var verifyWin = new Ext.Window({
		defaults : {
			border : false
		},
		modal : true,
		plain : true,
		shim : true,
		closable : true,
		closeAction : 'hide',
		collapsible : true,
		resizable : false,
		draggable : true,
		animCollapse : true,
		constrainHeader : true,
		shadow : false,
		width : 360,
		title : '请填写图片所示的验证码',
		items : [ verifyForm ],
		listeners : {
			'beforehide' : function(p) {
				p.editForm.getForm().reset();
			}
		}
	});
	verifyWin.editForm = verifyForm;
	this.verifyWin = verifyWin;

	var sinaVerifyField = new Ext.form.TextField({
		anchor : '98%',
		fieldLabel : '验证链接',
		readOnly : true,
		columnWidth : .75
	});
	var sinaVerifyForm = new Ext.form.FormPanel({
		frame : false,
		border : false,
		labelWidth : 60,
		autoHeight : true,
		waitMsgTarget : true,
		bodyStyle : 'padding : 3px 10px; background-color:' + sys_bgcolor,
		items : [ new Ext.form.Hidden({
			name : 'protocol'
		}), new Ext.form.Hidden({
			name : 'account'
		}), {
			layout : 'column',
			border : false,
			bodyStyle : 'background-color:' + sys_bgcolor,
			items : [ {
				layout : 'form',
				bodyStyle : 'background-color:' + sys_bgcolor,
				border : false,
				columnWidth : .9,
				items : [ sinaVerifyField ]
			}, {
				columnWidth : .1,
				border : false,
				bodyStyle : 'background-color:' + sys_bgcolor,
				items : new Ext.Button({
					text : '复制',
					handler : function() {
						Ext.copyToClipboard(sinaVerifyField.getValue());
					}
				})
			} ]
		}, {
			xtype : 'textfield',
			fieldLabel : '授权码',
			maxLength : 30,
			allowBlank : false,
			name : 'code',
			anchor : '50%'
		} ],
		tbar : [ {
			text : '确定',
			iconCls : 'icon-add',
			handler : function() {
				this.disable();
				var btn = this;
				if (!sinaVerifyForm.getForm().isValid()) {
					Ext.Msg.alert('提示', '您尚未正确完整的输入表单内容，请检查！');
					btn.enable();
					return false;
				}
				Ext.Ajax.request({
					url : 'robot-account!verify.action',
					params : sinaVerifyForm.getForm().getFieldValues(),
					success : function(resp) {
						setTimeout(function() {
							Dashboard.setAlert('验证码已提交，请稍后刷新查看帐号状态！');
							sinaVerifyWin.hide();
							btn.enable();
							self.getStore().reload();
						}, 800);
					},
					failure : function(resp) {
						Ext.Msg.alert('错误', resp.responseText);
						sinaVerifyWin.hide();
						btn.enable();
					}
				});
			}
		} ]
	});
	var sinaVerifyWin = new Ext.Window({
		defaults : {
			border : false
		},
		modal : true,
		plain : true,
		shim : true,
		closable : true,
		closeAction : 'hide',
		collapsible : true,
		resizable : false,
		draggable : true,
		animCollapse : true,
		constrainHeader : true,
		shadow : false,
		width : 420,
		title : '打开链接进行认证，并输入授权码',
		items : [ sinaVerifyForm ],
		listeners : {
			'beforehide' : function(p) {
				p.editForm.getForm().reset();
			}
		}
	});
	sinaVerifyWin.editForm = sinaVerifyForm;
	this.sinaVerifyWin = sinaVerifyWin;

	this.on('cellclick', function(grid, row, col, e) {
		var verifyBtn = e.getTarget('._verifyButton');
		var rec = this.getStore().getAt(row);
		var _verifyProtocol = rec.get('protocol');
		var _verifyAccount = rec.get('account');
		if (verifyBtn) {
			if ('fetion' == _verifyProtocol) {
				self.verifyWin.show();
				verifyImg.setValue('robot-account!showVerify.action?type=img&protocol=' + _verifyProtocol + '&account=' + _verifyAccount);
				self.verifyWin.editForm.getForm().loadRecord(rec);
			} else if ('tsina' == _verifyProtocol) {
				Ext.Ajax.request({
					url : 'robot-account!showVerify.action',
					params : {
						type : 'txt',
						protocol : _verifyProtocol,
						account : _verifyAccount
					},
					success : function(resp) {
						self.sinaVerifyWin.show();
						sinaVerifyField.setValue(resp.responseText);
						self.sinaVerifyWin.editForm.getForm().loadRecord(rec);
					},
					failure : function(resp) {
						Ext.Msg.alert('错误', resp.responseText);
					}
				});
			}
		}
	}, this);

	var _labelWidth = 80;
	var _accLable = '机器人账号';
	var _accEmtpyText = '添加多个账号请以回车隔开，每个账号一行。';
	var _pwdLable = '账号密码';
	var _tokenLable = 'Token';
	var _miscLable = '消息加解密密钥';
	var _ex0Lable = '原始ID';
	var _ex1Lable = '企业应用ID';
	if (this.wxstylePlatforms.indexOf(_platform) != -1) {
		_labelWidth = 110;
		_accLable = '公众平台AppId';
		_accEmtpyText = '请填写AppId,如没有AppId可填写账号等作为标识。';
		_pwdLable = '公众平台AppSecret';
		_tokenLable = '公众平台Token';
	}
	var isLine = _platform == 'line';
	if (isLine) {
		_labelWidth = 95;
		_accLable = 'Channel ID';
		_pwdLable = 'Channel Secret';
		_miscLable = 'Access Token';
		_ex0Lable = 'Refresh token';
		_ex1Lable = 'Expire';
	}

	var _numReg = /\d+/;
	this.accTextArea = new Ext.form.TextArea({
		xtype : 'textarea',
		fieldLabel : _accLable,
		height : 80,
		allowBlank : false,
		name : 'account',
		emptyText : _accEmtpyText
	});
	var _editForm = new Ext.form.FormPanel({
		frame : false,
		border : false,
		labelWidth : _labelWidth,
		autoHeight : true,
		waitMsgTarget : true,
		bodyStyle : 'padding : 3px 10px; background-color:' + sys_bgcolor,
		defaults : {
			anchor : '96%'
		},
		items : [ new Ext.form.Hidden({
			name : 'id'
		}), new Ext.form.Hidden({
			name : 'protocol',
			value : _platform
		}), this.accTextArea, {
			xtype : 'textfield',
			ref : 'pwdField',
			fieldLabel : _pwdLable,
			name : 'password',
			inputType : 'password',
			blankText : '字段不能为空'
		}, {
			xtype : 'textfield',
			ref : 'tokenField',
			hidden : !_weixinStyle,
			allowBlank : !_weixinStyle,
			fieldLabel : _tokenLable,
			name : 'token'
		}, {
			xtype : 'textfield',
			ref : 'miscDataField',
			hidden : !_weixinStyle && !isLine,
			allowBlank : true,
			fieldLabel : _miscLable,
			name : 'miscData',
			emptytText : '可选填'
		}, {
			xtype : 'textfield',
			ref : 'ex0Field',
			hidden : _platform != 'weixin' && !isLine,
			allowBlank : true,
			fieldLabel : _ex0Lable,
			emptytText : '可选填',
			name : 'ex0'
		}, {
			xtype : 'textfield',
			ref : 'ex1Field',
			hidden : _platform != 'weixin' && !isLine,
			allowBlank : true,
			fieldLabel : _ex1Lable,
			emptytText : '企业微信号必填',
			name : 'ex1'
		}, {
			id : 'maxFriendField' + _platform,
			xtype : 'textfield',
			hidden : _hideFriendLimit,
			fieldLabel : '最大好友数',
			name : 'maxFriend',
			allowBlank : _hideFriendLimit,
			validator : function(v) {
				if (_hideFriendLimit)
					return true;
				v = v.trim();
				if (!_numReg.test(v))
					return '请输入一个数字';
				if (parseInt(v) > 1000)
					return '最大好友数不能超过1000';
				return true;
			}
		}, {
			xtype : 'textfield',
			fieldLabel : '节点',
			name : 'node',
			allowBlank : false
		}, new Ext.form.CheckboxGroup({
			allowBlank : true,
			anchor : '100%',
			columns : 3,
			items : [ {
				boxLabel : '启用账号',
				name : 'enable',
				checked : true,
				inputValue : 'true'
			}, {
				boxLabel : '主账号',
				hidden : _hideMsnRelated,
				name : 'prime',
				inputValue : 'true'
			}, {
				boxLabel : '无限好友账号',
				hidden : _hideMsnRelated,
				name : 'provision',
				inputValue : 'true',
				listeners : {
					'check' : function() {
						var isProvision = this.getValue();
						var f = _editForm.findById('maxFriendField' + _platform);
						if (isProvision) {
							f.setValue(0);
							f.setDisabled(true);
						} else {
							f.setDisabled(false);
						}
					}
				}
			} ]
		}) ],
		tbar : [ {
			text : '保存',
			iconCls : 'icon-add',
			handler : function() {
				this.disable();
				self.saveAccount(this);
			}
		} ]
	});
	var _formWin = new Ext.Window({
		width : 480,
		title : '编辑账号',
		defaults : {
			border : false
		},
		modal : true,
		plain : true,
		shim : true,
		closable : true,
		closeAction : 'hide',
		collapsible : true,
		resizable : false,
		draggable : true,
		animCollapse : true,
		constrainHeader : true,
		shadow : false,
		listeners : {
			'beforehide' : function(p) {
				p.editForm.getForm().reset();
			}
		},
		items : [ _editForm ]
	});

	_formWin.editForm = _editForm;
	this.formWin = _formWin;
};

Ext.extend(RobotAccountPanel, Ext.grid.GridPanel, {
	init : function() {
		this.getStore().load();
	},
	getSelected : function() {
		var rec = this.getSelectionModel().getSelected();
		if (!rec) {
			Ext.Msg.alert('提示', '请选择一个账号！');
			return false;
		}
		return rec;
	},
	saveAccount : function(btn) {
		btn.disable();
		var f = this.formWin.editForm;
		if (!f.getForm().isValid()) {
			Ext.Msg.alert('提示', '您尚未正确完整的输入表单内容，请检查！');
			btn.enable();
			return false;
		}
		this.getEl().mask('正在保存...');
		this.data = f.getForm().getValues();
		var token = this.data.token;
		if (this.wxstylePlatforms.indexOf(this.platform) != -1) {
			var pwd = this.data.password;
			if (pwd)
				pwd = token + '_' + pwd;
			else
				pwd = token;
			this.data.password = pwd;
		}
		delete this.data.token;
		if (!this.data.maxFriend)
			this.data.maxFriend = 0;
		if (this.hideMsnRelated)
			this.data.provision = "true";
		var self = this;
		var params = {
			'data' : Ext.encode(self.data)
		};
		Ext.Ajax.request({
			url : 'robot-account!save.action',
			params : params,
			success : function(resp) {
				var ret = Ext.decode(resp.responseText);
				if (ret.success) {
					Dashboard.setAlert('保存成功');
					self.formWin.hide();
					self.getStore().reload();
				} else {
					Dashboard.setAlert('保存失败，请检查您的节点配置。');
				}
				btn.enable();
				self.getEl().unmask();
			},
			failure : function(resp) {
				Ext.Msg.alert('错误', resp.responseText);
				self.getEl().unmask();
				btn.enable();
			}
		});
	},
	editAccount : function(rec) {
		this.accTextArea.setReadOnly(true);
		this.formWin.show();
		this.formWin.editForm.getForm().loadRecord(rec);
		if (this.wxstylePlatforms.indexOf(this.platform) != -1) {
			var pwd = rec.get('password');
			if (pwd) {
				var idx = pwd.indexOf('_');
				var token = pwd, pwd0 = null;
				if (idx != -1) {
					token = pwd.substring(0, idx);
					pwd0 = pwd.substring(idx + 1);
				}
				this.formWin.editForm.tokenField.setValue(token ? token : '');
				this.formWin.editForm.pwdField.setValue(pwd0 ? pwd0 : '');
			}
		}
	},
	actAfterConfirm : function(url, action, params, cb) {
		var self = this;
		Ext.Msg.confirm('提示', '确认' + action + '？', function(btn) {
			if (btn == 'yes') {
				self.getEl().mask('正在' + action + '，请稍后...');
				Ext.Ajax.request({
					url : url,
					params : params,
					success : function(resp) {
						self.getEl().unmask();
						Dashboard.setAlert(action + '成功！');
						if (typeof cb == 'function')
							cb();
					},
					failure : function(resp) {
						self.getEl().unmask();
						Ext.Msg.alert('错误', resp.responseText);
					}
				});
			}
		});
	},
	externalLoad : function() {
		this.getStore().load();
	}
});
