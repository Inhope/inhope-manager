RobotAccountView = function() {

	var store = new Ext.data.ArrayStore({
		proxy : new Ext.data.MemoryProxy(),
		fields : [ 'id', 'name', 'features', 'disabled' ],
		sortInfo : {
			field : 'disabled',
			direction : 'ASC'
		}
	});
	var _data = Dashboard.defaultPlatforms.slice(0);
	var data = [];
	var _usr = new LoginUser('bot_om');
	for ( var i = 0; i < _data.length; i++) {
		var id = _data[i][0];
		if (id.indexOf('.') != -1)
			_data[i][0] = id.replace(/\./, '--');
		var featuresArr = _data[i][2];
		if (!featuresArr) {
			featuresArr = [];
			_data[i][2] = featuresArr;
		}
		for ( var j = 0; j < featuresArr.length; j++) {
			if (featuresArr[j] == 'acc')
				featuresArr[j] += '-账号列表';
			else if (featuresArr[j] == 'emot')
				featuresArr[j] += '-表情管理';
			else if (featuresArr[j] == 'tpl')
				featuresArr[j] += '-模板管理';
			else if (featuresArr[j] == 'adv')
				featuresArr[j] += '-形象套装';
			else if (featuresArr[j] == 'blst')
				featuresArr[j] += '-黑名单';
			else if (featuresArr[j] == 'friend')
				featuresArr[j] += '-用户管理';
			else if (featuresArr[j] == 'menu')
				featuresArr[j] += '-菜单管理';
		}
		if (_usr.allow('om.log.ask'))
			featuresArr.push('ommgr-askDetail-日志明细');
		if (_usr.allow('om.stat.tv'))
			featuresArr.push('ommgr-accData-日志统计');
		if (featuresArr.length)
			data.push(_data[i]);
	}
	store.loadData(data);

	var dataview = new Ext.DataView({
		store : store,
		tpl : new Ext.XTemplate('<ul>', '<tpl for=".">', '<li class="phone"><div class="wrapper">', '<img ', '<tpl if="disabled">',
				'style="-moz-opacity:0.5;opacity:.5;filter:alpha(opacity=50);"', '</tpl>',
				' width="64" height="64" src="images/robotview/{id}.png"/>', '<strong ', '<tpl if="disabled">', 'style="color:gray !important;"',
				'</tpl>', '>{name}</strong><div class="links">', '<tpl for="features">',
				'{[this.getTpl(values, parent.id, parent.disabled, xindex)]}', '</tpl>', '<tpl if="disabled">', '<div class="mask"></div>', '</tpl>',
				'</div></div></li>', '</tpl>', '</ul>', {
					getTpl : function(feature, id, disabled, idx) {
						id = id.replace(/--/g, '.');
						var tpl = '<span style="float:left;width:54px;"';
						if (disabled)
							tpl += ' class="disabled" ';
						var arr = feature.split('-');
						if (arr.length == 2)
							tpl += ' onclick="Dashboard.nav(\'' + id + '-' + arr[0] + '\', \'' + id + '\');">' + arr[1] + '</span>';
						else if (arr.length == 3)
							tpl += ' onclick="Dashboard.navExternal(\'' + arr[0] + '\',\'' + arr[1] + '\', \'' + id + '\');" >' + arr[2] + '</span>';
						return tpl;
					}
				}),
		plugins : [ new Ext.ux.DataViewTransition({
			duration : 550,
			idProperty : 'id'
		}) ],
		id : 'phones',
		itemSelector : 'li.phone',
		overClass : 'x-view-selected',
		singleSelect : true,
		multiSelect : false,
		autoScroll : true
	});

	var viewPanel = new Ext.Panel({
		title : '渠道列表',
		layout : 'fit',
		items : dataview,
		border : false,
		region : 'center'
	});
	// var webServPanel = new Ext.Panel({
	// title : '动态渠道',
	// layout : 'fit',
	// items : new RobotServiceInvoke(),
	// height : 285,
	// border : false,
	// style : 'border-top:1px solid #99bbe8;',
	// region : 'south'
	// });
	RobotAccountView.superclass.constructor.call(this, {
		border : false,
		layout : 'border',
		items : [ viewPanel ]
	});

	dataview.on('afterrender', function() {
		filterData();
	});

	function filterData() {
		store.sort('id', 'ASC');
	}

};

Ext.extend(RobotAccountView, Ext.Panel, {});