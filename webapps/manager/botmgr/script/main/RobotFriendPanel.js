RobotFriendPanel = function(initCfg) {

	var self = this;
	this.platform = initCfg.platform;

	this.RobotFriendRecord = Ext.data.Record.create([ 'userAccount', 'robotAccount', 'nick', 'location', 'createTime', 'group' ]);
	this.GroupRecord = Ext.data.Record.create([ 'id', 'name', 'createTime' ]);
	var store = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({
			api : {
				read : 'robot-friend!list.action',
				update : 'robot-friend!saveGroup.action',
			}
		}),
		reader : new Ext.data.JsonReader({
			idProperty : 'userAccount',
			root : 'data',
			fields : this.RobotFriendRecord
		}),
		writer : new Ext.data.JsonWriter({
			writeAllFields : true
		}),
		listeners : {
			beforewrite : function(store, data, rs) {
			}
		}
	});
	store.setBaseParam('platform', this.platform);

	var sm = new Ext.grid.CheckboxSelectionModel();
	var config = {
		store : store,
		columnLines : true,
		border : false,
		margins : '0 5 5 5',
		bbar : new Ext.PagingToolbar({
			store : store,
			displayInfo : true,
			pageSize : 25,
			prependButtons : true,
			beforePageText : '第',
			afterPageText : '页，共{0}页',
			displayMsg : '第{0}到{1}条记录，共{2}条',
			emptyMsg : "没有记录"
		}),
		tbar : [ {
			iconCls : 'icon-edit-group',
			text : '设置分组',
			handler : function() {
				var userRecords = self.getSelectionModel().getSelections();
				if (!userRecords) {
					Ext.Msg.alert('提示', '请先选择您需要设置分组的好友条目。');
					return false;
				}
				self.groupWin.show();
			}
		}, {
			iconCls : 'icon-push-msg',
			text : '推送消息',
			handler : function() {
			}
		}, {
			iconCls : 'icon-blocklist-mgr',
			text : '加入黑名单',
			handler : function() {
			}
		}, {
			iconCls : 'icon-refresh',
			text : '刷新',
			handler : function() {
				store.reload();
			}
		} ],
		viewConfig : {
			forceFit : true
		},
		sm : sm,
		loadMask : true,
		colModel : new Ext.grid.ColumnModel({
			columns : [ sm, new Ext.grid.RowNumberer(), {
				header : '好友ID',
				width : 200,
				dataIndex : 'userAccount'
			}, {
				header : '机器人账号',
				dataIndex : 'robotAccount',
				width : 200
			}, {
				header : '用户昵称',
				width : 120,
				dataIndex : 'nick',
			}, {
				header : '用户归属地',
				width : 120,
				dataIndex : 'location'
			}, {
				header : '创建时间',
				width : 120,
				dataIndex : 'createTime'
			}, {
				header : '用户分组',
				dataIndex : 'group',
				width : 120,
				renderer : function(v) {
					if (v)
						return v.name;
					return null;
				}
			} ]
		})
	};

	RobotFriendPanel.superclass.constructor.call(this, config);

	var _rowEditor = new Ext.ux.grid.RowEditor({
		saveText : '保存',
		cancelText : '取消',
		errorSummary : false
	});
	var _removePhantom = function(store) {
		var r, i = 0;
		while ((r = store.getAt(i++))) {
			if (r.phantom) {
				store.remove(r);
			}
		}
	};
	_rowEditor.on('canceledit', function(re, changes, rec, rowIdx) {
		_removePhantom(self.groupPanel.getStore());
	});
	var _groupPanel = new Ext.grid.GridPanel({
		columnLines : true,
		width : 300,
		height : 300,
		region : 'center',
		border : false,
		plugins : [ _rowEditor ],
		store : new Ext.data.Store({
			proxy : new Ext.data.HttpProxy({
				api : {
					read : 'robot-friend!listGroup.action',
					create : 'robot-friend!saveGroup.action',
					update : 'robot-friend!saveGroup.action',
					destroy : 'robot-friend!deleteGroup.action'
				}
			}),
			reader : new Ext.data.JsonReader({
				idProperty : 'id',
				root : 'data',
				fields : self.GroupRecord
			}),
			writer : new Ext.data.JsonWriter({
				writeAllFields : true
			}),
			listeners : {
				beforewrite : function(store, data, rec, opts) {
					if (!rec.get('name'))
						return false;
				}
			},
			autoLoad : true
		}),
		viewConfig : {
			forceFit : true
		},
		sm : new Ext.grid.CheckboxSelectionModel({
			singleSelect : true
		}),
		loadMask : true,
		tbar : [ {
			iconCls : 'icon-ok',
			text : '确定选择',
			handler : function() {
				var groupRec = self.groupPanel.getSelectionModel().getSelected();
				if (!groupRec) {
					Ext.Msg.alert('提示', '请先选择一个分组');
					return false;
				}
				var userRecords = self.getSelectionModel().getSelections();
				var userAccList = [];
				for ( var i = 0; i < userRecords.length; i++) {
					userAccList.push(userRecords[i].get('userAccount'));
				}
				self.getEl().mask('正在更新用户分组信息');
				self.groupWin.hide();
				Ext.Ajax.request({
					url : 'robot-friend!updateFriendGroup.action',
					params : {
						platform : self.platform,
						userAccList : userAccList.join(','),
						groupId : groupRec.get('id')
					},
					success : function(resp) {
						self.getEl().unmask();
						Dashboard.setAlert('设置分组成功！');
						self.getStore().reload();
					},
					failure : function(resp) {
						self.getEl().unmask();
						Ext.Msg.alert('错误', resp.responseText);
					}
				});
			}
		}, {
			iconCls : 'icon-add',
			text : '新增分组',
			handler : function() {
				var group = new self.GroupRecord({
					name : ''
				});
				_rowEditor.stopEditing();
				self.groupPanel.getStore().insert(0, group);
				self.groupPanel.getSelectionModel().selectRow(0);
				_rowEditor.startEditing(0, 1);
			}
		}, {
			iconCls : 'icon-refresh',
			text : '刷新',
			handler : function() {
				self.groupPanel.getStore().reload();
			}
		} ],
		colModel : new Ext.grid.ColumnModel({
			columns : [ {
				header : '分组名称',
				dataIndex : 'name',
				width : 100,
				editor : {
					xtype : 'textfield',
					validator : function(v) {
						v = v.trim();
						if (!v)
							return '输入分组名称';
						return true;
					}
				}
			}, {
				header : '创建时间',
				dataIndex : 'createTime',
				width : 100
			}, {
				width : 50,
				header : '操作',
				dataIndex : 'operations',
				renderer : function() {
					return "<a class='_delButton' href='javascript:void(0);' onclick='javscript:return false;'>删除</a>";
				}
			} ]
		})
	});
	_groupPanel.on('cellclick', function(grid, row, col, e) {
		var delBtn = e.getTarget('._delButton');
		if (delBtn) {
			var rec = this.getStore().getAt(row);
			Ext.Msg.confirm('提示', '确认删除？', function(btn) {
				if (btn == 'yes') {
					grid.getEl().mask('删除中...');
					Ext.Ajax.request({
						url : 'robot-friend!deleteGroup.action',
						params : {
							id : rec.get('id')
						},
						success : function(resp) {
							grid.getEl().unmask();
							Dashboard.setAlert('删除成功！');
							grid.getStore().reload();
						},
						failure : function(resp) {
							grid.getEl().unmask();
							Ext.Msg.alert('错误', resp.responseText);
						}
					});
				}
				;
			});
			return false;
		}
		;
	}, _groupPanel);

	var _groupWin = new Ext.Window({
		width : 480,
		layout : 'fit',
		title : '编辑账号',
		defaults : {
			border : false
		},
		modal : true,
		plain : true,
		shim : true,
		closable : true,
		closeAction : 'hide',
		collapsible : true,
		resizable : false,
		draggable : true,
		animCollapse : true,
		constrainHeader : true,
		shadow : false,
		listeners : {},
		items : [ _groupPanel ]
	});

	this.groupWin = _groupWin;
	this.groupPanel = _groupPanel;
};

Ext.extend(RobotFriendPanel, Ext.grid.EditorGridPanel, {
	init : function() {
		this.getStore().load();
	},
	getSelected : function() {
		var rec = this.getSelectionModel().getSelected();
		if (!rec) {
			Ext.Msg.alert('提示', '请选择一条记录！');
			return false;
		}
		return rec;
	},
	externalLoad : function() {
		this.getStore().load();
	}
});
