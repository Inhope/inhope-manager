RobotServiceExPanel = function() {
	var self = this;

	var sessionId = new Ext.form.TextField({
				name : 'sessionId',
				fieldLabel : 'sessionId',
				emptyText : '请求sessionId',
				anchor : '95%'
			});
	var userId = new Ext.form.TextField({
				name : 'userId',
				fieldLabel : 'userId',
				emptyText : '请求userId',
				anchor : '95%'
			})
	var modules = new Ext.form.TextField({
				name : 'modules',
				fieldLabel : 'modules',
				emptyText : '输入多个以","分割',
				anchor : '95%'
			})
	var tags = new Ext.form.TextField({
				name : 'tags',
				fieldLabel : 'tags',
				emptyText : '输入多个以","分割',
				anchor : '95%'
			})
	var maxReturn = new Ext.form.NumberField({
				name : 'maxReturn',
				fieldLabel : 'maxReturn',
				emptyText : '扩展问最大数',
				anchor : '95%',
				allowNegative : false
			})
	var userAttribute = new Ext.form.TextField({
				name : 'UserAttribute',
				fieldLabel : 'UserAttribute',
				emptyText : '属性,输入格式:键值对，多个以";"隔开,样例->platform:web;...',
				anchor : '95%'
			})

	var question = new Ext.form.TextArea({
				name : 'question',
				region : 'center',
				fieldLabel : 'question',
				emptyText : '问题',
				anchor : '95%',
				height : 50,
				allowBlank : false
			});

	var requestSet = new Ext.form.FieldSet({
				title : 'RobotRequest',
				collapsible : true,
				// autoHeight : true,
				defaultType : 'textfield',
				items : [sessionId, userId, modules, tags, maxReturn,
						userAttribute, question]
			});
	this.sessionId = sessionId;
	this.userId = userId;
	this.modules = userId;
	this.tags = tags;
	this.maxReturn = maxReturn;
	this.userAttribute = userAttribute;
	this.question = question;

	var content = new Ext.form.TextArea({
				name : 'content',
				height : 40,
				fieldLabel : 'content',
				emptyText : '显示content',
				anchor : '95%',
				readOnly : true
			});
	var moduleId = new Ext.form.TextField({
				name : 'moduleId',
				fieldLabel : 'moduleId',
				emptyText : '显示moduleId',
				anchor : '95%',
				readOnly : true
			});
	var nodeId = new Ext.form.TextField({
				name : 'nodeId',
				fieldLabel : 'nodeId',
				emptyText : '显示nodeId',
				anchor : '95%',
				readOnly : true
			});
	var similarity = new Ext.form.NumberField({
				name : 'similarity',
				fieldLabel : 'similarity',
				emptyText : '显示similarity',
				anchor : '95%',
				decimalPrecision : 3,
				readOnly : true
			});

	var store = new Ext.data.JsonStore({
		root : '',
		// autoLoad : false,
		anchor : '100%',
		fields : [{
					name : 'name',
					type : 'string'
				}, {
					name : 'args',
					type : 'string'
				}, {
					name : 'state',
					type : 'int'
				}]
			// data : root
		});
	this.store = store;
	var commandsView = new Ext.ListView({
				store : store,
				fieldLabel : 'commands',
				// emptyText : '显示List<RobotCommand>',
				reserveScrollOffset : true,
				anchor : '95%',
				columns : [{
							header : 'name',
							width : .20,
							dataIndex : 'name'
						}, {
							header : 'args(多个以","分割)',
							width : .70,
							dataIndex : 'args',
							tooltip : ''
						}, {
							header : 'state',
							dataIndex : 'state',
							align : 'right'
						}],
				columnResize : false
			});
	this.commandsView = commandsView;

	var relatedQuestions = new Ext.form.TextArea({
				name : 'relatedQuestions',
				fieldLabel : 'relatedQuestions',
				emptyText : '显示List<String>,以"换行"分割',
				height : 100,
				anchor : '95%',
				readOnly : true
			});
	var rTags = new Ext.form.TextField({
				name : 'tags',
				fieldLabel : 'tags',
				emptyText : '显示List<String>,以","分割',
				anchor : '95%',
				readOnly : true
			});
	var type = new Ext.form.NumberField({
				name : 'type',
				fieldLabel : 'type',
				emptyText : '显示type',
				anchor : '95%',
				readOnly : true
			});
	var responseSet = new Ext.form.FieldSet({
				title : 'RobotResponse',
				collapsible : true,
				// autoHeight : true,
				defaultType : 'textfield',
				items : [content, moduleId, nodeId, similarity, commandsView,
						relatedQuestions, rTags, type]
			});
	this.content = content;
	this.moduleId = moduleId;
	this.nodeId = nodeId;
	this.similarity = similarity;
	// this.commands = commands;
	this.relatedQuestions = relatedQuestions;
	this.rTags = rTags;
	this.type = type;
	var robotServiceExform = new Ext.form.FormPanel({
				labelWidth : 90,
				// labelAlign : 'right',
				layout : 'form',
				border : false,
				autoHeight : true,
				bodyStyle : 'background:#dfe8f6;',
				items : [requestSet, responseSet]
			});

	this.index = Ext.id();
	var wsdlForm = new Ext.Panel({
				border : false,
				bodyStyle : 'background:#dfe8f6;',
				html : '<textarea id="'
						+ self.index
						+ '" class="xml" name="code" style="width:100%;height:100%">'
						+ '</textarea>'
			});
	var cfg = {
		id : 'RobotServiceExPanel',
		border : false,
		autoWidth : true,
		layout : "border",
		region : 'center',
		items : [{
					region : 'center',
					layout : "fit",
					items : wsdlForm
				}, {
					width : 500,
					split : true,
					collapseMode : 'mini',
					region : 'east',
					layout : "fit",
					bodyStyle : 'background:#dfe8f6;',
					items : robotServiceExform,
					tbar : new Ext.Toolbar({
								items : [{
											text : 'openSession',
											ref : 'searchBtn',
											xtype : 'button',
											iconCls : 'icon-add',
											handler : function() {
												self.clearData();
												self.requestInvoke("0");
											}
										}, {
											text : 'deliver',
											ref : 'searchBtn',
											xtype : 'button',
											iconCls : 'icon-ok',
											handler : function() {
												self.clearData();
												self.requestInvoke("1");
											}
										}, {
											text : 'closeSession',
											ref : 'searchBtn',
											xtype : 'button',
											iconCls : 'icon-delete',
											handler : function() {
												self.clearData();
												self.requestInvoke("2");
											}
										}]
							})
				}]

	};
	RobotServiceExPanel.superclass.constructor.call(this, cfg);
	// this.questionText = questionText;
}
Ext.extend(RobotServiceExPanel, Ext.Panel, {
	init : function() {
		var self = this;
		Ext.Ajax.request({
					url : 'robot-service-ex!requestWsdl.action',
					success : function(resp) {
						var data = Ext.util.JSON.decode(resp.responseText).message;
						var ua = navigator.userAgent.toLowerCase();
						if (ua.indexOf("msie") > -1)
							Ext.getDom(self.index).value = data;
						else
							Ext.getDom(self.index).defaultValue = data;
						dp.SyntaxHighlighter.HighlightAll('code');
					},
					failure : function(resp) {
						Ext.Msg.alert('错误', resp.responseText)
					}
				});
	},
	clearData : function() {
		var self = this;
		self.content.setValue(null);
		self.moduleId.setValue(null);
		self.nodeId.setValue(null);
		self.similarity.setValue(null);
		self.store.removeAll();
		// self.commands.setValue(null);
		self.relatedQuestions.setValue(null);
		self.tags.setValue(null);
		self.type.setValue(null);
	},
	requestInvoke : function(operateType) {
		var self = this;
		var question = self.question.getValue();
		if (question) {
			Ext.Ajax.request({
						url : 'robot-service-ex!requestInvoke.action',
						params : {
							'operatetype' : operateType,
							'question' : question,
							'sessionId' : self.sessionId.getValue(),
							'userId' : self.userId.getValue(),
							'modules' : self.userId.getValue(),
							'tags' : self.tags.getValue(),
							'maxReturn' : self.maxReturn.getValue(),
							'userAttribute' : self.userAttribute.getValue()
						},
						success : function(resp) {
							var obj = Ext.util.JSON.decode(resp.responseText);
							var data = obj.data;
							if (data) {
								self.showReplyResponse(data, self);
							} else {
								Ext.Msg.alert('异常', obj.message);
							}
						},
						failure : function(resp) {
							Ext.Msg.alert('错误', resp.responseText)
						}
					});
		} else {
			Ext.Msg.alert('提示', 'question不能为空!');
		}
	},
	showReplyResponse : function(data, self) {
		self.content.setValue(data.content);
		self.moduleId.setValue(data.moduleId);
		self.nodeId.setValue(data.nodeId);
		self.similarity.setValue(data.similarity);
		// self.commands.setValue(data);
		self.type.setValue(data.type);
		var commands = data.commands
		var args = "";
		if (commands) {
			for (var t = 0; t < commands.length; t++) {
				var command = commands[t];
				var name = command.name;
				if (name)
					data.commands[t].name = '<span title="' + name + '">'
							+ name + '</span>';
				var arg = command.args;
				if (arg) {
					for (var i = 0; i < command.args.length; i++) {
						args += "," + command.args[i];
					}
					args = args.substring(1, args.length);
					data.commands[t].args = '<span title="' + args + '">'
							+ args + '</span>';
				}
			}
			self.store.loadData(data.commands);
		}
		var tag = data.tags;
		if (tag) {
			var tags = '';
			for (var c = 0; c < tag.length; c++) {
				tags += +"," + tag[c];
			}
			tags = tags.substring(1, tags.length);
			if (tags)
				self.tags.setValue(tags);
		}
		var str = data.relatedQuestions;
		var exq = '';
		if (str) {
			for (var c = 0; c < str.length; c++) {
				exq += (c + 1) + "." + str[c] + "\r\n";
			}
			if (exq)
				self.relatedQuestions.setValue(exq);
		}
	}
});