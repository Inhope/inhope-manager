WebTemplatePanel = function() {
	var self = this;
	this.templateid;

	this.TemplateRecord = Ext.data.Record.create(['id', 'name']);
	var _templatePanel = new Ext.grid.GridPanel({
				id : 'templatePanel',
				region : 'west',
				width : 310,
				split : true,
				columnLines : true,
				collapsible : true,
				collapseMode : 'mini',
				style : 'border-right: 1px solid ' + sys_bdcolor,
				border : false,
				title : '模板列表',
				store : new Ext.data.Store({
							proxy : new Ext.data.HttpProxy({
										url : 'web-template!list.action'
									}),
							reader : new Ext.data.JsonReader({
										idProperty : 'id',
										root : 'data',
										fields : self.TemplateRecord
									})
						}),
				tbar : [{
							iconCls : 'icon-add',
							text : '新增模板',
							handler : function() {
								_templateForm.items.item(0).setReadOnly(false);
								self.addTemplate();
							}
						}, {
							iconCls : 'icon-edit',
							text : '修改模板',
							handler : function() {
								_templateForm.items.item(0).setReadOnly(true);
								var rec = self.hasSelectedTempla();
								if (rec)
									self.editTemplate(rec);
							}
						}, {
							iconCls : 'icon-delete',
							text : '删除',
							handler : function() {
								self.deleteFn(self.templatePanel,
										'web-template!delete.action');
							}
						}, {
							iconCls : 'icon-refresh',
							text : '刷新',
							handler : function() {
								self.templatePanel.getStore().reload();
							}
						}, {
							iconCls : 'icon-preview',
							text : '预览',
							handler : function() {
								self.previewFn(self.templatePanel);
							}
						}],
				sm : new Ext.grid.RowSelectionModel({
							singleSelect : true
						}),
				loadMask : true,
				autoExpandColumn : 'nameCol',
				colModel : new Ext.grid.ColumnModel({
							columns : [new Ext.grid.RowNumberer(), {
										header : 'id',
										id : 'idCol',
										dataIndex : 'id'
									}, {
										header : '名称',
										id : 'nameCol',
										dataIndex : 'name'
									}]
						})
			});

	_templatePanel.getStore().on('load', function() {
				this.getSelectionModel().selectRow(0);
			}, _templatePanel)
	_templatePanel.on('rowdblclick', function(g, rowIdx, evt) {
				var rec = g.getStore().getAt(rowIdx);
				self.editTemplate(rec);
			}, _templatePanel);
	var _lastSelectedId;
	_templatePanel.getSelectionModel().on('rowselect',
			function(sm, rowIdx, rec) {
				var sel = sm.getSelected();
				var currId = sel.get('id');
				if (currId === _lastSelectedId)
					return false;
				else
					_lastSelectedId = currId;
				if (rec) {
					self.templateid = rec.get('id');
					_itemPanel.getStore().reload({
								params : {
									'templateId' : rec.get('id')
								}
							});
				}
			});

	var _formCfg = {
		frame : false,
		border : false,
		labelWidth : 70,
		autoHeight : true,
		waitMsgTarget : true,
		bodyStyle : 'padding : 3px 10px; background-color:' + sys_bgcolor
	}
	var _templateForm = new Ext.form.FormPanel(Ext.apply(_formCfg, {
				items : [{
							xtype : 'textfield',
							fieldLabel : '模板id',
							maxLength : 30,
							allowBlank : false,
							name : 'id',
							anchor : '98%'
						}, {
							xtype : 'textfield',
							fieldLabel : '模板名称',
							maxLength : 30,
							allowBlank : false,
							name : 'name',
							anchor : '98%'
						}],
				tbar : [{
					text : '保存',
					iconCls : 'icon-add',
					handler : function() {
						this.disable();
						self.saveFn('web-template!save.action',
								self.templatePanel.getStore(),
								self.templateWin, self.templateWin.editForm,
								this);
					}
				}]
			}));

	var _winCfg = {
		defaults : {
			border : false
		},
		modal : true,
		plain : true,
		shim : true,
		closable : true,
		closeAction : 'hide',
		collapsible : true,
		resizable : false,
		draggable : true,
		animCollapse : true,
		constrainHeader : true,
		shadow : false
	}

	var _templateWin = new Ext.Window(Ext.apply({
				width : 360,
				title : '编辑活动',
				items : [_templateForm],
				listeners : {
					'beforehide' : function(p) {
						p.editForm.getForm().reset();
					}
				}
			}, _winCfg));
	_templateWin.editForm = _templateForm;
	this.templateWin = _templateWin;

	var isImgItem = function(type) {
		if (type === 1) {
			return false;
		}
		if (type === 2) {
			return true;
		}
	}
	this.ItemRecord = Ext.data.Record.create(['id', 'data', 'name', 'type']);
	var _itemPanel = new Ext.grid.GridPanel({
		id : 'templateItemPanel',
		region : 'center',
		title : '模板元素设置',
		columnLines : true,
		style : 'border-left: 1px solid ' + sys_bdcolor + ' border-right: 1px solid ' + sys_bdcolor,
		border : false,
		store : new Ext.data.Store({
					proxy : new Ext.data.HttpProxy({
								url : 'web-template!listItem.action'
							}),
					reader : new Ext.data.JsonReader({
								idProperty : 'id',
								root : 'data',
								fields : self.ItemRecord
							})
				}),
		tbar : [{
					iconCls : 'icon-refresh',
					text : '刷新',
					handler : function() {
						self.itemPanel.getStore().reload();
					}
				}],
		sm : new Ext.grid.RowSelectionModel({
					singleSelect : true
				}),
		loadMask : true,
		autoExpandColumn : 'tempaleContentCol',
		colModel : new Ext.grid.ColumnModel({
			columns : [new Ext.grid.RowNumberer(), {
						header : 'id',
						dataIndex : 'id'
					}, {
						header : '位置名称',
						width : 90,
						dataIndex : 'name',
						renderer : function(v, meta, rec) {
							return v;
						}
					}, {
						header : '内容',
						id : 'tempaleContentCol',
						dataIndex : 'data',
						renderer : function(v, meta, rec) {
							if (isImgItem(rec.get('type'))) {
								return '<img style="border:none;display:none;align:middle" src="'
										+ rec.get('data')
										+ '?ts='
										+ new Date().getTime()
										+ '" onload="adjustImageSize(this,'
										+ (_itemPanel.getColumnModel()
												.getColumnWidth(3) - 10)
										+ ')" />';

							} else {
								return v;
							}
						}
					}, {
						width : 80,
						header : '操作',
						dataIndex : 'operations',
						renderer : function(v, meta, rec) {
							var ret = "<a class='_viewButton' href='javascript:void(0);' onclick='javscript:return false;'>编辑</a>";
							return ret;
						}
					}]
		})
	});

	_itemPanel.on('cellclick', function(grid, row, col, e) {
				var editBtn = e.getTarget('._viewButton');
				var rec = this.getStore().getAt(row);
				if (editBtn) {
					self.itemWin.show();
					if (rec.get('type') === 2) {
						_switchTab.setActiveTab(2);
						_switchTab.get('templateImgTab').setDisabled(false);
						_switchTab.get('templateTxtTab').setDisabled(true);
						_switchTab.get('templateCssTab').setDisabled(true);
						self.itemWin.imgForm.getForm().loadRecord(rec);
					} else if (rec.get('type') === 1) {
						_switchTab.setActiveTab(0);
						_switchTab.get('templateTxtTab').setDisabled(false);
						_switchTab.get('templateImgTab').setDisabled(true);
						_switchTab.get('templateCssTab').setDisabled(true);
						self.itemWin.txtForm.getForm().loadRecord(rec);
					} else {
						_switchTab.setActiveTab(1);
						_switchTab.get('templateCssTab').setDisabled(false);
						_switchTab.get('templateImgTab').setDisabled(true);
						_switchTab.get('templateTxtTab').setDisabled(true);
						self.itemWin.cssForm.getForm().loadRecord(rec);
					}
				}
			}, _itemPanel);

	var _imgItemForm = new Ext.FormPanel(Ext.apply(_formCfg, {
		fileUpload : true,
		defaults : {
			anchor : '95%',
			msgTarget : 'side'
		},
		items : [new Ext.form.Hidden({
							name : 'id'
						}), {
					id : 'templateNameField',
					xtype : 'textfield',
					fieldLabel : '位置名称',
					readOnly : true,
					name : 'name'
				}, {
					xtype : 'fileuploadfield',
					emptyText : '选择一幅图片',
					fieldLabel : '上传路径',
					name : 'img',
					buttonText : '',
					buttonCfg : {
						iconCls : 'icon-upload'
					}
				}],
		tbar : [{
			text : '保存',
			iconCls : 'icon-add',
			handler : function() {
				var btn = this;
				btn.disable();
				if (_imgItemForm.getForm().isValid()) {
					_imgItemForm.getForm().submit({
						url : 'web-template!saveImgItem.action',
						waitMsg : '正在上传图片...',
						params : {
							'templateid' : self.templatePanel
									.getSelectionModel().getSelected()
									.get('id')
						},
						success : function(form, act) {
							Ext.Msg.alert('提示', '上传成功', function() {
										btn.enable();
										_itemWin.hide();
										_itemPanel.getStore().reload();
									});
						},
						failure : function(form, act) {
							Ext.Msg.alert('错误', act.result.message);
							btn.enable();
						}
					});
				} else {
					Ext.Msg.alert('错误', '您尚未正确完整的输入表单内容，请检查！', function() {
								btn.enable()
							});
				}
			}
		}, '-', '<font color="blue">头像须为96*96的GIF,JPG或PNG图片,场景图片须小于100K</font>']
	}));
	var _txtItemForm = new Ext.form.FormPanel(Ext.apply(_formCfg, {
				defaults : {
					anchor : '92%',
					// allowBlank : false,
					msgTarget : 'side'
				},
				items : [new Ext.form.Hidden({
									name : 'id'
								}), {
							xtype : 'textfield',
							fieldLabel : '位置名称',
							name : 'name',
							readOnly : true,
							allowBlank : false
						}, {

							xtype : 'textarea',
							fieldLabel : '内容',
							name : 'data'
						}],
				tbar : [{
					text : '保存',
					iconCls : 'icon-add',
					handler : function() {
						this.disable();
						var rec = self.templatePanel.getSelectionModel()
								.getSelected();
						self.saveFn('web-template!saveTxtItem.action',
								self.itemPanel.getStore(), self.itemWin,
								self.itemWin.txtForm, this, {
									'templateid' : rec.get('id')
								});
					}
				}]
			}));
	var _cssItemForm = new Ext.form.FormPanel(Ext.apply(_formCfg, {
				defaults : {
					anchor : '92%',
					// allowBlank : false,
					msgTarget : 'side'
				},
				items : [new Ext.form.Hidden({
									name : 'id'
								}), {
							xtype : 'textfield',
							fieldLabel : '位置名称',
							name : 'name',
							readOnly : true,
							allowBlank : false
						}, {
							xtype : 'textarea',
							fieldLabel : '颜色值',
							name : 'data'
						}],
				tbar : [{
					text : '保存',
					iconCls : 'icon-add',
					handler : function() {
						this.disable();
						var rec = self.templatePanel.getSelectionModel()
								.getSelected();
						self.saveFn('web-template!saveCssItem.action',
								self.itemPanel.getStore(), self.itemWin,
								self.itemWin.cssForm, this, {
									'templateid' : rec.get('id')
								});
					}
				}]
			}));
	var _switchTab = new Ext.TabPanel({
				activeTab : 0,
				border : false,
				resizeTabs : true,
				tabWidth : 100,
				enableTabScroll : true,
				layoutOnTabChange : true
			});
	_switchTab.add({
				id : 'templateTxtTab',
				layout : 'fit',
				title : '文字设置',
				items : _txtItemForm
			});
	_switchTab.add({
				id : 'templateCssTab',
				layout : 'fit',
				title : '颜色设置',
				items : _cssItemForm
			});
	_switchTab.add({
				id : 'templateImgTab',
				layout : 'fit',
				title : '图片设置',
				items : _imgItemForm
			});
	var _itemWin = new Ext.Window(Ext.apply({
				title : '编辑模板位',
				width : 400,
				closable : true, // 关闭
				items : [_switchTab],
				listeners : {
					'beforehide' : function(p) {
						// _switchTab.setActiveTab(0);
						// _switchTab.get('templateImgTab').setDisabled(false);
						p.imgForm.getForm().reset();
						p.txtForm.getForm().reset();
						p.cssForm.getForm().reset();
					}
				}
			}, _winCfg));
	_itemWin.imgForm = _imgItemForm;
	_itemWin.txtForm = _txtItemForm;
	_itemWin.cssForm = _cssItemForm;
	this.itemWin = _itemWin;

	this.templatePanel = _templatePanel;
	this.itemPanel = _itemPanel;

	var config = {
		id : 'webTemplatePanel',
		layout : 'border',
		border : false,
		items : [_templatePanel, _itemPanel]
	};

	WebTemplatePanel.superclass.constructor.call(this, config);
	this.on('onTabActive', function() {
			});
};

Ext.extend(WebTemplatePanel, Ext.Panel, {
	init : function() {
		var self = this;
		Ext.Ajax.request({
					url : 'web-template!init.action',
					success : function(resp) {
						self.robotAddr = resp.responseText.replace(/"/g, '');
						self.templatePanel.getStore().load();
					}
				});

	},
	externalLoad : function() {
		this.init();
	},
	hasSelectedTempla : function() {
		var rec = this.templatePanel.getSelectionModel().getSelected();
		if (!rec) {
			Ext.Msg.alert('提示', '请先在活动列表里选择一条记录！');
			return false;
		}
		return rec;
	},
	addTemplate : function() {
		this.templateWin.show();
	},
	editTemplate : function(rec) {
		this.templateWin.show();
		var f = this.templateWin.editForm.getForm().loadRecord(rec);
	},
	afterSave : function(btn, win) {
		btn.enable();
		this.getEl().unmask();
		win.hide();
	},
	saveFn : function(url, st, win, form, btn, extraParams) {
		var self = this;
		var f = form.getForm();
		if (!f.isValid()) {
			Ext.Msg.alert('提示', '您尚未正确完整的输入表单内容，请检查！');
			btn.enable();
			return false;
		}
		this.getEl().mask('保存中...');
		var vals = f.getFieldValues();
		if (!vals.id)
			vals.id = null;
		params = {
			'data' : Ext.encode(vals)
		}
		if (extraParams)
			Ext.apply(params, extraParams);
		Ext.Ajax.request({
					url : url,
					params : params,
					success : function(resp) {
						var rst = Ext.decode(resp.responseText);
						if (rst.message) {
							Ext.Msg.alert('提示', rst.message);
						} else {
							Dashboard.setAlert('保存成功！');
							st.reload();
						}
						self.afterSave(btn, win);
					},
					failure : function(resp) {
						Ext.Msg.alert('错误', resp.responseText);
						self.afterSave(btn, win);
					}
				});
	},
	deleteFn : function(grid, url) {
		var self = this;
		var rows = grid.getSelectionModel().getSelections();
		if (rows.length == 0) {
			Ext.Msg.alert('提示', '请选择一条记录删除！');
			return false;
		}
		Ext.Msg.confirm('提示', '确认删除？', function(btn) {
					if (btn == 'yes') {
						self.getEl().mask('删除中...');
						var ids = [];
						Ext.each(rows, function() {
									ids.push(this.id)
								});
						Ext.Ajax.request({
									url : url,
									params : {
										ids : ids.join(',')
									},
									success : function(resp) {
										self.getEl().unmask();
										Dashboard.setAlert('删除成功！');
										grid.getStore().reload();
									},
									failure : function(resp) {
										self.getEl().unmask();
										Ext.Msg.alert('错误', resp.responseText);
									}
								})
					}
				});
	},
	previewFn : function(grid) {
		var rec = grid.getSelectionModel().getSelected();
		if (!rec) {
			Ext.Msg.alert('提示', '请选择一条记录进行预览！');
			return false;
		}
		window.open(this.robotAddr + 'template/themes/' + rec.get('id') + '/');
	}
});
function adjustImageSize(img, limit) {
	var width = img.width;
	var height = img.height;
	if (width > limit) {
		if (width >= height) {
			img.width = width * (limit / width);
		}
	}
	if (height > 50) {
		if (width < height) {
			img.width = width * (50 / height);
			img.height = height * (50 / height);
		}
	}
	img.style.display = 'block';
}
