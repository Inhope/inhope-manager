<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link type="text/css"  rel="stylesheet" href="css/apiUser.css"/>
<link type="text/css"  rel="stylesheet" href="css/account.css"/>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.json.js"></script>
<script type="text/javascript" src="js/menu.js"></script>
<script type="text/javascript">window.protocol='weixin'; <c:if test="${empty menu}">noMenuData = true;</c:if></script>
<title>微信菜单</title>
</head>
<body style="background:none;">
     <div>
		 <div class="con" style="width:auto;padding:5px 15px;">
			  <div style="width:776px; float:left;">
			        <div style="width:776px; margin:10px 0 5px 0;">
					    <div class="right_con" style="padding:0;border:none;">
						<div class="Contentboxtwo ov_fl">
						
						<div id="jsweb_c4" class="Contentboxdiv1" style="height:auto;padding:0;">
							<div class="CM-dsp">
					        	<div class="CMOP-customtitle"><p><a id="addRootMenuBtn" href="#" class="wx-button06"></a></p><p class="CM-cl03">主菜单名最多16个字节，子菜单名最多40个汉字，问句128字节以内，链接256个字节以内。(汉子、中文标点为2字节)</p></div>
					            <div class="CMOP-customcontent ov_fl">
					            
					            	<div class="CMOP-customleft f_l">
					                	<div class="CMOP-ct01">
					                        <p style="width:112px;">菜单名</p>
					                        <p style="width:181px;">内容</p>
					                        <p style="width:104px;background:none;">操作</p>
					                    </div>
					                    
					                    <div class="CMOP-ct02 ov_fl">
											<table width="395" border="0" cellspacing="0" cellpadding="0">
											  <tbody id="menuContainer">
										  		<c:if test="${not empty menu}">
													<c:forEach items="${menu}" var="m">
													  <c:choose>
													  <c:when test="${not empty m.key}"><c:set var="menuVal" value="${m.key}" /></c:when>
													  <c:otherwise><c:set var="menuVal" value="${m.url}" /></c:otherwise>
													  </c:choose>
													  <tr name="rootMenu" state="display">
												        <td width="111" height="30" align="center" valign="middle" bgcolor="#f8f8f8" class="CMOP-cttd01">
												        	<div><span title="${m.name}"  name="menuName">${m.name}</span><input name="menuName_i" value="" style="display:none;width:100px;"/></div>
												        </td>
												        <td width="181" height="30" align="center" valign="middle" bgcolor="#f8f8f8" class="CMOP-cttd01">
												        	<div><span title="${menuVal}"  name="menuKey">${menuVal}</span><input name="menuKey_i" value="" style="display:none;width:150px;"/></div>
												        </td>
												        <td width="104" height="30" align="right" valign="middle" bgcolor="#f8f8f8" class="CMOP-cttd03">
												        	<a style="margin-right:15px;" href="#" class="CM-cta03"></a> <a href="#" class="CM-cta02"></a> <a href="#" class="CM-cta01"></a>　
												        </td>
												      </tr>
												      <c:if test="${not empty m.sub_button}">
													  <c:forEach items="${m.sub_button}" var="sm">
													  <c:choose>
													  <c:when test="${not empty sm.key}"><c:set var="subMenuVal" value="${sm.key}" /></c:when>
													  <c:otherwise><c:set var="subMenuVal" value="${sm.url}" /></c:otherwise>
													  </c:choose>
													  <tr name="menuItem" state="display">
												        <td width="111" height="30" align="center" valign="middle" class="CMOP-cttd01 CM-cl03">
												        	<div><span title="${sm.name}" name="menuName">${sm.name}</span><input name="menuName_i" value="" style="display:none;width:100px;border:1px solid #f8f8f8;"/></div>
												        </td>
												        <td width="181" height="30" align="center" valign="middle" class="CMOP-cttd01 CM-cl03">
												        	<div><span title="${subMenuVal}" name="menuKey">${subMenuVal}</span><input name="menuKey_i" value="" style="display:none;width:150px;border:1px solid #f8f8f8;"/></div>
												        </td>
												        <td width="104" height="30" align="right" valign="middle" class="CMOP-cttd03">
												        	<a style="margin-right:15px;" href="#" class="CM-cta05"></a> <a href="#" class="CM-cta04"></a>　
												        </td>
												      </tr>
												      </c:forEach>
												      </c:if>
											     </c:forEach>
											     </c:if>
											  </tbody>
											</table>
					
					                  </div>
					                  
					                   <ul style="list-style:none;" class="CMOP-ct03">
					                       <li>创建自定义菜单后，由于微信客户端缓存，需要24小时微信客户端才会展现出来。建议测试时可以尝试取消关注公众账号后，再次关注，则可以看到创建后的效果；</li>
					                       <li>只有设定过AppID和Secret后，自定义菜单管理才能生效；</li>
					                       <li>订阅号用户可以发送申请邮件到<a href="mailto:weixinmp@qq.com">weixinmp@qq.com</a>申请自定义菜单权限；</li>
					                       <li>若菜单项内容以http://或https://开头则该菜单项点击触发后是链接跳转的效果；</li>        
					                   </ul>
					                   
					                   <div class="CMOP-ct04">
										<a href="#" id="saveMenuBtn" class="wx-button04 f_l">保　存</a>
					                   	<a href="#" id="deployMenuBtn" class="wx-button04 f_l" style="margin-left:15px;">发布到微信</a>
					                   	</div>
					                   <p>&nbsp;</p> <p>&nbsp;</p> <p>&nbsp;</p> <p>&nbsp;</p> <p>&nbsp;</p> <p>&nbsp;</p> <p>&nbsp;</p> <p>&nbsp;</p> <p>&nbsp;</p>
					              </div>
					                <div class="CMOP-customright f_l ov_fl">
										<div class="weixin-menucn f_l" style="margin-top:-10px;">
											<div id="previewCont" style="display:none;" class="weixin-ul-one1 ov_fl">
												<div class="weixin-ul-one2">
												<div class="weixin-ul-one3">
												<ul id="previewSubItems">
												</ul>
												</div>
												</div>
											</div>
										<ul id="previewRootItems" class="weixin-ul-two">
										</ul>
										</div>
					                </div>
					            </div>
					        </div>
						</div>
				
				</div></div>
	 </div></div></div></div>
</body>
</html>
