<%@page import="com.incesoft.xmas.commons.LicenceHolder"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:directive.page
	import="org.springframework.web.context.support.WebApplicationContextUtils" />
<jsp:directive.page
	import="com.incesoft.xmas.commons.PropertyPlaceholderRecorder" />
<jsp:directive.page
	import="org.springframework.beans.factory.config.PropertyPlaceholderConfigurer" />
<%@taglib prefix="res" uri="/WEB-INF/tld/import-res.tld"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="usr" uri="/WEB-INF/tld/login-user.tld"%>
<%
	ApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(application);
	PropertyPlaceholderRecorder placeHolder = (PropertyPlaceholderRecorder) ctx
			.getBean(PropertyPlaceholderRecorder.class);
	request.setAttribute("projectName", placeHolder.getProperty("projectName"));
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title><c:if test="${not empty projectName}">${projectName } - </c:if>小i智能机器人统一管理平台</title>
		<base target="_self" />
		<link rel="stylesheet" type="text/css" href="custom.css" />
		<link rel="shortcut icon" type="image/x-icon" href="../favicon.ico" />
		<%@include file="/commons/extjsvable.jsp"%>

		<link rel="stylesheet" type="text/css" href="<c:url value='/ext/vtab/ext-vtab.css'/>" />
		<script type="text/javascript" src="<c:url value='/ext/vtab/ext-vtab.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/authmgr/script/SysInfoPanel.js'/>"></script>
		<usr:checkPerm jsExportVar="c_check_perm_result" permissionNames="c_app,c_preset,c_common,c_monitor,auth" />
		<res:import dir="script" recursive="true" include=".*\.js" />
		<style>
#header {
	background: url("images/head_bg.jpg") repeat-x scroll 0 0 #1E4176;
	border: 0 none;
	padding: 0 3px;
	margin: 0;
}

#header .api-title {
	background: url(images/head_left_bg.jpg) no-repeat left top;
	color: white;
	height: 31px;
	/*margin: 0 0 0 5px;*/
	padding: 6px 0 0 100px;
}

a.blue_link {
	font-size: 12px;
	text-decoration: none;
	color: #0559c2;
	font-family: 宋体;
	font-weight: normal
}

a.blue_link:hover,a.blue_link:active {
	font-size: 12px;
	text-decoration: underline;
}
</style>
	</head>
	<body>
		<div id="header">
			<div class="api-title" style="position: relative">
				<div style="background: url(images/head_logo.jpg) no-repeat; position: absolute; left: 0; top: 0; width: 135px; height: 31px;"></div>
				<div style="color: #fff; font-family: '宋体'; font-size: 16px; font-weight: bold; margin: 2px 0 0 35px;"><%=((PropertyPlaceholderRecorder) WebApplicationContextUtils.getWebApplicationContext(application).getBean(PropertyPlaceholderRecorder.class)).getProperty("systemName")%>
					<span style="color: #fff0ad; font-family: '宋体'; font-size: 12px; font-weight: normal; margin: 0 0 0 0;"><%=((PropertyPlaceholderRecorder) WebApplicationContextUtils.getWebApplicationContext(application).getBean(PropertyPlaceholderRecorder.class)).getProperty("projectName")%></span>
				</div>
				<div
					style="background: url(images/head_right_bg.jpg) no-repeat right bottom;position: absolute;right:0;top:0;width: 400px; height: 36px;">
					<div style="position: absolute; right: 20px; top: 9px;">
						<span style="color: #fff000; font-family: '宋体'; font-size: 12px;font-weight: bold;">${licenceExpireInXDay} </span>
						<span style="color: #fff0ad; font-family: '宋体'; font-size: 14px;"><usr:name /> </span>
						<span><a target="_blank" style="color: #cde2ff; font-family: '宋体'; font-size: 12px;" href="login.jsp">登陆应用平台</a> <a style="color: #cde2ff; font-family: '宋体'; font-size: 12px;" href="../authmgr/auth!logout.action">注销</a> </span>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
