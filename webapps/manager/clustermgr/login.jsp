<%@page import="java.net.URLEncoder"%>
<%@page
	import="com.eastrobot.commonsapi.ibotcluster.ClusterConfigServiceHelper"%>
<%@page import="com.eastrobot.commonsapi.ibotcluster.ClusterAppService"%>
<%@page import="com.incesoft.xmas.authmgr.web.AuthAction"%>
<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:directive.page
	import="org.springframework.web.context.support.WebApplicationContextUtils" />
<jsp:directive.page
	import="org.springframework.beans.factory.config.PropertyPlaceholderConfigurer" />
<jsp:directive.page
	import="com.incesoft.xmas.commons.PropertyPlaceholderRecorder" />
<%
	ClusterAppService mgrappsvc = (ClusterAppService) WebApplicationContextUtils
			.getWebApplicationContext(application).getBean(ClusterAppService.class);
	request.setAttribute("cates", mgrappsvc.listCategoryWithApps());
	Cookie[] cks = request.getCookies();
	String _rm_username = "";
	if (cks != null && cks.length > 0) {
		for (Cookie c : cks) {
			if ("_rm_username".equals(c.getName())) {
				_rm_username = c.getValue();
				break;
			}
		}
	}
	request.setAttribute("extjsvable_excludeCSS", true);
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="css/css.css">
<%@include file="/commons/extjsvable.jsp"%>
<title>iBotCluster 智能机器人集群</title>
<script>
	(function() {
		if (getTopWindow() != window) {
			getTopWindow().location = window.location
			return;
		}
		var error_code = encodeURIComponent('${param.error_code}');
		if (error_code == '1') {
			window.location = '../unimgr/error.jsp?msg='
					+ encodeURIComponent('请求的资源未授权给当前登陆用户,请联系管理员');
		}
	})();
	var markInvalid = function(msg, target) {
		target = target || 'username'
		Ext.getBody().unmask();
		Ext.each([ 'username', 'password', 'captcha' ], function(t) {
			var cls1 = t == 'captcha' ? 'form-p3' : 'form-p2';
			var cls2 = t == 'captcha' ? 'form-p5' : 'form-p4';
			if (t == target) {
				Ext.getDom(t).title = msg;
				var p = Ext.get(t).parent();
				p.removeClass(cls1);
				p.addClass(cls2);
			} else {
				Ext.getDom(t).title = '';
				var p = Ext.get(t).parent();
				p.removeClass(cls2);
				p.addClass(cls1);
			}
		})
		Ext.get(target).focus();
		if (Ext.getDom('captchaImg'))
			reloadImg(Ext.getDom('captchaImg'))
	}

	var authCallback = function(response, options) {
		var host = options.host;
		var result = Ext.decode(response.responseText);
		if (result.success) {
			Ext.getBody().mask('登录成功，正在为您转到目标页面', 'x-mask-loading');
			window.location = result.data;
		} else {
			if (result.message != 'probe')
				markInvalid(result.message, result.data);
			Ext.getBody().unmask();
		}
	}
	var doAuth = function(probe) {

		var username = Ext.getDom('username').value
		var password = Ext.getDom('password').value
		var captcha = Ext.getDom('captcha') ? Ext.getDom('captcha').value : ''
		var appURINodes = Ext.getBody().query(
				'input[type=radio][name=appURI]:checked');
		var appURI = appURINodes.length ? appURINodes[0].value : ''
		var cloudLogin = Ext.getDom('cloudLogin').checked

		if (!probe) {
			if (!username) {
				markInvalid('缺少用户名', 'username');
				return;
			}
			if (!password) {
				markInvalid('缺少密码', 'password');
				return;
			}
			if (!cloudLogin && !appURI) {
				alert('请先选择您要登陆的项目');
				return;
			}
		}

		Ext.getBody().mask('登录中...', 'x-mask-loading');
		Ext.Ajax.request({
			url : (!cloudLogin ? '../app/' + appURI + '/' : '')
					+ 'auth!login.action',
			params : {
				username : username,
				password : username == 'INCE.SUPERVISOR'?password:CryptoJS.SHA1(password).toString(),
				captcha : captcha,
				probe : probe,
				cluster : true,
				remberUsername : "_rm_username"
			},
			success : authCallback,
			failure : markInvalid
		});
	}
	Ext.onReady(function() {
		Ext.get('username').on('keydown', function(e) {
			if (e.getKey() == 13)
				doAuth();
		});
		Ext.get('password').on('keydown', function(e) {
			if (e.getKey() == 13)
				doAuth();
		});
		if (Ext.get('captcha'))
			Ext.get('captcha').on('keydown', function(e) {
				if (e.getKey() == 13)
					doAuth();
			});
		Ext.get('loginButton').on('click', function(e) {
			doAuth();
		});
		/*Ext.get('resetButton').on('click',function(e){
			Ext.getDom('username').value = window.initUsername
			Ext.getDom('password').value = window.initPassword
			Ext.get('username').focus();
		});*/
		window.initUsername = Ext.getDom('username').value
		window.initPassword = Ext.getDom('password').value
		Ext.get('username').focus();
	})
	function reloadImg(img) {
		if (img)
			img.src = img.src.replace(/\?.*/, '') + '?' + new Date().getTime()
	}
</script>
</head>

<body>
	<div class="wrapbox">
		<!--header content start-->
		<div class="header">
			<div class="header-content">
				<div class="logo f-l ov-fl">
					<img src="images/logo.jpg">
				</div>
				<div class="form-content f-r ov-fl">
					<p class="form-p1 f-l">用户名：</p>
					<p class="form-p2 f-l ov-fl">
						<input id="username" type="text"
							value="<%=URLEncoder.encode(_rm_username)%>" style="width: 122px;">
					</p>
					<p class="form-p1 f-l">&nbsp;&nbsp;密 码：</p>
					<p class="form-p2 f-l ov-fl">
						<input id="password" type="password"
							value="${requestScope._rm_password}" style="width: 122px;">
					</p>
					<%
						if (AuthAction.isCaptchaEnabled()) {
					%>
					<p class="form-p1 f-l">&nbsp;&nbsp;验证码：</p>
					<p class="form-p3 f-l ov-fl">
						<input id="captcha" type="text" value="" style="width: 38px;">
					</p>
					<p class="form-p1 f-l">
						<img alt="看不清点击换一张" tabindex="4"
							style="height: 24px; width: 54px; cursor: pointer;"
							id="captchaImg" src='auth!captcha.action'
							onclick="reloadImg(this)" />
					</p>
					<%
						}
					%>
					<p class="form-p1 f-l">&nbsp;&nbsp;集群管理：</p>
					<p class="form-p1 f-l" style="padding: 3px 0px 0px 0px;">
						<input id="cloudLogin" type="checkbox" value="1">
					</p>
					<p class="form-p1 f-l" style="padding-left: 30px;">
						<a id="loginButton" href="#" class="login"></a>
					</p>
				</div>
			</div>
		</div>
		<!--header content end-->

		<!--footer content start-->
		<div class="wrap">
			<!--middle content start-->
			<div class="content">
				<c:set var="cateCount" value="1" />
				<c:forEach var="cate" items="${cates}" varStatus="status">
					<c:if test="${cate.name != '公共库'}">
						<div class="col1">
							<h2>${cate.name}</h2>
							<ul>
								<c:set var="cateCount" value="${cateCount+1}" />
								<c:forEach var="app" items="${cate.apps}">
									<li onclick="this.childNodes[0].checked=true"><input
										type="radio" class="radio" name="appURI" value="${app.uri}" /><span
										style="cursor: pointer"> ${app.name}(${app.uri}) </span></li>
								</c:forEach>
							</ul>
						</div>
					</c:if>
				</c:forEach>
				<c:forEach begin="${cateCount}" end="8">
					<div class="col2"></div>
				</c:forEach>
			</div>
			<!--middle content end-->

			<!--footer conteht start-->
			<div class="copyright">Copyright © 2013 www.xiaoi.com 版权所有</div>
			<!--footer content end-->

		</div>
		<!--footer content end-->
	</div>
</body>
</html>
