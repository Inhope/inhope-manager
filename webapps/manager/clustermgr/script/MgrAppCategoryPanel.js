var MgrAppCategoryPanel = Ext.extend(Ext.grid.EditorGridPanel, {
			initComponent : function() {
				var listStore = new Ext.data.JsonStore({
							url : 'mgr-app!category.action',
							fields : ['id', {
										name : 'name',
										allowBlank : false
									}],
							root : 'data',
							autoLoad : true,
							writer : new Ext.data.JsonWriter({
										writeAllFields : true
									})
						})
				Ext.apply(this, {
							loadMask : true,
							clicksToEdit : 1,
							store : listStore,
							tbar : [{
										text : '新增',
										iconCls : 'icon-add',
										handler : function() {
											this.addEntry()
										},
										scope : this
									}, {
										text : '刷新',
										iconCls : 'icon-refresh',
										handler : function() {
											listStore.reload()
										},
										scope : this
									}, {
										text : '删除',
										iconCls : 'icon-delete',
										handler : function() {
											var rs = this.getSelectionModel().getSelections();
											if (rs && rs.length) {
												listStore.remove(rs)
											}
										},
										scope : this
									}],
							colModel : new Ext.grid.ColumnModel({
										defaults : {
											sortable : true
										},
										columns : [new Ext.grid.RowNumberer(), {
													header : '分类名字',
													dataIndex : 'name',
													width : 150,
													editor : {
														xtype : 'textfield'
													}
												}]
									}),
							sm : new Ext.grid.RowSelectionModel()
						})
				MgrAppCategoryPanel.superclass.initComponent.call(this)
			},
			addEntry : function() {
				var d = {}
				this.getStore().recordType.prototype.fields.each(function(f) {
							d[f.name] = null
						})
				var r = new (this.getStore().recordType)(d);
				this.stopEditing();
				this.getStore().insert(0, r);
				this.startEditing(0, 1);
			}
		})
