var MgrAppMainPanel = Ext.extend(Ext.Panel, {
			initComponent : function() {
				var mainPanel = new Ext.TabPanel({
							region : 'center',
							activeTab : 0,
							border : false,
							style : 'border-left: 1px solid ' + sys_bdcolor,
							resizeTabs : true,
							tabWidth : 150,
							minTabWidth : 120,
							enableTabScroll : true,
							layoutOnTabChange : true,// with afterLayout
							listeners : {
								afterLayout : function() {

								}
							}
						});
				var tabCtxMenu = new Ext.menu.Menu({
							items : [{
										text : '关闭其他标签页',
										iconCls : 'icon-status-cancel',
										width : 50,
										handler : function() {
											var atab = this.getActiveTab();
											var c = this.items.getCount();
											for (var i = c - 1; i >= 0; i--) {
												if (atab != this.items.get(i)) {
													mainPanel.remove(this.items.get(i).id);
												}
											}
										},
										scope : mainPanel
									}]
						});
				mainPanel.on('contextmenu', function(tabp, targetp, e) {
							tabCtxMenu.showAt(e.getXY())
						})
				var r = MgrAppMainPanel.navRegistry.sort(function(nav1, nav2) {
							// if (nav1.cls == WordNavPanel)return -1;
							// if (nav2.cls == WordNavPanel)return 1;
							return nav1.sort - nav2.sort
						});

				var tabRegistry = {};

				var navItems = [];

				for (var i = 0; i < r.length; i++) {
					var nav = r[i].createNav();
					nav.moduleIndex = i;
					nav.showTab = function(panelClassOrCreateFn, tabId, title, iconCls, closable) {
						if (!panelClassOrCreateFn)
							return false;
						var tab = mainPanel.get(tabId);
						if (!tab) {
							var tabPanel = !panelClassOrCreateFn.creator ? new panelClassOrCreateFn() : panelClassOrCreateFn();
							tabPanel.navPanel = this;
							tabPanel.tabId = tabId;
							tabPanel.tab = tab = mainPanel.add({
										id : tabId,
										layout : 'fit',
										title : title,
										items : tabPanel,
										iconCls : iconCls,
										closable : closable
									});
							tabPanel.addEvents('tabactive', 'tabinactive')
							tabPanel.close = function() {
								nav.closeTab(this.tabId)
							}
							tab.on('close', function() {
										return tabPanel.fireEvent("close", tabPanel);
									});
							tab.on('beforeclose', function() {
										return tabPanel.fireEvent("beforeclose", tabPanel);
									});
							tabRegistry[tabId] = this.moduleIndex;
							tab.show();
						} else {
							mainPanel.activate(tabId);
							if (title)
								tab.setTitle(title)
							if (iconCls)
								tab.setIconClass(iconCls)
						}
						return tab.get(0);
					};
					nav.closeTab = function(tabId) {
						mainPanel.remove(tabId)
					};

					if (i == 0) {
						var f = function(node) {
							// //
							// this.showObjectTab({id:'9f29e4d2ee0d4264ab8e6c09ed853009',ispub:false})
							if (node.childNodes && node.childNodes.length) {
								node.childNodes[0].expand();
								(function() {
									node.childNodes[0].fireEvent('click', node.childNodes[0]);
								}).defer(100);
							}
							this.removeListener('load', f)
						}
						nav.on('load', f, nav)
					}
					navItems.push(nav);
				}

				var navPanel = new Ext.Panel({
							region : 'west',
							width : 300,
							minSize : 175,
							maxSize : 400,
							split : true,
							collapsible : true,
							collapseMode : 'mini',
							border : false,
							style : 'border-right: 1px solid ' + sys_bdcolor,
							layout : 'accordion',
							title : '控制台',
							items : navItems
						});

				this.items = [navPanel, mainPanel]
				this.layout = 'border'
				MgrAppMainPanel.superclass.initComponent.call(this)
				mainPanel.on('tabchange', function(tabPanel, tab) {
							if (!tab)
								return;
							var navIndex = tabRegistry[tab.id];
							if (typeof navIndex == 'undefined')
								return;
							navPanel.layout.setActiveItem(navIndex);

						})
				mainPanel.on('beforetabchange', function(tabPanel, newtab, currenttab) {
							if (currenttab)
								currenttab.get(0).fireEvent('tabinactive')
							if (newtab)
								newtab.get(0).fireEvent('tabactive')
						})
			}
		})

Ext.apply(MgrAppMainPanel, {
			navRegistry : [],
			registerNav : function(navClass, sort) {
				this.navRegistry.push({
							cls : navClass,
							'createNav' : function(id) {
								return new navClass({
											'id' : id
										})
							},
							'sort' : sort
						})
			}
		});