var MgrAppMonitorPanel = Ext.extend(Ext.Panel, {
			layout : 'fit',
			initComponent : function() {
				var res = ux.util.httpRequest('mgr-app!listAllApp.action')
				var apps = Ext.decode(res);
				var cate_apps = {}
				Ext.each(apps, function(a) {
							var cateapps = cate_apps[a.cateName];
							if (!cateapps) {
								cateapps = cate_apps[a.cateName] = [];
							}
							cateapps.push(a)
						}, this)
				var btngrps = []
				for (var key in cate_apps) {
					if (key == '公共库')
						continue;
					var grp = {
						xtype : 'buttongroup',
						title : key,
						width : 200,
						columns : 2,
						defaults : {
							scale : 'small',
							width : 100
						},
						items : []
					}
					btngrps.push(grp)
					var cateapps = cate_apps[key]
					Ext.each(cateapps, function(cateapp) {
								grp.items.push({
											text : cateapp.name,
											app : cateapp,
											iconCls : 'icon-mgrapp',
											enableToggle : true,
											toggleGroup : 'mgrappcate',
											handler : function(btn) {
												this.contentP.setLocation('../app/' + btn.app.uri + '/clustermgr/sysinfo-wrap.jsp')
											},
											scope : this
										})
							}, this)
				}
				this.tbar = btngrps
				this.items = [this.contentP = new IFramePanel()]
				MgrAppMonitorPanel.superclass.initComponent.call(this)
				this.on('afterrender', function() {
							var maxHeight = 0
							this.getTopToolbar().items.each(function(item) {
										if (item.getHeight() > maxHeight) {
											maxHeight = item.getHeight()
										}
									}, this)
							this.getTopToolbar().items.each(function(item) {
										if (item.getHeight() < maxHeight) {
											item.setHeight(maxHeight)
										}
									}, this)
							this.getTopToolbar().doLayout()
						})
			}
		})
