var REF_DEFAULTVALUE = 1;
var REF_ENTRY = 2;
var REF_CATE = 3;

var MgrAppPanel = Ext.extend(Ext.Panel, {
			initComponent : function() {
				var listP = this.listP = new MgrAppTreePanel({
							region : 'west'
						})
				var editP = this.editP = new MgrAppEditPanel({
							region : 'center'
						})
				listP.getSelectionModel().on('rowselect', function(sm, row, r) {
							this.editP.loadData(r.id)
						}, this)
				this.items = [listP, editP]
				MgrAppPanel.superclass.initComponent.call(this)
			},
			layout : 'border'
		})
var MgrAppTreePanel = Ext.extend(Ext.tree.TreePanel, {
			width : 300,
			minSize : 175,
			maxSize : 400,
			title : '应用管理',
			collapsible : true,
			rootVisible : false,
			border : false,
			split : true,
			autoScroll : true,
			enableDD : true,
			initComponent : function() {
				Ext.apply(this, {
							dataUrl : 'mgr-app!list.action',
							root : {
								lazyLoad : true,
								id : '0',
								text : 'root'
							},
							tbar : [{
										ref : 'addAppBtn',
										text : '新增应用',
										iconCls : 'icon-add',
										handler : this.addApp,
										scope : this,
										disabled : true
									}, {
										text : '新增分类',
										iconCls : 'icon-add',
										handler : this.addCate,
										scope : this
									}, {
										text : '删除',
										iconCls : 'icon-delete',
										handler : this.deleteNode,
										scope : this
									}, {
										text : '编辑',
										iconCls : 'icon-edit',
										handler : function() {
											var n = this.getSelectedNode();
											if (this.isApp(n)) {
												this.editApp(n)
											} else {
												this.editCate(n)
											}
										},
										scope : this
									}, {
										text : '刷新',
										iconCls : 'icon-refresh',
										handler : function() {
											this.refreshNode()
										},
										scope : this
									}]
						});
				MgrAppTreePanel.superclass.initComponent.call(this)
				this.on("click", this.editNode)
				this.on('cateSaved', function() {
							this.refreshNode()
						}, this)
				// DD
				this.on('nodedragover', function(e) {
							if (!this.isApp(e.dropNode) && !this.isApp(e.target) && e.point != 'append' // cate
									|| this.isApp(e.dropNode) && // app
									(this.isApp(e.target) && e.point != 'append' || !this.isApp(e.target) && e.point == 'append')) {
								return true;
							}
							return false;
						})
				// this.on('beforenodedrop', function(e) {
				this.on('movenode', function(tree, node, oldpnode, newpnode, i) {
							var c = Ext.Msg.confirm('提示', '确定要移动&nbsp&nbsp' + node.text + '&nbsp&nbsp吗', function(btn, text) {
										if (btn == 'yes') {
											var swapid = []
											var nodes;
											if (this.isApp(node)) {
												nodes = newpnode.childNodes;
											} else {
												nodes = this.getRootNode().childNodes;
											}
											Ext.each(nodes, function(n) {
														swapid.push(n.id)
													}, this)
											Ext.Ajax.request({
														url : 'mgr-app!move.action',
														params : {
															cateId : this.isApp(node) ? newpnode.id : '',
															swapId : swapid.join(',')
														},
														success : function(form, action) {
															Dashboard.setAlert('移动成功')
														},
														failure : function(response) {
															source.appendChild(node);
															source.expand();
															Ext.Msg.alert("错误", response.responseText);
														}
													});
										} else {
											newpnode.removeChild(node)
											if (oldpnode.reload)
												oldpnode.reload();
										}
									}, this);
						})
			},
			// model
			editNode : function(n) {
				if (this.isApp(n)) {
					this.editApp(n)
					this.getTopToolbar().addAppBtn.disable()
				} else {
					this.getTopToolbar().addAppBtn.enable()
					if (this.editP)
						this.editP.disable()
				}
			},
			isApp : function(n) {
				return n.attributes.attachment.cateId
			},
			deleteNode : function() {
				var n = this.getSelectedNode();
				if (n) {
					if (this.isApp(n)) {
						this.deleteApp(n)
					} else {
						this.deleteCate(n)
					}
				} else {
					Ext.Msg.alert('提示', '请先选择要删除的分类或应用')
				}
			},
			showAppEditP : function(id, name) {
				this.editP = this.showTab(MgrAppEditPanel, 'MgrAppEditPanel-' + (id || Ext.id()), id ? '编辑-' + name : '新增应用', 'icon-mgrapp', true);
				if (!this.editP.eventBind) {
					this.editP.on('appSaved', function(app) {
								if (app && app.cateId) {
									this.getNodeById(app.cateId).reload()
								}
							}, this)
					this.editP.eventBind = true;
				}
			},
			addApp : function() {
				if (this.getSelectedNode()) {
					this.showAppEditP()
					this.editP.loadData('', this.getSelectedNode().id);
				}
			},
			editApp : function(n) {
				this.showAppEditP(n.id, n.text)
				this.editP.loadData(n.id);
			},
			deleteApp : function(n) {
				if (this.editP)
					this.editP.disable();
				Ext.Ajax.request({
							url : 'mgr-app!delete.action',
							params : {
								id : n.id
							},
							success : function(response) {
								Dashboard.setAlert('删除成功');
								this.refreshNode(n.parentNode)
							},
							scope : this
						})
			},
			addCate : function(n) {
				// prompt(title, msg, fn, scope, multiline, value)
				this.saveOrUpdateCate({})
			},
			editCate : function(n) {
				this.saveOrUpdateCate(n.attributes.attachment)
			},
			saveOrUpdateCate : function(d) {
				Ext.Msg.prompt('编辑分类', '请输入分类名称', function(btn, text) {
							if (btn == 'ok') {
								d.name = text
								Ext.Ajax.request({
											url : 'mgr-app!category.action',
											params : {
												xaction : 'update',
												data : Ext.encode(d)
											},
											success : function(response) {
												var result = Ext.decode(response.responseText);
												if (result) {
													this.fireEvent('cateSaved')
													Dashboard.setAlert('保存成功')
												} else
													Ext.Msg.alert('错误', '保存失败')
											},
											scope : this
										})
							}
						}, this, null, d.name ? d.name : '');
			},
			deleteCate : function(n) {
				Ext.Msg.confirm('确认', '你确定要删除分类 ' + n.text + ' 吗？', function(btn, text) {
							if (btn == 'yes') {
								Ext.Ajax.request({
											url : 'mgr-app!category.action',
											params : {
												xaction : 'destroy',
												data : Ext.encode(n.id)
											},
											success : function(response) {
												var result = Ext.decode(response.responseText);
												if (result) {
													this.fireEvent('cateSaved')
													Dashboard.setAlert('删除成功')
												} else
													Ext.Msg.alert('错误', '删除失败')
											},
											scope : this
										})
							}
						}, this);
			},
			// helper
			getSelectedNode : function() {
				return this.getSelectionModel().getSelectedNode();
			},
			refresh : function(node) {
				if (node.reload || node.isExpanded())
					node.reload();
				else
					node.expand();

			},
			refreshNode : function(n) {
				if (!n)
					n = this.getRootNode()
				this.refresh(n);
			}
		})
var MgrAppEditPanel = Ext.extend(Ext.Panel, {
			layout : 'border',
			border : false,
			disabled : true,
			disable : function(reset) {
				if (reset || reset === null)
					this.reset()
				MgrAppEditPanel.superclass.disable.call(this)
			},
			initComponent : function() {
				this.tbar = [{
							text : '保存',
							iconCls : 'icon-save',
							handler : function() {
								this.saveData()
							},
							scope : this
						}, {
							text : '刷新',
							iconCls : 'icon-refresh',
							handler : function() {
								this.loadData(this.loadDataId)
								this.presetEntrySelector.getStore().reload()
								this.editFormP.find('hiddenName', 'cateId')[0].getStore().reload()
							},
							scope : this
						}, {
							text : '创建数据库',
							iconCls : 'icon-ontology-root-add',
							handler : function() {
								var dd = {}
								this.editGridP.getStore().each(function(r) {
											dd[r.data.propkey] = r.data.propvalue
										})
								var initvalue = (dd['jdbc.username'] || '') + ',' + (dd['jdbc.username'] || '') + ',' + (dd['jdbc.password'] || '')
								Ext.Msg.prompt('创建数据库配置', '请填写需要创建的数据库配置，格式：数据库名,用户名,密码', function(result, value) {
											if (result == 'ok' && value) {
												var d = value.split(',')
												Ext.Ajax.request({
															url : 'mgr-app!createDB.action',
															params : {
																dbname : d[0],
																dbusername : d[1],
																dbpassword : d[2]
															},
															success : function(response) {
																Ext.Msg.alert('提示', '创建成功')
															},
															scope : this
														});
											}
										}, this, '', initvalue);
							},
							scope : this
						}, {
							text : '初始化数据库',
							iconCls : 'icon-ontology-root-add',
							handler : function() {
								var d = {}
								this.editGridP.getStore().each(function(r) {
											d[r.data.propkey] = r.data.propvalue
										})
								Ext.Ajax.request({
											url : 'mgr-app!initDB.action',
											params : {
												username : d['jdbc.username'],
												password : d['jdbc.password'],
												driver : d['jdbc.driver'],
												url : d['jdbc.url']
											},
											success : function(form, action) {
												var timer = new ProgressTimer({
															initData : Ext.decode(form.responseText),
															progressId : 'mgrapp.createdb',
															boxConfig : {
																title : '初始化数据库'
															},
															finish : function(progress) {
															}
														});
												timer.start();
											},
											scope : this
										});
							},
							scope : this
						},{
							text : '初始化数据库(全部)',
							iconCls : 'icon-ontology-root-add',
							handler : function() {
								var d = {}
								Ext.Ajax.request({
											url : 'mgr-app!initDB.action',
											params : {
												initall : 'true'
											},
											success : function(form, action) {
												var timer = new ProgressTimer({
															initData : Ext.decode(form.responseText),
															progressId : 'mgrapp.createdb',
															boxConfig : {
																title : '初始化数据库(全部)'
															},
															finish : function(progress) {
															}
														});
												timer.start();
											},
											scope : this
										});
							},
							scope : this
						}]
				var cbo;
				var editFormP = this.editFormP = new Ext.form.FormPanel({
							region : 'north',
							autoHeight : true,
							border : false,
							bodyStyle : 'padding:4px 0 0 5px',
							items : [{
										xtype : 'textfield',
										name : 'name',
										fieldLabel : '应用名称',
										allowBlank : false
									}, {
										xtype : 'textfield',
										name : 'uri',
										fieldLabel : '访问路径',
										allowBlank : false,
										listeners : {
											blur : function(f) {
												this.setConfigValueByURI(f.getValue())
											},
											scope : this
										}
									}, {
										xtype : 'textfield',
										hidden : true,
										name : 'priority'
									}, cbo = new Ext.form.ComboBox({
												triggerAction : 'all',
												mode : 'remote',
												hiddenName : 'cateId',
												fieldLabel : '所属分类',
												readOnly : true,
												store : new Ext.data.JsonStore({
															url : 'mgr-app!category.action?xaction=read',
															fields : ['id', 'name'],
															root : 'data',
															autoLoad : true,
															listeners : {
																load : function() {
																	cbo.setValue(cbo.getValue())
																}
															}
														}),
												valueField : 'id',
												displayField : 'name'
											}), {
										xtype : 'checkbox',
										name : 'disabled',
										inputValue : 1,
										fieldLabel : '是否禁用'
									}, {
										xtype : 'textfield',
										name : 'id',
										hidden : true
									}]
						})
				var editGridStore = new DataOrderGroupingStore({
							reader : new Ext.data.JsonReader({
										fields : 'id propkey name propvalue presetCateId presetCateName presetEntryId presetEntryName cateId cateName defaultValue'.split(' '),
										root : 'configs'
									}),
							groupField : 'cateId'
						})
				var presetEntrySelector = this.presetEntrySelector = new Ext.form.ComboBox({
							editable : false,
							triggerAction : 'all',
							mode : 'remote',
							store : new Ext.data.JsonStore({
										url : 'preset-config!listEntry.action',
										fields : ['propkey', 'name', 'id', 'propvalue'],
										root : 'data',
										autoLoad : true
									}),
							valueField : 'id',
							displayField : 'name'
						});
				presetEntrySelector.on('focus', function() {
							this.onTriggerClick.defer(100, this, []);
						})
				presetEntrySelector.getStore().on('load', function() {
							this.filter('propkey', presetEntrySelector.propkey)
						})
				var editGridP = this.editGridP = new Ext.grid.EditorGridPanel({
							region : 'center',
							bodyStyle : 'border-left:none;border-bottom:none',
							store : editGridStore,
							clicksToEdit : 1,
							autoExpandColumn : 'propvalue',
							colModel : new Ext.grid.ColumnModel({
										columns : [new Ext.grid.RowNumberer(), {
													header : '属性名',
													dataIndex : 'name',
													width : 100,
													renderer : function(v, meta, r) {
														meta.attr = 'style="background-color:#ccccbb"'
														return v
													}
												}, {
													id : 'propvalue',
													header : '属性值',
													dataIndex : 'propvalue',
													renderer : function(v, meta, r) {
														var flag = r.get('defaultValue')
														if (flag) {
															var c;
															if (flag == REF_DEFAULTVALUE) {
																c = 'e0e0e0'
															} else if (flag == REF_CATE) {
																c = 'e8e8e8'
															} else if (flag == REF_ENTRY) {
																c = 'eeeeee'
															}
															if (c)
																meta.attr = 'style="background-color:#' + c + '"'
														}
														return v
													},
													editor : {
														xtype : 'textfield'
													}
												}, {
													dataIndex : 'cateId'
												}, {
													width : 50,
													header : '引用预设配置名',
													dataIndex : 'presetEntryId',
													editor : presetEntrySelector,
													renderer : function(v, m, r) {
														return r.get('presetEntryName')
													}
												}]
									}),
							view : new Ext.grid.GroupingView({
										headersDisabled : true,
										hideGroupedColumn : true,
										forceFit : true,
										groupOnSort : false,
										groupTextTpl : '{[this.getV(values,"cateName")?this.getV(values,"cateName"):"自定义属性"]}<span id=""'
												+ ' class="presetCateFieldHolder" presetCateId="{[this.getV(values,\'presetCateId\')]}"'
												+ ' presetCateName="{[this.getV(values,\'presetCateName\')]}"' //
												+ ' cateId="{[this.getV(values,\'cateId\')]}"></span>',
										listeners : {
											beforerefresh : function() {
												Ext.apply(this.startGroup, {
															getV : function(values, field) {
																var ret = ''
																Ext.each(values.rs, function(r) {
																			if (ret = r.get(field))
																				return false;
																		})
																return ret;
															}
														})
											}
										}
									})
						})
				editGridP.on('beforeedit', function(e) {
							presetEntrySelector.propkey = e.record.get('propkey')
							presetEntrySelector.getStore().filter('propkey', presetEntrySelector.propkey)
						})
				editGridP.on('afteredit', function(e) {
							var r = e.record, col = e.column
							this.editGridP_afteredit(r, col)
						}, this)
				this.editGridP_afteredit = function(r, col) {
					var editorSt = presetEntrySelector.getStore();
					editorSt.clearFilter(true)
					var dataIndex = this.editGridP.getColumnModel().getDataIndex(col)
					if (dataIndex == 'presetEntryId') {
						for (var key in r.data) {
							if (key && key.indexOf('presetCate') == 0) {
								delete r.data[key]
							}
						}
						var rr = editorSt.getById(r.get('presetEntryId'))
						if (rr) {
							r.data.presetEntryName = rr.get('name')
							r.data.propvalue = rr.get('propvalue')
							r.set('defaultValue', 2)
							editGridP.getView().refreshRow(e.row)
						}
					} else if (dataIndex == 'propvalue') {
						r.beginEdit()
						for (var key in r.data) {
							if (key && key.indexOf('preset') == 0) {
								delete r.data[key]
							}
						}
						r.set('defaultValue', 0)
						r.endEdit()
					}

				}
				// patch grouping view header
				editGridP.on('groupmousedown', function(grid, field, groupValue, e) {
							return false;
						})
				editGridP.on('cellclick', function(grid, row, col, e) {
							if (this.getColumnModel().getDataIndex(col) == 'name') {
								this.startEditing(row, editGridP.getColumnModel().findColumnIndex('propvalue'))
								return false;
							}
						})
				editGridP.getView().on('refresh', function() {
							var presetCateFieldHolders = editGridP.getEl().query('.presetCateFieldHolder')
							// this.presetCateSelectors = {}
							Ext.each(presetCateFieldHolders, function(h) {
								var presetCateEditor = new Ext.form.ComboBox({
											emptyText : '点击引用预设配置组...',
											typeAhead : true,
											triggerAction : 'all',
											lazyRender : true,
											mode : 'remote',
											width : 200,
											value : presetCateFieldHolders.presetCateId,
											store : new Ext.data.JsonStore({
														url : 'preset-config!listPresetCategory.action?withEntrys=1&cateId=' + h.getAttribute('cateId'),
														fields : ['id', 'name'],
														root : 'data'
													}),
											valueField : 'id',
											displayField : 'name',
											listeners : {
												select : this.onPresetCateIdChange,
												scope : this
											}
										});
								presetCateEditor.render(h)
								if (h.getAttribute('presetCateName')) {
									presetCateEditor.setRawValue(h.getAttribute('presetCateName'))
								}
									// this.presetCateSelectors[presetCateFieldHolders.presetCateId]
									// = presetCateEditor;
								}, this)
						}, this)
				this.items = [editFormP, editGridP]
				MgrAppEditPanel.superclass.initComponent.call(this)
			},
			loadData0 : function(data) {
				this.loadDataId = data.id
				this.editFormP.getForm().loadRecord(new Ext.data.Record(data));
				this.editGridP.getStore().loadData(data);
			},
			reset : function() {
				this.editFormP.getForm().reset()
				this.editGridP.getStore().removeAll();
			},
			loadData : function(appid, cateId) {
				if (this.inProgress)
					return;
				this.enable();
				this.editFormP.getForm().reset()
				this.loadDataId = appid
				Ext.Ajax.request({
							url : 'mgr-app!load.action',
							params : {
								id : appid || ''
							},
							success : function(response) {
								var result = Ext.decode(response.responseText);
								if (result) {
									if (cateId)
										result.data.cateId = cateId;
									this.loadData0(result.data)
								} else
									Ext.Msg.alert('错误', '加载失败')
							},
							scope : this
						})
			},
			saveData : function() {
				if (!this.editFormP.getForm().isValid()) {
					Dashboard.setAlert('请先填写完整内容后保存', 'ERROR')
					return
				}
				var vs = this.editFormP.getForm().getValues();
				if (!vs.id)
					delete vs.id
				if (!vs.cateId)
					delete vs.cateId
				vs.configs = []
				var presetCateRef = {}
				this.editGridP.getStore().each(function(r) {
							if (r.data.defaultValue == 0 && r.get('propvalue') && r.get('propvalue').trim() || r.data.defaultValue == REF_ENTRY) {
								vs.configs.push(r.data)
							} else if (r.data.defaultValue == REF_CATE) {
								if (!presetCateRef[r.data.presetCateId]) {
									presetCateRef[r.data.presetCateId] = true;
									vs.configs.push(r.data)
								}
							}
						})
				this.disable(false)
				this.inProgress = true
				Ext.Ajax.request({
							url : 'mgr-app!save.action',
							params : {
								data : Ext.encode(ux.util.resetEmptyString(vs))
							},
							success : function(response) {
								delete this.inProgress
								this.enable()
								var result = Ext.decode(response.responseText);
								if (result) {
									this.loadData0(result.data)
									Dashboard.setAlert('保存成功')
									this.fireEvent('appSaved', result.data)
								} else
									Ext.Msg.alert('错误', '加载失败')
							},
							failure : function() {
								delete this.inProgress
								this.enable()
							},
							scope : this
						})
			},
			onPresetCateIdChange : function(combo, r, row) {
				var presetCateId = r.id
				Ext.Ajax.request({
							url : 'preset-config!loadPresetCategory.action',
							params : {
								id : presetCateId
							},
							success : function(response) {
								var result = Ext.decode(response.responseText);
								if (result) {
									var configs = result.data.configs
									this.editGridP.getStore().each(function(rr) {
												Ext.each(configs, function(d) {
															if (d.propkey == rr.get('propkey')) {
																rr.beginEdit()
																rr.set('propvalue', '')
																delete rr.data.presetEntryId
																delete rr.data.presetEntryName
																rr.set('presetCateId', presetCateId)
																rr.set('propvalue', d.propvalue)
																rr.set('defaultValue', 3)
																rr.endEdit()
																return false;
															}
														})
											}, this)
								} else
									Ext.Msg.alert('错误', '加载失败')
							},
							scope : this
						})
			},
			generateFullURI : function(loc, uri) {
				var locs = loc.split(',')
				for (var i = 0; i < locs.length; i++) {
					locs[i] = locs[i].replace(/(https?:\/\/(?:[^/]+\/){2})(.*?)/g, '$1app/' + uri + '/$2')
				}
				return locs.join(',')
			},
			setConfigValueByURI : function(uri) {
				if (!uri)
					return
				var path = this.contextpath;
				if (!this.contextpath) {
					path = this.contextpath = {}
					path['robot.context_path'] = ux.util.httpRequest('property!get.action?key=robot.context_path')
					path['robot.context_path.internal'] = ux.util.httpRequest('property!get.action?key=robot.context_path.internal')
				}
				this.editGridP.getStore().each(function(r) {
							var key = r.get('propkey')
							var ov = r.get('propvalue')
							if ((!ov || ov == this.lasturi) && (key == 'robot.project_id' || key == 'projectID')) {
								r.set('propvalue', uri)
								this.editGridP_afteredit(r, 2)
							} else if (key == 'robot.context_path' || key == 'robot.context_path.internal') {
								if (!ov || this.lasturi && this.generateFullURI(path[key], this.lasturi) == ov) {
									r.set('propvalue', this.generateFullURI(path[key], uri))
									this.editGridP_afteredit(r, 2)
								}
							}
						}, this)
				this.lasturi = uri;
			}
		})
MgrAppMainPanel.registerNav(MgrAppTreePanel, 1)