var PresetConfigMainPanel = Ext.extend(Ext.Panel, {
			initComponent : function() {
				this.layout = 'border'
				var mainPanel = this.mainPanel = new Ext.TabPanel({
							region : 'center',
							border : false,
							style : 'border-left: 1px solid ' + sys_bdcolor,
							resizeTabs : true,
							tabWidth : 250,
							minTabWidth : 200,
							enableTabScroll : true,
							layoutOnTabChange : true
						});
				this.items = [new NavPresetConfigPanel({
									region : 'west',
									split : true,
									width : 200
								}), mainPanel]
				PresetConfigMainPanel.superclass.initComponent.call(this);
			},
			showTab : function(panelClassOrCreateFn, tabId, title, iconCls, closable) {
				if (!panelClassOrCreateFn)
					return false;
				var tab = this.mainPanel.get(tabId);
				if (!tab) {
					var tabPanel = !panelClassOrCreateFn.creator ? new panelClassOrCreateFn() : panelClassOrCreateFn();
					tabPanel.tabId = tabId;
					tabPanel.tab = tab = this.mainPanel.add({
								id : tabId,
								layout : 'fit',
								title : title,
								items : tabPanel,
								iconCls : iconCls,
								closable : closable
							});
					tab.show();
				} else {
					this.mainPanel.activate(tabId);
					if (title)
						tab.setTitle(title)
					if (iconCls)
						tab.setIconClass(iconCls)
				}
				return tab.get(0);
			}
		})
var NavPresetConfigPanel = function(cfg) {
	var self = this;
	var menu = [{
				id : '1',
				name : '预设配置组管理',
				module : 'presetcate',
				icon : 'icon-toolbox',
				panelClass : PresetConfigCatePanel
			}, {
				id : '2',
				name : '预设配置项管理',
				module : 'presetentry',
				icon : 'icon-toolbox',
				panelClass : PresetConfigEntryPanel
			}];
	var rootNode = new Ext.tree.TreeNode({
				id : '0',
				text : 'root'
			});
	var _nodeMap = {};

	for (var i = 0; i < menu.length; i++) {
		var _item = menu[i];
		var _node = new Ext.tree.TreeNode({
					id : _item.id,
					text : _item.name,
					iconCls : _item.icon,
					module : _item.module,
					panelClass : _item.panelClass,
					handler : _item.handler
				});
		_nodeMap[_item.id] = _node;
		if (!_item.parent)
			rootNode.appendChild(_node);
		else
			_nodeMap[_item.parent].appendChild(_node);
	}
	var config = {
		title : '预设配置管理',
		border : false,
		autoScroll : true,
		rootVisible : false,
		lines : false,
		root : rootNode
	};
	Ext.apply(config, cfg)
	NavPresetConfigPanel.superclass.constructor.call(this, config);
	this.modulePanels = {};
	this.on('click', function(n) {
				if (!n)
					return false;
				var module = n.attributes.module;
				var panelClass = n.attributes.panelClass;
				var p = this.showTab(panelClass, module + 'Tab', n.text, n.attributes.iconCls, true);
			});
}

Ext.extend(NavPresetConfigPanel, Ext.tree.TreePanel, {});
MgrAppMainPanel.registerNav(NavPresetConfigPanel,2)