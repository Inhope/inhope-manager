var PresetConfigCatePanel = Ext.extend(Ext.Panel, {
			initComponent : function() {
				var listStore = new Ext.data.JsonStore({
							url : 'preset-config!listPresetCategory.action',
							fields : ['id', 'name', 'cateName'],
							root : 'data',
							autoLoad : true
						})
				var listP = this.listP = new Ext.grid.GridPanel({
							region : 'west',
							border : false,
							width : 200,
							split : true,
							collapseMode : 'mini',
							store : listStore,
							tbar : [{
										text : '新增',
										iconCls : 'icon-add',
										handler : function() {
											this.editP.reset()
										},
										scope : this
									}, {
										text : '编辑',
										iconCls : 'icon-edit',
										handler : function() {
											var r = this.listP.getSelectionModel().getSelected()
											if (r)
												this.editP.loadData(r.id)
										},
										scope : this
									}, {
										text : '删除',
										iconCls : 'icon-delete',
										handler : function() {
											var rs = this.listP.getSelectionModel().getSelections()
											if (rs && rs.length) {
												var id = []
												Ext.each(rs, function(r) {
															id.push(r.id)
														})
												Ext.Ajax.request({
															url : 'preset-config!deletePresetCategory.action',
															params : {
																id : id.join(',')
															},
															success : function(response) {
																Dashboard.setAlert('删除成功');
																this.listP.getStore().reload()
															},
															scope : this
														})
											}
										},
										scope : this
									}, {
										text : '刷新',
										iconCls : 'icon-refresh',
										handler : function() {
											this.listP.getStore().reload()
										},
										scope : this
									}],
							colModel : new Ext.grid.ColumnModel({
										defaults : {
											sortable : true
										},
										columns : [new Ext.grid.RowNumberer(), {
													header : '预设配置组名',
													dataIndex : 'name'
												}, {
													header : '配置组名',
													dataIndex : 'cateName'
												}]
									}),
							sm : new Ext.grid.RowSelectionModel()
						})
				var editP = this.editP = new PresetConfigCateEditPanel({
							region : 'center'
						})
				listP.getSelectionModel().on('rowselect', function(sm, row, r) {
							this.editP.loadData(r.id)
						}, this)
				this.items = [listP, editP]
				PresetConfigCatePanel.superclass.initComponent.call(this)
			},
			layout : 'border'
		})
var PresetConfigCateEditPanel = Ext.extend(Ext.Panel, {
			layout : 'border',
			border : false,
			initComponent : function() {
				this.tbar = [{
							text : '保存',
							iconCls : 'icon-save',
							handler : this.saveData,
							scope : this
						}, {
							text : '刷新',
							iconCls : 'icon-refresh',
							handler : function() {
								this.loadData(this.loadDataId)
							},
							scope : this
						}, '-', {
							ref : 'createDBBtn',
							disabled : true,
							text : '创建数据库',
							iconCls : 'icon-ontology-root-add',
							handler : function() {
								var d = {}
								this.editGridP.getStore().each(function(r) {
											d[r.data.propkey] = r.data.propvalue
										})
								var vs = this.editFormP.getForm().getValues()
								Ext.Ajax.request({
											url : 'mgr-app!createDB.action',
											params : {
												username : d['jdbc.username'],
												password : d['jdbc.password'],
												driver : d['jdbc.driver'],
												url : d['jdbc.url']
											},
											success : function(form, action) {
												var timer = new ProgressTimer({
															initData : Ext.decode(form.responseText),
															progressId : 'mgrapp.createdb',
															boxConfig : {
																title : '创建数据库'
															},
															finish : function(progress) {
															}
														});
												timer.start();
											},
											scope : this
										});
							},
							scope : this
						}]
				var editFormP = this.editFormP = new Ext.form.FormPanel({
							region : 'north',
							autoHeight : true,
							border : false,
							bodyStyle : 'padding:4px 0 0 5px',
							items : [{
										xtype : 'textfield',
										name : 'name',
										fieldLabel : '预设配置组名',
										allowBlank : false
									}, new Ext.form.ComboBox({
												allowBlank : false,
												editable : false,
												triggerAction : 'all',
												mode : 'remote',
												hiddenName : 'cateId',
												fieldLabel : '配置组名',
												store : new Ext.data.JsonStore({
															url : 'preset-config!listSpecCategory.action',
															fields : ['propkey', 'name'],
															idProperty : 'propkey',
															root : 'data',
															autoLoad : true
														}),
												listeners : {
													select : this.fillEditGridByCategory,
													scope : this
												},
												valueField : 'propkey',
												displayField : 'name'
											}), {
										xtype : 'textfield',
										name : 'id',
										hidden : true
									}]
						})
				var editGridStore = new Ext.data.JsonStore({
							fields : 'id propkey propkeyname propvalue defaultValue'.split(' '),
							root : 'configs'
						})
				editGridStore.on('update', function(st, r, operation) {
							r.set('defaultValue', null)
						})
				var editGridP = this.editGridP = new Ext.grid.EditorGridPanel({
							region : 'center',
							store : editGridStore,
							clicksToEdit : 1,
							autoExpandColumn : 'propvalue',
							colModel : new Ext.grid.ColumnModel({
										columns : [new Ext.grid.RowNumberer(), {
													header : '属性名',
													dataIndex : 'propkeyname',
													width : 200
												}, {
													id : 'propvalue',
													header : '属性值',
													dataIndex : 'propvalue',
													renderer : function(v, meta, r) {
														if (r.get('defaultValue')) {
															meta.attr = 'style="background-color:#eeeeee"'
														}
														return v
													},
													editor : {
														xtype : 'textfield'
													}
												}]
									})
						})
				this.items = [editFormP, editGridP]
				PresetConfigCateEditPanel.superclass.initComponent.call(this)
			},
			loadData : function(id) {
				this.loadDataId = id;
				Ext.Ajax.request({
							url : 'preset-config!loadPresetCategory.action',
							params : {
								id : id || ''
							},
							success : function(response) {
								var result = Ext.decode(response.responseText);
								if (result) {
									this.editFormP.getForm().loadRecord(new Ext.data.Record(result.data));
									this.editGridP.getStore().loadData(result.data);
									this.getTopToolbar().createDBBtn[result.data.cateId == 'jdbc' ? 'enable' : 'disable']();
								} else
									Ext.Msg.alert('错误', '加载失败')
							},
							scope : this
						})
			},
			saveData : function() {
				var vs = this.editFormP.getForm().getValues();
				if (!vs.id)
					delete vs.id
				vs.configs = []
				this.editGridP.getStore().each(function(r) {
							if (!r.data.defaultValue)
								vs.configs.push(r.data)
						})
				Ext.Ajax.request({
							url : 'preset-config!savePresetCategory.action',
							params : {
								data : Ext.encode(vs)
							},
							success : function(response) {
								var result = Ext.decode(response.responseText);
								if (result) {
									this.editFormP.getForm().loadRecord(new Ext.data.Record(result.data));
									this.editGridP.getStore().loadData(result.data);
									Dashboard.setAlert('保存成功');
								} else
									Ext.Msg.alert('错误', '加载失败')
							},
							scope : this
						})
			},
			fillEditGridByCategory : function(combo, r, row) {
				Ext.Ajax.request({
							url : 'preset-config!loadDefaultEntrysByCateId.action',
							params : {
								cateId : r.get('propkey')
							},
							success : function(response) {
								var result = Ext.decode(response.responseText);
								if (result) {
									this.editGridP.getStore().loadData({
												configs : result.data
											});
								} else
									Ext.Msg.alert('错误', '加载失败')
							},
							scope : this
						})
			},
			reset : function() {
				this.editFormP.getForm().reset();
				this.editGridP.store.removeAll()
			}
		})