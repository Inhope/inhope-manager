var PresetConfigEntryPanel = Ext.extend(Ext.grid.EditorGridPanel, {
			initComponent : function() {
				var listStore = new Ext.data.JsonStore({
							url : 'preset-config!entry.action',
							fields : ['id', 'propkeyname', {
										name : 'name',
										allowBlank : false
									}, {
										name : 'propkey',
										allowBlank : false
									}, {
										name : 'propvalue',
										allowBlank : false
									}],
							root : 'data',
							autoLoad : true,
							writer : new Ext.data.JsonWriter({
										writeAllFields : true
									})
						})
				listStore.on('update', function(st, r, op) {
							var rr = specEntryCombo.findRecord('propkey', r.get('propkey'))
							if (rr) {
								r.data.propkeyname = rr.get('name')
							}
						})
				var specEntryCombo = new Ext.form.ComboBox({
							editable : false,
							triggerAction : 'all',
							mode : 'remote',
							store : new Ext.data.JsonStore({
										url : 'preset-config!listSpecEntry.action',
										fields : ['propkey', 'name'],
										idProperty : 'propkey',
										root : 'data'
									}),
							valueField : 'propkey',
							displayField : 'name'
						});
				Ext.apply(this, {
							loadMask : true,
							clicksToEdit : 1,
							store : listStore,
							tbar : [{
										text : '新增',
										iconCls : 'icon-add',
										handler : function() {
											this.addEntry()
										},
										scope : this
									}, {
										text : '刷新',
										iconCls : 'icon-refresh',
										handler : function() {
											listStore.reload()
										},
										scope : this
									}, {
										text : '删除',
										iconCls : 'icon-delete',
										handler : function() {
											var rs = this.getSelectionModel().getSelections();
											if (rs && rs.length) {
												listStore.remove(rs)
											}
										},
										scope : this
									}],
							autoExpandColumn : 'propvalue',
							colModel : new Ext.grid.ColumnModel({
										defaults : {
											sortable : true
										},

										columns : [new Ext.grid.RowNumberer(), {
													header : '预设名',
													dataIndex : 'name',
													width : 150,
													editor : {
														xtype : 'textfield'
													}
												}, {
													header : '属性名',
													dataIndex : 'propkey',
													width : 200,
													editor : specEntryCombo,
													renderer : function(v, m, r) {
														if (v && !r.get('propkeyname')) {
															var rr = specEntryCombo.findRecord('propkey', v);
															r.data.propkeyname = rr.get('name')
														}
														return r.get('propkeyname')
													}
												}, {
													id : 'propvalue',
													header : '属性值',
													dataIndex : 'propvalue',
													editor : {
														xtype : 'textfield'
													}
												}]
									}),
							sm : new Ext.grid.RowSelectionModel()
						})
				PresetConfigEntryPanel.superclass.initComponent.call(this)
				this.on('render', function() {
							specEntryCombo.store.load()
						})
			},
			addEntry : function() {
				var d = {}
				this.getStore().recordType.prototype.fields.each(function(f) {
							d[f.name] = null
						})
				var r = new (this.getStore().recordType)(d);
				this.stopEditing();
				this.getStore().insert(0, r);
				this.startEditing(0, 1);
			},
			saveEntry : function() {

			}
		})
