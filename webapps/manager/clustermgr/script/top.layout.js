Dashboard = {
	u : function() {
		return this._usr ? this._usr : this._usr = new LoginUser('c')
	},
	setAlert : function(msg, status, delay) {
		if (!status)
			status = 'ok'
		if (!delay)
			delay = 3
		if (!this.msgCt) {
			this.msgCt = Ext.DomHelper.insertFirst(document.body, {
						id : 'msg-div'
					}, true);
			this.msgCt.setStyle('position', 'absolute');
			this.msgCt.setStyle('z-index', 9999);
			this.msgCt.setWidth(300);
		}
		this.msgCt.alignTo(document, 't-t');
		var html = ['<div class="app-msg">', '<div class="x-box-tl"><div class="x-box-tr"><div class="x-box-tc"></div></div></div>',
				'<div class="x-box-ml"><div class="x-box-mr"><div class="x-box-mc"><h3 class="x-icon-text icon-status-' + status + '">', msg, '</h3>', '</div></div></div>',
				'<div class="x-box-bl"><div class="x-box-br"><div class="x-box-bc"></div></div></div>', '</div>'].join('');
		Ext.DomHelper.append(this.msgCt, {
					'html' : html
				}, true).slideIn('t').pause(delay).ghost("t", {
					remove : true
				});
	},
	navRegistry : [],
	registerNav : function(navClass, sort) {
		this.navRegistry.push({
					'createNav' : function(id) {
						return new navClass({
									'id' : id
								})
					},
					'sort' : sort
				})
	}
};
Ext.onReady(function() {
			var moduleConfig = Dashboard.u().filterAllowed([{
						id : 'appmgr',
						title : '应用管理',
						authName : 'c_app',
						panelClass : MgrAppMainPanel
					}/*, {
						id : 'presetcfg',
						title : '预设配置管理',
						authName : 'c_preset',
						panelClass : PresetConfigMainPanel
					}*/, {
						id : 'pubappmgr',
						title : '公共库管理',
						authName : 'c_common',
						panelClass : {
							createor : function() {
								var pubApps = ux.util.httpRequest('mgr-app!listPubApp.action');
								if (pubApps) {
									pubApps = Ext.decode(pubApps);
									if (pubApps.data && pubApps.data.length) {
										var appUri = pubApps.data[0].uri
										return new IFramePanel({
													frameURL : '../app/' + appUri + '/authmgr/auth!login.action?username=admin_cluster&password='+ CryptoJS.SHA1('admin_cluster').toString()+'&success_url=../app/' + appUri
															+ '/kbmgr/',
													style : 'border-width:0px;border-right-width:1px',
													border : true
												});
									}
								}
							}
						}
					}, {
						id : 'appmonitor',
						title : '应用监控',
						authName : 'c_monitor',
						panelClass : MgrAppMonitorPanel
					}, {
						id : 'authmgr',
						title : '授权管理',
						authName : 'auth',
						panelClass : {
							createor : function() {
								return new IFramePanel({
											frameURL : '../authmgr/',
											style : 'border-width:0px;border-right-width:1px',
											border : true
										});
							}
						}
					}])
			getTopWindow().modules = moduleConfig;
			var menuTab = new Ext.ux.TabPanel({
						tabPosition : 'left',
						autoScroll : true,
						// deferredRender:false,
						activeTab : 0,
						enableTabScroll : true,
						border : false,
						region : 'center'
					});
			getTopWindow().vtab = menuTab;
			var r = moduleConfig
			var viewport = new Ext.Viewport({
						layout : 'border',
						items : [{
									cls : 'docs-header',
									height : 36,
									region : 'north',
									xtype : 'box',
									el : 'header',
									border : false,
									margins : '0 0 5 0'
								}, menuTab]
					});
			var defaultCfg = {}
			Ext.each(moduleConfig, function(m, i) {
						if (m.panelClass) {
							var p = new Ext.Panel({
										layout : 'fit',
										title : m.title,
										iconCls : 'top-module-' + m.id,
										border : false,
										items : m.panelClass.createor ? m.panelClass.createor(defaultCfg) : new m.panelClass(defaultCfg)
									});
							menuTab.add(p)
						}
					})
			menuTab.setActiveTab(0)
			Ext.get('header').on('click', function() {
						viewport.focus();
					});
		});
