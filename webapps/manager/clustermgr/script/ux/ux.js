Ext.override(Ext.grid.GroupingView, {
			processEvent : function(name, e) {
				Ext.grid.GroupingView.superclass.processEvent.call(this, name, e);
				var hd = e.getTarget('.x-grid-group-hd', this.mainBody);
				if (hd) {
					// group value is at the end of the string
					var field = this.getGroupField(), prefix = this.getPrefix(field), groupValue = hd.id.substring(prefix.length), emptyRe = new RegExp('gp-' + Ext.escapeRe(field)
							+ '--hd');

					// remove trailing '-hd'
					groupValue = groupValue.substr(0, groupValue.length - 3);

					// also need to check for empty groups
					if (groupValue || emptyRe.test(hd.id)) {
						if (this.grid.fireEvent('group' + name, this.grid, field, groupValue, e) === false) {
							return;
						}
					}
					if (name == 'mousedown' && e.button == 0) {
						this.toggleGroup(hd.parentNode);
					}
				}

			}
		})
var DataOrderGroupingStore = Ext.extend(Ext.data.GroupingStore, {
			constructor : function(config) {
				var orderMap = {}
				DataOrderGroupingStore.superclass.constructor.call(this, config);
				this.recordType.prototype.fields.each(function(f) {
							if (f.name == this.groupField) {
								f.sortType = function(v) {
									return orderMap[v]
								}
								return false;
							}
						}, this)
				this.on('load', function() {
							this.each(function(r, i) {
										if (r.get(this.groupField))
											orderMap[r.get(this.groupField)] = i
									}, this)
						})
			}
		})
Ext.namespace('ux')
ux.util = {
	resetEmptyString : function(obj) {
		for (var key in obj) {
			var v = obj[key];
			if (!v) {
				delete obj[key];
			} else if (v.push && v.length) {
				for (var i = 0; i < v.length; i++) {
					this.resetEmptyString(v[i]);
				}
			} else if (key.indexOf('id') != -1 && key.indexOf('-') == 0) {
				delete obj[key]// reset negative id as null
			}
		}
		return obj;
	},
	httpRequest : function(url, postObj) {
		var xhr = ux.util.createXHR();
		if (postObj) {
			var bodystr = ''
			for (var key in postObj) {
				bodystr += key + '=' + encodeURIComponent(postObj[key]) + '&'
			}
			xmlhttp.open("POST", url, false);
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send(bodystr);
		} else {
			xhr.open('GET', url, false);
			xhr.send();
		}
		return xhr.responseText;
	},
	createXHR : function() {
		var _xhr = false;
		try {
			_xhr = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				_xhr = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e2) {
				_xhr = false;
			}
		}
		if (!_xhr && window.XMLHttpRequest)
			_xhr = new XMLHttpRequest();
		return _xhr;
	}
}
var IFramePanel = Ext.extend(Ext.Panel, {
			border : false,
			initComponent : function() {
				this.html = "<iframe id=" + (this.fid = Ext.id())
						+ " height='100%' width='100%' frameborder='no' border='0' marginwidth='0' marginheight='0' scrolling='auto' allowtransparency='yes'></iframe>"
				IFramePanel.superclass.initComponent.call(this)
				this.on('afterrender', function() {
							if (this.frameURL) {
								this.setLocation(this.frameURL)
							}
						})
			},
			getFrame : function() {
				return Ext.getDom(this.fid);
			},
			getFrameWin : function() {
				return Ext.getDom(this.fid).contentWindow;
			},
			setLocation : function(url) {
				Ext.getDom(this.fid).src = url;
				this.frameURL = url
			}
		})