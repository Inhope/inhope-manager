<%@page import="com.incesoft.xmas.commons.LicenceHolder"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:directive.page
	import="org.springframework.web.context.support.WebApplicationContextUtils" />
<jsp:directive.page
	import="com.incesoft.xmas.commons.PropertyPlaceholderRecorder" />
<jsp:directive.page
	import="org.springframework.beans.factory.config.PropertyPlaceholderConfigurer" />
<%@taglib prefix="res" uri="/WEB-INF/tld/import-res.tld"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="usr" uri="/WEB-INF/tld/login-user.tld"%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<%@include file="/commons/extjsvable.jsp"%>
		<script type="text/javascript" src="<c:url value='/authmgr/script/ux/ux.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/authmgr/script/Map.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/authmgr/script/SysInfoPanel.js'/>"></script>
		<script type="text/javascript">
		Ext.onReady(function() {
			var p = new SysInfoPanel()
			var viewport = new Ext.Viewport({
						layout : 'fit',
						items : [p]
					});
		})
		</script>
	</head>
	<body>
	</body>
</html>
