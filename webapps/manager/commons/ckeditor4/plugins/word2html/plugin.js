CKEDITOR.plugins.add('word2html', {
	init : function(editor) {
		// Add the link and unlink buttons.
		editor.addCommand('word2html', new CKEDITOR.dialogCommand('word2html')); // 定義dialog，也就是下面的code
		editor.ui.addButton('Word2HTML', // 定義button的名稱及圖片,以及按下後彈出的dialog
		{ // 這裡將button名字取叫'Code'，因此剛剛上方的toolbar也是加入名為Code的按鈕
			label : '转换word文档',
			icon : CKEDITOR.plugins.getPath("word2html") + 'word_logo.png',
			command : 'word2html'
		});
		// CKEDITOR.dialog.add( 'link’, this.path + 'dialogs/link.js’ );
		// dialog也可用抽離出去變一個js，不過這裡我直接寫在下面
		CKEDITOR.dialog.add('word2html', function(editor) {
			// 以下開始定義dialog的屬性及事件
			return { // 定義簡單的title及寬高
				title : '转换word文档',
				minWidth : 250,
				minHeight : 60,
				contents : [ {
					id : 'word',
					elements : // elements是定義dialog內部的元件，除了下面用到的select跟textarea之外
					[ // 還有像radio或是file之類的可以選擇
					{
						action : 'file-upload!word2html.action?p4id=' + (typeof p4id != 'undefined' ? p4id : ''),
						type : 'file',
						label : '选择word文档',
						id : 'file'
					} ]
				} ],
				onOk : function() {
					// 當按下ok鈕時,將上方定義的元件值取出來，利用insertHtml
					// 將組好的字串插入ckeditor的內容中
					var filePath = this.getValueOf('word', 'file');
					if (!filePath) {
						alert('尚未选择word文档');
						return false;
					}
					if (filePath.indexOf('.docx') == filePath.length - 5 || filePath.indexOf('.doc') == filePath.length - 4) {
						var ifr = this.parts.contents.$.getElementsByTagName('iframe')[0];
						ifr.contentWindow.document.getElementsByTagName('form')[0].submit();
					} else {
						alert('请选择合法的word文档文件');
						return false;
					}
				}
			};
		});
	}
})