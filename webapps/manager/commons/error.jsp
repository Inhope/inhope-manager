<%@ page contentType="text/html;charset=UTF-8"%><%@ page import="org.apache.commons.logging.LogFactory"%><jsp:directive.page import="org.apache.commons.logging.Log" /><%!static Log log = LogFactory.getLog("com.incesoft");%><%
Throwable t = (Throwable) request.getAttribute("exception");
log.error("", t);
response.setStatus(500);
response.getWriter().write((t.getMessage()+"").replace("<", "&lt;").replace(">", "&gt;"));
%>