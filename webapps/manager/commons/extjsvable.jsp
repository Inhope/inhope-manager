<%@page import="com.incesoft.xmas.commons.LicenceValidator"%>
<%@page import="com.incesoft.xmas.commons.LabsUtil"%>
<%@page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${empty requestScope.extjsvable_excludeCSS}">
<link rel="stylesheet" type="text/css" href='<c:url value="/ext/resources/css/ext-all.css"/>'/>
<link rel="stylesheet" type="text/css" href='<c:url value="/ext/resources/css/patch.css"/>'/>
<link rel="stylesheet" type="text/css" href='<c:url value="/ext/ux/css/ux-all.css"/>'/>
</c:if>
<c:if test="${param.debug==null}">
<script type="text/javascript" src='<c:url value="/ext/adapter/ext/ext-base.js"/>'></script>
<script type="text/javascript" src='<c:url value="/ext/ext-all.js"/>'></script>
<script type="text/javascript" src='<c:url value="/ext/ux/ux-all.js"/>'></script>
</c:if>
<c:if test="${param.debug!=null}">
<script type="text/javascript" src='<c:url value="/ext/adapter/ext/ext-base-debug.js"/>'></script>
<script type="text/javascript" src='<c:url value="/ext/ext-all-debug.js"/>'></script>
<script type="text/javascript" src='<c:url value="/ext/ux/ux-all-debug.js"/>'></script>
</c:if>
<script type="text/javascript" src='<c:url value="/commons/scripts/commons.js"/>'></script>
<script type="text/javascript" src='<c:url value="/commons/scripts/sha1.js"/>'></script>
<script type="text/javascript">
Ext.BLANK_IMAGE_URL = '<c:url value="/ext/resources/images/default/s.gif"/>';
var sys_labs = <%=LabsUtil.isLabs()%>
var sys_bdcolor = '#8db2e3;';
var sys_bgcolor = '#d3e1f1;';
</script>
<% if ("gray".equals(LabsUtil.getExtTheme())) { %>
<link rel="stylesheet" type="text/css" href='<c:url value="/ext/resources/css/xtheme-gray.css"/>'/>
<script type="text/javascript">
sys_bdcolor = '#d0d0d0;';
sys_bgcolor = '#f1f1f1;';
</script>
<% } %>
<script type="text/javascript" src='<c:url value="/ext/locale/ext-lang-zh_CN.js"/>'></script>