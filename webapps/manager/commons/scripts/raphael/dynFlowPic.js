var paper ;

/**存放创建的各种line的容器*/
var linesContain = [];

/**存放创建的各种shape的容器*/
var shapesContain = [];

/**存放创建的procedureContain的容器*/
var procedureContain = [];

/**存放key:component_id,value:component_obj的map (供updateComponent用)*/
var componentObjMap = {};

function addComponentObjMap(component_id,component_obj){
	componentObjMap[component_id] = component_obj;
}

function removeComponentObjMap(component_id){
	delete componentObjMap[component_id];
}

function keyupHandler(event){
	event = event || window.event;
	if(event.keyCode==46){
		//delete键位
		eve("custom.someoneDelete");
	}
}

function mouseupHandler(){
	eve("custom.someoneAdjusting");
}

function reset(){
	if(paper){
//		console.log("reset");
		paper.clear();
//		paper.remove();
		linesContain = [];
		shapesContain = [];
		procedureContain = [];
		eve.off("custom.someoneDelete");
		eve.off("custom.connectionHandler_branch");
		eve.off("custom.connectionHandler_rect");
		eve.off("custom.someoneAdjusting");
		eve.off("custom.connectionHandlerByBranch");
		eve.off("*.custom.rectResize");
		eve.off("custom.connectProcedureContain");
		eve.off("custom.connectProcedureContainTrigger");
		document.onkeyup=function (event){
			keyupHandler(event);
		};
		document.onmouseup=function (event){
			mouseupHandler();
		};
		eve.on("custom.someoneAdjusting",toggleRectContentSpanAndBranchContentSpan);
	}
}

function removeComponent(){
	eve("custom.someoneDelete");
}

function initPaper(){
	paper = Raphael("holder", 1200, 1000);
	document.onkeyup=function (event){
		keyupHandler(event);
	};
	document.onmouseup=function (event){
		mouseupHandler();
	};
}

/**保存*/
function saveElement(){
	//重新再完善补充一次procedure和各shape(不需要检查line)的关系
	var isSession = false;
	if(procedureContain && procedureContain.length > 0){
		isSession = true;
	}
	
	if(isSession){
		for(var si = 0 ;si<shapesContain.length;si++){
			if(shapesContain[si]){
				shapesContain[si].eveConnectProcedureContain();
			}
		}
	}
	
	var resultJson = {};
	//整合成json格式（注意处理关系）
	for(var index=0;index<linesContain.length;index++){
			var lineObj = linesContain[index];
			var parentObj = lineObj.relationArray[0];
			var childObj = lineObj.relationArray[1];
			var hasP = false;
			var hasC = false;
			if(parentObj!=null && isInShape(parentObj)){
				hasP=true;
				var parentJson ;
				if(!resultJson[parentObj.getId()]){
					parentJson = {};
					parentJson.id = parentObj.getId();
					parentJson.code = parentObj.getCode();
					parentJson.__title = parentObj.getTitle();
					parentJson.type = parentObj.type;
					parentJson.x = parentObj.getX();
					parentJson.y = parentObj.getY();
					parentJson.width = parentObj.getWidth();
					parentJson.height = parentObj.getHeight();
					if(parentObj.getDAStr()){
						parentJson.dAStr = parentObj.getDAStr();
					}
					if(isSession){
						var procedureId = getProcedureContainIdByShapeObjId(parentObj.getId());
						if(procedureId){
							parentJson.procedureId = procedureId;
						}
					}
					resultJson[parentObj.getId()]=parentJson;
				}
				if(childObj!=null && isInShape(childObj)){
					parentJson = resultJson[parentObj.getId()];
					if(!parentJson.child){
						parentJson.child = childObj.getId();
					}else{
						// 第2版完成多个child(暂时不要)
					}
				}
				if(parentJson && lineObj.type.indexOf("_branch_")>-1){
					parentJson.has_branch = lineObj.getId();
				}
//				console.log(parentJson)
			}
			if(childObj!=null && isInShape(childObj)){
				hasC=true;
				var childJson ;
				if(!resultJson[childObj.getId()]){
					childJson = {};
					childJson.id = childObj.getId();
					childJson.code = childObj.getCode();
					childJson.__title = childObj.getTitle();
					childJson.type = childObj.type;
					childJson.x = childObj.getX();
					childJson.y = childObj.getY();
					childJson.width = childObj.getWidth();
					childJson.height = childObj.getHeight();
					if(childObj.getDAStr()){
						childJson.dAStr = childObj.getDAStr();
					}
					if(isSession){
						var procedureId = getProcedureContainIdByShapeObjId(childObj.getId());
						if(procedureId){
							childJson.procedureId = procedureId;
						}
					}
					resultJson[childObj.getId()]=childJson;
				}
				if(parentObj!=null && isInShape(parentObj)){
					childJson = resultJson[childObj.getId()];
					if(!childJson.parent){
						childJson.parent = parentObj.getId();
					}else{
						// 第2版完成多个parent(暂时不要)
					}
				}	
			}
		
		if(lineObj.type.indexOf("_branch_")>-1){
//			console.log("_branch_")
			//判断框
			//先把branch的rect放入result
			var branchShapeObjs = lineObj.containsArray;
			var branchShapeId = "";
			for(var i=0;i<branchShapeObjs.length;i++){
				var bsObjArray = branchShapeObjs[i];
				var bsObjArrayRepaired = [];
//				//修整bsObjArray
//				//去除null的
//				for(var k=0;k<bsObjArray.length;k++){
//					if(bsObjArray[k]!=null){
//						bsObjArrayRepaired.push(bsObjArray[k]);
//					}
//				}
				if(bsObjArray!=null){
					bsObjArrayRepaired.push(bsObjArray);
				}
//				console.log("before",bsObjArrayRepaired);
				//排序
				for(var l=0;l<bsObjArrayRepaired.length;l++){
					var min = 100000;
					var minIndex = -1;
					for(var m=l;m<bsObjArrayRepaired.length;m++){
						var currentBsObj = bsObjArrayRepaired[m];
						if(min>currentBsObj.getY()){
							min=currentBsObj.getY();
							minIndex = m;
						}
					}
					if(minIndex>-1){ 
						var tmpObj = bsObjArrayRepaired[l];
//						console.log(minIndex)
						bsObjArrayRepaired[l] = bsObjArrayRepaired[minIndex];
						bsObjArrayRepaired[minIndex] = tmpObj;
					}
				}
				
				var bsObj = null;
				if(bsObjArrayRepaired==null || bsObjArrayRepaired.length>0){
					for(var j=0;j<bsObjArrayRepaired.length;j++){
						if(bsObjArrayRepaired[j]!=null){
							bsObj = bsObjArrayRepaired[j];
							//多个child
							bsObj.indexInSelfBranch = j;
	//						break;
						}
						if(bsObj){
							var branchJson = resultJson[bsObj.getId()]||{};
							branchJson.id = bsObj.getId();
							branchJson.code = bsObj.getCode();
							branchJson.__title = bsObj.getTitle();
							branchJson.type = bsObj.type;
							branchJson.x = bsObj.getX();
							branchJson.y = bsObj.getY();
							branchJson.width = bsObj.getWidth();
							branchJson.height = bsObj.getHeight();
							branchJson.branch = lineObj.getId()+"["+i+"]"+"["+bsObj.indexInSelfBranch+"]";
							if(bsObj.getDAStr()){
								branchJson.dAStr = bsObj.getDAStr();
							}
							if(isSession){
								var procedureId = getProcedureContainIdByShapeObjId(bsObj.getId());
								if(procedureId){
									branchJson.procedureId = procedureId;
								}
							}
							
							resultJson[bsObj.getId()]=branchJson;
							
							if(i==0){
								if(j==0){
									branchShapeId+=bsObj.getId();
								}else{
									branchShapeId+="&splInner"+bsObj.getId();
								}
							}else{
								if(j==0){
									branchShapeId+="&split"+bsObj.getId();
								}else{
									branchShapeId+="&splInner"+bsObj.getId();
								}
							}
						}
						
					}
				}else{
					if(i==0){
						branchShapeId+="empty";
					}else{
						branchShapeId+="&splitempty";
					}
				}
				
			}
			/**用于存放每个branch分支的判断条件text*/
			var branchTexts = lineObj.getBranchTextObjs();
			// line放入result
			var branchLineJson = {};
			branchLineJson.id = lineObj.getId();
			branchLineJson.type = lineObj.type;
			if(hasP){
				branchLineJson.relation_p = parentObj.getId();
			}
			if(hasC){
				branchLineJson.relation_c = childObj.getId();
			}
			branchLineJson.branch_num = lineObj.getBranchNum();
			branchLineJson.top_x = lineObj.getTopX();
			branchLineJson.top_y = lineObj.getTopY();
//			branchLineJson.bottom_x = lineObj.getBottomX();
//			branchLineJson.bottom_y = lineObj.getBottomY();
			branchLineJson.left_x = lineObj.getLeftX();
			branchLineJson.left_y = lineObj.getLeftY();
			branchLineJson.right_x = lineObj.getRightX();
			branchLineJson.right_y = lineObj.getRightY();
			branchLineJson.outTop_x = lineObj.getOutTopX();
			branchLineJson.outTop_y = lineObj.getOutTopY();
//			branchLineJson.outBottom_x = lineObj.getOutBottomX();
//			branchLineJson.outBottom_y = lineObj.getOutBottomY();
			branchLineJson.dAStr = lineObj.getDAStr();
			var tcs = "";
			for(var i = 0;i<branchTexts.length;i++){
				var textObj = branchTexts[i];
				if(i==0){
					if(!textObj.val_value){
						tcs +="empty";
					}else{
						tcs +=textObj.val_value;
					}
				}else{
					if(!textObj.val_value){
						tcs +="&splitempty";
					}else{
						tcs +="&split"+textObj.val_value;
					}
				}
			}
			branchLineJson.text_contents = tcs;
			branchLineJson.text_contents_id = branchShapeId;
			resultJson[lineObj.getId()]=branchLineJson;
		}else{
//			console.log("sim")
			//普通的线
			// line放入result
			var simLineJson = {};
			simLineJson.id = lineObj.getId();
			simLineJson.type = lineObj.type;
			if(hasP){
				simLineJson.relation_p = parentObj.getId();
			}
			if(hasC){
				simLineJson.relation_c = childObj.getId();
			}
			if(lineObj.type.indexOf("_sla_")>-1){
				simLineJson.start_x = lineObj.getStartX();   
				simLineJson.start_y = lineObj.getStartY();
				simLineJson.end_x = lineObj.getEndX();
				simLineJson.end_y = lineObj.getEndY();
				
			}else if(lineObj.type.indexOf("_clr_")>-1){
				simLineJson.start_x = lineObj.getStartX();   
				simLineJson.start_y = lineObj.getStartY();
				simLineJson.end_x = lineObj.getEndX();
				simLineJson.end_y = lineObj.getEndY();
				simLineJson.mid_x = lineObj.getMidX();
				simLineJson.mid_y = lineObj.getMidY();
			}else if(lineObj.type.indexOf("_cud_")>-1){
				simLineJson.start_x = lineObj.getStartX();
				simLineJson.start_y = lineObj.getStartY();
				simLineJson.end_x = lineObj.getEndX();
				simLineJson.end_y = lineObj.getEndY();
				simLineJson.mid_x = lineObj.getMidX();
				simLineJson.mid_y = lineObj.getMidY();
			}
			
			resultJson[lineObj.getId()]=simLineJson;
		}
		
	}
	
	if(isSession){
		for(var pi = 0 ;pi<procedureContain.length;pi++){
			var pCObj = procedureContain[pi];
			var pCJson = {};
			pCJson.id = pCObj.getId();
			pCJson.type = pCObj.type;
			pCJson.x = pCObj.getX();
			pCJson.y = pCObj.getY();
			pCJson.width = pCObj.getWidth();
			pCJson.height = pCObj.getHeight();
			if(pCObj.getDAStr()){
				pCJson.dAStr = pCObj.getDAStr();
			}
			pCJson.code = pCObj.getTitle();
			resultJson[pCObj.getId()]=pCJson;
		}
	}
	
	var rt = JSON.toJson(resultJson);
	return rt;
}

/**通过sharpObjId来在procedureContain中查找是否有包含该shapeObj的procedure容器，并返回procedureContainId*/
function getProcedureContainIdByShapeObjId(shapeObjId){
	for(var i = 0 ;i<procedureContain.length;i++){
		var pdObj = procedureContain[i];
		var innerObjMap  = pdObj.getInnerObjs();
		if(innerObjMap[shapeObjId]){
			return pdObj.getId();
		}else{
			return null;
		}
	}
}

/**字符转成图片*/
function loadPic(str){
//	if(!str){
//		str = rt;
//		str = "{'diy_rect_1355823595645':{'id':'diy_rect_1355823595645','code':'','type':'d_shape','x':260,'y':75,'width':150,'height':70,'dAStr':'{\"key1\":\"value1\"}&comma{\"key2\":\"value2\"}&comma{\"键1\":\"值1\"}','procedureId':'diy_conProcedure_1355823596467','child':'diy_rect_1355823609519','has_branch':'diy_branch_1355823598998'},'diy_rect_1355823609519':{'id':'diy_rect_1355823609519','code':'','type':'d_shape','x':260,'y':465,'width':150,'height':70,'dAStr':'{\"key1\":\"value1\"}&comma{\"key2\":\"value2\"}&comma{\"键1\":\"值1\"}','procedureId':'diy_conProcedure_1355823596467','parent':'diy_rect_1355823595645'},'diy_branch_1355823598998':{'id':'diy_branch_1355823598998','type':'d_branch_line','relation_p':'diy_rect_1355823595645','relation_c':'diy_rect_1355823609519','branch_num':3,'top_x':335,'top_y':195,'bottom_x':335,'bottom_y':415,'left_x':235,'left_y':305,'right_x':435,'right_y':305,'outTop_x':335,'outTop_y':145,'outBottom_x':335,'outBottom_y':465,'dAStr':'{\"0\":\"value1\"}&comma{\"1\":\"value2\"}&comma{\"2\":\"值1\"}','text_contents':'empty&splitempty&splitempty','text_contents_id':'empty&splitempty&splitempty'},'diy_conProcedure_1355823596467':{'id':'diy_conProcedure_1355823596467','type':'d_contain_procedure','x':100,'y':50,'width':500,'height':500}}";
//	}
	shapesContain = [];
	linesContain = [];
	procedureContain = [];
	var jsonObj = JSON.fromJson(str);
//	console.log("jsonObj",jsonObj);
	//用来存放new好的对象  key:id value:obj对象
	var objsMap = {};
	//step1 简单画  new 和 add line or shape 再加入objsMap中
	for(var id in jsonObj){
		var obj = jsonObj[id];
		if(obj["type"].indexOf("_shape")>-1){
//			console.log(obj["type"]);
//			diyRect(paper,initX,initY,initWidth,initHeight,id,code)
			var code = "code:"+obj["code"];
			var rect = new diyRect(paper,obj["x"]-0,obj["y"]-0,obj["width"]-0,obj["height"]-0,obj["id"],code,obj["dAStr"],obj["__title"]);
			addShape(rect);
			addComponentObjMap(rect.getId(),rect);
			objsMap[obj["id"]] = rect;
		}else if(obj["type"].indexOf("_contain_procedure")>-1){
			// diyProcedureContainRect(paper,initX,initY,initWidth,initHeight,id)
			var procedureObj = new diyProcedureContainRect(paper,obj["x"]-0,obj["y"]-0,obj["width"]-0,obj["height"]-0,obj["id"],obj["code"],obj["dAStr"]);
			addProcedureContain(procedureObj);
			addComponentObjMap(procedureObj.getId(),procedureObj);
			objsMap[obj["id"]] = procedureObj;
		}else if(obj["type"].indexOf("_branch_")>-1){
//			diyBranch(paper,initX,initY,lineXLength,lineYLength,branchNum,textOffsetY,id,top_x,top_y,bottom_x,bottom_y,left_x,left_y,right_x,right_y,text_contents,dAStr, outTop_x,outTop_y,outBottom_x,outBottom_y)
			var branchObj = new diyBranch(paper,null,null,null,null,obj["branch_num"]-0,null,obj["id"],obj["top_x"]-0,obj["top_y"]-0,obj["left_x"]-0,obj["left_y"]-0,obj["right_x"]-0,obj["right_y"]-0,obj["text_contents"],obj["dAStr"],obj["outTop_x"]-0,obj["outTop_y"]-0);
			addLine(branchObj);
			addComponentObjMap(branchObj.getId(),branchObj);
			objsMap[obj["id"]] = branchObj;
		}else if(obj["type"].indexOf("_sla_")>-1){
//		diySimpleLineWithArrow(paper,initX,initY,lineLength,id,start_x,start_y,end_x,end_y)
			var slaObj = new diySimpleLineWithArrow(paper,null,null,null,obj["id"],obj["start_x"]-0,obj["start_y"]-0,obj["end_x"]-0,obj["end_y"]-0);
			addLine(slaObj);
			objsMap[obj["id"]] = slaObj;
		}else if(obj["type"].indexOf("_clr_")>-1){
//			diyComplexLineLR(paper,initX,initY,lineLength,id,start_x,start_y,mid_x,mid_y,end_x,end_y)
			var clrObj = new diyComplexLineLR(paper,null,null,null,obj["id"],obj["start_x"]-0,obj["start_y"]-0,obj["mid_x"]-0,obj["mid_y"]-0,obj["end_x"]-0,obj["end_y"]-0);
			addLine(clrObj);
			objsMap[obj["id"]] = clrObj;
		}else if(obj["type"].indexOf("_cud_")>-1){
//			diyComplexLineUD(paper,initX,initY,lineLength,id,start_x,start_y,mid_x,mid_y,end_x,end_y)
			var cudObj = new diyComplexLineUD(paper,null,null,null,obj["id"],obj["start_x"]-0,obj["start_y"]-0,obj["mid_x"]-0,obj["mid_y"]-0,obj["end_x"]-0,obj["end_y"]-0);
			addLine(cudObj);
			objsMap[obj["id"]] = cudObj;
		}
	}
	//step2 建立关系 line的2点 和 手动eve事件
	for(var i=0;i<linesContain.length;i++){
		var lineObj = linesContain[i];
		if(!(lineObj.type.indexOf("_branch_")>-1)){
			eve("custom.connectionHandler_rect",'',lineObj,lineObj.getLeft_c(),lineObj.getRight_c());
			eve("custom.connectionHandler_rect",'',lineObj,lineObj.getRight_c(),lineObj.getLeft_c());
		}
	}
	
	for(var i=0;i<shapesContain.length;i++){
		var shapeObj = shapesContain[i];
//		console.log("shapesContain.length123",shapesContain.length)
		eve("custom.connectionHandler_branch",'',shapeObj,shapeObj.getReal_pic(),shapeObj.getLeftTop_x(),shapeObj.getLeftTop_y(),shapeObj.getRightBottom_x(), shapeObj.getRightBottom_y());
	}
	
	/**建立procedureContain与其他元素(包括shape和line)的关系*/
	if(procedureContain && procedureContain.length>0){
//		for(var si = 0 ;si<shapesContain.length;si++){
//			if(shapesContain[si]){
//				shapesContain[si].eveConnectProcedureContain();
//			}
//		}
//		for(var li=0;li<linesContain.length;li++){
//			if(linesContain[li]){
//				linesContain[li].eveConnectProcedureContain();
//			}
//		}
		// 改成基于事件的方式(每个组件自己注册事件)
		eve("custom.connectProcedureContainTrigger");
	}
}

function isInShape(obj){
	for(var i=0;i<shapesContain.length;i++){
		if(shapesContain[i]==obj){
			return true;
		}
	}
	return false;
}

function addLine(obj){
	linesContain.push(obj);
}

function addShape(obj){
	shapesContain.push(obj);
}

function addProcedureContain(obj){
	procedureContain.push(obj);
}

function removeLine(obj){
	for(var i=0;i<linesContain.length;i++){
		if(linesContain[i]==obj){
			linesContain.splice(i,1);
			break;
		}
	}
}

function removeShape(obj){
	for(var i=0;i<shapesContain.length;i++){
		if(shapesContain[i]==obj){
			shapesContain.splice(i,1);
			break;
		}
	}
//	console.log(shapesContain)
}

function removeProcedureContain(obj){
	for(var i=0;i<procedureContain.length;i++){
		if(procedureContain[i]==obj){
			procedureContain.splice(i,1);
			break;
		}
	}
}

/**
 * 添加组件
 * @param pageType:codeRect,simpleLine,complexLRLine,complexUDLine,branch(这个type和组件obj里面的type不一样,这个是给页面设置属性用的)
 * pageType=[要画的类型，线，框等]String,dataArray=[数据，key,value形式]   object数组
 * @return 组件id
 * */
function addComponent(pageType,dataArray){
//	console.log("pageType",pageType);
//	console.log("dataArray",dataArray);
	if(pageType=="codeRect"){
		var results = resolveRect(dataArray);
//		console.log("dataArrayStr",results[1]);
		return drawRect(results[0],results[1],results[2]);
	}else if(pageType=="procedureContainRect"){
		var results = resolveprocedureContainRect(dataArray);
		return drawProcedureContainRect(results[0],results[1]);
	}else if(pageType=="simpleLine"){
		return drawDiySimpleLineWithArrow();
	}else if(pageType=="complexLRLine"){
		return drawDiyComplexLineLR();
	}else if(pageType=="complexUDLine"){
		return drawDiyComplexLineUD();
	}else if(pageType=="branch"){
		var results = resolveBranch(dataArray);
		return drawDiyBranch(results[0],results[1],results[2]);
	}
}

function resolveBranch(dataArray){
	var defaultBn = 2;
	var contentTexts = [];
	var dataArrayStr = null;
	if(dataArray){
		dataArrayStr="";
		for(var di =0 ;di<dataArray.length;di++){
			var keyValue = dataArray[di]["key"];
			var valValue = dataArray[di]["value"];
			if(valValue){
				contentTexts.push(valValue);
			}
			dataArrayStr+=JSON.toJson(dataArray[di]);
			if(di!=dataArray.length-1){
				dataArrayStr+="&comma";
			}
		}
		if(dataArray.length>1){
			defaultBn = dataArray.length;
		}
	}
	if(contentTexts.length ==0){
		contentTexts = null;
	}
	return [defaultBn,contentTexts,dataArrayStr];
}

function resolveRect(dataArray){
	var code = null;
	var dataArrayStr = null;
	if(dataArray){
		code = "";
		dataArrayStr = "";
		var titleValue ;
		for(var di =0 ;di<dataArray.length;di++){
			var keyValue = dataArray[di]["key"];
			var valValue = dataArray[di]["value"];
			if(keyValue=='__title'){
				titleValue = valValue;
			}else if(keyValue=='code'){
				if(keyValue){
					code+=keyValue+":";
				}
				if(valValue){
					code+=valValue;
				}
			}
			dataArrayStr+=JSON.toJson(dataArray[di]);
			if(di!=dataArray.length-1){
//				code+="\n";
				dataArrayStr+="&comma";
			}
		}
	}
	return [code,dataArrayStr,titleValue];
}

function resolveprocedureContainRect(dataArray){
	var title = null;
	var dataArrayStr = null;
	if(dataArray){
		title = "";
		dataArrayStr = "";
		for(var di =0 ;di<dataArray.length;di++){
			var keyValue = dataArray[di]["key"];
			var valValue = dataArray[di]["value"];
			if(title=="" && keyValue == "name" && valValue){
				title = valValue;
			}
			dataArrayStr+=JSON.toJson(dataArray[di]);
			if(di!=dataArray.length-1){
				dataArrayStr+="&comma";
			}
		}
	}
	return [title,dataArrayStr];
}

/**
 * 从页面拿到设置好的数据并更新
 * @argument data:{}对象格式
 * */
function updateComponent(componentId,dataArray){
	if(componentObjMap[componentId]){
		if(componentObjMap[componentId].update){
			componentObjMap[componentId].update(dataArray);
		}
	}
}

/**
 * 主动调用页面的设置属性的方法
 * @param data格式key:value\nkey:value\nkey:value...
 * */
function toEdit(componentId,pageType,dataArrayStr){
	var  dataPost = [];
	if(dataArrayStr){
		var dataArray = dataArrayStr.split("&comma");
		for(var i = 0;i < dataArray.length;i++){
			if(dataArray[i]){
				dataPost.push(JSON.fromJson(dataArray[i]));
			}
		}
	}
//	console.log("dataPost",dataPost);
	if(window.parent.editComponent){
		window.parent.editComponent(componentId,pageType,dataPost);
	}
}

/**draw 各种图形*/
function drawRect(code,dataArrayStr,__title){
	var rectObj = new diyRect(paper,50,50,150,70,null,code,dataArrayStr,__title);
	addShape(rectObj);
	addComponentObjMap(rectObj.getId(),rectObj);
	return rectObj.getId();
}

function drawProcedureContainRect(title,dataArrayStr){
	var pcrObj = new diyProcedureContainRect(paper,100,50,500,500,null,title,dataArrayStr);
	addProcedureContain(pcrObj);
	addComponentObjMap(pcrObj.getId(),pcrObj);
	eve("custom.connectProcedureContainTrigger");
	return pcrObj.getId();
}

function drawDiySimpleLineWithArrow(){
	var dslObj = new diySimpleLineWithArrow(paper,50,150,200);
	addLine(dslObj);
	addComponentObjMap(dslObj.getId(),dslObj);
	return dslObj.getId();
}

function drawDiyComplexLineLR(){
	var dclLRObj = new diyComplexLineLR(paper,50,310,150);
	addLine(dclLRObj);
	addComponentObjMap(dclLRObj.getId(),dclLRObj);
	return dclLRObj.getId();
}

function drawDiyComplexLineUD(){
	var dclUDObj = new diyComplexLineUD(paper,50,310,150);
	addLine(dclUDObj);
	addComponentObjMap(dclUDObj.getId(),dclUDObj);
	return dclUDObj.getId();
}

function drawDiyBranch(bn,contentTexts,dataArrayStr){
//	console.log("dataArrayStr",dataArrayStr)
//	diyBranch(paper,initX,initY,lineXLength,lineYLength,branchNum,textOffsetY,id,top_x,top_y,bottom_x,bottom_y,left_x,left_y,right_x,right_y,text_contents,  outTop_x,outTop_y,outBottom_x,outBottom_y)
	var bObj = new diyBranch(paper,300,80,200,220,bn,null,null,null,null,null,null,null,null,contentTexts,dataArrayStr);
	addLine(bObj);
	addComponentObjMap(bObj.getId(),bObj);
	return bObj.getId();
}

/**
 * 工具
 * */
function getDistanceByTwoPoints(x1,y1,x2,y2){
	return Math.sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));
}

function getMinDistance(selfConnectPoints,x,y){
	var min = 1000;
	var minIndex = -1;
	for(var i=0;i<selfConnectPoints.length;i++){
		var distance = getDistanceByTwoPoints(selfConnectPoints[i].attr("cx"),selfConnectPoints[i].attr("cy"),x,y);
		if(min>distance){
			min = distance;
			minIndex=i;
		}
	}
	return minIndex;
}

/**生成自定义的矩形
 * @argument 从id开始就都是load时候用的参数
 * */
function diyRect(paper,initX,initY,initWidth,initHeight,id,code,dataArrayStr,__title){
	this.type = "d_shape";
	this.pageType="codeRect"
	var defaultId;
	if(id){
		defaultId = id;
	}else{
		defaultId = "diy_rect_"+new Date().getTime();
	}
	//文字距离矩形左边的空白距离
	var textSpaceLeft = 10;
	//存放文字
	var textContain;
	//与textContain比，__titleContain优先显示
	var __titleContain;
	/**dataArrayStr保存从编辑传来的object数组转化的原始字符串*/
	var dAStr;
	if(dataArrayStr){
		dAStr = dataArrayStr;
	}
	var selfObj = this;
	//真正矩形的size
	var real_x = initX;
	var real_y = initY;
	var real_width = initWidth;
	var real_height = initHeight;
	var space = 0;//矩形和调整框的空白处
	//调整框的size
	var x = initX-space;
	var y = initY-space;
	var width = initWidth+2*space;
	var height = initHeight+2*space;
	var real_pic = paper.rect(real_x,real_y,real_width,real_height,10);
	real_pic.attr({fill: "rgb(255,255,204)", stroke: "rgb(153,0,51)", "fill-opacity": 1, "stroke-width": .8, cursor: "move"});
	real_pic.data("id",defaultId);
	//pic是调整框
	var pic = paper.rect(x,y,width,height);
	pic.attr({stroke: "#99ccff", "fill-opacity": 1, "stroke-width": .8, cursor: "move","stroke-dasharray": "--"});
	/**用来存放relation点的数组(0:上 1:左 2:下 3:右)*/
	var rcArray = new Array(4);
	//用来标志连接的点
	var sideLength = 5;
	var conTopMark = paper.rect(x+initWidth/2-sideLength/2,y-sideLength/2,sideLength,sideLength);
	var conLeftMark = paper.rect(x-sideLength/2,y+real_height/2-sideLength/2,sideLength,sideLength);
	var conBottomMark = paper.rect(x+width/2-sideLength/2,y+real_height-sideLength/2,sideLength,sideLength);
	var conRightMark = paper.rect(x+width-sideLength/2,y+real_height/2-sideLength/2,sideLength,sideLength);
	conTopMark.attr({stroke: "red",fill: "red", "fill-opacity": 1}).hide();
	conLeftMark.attr({stroke: "red",fill: "red", "fill-opacity": 1}).hide();
	conBottomMark.attr({stroke: "red",fill: "red", "fill-opacity": 1}).hide();
	conRightMark.attr({stroke: "red",fill: "red", "fill-opacity": 1}).hide();
	/**用于标记连接点的集合*/
	var conMarks = [conTopMark,conLeftMark,conBottomMark,conRightMark];
	
	var innerText = paper.text(real_x-0+textSpaceLeft,real_y+real_height/2,"");
	innerText.attr({"text-anchor":"start"});
	
	var ponint_r = 5;
	var leftTop_c,centerTop_c,rightTop_c,leftMiddle_c,rightMiddle_c,leftBottom_c,centerBottom_c,rightBottom_c;
	var selfConnectPoints ;
	
	var isAdjusting = false;
	
	/**是否自己被ProcedureContain包含*/
	this.isProcedureContained = false;
	
	/**参数 dx,dy*/
	this.rect_move_connect = function (dx, dy) {
        var att = {x: real_pic.attr("x") + dx, y: real_pic.attr("y") + dy};
        real_pic.attr(att);
//        pic.attr({x: real_pic.attr("x") + dx-space, y: real_pic.attr("y") + dy-space,"width":real_pic.attr("width")+2*space,"height":real_pic.attr("height")+2*space});
        pic.attr({x: real_pic.attr("x")-space, y: real_pic.attr("y")-space});
        eve(defaultId+".custom.rectResize",'',real_pic.attr("x")-space,real_pic.attr("y")-space,real_pic.attr("width")+2*space,real_pic.attr("height")+2*space);
        
        for(var i = 0;i<rcArray.length;i++){
        	if(rcArray[i] && rcArray[i][1]==null){
        		//就是branch的父或子矩形
        		if(i==2){
            		rcArray[i][0].line_move_branch(null,null,centerBottom_c.attr("cx"),centerBottom_c.attr("cy"),"bottom");
        		}else if(i==0){
        			rcArray[i][0].line_move_branch(null,null,centerTop_c.attr("cx"),centerTop_c.attr("cy"),"top",selfObj);
        		}
        	}
        }
        paper.safari();
    }
    
	this.rect_move_connect_dist = function (distx,disty,conObj) {
		var dx ;
		var dy ;
		for(var rIn = 0;rIn<rcArray.length;rIn++){
			if(rcArray[rIn] !=null && rcArray[rIn][0]==conObj){
				switch(rIn){
					case 0:
						dx = centerTop_c.attr("cx")-distx;
						dy = centerTop_c.attr("cy")-disty;
						break;
					case 1:
						dx = leftMiddle_c.attr("cx")-distx;
						dy = leftMiddle_c.attr("cy")-disty;
						break;
					case 2:
						dx = centerBottom_c.attr("cx")-distx;
						dy = centerBottom_c.attr("cy")-disty;
						break;
					case 3:
						dx = rightMiddle_c.attr("cx")-distx;
						dy = rightMiddle_c.attr("cy")-disty;
						break;
				}
				dx = 0-dx;
				dy = 0-dy;
				break;
			}
		}
        var att = {x: real_pic.attr("x") + dx, y: real_pic.attr("y") + dy};
        real_pic.attr(att);
        pic.attr({x: real_pic.attr("x")-space, y: real_pic.attr("y")-space});
        eve(defaultId+".custom.rectResize",'',real_pic.attr("x")-space,real_pic.attr("y")-space,real_pic.attr("width")+2*space,real_pic.attr("height")+2*space);
        for(var i = 0;i<rcArray.length;i++){
        	if(rcArray[i] && rcArray[i][1]==null){
        		//就是branch的父或子矩形
        		if(i==2){
        			//TODO !!!!!
            		rcArray[i][0].line_move_branch(null,null,centerBottom_c.attr("cx"),centerBottom_c.attr("cy"),"bottom");
        		}else if(i==0){
        			rcArray[i][0].line_move_branch(null,null,centerTop_c.attr("cx"),centerTop_c.attr("cy"),"top",selfObj);
        		}
        	}
        }
        paper.safari();
    }
    
    this.rect_move_connect_mouseUp = function () {
    	 eve("custom.connectionHandler_branch",'',selfObj,real_pic,leftTop_c.attr("cx"),leftTop_c.attr("cy"),rightBottom_c.attr("cx"), rightBottom_c.attr("cy"));//触发事件
	     selfObj.eveConnectProcedureContain();
    }
    
    this.overlay  = function (branchLineObj){
    	innerText.insertAfter(branchLineObj);
    	leftTop_c.insertAfter(branchLineObj);
    	centerTop_c.insertAfter(branchLineObj);
    	rightTop_c.insertAfter(branchLineObj);
    	leftMiddle_c.insertAfter(branchLineObj);
    	rightMiddle_c.insertAfter(branchLineObj);
    	leftBottom_c.insertAfter(branchLineObj);
    	centerBottom_c.insertAfter(branchLineObj);
    	rightBottom_c.insertAfter(branchLineObj);
    	conTopMark.insertAfter(branchLineObj);
    	conLeftMark.insertAfter(branchLineObj);
    	conBottomMark.insertAfter(branchLineObj);
    	conRightMark.insertAfter(branchLineObj);
    	
    	real_pic.insertAfter(branchLineObj);
    	pic.insertAfter(branchLineObj);
    }
    
    /**由ProcedureContainRect的移动触发自己移动*/
    this.linkageByProcedureContainRect = function (dx, dy) {
        var att = {x: real_pic.attr("x") + dx, y: real_pic.attr("y") + dy};
        real_pic.attr(att);
        pic.attr({x: real_pic.attr("x")-space, y: real_pic.attr("y")-space});
        eve(defaultId+".custom.rectResize",'',real_pic.attr("x")-space,real_pic.attr("y")-space,real_pic.attr("width")+2*space,real_pic.attr("height")+2*space,true);
        paper.safari();
    }
	
	/**onstart*/
    var rect_dragger = function () {
        this.ox = this.attr("x");
        this.oy = this.attr("y");
        this.animate({"fill-opacity": .3}, 500);
    },/**onmove*/
        rect_move = function (dx, dy) {
            var att = {x: this.ox + dx, y: this.oy + dy};
            this.attr(att);
            pic.attr({x: this.ox + dx-space, y: this.oy + dy-space,"width":this.attr("width")+2*space,"height":this.attr("height")+2*space});
            eve(defaultId+".custom.rectResize",'',this.attr("x")-space,this.attr("y")-space,this.attr("width")+2*space,this.attr("height")+2*space);
//            rectResizeHandler(this.attr("x")-space,this.attr("y")-space,this.attr("width")+2*space,this.attr("height")+2*space);
            for(var i = 0;i<rcArray.length;i++){
            	if(rcArray[i] && rcArray[i][1]==null){
            		//就是branch的父或子矩形
            		if(i==2){
	            		rcArray[i][0].line_move_branch(null,null,centerBottom_c.attr("cx"),centerBottom_c.attr("cy"),"bottom");
            		}
            		else if(i==0){
            			rcArray[i][0].line_move_branch(null,null,centerTop_c.attr("cx"),centerTop_c.attr("cy"),"top",selfObj);
            		}
            		//TODO 左右2点
            		
            	}
            }
            paper.safari();
        },/**onend*/
        rect_up = function () {
//        	console.log("rect_up call eve!");
        	 //如果是作为branch的父或子矩形，则要联动
	        eve("custom.connectionHandler_branch",'',selfObj,real_pic,leftTop_c.attr("cx"),leftTop_c.attr("cy"),rightBottom_c.attr("cx"), rightBottom_c.attr("cy"));//触发事件
//           	console.log("connectProcedureContain!!!");
//			eve("custom.connectProcedureContain",'',selfObj,defaultId,leftTop_c.attr("cx"),rightBottom_c.attr("cx"),leftTop_c.attr("cy"),rightBottom_c.attr("cy"));
	        selfObj.eveConnectProcedureContain();
	        eve("custom.someoneAdjusting",'',real_pic);
	        this.animate({"fill-opacity": 1}, 500);
        };
	real_pic.drag(rect_move, rect_dragger, rect_up);//onmove, onstart, onend
	
	this.eveConnectProcedureContain = function (){
		eve("custom.connectProcedureContain",'',this,defaultId,leftTop_c.attr("cx"),rightBottom_c.attr("cx"),leftTop_c.attr("cy"),rightBottom_c.attr("cy"));
	}
	
	var connectProcedureContainTriggerFunction  = function (){
		eve("custom.connectProcedureContain",'',selfObj,defaultId,leftTop_c.attr("cx"),rightBottom_c.attr("cx"),leftTop_c.attr("cy"),rightBottom_c.attr("cy"));
	}
	
	/**注册事件(触发各个组件自己再去触发ProcedureContain的关系判断事件)*/
	eve.on("custom.connectProcedureContainTrigger",connectProcedureContainTriggerFunction);
	
	var connectionHandlerByBranchFunction = function (){
		eve("custom.connectionHandler_branch",'',selfObj,real_pic,leftTop_c.attr("cx"),leftTop_c.attr("cy"),rightBottom_c.attr("cx"), rightBottom_c.attr("cy"));//触发事件
	}
	
	//注册事件(由移动branch的out2个顶点触发的是否关联矩形的事件)
	eve.on("custom.connectionHandlerByBranch",connectionHandlerByBranchFunction);
	
	/**当调整8个点中某个点的位置（矩形的大小）时，即重新调整8个点位置和实际的rect的大小和位置*/
	var rectResizeHandler = function (x,y,width,height,notNeedToRcArrayAdjust){
		leftTop_c.attr({cx:x,cy:y});
		centerTop_c.attr({cx:x+width/2,cy:y});
		rightTop_c.attr({cx:x+width,cy:y});
		leftMiddle_c.attr({cx:x,cy:y+height/2});
		rightMiddle_c.attr({cx:x+width,cy:y+height/2});
		leftBottom_c.attr({cx:x,cy:y+height});
		var beforeCbX = centerBottom_c.attr("cx");
		var beforeCbY = centerBottom_c.attr("cy");
		centerBottom_c.attr({cx:x+width/2,cy:y+height});
		rightBottom_c.attr({cx:x+width,cy:y+height});
		real_pic.attr({"x":x-0+space,"y":y-0+space,"width":width-2*space,"height":height-2*space});
		innerText.attr({"x":real_pic.attr("x")-0+textSpaceLeft,"y":real_pic.attr("y")-0+real_pic.attr("height")/2});
		if(!notNeedToRcArrayAdjust){
			rectMove_rcArrayAdjust();
		}
		rectMove_conMarksAdjust();
		showTextPreHandler();
		//触发是否在ProcedureContain内的事件(关联/解除之前的关系)
	}
	//注册事件
	eve.on(defaultId+".custom.rectResize",rectResizeHandler);
	
	/**调整定位点的拖拉事件处理*/
	var point_dragger = function () {
        this.ox = this.type == "rect" ? this.attr("x") : this.attr("cx");
        this.oy = this.type == "rect" ? this.attr("y") : this.attr("cy");
        this.owidth = pic.attr("width");
        this.oheight = pic.attr("height");
        this.animate({"fill-opacity": .3}, 500);
    },/**onmove*/
        point_move = function (dx, dy) {
        	if(this.data("id")==defaultId+"leftTop_c"){
        		pic.attr({x:this.ox-0+dx,y:this.oy-0+dy,"width":this.owidth-0-dx,"height":this.oheight-0-dy});
        		//触发custom.rectResize事件
        		eve(defaultId+".custom.rectResize",'',pic.attr("x"),pic.attr("y"),pic.attr("width"),pic.attr("height"));
        	}else if(this.data("id")==defaultId+"centerTop_c"){
        		pic.attr({y:this.oy-0+dy,"height":this.oheight-0-dy});
        		eve(defaultId+".custom.rectResize",'',pic.attr("x"),pic.attr("y"),pic.attr("width"),pic.attr("height"));
        	}else if(this.data("id")==defaultId+"rightTop_c"){
        		pic.attr({y:this.oy-0+dy,"width":this.owidth-0+dx,"height":this.oheight-0-dy});
        		eve(defaultId+".custom.rectResize",'',pic.attr("x"),pic.attr("y"),pic.attr("width"),pic.attr("height"));
        	}else if(this.data("id")==defaultId+"leftMiddle_c"){
        		pic.attr({x:this.ox-0+dx,"width":this.owidth-0-dx});
        		eve(defaultId+".custom.rectResize",'',pic.attr("x"),pic.attr("y"),pic.attr("width"),pic.attr("height"));
        	}else if(this.data("id")==defaultId+"rightMiddle_c"){
        		pic.attr({"width":this.owidth-0+dx});
        		eve(defaultId+".custom.rectResize",'',pic.attr("x"),pic.attr("y"),pic.attr("width"),pic.attr("height"));
        	}else if(this.data("id")==defaultId+"leftBottom_c"){
        		pic.attr({x:this.ox-0+dx,"width":this.owidth-0-dx,"height":this.oheight-0+dy});
        		eve(defaultId+".custom.rectResize",'',pic.attr("x"),pic.attr("y"),pic.attr("width"),pic.attr("height"));
        	}else if(this.data("id")==defaultId+"centerBottom_c"){
        		pic.attr({"height":this.oheight-0+dy});
        		eve(defaultId+".custom.rectResize",'',pic.attr("x"),pic.attr("y"),pic.attr("width"),pic.attr("height"));
        	}else if(this.data("id")==defaultId+"rightBottom_c"){
        		pic.attr({"width":this.owidth-0+dx,"height":this.oheight-0+dy});
        		eve(defaultId+".custom.rectResize",'',pic.attr("x"),pic.attr("y"),pic.attr("width"),pic.attr("height"));
        	}
            paper.safari();
            for(var i = 0;i<rcArray.length;i++){
	        	if(rcArray[i] && rcArray[i][1]==null){
	        		//就是branch的父或子矩形
	        		if(i==2){
	            		rcArray[i][0].line_move_branch(null,null,centerBottom_c.attr("cx"),centerBottom_c.attr("cy"),"bottom");
            		}else if(i==0){
            			rcArray[i][0].line_move_branch(null,null,centerTop_c.attr("cx"),centerTop_c.attr("cy"),"top",selfObj);
            		}
            		//TODO
	        	}
       		}
        },/**onend*/
        point_up = function () {
//        	console.log("connectProcedureContain!!!");
        	selfObj.eveConnectProcedureContain();
//			eve("custom.connectProcedureContain",'',selfObj,defaultId,leftTop_c.attr("cx"),rightBottom_c.attr("cx"),leftTop_c.attr("cy"),rightBottom_c.attr("cy"));
            this.animate({"fill-opacity": .5}, 500);
            eve("custom.someoneAdjusting",'',real_pic);
        };
	
    /**生成8个定位点*/
	var draw8PointAround = function (paper,x,y,width,height,r, point_dragger, point_move, point_up){
		leftTop_c = paper.circle(x,y,r).data("id",defaultId+"leftTop_c");
		leftTop_c.drag(point_move, point_dragger, point_up);
		leftTop_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "nw-resize"});
		centerTop_c = paper.circle(x+width/2,y,r).data("id",defaultId+"centerTop_c");
		centerTop_c.drag(point_move, point_dragger, point_up);
		centerTop_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "n-resize"});
		rightTop_c = paper.circle(x+width,y,r).data("id",defaultId+"rightTop_c");
		rightTop_c.drag(point_move, point_dragger, point_up);
		rightTop_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "ne-resize"});
		leftMiddle_c = paper.circle(x,y+height/2,r).data("id",defaultId+"leftMiddle_c");
		leftMiddle_c.drag(point_move, point_dragger, point_up);
		leftMiddle_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "e-resize"});
		rightMiddle_c = paper.circle(x+width,y+height/2,r).data("id",defaultId+"rightMiddle_c");;
		rightMiddle_c.drag(point_move, point_dragger, point_up);
		rightMiddle_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "e-resize"});
		leftBottom_c = paper.circle(x,y+height,r).data("id",defaultId+"leftBottom_c");
		leftBottom_c.drag(point_move, point_dragger, point_up);
		leftBottom_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "ne-resize"});
		centerBottom_c = paper.circle(x+width/2,y+height,r).data("id",defaultId+"centerBottom_c");
		centerBottom_c.drag(point_move, point_dragger, point_up);
		centerBottom_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "n-resize"});
		rightBottom_c = paper.circle(x+width,y+height,r).data("id",defaultId+"rightBottom_c");
		rightBottom_c.drag(point_move, point_dragger, point_up);
		rightBottom_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "nw-resize"});
		selfConnectPoints = [centerTop_c,leftMiddle_c,centerBottom_c,rightMiddle_c];
	}
	
	var hideAdjustPic = function(){
		pic.hide();
		leftTop_c.hide();
		centerTop_c.hide();
		rightTop_c.hide();
		leftMiddle_c.hide();
		rightMiddle_c.hide();
		leftBottom_c.hide();
		centerBottom_c.hide();
		rightBottom_c.hide();
		isAdjusting = false;
	}
	
	var showAdjustPic = function(){
		pic.show();
		leftTop_c.show();
		centerTop_c.show();
		rightTop_c.show();
		leftMiddle_c.show();
		rightMiddle_c.show();
		leftBottom_c.show();
		centerBottom_c.show();
		rightBottom_c.show();
		isAdjusting=true;
	}
	
	//生成8个点
	draw8PointAround(paper,x,y,width,height,ponint_r, point_dragger, point_move, point_up);
	hideAdjustPic();
	real_pic.click(function (){
		eve("custom.someoneAdjusting",'',real_pic);
	});
	
	/**计算字符串的长度(半角1个长度,全角2个长度)*/
	var computeStrLength = function (str){
		var lengthTotal = 0;
		for(var i=0;i<str.length;i++){
			var s=str[i];
			if(isHalf(s)){
				lengthTotal+=1;
			}else{
				lengthTotal+=2;
			}
		}
		return lengthTotal;
	}
	
	/**是否是半角*/
	var isHalf = function (str){
		if(str.match(/[\u0000-\u00ff]/g)){
			return true;
		}
		return false;
	}
	
	//显示文字前的处理
	var showTextPreHandler = function (){
		if(__titleContain && __titleContain!=null && __titleContain!=""){
			var result = __titleContain;
			innerText.attr({"text":__titleContain});
			innerText.attr({"title":__titleContain});
			innerText.attr({"font-size":15});
			innerText.attr({"text-anchor":"middle"});
			innerText.attr({"font-weight":"bold"});
			innerText.attr({x:real_pic.attr("x")+real_pic.attr("width")/2,y:real_pic.attr("y")+real_pic.attr("height")/2});
		}else if(textContain && textContain!=null && textContain!=""){
			//可容纳文字最大高度
			var maxHeight_text = rightBottom_c.attr("cy") - leftTop_c.attr("cy");
			var lineArray = textContain.split("\n");
			var lineHeight = 12;
			var maxLine = (maxHeight_text-10)/12;
			lineArray.splice(maxLine);
			//可容纳文字最大宽度
			var maxWidth_text = rightBottom_c.attr("cx") - leftTop_c.attr("cx");
			var semiangleWidth = 6;
			var maxWordCountInLine = (maxWidth_text-10)/semiangleWidth;
//			console.log("maxWordCountInLine",maxWordCountInLine);
			for(var i=0;i<lineArray.length;i++){
				var currentLineStr = lineArray[i];
				var lengthTotal = computeStrLength(currentLineStr);
//				console.log("lengthTotal",lengthTotal);
				if(lengthTotal > maxWordCountInLine){
					var currentLineStrLength = 0;
					for(var j=0;j<currentLineStr.length;j++){
						if(isHalf(currentLineStr[j])){
							if(currentLineStrLength+1>maxWordCountInLine){
								var subLine = currentLineStr.substring(0,j);
								lineArray[i] = subLine;
//								console.log(lineArray[i].length);
								break;
							}else{
								currentLineStrLength++;
							}
						}else{
							if(currentLineStrLength+2>maxWordCountInLine){
								var subLine = currentLineStr.substring(0,j);
								lineArray[i] = subLine;
//								console.log(lineArray[i].length);
								break;
							}else{
								currentLineStrLength+=2;
							}
						}
					}
				}		
			}
			result = lineArray.join("\n");
			innerText.attr({"text":result});
			innerText.attr({"title":textContain});
		}
		return result;
	}
	
	
	if(__title){
		__titleContain = __title;
	} 
	if(code){
		textContain = code;
	}
	showTextPreHandler();
	
	/**
	 * @argument dataArray:[{},{}...]
	 * */
	this.update = function (dataArray){
		var results = resolveRect(dataArray);
		if(results[0]){
			textContain = results[0];
			dAStr = results[1];
			showTextPreHandler();
		}
	}
	
	var someoneAdjustingFunction = function (triggerEle){
		if(triggerEle == real_pic){
			//是自己的real_pic触发的
			if(!isAdjusting){
				showAdjustPic();
				
				toEdit(defaultId,selfObj.pageType,dAStr);
				//remove
//				document.getElementById("rectText").value=innerText.attr("text");
//				document.getElementById("rectText").onchange = function (){
//					textContain = document.getElementById("rectText").value;
//					showTextPreHandler();
//				}
				//
			}
			eve.stop();
		}else{
			//是其他的real_pic触发的
			if(isAdjusting){
				hideAdjustPic();
			}
		}
	}
	
	eve.on("custom.someoneAdjusting",someoneAdjustingFunction);
	
	var someoneDeleteFunction = function(){
		if(isAdjusting){
			for(var i = 0;i<rcArray.length;i++){
				if(rcArray[i]!=null){
					if(rcArray[i][0].point_move_remove_connect){
						rcArray[i][0].point_move_remove_connect(null,selfObj);
					}
				}
			}
			real_pic.remove();
			pic.remove();
			innerText.remove();
			leftTop_c.remove();
			centerTop_c.remove();
			rightTop_c.remove();
			leftMiddle_c.remove();
			rightMiddle_c.remove();
			leftBottom_c.remove();
			centerBottom_c.remove();
			rightBottom_c.remove();
			for(var i=0 ; i< conMarks.length;i++){
				conMarks[i].remove();
			}
			removeShape(selfObj);
			removeComponentObjMap(defaultId);
			eve.off(defaultId+".custom.rectResize", rectResizeHandler);
			eve.off("custom.someoneAdjusting",someoneAdjustingFunction);
			eve.off("custom.connectionHandlerByBranch", connectionHandlerByBranchFunction);
			eve.off("custom.connectProcedureContainTrigger", connectProcedureContainTriggerFunction);
			eve.off("custom.someoneDelete", someoneDeleteFunction);
			selfObj = null;
		}	
	}

	this.otherConDetelte = function (triggerObj){
		for(var i=0;i<rcArray.length;i++){
			if(rcArray[i]!=null){
				if(rcArray[i][0]==triggerObj){
					rcArray[i]=null;
					conMarks[i].hide();
				}
			}
		}
		
	};
	
	//注册删除元素事件
	eve.on("custom.someoneDelete",someoneDeleteFunction);
	
	this.conMarkShow = function(type){
		if(type=="top"){
			conMarks[0].show();
		}else if(type=="bottom"){
			conMarks[2].show();
		}
	}
	
	/**解除存放在位置关系集合rcArray中的关系，并消除关系点*/
	this.conMarkHide = function(type){
		if(type=="top"){
			conMarks[0].hide();
			rcArray[0]=null;
		}else if(type=="bottom"){
			conMarks[2].hide();
			rcArray[2]=null;
		}
	}
	
	//注册事件
	//calPointObj用于计算最近距离的远端连接点
	eve.on("custom.connectionHandler_rect",function (triggerObj,connectPointObj,calPointObj){
		var connectPoint_x = connectPointObj.attr("cx");
		var connectPoint_y = connectPointObj.attr("cy");
		var minRectX = leftTop_c.attr("cx");
		var minRectY = leftTop_c.attr("cy");
		var maxRectX = rightBottom_c.attr("cx");
		var maxRectY = rightBottom_c.attr("cy");
		if(connectPoint_x>=minRectX && connectPoint_x<= maxRectX && connectPoint_y>=minRectY && connectPoint_y<= maxRectY ){
			//连接
			var isConnected = false;
			var connectedIndex = -1;
			for(var i=0;i<rcArray.length;i++){
				if(rcArray[i]!=null){
					if(rcArray[i][0]==triggerObj && rcArray[i][1]==connectPointObj){
						connectedIndex = i;
						isConnected=true;
						break;
					}
				}
			}
			if(isConnected){
				//如果线和矩形已经连接过了，则要先去掉之前的连接
				triggerObj.point_move_remove_connect(null,selfObj);
				rcArray[connectedIndex]=null;
				conMarks[connectedIndex].hide();
			}
			var minIndex = getMinDistance(selfConnectPoints,calPointObj.attr("cx"),calPointObj.attr("cy"));
			//再看矩形上要连接的点是否已经存在关系了，如果存在则要去掉
			if(rcArray[minIndex]!=null && rcArray[minIndex][0]!=null){
				rcArray[minIndex][0].point_move_remove_connect(null,selfObj);
				rcArray[minIndex]=null;
				conMarks[minIndex].hide();
			}
			//最后建立全新的关系
			var scPoint = selfConnectPoints[minIndex];
			rcArray[minIndex]=[triggerObj,connectPointObj];
			triggerObj.point_move_connect(connectPointObj,scPoint.attr("cx"),scPoint.attr("cy"),selfObj);
			conMarks[minIndex].show();
//			if(!isConnected){
//				//没有任何连接
//				var minIndex = getMinDistance(selfConnectPoints,calPointObj.attr("cx"),calPointObj.attr("cy"));
//				var scPoint = selfConnectPoints[minIndex];
//				rcArray[minIndex]=[triggerObj,connectPointObj];
//				triggerObj.point_move_connect(connectPointObj,scPoint.attr("cx"),scPoint.attr("cy"),selfObj);
//				conMarks[minIndex].show();
//			}else{
//				//已经连接过了
//				var scPoint = selfConnectPoints[connectedIndex];
//				triggerObj.point_move_connect(connectPointObj,scPoint.attr("cx"),scPoint.attr("cy"),selfObj);			
//			}
		}else{
			for(var i=0;i<rcArray.length;i++){
				if(rcArray[i]!=null){
					if(rcArray[i][0]==triggerObj && rcArray[i][1]==connectPointObj){
						rcArray[i]=null;
						conMarks[i].hide();
						break;
					}
				}
			}
			if(triggerObj.point_move_remove_connect){
				triggerObj.point_move_remove_connect(connectPointObj,selfObj);
			}
		}
	});
	
	var rectMove_rcArrayAdjust = function(){
		for(var i=0;i<rcArray.length;i++){
			if(rcArray[i]!=null){
				if(rcArray[i][0].point_move_connect){
					if(i==0){
						rcArray[i][0].point_move_connect(rcArray[i][1],centerTop_c.attr("cx"),centerTop_c.attr("cy"),selfObj,true);
					}else if(i==1){
						rcArray[i][0].point_move_connect(rcArray[i][1],leftMiddle_c.attr("cx"),leftMiddle_c.attr("cy"),selfObj,true);
					}else if(i==2){
						rcArray[i][0].point_move_connect(rcArray[i][1],centerBottom_c.attr("cx"),centerBottom_c.attr("cy"),selfObj,true);
					}else if(i==3){
						rcArray[i][0].point_move_connect(rcArray[i][1],rightMiddle_c.attr("cx"),rightMiddle_c.attr("cy"),selfObj,true);
					}
				}
			}
		}
	}
	
	var rectMove_conMarksAdjust = function(){
		for(var i=0;i<conMarks.length;i++){
			if(i==0){
				conMarks[i].attr({x:centerTop_c.attr("cx")-sideLength/2,y:centerTop_c.attr("cy")-sideLength/2});
			}else if(i==1){
				conMarks[i].attr({x:leftMiddle_c.attr("cx")-sideLength/2,y:leftMiddle_c.attr("cy")-sideLength/2});
			}else if(i==2){
				conMarks[i].attr({x:centerBottom_c.attr("cx")-sideLength/2,y:centerBottom_c.attr("cy")-sideLength/2});
			}else if(i==3){
				conMarks[i].attr({x:rightMiddle_c.attr("cx")-sideLength/2,y:rightMiddle_c.attr("cy")-sideLength/2});
			}
		}
	}
	
	/**为Branch添加rcArray方法*/
	this.addRcArray = function (triggerObj,type){
		if(type=="top"){
			rcArray[0] = [triggerObj,null];
		}else if(type=="bottom"){
			rcArray[2] = [triggerObj,null];
		}
	}
	
	this.getId = function (){
		return defaultId;
	}
	
	this.getCode = function(){
//		return innerText.attr("text");
		var tc ="";
		if(textContain){
			tc = textContain.replace("code:","");
		}
		return tc;
	}
	
	this.getTitle = function(){
		return __titleContain;
	}
	
	this.getX = function(){
		return real_pic.attr("x");
	}
	
	this.getY = function(){
		return real_pic.attr("y");
	}
	
	this.getWidth = function(){
		return real_pic.attr("width");
	}
	
	this.getHeight = function(){
		return real_pic.attr("height");
	}
	this.getReal_pic = function (){
		return real_pic;
	}
	this.getLeftTop_x = function(){
		return leftTop_c.attr("cx");
	}
	this.getLeftTop_y = function(){
		return leftTop_c.attr("cy");
	}
	this.getRightBottom_x = function(){
		return rightBottom_c.attr("cx");
	}
	this.getRightBottom_y = function(){
		return rightBottom_c.attr("cy");
	}
	this.getDAStr = function (){
		return dAStr;
	}
	
	return this;
}

//画procedure容器矩形
function diyProcedureContainRect(paper,initX,initY,initWidth,initHeight,id,code,dataArrayStr){
	this.type = "d_contain_procedure";
	this.pageType="procedureContainRect";
	var defaultId;
	if(id){
		defaultId = id;
	}else{
		defaultId = "diy_conProcedure_"+new Date().getTime();
	}
	var selfObj = this;
	//真正矩形的size
	var real_x = initX;
	var real_y = initY;
	var real_width = initWidth;
	var real_height = initHeight;
	var space = 0;//矩形和调整框的空白处
	//调整框的size
	var x = initX-space;
	var y = initY-space;
	var width = initWidth+2*space;
	var height = initHeight+2*space;
	var real_pic = paper.rect(real_x,real_y,real_width,real_height).toBack();
	real_pic.attr({fill: "#ECE9D8", stroke: "#ACA899", "fill-opacity": .3, "stroke-width": .8, cursor: "pointer"});
	real_pic.data("id",defaultId);
	//pic是调整框
	var pic = paper.rect(x,y,width,height).toBack();
	pic.attr({stroke: "#99ccff", "fill-opacity": .3, "stroke-width": .8, cursor: "move","stroke-dasharray": "--"});
	/**用来存放relation点的数组(0:上 1:左 2:下 3:右)*/
	var rcArray = new Array(4);
	//用来标志连接的点
	var sideLength = 5;
	var conTopMark = paper.rect(x+initWidth/2-sideLength/2,y-sideLength/2,sideLength,sideLength);
	var conLeftMark = paper.rect(x-sideLength/2,y+real_height/2-sideLength/2,sideLength,sideLength);
	var conBottomMark = paper.rect(x+width/2-sideLength/2,y+real_height-sideLength/2,sideLength,sideLength);
	var conRightMark = paper.rect(x+width-sideLength/2,y+real_height/2-sideLength/2,sideLength,sideLength);
	conTopMark.attr({stroke: "red",fill: "red", "fill-opacity": 1}).hide();
	conLeftMark.attr({stroke: "red",fill: "red", "fill-opacity": 1}).hide();
	conBottomMark.attr({stroke: "red",fill: "red", "fill-opacity": 1}).hide();
	conRightMark.attr({stroke: "red",fill: "red", "fill-opacity": 1}).hide();
	/**用于标记连接点的集合*/
	var conMarks = [conTopMark,conLeftMark,conBottomMark,conRightMark];
	var ponint_r = 5;
	var leftTop_c,centerTop_c,rightTop_c,leftMiddle_c,rightMiddle_c,leftBottom_c,centerBottom_c,rightBottom_c;
	var selfConnectPoints ;
	var isAdjusting = false;
	
	var titleSpaceLeft = 10;
	var titleSpaceTop = 10;
	var innerTitle = paper.text(real_x-0+titleSpaceLeft,real_y-0+titleSpaceTop,"");
	innerTitle.attr({"text-anchor":"start","font-size":15,"fill":"#3166C6"});
	var dAStr ;
	if(dataArrayStr){
		dAStr = dataArrayStr;
	}
	var title;
	if(code){
		title = code;
		innerTitle.attr({"text":code,"title":code});
	}
	
	/**
	 * 用来存放包含在其之中的内部组件的object的map
	 * key:objId value:obj
	 * */
	var innerObjs = {};
	
	/**联动innerObjs中的组件*/
	var linkageInnerObjs = function (dx,dy){
		for(var oId in innerObjs){
			innerObjs[oId].linkageByProcedureContainRect(dx,dy);
		}
	}
	
	this.rect_move_connect_dist = function (distx,disty,conObj) {
		var dx ;
		var dy ;
		for(var rIn = 0;rIn<rcArray.length;rIn++){
			if(rcArray[rIn] !=null && rcArray[rIn][0]==conObj){
				switch(rIn){
					case 0:
						dx = centerTop_c.attr("cx")-distx;
						dy = centerTop_c.attr("cy")-disty;
						break;
					case 1:
						dx = leftMiddle_c.attr("cx")-distx;
						dy = leftMiddle_c.attr("cy")-disty;
						break;
					case 2:
						dx = centerBottom_c.attr("cx")-distx;
						dy = centerBottom_c.attr("cy")-disty;
						break;
					case 3:
						dx = rightMiddle_c.attr("cx")-distx;
						dy = rightMiddle_c.attr("cy")-disty;
						break;
				}
				dx = 0-dx;
				dy = 0-dy;
				break;
			}
		}
        var att = {x: real_pic.attr("x") + dx, y: real_pic.attr("y") + dy};
        real_pic.attr(att);
        pic.attr({x: real_pic.attr("x")-space, y: real_pic.attr("y")-space});
        eve(defaultId+".custom.rectResize",'',real_pic.attr("x")-space,real_pic.attr("y")-space,real_pic.attr("width")+2*space,real_pic.attr("height")+2*space);
        
        linkageInnerObjs(dx , dy);
        paper.safari();
    }
    
    this.rect_move_connect_mouseUp = function () {
    	eve("custom.connectProcedureContainTrigger");
    }
    
	/**onstart*/
    var rect_dragger = function () {
        this.ox = this.attr("x");
        this.oy = this.attr("y");
        this.preX = this.ox;
        this.preY = this.oy;
        this.animate({"fill-opacity": .3}, 500);
    },/**onmove*/
        rect_move = function (dx, dy) {
            var att = {x: this.ox + dx, y: this.oy + dy};
            this.attr(att);
            pic.attr({x: this.ox + dx-space, y: this.oy + dy-space,"width":this.attr("width")+2*space,"height":this.attr("height")+2*space});
            eve(defaultId+".custom.rectResize",'',this.attr("x")-space,this.attr("y")-space,this.attr("width")+2*space,this.attr("height")+2*space);
            
        	var currentDx = this.preX - this.attr("x");
        	var currentDy = this.preY - this.attr("y");
            linkageInnerObjs(0-currentDx , 0-currentDy);
            this.preX = this.attr("x");
            this.preY = this.attr("y");
            paper.safari();
        },/**onend*/
        rect_up = function () {
        	eve("custom.someoneAdjusting",'',real_pic);
        	eve("custom.connectProcedureContainTrigger");
            this.animate({"fill-opacity": .3}, 500);
        };
	real_pic.drag(rect_move, rect_dragger, rect_up);//onmove, onstart, onend
	
	/**当调整8个点中某个点的位置（矩形的大小）时，即重新调整8个点位置和实际的rect的大小和位置*/
	var rectResizeHandler = function (x,y,width,height){
		leftTop_c.attr({cx:x,cy:y});
		centerTop_c.attr({cx:x+width/2,cy:y});
		rightTop_c.attr({cx:x+width,cy:y});
		leftMiddle_c.attr({cx:x,cy:y+height/2});
		rightMiddle_c.attr({cx:x+width,cy:y+height/2});
		leftBottom_c.attr({cx:x,cy:y+height});
		centerBottom_c.attr({cx:x+width/2,cy:y+height});
		rightBottom_c.attr({cx:x+width,cy:y+height});
		real_pic.attr({"x":x-0+space,"y":y-0+space,"width":width-2*space,"height":height-2*space});
		innerTitle.attr({"x":real_pic.attr("x")-0+titleSpaceLeft,"y":real_pic.attr("y")-0+titleSpaceTop});
		rectMove_rcArrayAdjust();
		rectMove_conMarksAdjust();
	}
	//注册事件
	eve.on(defaultId+".custom.rectResize",rectResizeHandler);
	
	/**调整定位点的拖拉事件处理*/
	var point_dragger = function () {
        this.ox = this.attr("cx");
        this.oy = this.attr("cy");
        this.preX = this.ox;
        this.preY = this.oy;
        this.owidth = pic.attr("width");
        this.oheight = pic.attr("height");
        this.animate({"fill-opacity": .3}, 500);
    },/**onmove*/
        point_move = function (dx, dy) {
        	if(this.data("id")==defaultId+"leftTop_c"){
        		pic.attr({x:this.ox-0+dx,y:this.oy-0+dy,"width":this.owidth-0-dx,"height":this.oheight-0-dy});
        		eve(defaultId+".custom.rectResize",'',pic.attr("x"),pic.attr("y"),pic.attr("width"),pic.attr("height"));
        	}else if(this.data("id")==defaultId+"centerTop_c"){
        		pic.attr({y:this.oy-0+dy,"height":this.oheight-0-dy});
        		eve(defaultId+".custom.rectResize",'',pic.attr("x"),pic.attr("y"),pic.attr("width"),pic.attr("height"));
        	}else if(this.data("id")==defaultId+"rightTop_c"){
        		pic.attr({y:this.oy-0+dy,"width":this.owidth-0+dx,"height":this.oheight-0-dy});
        		eve(defaultId+".custom.rectResize",'',pic.attr("x"),pic.attr("y"),pic.attr("width"),pic.attr("height"));
        	}else if(this.data("id")==defaultId+"leftMiddle_c"){
        		pic.attr({x:this.ox-0+dx,"width":this.owidth-0-dx});
        		eve(defaultId+".custom.rectResize",'',pic.attr("x"),pic.attr("y"),pic.attr("width"),pic.attr("height"));
        	}else if(this.data("id")==defaultId+"rightMiddle_c"){
        		pic.attr({"width":this.owidth-0+dx});
        		eve(defaultId+".custom.rectResize",'',pic.attr("x"),pic.attr("y"),pic.attr("width"),pic.attr("height"));
        	}else if(this.data("id")==defaultId+"leftBottom_c"){
        		pic.attr({x:this.ox-0+dx,"width":this.owidth-0-dx,"height":this.oheight-0+dy});
        		eve(defaultId+".custom.rectResize",'',pic.attr("x"),pic.attr("y"),pic.attr("width"),pic.attr("height"));
        	}else if(this.data("id")==defaultId+"centerBottom_c"){
        		pic.attr({"height":this.oheight-0+dy});
        		eve(defaultId+".custom.rectResize",'',pic.attr("x"),pic.attr("y"),pic.attr("width"),pic.attr("height"));
        	}else if(this.data("id")==defaultId+"rightBottom_c"){
        		pic.attr({"width":this.owidth-0+dx,"height":this.oheight-0+dy});
        		eve(defaultId+".custom.rectResize",'',pic.attr("x"),pic.attr("y"),pic.attr("width"),pic.attr("height"));
        	}
        	
        	var currentDx = this.preX - this.attr("cx");
        	var currentDy = this.preY - this.attr("cy");
            linkageInnerObjs(0-currentDx , 0-currentDy);
            this.preX = this.attr("cx");
            this.preY = this.attr("cy");
            paper.safari();
        },/**onend*/
        point_up = function () {
        	eve("custom.someoneAdjusting",'',real_pic);
        	eve("custom.connectProcedureContainTrigger");
            this.animate({"fill-opacity": .5}, 500);
        };
	
    /**生成8个定位点*/
	var draw8PointAround = function (paper,x,y,width,height,r, point_dragger, point_move, point_up){
		leftTop_c = paper.circle(x,y,r).data("id",defaultId+"leftTop_c");
		leftTop_c.drag(point_move, point_dragger, point_up);
		leftTop_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "nw-resize"});
		centerTop_c = paper.circle(x+width/2,y,r).data("id",defaultId+"centerTop_c");
		centerTop_c.drag(point_move, point_dragger, point_up);
		centerTop_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "n-resize"});
		rightTop_c = paper.circle(x+width,y,r).data("id",defaultId+"rightTop_c");
		rightTop_c.drag(point_move, point_dragger, point_up);
		rightTop_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "ne-resize"});
		leftMiddle_c = paper.circle(x,y+height/2,r).data("id",defaultId+"leftMiddle_c");
		leftMiddle_c.drag(point_move, point_dragger, point_up);
		leftMiddle_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "e-resize"});
		rightMiddle_c = paper.circle(x+width,y+height/2,r).data("id",defaultId+"rightMiddle_c");;
		rightMiddle_c.drag(point_move, point_dragger, point_up);
		rightMiddle_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "e-resize"});
		leftBottom_c = paper.circle(x,y+height,r).data("id",defaultId+"leftBottom_c");
		leftBottom_c.drag(point_move, point_dragger, point_up);
		leftBottom_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "ne-resize"});
		centerBottom_c = paper.circle(x+width/2,y+height,r).data("id",defaultId+"centerBottom_c");
		centerBottom_c.drag(point_move, point_dragger, point_up);
		centerBottom_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "n-resize"});
		rightBottom_c = paper.circle(x+width,y+height,r).data("id",defaultId+"rightBottom_c");
		rightBottom_c.drag(point_move, point_dragger, point_up);
		rightBottom_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "nw-resize"});
		selfConnectPoints = [centerTop_c,leftMiddle_c,centerBottom_c,rightMiddle_c];
	}
	
	var hideAdjustPic = function(){
		pic.hide();
		leftTop_c.hide();
		centerTop_c.hide();
		rightTop_c.hide();
		leftMiddle_c.hide();
		rightMiddle_c.hide();
		leftBottom_c.hide();
		centerBottom_c.hide();
		rightBottom_c.hide();
		isAdjusting = false;
	}
	
	var showAdjustPic = function(){
		pic.show();
		leftTop_c.show();
		centerTop_c.show();
		rightTop_c.show();
		leftMiddle_c.show();
		rightMiddle_c.show();
		leftBottom_c.show();
		centerBottom_c.show();
		rightBottom_c.show();
		isAdjusting=true;
	}
	
	//生成8个点
	draw8PointAround(paper,x,y,width,height,ponint_r, point_dragger, point_move, point_up);
	hideAdjustPic();
	real_pic.click(function (){
		eve("custom.someoneAdjusting",'',real_pic);
	});
	
	var someoneAdjustingFunction = function (triggerEle){
		if(triggerEle == real_pic){
			//是自己的real_pic触发的
			if(!isAdjusting){
				showAdjustPic();
				toEdit(defaultId,selfObj.pageType,dAStr);
			}
			eve.stop();
		}else{
			//是其他的real_pic触发的
			if(isAdjusting){
				hideAdjustPic();
			}
		}
	}
	
	this.update = function (dataArray){
		var results = resolveprocedureContainRect(dataArray);
		title = results[0];
		dAStr = results[1];
		innerTitle.attr({"text":title,"title":title});
	}
	
	eve.on("custom.someoneAdjusting",someoneAdjustingFunction);
	
	/**判断被判断的图形是否在其之中(不会移动被判断的图形)*/
	var connectProcedureContainFunction  = function(triggerObj,triggerId,minX,maxX,minY,maxY){
		if(minX>=leftTop_c.attr("cx") && maxX<=rightBottom_c.attr("cx") && minY>=leftTop_c.attr("cy") && maxY<=rightBottom_c.attr("cy")){
			//在范围内
			if(!triggerObj.isProcedureContained){
				//没有被其他ProcedureContain包含
				//是否已经存在关系
				if(!innerObjs[triggerId]){
					innerObjs[triggerId] = triggerObj;
					triggerObj.isProcedureContained = true;
				}
			}
		}else{
			//在范围外
			if(triggerObj.isProcedureContained){
				//是否已经存在关系
				if(innerObjs[triggerId]){
					triggerObj.isProcedureContained = false;
					delete innerObjs[triggerId];
				}
			}
		}
	}
	
	/**定义判断其他组件是否在其之内的事件*/
	eve.on("custom.connectProcedureContain",connectProcedureContainFunction);
	
	var someoneDeleteFunction = function(){
		if(isAdjusting){
			for(var oId in innerObjs){
				innerObjs[oId].isProcedureContained = false;
			}
			innerObjs={};
			real_pic.remove();
			pic.remove();
			leftTop_c.remove();
			centerTop_c.remove();
			rightTop_c.remove();
			leftMiddle_c.remove();
			rightMiddle_c.remove();
			leftBottom_c.remove();
			centerBottom_c.remove();
			rightBottom_c.remove();
			for(var i=0 ; i< conMarks.length;i++){
				conMarks[i].remove();
			}
			innerTitle.remove();
			removeProcedureContain(selfObj);
			removeComponentObjMap(defaultId);
			eve.off(defaultId+".custom.rectResize", rectResizeHandler);
			eve.off("custom.someoneAdjusting",someoneAdjustingFunction);
			eve.off("custom.connectProcedureContain",connectProcedureContainFunction);
			eve.off("custom.someoneDelete", someoneDeleteFunction);
			selfObj = null;
		}	
	}

	this.otherConDetelte = function (triggerObj){
		for(var i=0;i<rcArray.length;i++){
			if(rcArray[i]!=null){
				if(rcArray[i][0]==triggerObj){
					rcArray[i]=null;
					conMarks[i].hide();
				}
			}
		}
		
	};
	
	//注册删除元素事件
	eve.on("custom.someoneDelete",someoneDeleteFunction);
	
	//注册事件
	//calPointObj用于计算最近距离的远端连接点 (离开边框很近的时候建立连接，否则各种line无法进入contain中)
	eve.on("custom.connectionHandler_rect",function (triggerObj,connectPointObj,calPointObj){
		var connectPoint_x = connectPointObj.attr("cx");
		var connectPoint_y = connectPointObj.attr("cy");
		var minRectX = leftTop_c.attr("cx");
		var minRectY = leftTop_c.attr("cy");
		var maxRectX = rightBottom_c.attr("cx");
		var maxRectY = rightBottom_c.attr("cy");
		
		if((Math.abs(connectPoint_x-minRectX)<=10 && connectPoint_y>=minRectY && connectPoint_y<= maxRectY) 
		|| (Math.abs(connectPoint_x-maxRectX)<=10 && connectPoint_y>=minRectY && connectPoint_y<= maxRectY) 
		|| (Math.abs(connectPoint_y-maxRectY)<=10 && connectPoint_x>=minRectX && connectPoint_x<= maxRectX)
		|| (Math.abs(connectPoint_y-minRectY)<=10 && connectPoint_x>=minRectX && connectPoint_x<= maxRectX)){
			//连接
			var isConnected = false;
			var connectedIndex = -1;
			for(var i=0;i<rcArray.length;i++){
				if(rcArray[i]!=null){
					if(rcArray[i][0]==triggerObj && rcArray[i][1]==connectPointObj){
						connectedIndex = i;
						isConnected=true;
						break;
					}
				}
			}
			if(!isConnected){
				var minIndex = getMinDistance(selfConnectPoints,calPointObj.attr("cx"),calPointObj.attr("cy"));
				var scPoint = selfConnectPoints[minIndex];
				rcArray[minIndex]=[triggerObj,connectPointObj];
				triggerObj.point_move_connect(connectPointObj,scPoint.attr("cx"),scPoint.attr("cy"),selfObj);
				conMarks[minIndex].show();
			}else{
				var scPoint = selfConnectPoints[connectedIndex];
				triggerObj.point_move_connect(connectPointObj,scPoint.attr("cx"),scPoint.attr("cy"),selfObj);			
			}
		}else{
			for(var i=0;i<rcArray.length;i++){
				if(rcArray[i]!=null){
					if(rcArray[i][0]==triggerObj && rcArray[i][1]==connectPointObj){
						rcArray[i]=null;
						conMarks[i].hide();
						break;
					}
				}
			}
			if(triggerObj.point_move_remove_connect){
				triggerObj.point_move_remove_connect(connectPointObj,selfObj);
			}
		}
	});
	
	var rectMove_rcArrayAdjust = function(){
		for(var i=0;i<rcArray.length;i++){
			if(rcArray[i]!=null){
				if(rcArray[i][0].point_move_connect){
					if(i==0){
						rcArray[i][0].point_move_connect(rcArray[i][1],centerTop_c.attr("cx"),centerTop_c.attr("cy"),selfObj,true);
					}else if(i==1){
						rcArray[i][0].point_move_connect(rcArray[i][1],leftMiddle_c.attr("cx"),leftMiddle_c.attr("cy"),selfObj,true);
					}else if(i==2){
						rcArray[i][0].point_move_connect(rcArray[i][1],centerBottom_c.attr("cx"),centerBottom_c.attr("cy"),selfObj,true);
					}else if(i==3){
						rcArray[i][0].point_move_connect(rcArray[i][1],rightMiddle_c.attr("cx"),rightMiddle_c.attr("cy"),selfObj,true);
					}
				}
			}
		}
	}
	
	var rectMove_conMarksAdjust = function(){
		for(var i=0;i<conMarks.length;i++){
			if(i==0){
				conMarks[i].attr({x:centerTop_c.attr("cx")-sideLength/2,y:centerTop_c.attr("cy")-sideLength/2});
			}else if(i==1){
				conMarks[i].attr({x:leftMiddle_c.attr("cx")-sideLength/2,y:leftMiddle_c.attr("cy")-sideLength/2});
			}else if(i==2){
				conMarks[i].attr({x:centerBottom_c.attr("cx")-sideLength/2,y:centerBottom_c.attr("cy")-sideLength/2});
			}else if(i==3){
				conMarks[i].attr({x:rightMiddle_c.attr("cx")-sideLength/2,y:rightMiddle_c.attr("cy")-sideLength/2});
			}
		}
	}
	
	this.getId = function (){
		return defaultId;
	}
	
	this.getX = function(){
		return real_pic.attr("x");
	}
	
	this.getY = function(){
		return real_pic.attr("y");
	}
	
	this.getWidth = function(){
		return real_pic.attr("width");
	}
	
	this.getHeight = function(){
		return real_pic.attr("height");
	}
	this.getReal_pic = function (){
		return real_pic;
	}
	this.getLeftTop_x = function(){
		return leftTop_c.attr("cx");
	}
	this.getLeftTop_y = function(){
		return leftTop_c.attr("cy");
	}
	this.getRightBottom_x = function(){
		return rightBottom_c.attr("cx");
	}
	this.getRightBottom_y = function(){
		return rightBottom_c.attr("cy");
	}
	this.getInnerObjs = function (){
		return innerObjs;
	}
	this.getDAStr = function (){
		return dAStr;
	}
	this.getTitle = function (){
		return title;
	}
	
	return this;
}

/**带箭头的直线
 * @argument 从id开始就都是load时候用的参数
 * */
function diySimpleLineWithArrow(paper,initX,initY,lineLength,id,start_x,start_y,end_x,end_y){
	//2个元素，0:父,1:子
	/**存放父子元素的集合*/
	this.relationArray = [null,null];
	var defaultId;
	if(id){
		defaultId = id;
	}else{
		defaultId = "diy_simpleLineWithArrow_"+new Date().getTime();
	}
	this.type = "d_sla_line";
	this.pageType="simpleLine";
	var selfObj = this;
	
	var simpleLineWithArrowPrevent;
	var simpleLineWithArrow;
	var simpleLineWithArrowAdjust;
	if(id){
		simpleLineWithArrowPrevent = paper.path(["M",start_x,start_y,"L",end_x,end_y].join(","));
		simpleLineWithArrow = paper.path(["M",start_x,start_y,"L",end_x,end_y].join(","));
		simpleLineWithArrowAdjust = paper.path(["M",start_x,start_y,"L",end_x,end_y].join(","));
	}else{
		simpleLineWithArrowPrevent = paper.path(["M",initX,initY,"L",initX-0+lineLength,initY].join(","));
		simpleLineWithArrow = paper.path(["M",initX,initY,"L",initX-0+lineLength,initY].join(","));
		simpleLineWithArrowAdjust = paper.path(["M",initX,initY,"L",initX-0+lineLength,initY].join(","));
	}
	
	simpleLineWithArrowPrevent.attr({"opacity": .1,stroke: "white","stroke-width":"9","fill":"none","cursor": "move"});
	simpleLineWithArrow.attr({"stroke-width":"1","shape-rendering":"crispEdges","fill":"none"});
	simpleLineWithArrowAdjust.attr({stroke: "#99ccff","stroke-width":"1","shape-rendering":"crispEdges","stroke-dasharray": "--"});
	
	var initAngle = 180;
	/**画箭头*/
	var reDraw  = function(angle,destX,destY){
   		if(angle!=null){
       		arrow.lastAngle = angle;
   		}else{
   			angle = arrow.lastAngle;
   		}
   		var arrowPath = ["M",destX+15*Math.cos(aTorR(165+angle)),destY+15*Math.sin(aTorR(165+angle)),"L",destX+15*Math.cos(aTorR(195+angle)),destY-0+15*Math.sin(aTorR(195+angle)),"L",destX,destY,"Z"].join(",");
   		arrow.attr({path:arrowPath});
   }
   
	var arrow = paper.path("");
	if(id){
		var currentAngle = Raphael.angle(start_x,start_y,end_x,end_y);
		var rotateAngle = initAngle - currentAngle;
		reDraw(0-rotateAngle,end_x,end_y);
	}else{
		reDraw(0,initX-0+lineLength,initY);
	}
	arrow.attr({fill:"#000000",stroke: "#000000","stroke-width":"1","shape-rendering":"crispEdges"});
	//初始直线(箭头)角度
//	var initAngle = Raphael.angle(initX,initY, initX-0+lineLength,initY);
	
	var ponint_r = 5;
	var left_c,right_c;
	var isAdjusting = false;
	
	/**是否自己被ProcedureContain包含*/
	this.isProcedureContained = false;
	
	this.eveConnectProcedureContain = function (){
		var minX = left_c.attr("cx");
		var maxX = right_c.attr("cx");
		if(minX>maxX){
			minX = right_c.attr("cx");
			maxX = left_c.attr("cx");
		}
		var minY = left_c.attr("cy");
		var maxY = right_c.attr("cy");
		if(minX>maxY){
			minY = right_c.attr("cy");
			maxY = left_c.attr("cy");
		}
		eve("custom.connectProcedureContain",'',this,defaultId,minX,maxX,minY,maxY);
	}
	
	var connectProcedureContainTriggerFunction  = function (){
		var minX = left_c.attr("cx");
		var maxX = right_c.attr("cx");
		if(minX>maxX){
			minX = right_c.attr("cx");
			maxX = left_c.attr("cx");
		}
		var minY = left_c.attr("cy");
		var maxY = right_c.attr("cy");
		if(minX>maxY){
			minY = right_c.attr("cy");
			maxY = left_c.attr("cy");
		}
		eve("custom.connectProcedureContain",'',selfObj,defaultId,minX,maxX,minY,maxY);
	}
	
	/**注册事件(触发各个组件自己再去触发ProcedureContain的关系判断事件)*/
	eve.on("custom.connectProcedureContainTrigger",connectProcedureContainTriggerFunction);
	
	this.linkageByProcedureContainRect = function (dx, dy) {
		var olx = left_c.attr("cx");
        var oly = left_c.attr("cy");
        var orx = right_c.attr("cx");
        var ory = right_c.attr("cy");
    	var newPath=["M",olx-0+dx,oly-0+dy,"L",orx-0+dx,ory-0+dy].join(",");
    	simpleLineWithArrowPrevent.attr({path:newPath});
    	simpleLineWithArrowAdjust.attr({path:newPath});
    	simpleLineWithArrow.attr({path:newPath});
    	left_c.attr({cx:olx-0+dx,cy:oly-0+dy});
    	right_c.attr({cx:orx-0+dx,cy:ory-0+dy});
    	reDraw(null,orx-0+dx,ory-0+dy);
        paper.safari();
	}
	
	 var line_dragger = function () {
        this.olx = left_c.attr("cx");
        this.oly = left_c.attr("cy");
        this.orx = right_c.attr("cx");
        this.ory = right_c.attr("cy");
//        this.animate({"fill-opacity": .3}, 500);
    },/**onmove*/
        line_move = function (dx, dy) {
//        	for(var i=0;i<selfObj.relationArray.length;i++){
//				if(selfObj.relationArray[i]!=null){
//					selfObj.relationArray[i].otherConDetelte(selfObj);
//				}
//			}
        	var newPath=["M",this.olx-0+dx,this.oly-0+dy,"L",this.orx-0+dx,this.ory-0+dy].join(",");
        	this.attr({path:newPath});
        	simpleLineWithArrowAdjust.attr({path:newPath});
        	simpleLineWithArrow.attr({path:newPath});
        	left_c.attr({cx:this.olx-0+dx,cy:this.oly-0+dy});
        	right_c.attr({cx:this.orx-0+dx,cy:this.ory-0+dy});
        	reDraw(null,this.orx-0+dx,this.ory-0+dy);
        	
        	for(var i=0;i<selfObj.relationArray.length;i++){
				if(selfObj.relationArray[i]!=null){
					if(i==0)
						selfObj.relationArray[i].rect_move_connect_dist(left_c.attr("cx"),left_c.attr("cy"),selfObj);
					else
						selfObj.relationArray[i].rect_move_connect_dist(right_c.attr("cx"),right_c.attr("cy"),selfObj);
				}
			}
			
            paper.safari();
        },/**onend*/
        line_up = function () {
        	//触发是否在ProcedureContain内的事件(关联/解除之前的关系)
			selfObj.eveConnectProcedureContain();
			eve("custom.someoneAdjusting",'',simpleLineWithArrow);
			for(var i=0;i<selfObj.relationArray.length;i++){
				if(selfObj.relationArray[i]!=null){
					selfObj.relationArray[i].rect_move_connect_mouseUp();
				}
			}
//            this.animate({"fill-opacity": 1}, 500);
        };
	simpleLineWithArrowPrevent.drag(line_move, line_dragger,line_up);//onmove, onstart, onend
	
	/**
	 * 移动直线到最终点
	 * @argument distX 最终的x
	 * @argument distY 最终的y
	 * */
	this.point_move_connect = function (point, distX, distY,rectObj,notNeedConnection){
		if(point.data("id")==defaultId+"left_c"){
			var newPath=["M",distX,distY,"L",right_c.attr("cx"),right_c.attr("cy")].join(",");
			simpleLineWithArrowAdjust.attr({path:newPath});
    		simpleLineWithArrow.attr({path:newPath});
    		simpleLineWithArrowPrevent.attr({path:newPath});
    		point.attr({cx:distX,cy:distY});
    		
    		var currentAngle = Raphael.angle(distX,distY,right_c.attr("cx"),right_c.attr("cy"));
    		var rotateAngle = initAngle - currentAngle;
    		reDraw(0-rotateAngle,right_c.attr("cx"),right_c.attr("cy"));
    		if(!notNeedConnection){
    			this.addRelationParent(rectObj);
    		}
		}else if(point.data("id")==defaultId+"right_c"){
			var newPath=["M",left_c.attr("cx"),left_c.attr("cy"),"L",distX,distY].join(",");
			simpleLineWithArrowAdjust.attr({path:newPath});
    		simpleLineWithArrow.attr({path:newPath});
    		simpleLineWithArrowPrevent.attr({path:newPath});
    		point.attr({cx:distX,cy:distY});
    		
    		var currentAngle = Raphael.angle(left_c.attr("cx"),left_c.attr("cy"),distX,distY);
    		var rotateAngle = initAngle - currentAngle;
    		reDraw(0-rotateAngle,distX,distY);
    		if(!notNeedConnection){
	    		this.addRelationChild(rectObj);
    		}
		}
		this.eveConnectProcedureContain();
	}
	
	this.addRelationParent = function(obj){
		this.relationArray[0] = obj;
	}
	
	this.addRelationChild = function(obj){
		this.relationArray[1] = obj;
	}
	
	this.point_move_remove_connect =function (point,obj){
		if(point!=null){
			if(point.data("id")==defaultId+"left_c"){
				this.removeRelationParent(obj);
			}else if(point.data("id")==defaultId+"right_c"){
				this.removeRelationChild(obj);
			}
		}else{
			this.removeRelationParent(obj);		
			this.removeRelationChild(obj);
		}
	}
	
	this.removeRelationParent = function(obj){
		if(this.relationArray[0] == obj){
			this.relationArray[0] = null;
		}
	}
	
	this.removeRelationChild = function(obj){
		if(this.relationArray[1] == obj){
			this.relationArray[1] = null;
		}
	}
	
	/**调整定位点的拖拉事件处理*/
	var point_dragger = function () {
        this.ox = this.attr("cx");
        this.oy = this.attr("cy");
        this.animate({"fill-opacity": .3}, 500);
    },/**onmove*/
        point_move = function (dx, dy) {
        	if(this.data("id")==defaultId+"left_c"){
        		var newPath=["M",this.ox-0+dx,this.oy-0+dy,"L",right_c.attr("cx"),right_c.attr("cy")].join(",");
        		simpleLineWithArrowAdjust.attr({path:newPath});
        		simpleLineWithArrow.attr({path:newPath});
        		simpleLineWithArrowPrevent.attr({path:newPath});
        		left_c.attr({cx:this.ox+dx,cy:this.oy+dy});
        		
        		var currentAngle = Raphael.angle(this.ox-0+dx,this.oy-0+dy,right_c.attr("cx"),right_c.attr("cy"));
        		var rotateAngle = initAngle - currentAngle;
        		reDraw(0-rotateAngle,right_c.attr("cx"),right_c.attr("cy"));
        		
        	}else if(this.data("id")==defaultId+"right_c"){
        		var newPath=["M",left_c.attr("cx"),left_c.attr("cy"),"L",this.ox-0+dx,this.oy-0+dy].join(",");
        		simpleLineWithArrowAdjust.attr({path:newPath});
        		simpleLineWithArrow.attr({path:newPath});
        		simpleLineWithArrowPrevent.attr({path:newPath});
        		right_c.attr({cx:this.ox+dx,cy:this.oy+dy});
        		
        		var currentAngle = Raphael.angle(left_c.attr("cx"),left_c.attr("cy"),this.ox-0+dx,this.oy-0+dy);
        		var rotateAngle = initAngle - currentAngle;
        		reDraw(0-rotateAngle,this.ox-0+dx,this.oy-0+dy);
        	}
            paper.safari();
        },/**onend*/
        point_up = function () {
        	if(this.data("id")==defaultId+"left_c"){
	        	eve("custom.connectionHandler_rect",'',selfObj,this,right_c);//触发事件
        	}else{
        		eve("custom.connectionHandler_rect",'',selfObj,this,left_c);//触发事件
        	}
        	selfObj.eveConnectProcedureContain();
        	eve("custom.someoneAdjusting",'',simpleLineWithArrow);
            this.animate({"fill-opacity": .5}, 500);
        };
        
	/**生成2个端点的调整点*/
	var draw2AjustPoint = function (paper,x,y,l,r, point_dragger, point_move, point_up){
		left_c = paper.circle(x,y,r).data("id",defaultId+"left_c");
		left_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "move"});
		left_c.drag(point_move, point_dragger, point_up);
		right_c = paper.circle(x-0+l,y,r).data("id",defaultId+"right_c");
		right_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "move"});
		right_c.drag(point_move, point_dragger, point_up);
	}
	var draw2AjustPointLoad = function (paper,start_x,start_y,end_x,end_y,l,r, point_dragger, point_move, point_up){
		left_c = paper.circle(start_x,start_y,r).data("id",defaultId+"left_c");
		left_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "move"});
		left_c.drag(point_move, point_dragger, point_up);
		right_c = paper.circle(end_x,end_y,r).data("id",defaultId+"right_c");
		right_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "move"});
		right_c.drag(point_move, point_dragger, point_up);
	}
	
	var hideAdjustLine = function(){
		simpleLineWithArrowAdjust.hide();
		left_c.hide();
		right_c.hide();
		isAdjusting = false;
	}
	var showAdjustLine = function(){
		simpleLineWithArrowAdjust.show();
		left_c.show();
		right_c.show();
		isAdjusting=true;
	}
	if(id){
		draw2AjustPointLoad(paper,start_x,start_y,end_x,end_y,lineLength,ponint_r, point_dragger, point_move, point_up);
	}else{
		draw2AjustPoint(paper,initX,initY,lineLength,ponint_r, point_dragger, point_move, point_up);
	}
	hideAdjustLine();
	simpleLineWithArrowPrevent.click(function (){
		eve("custom.someoneAdjusting",'',simpleLineWithArrow);//触发事件
	});
	
	var someoneAdjustingFunction = function (triggerEle){
		if(triggerEle == simpleLineWithArrow){
			//是自己的元素触发的
			if(!isAdjusting){
				showAdjustLine();
			}
			eve.stop();
		}else{
			//是其他的元素触发的
			if(isAdjusting){
				hideAdjustLine();
			}
		}
	}
	//注册事件
	eve.on("custom.someoneAdjusting",someoneAdjustingFunction);
	
	var someoneDeleteFunction = function(){
		if(isAdjusting){
			for(var i=0;i<selfObj.relationArray.length;i++){
				if(selfObj.relationArray[i]!=null){
					selfObj.relationArray[i].otherConDetelte(selfObj);
				}
			}
			
			simpleLineWithArrowPrevent.remove();
			simpleLineWithArrow.remove();
			simpleLineWithArrowAdjust.remove();
			left_c.remove();
			right_c.remove();
			arrow.remove();
			removeLine(selfObj);
			removeComponentObjMap(defaultId);
			eve.off("custom.someoneAdjusting",someoneAdjustingFunction);
			eve.off("custom.connectProcedureContainTrigger", connectProcedureContainTriggerFunction);
			eve.off("custom.someoneDelete", someoneDeleteFunction);
			selfObj = null;
		}		
	}
	
	//注册删除元素事件
	eve.on("custom.someoneDelete",someoneDeleteFunction);
	
	this.getId = function (){
		return defaultId;
	}
	this.getStartX = function (){
		return left_c.attr("cx");
	}
	this.getStartY = function (){
		return left_c.attr("cy");
	}
	this.getEndX = function (){
		return right_c.attr("cx");
	}
	this.getEndY = function (){
		return right_c.attr("cy");
	}
	this.getLeft_c = function(){
		return left_c;
	}
	this.getRight_c = function(){
		return right_c;
	}
	
	return this;
}

/**带箭头的复杂线段(左右)
 * @argument 从id开始就都是load时候用的参数
 * */
function diyComplexLineLR(paper,initX,initY,lineLength,id,start_x,start_y,mid_x,mid_y,end_x,end_y){
	var defaultId;
	if(id){
		defaultId = id;
	}else{
		defaultId = "diy_complexLineLR_"+new Date().getTime();
	}
	this.type = "d_clr_line";
	this.pageType="complexLRLine";
	//2个元素，0:父,1:子
	this.relationArray = [null,null];
	var selfObj = this;
	var ponint_r = 5;
	var isAdjusting = false;
	var left_c,right_c,middle_c;
	
	var initPath;
	if(id){
		initPath = ["M",start_x,start_y,"L",mid_x,start_y,"L",mid_x,end_y,"L",end_x,end_y].join(",");
	}else{
		initPath = ["M",initX,initY,"L",initX-0+lineLength/2,initY,"L",initX-0+lineLength/2,initY-lineLength,"L",initX-0+lineLength,initY-lineLength].join(",");
	}
	
	var complexLineLRPrevent = paper.path(initPath);
	complexLineLRPrevent.attr({"opacity": .1,stroke: "white","stroke-width":"9","fill":"none","cursor": "move"});
	var complexLineLR = paper.path(initPath);
	complexLineLR.attr({"stroke-width":"1","shape-rendering":"crispEdges","fill":"none"});
	var complexLineLRAdjust = paper.path(initPath);
	complexLineLRAdjust.attr({stroke: "#99ccff","stroke-width":"1","shape-rendering":"crispEdges","stroke-dasharray": "--"});
	var arrow;
	if(id){
		arrow = paper.path(["M",end_x-20,end_y-6,"L",end_x-20,end_y-0+6,"L",end_x,end_y].join(","));
	}else{
		arrow = paper.path(["M",initX-0+lineLength-20,initY-lineLength-6,"L",initX-0+lineLength-20,initY-lineLength+6,"L",initX-0+lineLength,initY-lineLength].join(","));
	}
	arrow.attr({fill:"#000000",stroke: "#000000","stroke-width":"1","shape-rendering":"crispEdges"});
	
	/**是否自己被ProcedureContain包含*/
	this.isProcedureContained = false;
	
	this.eveConnectProcedureContain = function (){
		var minX = left_c.attr("cx");
		var maxX = right_c.attr("cx");
		if(minX>maxX){
			minX = right_c.attr("cx");
			maxX = left_c.attr("cx");
		}
		var minY = left_c.attr("cy");
		var maxY = right_c.attr("cy");
		if(minX>maxY){
			minY = right_c.attr("cy");
			maxY = left_c.attr("cy");
		}
		eve("custom.connectProcedureContain",'',this,defaultId,minX,maxX,minY,maxY);
	}
	
	var connectProcedureContainTriggerFunction  = function (){
		var minX = left_c.attr("cx");
		var maxX = right_c.attr("cx");
		if(minX>maxX){
			minX = right_c.attr("cx");
			maxX = left_c.attr("cx");
		}
		var minY = left_c.attr("cy");
		var maxY = right_c.attr("cy");
		if(minX>maxY){
			minY = right_c.attr("cy");
			maxY = left_c.attr("cy");
		}
		eve("custom.connectProcedureContain",'',selfObj,defaultId,minX,maxX,minY,maxY);
	}
	
	/**注册事件(触发各个组件自己再去触发ProcedureContain的关系判断事件)*/
	eve.on("custom.connectProcedureContainTrigger",connectProcedureContainTriggerFunction);
	
	this.linkageByProcedureContainRect = function (dx, dy) {
		var olx = left_c.attr("cx");
        var oly = left_c.attr("cy");
        var orx = right_c.attr("cx");
        var ory = right_c.attr("cy");
        var omx = middle_c.attr("cx");
        var omy = middle_c.attr("cy");
    	var newPath=["M",olx-0+dx,oly-0+dy,"L",omx-0+dx,oly-0+dy,"L",omx-0+dx,ory-0+dy,"L",orx-0+dx,ory-0+dy].join(",");
    	complexLineLRPrevent.attr({path:newPath});
    	complexLineLRAdjust.attr({path:newPath});
    	complexLineLR.attr({path:newPath});
    	left_c.attr({cx:olx-0+dx,cy:oly-0+dy});
    	right_c.attr({cx:orx-0+dx,cy:ory-0+dy});
    	middle_c.attr({cx:omx-0+dx,cy:omy-0+dy});
    	if(arrow.diy_direction !=null && arrow.diy_direction == "l"){
        	var newArrowPath = ["M",orx-0+dx+20,ory-0+dy-6,"L",orx-0+dx+20,ory-0+dy+6,"L",orx-0+dx,ory-0+dy].join(",");
			arrow.attr({path:newArrowPath});
    	}else{
    		var newArrowPath = ["M",orx-0+dx-20,ory-0+dy-6,"L",orx-0+dx-20,ory-0+dy+6,"L",orx-0+dx,ory-0+dy].join(",");
			arrow.attr({path:newArrowPath});
    	}
        paper.safari();
	}
	
	 var line_dragger = function () {
        this.olx = left_c.attr("cx");
        this.oly = left_c.attr("cy");
        this.orx = right_c.attr("cx");
        this.ory = right_c.attr("cy");
        this.omx = middle_c.attr("cx");
        this.omy = middle_c.attr("cy");
//        this.animate({"fill-opacity": .3}, 500);
    },/**onmove*/
        line_move = function (dx, dy) {
//        	for(var i=0;i<selfObj.relationArray.length;i++){
//				if(selfObj.relationArray[i]!=null){
//					selfObj.relationArray[i].otherConDetelte(selfObj);
//				}
//			}
        	var newPath=["M",this.olx-0+dx,this.oly-0+dy,"L",this.omx-0+dx,this.oly-0+dy,"L",this.omx-0+dx,this.ory-0+dy,"L",this.orx-0+dx,this.ory-0+dy].join(",");
        	this.attr({path:newPath});
        	complexLineLRAdjust.attr({path:newPath});
        	complexLineLR.attr({path:newPath});
        	left_c.attr({cx:this.olx-0+dx,cy:this.oly-0+dy});
        	right_c.attr({cx:this.orx-0+dx,cy:this.ory-0+dy});
        	middle_c.attr({cx:this.omx-0+dx,cy:this.omy-0+dy});
        	if(arrow.diy_direction !=null && arrow.diy_direction == "l"){
	        	var newArrowPath = ["M",this.orx-0+dx+20,this.ory-0+dy-6,"L",this.orx-0+dx+20,this.ory-0+dy+6,"L",this.orx-0+dx,this.ory-0+dy].join(",");
				arrow.attr({path:newArrowPath});
        	}else{
        		var newArrowPath = ["M",this.orx-0+dx-20,this.ory-0+dy-6,"L",this.orx-0+dx-20,this.ory-0+dy+6,"L",this.orx-0+dx,this.ory-0+dy].join(",");
				arrow.attr({path:newArrowPath});
        	}
        	for(var i=0;i<selfObj.relationArray.length;i++){
				if(selfObj.relationArray[i]!=null){
					if(i==0)
						selfObj.relationArray[i].rect_move_connect_dist(left_c.attr("cx"),left_c.attr("cy"),selfObj);
					else
						selfObj.relationArray[i].rect_move_connect_dist(right_c.attr("cx"),right_c.attr("cy"),selfObj);
				}
			}
            paper.safari();
        },/**onend*/
        line_up = function () {
        	selfObj.eveConnectProcedureContain();
        	eve("custom.someoneAdjusting",'',complexLineLR);
        	for(var i=0;i<selfObj.relationArray.length;i++){
				if(selfObj.relationArray[i]!=null){
					selfObj.relationArray[i].rect_move_connect_mouseUp();
				}
			}
//            this.animate({"fill-opacity": 1}, 500);
        };
	complexLineLRPrevent.drag(line_move, line_dragger,line_up);//onmove, onstart, onend
	
	/**
	 * @argument distX 最终的x
	 * @argument distY 最终的y
	 * */
	this.point_move_connect = function (point, distX, distY,rectObj,notNeedConnection){
		if(point.data("id")==defaultId+"left_c"){
    		var newPath=["M",distX, distY,"L",middle_c.attr("cx"),distY,"L",middle_c.attr("cx"),right_c.attr("cy"),"L",right_c.attr("cx"),right_c.attr("cy")].join(",");
    		complexLineLRAdjust.attr({path:newPath});
    		complexLineLR.attr({path:newPath});
    		complexLineLRPrevent.attr({path:newPath});
    		left_c.attr({cx:distX,cy:distY});
    		if(right_c.attr("cy")>=distY){
	    		middle_c.attr({cy:(right_c.attr("cy")-distY)/2+distY});
    		}else{
    			middle_c.attr({cy:(distY-right_c.attr("cy"))/2+right_c.attr("cy")});
    		}
    		if(!notNeedConnection){
    			this.addRelationParent(rectObj);
    		}
		}else if(point.data("id")==defaultId+"right_c"){
    		var newPath=["M",left_c.attr("cx"),left_c.attr("cy"),"L",middle_c.attr("cx"),left_c.attr("cy"),"L",middle_c.attr("cx"),distY,"L",distX,distY].join(",");
    		complexLineLRAdjust.attr({path:newPath});
    		complexLineLR.attr({path:newPath});
    		complexLineLRPrevent.attr({path:newPath});
    		right_c.attr({cx:distX,cy:distY});
    		if(left_c.attr("cy")>=distY){
	    		middle_c.attr({cy:(left_c.attr("cy")-distY)/2+distY});
    		}else{
    			middle_c.attr({cy:(distY-left_c.attr("cy"))/2+left_c.attr("cy")});
    		}
    		
			if(distX >= middle_c.attr("cx")){
        		var newArrowPath = ["M",distX-20,distY-6,"L",distX-20,distY+6,"L",distX,distY].join(",");
				arrow.attr({path:newArrowPath});
				arrow.diy_direction = "r";
    		}else{
    			var newArrowPath = ["M",distX+20,distY-6,"L",distX+20,distY+6,"L",distX,distY].join(",");
				arrow.attr({path:newArrowPath});
				arrow.diy_direction = "l";
    		}
    		if(!notNeedConnection){
	    		this.addRelationChild(rectObj);
    		}
    	}
    	this.eveConnectProcedureContain();
	}
	
	this.addRelationParent = function(obj){
		this.relationArray[0] = obj;
	}
	
	this.addRelationChild = function(obj){
		this.relationArray[1] = obj;
	}
	
	this.point_move_remove_connect =function (point,obj){
		if(point!=null){
			if(point.data("id")==defaultId+"left_c"){
				this.removeRelationParent(obj);
			}else if(point.data("id")==defaultId+"right_c"){
				this.removeRelationChild(obj);
			}
		}else{
			this.removeRelationParent(obj);		
			this.removeRelationChild(obj);
		}
	}
	
	this.removeRelationParent = function(obj){
		if(this.relationArray[0] == obj){
			this.relationArray[0] = null;
		}
	}
	
	this.removeRelationChild = function(obj){
		if(this.relationArray[1] == obj){
			this.relationArray[1] = null;
		}
	}
	
	/**调整定位点的拖拉事件处理*/
	var point_dragger = function () {
        this.ox = this.attr("cx");
        this.oy = this.attr("cy");
        this.omy = middle_c.attr("cy");
        this.animate({"fill-opacity": .3}, 500);
    },/**onmove*/
        point_move = function (dx, dy) {
        	if(this.data("id")==defaultId+"left_c"){
        		var newPath=["M",this.ox-0+dx,this.oy-0+dy,"L",middle_c.attr("cx"),this.oy-0+dy,"L",middle_c.attr("cx"),right_c.attr("cy"),"L",right_c.attr("cx"),right_c.attr("cy")].join(",");
        		complexLineLRAdjust.attr({path:newPath});
        		complexLineLR.attr({path:newPath});
        		complexLineLRPrevent.attr({path:newPath});
        		left_c.attr({cx:this.ox+dx,cy:this.oy+dy});
        		middle_c.attr({cy:this.omy-0+dy/2});
        		
        	}else if(this.data("id")==defaultId+"middle_c"){
        		var newPath=["M",left_c.attr("cx"),left_c.attr("cy"),"L",this.ox-0+dx,left_c.attr("cy"),"L",this.ox-0+dx,right_c.attr("cy"),"L",right_c.attr("cx"),right_c.attr("cy")].join(",");
        		complexLineLRAdjust.attr({path:newPath});
        		complexLineLR.attr({path:newPath});
        		complexLineLRPrevent.attr({path:newPath});
        		middle_c.attr({cx:this.ox+dx});
        		
        		if(right_c.attr("cx") >= this.ox-0+dx){
	        		var newArrowPath = ["M",right_c.attr("cx")-20,right_c.attr("cy")-6,"L",right_c.attr("cx")-20,right_c.attr("cy")+6,"L",right_c.attr("cx"),right_c.attr("cy")].join(",");
					arrow.attr({path:newArrowPath});
					arrow.diy_direction = "r";
        		}else{
        			var newArrowPath = ["M",right_c.attr("cx")+20,right_c.attr("cy")-6,"L",right_c.attr("cx")+20,right_c.attr("cy")+6,"L",right_c.attr("cx"),right_c.attr("cy")].join(",");
					arrow.attr({path:newArrowPath});
					arrow.diy_direction = "l";
        		}
        		
        	}else if(this.data("id")==defaultId+"right_c"){
        		var newPath=["M",left_c.attr("cx"),left_c.attr("cy"),"L",middle_c.attr("cx"),left_c.attr("cy"),"L",middle_c.attr("cx"),this.oy-0+dy,"L",this.ox-0+dx,this.oy-0+dy].join(",");
        		complexLineLRAdjust.attr({path:newPath});
        		complexLineLR.attr({path:newPath});
        		complexLineLRPrevent.attr({path:newPath});
        		right_c.attr({cx:this.ox+dx,cy:this.oy+dy});
        		middle_c.attr({cy:this.omy-0+dy/2});
        		
				if(this.ox-0+dx >= middle_c.attr("cx")){
	        		var newArrowPath = ["M",this.ox-0+dx-20,this.oy-0+dy-6,"L",this.ox-0+dx-20,this.oy-0+dy+6,"L",this.ox-0+dx,this.oy-0+dy].join(",");
					arrow.attr({path:newArrowPath});
					arrow.diy_direction = "r";
        		}else{
        			var newArrowPath = ["M",this.ox-0+dx+20,this.oy-0+dy-6,"L",this.ox-0+dx+20,this.oy-0+dy+6,"L",this.ox-0+dx,this.oy-0+dy].join(",");
					arrow.attr({path:newArrowPath});
					arrow.diy_direction = "l";
        		}
        	}
        	
        	
            paper.safari();
        },/**onend*/
        point_up = function () {
        	if(this.data("id")==defaultId+"left_c"){
	        	eve("custom.connectionHandler_rect",'',selfObj,this,right_c);//触发事件
        	}else{
	        	eve("custom.connectionHandler_rect",'',selfObj,this,left_c);//触发事件
        	}
        	selfObj.eveConnectProcedureContain();
        	eve("custom.someoneAdjusting",'',complexLineLR);
            this.animate({"fill-opacity": .5}, 500);
        };
	
	/**生成3个调整点*/
	var draw3AjustPoint = function (paper,x,y,l,r, point_dragger, point_move, point_up){
		left_c = paper.circle(x,y,r).data("id",defaultId+"left_c");
		left_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "move"});
		left_c.drag(point_move, point_dragger, point_up);
		middle_c = paper.circle(x-0+l/2,y-l/2,r).data("id",defaultId+"middle_c");
		middle_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "e-resize"});
		middle_c.drag(point_move, point_dragger, point_up);
		right_c = paper.circle(x-0+l,y-0-l,r).data("id",defaultId+"right_c");
		right_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "move"});
		right_c.drag(point_move, point_dragger, point_up);
	}
	
	var draw3AjustPointLoad = function (paper,start_x,start_y,mid_x,mid_y,end_x,end_y,r, point_dragger, point_move, point_up){
		left_c = paper.circle(start_x,start_y,r).data("id",defaultId+"left_c");
		left_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "move"});
		left_c.drag(point_move, point_dragger, point_up);
		middle_c = paper.circle(mid_x,mid_y,r).data("id",defaultId+"middle_c");
		middle_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "e-resize"});
		middle_c.drag(point_move, point_dragger, point_up);
		right_c = paper.circle(end_x,end_y,r).data("id",defaultId+"right_c");
		right_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "move"});
		right_c.drag(point_move, point_dragger, point_up);
	}
	
	var hideAdjustLine = function(){
		complexLineLRAdjust.hide();
		left_c.hide();
		right_c.hide();
		middle_c.hide();
		isAdjusting = false;
	}
	var showAdjustLine = function(){
		complexLineLRAdjust.show();
		left_c.show();
		right_c.show();
		middle_c.show();
		isAdjusting=true;
	}
	if(id){
		draw3AjustPointLoad(paper,start_x,start_y,mid_x,mid_y,end_x,end_y,ponint_r, point_dragger, point_move, point_up);
	}else{
		draw3AjustPoint(paper,initX,initY,lineLength,ponint_r, point_dragger, point_move, point_up);
	}
	
	hideAdjustLine();
	complexLineLRPrevent.click(function (){
		eve("custom.someoneAdjusting",'',complexLineLR);//触发事件
	});
	
	var someoneAdjustingFunction = function (triggerEle){
		if(triggerEle == complexLineLR){
			//是自己的元素触发的
			if(!isAdjusting){
				showAdjustLine();
			}
			eve.stop();
		}else{
			//是其他的元素触发的
			if(isAdjusting){
				hideAdjustLine();
			}
		}
	}
	//注册事件
	eve.on("custom.someoneAdjusting",someoneAdjustingFunction);
	
	var someoneDeleteFunction = function(){
		if(isAdjusting){
			for(var i=0;i<selfObj.relationArray.length;i++){
				if(selfObj.relationArray[i]!=null){
					selfObj.relationArray[i].otherConDetelte(selfObj);
				}
			}
			complexLineLRPrevent.remove();
			complexLineLR.remove();
			complexLineLRAdjust.remove();
			left_c.remove();
			right_c.remove();
			arrow.remove();
			middle_c.remove();
			removeLine(selfObj);
			removeComponentObjMap(defaultId);
			eve.off("custom.someoneAdjusting",someoneAdjustingFunction);
			eve.off("custom.connectProcedureContainTrigger", connectProcedureContainTriggerFunction);
			eve.off("custom.someoneDelete", someoneDeleteFunction);
			selfObj = null;
		}		
	}
	
	//注册删除元素事件
	eve.on("custom.someoneDelete",someoneDeleteFunction);
	
	this.getId = function (){
		return defaultId;
	}
	this.getStartX = function (){
		return left_c.attr("cx");
	}
	this.getStartY = function (){
		return left_c.attr("cy");
	}
	this.getEndX = function (){
		return right_c.attr("cx");
	}
	this.getEndY = function (){
		return right_c.attr("cy");
	}
	this.getMidX = function (){
		return middle_c.attr("cx");
	}
	this.getMidY = function (){
		return middle_c.attr("cy");
	}
	this.getLeft_c = function(){
		return left_c;
	}
	this.getRight_c = function(){
		return right_c;
	}
	
	return this;
}

/**带箭头的复杂线段(上下)
 * @argument 从id开始就都是load时候用的参数
 * */
function diyComplexLineUD(paper,initX,initY,lineLength,id,start_x,start_y,mid_x,mid_y,end_x,end_y){
	var defaultId;
	if(id){
		defaultId = id;
	}else{
		defaultId = "diy_complexLineUD_"+new Date().getTime();
	}
	this.type = "d_cud_line";
	this.pageType="complexUDLine";
	this.relationArray=[null,null];
	var selfObj =  this;
	var ponint_r = 5;
	var isAdjusting = false;
	var left_c,right_c,middle_c;
	
	var initPath;
	if(id){
		initPath = ["M",start_x,start_y,"L",start_x,mid_y,"L",end_x,mid_y,"L",end_x,end_y].join(",");
	}else{
		initPath = ["M",initX,initY,"L",initX,initY-lineLength/2,"L",initX-0+lineLength,initY-lineLength/2,"L",initX-0+lineLength,initY-lineLength].join(",");
	}
	
	var diyComplexLineUDPrevent = paper.path(initPath);
	diyComplexLineUDPrevent.attr({"opacity": .1,stroke: "white","stroke-width":"9","fill":"none","cursor": "move"});
	var diyComplexLineUD = paper.path(initPath);
	diyComplexLineUD.attr({"stroke-width":"1","shape-rendering":"crispEdges","fill":"none"});
	var diyComplexLineUDAdjust = paper.path(initPath);
	diyComplexLineUDAdjust.attr({stroke: "#99ccff","stroke-width":"1","shape-rendering":"crispEdges","stroke-dasharray": "--"});
	
	var arrow ;
	if(id){
		arrow = paper.path(["M",end_x-6,end_y-0+20,"L",end_x-0+6,end_y-0+20,"L",end_x,end_y].join(","));
	}else{
		arrow = paper.path(["M",initX-0+lineLength-6,initY-lineLength+20,"L",initX-0+lineLength+6,initY-lineLength+20,"L",initX-0+lineLength,initY-lineLength].join(","));
	}
	arrow.attr({fill:"#000000",stroke: "#000000","stroke-width":"1","shape-rendering":"crispEdges"});
	
	/**是否自己被ProcedureContain包含*/
	this.isProcedureContained = false;
	
	this.eveConnectProcedureContain = function (){
		var minX = left_c.attr("cx");
		var maxX = right_c.attr("cx");
		if(minX>maxX){
			minX = right_c.attr("cx");
			maxX = left_c.attr("cx");
		}
		var minY = left_c.attr("cy");
		var maxY = right_c.attr("cy");
		if(minX>maxY){
			minY = right_c.attr("cy");
			maxY = left_c.attr("cy");
		}
		eve("custom.connectProcedureContain",'',this,defaultId,minX,maxX,minY,maxY);
	}
	
	var connectProcedureContainTriggerFunction  = function (){
		var minX = left_c.attr("cx");
		var maxX = right_c.attr("cx");
		if(minX>maxX){
			minX = right_c.attr("cx");
			maxX = left_c.attr("cx");
		}
		var minY = left_c.attr("cy");
		var maxY = right_c.attr("cy");
		if(minX>maxY){
			minY = right_c.attr("cy");
			maxY = left_c.attr("cy");
		}
		eve("custom.connectProcedureContain",'',selfObj,defaultId,minX,maxX,minY,maxY);
	}
	
	/**注册事件(触发各个组件自己再去触发ProcedureContain的关系判断事件)*/
	eve.on("custom.connectProcedureContainTrigger",connectProcedureContainTriggerFunction);
	
	this.linkageByProcedureContainRect = function (dx, dy) {
		var olx = left_c.attr("cx");
        var oly = left_c.attr("cy");
        var orx = right_c.attr("cx");
        var ory = right_c.attr("cy");
        var omx = middle_c.attr("cx");
        var omy = middle_c.attr("cy");
		var newPath=["M", olx-0+dx, oly-0+dy,"L", olx-0+dx, omy-0+dy,"L", orx-0+dx, omy-0+dy,"L", orx-0+dx, ory-0+dy].join(",");
    	diyComplexLineUDPrevent.attr({path:newPath});
    	diyComplexLineUDAdjust.attr({path:newPath});
    	diyComplexLineUD.attr({path:newPath});
    	left_c.attr({cx: olx-0+dx,cy: oly-0+dy});
    	right_c.attr({cx: orx-0+dx,cy: ory-0+dy});
    	middle_c.attr({cx: omx-0+dx,cy: omy-0+dy});
    	if(arrow.diy_direction !=null && arrow.diy_direction == "d"){
        	var newArrowPath = ["M", orx-0+dx-6, ory-0+dy-20,"L", orx-0+dx+6, ory-0+dy-20,"L", orx-0+dx, ory-0+dy].join(",");
			arrow.attr({path:newArrowPath});
    	}else{
    		var newArrowPath = ["M", orx-0+dx-6, ory-0+dy+20,"L", orx-0+dx+6, ory-0+dy+20,"L", orx-0+dx, ory-0+dy].join(",");
			arrow.attr({path:newArrowPath});
    	}
        paper.safari();
	}
	
	 var line_dragger = function () {
        this.olx = left_c.attr("cx");
        this.oly = left_c.attr("cy");
        this.orx = right_c.attr("cx");
        this.ory = right_c.attr("cy");
        this.omx = middle_c.attr("cx");
        this.omy = middle_c.attr("cy");
//        this.animate({"fill-opacity": .3}, 500);
    },/**onmove*/
        line_move = function (dx, dy) {
//        	for(var i=0;i<selfObj.relationArray.length;i++){
//				if(selfObj.relationArray[i]!=null){
//					selfObj.relationArray[i].otherConDetelte(selfObj);
//				}
//			}
        	var newPath=["M",this.olx-0+dx,this.oly-0+dy,"L",this.olx-0+dx,this.omy-0+dy,"L",this.orx-0+dx,this.omy-0+dy,"L",this.orx-0+dx,this.ory-0+dy].join(",");
        	this.attr({path:newPath});
        	diyComplexLineUDAdjust.attr({path:newPath});
        	diyComplexLineUD.attr({path:newPath});
        	left_c.attr({cx:this.olx-0+dx,cy:this.oly-0+dy});
        	right_c.attr({cx:this.orx-0+dx,cy:this.ory-0+dy});
        	middle_c.attr({cx:this.omx-0+dx,cy:this.omy-0+dy});
        	if(arrow.diy_direction !=null && arrow.diy_direction == "d"){
	        	var newArrowPath = ["M",this.orx-0+dx-6,this.ory-0+dy-20,"L",this.orx-0+dx+6,this.ory-0+dy-20,"L",this.orx-0+dx,this.ory-0+dy].join(",");
				arrow.attr({path:newArrowPath});
        	}else{
        		var newArrowPath = ["M",this.orx-0+dx-6,this.ory-0+dy+20,"L",this.orx-0+dx+6,this.ory-0+dy+20,"L",this.orx-0+dx,this.ory-0+dy].join(",");
				arrow.attr({path:newArrowPath});
        	}
        	for(var i=0;i<selfObj.relationArray.length;i++){
				if(selfObj.relationArray[i]!=null){
					if(i==0)
						selfObj.relationArray[i].rect_move_connect_dist(left_c.attr("cx"),left_c.attr("cy"),selfObj);
					else
						selfObj.relationArray[i].rect_move_connect_dist(right_c.attr("cx"),right_c.attr("cy"),selfObj);
				}
			}
            paper.safari();
        },/**onend*/
        line_up = function () {
        	selfObj.eveConnectProcedureContain();
        	eve("custom.someoneAdjusting",'',diyComplexLineUD);
        	for(var i=0;i<selfObj.relationArray.length;i++){
				if(selfObj.relationArray[i]!=null){
					selfObj.relationArray[i].rect_move_connect_mouseUp();
				}
			}
//            this.animate({"fill-opacity": 1}, 500);
        };
	diyComplexLineUDPrevent.drag(line_move, line_dragger,line_up);//onmove, onstart, onend
	
	/**
	 * @argument distX 最终的x
	 * @argument distY 最终的y
	 * */
	this.point_move_connect = function (point, distX, distY,rectObj,notNeedConnection){
		if(point.data("id")==defaultId+"left_c"){
    		var newPath=["M",distX,distY,"L",distX,middle_c.attr("cy"),"L",right_c.attr("cx"),middle_c.attr("cy"),"L",right_c.attr("cx"),right_c.attr("cy")].join(",");
    		diyComplexLineUDAdjust.attr({path:newPath});
    		diyComplexLineUD.attr({path:newPath});
    		diyComplexLineUDPrevent.attr({path:newPath});
    		left_c.attr({cx:distX,cy:distY});
    		if(right_c.attr("cx")>=distX){
	    		middle_c.attr({cx:(right_c.attr("cx")-distX)/2+distX});
    		}else{
    			middle_c.attr({cx:(distX-right_c.attr("cx"))/2+right_c.attr("cx")});
    		}
    		if(!notNeedConnection){
    			this.addRelationParent(rectObj);
    		}
		}else if(point.data("id")==defaultId+"right_c"){
    		var newPath=["M",left_c.attr("cx"),left_c.attr("cy"),"L",left_c.attr("cx"),middle_c.attr("cy"),"L",distX,middle_c.attr("cy"),"L",distX,distY].join(",");
    		diyComplexLineUDAdjust.attr({path:newPath});
    		diyComplexLineUD.attr({path:newPath});
    		diyComplexLineUDPrevent.attr({path:newPath});
    		right_c.attr({cx:distX,cy:distY});
    		if(left_c.attr("cx")>=distX){
	    		middle_c.attr({cx:(left_c.attr("cx")-distX)/2+distX});
    		}else{
    			middle_c.attr({cx:(distX-left_c.attr("cx"))/2+left_c.attr("cx")});
    		}
			if(distY <= middle_c.attr("cy")){
        		var newArrowPath = ["M",distX-6,distY+20,"L",distX+6,distY+20,"L",distX,distY].join(",");
				arrow.attr({path:newArrowPath});
				arrow.diy_direction = "u";
    		}else{
    			var newArrowPath = ["M",distX-6,distY-20,"L",distX+6,distY-20,"L",distX,distY].join(",");
				arrow.attr({path:newArrowPath});
				arrow.diy_direction = "d";
    		}
    		if(!notNeedConnection){
	        	this.addRelationChild(rectObj);
    		}
    	}
    	this.eveConnectProcedureContain();
	}
	
	this.addRelationParent = function(obj){
		this.relationArray[0] = obj;
	}
	
	this.addRelationChild = function(obj){
		this.relationArray[1] = obj;
	}
	
	this.point_move_remove_connect =function (point,obj){
		if(point!=null){
			if(point.data("id")==defaultId+"left_c"){
				this.removeRelationParent(obj);
			}else if(point.data("id")==defaultId+"right_c"){
				this.removeRelationChild(obj);
			}
		}else{
			this.removeRelationParent(obj);		
			this.removeRelationChild(obj);
		}
	}
	
	this.removeRelationParent = function(obj){
		if(this.relationArray[0] == obj){
			this.relationArray[0] = null;
		}
	}
	
	this.removeRelationChild = function(obj){
		if(this.relationArray[1] == obj){
			this.relationArray[1] = null;
		}
	}
	
	/**调整定位点的拖拉事件处理*/
	var point_dragger = function () {
        this.ox = this.attr("cx");
        this.oy = this.attr("cy");
        this.omx = middle_c.attr("cx");
        this.animate({"fill-opacity": .3}, 500);
    },/**onmove*/
        point_move = function (dx, dy) {
        	if(this.data("id")==defaultId+"left_c"){
        		var newPath=["M",this.ox-0+dx,this.oy-0+dy,"L",this.ox-0+dx,middle_c.attr("cy"),"L",right_c.attr("cx"),middle_c.attr("cy"),"L",right_c.attr("cx"),right_c.attr("cy")].join(",");
        		diyComplexLineUDAdjust.attr({path:newPath});
        		diyComplexLineUD.attr({path:newPath});
        		diyComplexLineUDPrevent.attr({path:newPath});
        		left_c.attr({cx:this.ox+dx,cy:this.oy+dy});
        		middle_c.attr({cx:this.omx-0+dx/2});
        		
        	}else if(this.data("id")==defaultId+"middle_c"){
        		var newPath=["M",left_c.attr("cx"),left_c.attr("cy"),"L",left_c.attr("cx"),this.oy-0+dy,"L",right_c.attr("cx"),this.oy-0+dy,"L",right_c.attr("cx"),right_c.attr("cy")].join(",");
        		diyComplexLineUDAdjust.attr({path:newPath});
        		diyComplexLineUD.attr({path:newPath});
        		diyComplexLineUDPrevent.attr({path:newPath});
        		middle_c.attr({cy:this.oy+dy});
        		
        		if(right_c.attr("cy") <= this.oy-0+dy){
	        		var newArrowPath = ["M",right_c.attr("cx")-6,right_c.attr("cy")+20,"L",right_c.attr("cx")+6,right_c.attr("cy")+20,"L",right_c.attr("cx"),right_c.attr("cy")].join(",");
					arrow.attr({path:newArrowPath});
					arrow.diy_direction = "u";
        		}else{
        			var newArrowPath = ["M",right_c.attr("cx")-6,right_c.attr("cy")-20,"L",right_c.attr("cx")+6,right_c.attr("cy")-20,"L",right_c.attr("cx"),right_c.attr("cy")].join(",");
					arrow.attr({path:newArrowPath});
					arrow.diy_direction = "d";
        		}
        	}else if(this.data("id")==defaultId+"right_c"){
        		var newPath=["M",left_c.attr("cx"),left_c.attr("cy"),"L",left_c.attr("cx"),middle_c.attr("cy"),"L",this.ox-0+dx,middle_c.attr("cy"),"L",this.ox-0+dx,this.oy-0+dy].join(",");
        		diyComplexLineUDAdjust.attr({path:newPath});
        		diyComplexLineUD.attr({path:newPath});
        		diyComplexLineUDPrevent.attr({path:newPath});
        		right_c.attr({cx:this.ox+dx,cy:this.oy+dy});
        		middle_c.attr({cx:this.omx-0+dx/2});
        		
				if(this.oy-0+dy <= middle_c.attr("cy")){
	        		var newArrowPath = ["M",this.ox-0+dx-6,this.oy-0+dy+20,"L",this.ox-0+dx+6,this.oy-0+dy+20,"L",this.ox-0+dx,this.oy-0+dy].join(",");
					arrow.attr({path:newArrowPath});
					arrow.diy_direction = "u";
        		}else{
        			var newArrowPath = ["M",this.ox-0+dx-6,this.oy-0+dy-20,"L",this.ox-0+dx+6,this.oy-0+dy-20,"L",this.ox-0+dx,this.oy-0+dy].join(",");
					arrow.attr({path:newArrowPath});
					arrow.diy_direction = "d";
        		}
        	}
        	
        	
            paper.safari();
        },/**onend*/
        point_up = function () {
        	if(this.data("id")==defaultId+"left_c"){
	        	eve("custom.connectionHandler_rect",'',selfObj,this,right_c);//触发事件
        	}else{
	        	eve("custom.connectionHandler_rect",'',selfObj,this,left_c);//触发事件
        	}
        	selfObj.eveConnectProcedureContain();
        	eve("custom.someoneAdjusting",'',diyComplexLineUD);
            this.animate({"fill-opacity": .5}, 500);
        };
	
	/**生成3个调整点*/
	var draw3AjustPoint = function (paper,x,y,l,r, point_dragger, point_move, point_up){
		left_c = paper.circle(x,y,r).data("id",defaultId+"left_c");
		left_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "move"});
		left_c.drag(point_move, point_dragger, point_up);
		middle_c = paper.circle(x-0+l/2,y-l/2,r).data("id",defaultId+"middle_c");
		middle_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "n-resize"});
		middle_c.drag(point_move, point_dragger, point_up);
		right_c = paper.circle(x-0+l,y-0-l,r).data("id",defaultId+"right_c");
		right_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "move"});
		right_c.drag(point_move, point_dragger, point_up);
	}
	
	var draw3AjustPointLoad = function (paper,s_x,s_y,m_x,m_y,e_x,e_y,r, point_dragger, point_move, point_up){
		left_c = paper.circle(s_x,s_y,r).data("id",defaultId+"left_c");
		left_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "move"});
		left_c.drag(point_move, point_dragger, point_up);
		middle_c = paper.circle(m_x,m_y,r).data("id",defaultId+"middle_c");
		middle_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "n-resize"});
		middle_c.drag(point_move, point_dragger, point_up);
		right_c = paper.circle(e_x,e_y,r).data("id",defaultId+"right_c");
		right_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "move"});
		right_c.drag(point_move, point_dragger, point_up);
	}
	
	var hideAdjustLine = function(){
		diyComplexLineUDAdjust.hide();
		left_c.hide();
		right_c.hide();
		middle_c.hide();
		isAdjusting = false;
	}
	var showAdjustLine = function(){
		diyComplexLineUDAdjust.show();
		left_c.show();
		right_c.show();
		middle_c.show();
		isAdjusting=true;
	}
	if(id){
		draw3AjustPointLoad(paper,start_x,start_y,mid_x,mid_y,end_x,end_y,ponint_r, point_dragger, point_move, point_up);
	}else{
		draw3AjustPoint(paper,initX,initY,lineLength,ponint_r, point_dragger, point_move, point_up);
	}
	hideAdjustLine();
	diyComplexLineUDPrevent.click(function (){
		eve("custom.someoneAdjusting",'',diyComplexLineUD);//触发事件
	});
	
	var someoneAdjustingFunction = function (triggerEle){
		if(triggerEle == diyComplexLineUD){
			//是自己的元素触发的
			if(!isAdjusting){
				showAdjustLine();
			}
			eve.stop();
		}else{
			//是其他的元素触发的
			if(isAdjusting){
				hideAdjustLine();
			}
		}
	}
	//注册事件
	eve.on("custom.someoneAdjusting",someoneAdjustingFunction);
	
	var someoneDeleteFunction = function(){
		if(isAdjusting){
			for(var i=0;i<selfObj.relationArray.length;i++){
				if(selfObj.relationArray[i]!=null){
					selfObj.relationArray[i].otherConDetelte(selfObj);
				}
			}
			diyComplexLineUDPrevent.remove();
			diyComplexLineUD.remove();
			diyComplexLineUDAdjust.remove();
			left_c.remove();
			right_c.remove();
			arrow.remove();
			middle_c.remove();
			removeLine(selfObj);
			removeComponentObjMap(defaultId);
			eve.off("custom.someoneAdjusting",someoneAdjustingFunction);
			eve.off("custom.connectProcedureContainTrigger", connectProcedureContainTriggerFunction);
			eve.off("custom.someoneDelete", someoneDeleteFunction);
			selfObj = null;
		}		
	}
	
	//注册删除元素事件
	eve.on("custom.someoneDelete",someoneDeleteFunction);
	
	this.getId = function (){
		return defaultId;
	}
	this.getId = function (){
		return defaultId;
	}
	this.getStartX = function (){
		return left_c.attr("cx");
	}
	this.getStartY = function (){
		return left_c.attr("cy");
	}
	this.getEndX = function (){
		return right_c.attr("cx");
	}
	this.getEndY = function (){
		return right_c.attr("cy");
	}
	this.getMidX = function (){
		return middle_c.attr("cx");
	}
	this.getMidY = function (){
		return middle_c.attr("cy");
	}
	this.getLeft_c = function(){
		return left_c;
	}
	this.getRight_c = function(){
		return right_c;
	}
	
	return this;
}

/**判断流程框
 * @augments branchNum 和 从id开始就都是load时候用的参数
 * */
function diyBranch(paper,initX,initY,lineXLength,lineYLength,branchNum,textOffsetY,id,top_x,top_y,left_x,left_y,right_x,right_y,text_contents,dataArrayStr,outTop_x,outTop_y){
	this.type = "d_branch_line";
	this.pageType="branch";
	if(!branchNum){
		branchNum = 2;
	}
	if(!textOffsetY){
		textOffsetY = 20;
	}
	/**dataArrayStr保存从编辑传来的object数组转化的原始字符串*/
	var dAStr;
	if(dataArrayStr){
		dAStr=dataArrayStr;
	}
	var defaultId;
	if(id){
		defaultId = id;
	}else{
		defaultId = "diy_branch_"+new Date().getTime();
	}
	var selfObj = this;
	/**是否自己被ProcedureContain包含*/
	this.isProcedureContained = false;
	/**用来存放top上的元素*/
	this.relationArray=[null];
	/**用来存放branch中的矩形,用于save持久化的时候(this.containsArray是一纬数组，非2维，因为现在每个branch只有一个矩形)*/
	this.containsArray = new Array(branchNum);
//	for(var i=0;i<this.containsArray.length;i++){
//		this.containsArray[i] = new Array();
//	}
	var branchLineObj;
	var branchLineObjPrevent;
	var branchLineObjAdjust;
	var judgeObj1;
//	var branchLineObjs = [];//存放分支的array
	var branchTextObjs = [];//存放分支text的array
	var ponint_r = 5;
	var isAdjusting = false;
	var top_c;
	var left_c;
	var right_c;
	var outTop_c;
	/**用来存放relation点的数组(0:上 n-1:branch分支 index=n-1)(主要用于联动)*/
	var rcArray = new Array(branchNum-0+1); 
	var outTopLength = 50;
	
	var init = function (){
		var branchLine = ["M",initX,initY,"L",initX,initY+outTopLength,"M",initX-lineXLength/2,initY+outTopLength,"L",initX-0+lineXLength/2,initY+outTopLength ].join(",");
		var gap = lineXLength/(branchNum-1);
		for(var i=0;i<branchNum;i++){
			var ts = ["M",(initX-lineXLength/2)+(i*gap),initY+outTopLength,"L",(initX-lineXLength/2)+(i*gap),initY+outTopLength+lineYLength/2].join(",");
			branchLine+=ts;
//			var tmpLineObj = paper.path(ts);
//			tmpLineObj.attr({"stroke-width":"1","shape-rendering":"crispEdges","fill":"none"});
//			branchLineObjs.push(tmpLineObj);
			var tmpTextObj = paper.text((initX-lineXLength/2)+(i*gap),initY-0+50-0+textOffsetY,"").attr({"font-size":14}).data("id",defaultId+"["+(i+1)+"]");
			if(text_contents && text_contents[i]){
				var showValue = text_contents[i];
				
				if(showValue.length>15){
					showValue = showValue.substring(0,15)+"...";
				}
				tmpTextObj.attr({"text":showValue});
				tmpTextObj.val_value=text_contents[i];
			}
			branchTextObjs.push(tmpTextObj);
		}
		branchLineObj=paper.path(branchLine);
		branchLineObj.cusType = "branch";
		branchLineObjPrevent=paper.path(branchLine);
		branchLineObj.attr({"stroke-width":"1","shape-rendering":"crispEdges","fill":"none"});
		branchLineObjPrevent.attr({"opacity": .1,stroke: "white","stroke-width":"9","fill":"none","cursor": "move"});
		branchLineObjAdjust = paper.path(branchLine);
		branchLineObjAdjust.attr({stroke: "#99ccff","stroke-width":"1","shape-rendering":"crispEdges","stroke-dasharray": "--"});
		judgeObj1 = paper.path(["M",initX,initY+outTopLength-10,"L",initX-10,initY+outTopLength,"L",initX,initY+outTopLength+10,"L",initX+10,initY+outTopLength,"Z"].join(","));
		judgeObj1.attr({fill:"#000000",stroke: "#000000","stroke-width":"1","shape-rendering":"crispEdges","cursor": "move"});
	}
	
	if(id){
//		top_x,top_y,bottom_x,bottom_y,left_x,left_y,right_x,right_y,text_contents,outTop_x,outTop_y,outBottom_x,outBottom_y
		var branchLine = ["M",top_x,top_y,"L",outTop_x,outTop_y,"M",left_x,top_y,"L", right_x,top_y].join(",");
		var gap = (right_x-left_x)/(branchNum-1);
		var tcArray = text_contents.split("&split");
		for(var i=0;i<branchNum;i++){
			//TODO left_y 就是每个分支的最下点y left_y*********
			var ts = ["M",(left_x-0)+(i*gap),top_y,"L",(left_x-0)+(i*gap),left_y].join(",");
			branchLine+=ts;
			if(tcArray[i]=="empty"){
				tcArray[i] = "";
			}
			var tmpTextObj = paper.text((left_x-0)+(i*gap),top_y-0+textOffsetY,tcArray[i]).data("id",defaultId+"["+(i+1)+"]");
			var val_value = tmpTextObj.attr("text");
			var showValue = tmpTextObj.attr("text");
			if(showValue){
				if(showValue.length>15){
					showValue = showValue.substring(0,15)+"...";
				}
				tmpTextObj.attr({"text":showValue});
				tmpTextObj.val_value=val_value;
			}
			
			branchTextObjs.push(tmpTextObj);
		}
		branchLineObj=paper.path(branchLine);
		branchLineObj.cusType = "branch";
		branchLineObjPrevent=paper.path(branchLine);
		branchLineObj.attr({"stroke-width":"1","shape-rendering":"crispEdges","fill":"none"});
		branchLineObjPrevent.attr({"opacity": .1,stroke: "white","stroke-width":"9","fill":"none","cursor": "move"});
		branchLineObjAdjust = paper.path(branchLine);
		branchLineObjAdjust.attr({stroke: "#99ccff","stroke-width":"1","shape-rendering":"crispEdges","stroke-dasharray": "--"});
		judgeObj1 = paper.path(["M",top_x,top_y-10,"L",top_x-10,top_y,"L",top_x,top_y-0+10,"L",top_x-0+10,top_y,"Z"].join(","));
		judgeObj1.attr({fill:"#000000",stroke: "#000000","stroke-width":"1","shape-rendering":"crispEdges","cursor": "move"});
	}else{
		init();
	}
	
	this.eveConnectProcedureContain = function (){
		var minX = left_c.attr("cx");
		var maxX = right_c.attr("cx");
		var minY = outTop_c.attr("cy");
		var maxY = left_c.attr("cy");
		eve("custom.connectProcedureContain",'',this,defaultId,minX,maxX,minY,maxY);
	}
	
	var connectProcedureContainTriggerFunction  = function (){
		var minX = left_c.attr("cx");
		var maxX = right_c.attr("cx");
		var minY = outTop_c.attr("cy");
		var maxY = left_c.attr("cy");
		eve("custom.connectProcedureContain",'',selfObj,defaultId,minX,maxX,minY,maxY);
	}
	
	/**注册事件(触发各个组件自己再去触发ProcedureContain的关系判断事件)*/
	eve.on("custom.connectProcedureContainTrigger",connectProcedureContainTriggerFunction);
	
	this.line_move_branch = function (dx, dy,distX ,distY,type,triggerObj) {
		if(dx == null ){
			if(type=="bottom"){
				dx = distX - outTop_c.attr("cx");
				dy = distY - outTop_c.attr("cy");
			}else if(type=="top"){
				var targetI=-1;
				for(var ii = 0 ;ii<selfObj.containsArray.length;ii++){
					if(selfObj.containsArray[ii]==triggerObj){
						targetI=ii;
						break;
					}
				}
				var gap = (right_c.attr("cx")-left_c.attr("cx"))/(branchNum-1);
				dx = distX - (left_c.attr("cx")+((targetI)*gap));
				dy = distY - left_c.attr("cy");
			}
		}
		if(dx ==0 && dy ==0){
			return ;
		}
    	var newPath = ["M",outTop_c.attr("cx")+dx,outTop_c.attr("cy")+dy,"L",top_c.attr("cx")+dx,top_c.attr("cy")+dy,"M",left_c.attr("cx")+dx,top_c.attr("cy")+dy,"L",right_c.attr("cx")+dx,top_c.attr("cy")+dy].join(",");
        var gap = (right_c.attr("cx")-left_c.attr("cx"))/(branchNum-1);
        for(var i=0;i<branchNum;i++){
			var ts = ["M",left_c.attr("cx")+dx+(i*gap),top_c.attr("cy")-0+dy,"L",left_c.attr("cx")+dx+(i*gap),left_c.attr("cy")-0+dy].join(",");
			newPath+=ts;
		}
        branchLineObj.attr({path:newPath});
		branchLineObjAdjust.attr({path:newPath});
		branchLineObjPrevent.attr({path:newPath});
		judgeObj1.attr({path:["M",top_c.attr("cx")+dx,top_c.attr("cy")+dy-10,"L",top_c.attr("cx")-10+dx,top_c.attr("cy")+dy,"L",top_c.attr("cx")+dx,top_c.attr("cy")+dy+10,"L",top_c.attr("cx")+dx+10,top_c.attr("cy")+dy].join(",")});
		top_c.attr({cx:top_c.attr("cx")+dx,cy:top_c.attr("cy")+dy});
		left_c.attr({cx:left_c.attr("cx")+dx,cy:left_c.attr("cy")+dy});
		right_c.attr({cx:right_c.attr("cx")+dx,cy:right_c.attr("cy")+dy});
        outTop_c.attr({cx:outTop_c.attr("cx")+dx,cy:outTop_c.attr("cy")+dy});
        for(var i=0;i<branchTextObjs.length;i++){
        	branchTextObjs[i].attr({"x":left_c.attr("cx")+(i*gap),"y":top_c.attr("cy")+textOffsetY});
        }
        linkageAdjust();
        this.eveConnectProcedureContain();
        paper.safari();
    }
    
    this.linkageByProcedureContainRect = function (dx, dy) {
    	var olx = left_c.attr("cx");
        var oly = left_c.attr("cy");
        var orx = right_c.attr("cx");
        var ory = right_c.attr("cy");
        var otx = top_c.attr("cx");
        var oty = top_c.attr("cy");
        var ootx = outTop_c.attr("cx");
        var ooty = outTop_c.attr("cy");
    	var newPath = ["M",ootx+dx,ooty+dy,"L",otx+dx,oty+dy,"M",olx+dx,oty+dy,"L",orx+dx,oty+dy].join(",");
        var gap = (orx-olx)/(branchNum-1);
        for(var i=0;i<branchNum;i++){
			var ts = ["M",olx+dx+(i*gap),oty-0+dy,"L",olx+dx+(i*gap),oly-0+dy].join(",");
			newPath+=ts;
		}
        branchLineObj.attr({path:newPath});
		branchLineObjAdjust.attr({path:newPath});
		branchLineObjPrevent.attr({path:newPath});
		judgeObj1.attr({path:["M",otx+dx,oty+dy-10,"L",otx-10+dx,oty+dy,"L",otx+dx,oty+dy+10,"L",otx+dx+10,oty+dy].join(",")});
		top_c.attr({cx:otx+dx,cy:oty+dy});
		left_c.attr({cx:olx+dx,cy:oly+dy});
		right_c.attr({cx:orx+dx,cy:ory+dy});
		outTop_c.attr({cx:ootx+dx,cy:ooty+dy});
        for(var i=0;i<branchTextObjs.length;i++){
        	branchTextObjs[i].attr({"x":olx+dx+(i*gap),"y":oty+dy+textOffsetY});
        }
        paper.safari();
    }

	var line_dragger = function () {
        this.olx = left_c.attr("cx");
        this.oly = left_c.attr("cy");
        this.orx = right_c.attr("cx");
        this.ory = right_c.attr("cy");
        this.otx = top_c.attr("cx");
        this.oty = top_c.attr("cy");
        this.ootx = outTop_c.attr("cx");
        this.ooty = outTop_c.attr("cy");
//        this.animate({"fill-opacity": .3}, 500);
    },/**onmove*/
        line_move = function (dx, dy) {
        	var newPath = ["M",this.ootx+dx,this.ooty+dy,"L",this.otx+dx,this.oty+dy,"M",this.olx+dx,this.oty+dy,"L",this.orx+dx,this.oty+dy].join(",");
            var gap = (this.orx-this.olx)/(branchNum-1);
	        for(var i=0;i<branchNum;i++){
				var ts = ["M",this.olx+dx+(i*gap),this.oty-0+dy,"L",this.olx+dx+(i*gap),this.oly-0+dy].join(",");
				newPath+=ts;
			}
            branchLineObj.attr({path:newPath});
    		branchLineObjAdjust.attr({path:newPath});
    		branchLineObjPrevent.attr({path:newPath});
    		judgeObj1.attr({path:["M",this.otx+dx,this.oty+dy-10,"L",this.otx-10+dx,this.oty+dy,"L",this.otx+dx,this.oty+dy+10,"L",this.otx+dx+10,this.oty+dy].join(",")});
    		top_c.attr({cx:this.otx+dx,cy:this.oty+dy});
    		left_c.attr({cx:this.olx+dx,cy:this.oly+dy});
    		right_c.attr({cx:this.orx+dx,cy:this.ory+dy});
    		outTop_c.attr({cx:this.ootx+dx,cy:this.ooty+dy});
            for(var i=0;i<branchTextObjs.length;i++){
            	branchTextObjs[i].attr({"x":this.olx+dx+(i*gap),"y":this.oty+dy+textOffsetY});
            }
            linkageAdjust();
            paper.safari();
        },/**onend*/
        line_up = function () {
        	selfObj.eveConnectProcedureContain();
        	eve("custom.someoneAdjusting",'',branchLineObj);
//            this.animate({"fill-opacity": 1}, 500);
        };
	branchLineObjPrevent.drag(line_move, line_dragger,line_up);//onmove, onstart, onend
	judgeObj1.drag(line_move, line_dragger,line_up);//onmove, onstart, onend
	
	/**调整定位点的拖拉事件处理*/
	var point_dragger = function () {
        this.ox = this.attr("cx");
        this.oy = this.attr("cy");
        this.oly = left_c.attr("cy");
        this.olx = left_c.attr("cx");
        this.ory = right_c.attr("cy");
        this.orx = right_c.attr("cx");
        this.ooty = outTop_c.attr("cy");
        this.ootx = outTop_c.attr("cx");
        this.animate({"fill-opacity": .3}, 500);
        this.tmpOotyDy = 0;
        this.tmpOobyDy = 0;
    },/**onmove*/
        point_move = function (dx, dy) {
        	if(this.data("id")==defaultId+"top_c"){
        		var newPath = ["M",outTop_c.attr("cx"),outTop_c.attr("cy"),"L",this.ox,this.oy-0+dy,"M",left_c.attr("cx"),this.oy-0+dy,"L",right_c.attr("cx"),this.oy-0+dy ].join(",");
		        var gap = (right_c.attr("cx")-left_c.attr("cx"))/(branchNum-1);
		        for(var i=0;i<branchNum;i++){
					var ts = ["M",left_c.attr("cx")+(i*gap),this.oy-0+dy,"L",left_c.attr("cx")+(i*gap),left_c.attr("cy")].join(",");
					newPath+=ts;
				}
        		branchLineObj.attr({path:newPath});
        		branchLineObjAdjust.attr({path:newPath});
        		branchLineObjPrevent.attr({path:newPath});
        		top_c.attr({cy:this.oy+dy});
        		left_c.attr({cy:this.oly+dy/2});
        		right_c.attr({cy:this.ory+dy/2});
        		judgeObj1.attr({path:["M",this.ox,this.oy-10+dy,"L",this.ox-10,this.oy+dy,"L",this.ox,this.oy-0+10+dy,"L",this.ox-0+10,this.oy+dy].join(",")});
        		for(var i=0;i<branchTextObjs.length;i++){
	            	branchTextObjs[i].attr({"y":this.oy+dy+textOffsetY});
	            }
	            linkageAdjust();
        	}else if(this.data("id")==defaultId+"left_c"){
        		var newPath = ["M",outTop_c.attr("cx"),outTop_c.attr("cy"),"L",top_c.attr("cx"),top_c.attr("cy"),"M",this.ox+dx,top_c.attr("cy"),"L",this.orx-dx,top_c.attr("cy")].join(",");
		        var gap = (this.orx-this.ox-dx*2)/(branchNum-1);
		        for(var i=0;i<branchNum;i++){
					var ts = ["M",this.ox+dx+(i*gap),top_c.attr("cy"),"L",this.ox+dx+(i*gap),this.oy+dy].join(",");
					newPath+=ts;
				}
        		branchLineObj.attr({path:newPath});
        		branchLineObjAdjust.attr({path:newPath});
        		branchLineObjPrevent.attr({path:newPath});
        		left_c.attr({cx:this.ox+dx,cy:this.oy+dy});
        		right_c.attr({cx:this.orx-dx,cy:this.oy+dy});
        		for(var i=0;i<branchTextObjs.length;i++){
	            	branchTextObjs[i].attr({"x":this.ox+dx+(i*gap)});
	            }
	            linkageAdjust("left");
        	}else if(this.data("id")==defaultId+"right_c"){
        		var newPath = ["M",outTop_c.attr("cx"),outTop_c.attr("cy"),"L",top_c.attr("cx"),top_c.attr("cy"),"M",this.olx-dx,top_c.attr("cy"),"L",this.ox+dx,top_c.attr("cy")].join(",");
		        var gap = (this.ox-this.olx+dx*2)/(branchNum-1);
		        for(var i=0;i<branchNum;i++){
					var ts = ["M",this.olx-dx+(i*gap),top_c.attr("cy"),"L",this.olx-dx+(i*gap),this.oy+dy].join(",");
					newPath+=ts;
				}
        		branchLineObj.attr({path:newPath});
        		branchLineObjAdjust.attr({path:newPath});
        		branchLineObjPrevent.attr({path:newPath});
        		left_c.attr({cx:this.olx-dx,cy:this.oy+dy});
        		right_c.attr({cx:this.ox+dx,cy:this.oy+dy});
        		for(var i=0;i<branchTextObjs.length;i++){
	            	branchTextObjs[i].attr({"x":this.olx-dx+(i*gap)});
	            }
	            linkageAdjust("right");
        	}else if(this.data("id")==defaultId+"outTop_c"){
        		if(this.ooty+dy < top_c.attr("cy")){
        			outTop_c.attr({cy:this.ooty+dy});
        			
        			var newPath = ["M",outTop_c.attr("cx"),this.ooty+dy,"L",top_c.attr("cx"),top_c.attr("cy"),"M",left_c.attr("cx"),top_c.attr("cy"),"L",right_c.attr("cx"),top_c.attr("cy")].join(",");
			        var gap = (right_c.attr("cx")-left_c.attr("cx"))/(branchNum-1);
			        for(var i=0;i<branchNum;i++){
						var ts = ["M",left_c.attr("cx")+(i*gap),top_c.attr("cy"),"L",left_c.attr("cx")+(i*gap),left_c.attr("cy")].join(",");
						newPath+=ts;
					}
	        		branchLineObj.attr({path:newPath});
	        		branchLineObjAdjust.attr({path:newPath});
	        		branchLineObjPrevent.attr({path:newPath});
        		}
        	}
            paper.safari();
        },/**onend*/
        point_up = function () {
        	if(this.data("id")==defaultId+"outTop_c"){
        		if(rcArray[0]!=null){
        			//如果已经有关系，则判断是否解除和矩形的关系
        			if(rcArray[0][0]!=null){
        				var triObj = rcArray[0][0];
        				//判断outTop是否在矩形triObj内
        				var ot_y = outTop_c.attr("cy")
        				if(ot_y>=triObj.getLeftTop_y() && ot_y<=triObj.getRightBottom_y()){
        					//在矩形内
        					linkageAdjust();
        				}else{
        					//取消关系
        					selfObj.removeRelationParent(triObj);
	        				triObj.conMarkHide("bottom");
	        				rcArray[0]=null;
        				}
        			}
        		}else{
        			//没有关系则判断是否添加和矩形的关系
        			eve("custom.connectionHandlerByBranch");
        		}
        	}else if(this.data("id")==defaultId+"left_c"){
        		if(rcArray[1]!=null){
        			//如果已经有关系，则判断是否解除和矩形的关系
        			if(rcArray[1][0]!=null){
        				var triObj = rcArray[1][0];
        				//判断outTop是否在矩形triObj内
        				var ot_y = left_c.attr("cy");
        				var ot_x = left_c.attr("cx");
        				if(ot_y>=triObj.getLeftTop_y() && ot_y<=triObj.getRightBottom_y() && ot_x>=triObj.getLeftTop_x() && ot_x<=triObj.getRightBottom_x()){
        					//在矩形内
        					linkageAdjust();
        				}else{
        					//取消关系
//        					selfObj.removeRelationParent(triObj);
	        				triObj.conMarkHide("top");
	        				rcArray[1]=null;
	        				selfObj.containsArray[0] = null;
        				}
        			}
        		}else{
        			//没有关系则判断是否添加和矩形的关系
        			eve("custom.connectionHandlerByBranch");
        		}
        	}else if(this.data("id")==defaultId+"right_c"){
        		if(rcArray[2]!=null){
        			//如果已经有关系，则判断是否解除和矩形的关系
        			if(rcArray[2][0]!=null){
        				var triObj = rcArray[2][0];
        				//判断outTop是否在矩形triObj内
        				var ot_y = right_c.attr("cy");
        				var ot_x = right_c.attr("cx");
        				if(ot_y>=triObj.getLeftTop_y() && ot_y<=triObj.getRightBottom_y() && ot_x>=triObj.getLeftTop_x() && ot_x<=triObj.getRightBottom_x()){
        					//在矩形内
        					linkageAdjust();
        				}else{
        					//取消关系
//        					selfObj.removeRelationParent(triObj);
	        				triObj.conMarkHide("top");
	        				rcArray[2]=null;
	        				selfObj.containsArray[1] = null;
        				}
        			}
        		}else{
        			//没有关系则判断是否添加和矩形的关系
        			eve("custom.connectionHandlerByBranch");
        		}
        	}
        	//TODO 托left_c,right_c的点
        	selfObj.eveConnectProcedureContain();
        	eve("custom.someoneAdjusting",'',branchLineObj);
            this.animate({"fill-opacity": .5}, 500);
        };
        
	/**生成4个调整点*/
	var drawNAjustPoint = function (paper,x,y,xl,yl,r, point_dragger, point_move, point_up){
		top_c = paper.circle(x,y+outTopLength,r).data("id",defaultId+"top_c");
		top_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "n-resize"});
		top_c.drag(point_move, point_dragger, point_up);
		left_c = paper.circle(x-xl/2,y+outTopLength+yl/2,r).data("id",defaultId+"left_c");
		left_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "move"});
		left_c.drag(point_move, point_dragger, point_up);
		right_c = paper.circle(x+xl/2,y+outTopLength+yl/2,r).data("id",defaultId+"right_c");
		right_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "move"});
		right_c.drag(point_move, point_dragger, point_up);
		outTop_c = paper.circle(x,y,r).data("id",defaultId+"outTop_c");
		outTop_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "n-resize"});
		outTop_c.drag(point_move, point_dragger, point_up);
	}
	var drawNAjustPointLoad = function (paper,top_x,top_y,left_x,left_y,right_x,right_y,r,outTop_x,outTop_y, point_dragger, point_move, point_up){
		top_c = paper.circle(top_x,top_y,r).data("id",defaultId+"top_c");
		top_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "n-resize"});
		top_c.drag(point_move, point_dragger, point_up);
		left_c = paper.circle(left_x,left_y,r).data("id",defaultId+"left_c");
		left_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "move"});
		left_c.drag(point_move, point_dragger, point_up);
		right_c = paper.circle(right_x,right_y,r).data("id",defaultId+"right_c");
		right_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "move"});
		right_c.drag(point_move, point_dragger, point_up);
		outTop_c = paper.circle(outTop_x,outTop_y,r).data("id",defaultId+"outTop_c");
		outTop_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "n-resize"});
		outTop_c.drag(point_move, point_dragger, point_up);
	}
	if(id){
		drawNAjustPointLoad(paper,top_x,top_y,left_x,left_y,right_x,right_y,ponint_r,outTop_x,outTop_y, point_dragger, point_move, point_up);
	}else{
		drawNAjustPoint(paper,initX,initY,lineXLength,lineYLength,ponint_r, point_dragger, point_move, point_up);
	}
	var hideAdjustLine = function(){
		branchLineObjAdjust.hide();
		top_c.hide();
		left_c.hide();
		right_c.hide();
		outTop_c.hide();
		isAdjusting = false;
	}
	var showAdjustLine = function(){
		branchLineObjAdjust.show();
		left_c.show();
		right_c.show();
		top_c.show();
		outTop_c.show();
		isAdjusting=true;
	}
	hideAdjustLine();
	
	branchLineObjPrevent.click(function (){
		eve("custom.someoneAdjusting",'',branchLineObj);//触发事件
	});
	
	this.update = function (dataArray){
		var results = resolveBranch(dataArray);
		dAStr = results[2];
		for(var di = 0;di<branchNum;di++){
			var valValue = "";
			if(results[1][di]){
				valValue = results[1][di];
			}
			var showText = valValue;
			if(showText.length>15){
				showText = showText.substring(0,15)+"...";
			}
			branchTextObjs[di].attr({"text":showText});
			branchTextObjs[di].val_value=valValue;
		}
	}
	
	var someoneAdjustingFunction = function (triggerEle){
		if(triggerEle == branchLineObj){
			//是自己的元素触发的
			if(!isAdjusting){
				showAdjustLine();
				toEdit(defaultId,selfObj.pageType,dAStr);
				//remove
				//各个组件自己的特殊化操作
//				document.getElementById("branchDynContain").innerHTML ="";
//				var inputContain = document.getElementById("branchDynContain");
//				for(var bi = 0;bi<branchNum;bi++){
//					var add_input=document.createElement("input")
//				    add_input.id="branchText"+(bi+1);
//				    add_input.tmp_bi=bi;
//				    add_input.value = branchTextObjs[bi].attr("text");
//				    inputContain.appendChild(add_input);
//				    add_input.onchange = function (){
//						branchTextObjs[this.tmp_bi].attr({"text":this.value});
//					}
//				}
			}
			eve.stop();
		}else{
			//是其他的元素触发的
			if(isAdjusting){
				hideAdjustLine();
			}
		}
	}
	//注册事件
	eve.on("custom.someoneAdjusting",someoneAdjustingFunction);
	
	/**判断其他组件和branch的关系*/
	var connectionHandlerFunction = function (triggerObj,real_pic,minRectX,minRectY,maxRectX,maxRectY){
		var top_x_branch = outTop_c.attr("cx");
		var top_y_branch = outTop_c.attr("cy");
		var left_x_branch = left_c.attr("cx");
		var y_min_branch = top_c.attr("cy");
		var y_max_branch =  left_c.attr("cy");
		var right_x_branch = right_c.attr("cx");
		
		if(top_x_branch>=minRectX && top_x_branch<= maxRectX && top_y_branch>=minRectY && top_y_branch<= maxRectY ){
//			console.log("连接到branch的最顶点");
			//连接到branch的最顶点   rect 的 dx,dy
			triggerObj.rect_move_connect(0-((maxRectX-minRectX)/2+minRectX-top_x_branch),0-(maxRectY-top_y_branch));
			selfObj.addRelationParent(triggerObj);
			rcArray[0]=[triggerObj,real_pic];
			triggerObj.conMarkShow("bottom");
			triggerObj.addRcArray(selfObj,"bottom");
//			console.log("console.log(triggerObj)0",triggerObj)
		}else if(selfObj.relationArray[0]==triggerObj){
//			console.log("解除branch的最顶点");
			selfObj.removeRelationParent(triggerObj);
			rcArray[0]=null;
			triggerObj.conMarkHide("bottom");
		}
		var gap = (right_x_branch-left_x_branch)/(branchNum-1);
		var isHadContained = false;
		for(var i=0;i<branchNum;i++){
			if(!isHadContained && left_x_branch+(i*gap)>=minRectX && left_x_branch+(i*gap)<= maxRectX && y_max_branch>=minRectY && y_max_branch<= maxRectY){
				//有交集
				triggerObj.rect_move_connect((left_x_branch+(i*gap))-((maxRectX-minRectX)/2+minRectX),y_max_branch-minRectY);
				var isContained = false;
				if(selfObj.containsArray[i] && selfObj.containsArray[i]!=null){
					if(selfObj.containsArray[i]==triggerObj){
						isContained=true;
					}
				}
				if(!isContained){
					//把矩形放到branch的path之上显示
					selfObj.containsArray[i] = triggerObj;
					
//					if(rcArray[i+1] == null){
//						rcArray[i+1]= new Array();
//					}
					rcArray[i+1]=[triggerObj,real_pic,top_c.attr("cy")];
//					console.log("!!!")
					
					triggerObj.conMarkShow("top");
					triggerObj.addRcArray(selfObj,"top");
				}
				isHadContained=true;
			}else{
				//无交集
				if(selfObj.containsArray[i]==triggerObj){
					selfObj.containsArray[i]=null;
					triggerObj.conMarkHide("top");
//					break;
				}
				if(rcArray[i+1]!=null && rcArray[i+1][0]==triggerObj){
					rcArray[i+1] = null;
//					break;
//					for(var k=0;k<rcArray[i+1].length;k++){
//						if(rcArray[i+1][k]!=null && rcArray[i+1][k][0]==triggerObj){
//							rcArray[i+1][k] = null;
//							break;
//						}
//					}
				}
			}
		}
		
		
		//TODO 加入左右2点的判断
//		var gap = (right_x_branch-left_x_branch)/(branchNum-1);
//		/**用来标识矩形是否已经和之前的branch的分支组成关系*/
//		var isHadContained = false;
//		for(var i=0;i<branchNum;i++){
//			if(!isHadContained && left_x_branch+(i*gap)>=minRectX && left_x_branch+(i*gap)<=maxRectX && maxRectY>=y_min_branch && minRectY <= y_max_branch){
//				//有交集
//				triggerObj.rect_move_connect(left_x_branch+(i*gap)-((maxRectX-minRectX)/2+minRectX),0);
//				/**用来标识矩形是否在之前已经和的branch的分支存在关系了，不需要重复加关系*/
//				var isContained = false;
//				if(selfObj.containsArray[i] && selfObj.containsArray[i]!=null){
//					for(var j=0;j<selfObj.containsArray[i].length;j++){
//						if(selfObj.containsArray[i][j]==triggerObj){
//							isContained=true;
//						}
//					}
//				}
//				if(!isContained){
//					//把矩形放到branch的path之上显示
//					triggerObj.overlay(branchLineObj);
//					selfObj.containsArray[i].push(triggerObj);
//					if(rcArray[i+1] == null){
//						rcArray[i+1]= new Array();
//					}
//					rcArray[i+1].push([triggerObj,real_pic,top_c.attr("cy")]);
//				}
//				isHadContained=true;
//			}else{
//				//无交集
//				for(var j=0;j<selfObj.containsArray[i].length;j++){
//					if(selfObj.containsArray[i][j]==triggerObj){
//						selfObj.containsArray[i][j]=null;
//						break;
//					}
//				}
//				if(rcArray[i+1]!=null){
//					for(var k=0;k<rcArray[i+1].length;k++){
//						if(rcArray[i+1][k]!=null && rcArray[i+1][k][0]==triggerObj){
//							rcArray[i+1][k] = null;
//							break;
//						}
//					}
//				}
//			}
//		}
//		console.log("relationArray",selfObj.relationArray);
//		console.log("containsArray",selfObj.containsArray);
//		console.log("rcArray",rcArray);
	}
	
	//注册事件
	/**
	 * @argument : minRectX,minRectY,maxRectX,maxRectY rect的4个顶点
	 * */
	eve.on("custom.connectionHandler_branch",connectionHandlerFunction);
	
	var linkageAdjust = function(rigthOrLeft){
		for(var i=0;i<rcArray.length;i++){
			if(i==0 && rcArray[i]!=null){
//				console.log(111)
				var rp = rcArray[i][1];
				rcArray[i][0].rect_move_connect(outTop_c.attr("cx")-(rp.attr("x")+rp.attr("width")/2),outTop_c.attr("cy")-(rp.attr("y")+rp.attr("height")));
			}else{
				// 其他联动
				if(i>0 && rcArray[i]!=null){
					if( !rigthOrLeft || (rigthOrLeft=="left" && i==2) || (rigthOrLeft=="right" && i==1)){
						var rp = rcArray[i][1];
						var gap = (right_c.attr("cx")-left_c.attr("cx"))/(branchNum-1);
						rcArray[i][0].rect_move_connect(left_c.attr("cx")+((i-1)*gap)-((rp.attr("x")+rp.attr("width")/2)),left_c.attr("cy")-rp.attr("y"));
	//					rcArray[i][2]=top_c.attr("cy");
						
	//					for(var j=0;j<rcArray[i].length;j++){
	//						if(rcArray[i][j]!=null){
	//							var rp = rcArray[i][j][1];
	//							var gap = (right_c.attr("cx")-left_c.attr("cx"))/(branchNum-1);
	//							rcArray[i][j][0].rect_move_connect(left_c.attr("cx")+((i-1)*gap)-((rp.attr("x")+rp.attr("width")/2)),top_c.attr("cy")-rcArray[i][j][2]);
	//							rcArray[i][j][2]=top_c.attr("cy");
	//						}
	//					}
					}
				}
			} 
			
		}
	}
	
	this.addRelationParent = function(obj){
		this.relationArray[0] = obj;
	}
	
	this.addRelationChild = function(obj){
		this.relationArray[1] = obj;
	}
	
	this.point_move_remove_connect =function (point,obj){
		if(point!=null){
			if(point.data("id")==defaultId+"left_c"){
				this.removeRelationParent(obj);
			}else if(point.data("id")==defaultId+"right_c"){
				this.removeRelationChild(obj);
			}
		}else{
			this.removeRelationParent(obj);		
			this.removeRelationChild(obj);
		}
	}
	
	this.removeRelationParent = function(obj){
		if(this.relationArray[0] == obj){
			this.relationArray[0] = null;
		}
	}
	
	this.removeRelationChild = function(obj){
		if(this.relationArray[1] == obj){
			this.relationArray[1] = null;
		}
	}
	
	var deleteFunction = function(){
		if(isAdjusting){
			for(var i=0;i<selfObj.relationArray.length;i++){
				if(selfObj.relationArray[i]!=null){
					selfObj.relationArray[i].otherConDetelte(selfObj);
				}
			}
			branchLineObj.remove();
			branchLineObjPrevent.remove();
			branchLineObjAdjust.remove();
			left_c.remove();
			right_c.remove();
			top_c.remove();
			outTop_c.remove();
			judgeObj1.remove();
			for(var i=0;i<branchTextObjs.length;i++){
				branchTextObjs[i].remove();
			}
			removeLine(selfObj);
			removeComponentObjMap(defaultId);
			//注销对象上的所有已注册的事件
			eve.off("custom.someoneAdjusting", someoneAdjustingFunction);
			eve.off("custom.connectionHandler_branch", connectionHandlerFunction);
			eve.off("custom.connectProcedureContainTrigger", connectProcedureContainTriggerFunction);
			eve.off("custom.someoneDelete", deleteFunction);
			selfObj = null;
		}		
	}
	
	this.id = defaultId;
	
	//注册删除元素事件
	eve.on("custom.someoneDelete",deleteFunction);
	
	this.getId = function (){
		return defaultId;
	}
	this.getBranchTextObjs = function (){
		return branchTextObjs;
	}
	this.getBranchNum = function (){
		return branchNum;
	}
	this.getTopX = function (){
		return top_c.attr("cx");
	}
	this.getTopY = function (){
		return top_c.attr("cy");
	}
	this.getBottomX = function (){
		return bottom_c.attr("cx");
	}
	this.getBottomY = function (){
		return bottom_c.attr("cy");
	}
	this.getLeftX = function (){
		return left_c.attr("cx");
	}
	this.getLeftY = function (){
		return left_c.attr("cy");
	}
	this.getRightX = function (){
		return right_c.attr("cx");
	}
	this.getRightY = function (){
		return right_c.attr("cy");
	}
	this.getOutTopX = function (){
		return outTop_c.attr("cx");
	}
	this.getOutTopY = function (){
		return outTop_c.attr("cy");
	}
//	this.getOutBottomX = function (){
//		return outBottom_c.attr("cx");
//	}
//	this.getOutBottomY = function (){
//		return outBottom_c.attr("cy");
//	}
	this.getDAStr = function (){
		return dAStr;
	}
	
	return this;
}

//function diyBranch(paper,initX,initY,lineXLength,lineYLength,branchNum,textOffsetY,id,top_x,top_y,bottom_x,bottom_y,left_x,left_y,right_x,right_y,text_contents,dataArrayStr,outTop_x,outTop_y,outBottom_x,outBottom_y){
//	this.type = "d_branch_line";
//	this.pageType="branch";
//	if(!branchNum){
//		branchNum = 2;
//	}
//	if(!textOffsetY){
//		textOffsetY = 20;
//	}
//	/**dataArrayStr保存从编辑传来的object数组转化的原始字符串*/
//	var dAStr;
//	if(dataArrayStr){
//		dAStr=dataArrayStr;
//	}
//	var defaultId;
//	if(id){
//		defaultId = id;
//	}else{
//		defaultId = "diy_branch_"+new Date().getTime();
//	}
//	var selfObj = this;
//	/**是否自己被ProcedureContain包含*/
//	this.isProcedureContained = false;
//	//
//	this.relationArray=[null,null];
//	/**用来存放branch中的矩形,用于save持久化的时候*/
//	this.containsArray = new Array(branchNum);
//	for(var i=0;i<this.containsArray.length;i++){
//		this.containsArray[i] = new Array();
//	}
//	var branchLineObj;
//	var branchLineObjPrevent;
//	var branchLineObjAdjust;
//	var judgeObj1;
//	var judgeObj2;
////	var branchLineObjs = [];//存放分支的array
//	var branchTextObjs = [];//存放分支text的array
//	var ponint_r = 5;
//	var isAdjusting = false;
//	var top_c;
//	var bottom_c;
//	var left_c;
//	var right_c;
//	var outTop_c;
//	var outBottom_c;
//	var bottom_arrow;
//	/**用来存放relation点的数组(0:上 1:下 2-n:branch分支 index=n-2)(主要用于联动)*/
//	var rcArray = new Array(branchNum-0+2); 
//	var outTopLength = 50;
//	var outBottomLength = 50;
//	
//	var init = function (){
//		var branchLine = ["M",initX,initY,"L",initX,initY+outTopLength,"M",initX-lineXLength/2,initY+outTopLength,"L",initX-0+lineXLength/2,initY+outTopLength ,"M",initX-lineXLength/2,initY+outTopLength+lineYLength,"L",initX-0+lineXLength/2,initY+outTopLength+lineYLength,"M",initX,initY+50+lineYLength,"L",initX,initY+outTopLength+lineYLength+outBottomLength].join(",");
//		var gap = lineXLength/(branchNum-1);
//		for(var i=0;i<branchNum;i++){
//			var ts = ["M",(initX-lineXLength/2)+(i*gap),initY+outTopLength,"L",(initX-lineXLength/2)+(i*gap),initY+outTopLength+lineYLength].join(",");
//			branchLine+=ts;
////			var tmpLineObj = paper.path(ts);
////			tmpLineObj.attr({"stroke-width":"1","shape-rendering":"crispEdges","fill":"none"});
////			branchLineObjs.push(tmpLineObj);
//			var tmpTextObj = paper.text((initX-lineXLength/2)+(i*gap),initY-0+50-0+textOffsetY,"").attr({"font-size":14}).data("id",defaultId+"["+(i+1)+"]");
//			if(text_contents && text_contents[i]){
//				tmpTextObj.attr({"text":text_contents[i]});
//			}
//			branchTextObjs.push(tmpTextObj);
//		}
//		branchLineObj=paper.path(branchLine);
//		branchLineObj.cusType = "branch";
//		branchLineObjPrevent=paper.path(branchLine);
//		branchLineObj.attr({"stroke-width":"1","shape-rendering":"crispEdges","fill":"none"});
//		branchLineObjPrevent.attr({"opacity": .1,stroke: "white","stroke-width":"9","fill":"none","cursor": "move"});
//		branchLineObjAdjust = paper.path(branchLine);
//		branchLineObjAdjust.attr({stroke: "#99ccff","stroke-width":"1","shape-rendering":"crispEdges","stroke-dasharray": "--"});
//		judgeObj1 = paper.path(["M",initX,initY+outTopLength-10,"L",initX-10,initY+outTopLength,"L",initX,initY+outTopLength+10,"L",initX+10,initY+outTopLength,"Z"].join(","));
//		judgeObj1.attr({fill:"#000000",stroke: "#000000","stroke-width":"1","shape-rendering":"crispEdges","cursor": "move"});
//		judgeObj2 = paper.path(["M",initX,initY+outTopLength+lineYLength-10,"L",initX-10,initY+outTopLength+lineYLength,"L",initX,initY+outTopLength+lineYLength+10,"L",initX+10,initY+outTopLength+lineYLength,"Z"].join(","));
//		judgeObj2.attr({fill:"#000000",stroke: "#000000","stroke-width":"1","shape-rendering":"crispEdges","cursor": "move"});
//		bottom_arrow = paper.path(["M",initX-6,initY+outTopLength+lineYLength+outBottomLength-20,"L",initX+6,initY+outBottomLength+lineYLength+outTopLength-20,"L",initX,initY+outTopLength+lineYLength+outBottomLength].join(","));
//		bottom_arrow.attr({fill:"#000000",stroke: "#000000","stroke-width":"1","shape-rendering":"crispEdges"});
////		branchLineObj.toBack();
////		branchLineObjPrevent.toBack();
////		branchLineObjAdjust.toBack();
////		judgeObj1.toBack();
////		judgeObj2.toBack();
////		bottom_arrow.toBack();
//	}
//	
//	if(id){
////		top_x,top_y,bottom_x,bottom_y,left_x,left_y,right_x,right_y,text_contents,outTop_x,outTop_y,outBottom_x,outBottom_y
//		var branchLine = ["M",top_x,top_y,"L",outTop_x,outTop_y,"M",left_x,top_y,"L", right_x,top_y ,"M",left_x,bottom_y,"L",right_x,bottom_y,"M",bottom_x,bottom_y,"L",outBottom_x,outBottom_y].join(",");
//		var gap = (right_x-left_x)/(branchNum-1);
//		var tcArray = text_contents.split("&split");
//		for(var i=0;i<branchNum;i++){
//			var ts = ["M",(left_x-0)+(i*gap),top_y,"L",(left_x-0)+(i*gap),bottom_y].join(",");
//			branchLine+=ts;
//			if(tcArray[i]=="empty"){
//				tcArray[i] = "";
//			}
//			var tmpTextObj = paper.text((left_x-0)+(i*gap),top_y-0+textOffsetY,tcArray[i]).data("id",defaultId+"["+(i+1)+"]");
//			branchTextObjs.push(tmpTextObj);
//		}
//		branchLineObj=paper.path(branchLine);
//		branchLineObj.cusType = "branch";
//		branchLineObjPrevent=paper.path(branchLine);
//		branchLineObj.attr({"stroke-width":"1","shape-rendering":"crispEdges","fill":"none"});
//		branchLineObjPrevent.attr({"opacity": .1,stroke: "white","stroke-width":"9","fill":"none","cursor": "move"});
//		branchLineObjAdjust = paper.path(branchLine);
//		branchLineObjAdjust.attr({stroke: "#99ccff","stroke-width":"1","shape-rendering":"crispEdges","stroke-dasharray": "--"});
//		judgeObj1 = paper.path(["M",top_x,top_y-10,"L",top_x-10,top_y,"L",top_x,top_y-0+10,"L",top_x-0+10,top_y,"Z"].join(","));
//		judgeObj1.attr({fill:"#000000",stroke: "#000000","stroke-width":"1","shape-rendering":"crispEdges","cursor": "move"});
//		judgeObj2 = paper.path(["M",bottom_x,bottom_y-10,"L",bottom_x-10,bottom_y,"L",bottom_x,bottom_y-0+10,"L",bottom_x-0+10,bottom_y,"Z"].join(","));
//		judgeObj2.attr({fill:"#000000",stroke: "#000000","stroke-width":"1","shape-rendering":"crispEdges","cursor": "move"});
//		bottom_arrow = paper.path(["M",outBottom_x-6,outBottom_y-20,"L",outBottom_x+6,outBottom_y-20,"L",outBottom_x,outBottom_y].join(","));
//		bottom_arrow.attr({fill:"#000000",stroke: "#000000","stroke-width":"1","shape-rendering":"crispEdges"});
////		branchLineObj.toBack();
////		branchLineObjPrevent.toBack();
////		branchLineObjAdjust.toBack();
////		judgeObj1.toBack();
////		judgeObj2.toBack();
////		bottom_arrow.toBack();
//	}else{
//		init();
//	}
//	
//	this.eveConnectProcedureContain = function (){
//		var minX = left_c.attr("cx");
//		var maxX = right_c.attr("cx");
//		var minY = outTop_c.attr("cy");
//		var maxY = outBottom_c.attr("cy");
//		eve("custom.connectProcedureContain",'',this,defaultId,minX,maxX,minY,maxY);
//	}
//	
//	var connectProcedureContainTriggerFunction  = function (){
//		var minX = left_c.attr("cx");
//		var maxX = right_c.attr("cx");
//		var minY = outTop_c.attr("cy");
//		var maxY = outBottom_c.attr("cy");
//		eve("custom.connectProcedureContain",'',selfObj,defaultId,minX,maxX,minY,maxY);
//	}
//	
//	/**注册事件(触发各个组件自己再去触发ProcedureContain的关系判断事件)*/
//	eve.on("custom.connectProcedureContainTrigger",connectProcedureContainTriggerFunction);
//	
//	this.line_move_branch = function (dx, dy,distX ,distY,type) {
//		if(dx == null ){
//			if(type=="bottom"){
//				dx = distX - outTop_c.attr("cx");
//				dy = distY - outTop_c.attr("cy");
//			}else{
//				dx = distX - outBottom_c.attr("cx");
//				dy = distY - outBottom_c.attr("cy");
//			}
//		}
//    	var newPath = ["M",outTop_c.attr("cx")+dx,outTop_c.attr("cy")+dy,"L",top_c.attr("cx")+dx,top_c.attr("cy")+dy,"M",left_c.attr("cx")+dx,top_c.attr("cy")+dy,"L",right_c.attr("cx")+dx,top_c.attr("cy")+dy,"M",left_c.attr("cx")+dx,bottom_c.attr("cy")+dy,"L",right_c.attr("cx")+dx,bottom_c.attr("cy")+dy,"M",bottom_c.attr("cx")+dx,bottom_c.attr("cy")+dy,"L",outBottom_c.attr("cx")+dx,outBottom_c.attr("cy")+dy].join(",");
//        var gap = (right_c.attr("cx")-left_c.attr("cx"))/(branchNum-1);
//        for(var i=0;i<branchNum;i++){
//			var ts = ["M",left_c.attr("cx")+dx+(i*gap),top_c.attr("cy")-0+dy,"L",left_c.attr("cx")+dx+(i*gap),bottom_c.attr("cy")-0+dy].join(",");
//			newPath+=ts;
//		}
//        branchLineObj.attr({path:newPath});
//		branchLineObjAdjust.attr({path:newPath});
//		branchLineObjPrevent.attr({path:newPath});
//		judgeObj1.attr({path:["M",top_c.attr("cx")+dx,top_c.attr("cy")+dy-10,"L",top_c.attr("cx")-10+dx,top_c.attr("cy")+dy,"L",top_c.attr("cx")+dx,top_c.attr("cy")+dy+10,"L",top_c.attr("cx")+dx+10,top_c.attr("cy")+dy].join(",")});
//		judgeObj2.attr({path:["M",bottom_c.attr("cx")+dx,bottom_c.attr("cy")+dy-10,"L",bottom_c.attr("cx")-10+dx,bottom_c.attr("cy")+dy,"L",bottom_c.attr("cx")+dx,bottom_c.attr("cy")+dy+10,"L",bottom_c.attr("cx")+dx+10,bottom_c.attr("cy")+dy].join(",")});
//		top_c.attr({cx:top_c.attr("cx")+dx,cy:top_c.attr("cy")+dy});
//		left_c.attr({cx:left_c.attr("cx")+dx,cy:left_c.attr("cy")+dy});
//		right_c.attr({cx:right_c.attr("cx")+dx,cy:right_c.attr("cy")+dy});
//		bottom_c.attr({cx:bottom_c.attr("cx")+dx,cy:bottom_c.attr("cy")+dy});
//        outBottom_c.attr({cx:outBottom_c.attr("cx")+dx,cy:outBottom_c.attr("cy")+dy});
//		bottom_arrow.attr({path:["M",outBottom_c.attr("cx")-6,outBottom_c.attr("cy")-20,"L",outBottom_c.attr("cx")-0+6,outBottom_c.attr("cy")-20,"L",outBottom_c.attr("cx"),outBottom_c.attr("cy")].join(",")});
//        outTop_c.attr({cx:outTop_c.attr("cx")+dx,cy:outTop_c.attr("cy")+dy});
//        for(var i=0;i<branchTextObjs.length;i++){
//        	branchTextObjs[i].attr({"x":left_c.attr("cx")+(i*gap),"y":top_c.attr("cy")+textOffsetY});
//        }
//        linkageAdjust();
//        this.eveConnectProcedureContain();
//        paper.safari();
//    }
//    
//    this.linkageByProcedureContainRect = function (dx, dy) {
//    	var olx = left_c.attr("cx");
//        var oly = left_c.attr("cy");
//        var orx = right_c.attr("cx");
//        var ory = right_c.attr("cy");
//        var otx = top_c.attr("cx");
//        var oty = top_c.attr("cy");
//        var obx = bottom_c.attr("cx");
//        var oby = bottom_c.attr("cy");
//        var ootx = outTop_c.attr("cx");
//        var ooty = outTop_c.attr("cy");
//        var oobx = outBottom_c.attr("cx");
//        var ooby = outBottom_c.attr("cy");
//    	var newPath = ["M",ootx+dx,ooty+dy,"L",otx+dx,oty+dy,"M",olx+dx,oty+dy,"L",orx+dx,oty+dy,"M",olx+dx,oby+dy,"L",orx+dx,oby+dy,"M",obx+dx,oby+dy,"L",oobx+dx,ooby+dy].join(",");
//        var gap = (orx-olx)/(branchNum-1);
//        for(var i=0;i<branchNum;i++){
//			var ts = ["M",olx+dx+(i*gap),oty-0+dy,"L",olx+dx+(i*gap),oby-0+dy].join(",");
//			newPath+=ts;
//		}
//        branchLineObj.attr({path:newPath});
//		branchLineObjAdjust.attr({path:newPath});
//		branchLineObjPrevent.attr({path:newPath});
//		judgeObj1.attr({path:["M",otx+dx,oty+dy-10,"L",otx-10+dx,oty+dy,"L",otx+dx,oty+dy+10,"L",otx+dx+10,oty+dy].join(",")});
//		judgeObj2.attr({path:["M",obx+dx,oby+dy-10,"L",obx-10+dx,oby+dy,"L",obx+dx,oby+dy+10,"L",obx+dx+10,oby+dy].join(",")});
//		top_c.attr({cx:otx+dx,cy:oty+dy});
//		left_c.attr({cx:olx+dx,cy:oly+dy});
//		right_c.attr({cx:orx+dx,cy:ory+dy});
//		bottom_c.attr({cx:obx+dx,cy:oby+dy});
//		outBottom_c.attr({cx:oobx+dx,cy:ooby+dy});
//		outTop_c.attr({cx:ootx+dx,cy:ooty+dy});
//		bottom_arrow.attr({path:["M",oobx-6+dx,ooby+dy-20,"L",oobx-0+dx+6,ooby-0+dy-20,"L",oobx+dx,ooby-0+dy].join(",")});
//        for(var i=0;i<branchTextObjs.length;i++){
//        	branchTextObjs[i].attr({"x":olx+dx+(i*gap),"y":oty+dy+textOffsetY});
//        }
//        paper.safari();
//    }
//
//	var line_dragger = function () {
//        this.olx = left_c.attr("cx");
//        this.oly = left_c.attr("cy");
//        this.orx = right_c.attr("cx");
//        this.ory = right_c.attr("cy");
//        this.otx = top_c.attr("cx");
//        this.oty = top_c.attr("cy");
//        this.obx = bottom_c.attr("cx");
//        this.oby = bottom_c.attr("cy");
//        this.ootx = outTop_c.attr("cx");
//        this.ooty = outTop_c.attr("cy");
//        this.oobx = outBottom_c.attr("cx");
//        this.ooby = outBottom_c.attr("cy");
////        this.animate({"fill-opacity": .3}, 500);
//    },/**onmove*/
//        line_move = function (dx, dy) {
//        	var newPath = ["M",this.ootx+dx,this.ooty+dy,"L",this.otx+dx,this.oty+dy,"M",this.olx+dx,this.oty+dy,"L",this.orx+dx,this.oty+dy,"M",this.olx+dx,this.oby+dy,"L",this.orx+dx,this.oby+dy,"M",this.obx+dx,this.oby+dy,"L",this.oobx+dx,this.ooby+dy].join(",");
//            var gap = (this.orx-this.olx)/(branchNum-1);
//	        for(var i=0;i<branchNum;i++){
//				var ts = ["M",this.olx+dx+(i*gap),this.oty-0+dy,"L",this.olx+dx+(i*gap),this.oby-0+dy].join(",");
//				newPath+=ts;
//			}
//            branchLineObj.attr({path:newPath});
//    		branchLineObjAdjust.attr({path:newPath});
//    		branchLineObjPrevent.attr({path:newPath});
//    		judgeObj1.attr({path:["M",this.otx+dx,this.oty+dy-10,"L",this.otx-10+dx,this.oty+dy,"L",this.otx+dx,this.oty+dy+10,"L",this.otx+dx+10,this.oty+dy].join(",")});
//    		judgeObj2.attr({path:["M",this.obx+dx,this.oby+dy-10,"L",this.obx-10+dx,this.oby+dy,"L",this.obx+dx,this.oby+dy+10,"L",this.obx+dx+10,this.oby+dy].join(",")});
//    		top_c.attr({cx:this.otx+dx,cy:this.oty+dy});
//    		left_c.attr({cx:this.olx+dx,cy:this.oly+dy});
//    		right_c.attr({cx:this.orx+dx,cy:this.ory+dy});
//    		bottom_c.attr({cx:this.obx+dx,cy:this.oby+dy});
//    		outBottom_c.attr({cx:this.oobx+dx,cy:this.ooby+dy});
//    		outTop_c.attr({cx:this.ootx+dx,cy:this.ooty+dy});
//    		bottom_arrow.attr({path:["M",this.oobx-6+dx,this.ooby+dy-20,"L",this.oobx-0+dx+6,this.ooby-0+dy-20,"L",this.oobx+dx,this.ooby-0+dy].join(",")});
//            for(var i=0;i<branchTextObjs.length;i++){
//            	branchTextObjs[i].attr({"x":this.olx+dx+(i*gap),"y":this.oty+dy+textOffsetY});
//            }
//            linkageAdjust();
//            paper.safari();
//        },/**onend*/
//        line_up = function () {
//        	selfObj.eveConnectProcedureContain();
//        	eve("custom.someoneAdjusting",'',branchLineObj);
////            this.animate({"fill-opacity": 1}, 500);
//        };
//	branchLineObjPrevent.drag(line_move, line_dragger,line_up);//onmove, onstart, onend
//	judgeObj1.drag(line_move, line_dragger,line_up);//onmove, onstart, onend
//	judgeObj2.drag(line_move, line_dragger,line_up);//onmove, onstart, onend
//	
//	/**调整定位点的拖拉事件处理*/
//	var point_dragger = function () {
//        this.ox = this.attr("cx");
//        this.oy = this.attr("cy");
//        this.oly = left_c.attr("cy");
//        this.olx = left_c.attr("cx");
//        this.ory = right_c.attr("cy");
//        this.orx = right_c.attr("cx");
//        this.ooty = outTop_c.attr("cy");
//        this.ootx = outTop_c.attr("cx");
//        this.ooby = outBottom_c.attr("cy");
//        this.oobx = outBottom_c.attr("cx");
//        this.animate({"fill-opacity": .3}, 500);
//        this.tmpOotyDy = 0;
//        this.tmpOobyDy = 0;
//    },/**onmove*/
//        point_move = function (dx, dy) {
//        	if(this.data("id")==defaultId+"top_c"){
//        		var newPath = ["M",outTop_c.attr("cx"),outTop_c.attr("cy"),"L",this.ox,this.oy-0+dy,"M",left_c.attr("cx"),this.oy-0+dy,"L",right_c.attr("cx"),this.oy-0+dy ,"M",left_c.attr("cx"),bottom_c.attr("cy"),"L",right_c.attr("cx"),bottom_c.attr("cy"),"M",bottom_c.attr("cx"),bottom_c.attr("cy"),"L",outBottom_c.attr("cx"),outBottom_c.attr("cy")].join(",");
//		        var gap = (right_c.attr("cx")-left_c.attr("cx"))/(branchNum-1);
//		        for(var i=0;i<branchNum;i++){
//					var ts = ["M",left_c.attr("cx")+(i*gap),this.oy-0+dy,"L",left_c.attr("cx")+(i*gap),bottom_c.attr("cy")].join(",");
//					newPath+=ts;
//				}
//        		branchLineObj.attr({path:newPath});
//        		branchLineObjAdjust.attr({path:newPath});
//        		branchLineObjPrevent.attr({path:newPath});
//        		top_c.attr({cy:this.oy+dy});
//        		left_c.attr({cy:this.oly+dy/2});
//        		right_c.attr({cy:this.ory+dy/2});
//        		judgeObj1.attr({path:["M",this.ox,this.oy-10+dy,"L",this.ox-10,this.oy+dy,"L",this.ox,this.oy-0+10+dy,"L",this.ox-0+10,this.oy+dy].join(",")});
//        		for(var i=0;i<branchTextObjs.length;i++){
//	            	branchTextObjs[i].attr({"y":this.oy+dy+textOffsetY});
//	            }
//	            linkageAdjust();
//        	}else if(this.data("id")==defaultId+"bottom_c"){
//        		var newPath = ["M",outTop_c.attr("cx"),outTop_c.attr("cy"),"L",top_c.attr("cx"),top_c.attr("cy"),"M",left_c.attr("cx"),top_c.attr("cy"),"L",right_c.attr("cx"),top_c.attr("cy"),"M",left_c.attr("cx"),this.oy-0+dy,"L",right_c.attr("cx"),this.oy-0+dy,"M",this.ox,this.oy-0+dy,"L",outBottom_c.attr("cx"),outBottom_c.attr("cy")].join(",");
//		        var gap = (right_c.attr("cx")-left_c.attr("cx"))/(branchNum-1);
//		        for(var i=0;i<branchNum;i++){
//					var ts = ["M",left_c.attr("cx")+(i*gap),top_c.attr("cy"),"L",left_c.attr("cx")+(i*gap),this.oy-0+dy].join(",");
//					newPath+=ts;
//				}
//        		branchLineObj.attr({path:newPath});
//        		branchLineObjAdjust.attr({path:newPath});
//        		branchLineObjPrevent.attr({path:newPath});
//        		bottom_c.attr({cy:this.oy+dy});
//        		left_c.attr({cy:this.oly+dy/2});
//        		right_c.attr({cy:this.ory+dy/2});
//        		judgeObj2.attr({path:["M",this.ox,this.oy-10+dy,"L",this.ox-10,this.oy+dy,"L",this.ox,this.oy-0+10+dy,"L",this.ox-0+10,this.oy+dy].join(",")});
//        		//bottom_arrow.attr({path:["M",outBottom_c.attr("cx")-6,outBottom_c.attr("cy")-20,"L",outBottom_c.attr("cx")+6,outBottom_c.attr("cy")-20,"L",outBottom_c.attr("cx"),outBottom_c.attr("cy")].join(",")});
//        		linkageAdjust();
//        	}else if(this.data("id")==defaultId+"left_c"){
//        		var newPath = ["M",outTop_c.attr("cx"),outTop_c.attr("cy"),"L",top_c.attr("cx"),top_c.attr("cy"),"M",this.ox+dx,top_c.attr("cy"),"L",this.orx-dx,top_c.attr("cy"),"M",this.ox+dx,bottom_c.attr("cy"),"L",this.orx-dx,bottom_c.attr("cy"),"M",bottom_c.attr("cx"),bottom_c.attr("cy"),"L",outBottom_c.attr("cx"),outBottom_c.attr("cy")].join(",");
//		        var gap = (this.orx-this.ox-dx*2)/(branchNum-1);
//		        for(var i=0;i<branchNum;i++){
//					var ts = ["M",this.ox+dx+(i*gap),top_c.attr("cy"),"L",this.ox+dx+(i*gap),bottom_c.attr("cy")].join(",");
//					newPath+=ts;
//				}
//        		branchLineObj.attr({path:newPath});
//        		branchLineObjAdjust.attr({path:newPath});
//        		branchLineObjPrevent.attr({path:newPath});
//        		left_c.attr({cx:this.ox+dx});
//        		right_c.attr({cx:this.orx-dx});
//        		for(var i=0;i<branchTextObjs.length;i++){
//	            	branchTextObjs[i].attr({"x":this.ox+dx+(i*gap)});
//	            }
//	            linkageAdjust();
//        	}else if(this.data("id")==defaultId+"right_c"){
//        		var newPath = ["M",outTop_c.attr("cx"),outTop_c.attr("cy"),"L",top_c.attr("cx"),top_c.attr("cy"),"M",this.olx-dx,top_c.attr("cy"),"L",this.ox+dx,top_c.attr("cy"),"M",this.olx-dx,bottom_c.attr("cy"),"L",this.ox+dx,bottom_c.attr("cy"),"M",bottom_c.attr("cx"),bottom_c.attr("cy"),"L",outBottom_c.attr("cx"),outBottom_c.attr("cy")].join(",");
//		        var gap = (this.ox-this.olx+dx*2)/(branchNum-1);
//		        for(var i=0;i<branchNum;i++){
//					var ts = ["M",this.olx-dx+(i*gap),top_c.attr("cy"),"L",this.olx-dx+(i*gap),bottom_c.attr("cy")].join(",");
//					newPath+=ts;
//				}
//        		branchLineObj.attr({path:newPath});
//        		branchLineObjAdjust.attr({path:newPath});
//        		branchLineObjPrevent.attr({path:newPath});
//        		left_c.attr({cx:this.olx-dx});
//        		right_c.attr({cx:this.ox+dx});
//        		for(var i=0;i<branchTextObjs.length;i++){
//	            	branchTextObjs[i].attr({"x":this.olx-dx+(i*gap)});
//	            }
//	            linkageAdjust();
//        	}else if(this.data("id")==defaultId+"outTop_c"){
//        		if(this.ooty+dy < top_c.attr("cy")){
//        			outTop_c.attr({cy:this.ooty+dy});
//        			
//        			var newPath = ["M",outTop_c.attr("cx"),this.ooty+dy,"L",top_c.attr("cx"),top_c.attr("cy"),"M",left_c.attr("cx"),top_c.attr("cy"),"L",right_c.attr("cx"),top_c.attr("cy") ,"M",left_c.attr("cx"),bottom_c.attr("cy"),"L",right_c.attr("cx"),bottom_c.attr("cy"),"M",bottom_c.attr("cx"),bottom_c.attr("cy"),"L",outBottom_c.attr("cx"),outBottom_c.attr("cy")].join(",");
//			        var gap = (right_c.attr("cx")-left_c.attr("cx"))/(branchNum-1);
//			        for(var i=0;i<branchNum;i++){
//						var ts = ["M",left_c.attr("cx")+(i*gap),top_c.attr("cy"),"L",left_c.attr("cx")+(i*gap),bottom_c.attr("cy")].join(",");
//						newPath+=ts;
//					}
//	        		branchLineObj.attr({path:newPath});
//	        		branchLineObjAdjust.attr({path:newPath});
//	        		branchLineObjPrevent.attr({path:newPath});
//	        		
////	        		if(rcArray[0]!=null){
////	        			//如果已经有关系，则判断是否解除和矩形的关系
////	        			if(dy>0 || dy>this.tmpOotyDy){
////		        			if(rcArray[0][0]!=null){
////		        				var triObj = rcArray[0][0];
////		        				selfObj.removeRelationParent(triObj);
////		        				triObj.conMarkHide("bottom");
////		        				rcArray[0]=null;
////		        			}
////		        		}else{
////		        			linkageAdjust();
////		        		}
////		        		this.tmpOotyDy = dy;
////	        		}else{
////	        			//没有关系则判断是否添加和矩形的关系
////	        			eve("custom.connectionHandlerByBranch");
////	        		}
//        		}
//        	}else if(this.data("id")==defaultId+"outBottom_c"){
//        		if(this.ooby+dy-20 > bottom_c.attr("cy")){
//        			outBottom_c.attr({cy:this.ooby+dy});
//        			
//        			var newPath = ["M",outTop_c.attr("cx"),outTop_c.attr("cy"),"L",top_c.attr("cx"),top_c.attr("cy"),"M",left_c.attr("cx"),top_c.attr("cy"),"L",right_c.attr("cx"),top_c.attr("cy") ,"M",left_c.attr("cx"),bottom_c.attr("cy"),"L",right_c.attr("cx"),bottom_c.attr("cy"),"M",bottom_c.attr("cx"),bottom_c.attr("cy"),"L",outBottom_c.attr("cx"),this.ooby+dy].join(",");
//			        var gap = (right_c.attr("cx")-left_c.attr("cx"))/(branchNum-1);
//			        for(var i=0;i<branchNum;i++){
//						var ts = ["M",left_c.attr("cx")+(i*gap),top_c.attr("cy"),"L",left_c.attr("cx")+(i*gap),bottom_c.attr("cy")].join(",");
//						newPath+=ts;
//					}
//	        		branchLineObj.attr({path:newPath});
//	        		branchLineObjAdjust.attr({path:newPath});
//	        		branchLineObjPrevent.attr({path:newPath});
//        			bottom_arrow.attr({path:["M",outBottom_c.attr("cx")-6,this.ooby-20+dy,"L",outBottom_c.attr("cx")+6,this.ooby-20+dy,"L",outBottom_c.attr("cx"),this.ooby+dy].join(",")});
//        			
////        			if(rcArray[1]!=null){
////	        			if(dy<0 || dy<this.tmpOobyDy){
////		        			if(rcArray[1][0]!=null){
////		        				var triObj = rcArray[1][0];
////		        				selfObj.removeRelationChild(triObj);
////		        				triObj.conMarkHide("top");
////		        				rcArray[1]=null;
////		        			}
////		        		}else{
////		        			linkageAdjust();
////		        		}
////		        		this.tmpOobyDy = dy;
////        			}else{
////        				//没有关系则判断是否添加和矩形的关系
////	        			eve("custom.connectionHandlerByBranch");
////        			}
//        		}
//        	}
//            paper.safari();
//        },/**onend*/
//        point_up = function () {
//        	if(this.data("id")==defaultId+"outTop_c"){
//        		if(rcArray[0]!=null){
//        			//如果已经有关系，则判断是否解除和矩形的关系
//        			if(rcArray[0][0]!=null){
//        				var triObj = rcArray[0][0];
//        				//判断outTop是否在矩形triObj内
//        				var ot_y = outTop_c.attr("cy")
//        				if(ot_y>=triObj.getRightBottom_y() && ot_y<=triObj.getLeftTop_y()){
//        					//在矩形内
//        					linkageAdjust();
//        				}else{
//        					//取消关系
//        					selfObj.removeRelationParent(triObj);
//	        				triObj.conMarkHide("bottom");
//	        				rcArray[0]=null;
//        				}
//        			}
//        		}else{
//        			//没有关系则判断是否添加和矩形的关系
//        			eve("custom.connectionHandlerByBranch");
//        		}
//        	}else if(this.data("id")==defaultId+"outBottom_c"){
//        		if(rcArray[1]!=null){
//        			if(rcArray[1][0]!=null){
//        				var triObj = rcArray[1][0];
//        				var ob_y = outBottom_c.attr("cy");
//        				if(ob_y>=triObj.getRightBottom_y() && ob_y<=triObj.getLeftTop_y()){
//        					//在矩形内
//        					linkageAdjust();
//        				}else{
//        					//取消关系
//        					selfObj.removeRelationChild(triObj);
//	        				triObj.conMarkHide("top");
//	        				rcArray[1]=null;
//        				}
//        			}
//        		}else{
//        			//没有关系则判断是否添加和矩形的关系
//        			eve("custom.connectionHandlerByBranch");
//        		}
//        	}
//        	selfObj.eveConnectProcedureContain();
//        	eve("custom.someoneAdjusting",'',branchLineObj);
//            this.animate({"fill-opacity": .5}, 500);
//        };
//        
//	/**生成4个调整点*/
//	var drawNAjustPoint = function (paper,x,y,xl,yl,r, point_dragger, point_move, point_up){
//		top_c = paper.circle(x,y+outTopLength,r).data("id",defaultId+"top_c");
//		top_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "n-resize"});
//		top_c.drag(point_move, point_dragger, point_up);
//		bottom_c = paper.circle(x,y+outTopLength+yl,r).data("id",defaultId+"bottom_c");
//		bottom_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "n-resize"});
//		bottom_c.drag(point_move, point_dragger, point_up);
//		left_c = paper.circle(x-xl/2,y+outTopLength+yl/2,r).data("id",defaultId+"left_c");
//		left_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "e-resize"});
//		left_c.drag(point_move, point_dragger, point_up);
//		right_c = paper.circle(x+xl/2,y+outTopLength+yl/2,r).data("id",defaultId+"right_c");
//		right_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "e-resize"});
//		right_c.drag(point_move, point_dragger, point_up);
//		outTop_c = paper.circle(x,y,r).data("id",defaultId+"outTop_c");
//		outTop_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "n-resize"});
//		outTop_c.drag(point_move, point_dragger, point_up);
//		outBottom_c = paper.circle(x,y+outTopLength+yl+outBottomLength,r).data("id",defaultId+"outBottom_c");
//		outBottom_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "n-resize"});
//		outBottom_c.drag(point_move, point_dragger, point_up);
//	}
//	var drawNAjustPointLoad = function (paper,top_x,top_y,bottom_x,bottom_y,left_x,left_y,right_x,right_y,r,outTop_x,outTop_y,outBottom_x,outBottom_y, point_dragger, point_move, point_up){
//		top_c = paper.circle(top_x,top_y,r).data("id",defaultId+"top_c");
//		top_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "n-resize"});
//		top_c.drag(point_move, point_dragger, point_up);
//		bottom_c = paper.circle(bottom_x,bottom_y,r).data("id",defaultId+"bottom_c");
//		bottom_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "n-resize"});
//		bottom_c.drag(point_move, point_dragger, point_up);
//		left_c = paper.circle(left_x,left_y,r).data("id",defaultId+"left_c");
//		left_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "e-resize"});
//		left_c.drag(point_move, point_dragger, point_up);
//		right_c = paper.circle(right_x,right_y,r).data("id",defaultId+"right_c");
//		right_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "e-resize"});
//		right_c.drag(point_move, point_dragger, point_up);
//		
//		outTop_c = paper.circle(outTop_x,outTop_y,r).data("id",defaultId+"outTop_c");
//		outTop_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "n-resize"});
//		outTop_c.drag(point_move, point_dragger, point_up);
//		outBottom_c = paper.circle(outBottom_x,outBottom_y,r).data("id",defaultId+"outBottom_c");
//		outBottom_c.attr({stroke: "#0088cf",fill: "#99ccff", "fill-opacity": .5, cursor: "n-resize"});
//		outBottom_c.drag(point_move, point_dragger, point_up);
//	}
//	if(id){
//		drawNAjustPointLoad(paper,top_x,top_y,bottom_x,bottom_y,left_x,left_y,right_x,right_y,ponint_r,outTop_x,outTop_y,outBottom_x,outBottom_y, point_dragger, point_move, point_up);
//	}else{
//		drawNAjustPoint(paper,initX,initY,lineXLength,lineYLength,ponint_r, point_dragger, point_move, point_up);
//	}
//	var hideAdjustLine = function(){
//		branchLineObjAdjust.hide();
//		top_c.hide();
//		bottom_c.hide();
//		left_c.hide();
//		right_c.hide();
//		outTop_c.hide();
//		outBottom_c.hide();
//		isAdjusting = false;
//	}
//	var showAdjustLine = function(){
//		branchLineObjAdjust.show();
//		left_c.show();
//		right_c.show();
//		bottom_c.show();
//		top_c.show();
//		outTop_c.show();
//		outBottom_c.show();
//		isAdjusting=true;
//	}
//	hideAdjustLine();
//	
//	branchLineObjPrevent.click(function (){
//		eve("custom.someoneAdjusting",'',branchLineObj);//触发事件
//	});
//	
//	this.update = function (dataArray){
//		var results = resolveBranch(dataArray);
//		dAStr = results[2];
//		for(var di = 0;di<branchNum;di++){
//			var valValue = "";
//			if(results[1][di]){
//				valValue = results[1][di];
//			}
//			branchTextObjs[di].attr({"text":valValue});
//		}
//	}
//	
//	var someoneAdjustingFunction = function (triggerEle){
//		if(triggerEle == branchLineObj){
//			//是自己的元素触发的
//			if(!isAdjusting){
//				showAdjustLine();
//				toEdit(defaultId,selfObj.pageType,dAStr);
//				//remove
//				//各个组件自己的特殊化操作
////				document.getElementById("branchDynContain").innerHTML ="";
////				var inputContain = document.getElementById("branchDynContain");
////				for(var bi = 0;bi<branchNum;bi++){
////					var add_input=document.createElement("input")
////				    add_input.id="branchText"+(bi+1);
////				    add_input.tmp_bi=bi;
////				    add_input.value = branchTextObjs[bi].attr("text");
////				    inputContain.appendChild(add_input);
////				    add_input.onchange = function (){
////						branchTextObjs[this.tmp_bi].attr({"text":this.value});
////					}
////				}
//			}
//			eve.stop();
//		}else{
//			//是其他的元素触发的
//			if(isAdjusting){
//				hideAdjustLine();
//			}
//		}
//	}
//	//注册事件
//	eve.on("custom.someoneAdjusting",someoneAdjustingFunction);
//	
//	/**判断其他组件和branch的关系*/
//	var connectionHandlerFunction = function (triggerObj,real_pic,minRectX,minRectY,maxRectX,maxRectY){
//		var top_x_branch = outTop_c.attr("cx");
//		var top_y_branch = outTop_c.attr("cy");
//		var bottom_x_branch = outBottom_c.attr("cx");
//		var bottom_y_branch = outBottom_c.attr("cy");
//		var left_x_branch = left_c.attr("cx");
//		var y_min_branch = top_c.attr("cy");
//		var y_max_branch = bottom_c.attr("cy");
//		var right_x_branch = right_c.attr("cx");
//		
//		if(top_x_branch>=minRectX && top_x_branch<= maxRectX && top_y_branch>=minRectY && top_y_branch<= maxRectY ){
////			console.log("连接到branch的最顶点");
//			//连接到branch的最顶点   rect 的 dx,dy
//			triggerObj.rect_move_connect(0-((maxRectX-minRectX)/2+minRectX-top_x_branch),0-(maxRectY-top_y_branch));
//			selfObj.addRelationParent(triggerObj);
//			rcArray[0]=[triggerObj,real_pic];
//			triggerObj.conMarkShow("bottom");
//			triggerObj.addRcArray(selfObj,"bottom");
////			console.log("console.log(triggerObj)0",triggerObj)
//		}else if(selfObj.relationArray[0]==triggerObj){
////			console.log("解除branch的最顶点");
//			selfObj.removeRelationParent(triggerObj);
//			rcArray[0]=null;
//			triggerObj.conMarkHide("bottom");
//		}
//		if(bottom_x_branch>=minRectX && bottom_x_branch<= maxRectX && bottom_y_branch>=minRectY && bottom_y_branch<= maxRectY){
//			//连接到branch的最底点
////			console.log("连接到branch的最底点");
//			triggerObj.rect_move_connect(bottom_x_branch-((maxRectX-minRectX)/2+minRectX),bottom_y_branch-minRectY);
//			selfObj.addRelationChild(triggerObj);
//			rcArray[1]=[triggerObj,real_pic];
//			triggerObj.conMarkShow("top");
//			triggerObj.addRcArray(selfObj,"top");
////			console.log("console.log(triggerObj)1",triggerObj)
//		}else if(selfObj.relationArray[1]==triggerObj){
////			console.log("解除branch的最底点");
//			selfObj.removeRelationChild(triggerObj);
//			rcArray[1]=null;
//			triggerObj.conMarkHide("top");
//		}
//		var gap = (right_x_branch-left_x_branch)/(branchNum-1);
//		/**用来标识矩形是否已经和之前的branch的分支组成关系*/
//		var isHadContained = false;
//		for(var i=0;i<branchNum;i++){
//			if(!isHadContained && left_x_branch+(i*gap)>=minRectX && left_x_branch+(i*gap)<=maxRectX && maxRectY>=y_min_branch && minRectY <= y_max_branch){
//				//有交集
//				triggerObj.rect_move_connect(left_x_branch+(i*gap)-((maxRectX-minRectX)/2+minRectX),0);
//				/**用来标识矩形是否在之前已经和的branch的分支存在关系了，不需要重复加关系*/
//				var isContained = false;
//				if(selfObj.containsArray[i] && selfObj.containsArray[i]!=null){
//					for(var j=0;j<selfObj.containsArray[i].length;j++){
//						if(selfObj.containsArray[i][j]==triggerObj){
//							isContained=true;
//						}
//					}
//				}
//				if(!isContained){
//					//把矩形放到branch的path之上显示
//					triggerObj.overlay(branchLineObj);
//					selfObj.containsArray[i].push(triggerObj);
//					if(rcArray[i+2] == null){
//						rcArray[i+2]= new Array();
//					}
//					rcArray[i+2].push([triggerObj,real_pic,top_c.attr("cy")]);
//				}
//				isHadContained=true;
//			}else{
//				//无交集
//				for(var j=0;j<selfObj.containsArray[i].length;j++){
//					if(selfObj.containsArray[i][j]==triggerObj){
//						selfObj.containsArray[i][j]=null;
//						break;
//					}
//				}
//				if(rcArray[i+2]!=null){
//					for(var k=0;k<rcArray[i+2].length;k++){
//						if(rcArray[i+2][k]!=null && rcArray[i+2][k][0]==triggerObj){
//							rcArray[i+2][k] = null;
//							break;
//						}
//					}
//				}
//			}
//		}
////		console.log("relationArray",selfObj.relationArray);
////		console.log("containsArray",selfObj.containsArray);
////		console.log("rcArray",rcArray);
//	}
//	
//	//注册事件
//	/**
//	 * @argument : minRectX,minRectY,maxRectX,maxRectY rect的4个顶点
//	 * */
//	eve.on("custom.connectionHandler_branch",connectionHandlerFunction);
//	
//	var linkageAdjust = function(){
//		for(var i=0;i<rcArray.length;i++){
//			if(i==0 && rcArray[i]!=null){
////				console.log(111)
//				var rp = rcArray[i][1];
//				rcArray[i][0].rect_move_connect(outTop_c.attr("cx")-(rp.attr("x")+rp.attr("width")/2),outTop_c.attr("cy")-(rp.attr("y")+rp.attr("height")));
//			}else if(i==1 && rcArray[i]!=null){
//				var rp = rcArray[i][1];
//				rcArray[i][0].rect_move_connect(outBottom_c.attr("cx")-(rp.attr("x")+rp.attr("width")/2),outBottom_c.attr("cy")-(rp.attr("y")));
//			}else{
//				// 其他联动
//				if(i>1 && rcArray[i]!=null){
//					for(var j=0;j<rcArray[i].length;j++){
//						if(rcArray[i][j]!=null){
//							var rp = rcArray[i][j][1];
//							var gap = (right_c.attr("cx")-left_c.attr("cx"))/(branchNum-1);
//							rcArray[i][j][0].rect_move_connect(left_c.attr("cx")+((i-2)*gap)-((rp.attr("x")+rp.attr("width")/2)),top_c.attr("cy")-rcArray[i][j][2]);
//							rcArray[i][j][2]=top_c.attr("cy");
//						}
//					}
//				}
//			} 
//			
//		}
//	}
//	
//	this.addRelationParent = function(obj){
//		this.relationArray[0] = obj;
//	}
//	
//	this.addRelationChild = function(obj){
//		this.relationArray[1] = obj;
//	}
//	
//	this.point_move_remove_connect =function (point,obj){
//		if(point!=null){
//			if(point.data("id")==defaultId+"left_c"){
//				this.removeRelationParent(obj);
//			}else if(point.data("id")==defaultId+"right_c"){
//				this.removeRelationChild(obj);
//			}
//		}else{
//			this.removeRelationParent(obj);		
//			this.removeRelationChild(obj);
//		}
//	}
//	
//	this.removeRelationParent = function(obj){
//		if(this.relationArray[0] == obj){
//			this.relationArray[0] = null;
//		}
//	}
//	
//	this.removeRelationChild = function(obj){
//		if(this.relationArray[1] == obj){
//			this.relationArray[1] = null;
//		}
//	}
//	
//	var deleteFunction = function(){
//		if(isAdjusting){
//			for(var i=0;i<selfObj.relationArray.length;i++){
//				if(selfObj.relationArray[i]!=null){
//					selfObj.relationArray[i].otherConDetelte(selfObj);
//				}
//			}
//			branchLineObj.remove();
//			branchLineObjPrevent.remove();
//			branchLineObjAdjust.remove();
//			left_c.remove();
//			right_c.remove();
//			bottom_c.remove();
//			top_c.remove();
//			outBottom_c.remove();
//			outTop_c.remove();
//			bottom_arrow.remove();
//			judgeObj1.remove();
//			judgeObj2.remove();
//			for(var i=0;i<branchTextObjs.length;i++){
//				branchTextObjs[i].remove();
//			}
//			removeLine(selfObj);
//			removeComponentObjMap(defaultId);
//			//注销对象上的所有已注册的事件
//			eve.off("custom.someoneAdjusting", someoneAdjustingFunction);
//			eve.off("custom.connectionHandler_branch", connectionHandlerFunction);
//			eve.off("custom.connectProcedureContainTrigger", connectProcedureContainTriggerFunction);
//			eve.off("custom.someoneDelete", deleteFunction);
//			selfObj = null;
//		}		
//	}
//	
//	this.id = defaultId;
//	
//	//注册删除元素事件
//	eve.on("custom.someoneDelete",deleteFunction);
//	
//	this.getId = function (){
//		return defaultId;
//	}
//	this.getBranchTextObjs = function (){
//		return branchTextObjs;
//	}
//	this.getBranchNum = function (){
//		return branchNum;
//	}
//	this.getTopX = function (){
//		return top_c.attr("cx");
//	}
//	this.getTopY = function (){
//		return top_c.attr("cy");
//	}
//	this.getBottomX = function (){
//		return bottom_c.attr("cx");
//	}
//	this.getBottomY = function (){
//		return bottom_c.attr("cy");
//	}
//	this.getLeftX = function (){
//		return left_c.attr("cx");
//	}
//	this.getLeftY = function (){
//		return left_c.attr("cy");
//	}
//	this.getRightX = function (){
//		return right_c.attr("cx");
//	}
//	this.getRightY = function (){
//		return right_c.attr("cy");
//	}
//	this.getOutTopX = function (){
//		return outTop_c.attr("cx");
//	}
//	this.getOutTopY = function (){
//		return outTop_c.attr("cy");
//	}
//	this.getOutBottomX = function (){
//		return outBottom_c.attr("cx");
//	}
//	this.getOutBottomY = function (){
//		return outBottom_c.attr("cy");
//	}
//	this.getDAStr = function (){
//		return dAStr;
//	}
//	
//	return this;
//}


function aTorR(a){
	return a*2*Math.PI/360;
}

/**
 * 统一处理rectContentSpan,branchContentSpan即矩形输入框和判断框输入的显示和隐藏
 * */
function toggleRectContentSpanAndBranchContentSpan(triggerEle){
//	if(triggerEle.type=="rect"){
//		document.getElementById("rectContentSpan").style.display = "";
//	}else{
//		document.getElementById("rectContentSpan").style.display = "none";
//	}
//	if(triggerEle.cusType && triggerEle.cusType == "branch"){
//		document.getElementById("branchContentSpan").style.display = "";
//	}else{
//		document.getElementById("branchContentSpan").style.display = "none";
//	}
}
//全局注册事件统一处理rectContentSpan即矩形输入框和判断框输入的显示和隐藏
eve.on("custom.someoneAdjusting",toggleRectContentSpanAndBranchContentSpan);

JSON = {
	toJson : function(obj) {
		var m = {
			'\b' : '\\b',
			'\t' : '\\t',
			'\n' : '\\n',
			'\f' : '\\f',
			'\r' : '\\r',
			'"' : '\\"',
			'\\' : '\\\\'
		}, s = {
			array : function(x) {
				var a = [ '[' ], b, f, i, l = x.length, v;
				for (i = 0; i < l; i += 1) {
					v = x[i];
					f = s[typeof v];
					if (f) {
						v = f(v);
						if (typeof v == 'string') {
							if (b) {
								a[a.length] = ',';
							}
							a[a.length] = v;
							b = true;
						}
					}
				}
				a[a.length] = ']';
				return a.join('');
			},
			'boolean' : function(x) {
				return String(x);
			},
			'null' : function(x) {
				return null;
			},
			number : function(x) {
				return isFinite(x) ? String(x) : 'null';
			},
			object : function(x) {
				if (x) {
					if (x instanceof Array) {
						return s.array(x);
					}
					var a = [ '{' ], b, f, i, v;
					for (i in x) {
						v = x[i];
						f = s[typeof v];
						if (f) {
							v = f(v);
							if (typeof v == 'string') {
								if (b) {
									a[a.length] = ',';
								}
								a.push(s.string(i), ':', v);
								b = true;
							}
						}
					}
					a[a.length] = '}';
					return a.join('');
				}
				return null;
			},
			string : function(x) {
				if (/["\\\x00-\x1f]/.test(x)) {
					x = x.replace(/([\x00-\x1f\\"])/g, function(a, b) {
						var c = m[b];
						if (c) {
							return c;
						}
						c = b.charCodeAt();
						return '\\u00' + Math.floor(c / 16).toString(16)
								+ (c % 16).toString(16);
					});
				}
				return '"' + x + '"';
			}
		};
		if (typeof obj == "array") {
			return s.array(obj)
		} else {
			return s.object(obj)
		}
	},
	fromJson : function(s) {
		var r
		eval("r=" + s)
		return r;
	}
}
