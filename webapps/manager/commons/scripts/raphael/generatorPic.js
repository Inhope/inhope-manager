var animatePlayMs=500;
var animateEasing='backOut';
var isAnimate = false;

Raphael.fn.connection = function (obj1, obj2, line, bg , lt) {
    if (obj1.line && obj1.from && obj1.to) {
        line = obj1;
        obj1 = line.from;
        obj2 = line.to;
        arrow = line.arrow;
    }
    //ie <9 getBBox bug
    obj1._.dirty=1;
    obj2._.dirty=1;
    var bb1 = obj1.getBBox(),
        bb2 = obj2.getBBox(),
        p = [{x: bb1.x + bb1.width / 2, y: bb1.y - 1},
        {x: bb1.x + bb1.width / 2, y: bb1.y + bb1.height + 1},
        {x: bb1.x - 1, y: bb1.y + bb1.height / 2},
        {x: bb1.x + bb1.width + 1, y: bb1.y + bb1.height / 2},
        {x: bb2.x + bb2.width / 2, y: bb2.y - 1},
        {x: bb2.x + bb2.width / 2, y: bb2.y + bb2.height + 1},
        {x: bb2.x - 1, y: bb2.y + bb2.height / 2},
        {x: bb2.x + bb2.width + 1, y: bb2.y + bb2.height / 2}],
        d = {}, dis = [];
    for (var i = 0; i < 4; i++) {
        for (var j = 4; j < 8; j++) {
            var dx = Math.abs(p[i].x - p[j].x),
                dy = Math.abs(p[i].y - p[j].y);
            if ((i == j - 4) || (((i != 3 && j != 6) || p[i].x < p[j].x) && ((i != 2 && j != 7) || p[i].x > p[j].x) && ((i != 0 && j != 5) || p[i].y > p[j].y) && ((i != 1 && j != 4) || p[i].y < p[j].y))) {
                dis.push(dx + dy);
                d[dis[dis.length - 1]] = [i, j];
            }
        }
    }
    if (dis.length == 0) {
        var res = [0, 4];
    } else {
        res = d[Math.min.apply(Math, dis)];
    }
    var x1 = p[res[0]].x,
        y1 = p[res[0]].y,
        x4 = p[res[1]].x,
        y4 = p[res[1]].y;
    dx = Math.max(Math.abs(x1 - x4) / 2, 10);
    dy = Math.max(Math.abs(y1 - y4) / 2, 10);
    var x2 = [x1, x1, x1 - dx, x1 + dx][res[0]].toFixed(3),
        y2 = [y1 - dy, y1 + dy, y1, y1][res[0]].toFixed(3),
        x3 = [0, 0, 0, 0, x4, x4, x4 - dx, x4 + dx][res[1]].toFixed(3),
        y3 = [0, 0, 0, 0, y1 + dy, y1 - dy, y4, y4][res[1]].toFixed(3);
    var path ;
    var arrowLength_x = 5;
    var arrowLength_y = 15;
    var fangxinIndex = res[0];  //0上,1下,2左,3右
    var arrowPath;
    var ax = x1.toFixed(3);
    var ay = y1.toFixed(3);
    if(fangxinIndex==0){
    	path = ["M", x1.toFixed(3), y1.toFixed(3)-0-arrowLength_y, "C", x2, y2-20, x3, y3, x4.toFixed(3), y4.toFixed(3)].join(",");
    	arrowPath=["M",ax-arrowLength_x,ay-arrowLength_y,"L",ax,ay,"L",ax-0+arrowLength_x,ay-arrowLength_y,"Z"].join(",");
    	
    }else if(fangxinIndex==1){
    	path = ["M", x1.toFixed(3), y1.toFixed(3)-0+arrowLength_y, "C", x2, y2-0+20, x3, y3, x4.toFixed(3), y4.toFixed(3)].join(",");
    	arrowPath=["M",ax-arrowLength_x,ay-0+arrowLength_y,"L",ax,ay,"L",ax-0+arrowLength_x,ay-0+arrowLength_y,"Z"].join(",");
    }else if(fangxinIndex==2){
    	path = ["M", x1.toFixed(3)-0-arrowLength_y, y1.toFixed(3), "C", x2-0-20, y2, x3, y3, x4.toFixed(3), y4.toFixed(3)].join(",");
    	arrowPath=["M",ax-arrowLength_y,ay-arrowLength_x,"L",ax,ay,"L",ax-arrowLength_y,ay-0+arrowLength_x,"Z"].join(",");
    }else if(fangxinIndex==3){
    	path = ["M", x1.toFixed(3)-0+arrowLength_y, y1.toFixed(3), "C", x2-0+20, y2, x3, y3, x4.toFixed(3), y4.toFixed(3)].join(",");
    	arrowPath=["M",ax-0+arrowLength_y,ay-arrowLength_x,"L",ax,ay,"L",ax-0+arrowLength_y,ay-0+arrowLength_x,"Z"].join(",");
    }
//    console.log("arrowPath",arrowPath);
    
    if (line && line.line) {
//        line.bg && line.bg.attr({path: path});
        line.line.attr({path: path});
        line.arrow.attr({path: arrowPath});
    } else {
        var color = typeof line == "string" ? line : "rgb(153,0,51)";
        var pathAttr;
        if(lt=="2"){
        	pathAttr = {stroke: color, fill: "none", "stroke-dasharray": "-"};
        }else{
        	pathAttr = {stroke: color, fill: "none"};
        } 
        return {
//            bg: bg && bg.split && this.path(path).attr({stroke: bg.split("|")[0], fill: "none", "stroke-width": bg.split("|")[1] || 3}),
            line: this.path(path).attr(pathAttr),
            from: obj1,
            to: obj2,
            arrow : this.path(arrowPath).attr({stroke: color, fill: "none"})
        };
    }
};

function drawPic(divId,cwidth,cheight,rData,isAmt,rootArrays,oArray,picDistance_y,ellipse_ry,picDistance_x,ellipse_rx) {
	if(isAmt)
		isAnimate = isAmt;
	var picType = "clz";
	var dataJsonObj = eval('(' + rData + ')');
	if(dataJsonObj){
		if(dataJsonObj["picType"]){
			picType = dataJsonObj["picType"];
		}
	}
	
	/**onstart*/
    var dragger = function () {
        this.ox = this.type == "rect" ? this.attr("x") : this.attr("cx");
        this.oy = this.type == "rect" ? this.attr("y") : this.attr("cy");
        this.animate({"fill-opacity": .3}, 500);
    },/**onmove*/
        move = function (dx, dy) {
        	
        	var self = this;
        	var selfId = self.data("id");
        	
        	var currentx;
        	var currenty;
        	var halfwidthSingle = ellipse_rx >= self.attr("width")/2 ? ellipse_rx : self.attr("width")/2;
        	if(this.type == "rect"){
        		currentx = startRect2ellipse_x(this.ox,halfwidthSingle) + dx;
	        	currenty = startRect2ellipse_y(this.oy,ellipse_ry) + dy;
        	}else if(this.type == "ellipse"){
	        	currentx = this.ox + dx;
	        	currenty = this.oy + dy;
        	}
    		if((currentx-halfwidthSingle)<0){
    			currentx = halfwidthSingle;
        	}else if((currentx+halfwidthSingle) >= canvas_width){
	        	currentx = canvas_width-halfwidthSingle;
	        }
	        if((currenty-ellipse_ry) < 0){
		    	currenty = ellipse_ry;
		    }else if((currenty+ellipse_ry) > canvas_height){
		    	currenty=canvas_height-ellipse_ry;
		    }
        	r.forEach(function (el) {
			    if(el.data("id")==selfId+"___text"){
			    	el.attr({x: currentx , y: currenty -ellipse_ry+6+ textOffsety+3 });
			    }
			    if(el.data("id")==selfId+"___clzPath"){
			    	var clzTextPathY = currenty-ellipse_ry+12+2*textOffsety+6;
			    	el.attr({path:["M",currentx-halfwidthSingle,clzTextPathY,"L",currentx+halfwidthSingle,clzTextPathY].join(",")});
			    }
			    if(el.data("id") && el.data("id").indexOf(selfId+"___attrText")>-1){
			    	var ai = el.data("id").substring(el.data("id").length-1)-0;
			    	var clzTextPathY = currenty-ellipse_ry+12+2*textOffsety+6;
			    	if(ai==3){
			    		el.attr({x:currentx-halfwidthSingle+el.getBBox()["width"]/2+1,y:clzTextPathY-0+((10/2+textOffsety*2)*((ai+1-1)+(1*(ai-1))))+8});
			    	}else{
				    	el.attr({x:currentx-halfwidthSingle+el.getBBox()["width"]/2+1,y:clzTextPathY-0+((10/2+textOffsety*2)*((ai+1)+(1*ai)))});
			    	}
			    }
			});
        	
            var att = this.type == "rect" ? {x: startEllipse2rect_x(currentx,halfwidthSingle), y: startEllipse2rect_y(currenty,ellipse_ry)} : {cx: currentx, cy: currenty};
            this.attr(att);
            for (var i = connections.length; i--;) {
            	if(selfId==connections[i].from.data("id") || selfId==connections[i].to.data("id")){
	                r.connection(connections[i]);
            	}
            }
            r.safari();
        },/**onend*/
        up = function () {
            this.animate({"fill-opacity": 1}, 500);
        },
        canvas_width = cwidth,
        canvas_height = cheight,
        r = Raphael(divId, canvas_width, canvas_height),
        textOffsetx = 3,
		textOffsety = 1,
        connections = [];
   if(rootArrays.length > 0){
   		var ri = 0
	   	var nextRi = ri+1;
	   	for( ;ri<rootArrays.length ;ri++){
	   		var start_x = canvas_width/2*(nextRi/((rootArrays.length)));
	   		nextRi = 2+nextRi;
			generatorPic(move, dragger, up,textOffsetx,textOffsety,r,connections,rootArrays[ri],ellipse_rx,ellipse_ry,picDistance_x,picDistance_y,start_x,50,null,null,canvas_width,canvas_width/2,ri,oArray);
	   	}
   }
   
};

function generatorPic(move, dragger, up,textOffsetx,textOffsety,paper,connections,picObject,ellipse_rx,ellipse_ry,picDistance_x,picDistance_y,start_x,start_y,parentPicObject,lt,canvas_width,rawstart_x,rootIndex,oArray){
	var isExist = false;
	var pic ;
	paper.forEach(function (el) {
	    if(el.data("id")==picObject.id){
	    	pic = el;
	    	isExist = true;
	    	return false;
	    }
	});
	if(isExist){
		if(parentPicObject){
			if(isAnimate){
				pic.animate({"fill-opacity": 1}, animatePlayMs,animateEasing,function(){
					connections.push(paper.connection(parentPicObject, pic, "rgb(153,0,51)",null,lt));
				});
			}else{
				connections.push(paper.connection(parentPicObject, pic, "rgb(153,0,51)",null,lt));
			}
		}
		return ;
	}
	var distXY ;
	if(picObject.type=='1'){
		if(isAnimate){
			pic = paper.rect(0,0,ellipse_rx*2,ellipse_ry*2);
			distXY = {x: startEllipse2rect_x(start_x,ellipse_rx),y: startEllipse2rect_y(start_y,ellipse_ry)};
		}else{
			pic = paper.rect(startEllipse2rect_x(start_x,ellipse_rx),startEllipse2rect_y(start_y,ellipse_ry),ellipse_rx*2,ellipse_ry*2);
		}
	}else if(picObject.type=='2'){
		if(isAnimate){
			pic = paper.ellipse(0,0,ellipse_rx,ellipse_ry);
			distXY = {cx: start_x,cy: start_y};
		}else{
			pic = paper.ellipse(start_x,start_y,ellipse_rx,ellipse_ry);
		}
	}else{
		//默认
		if(isAnimate){
			pic = paper.rect(0,0,ellipse_rx*2,ellipse_ry*2);
			distXY = {x: startEllipse2rect_x(start_x,ellipse_rx),y: startEllipse2rect_y(start_y,ellipse_ry)};
		}else{
			pic = paper.rect(startEllipse2rect_x(start_x,ellipse_rx),startEllipse2rect_y(start_y,ellipse_ry),ellipse_rx*2,ellipse_ry*2);
		}
	}
	pic.data("id",picObject.id);
	if(isAnimate){
		pic.animate(distXY, animatePlayMs,animateEasing,function(){
			// 计算text的起始位置
			startDraw(pic ,move, dragger, up,textOffsetx,textOffsety,paper,connections,picObject,ellipse_rx,ellipse_ry,picDistance_x,picDistance_y,start_x,start_y,parentPicObject,lt,canvas_width,rawstart_x,rootIndex,oArray);
		}); 
	}else{
		startDraw(pic ,move, dragger, up,textOffsetx,textOffsety,paper,connections,picObject,ellipse_rx,ellipse_ry,picDistance_x,picDistance_y,start_x,start_y,parentPicObject,lt,canvas_width,rawstart_x,rootIndex,oArray);
	}
	
}

function startDraw(pic ,move, dragger, up,textOffsetx,textOffsety,paper,connections,picObject,ellipse_rx,ellipse_ry,picDistance_x,picDistance_y,start_x,start_y,parentPicObject,lt,canvas_width,rawstart_x,rootIndex,oArray){
	var titleText = paper.text(start_x-picObject.text.length/2+textOffsetx, start_y-ellipse_ry+12/2+textOffsety+3, picObject.text).attr({font: '12px "Arial"'}).data("id",picObject.id+"___text");
	
	var clzTextPathY = start_y-ellipse_ry+12+2*textOffsety+6;
	
	var maxAttrWidth=0;
	var ts =[];
	if(picObject.attrs.length > 0){
		var attrNum = picObject.attrs.length;
		for(var ai = 0; ai<attrNum && ai<4 ; ai++){
			var attrText = "+"+picObject.attrs[ai];
			if(ai==3){
				attrText ="...";
				var t = paper.text(-100, -100, attrText).attr({font: '10px "Arial"'}).data("id",picObject.id+"___attrText"+ai);
				t.attr({x:start_x-ellipse_rx+t.getBBox()["width"]/2+1,y:clzTextPathY-0+((10/2+textOffsety*2)*((ai+1-1)+(1*(ai-1))))+8});
				ts.push(t);
			}else{
				var t = paper.text(-100, -100, attrText).attr({font: '10px "Arial"'}).data("id",picObject.id+"___attrText"+ai);
				t.attr({x:start_x-ellipse_rx+t.getBBox()["width"]/2+1,y:clzTextPathY-0+((10/2+textOffsety*2)*((ai+1)+(1*ai)))});
				if(t.getBBox()["width"] > ellipse_rx*2 && t.getBBox()["width"] > maxAttrWidth){
					maxAttrWidth = t.getBBox()["width"];
				}
				ts.push(t);
			}
		}
	}
	//判断title是不是超过ellipse_rx*2
	var titleMax = false;
	if(titleText.getBBox()["width"] > ellipse_rx*2 && titleText.getBBox()["width"] > maxAttrWidth){
		maxAttrWidth = titleText.getBBox()["width"];
		titleMax = true;
	}
	if(maxAttrWidth>0){
		if(titleMax){
			// 增大pic的width
			if(pic.type=="rect"){
				var picOffsetx = (maxAttrWidth-pic.attr("width"))/2+2;
				pic.attr({"width":maxAttrWidth+2*2,"x":pic.attr("x") - picOffsetx});
				for(var tindex in ts){
					ts[tindex].attr({"x":ts[tindex].attr("x")-picOffsetx});
				}
			}
			paper.path(["M",pic.attr("x"),clzTextPathY,"L",pic.attr("x")+maxAttrWidth+2*2,clzTextPathY].join(",")).attr({stroke: "rgb(153,0,51)", fill: "none"}).data("id",picObject.id+"___clzPath");
		}else{
			// 增大pic的width
			if(pic.type=="rect"){
				var picOffsetx = (maxAttrWidth-pic.attr("width"))/2;
				pic.attr({"width":maxAttrWidth+2,"x":pic.attr("x") - picOffsetx});
				for(var tindex in ts){
					ts[tindex].attr({"x":ts[tindex].attr("x")-picOffsetx});
				}
			}
			paper.path(["M",pic.attr("x"),clzTextPathY,"L",pic.attr("x")+maxAttrWidth+2,clzTextPathY].join(",")).attr({stroke: "rgb(153,0,51)", fill: "none"}).data("id",picObject.id+"___clzPath");
		}
	}else{
		paper.path(["M",start_x-ellipse_rx,clzTextPathY,"L",start_x+ellipse_rx,clzTextPathY].join(",")).attr({stroke: "rgb(153,0,51)", fill: "none"}).data("id",picObject.id+"___clzPath");
	}
	if(pic.type == "rect"){
		titleText.attr({"x":pic.attr("x")-0+pic.attr("width")/2});
	}else{
		titleText.attr({"x":pic.attr("cx")-0+pic.attr("width")/2});
	}
	
	var color = "rgb(255,255,204)";
	pic.attr({fill: color, stroke: "rgb(153,0,51)", "fill-opacity": 1, "stroke-width": .8, cursor: "move"});
	pic.drag(move, dragger, up);//onmove, onstart, onend
	if(parentPicObject){
		connections.push(paper.connection(parentPicObject, pic, "rgb(153,0,51)",null,lt));
	}
	if(picObject.children && picObject.children.length>0){
		var childNum = picObject.children.length;
		var offset_x = ellipse_rx - 0 + picDistance_x;
		var offset_y = ellipse_ry - 0 + picDistance_y;
		
		//(基于每行个数的) 计算自己pic的中心点位置
		start_y = start_y-0+offset_y;
		for(var index =0 ;index <childNum;index++){
			var pChild = picObject.children[index][0];
			var totalChildByRow = oArray[pChild.row];
			var leftX;
			if(totalChildByRow%2==0){
				//偶数个孩子
				leftX = rawstart_x - offset_x;
				//计算最左点圆心
				leftX = leftX - (totalChildByRow/2-1)*2*offset_x;
			}else{
				//奇数 计算最左点圆心
				leftX = rawstart_x - (totalChildByRow/2|0)*2*offset_x;
			}
			//计算自己的圆心点x
			start_x = (leftX-0) + (pChild.col-1)*2*offset_x;
			// 特殊处理下2个子的布局位置,不至于离的太近
			if(totalChildByRow==2){
				leftX = rawstart_x - (offset_x-0+15);
				start_x = (leftX-0) + (pChild.col-1)*2*(offset_x-0+15);
			}
			generatorPic(move, dragger, up,textOffsetx,textOffsety,paper,connections,picObject.children[index][0],ellipse_rx,ellipse_ry,picDistance_x,picDistance_y,start_x,start_y,pic,picObject.children[index][1],canvas_width,rawstart_x,rootIndex,oArray);
		}
		
		
		//(基于父类个数的)计算中心点
//		if(childNum%2==0){
//			//偶数个孩子
//			start_x = start_x - offset_x;
//			//计算最左点圆心
//			start_x = start_x - (childNum/2-1)*2*offset_x;
//		}else{
//			//奇数 计算最左点圆心
//			start_x = start_x - (childNum/2|0)*2*offset_x;
//		}
//		
//		start_y = start_y-0+offset_y;
//		for(var index in picObject.children){
//			//计算各种距离等
//			if(index>0){
//				start_x = (start_x-0) + 2*offset_x;
//			}
//			generatorPic(move, dragger, up,textOffsetx,textOffsety,paper,connections,picObject.children[index][0],ellipse_rx,ellipse_ry,picDistance_x,picDistance_y,start_x,start_y,pic,picObject.children[index][1],canvas_width);
//		}
	}
}

//起点(椭圆转矩形)
function startEllipse2rect_x(x,ellipse_rx){
	return x-ellipse_rx;
}

function startEllipse2rect_y(y,ellipse_ry){
	return y-ellipse_ry;
}

//起点(矩形转椭圆)
function startRect2ellipse_x(x,ellipse_rx){
	return x-0+ellipse_rx;
}

function startRect2ellipse_y(y,ellipse_ry){
	return y-0+ellipse_ry;
}

