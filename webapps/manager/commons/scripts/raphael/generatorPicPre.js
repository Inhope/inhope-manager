function generatorIFrame(width, height, rData, divId, renderTo, isAmt) {
	var rootArrays = getRootArrays(rData);
	// new 计算上下和左右的个数
	var oArray = {};
	if (rootArrays.length > 0) {
		var maxNumberRows = [0];
		for (var i = 0; i < rootArrays.length; i++) {
			compute(rootArrays[i], 1,oArray,maxNumberRows);
		}
//		console.log(oArray);
		// console.log(rootArrays);
	}
	var maxNumberRow=maxNumberRows[0];//竖直方向上最大值
//	console.log("maxNumberRow",maxNumberRow)
	// 计算height,width
	var picDistance_y = 67;
	var ellipse_ry = 37;
	var picDistance_x = 20;
    var ellipse_rx = 35;
//	console.log("maxNumberRow",maxNumberRow);
	var maxheight = (ellipse_ry*2)*maxNumberRow+(maxNumberRow-1)*(picDistance_y-ellipse_ry)+70;
//	var fontPerWord = 10;
	var maxNumberCol = getMaxColNumberByRow(oArray);
//	maxNumberCol
	var offset_x = ellipse_rx - 0 + picDistance_x;
	var maxWidth = 2*offset_x*maxNumberCol;
	
	if(maxheight<height){
		maxheight=height;
	}
	if(maxWidth<width){
		maxWidth=width;
	}
	
	if (!divId) {
		divId = "holder";
	}
	// 生成iframe
	var borderWidth = 0;
	var objiframe = document.createElement("iframe");
	objiframe.dWidth = maxWidth;
	objiframe.dHeight = maxheight;
	objiframe.style.width = (width - borderWidth * 2) + "px";
	objiframe.style.height = (height - borderWidth * 2) + "px";
	objiframe.style.border = borderWidth + "px solid grey";
	objiframe.scrolling = "auto";
	objiframe.frameBorder = "none";
	renderTo = renderTo || document.body
	renderTo.appendChild(objiframe);

	var scripts = document.getElementsByTagName("script");
	var location;
	for (var i = 0; i < scripts.length; i++) {
		if (scripts[i].src.indexOf("generatorPicPre.js") > -1) {
			location = scripts[i].src.substring(0, scripts[i].src
							.lastIndexOf("/")
							+ 1);
		}
	}

	objiframe.contentWindow.document
			.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\r\n"
					+ "<html>\r\n"
					+ "	<head>\r\n"
					+ "		<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n"
					+ "		<script src=\""
					+ location
					+ "raphael.js\" type=\"text/javascript\" charset=\"utf-8\"></script>\r\n"
					+ "		<script src=\""
					+ location
					+ "generatorPic.js\" type=\"text/javascript\" charset=\"utf-8\"></script>\r\n"
					+ "		\r\n"
					+ "	</head>\r\n"
					+ "	<body style=\"background-color:#eef;margin:0px auto;border:0px;padding:0px;\">\r\n"
					+ "	<div id='"
					+ divId
					+ "' style='width:"
					+ (maxWidth - borderWidth * 2)
					+ ";height:"
					+ (maxheight - borderWidth * 2)
					+ "' ></div>"
					+ "    </body>\r\n" + "</html>");
	objiframe.contentWindow.document.close();
	setTimeout(function() {
				objiframe.contentWindow.drawPic(divId, maxWidth - borderWidth * 2,
						maxheight - borderWidth * 2, rData, isAmt, rootArrays,
						oArray, picDistance_y, ellipse_ry,picDistance_x,ellipse_rx);
			}, 500);
	return objiframe;
}

function getRootArrays(rData) {
	var dataJsonObj = eval('(' + rData + ')');
	var picMap = {};
	var rootArrays = [];
	if (dataJsonObj) {
		var datas = dataJsonObj["data"];
		for (var index in datas) {
			var ele = datas[index];
			var eid = ele["id"];
			if (eid) {
				var picObj = new PicObj(eid, ele["text"], ele["type"],
						ele["parentId"], ele["lineType"], ele["attrs"]);
				picMap[eid] = picObj;
			}
		}
		for (var key in picMap) {
			var picObj = picMap[key];
			if (picObj.parentId[0] && picObj.parentId.length > 0) {
				var defaultLt = picObj.lineType;
				for (var index = 0; index < picObj.parentId.length; index++) {
					var parentStrOrObj = picObj.parentId[index];
					var parentId;
					var selfLt = 0;
					if (typeof(parentStrOrObj) == "string") {
						parentId = parentStrOrObj;
					} else {
						parentId = parentStrOrObj["id"];
						selfLt = parentStrOrObj["lineType"];
					}
					if (!selfLt) {
						selfLt = defaultLt;
					}
					var parentPicObj = picMap[parentId];
					if (parentPicObj) {
						parentPicObj.addChildren([picObj, selfLt]);
					}
				}
			} else {
				rootArrays.push(picObj);
			}
		}
		return rootArrays;
	}
}

// parentRowNum 上级的行标
// 计算行,列的最大数
function compute(picObj, parentRowNum,oArray,maxNumberRows) {
	// console.log("parentRowNum",parentRowNum);
	if (parentRowNum == 1) {
		if(oArray[parentRowNum]){
			oArray[parentRowNum] = oArray[parentRowNum]
						- 0 + 1;
			picObj.setRow(parentRowNum);
			picObj.setCol(oArray[parentRowNum]);			
		}else{
			oArray[parentRowNum] = 1;
			picObj.setRow(parentRowNum);
			picObj.setCol(oArray[parentRowNum]);
		}
	}
	if (picObj.children && picObj.children.length > 0) {
		parentRowNum = parentRowNum - 0 + 1;
		if(maxNumberRows[0]<parentRowNum){
			maxNumberRows[0]=parentRowNum;
		}
		if (!oArray[parentRowNum]
				&& oArray[parentRowNum] != 0) {
			oArray[parentRowNum] = 0;
		}
		for (var i = 0; i < picObj.children.length; i++) {
			if (!picObj.children[i][0].row) {
				oArray[parentRowNum] = oArray[parentRowNum]
						- 0 + 1;
				picObj.children[i][0].setRow(parentRowNum);
				picObj.children[i][0].setCol(oArray[parentRowNum]);
				compute(picObj.children[i][0], parentRowNum,oArray,maxNumberRows);
			}
		}
	}
//	 return oArray;
}

//计算一行中最大列数
function getMaxColNumberByRow(oArray){
	var maxNumberCol=0;//水平方向上最大值
	for(var key_row in oArray){
		if(maxNumberCol<oArray[key_row]-0){
			maxNumberCol=oArray[key_row]-0;
		}
	}
	return maxNumberCol;
}

//function getMaxTextLength(picObj){
//	var maxTextLength = picObj.text.length;
//	if(picObj.attrs && picObj.attrs.length>0){
//		for(var i=0;i<picObj.attrs.length && i<3;i++){
//			if(maxTextLength<picObj.attrs[i].length){
//				maxTextLength=picObj.attrs[i].length;
//			}
//		}
//	}
//	return maxTextLength;
//}

function PicObj(id,text,type,parentId,lineType,attrs){
	this.id=id;
	this.text=text;
	this.type=type;
	if(typeof(parentId)=="string"){
		parentId = [parentId];
	}else if(!(parentId instanceof Array)){
		parentId=[parentId];
	}
	this.parentId=parentId;
	this.lineType = lineType;
	if(!this.lineType){
		this.lineType="1";
	}
	this.children = [];
	this.row;
	this.col;
	if(attrs){
		this.attrs = attrs;
	}else{
		this.attrs = [];
	}
}

PicObj.prototype = {  
    addChildren : function(child) {  
        this.children.push(child);
    },
    setRow : function(row) {  
        this.row = row;
    },
    setCol : function(col) {  
        this.col = col;
    }
}