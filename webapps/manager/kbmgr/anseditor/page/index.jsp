<%@page import="org.springframework.context.ApplicationContext"%>
<%@page import="org.codehaus.jackson.map.ObjectMapper"%>
<%@page import="com.incesoft.xmas.commons.LicenceHolder"%>
<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="res" uri="/WEB-INF/tld/import-res.tld"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="usr" uri="/WEB-INF/tld/login-user.tld"%>
<%@taglib prefix="mdl" uri="/WEB-INF/tld/mdl-ft.tld"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>知识库管理</title>
		<script src="jquery.js" type="text/javascript"></script>
		<script src="simple-integration.js" type="text/javascript"></script>
		<script src="editor.js" type="text/javascript"></script>
		<link rel="stylesheet" type="text/css" href="../css/global_wx.css" />
		<link rel="stylesheet" type="text/css" href="../css/modal.css" />
	</head>

	<body style="padding:0;margin:0">
	</body>
</html>