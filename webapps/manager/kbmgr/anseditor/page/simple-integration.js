var showTextEditor = window.location.search.indexOf('showTextEditor') != -1;
var integeration_inited = false ;
var defaultTxtLimit = 600;
var pre_integerationId='integeration_';
var count_integerationId=0;

/**
 * key:group key value:contentJObj[]
 */
var contentJObjMap = {};
var valid_ans_feature = false;

var INTEGERATION_DEFAULT = 'integeration_default';

function integerationInit(initArgs){
	if(!integeration_inited){
		if(initArgs){
			if(initArgs['txtLimit'] && initArgs['txtLimit'] > 0){
				defaultTxtLimit = initArgs['txtLimit'];
			}
			var groupKeys_initArg = initArgs['groupKeys'] ; 
			if(groupKeys_initArg && groupKeys_initArg.constructor==Array){
				for(var i=0;i<groupKeys_initArg.length;i++){
					contentJObjMap[groupKeys_initArg[i]]=[];
				}
			}
			if(initArgs['validAnsFeature']){
				valid_ans_feature = true;
			}
		}
		if(!initArgs || !initArgs['groupKeys']){
			contentJObjMap[INTEGERATION_DEFAULT]=[];
		}
		integeration_inited = true;
	}
}

function getDatas_integerationGroup(groupKey){
	if(!groupKey){
		groupKey = INTEGERATION_DEFAULT;
	}
	var isValid = true;
	var errorMsg ;
	var datas = new Array();
	var contentJObjs = contentJObjMap[groupKey];
	
	for(var ci = 0;ci<contentJObjs.length;ci++){
		var cJObj = $(contentJObjs[ci]);
		// 验证
		var typeInputJObj = cJObj.find('input[type="hidden"]');
		var cType = typeInputJObj.val();
		var answer = cJObj.find('textarea').val();
		if(!answer){
			if(valid_ans_feature){
				continue;
			}else{
				isValid = false;
				errorMsg = '答案内容不能为空';
				break;
			}
		}else{
			if(!checkTextLengthValid(answer)){
				isValid = false;
				errorMsg = '答案内容超过限制字数';
				break;
			}
		}
		var dm ;
		if(!cType || cType == 'text'){
			dm = {'answer':answer,'tags':tags};
		}else if(cType == 'imgtxtmsg'){
			dm = {'answer':answer,'tags':tags,'type':cType};
		}else if(cType == 'imagemsg'){
			dm = {'answer':answer,'tags':tags,'type':cType};
		}else if(cType == 'audiomsg'){
			dm = {'answer':answer,'tags':tags,'type':cType};
		}else if(cType == 'videomsg'){
			dm = {'answer':answer,'tags':tags,'type':cType};
		}
		if(window.getDatasRendered){
			window.getDatasRendered(dm,getIntegerationId(cJObj));
		}
		datas.push(dm);
	}
	//
	if(!isValid){
		if(window.integrationError){
			window.integrationError(errorMsg);
		}else{
			XIDialog.alert({'title':'提示','msg':errorMsg});
		}
		return false;
	}
	return datas;
}

/**
 * @return null<br>
 *         object<br>
 *         object Array
 */
function getDatas_integeration(containDiv,notNullId){
	var isValid = true;
	var errorMsg ;
	var datas = new Array();
	var containJObj;
	if(typeof containDiv == 'object'){
		containJObj = containDiv;
	}else{
		containJObj = $('#'+containDiv);
	}
	var contentJObjs = containJObj.find('div[class="CMOP-welcontent2"]');
	
	for(var ci = 0;ci<contentJObjs.length;ci++){
		var cJObj = $(contentJObjs[ci]);
		// 验证
		var typeInputJObj = cJObj.find('input[type="hidden"]');
		var cType = typeInputJObj.val();
		var answer = cJObj.find('textarea').val();
		var tags = getSelectedDim(cJObj.find('div[class="CMOP-welcnceng"]'));
		
		// 无须输入问题的情况
		var integerationId = getIntegerationId(cJObj);
		if(!answer && notNullId){
			if(integerationId != notNullId || notNullId=='all'){
				dm = {'answer':'-','tags':new Array()};
				datas.push(dm);
				continue;
			}
		}
		
		if(!answer){
			if(valid_ans_feature){
				continue;
			}else{
				isValid = false;
				errorMsg = '答案内容不能为空';
				break;
			}
		}else{
			if(!checkTextLengthValid(answer)){
				isValid = false;
				errorMsg = '答案内容超过限制字数';
				break;
			}
		}
		
		var dm ;
		if(!cType || cType == 'text'){
			dm = {'answer':answer,'tags':tags};
		}else if(cType == 'imgtxtmsg'){
			dm = {'answer':answer,'tags':tags,'type':cType};
		}else if(cType == 'imagemsg'){
			dm = {'answer':answer,'tags':tags,'type':cType};
		}else if(cType == 'audiomsg'){
			dm = {'answer':answer,'tags':tags,'type':cType};
		}else if(cType == 'videomsg'){
			dm = {'answer':answer,'tags':tags,'type':cType};
		}
		if(window.getDatasRendered){
			window.getDatasRendered(dm,getIntegerationId(cJObj));
		}
		dm['id']=integerationId;
		datas.push(dm);
	}
	//
	if(!isValid){
		if(window.integrationError){
			window.integrationError(errorMsg);
		}else{
			XIDialog.alert({'title':'提示','msg':errorMsg});
		}
		return false;
	}
	return datas;
}

/**
 * @argument containDiv : 容器id <br>
 *           obj : 加载的内容object
 */
function addTemplate_integeration(obj,groupKey,integerationId){
	integerationInit();
	var contentJObj ;
	contentJObj = $(Template.html());
	if(integerationId){
		contentJObj.attr('id',integerationId);
	}else{
		contentJObj.attr('id',pre_integerationId+count_integerationId);
		count_integerationId++;
	}
	var gp = INTEGERATION_DEFAULT;
	if(groupKey)
		gp = groupKey;
	contentJObj.attr('gp',gp);
	if(contentJObjMap[gp]){
		contentJObjMap[gp].push(contentJObj);
	}
	
	if(obj){
		// load
		loadContent(contentJObj,obj['type'],obj['answer']);
	}
	if(window.integerationRendered){
		window.integerationRendered(contentJObj);
	}
	if (showTextEditor) {
		$('#textDiv', contentJObj).show();
		$('#__textEditor', contentJObj).show();
	}
	return contentJObj;
}

function closeIntegeration(){
	if(window.closeIntegerationRendered){
		window.closeIntegerationRendered();
	}
}

function getIntegerationId(contentJObj){
	return contentJObj.attr('id');
}

function getIntegerationGroupKey(contentJObj){
	return contentJObj.attr('gp');
}

function reset_integeration(containDiv,contentJObj){
	contentJObj.remove();
	return addTemplate_integeration(containDiv);
}

function reload_integeration(contentJObj,jsonStr,jsonObj){
	if(contentJObj)
		contentJObj.remove();
	contentJObj = addTemplate_integeration();
	if(jsonStr){
		jsonObj = eval('jsonObj = ' + jsonStr);
		loadContent(contentJObj,jsonObj['type'],jsonObj['answer']);
	}else if(jsonObj){
		loadContent(contentJObj,jsonObj['type'],jsonObj['answer']);
	}
	return contentJObj;
}

function remove_integeration(integerationId,group){
	var contentJObjDel = $('#'+integerationId);
	if(contentJObjDel){
		// 从contentJObjMap中删除
		var contentJObjs = contentJObjMap[group];
		if(contentJObjs){
			for(var i=0;i<contentJObjs.length;i++){
				if(getIntegerationId(contentJObjs[i]) == getIntegerationId(contentJObjDel)){
					contentJObjs = contentJObjs.splice(i,1);
				}
			}
		}
		contentJObjDel.remove();
	}
}

function removeContain(btnDomObj){
	var containJObj = $(btnDomObj).parent();
	containJObj.remove();
}

function loadContent(contentJObj,type,answer){
	var barJObj = contentJObj.find('div[class="tooltitlecn"]');
	var hiddenInputJObj = contentJObj.find('input[type="hidden"]');
	var textareaJObj = contentJObj.find('textarea');
	if(!type || type == 'text'){
// textareaJObj.val(answer);
		if(answer == '-'){
			answer='';
		}
		tLp5Click(contentJObj.find('a[class^="weltop-link01"]'),answer);
	}else if(type=='imgtxtmsg'){
		textareaJObj.val(answer);
		hiddenInputJObj.val('imgtxtmsg');
		loadAnswerByType(answer,type,textareaJObj);
		txtRemain_hide(textareaJObj);
	}else if(type=='imagemsg'){
		textareaJObj.val(answer);
		hiddenInputJObj.val('imagemsg');
		loadAnswerByType(answer,type,textareaJObj);
		txtRemain_hide(textareaJObj);
	}else if(type=='audiomsg'){
		textareaJObj.val(answer);
		hiddenInputJObj.val('audiomsg');
		loadAnswerByType(answer,type,textareaJObj);
		txtRemain_hide(textareaJObj);
	}else if(type=='videomsg'){
		textareaJObj.val(answer);
		hiddenInputJObj.val('videomsg');
		loadAnswerByType(answer,type,textareaJObj);
		txtRemain_hide(textareaJObj);
	}
}

Template = {
	html : function() {
	    var linesFun = function(){
			/*
				<div class="modal-dialog" id="myModal">
				    <div class="modal-content01 pst">
				        <div class="bs-example bs-example-tabs">
				          <div class="modaltab01"> 
				              <ul class="modal-tab fl">
				                <li class="active" id="__textEditor" style="display:none"><a href="javascript:void(0)" onclick="tLp5Click(this);return false;" class="tab01" style="display:none;">文字</a><p class="tab01">文字</p></li>
				                <li class="active"><a href="javascript:void(0)" onclick="tLp1Click(this);return false;" class="tab02">图文</a><p class="tab02" style="display:none;">图文</p></li>
				                <li class="active"><a href="javascript:void(0)" onclick="tLp3Click(this);return false;" class="tab03">语音</a><p class="tab03" style="display:none;">语言</p></li>
				                <li class="active"><a href="javascript:void(0)" onclick="tLp2Click(this);return false;" class="tab04">视频</a><p class="tab04" style="display:none;">视频</p></li>
				                <li class="active"><a href="javascript:void(0)" onclick="tLp4Click(this);return false;" class="tab05">图片</a><p class="tab05" style="display:none;">图片</p></li>
				              </ul>
				          </div>    
				          <div class="modal-cn01" id="ansSelContain" style="">
				            <div class="modal-text tl" style="display:none;" id="textDiv">
				              <textarea name="" cols="" rows="" class="form-control" style="width:96.2111111%;height:200px;"></textarea>
				              <div class="modal-textfooter">
				              	<p><a href=""><img src="../images/modal/weixin_face.jpg"></a></p>
				                <div>还可以输入 <span>297</span> 个字</div>
				              </div>
				            </div>
				          </div>
				        </div>
				        <div class="modal-footercn1 tr" style="display:none">
				            <button type="button" class="btn btn-primary ml10" onclick="selectSubmit()">保 存</button> <button type="button" onclick="closeIntegeration()" class="btn btn-default ml10 mr20" >取 消</button>
				        </div>
				    </div>
				</div>
			*/
		}
		var lines = new String(linesFun);
 		lines = lines.substring(lines.indexOf("/*") + 2,
                lines.lastIndexOf("*/"));
 		return lines;
	} 
}

function faceSel(faceA){
	var selFaceTitle = $(faceA).attr('title');
	var textareaJObj = $(faceA).parent().parent().parent().parent().parent().parent().parent().find('textarea');
	textareaJObj.insertAtCaret('['+selFaceTitle+']');
	checkTextLength(textareaJObj[0]);
	$(faceA).parent().parent().hide();
}

function faceToggle(selectBtn){
	var offsetObj = $(selectBtn).offset();
	var faceImg = $(selectBtn).next();
	if(faceImg.is(":hidden")){
		faceImg[0].style.bottom = "25px";
		faceImg[0].style.left = "5px";
		faceImg.show();
	}else{
		faceImg.hide();
	}
}

selectSubmit = function(directReturn) {
	if(window.frames["material_iframe"]){
		var t = $(window.frames["material_iframe"].document)
		.find("input[name='file']:checked").val();
		var selectedValJObj = $("#selectedVal",
				window.frames["material_iframe"].document);
		var type = $("#type", window.frames["material_iframe"].document).val();
		if (selectedValJObj && selectedValJObj.val()) {
			if (type == 'imgtxtmsg') {
				if (directReturn)
					return {'type':'imgtxtmsg','answer':selectedValJObj.val()};
				else
					tLp1ClickCb(selectedValJObj.val());
			} else if (type == 'imagemsg') {
				if (directReturn)
					return {'type':'imgmsg','answer':selectedValJObj.val()};
				else
					tLp4ClickCb(selectedValJObj.val());
			} else if (type == 'audiomsg') {
				if (directReturn)
					return {'type':'audiomsg','answer':selectedValJObj.val()};
				else
					tLp3ClickCb(selectedValJObj.val());
			} else if (type == 'videomsg') {
				if (directReturn)
					return {'type':'videomsg','answer':selectedValJObj.val()};
				else
					tLp2ClickCb(selectedValJObj.val());
			}
			closeIntegeration();
		} else {
			alert("请至少选择一项");
		}
	}else{
		var ansVal = $('textarea',$('#ansSelContain')).val();
		if(!ansVal){
			alert('请输入文字内容');
		}else{
			if (directReturn)
				return ansVal;
			tLp5ClickCb(ansVal);
			closeIntegeration();
		}
	}
}

function tLp1Click(triggerObj){
	var currentFoucsTextareaJObj = $(triggerObj).parent().parent().parent().next().find('textarea');
	renderBar(currentFoucsTextareaJObj[0],'imgtxtmsg');
	
	$('#material_iframe',$('#ansSelContain')).remove();
	$('#textDiv',$('#ansSelContain')).hide();
	var iframeJObj = $('<iframe id="material_iframe" name="material_iframe" frameborder="0" style="width: 100%; height: 400px;"/>')	;
	iframeJObj.attr('src','../../../matmgr/imgtxt-msg!list.action');
	$('#ansSelContain').append(iframeJObj);
}

function tLp1ClickCb(imgId){
	if(window.parent.answerRendered){
		window.parent.answerRendered({'type':'imgtxtmsg','answer':imgId});
	}
}

function tLp2Click(triggerObj){
	var currentFoucsTextareaJObj = $(triggerObj).parent().parent().parent().next().find('textarea');
	renderBar(currentFoucsTextareaJObj[0],'videomsg');
	
	$('#material_iframe',$('#ansSelContain')).remove();
	$('#textDiv',$('#ansSelContain')).hide();
	var iframeJObj = $('<iframe id="material_iframe" name="material_iframe" frameborder="0" style="width: 100%; height: 460px;"/>')	;
	iframeJObj.attr('src','../../../matmgr/video-msg!list.action');
	$('#ansSelContain').append(iframeJObj);
}

function tLp2ClickCb(imgId){
	if(window.parent.answerRendered){
		window.parent.answerRendered({'type':'videomsg','answer':imgId});
	}
}

function tLp3Click(triggerObj){
	var currentFoucsTextareaJObj = $(triggerObj).parent().parent().parent().next().find('textarea');
	renderBar(currentFoucsTextareaJObj[0],'audiomsg');
	
	$('#material_iframe',$('#ansSelContain')).remove();
	$('#textDiv',$('#ansSelContain')).hide();
	var iframeJObj = $('<iframe id="material_iframe" name="material_iframe" frameborder="0" style="width: 100%; height: 460px;"/>')	;
	iframeJObj.attr('src','../../../matmgr/audio-msg!list.action');
	$('#ansSelContain').append(iframeJObj);
}

function tLp3ClickCb(imgId){
	if(window.parent.answerRendered){
		window.parent.answerRendered({'type':'audiomsg','answer':imgId});
	}
}

function tLp4Click(triggerObj){
	var currentFoucsTextareaJObj = $(triggerObj).parent().parent().parent().next().find('textarea');
	renderBar(currentFoucsTextareaJObj[0],'imagemsg');
	
	$('#material_iframe',$('#ansSelContain')).remove();
	$('#textDiv',$('#ansSelContain')).hide();
	var iframeJObj = $('<iframe id="material_iframe" name="material_iframe" frameborder="0" style="width: 100%; height: 460px;"/>')	;
	iframeJObj.attr('src','../../../matmgr/image-msg!list.action');
	$('#ansSelContain').append(iframeJObj);
}

function tLp4ClickCb(imgId){
	if(window.parent.answerRendered){
		window.parent.answerRendered({'type':'imagemsg','answer':imgId});
	}
}

function tLp5Click(triggerObj,answer){
	var currentFoucsTextareaJObj = $(triggerObj).parent().parent().parent().next().find('textarea');
	$('#material_iframe',$('#ansSelContain')).remove();
	$('#textDiv',$('#ansSelContain')).show();
	renderBar(currentFoucsTextareaJObj[0],'text');
}

function tLp5ClickCb(text){
	if(window.parent.answerRendered){
		window.parent.answerRendered({'type':'text','answer':text});
	}
}

function renderBar(textareaDomObj,type){
	var textareaJObj = $(textareaDomObj);
	if(type){
		var typeBar = textareaJObj.parent().parent().prev();
		typeBar.find('a').show();
		typeBar.find('p').hide();
		if (type == 'text') {
			var liJObj = typeBar.find('li')[0];
		} else if (type == 'imagemsg') {
			var liJObj = typeBar.find('li')[4];
		} else if (type == 'audiomsg') {
			var liJObj = typeBar.find('li')[2];
		} else if (type == 'imgtxtmsg') {
			var liJObj = typeBar.find('li')[1];
		} else if (type == 'videomsg') {
			var liJObj = typeBar.find('li')[3];
		}
		$(liJObj).children('a').hide();
		$(liJObj).children('p').show();
	}
}

function loadAnswerByType(id,type,currentFoucsTextareaJObj){
	var url ;
	if(type=='imgtxtmsg'){
		url = "imgtxt-msg!getImgmsg.action?imgmsgId=";
	}else if(type=='imagemsg'){
		url = "matmgr/imagemsg/imageItem.jsp?imageId=";
	}else if(type =='audiomsg'){
		url = "matmgr/audiomsg/audioItem.jsp?audioId=";
	}else if(type =='videomsg'){
		url = "matmgr/videomsg/videoItem.jsp?videoId=";
	}
	if(currentFoucsTextareaJObj.next('iframe').length > 0){
		currentFoucsTextareaJObj.next('iframe').remove();
	}
	if(currentFoucsTextareaJObj.next('div').length > 0 ){
		currentFoucsTextareaJObj.next('div').remove();
	}
	var iframeJObj = $('<iframe/>').attr('src',url+id).attr('style', 'border: 0 none;height: 100%;width: 98%;padding-left:10px;');
	currentFoucsTextareaJObj.after(iframeJObj);
	currentFoucsTextareaJObj.hide();
	renderBar(currentFoucsTextareaJObj,type);
	iframeJObj.load(function(){ 
		iframeJObj[0].contentWindow.document.onclick=function(){
			renderBar(currentFoucsTextareaJObj,type);
		}
    }); 
}

function checkTextLength(currentFoucsTextareaDomObj) {
	var textareaJObj = $(currentFoucsTextareaDomObj);
	if (textareaJObj) {
		var remainDivJObj = textareaJObj.parent().parent().parent().find('div[class="CMOP-facetext f_r CM-fz14 CM-cl03 ov_fl"]');
		var inputText = textareaJObj.val();
		var usedSum = parseInt(sum(inputText)/2);
		remainDivJObj.children('span').html(defaultTxtLimit - usedSum);
		txtRemain_show(remainDivJObj);
	}
}

function checkTextLengthValid(answer) {
	var usedSum = parseInt(sum(answer)/2);
	if (usedSum > defaultTxtLimit) {
		return false;
	}else{
		return true;
	}
}

function sum(a) {
    if (!a)
        return 0;
    var b = a.match(/[^\x00-\xff]/g);
    return a.length + (b ? b.length : 0)
}


function txtRemain_show(textareaJObj){
	textareaJObj.parent().parent().parent().find('div[class="CMOP-facetext f_r CM-fz14 CM-cl03 ov_fl"]').show();
	textareaJObj.parent().parent().parent().find('img[name="faceBtn"]').show();
}

function txtRemain_hide(textareaJObj){
	textareaJObj.parent().parent().parent().find('div[class="CMOP-facetext f_r CM-fz14 CM-cl03 ov_fl"]').hide();
	var faceBtnJObj = textareaJObj.parent().parent().parent().find('img[name="faceBtn"]');
	faceBtnJObj.hide();
	faceBtnJObj.next().hide();
}

$.fn.extend({
insertAtCaret: function(myValue){
var $t=$(this)[0];
if (document.selection) {
this.focus();
sel = document.selection.createRange();
sel.text = myValue;
this.focus();
}
else 
if ($t.selectionStart || $t.selectionStart == '0') {
var startPos = $t.selectionStart;
var endPos = $t.selectionEnd;
var scrollTop = $t.scrollTop;
$t.value = $t.value.substring(0, startPos) + myValue + $t.value.substring(endPos, $t.value.length);
this.focus();
$t.selectionStart = startPos + myValue.length;
$t.selectionEnd = startPos + myValue.length;
$t.scrollTop = scrollTop;
}
else {
this.value += myValue;
this.focus();
}
}
}) 