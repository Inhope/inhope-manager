<%@page import="org.springframework.context.ApplicationContext"%>
<%@page import="org.codehaus.jackson.map.ObjectMapper"%>
<%@page import="com.incesoft.xmas.commons.LicenceHolder"%>
<%@page import="com.incesoft.xmas.commons.LabsUtil"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="res" uri="/WEB-INF/tld/import-res.tld"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="usr" uri="/WEB-INF/tld/login-user.tld"%>
<%@taglib prefix="mdl" uri="/WEB-INF/tld/mdl-ft.tld"%>
<%
	ApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(application);
	LicenceHolder licenceholder = ctx.getBean(LicenceHolder.class);
	if (licenceholder != null && licenceholder.getLicence() != null) {
		request.setAttribute("supportPlatforms", licenceholder.getLicence().getSupportedPlatforms());
	}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>通用聊天</title>
<usr:checkPerm jsExportVar="kb_check_perm_result" permissionNames="kb" dump="true" />
<usr:name jsExportVar="kb_username" />
<mdl:dump name="kb" jsExportVar="kb_mdl_ft" />
<mdl:dump name="kb" jsExportVar="kb_mdl_props" type="props" />
<script src="<c:url value="/ext4/ext-all.js"/>" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href='<c:url value="/ext4/ext-all.css"/>'/>
<link rel="stylesheet" type="text/css" href="<c:url value="/kbmgr/custom.css"/>" />
<link rel="stylesheet" type="text/css" href="<c:url value="/kbmgr/multitools.css"/>" />
<script src="<c:url value="/kbmgr/js/chat/commons.js"/>" type="text/javascript"></script>
<script type="text/javascript">
	var isAdd = http_get('../app/g/kbmgr/chat-category!isAllow.action?perm=chat.mgr.C');
	var isImpt = http_get('../app/g/kbmgr/chat-category!isAllow.action?perm=chat.mgr.IMP');
	var isExpt = http_get('../app/g/kbmgr/chat-category!isAllow.action?perm=chat.mgr.EXP');
	var isDel = http_get('../app/g/kbmgr/chat-category!isAllow.action?perm=chat.mgr.D');
	var sys_labs = <%=LabsUtil.isLabs()%>
</script>
<script src="<c:url value="/kbmgr/js/chat/index.js"/>" type="text/javascript"></script>
<script src="<c:url value="/kbmgr/js/chat/rowspan.js"/>" type="text/javascript"></script>
<script src="<c:url value="/kbmgr/js/chat/PrefabHelper.js"/>" type="text/javascript"></script>
<script src="<c:url value="/kbmgr/js/chat/ChatNav.js"/>" type="text/javascript"></script>
<script src="<c:url value="/kbmgr/js/chat/ChatDetail.js"/>" type="text/javascript"></script>
<script src="<c:url value="/kbmgr/js/chat/ChatType.js"/>" type="text/javascript"></script>
</head>

<body>
</body>
</html>