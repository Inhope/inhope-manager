<%@page import="org.springframework.context.ApplicationContext"%>
<%@page import="org.codehaus.jackson.map.ObjectMapper"%>
<%@page import="com.incesoft.xmas.commons.LicenceHolder"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="res" uri="/WEB-INF/tld/import-res.tld"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="usr" uri="/WEB-INF/tld/login-user.tld"%>
<%@taglib prefix="mdl" uri="/WEB-INF/tld/mdl-ft.tld"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>语义统计</title>
<usr:checkPerm jsExportVar="kb_check_perm_result" permissionNames="kb" dump="true" />
<usr:name jsExportVar="kb_username" />
<mdl:dump name="kb" jsExportVar="kb_mdl_ft" />
<mdl:dump name="kb" jsExportVar="kb_mdl_props" type="props" />
<script src="<c:url value="/ext4/ext-all.js"/>" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href='<c:url value="/ext4/ext-all.css"/>'/>
<link rel="stylesheet" type="text/css" href="<c:url value="/kbmgr/custom.css"/>" />
<link rel="stylesheet" type="text/css" href="<c:url value="/kbmgr/multitools.css"/>" />
<script type="text/javascript">
function getxhr() {
	var _xhr = false;
	try {
		_xhr = new ActiveXObject("Msxml2.XMLHTTP");
	} catch (e) {
		try {
			_xhr = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (e2) {
			_xhr = false;
		}
	}
	if (!_xhr && window.XMLHttpRequest)
		_xhr = new XMLHttpRequest();
	return _xhr;
}
function http_get(url){
var xhr = getxhr();
xhr.open('GET', url, false);
xhr.send();
return xhr.responseText;
}

var p = window.parent.Ext.getCmp("count_p");
</script>




<script src="<c:url value="/kbmgr/js/AbstractSemanticTaskCountDtlPanel.js"/>" type="text/javascript"></script>
</head>

<body>
</body>
</html>