<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<TITLE>Directory: ${basePath}</TITLE>
</head>
<body>
<H1>Directory: ${basePath}</H1>
<TABLE BORDER=0>
<TR><TD><A HREF="${basePath}../">Parent Directory</A></TD><TD></TD><TD></TD></TR>
<c:forEach var="f" items="${fileData}">
<TR><TD>${f[2]=='1'?'+ ':'- '}<A HREF="${basePath}${f[0]}">${f[0]}</A></TD><TD>${f[1]}</TD><TD></TD></TR>
</c:forEach>
</BODY>
</body>
</html>