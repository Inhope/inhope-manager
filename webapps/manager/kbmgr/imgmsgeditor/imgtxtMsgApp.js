var clickDivId;
var validateImgSuffix = true;
var imgmsgId = "";
function updateEditImgMsg(selectUpdateDivId, isSampleImgmsg) {
	autoUpdateContent(isSampleImgmsg);
	if (!isSampleImgmsg || !("true" == isSampleImgmsg)) {
		var pTip = document.getElementById("imgmsg-upload-tip");
		var editArea = document.getElementById("imgmsg-editer");
		var title = document.getElementById("imgmsg_edit_title");
		var imgAddress = document.getElementById("imgmsg_edit_imgurl");
		var sourceInput = document.getElementById("imgmsg_edit_source");
		var urlContentTextarea = document.getElementById("imgmsg_edit_urlContent");
		title.value = "";
		imgAddress.value = "";
		sourceInput.value = "";
		var divId = selectUpdateDivId;
		clickDivId = !divId ? selectUpdateDivId : divId;
		var itemDiv = document.getElementById(clickDivId);
		var msgArrowOut = document.getElementById("msg-arrow-out");
		var msgArrowIn = document.getElementById("msg-arrow-in");
		var childNs = itemDiv.childNodes;
		if ("imgmsgItem1" == clickDivId) {
			var sampleImgmsgHtmlAuth = document.getElementById("sampleImgmsg_htmlAuth");
			var imgmsgEditAuth = document.getElementById("imgmsg_edit_auth");
			imgmsgEditAuth.value = sampleImgmsgHtmlAuth.value;
			var marginstr = "0px 10px 0px 0px";
			editArea.style.margin = marginstr;
			msgArrowOut.style.margin = "44px" + " 0 0 0";
			msgArrowIn.style.margin = "44px" + " 0 0 0";
			pTip.innerHTML = "大图片建议尺寸:360像素*200像素";
			for (var i = 0; i < childNs.length; i++) {
				var seleNode = childNs[i];
				if (seleNode.nodeType == 1) {
					var nodeH4 = seleNode.getElementsByTagName("h4")[0];
					if (nodeH4) {
						var nodes = nodeH4.childNodes;
						for (var l = 0; l < nodes.length; l++) {
							var titleNode = nodes[l];
							if (titleNode.nodeType == 1) {
								// var titleValue = navigator.userAgent
								// .indexOf("MSIE") > 0
								// ? titleNode.outerText
								// : titleNode.innerText;
								var titleValue = titleNode.innerHTML;
								if (titleValue && "标题" != titleValue) {
									title.value = titleValue;
								} else {
									title.value = "";
								}
								break;
							}
						}
					}

					var nodeImg = seleNode.getElementsByTagName("img")[0];
					if (nodeImg) {
						var imgValue = nodeImg.src;
						if (imgValue)
							imgAddress.value = imgValue;
					}
					var sourceNode = seleNode.getElementsByTagName("input")[0];
					if (sourceNode && "source" == sourceNode.name) {
						sourceInput.value = sourceNode.value ? sourceNode.value : "http://";
					}
					var urlContentArea = seleNode.getElementsByTagName("textarea")[0];
					if (urlContentArea && "urlContent" == urlContentArea.name) {
						urlContentTextarea.innerText = urlContentArea.value;
						CKEDITOR.instances.imgmsg_edit_urlContent.setData(urlContentArea.value);
						return;
					}
				}
			}
		} else {
			var imgmsgHtmlAuth = document.getElementById("sampleImgmsg_htmlAuth" + clickDivId.replace("appmsgItem", ""));
			var imgmsgEditAuth = document.getElementById("imgmsg_edit_auth");
			imgmsgEditAuth.value = imgmsgHtmlAuth.value;
			var selectedDiv = document.getElementById(clickDivId);
			var marginstr = (selectedDiv.offsetTop - 50) + "px" + " 10px 0px 0px";
			editArea.style.margin = marginstr;
			msgArrowOut.style.margin = 44 + (selectedDiv.offsetTop - 50) + "px" + " 0 0 0";
			msgArrowIn.style.margin = 44 + (selectedDiv.offsetTop - 50) + "px" + " 0 0 0";
			pTip.innerHTML = "小图片建议尺寸:80像素*80像素";
			for (var i = 0; i < childNs.length; i++) {
				var itemNodes = childNs[i];
				if (itemNodes.nodeType == 1) {
					var sourceNode = itemNodes.nextSibling.nextSibling;
					var h4Node = itemNodes.nextSibling;
					var spanNode = h4Node.getElementsByTagName("span")[0];
					if (spanNode) {
						// var titleValue = navigator.userAgent.indexOf("MSIE")
						// > 0
						// ? spanNode.outerText
						// : spanNode.innerText;
						var titleValue = spanNode.innerHTML;
						if (titleValue && "标题" != titleValue) {
							title.value = titleValue;
						} else {
							title.value = "";
						}
					}
					var imgNode = itemNodes.getElementsByTagName("img")[0];
					if (imgNode) {
						var img = imgNode.src;
						if (img)
							imgAddress.value = img.split("?imgmsg")[0];
					}
					if ("source" == sourceNode.name) {
						if (sourceNode.value) {
							sourceInput.value = sourceNode.value ? sourceNode.value : "http://";
							addSourceUrl();
						}
					}
					var urlContentNode = itemDiv.getElementsByTagName("textarea")[0];
					if ("urlContent" == urlContentNode.name) {
						urlContentTextarea.innerText = urlContentNode.value;
						CKEDITOR.instances.imgmsg_edit_urlContent.setData(urlContentNode.value);
						return;
					}

				}
			}
		}
	}
}
function removeItem(selectRemoveDiv) {
	// 删除
	var removeDiv = document.getElementById(selectRemoveDiv);
	var nodeItemSize = 0
	for (var i = 2; i < 9; i++) {
		var addDiv = document.getElementById('appmsgItem' + i);
		if (addDiv)
			nodeItemSize++;
	}
	if (nodeItemSize <= 1) {
		alert('提示:多图文至少需要两条消息');
		return;
	}

	// ++++++++++++++++++++++++++++++++++++++++++
	var title = document.getElementById("imgmsg_edit_title");
	var imgAddress = document.getElementById("imgmsg_edit_imgurl");
	var sourceInput = document.getElementById("imgmsg_edit_source");
	var urlContentTextarea = document.getElementById("imgmsg_edit_urlContent");
	title.value = "";
	imgAddress.value = "";
	sourceInput.value = "";
	document.getElementById("img_msg_item").removeChild(removeDiv);
	var pTip = document.getElementById("imgmsg-upload-tip");
	var msgArrowOut = document.getElementById("msg-arrow-out");
	var msgArrowIn = document.getElementById("msg-arrow-in");
	var editArea = document.getElementById("imgmsg-editer");
	var sampleImgmsgHtmlAuth = document.getElementById("sampleImgmsg_htmlAuth");
	var imgmsgEditAuth = document.getElementById("imgmsg_edit_auth");
	imgmsgEditAuth.value = sampleImgmsgHtmlAuth.value;
	var marginstr = "0px 10px 0px 0px";
	editArea.style.margin = marginstr;
	msgArrowOut.style.margin = "44px" + " 0 0 0";
	msgArrowIn.style.margin = "44px" + " 0 0 0";
	pTip.innerHTML = "大图片建议尺寸:360像素*200像素";
	var itemDiv = document.getElementById("imgmsgItem1");
	var childNs = itemDiv.childNodes;
	for (var i = 0; i < childNs.length; i++) {
		var seleNode = childNs[i];
		if (seleNode.nodeType == 1) {
			var nodeH4 = seleNode.getElementsByTagName("h4")[0];
			if (nodeH4) {
				var nodes = nodeH4.childNodes;
				for (var l = 0; l < nodes.length; l++) {
					var titleNode = nodes[l];
					if (titleNode.nodeType == 1) {
						var titleValue = titleNode.innerHTML;
						if (titleValue && "标题" != titleValue) {
							title.value = titleValue;
						} else {
							title.value = "";
						}
						break;
					}
				}
			}

			var nodeImg = seleNode.getElementsByTagName("img")[0];
			if (nodeImg) {
				var imgValue = nodeImg.src;
				if (imgValue)
					imgAddress.value = imgValue;
			}
			var sourceNode = seleNode.getElementsByTagName("input")[0];
			if (sourceNode && "source" == sourceNode.name) {
				sourceInput.value = sourceNode.value ? sourceNode.value : "http://";
			}
			var urlContentArea = seleNode.getElementsByTagName("textarea")[0];
			if (urlContentArea && "urlContent" == urlContentArea.name) {
				urlContentTextarea.innerText = urlContentArea.value;
				CKEDITOR.instances.imgmsg_edit_urlContent.setData(urlContentArea.value);
				return;
			}
		}
	}
	// ++++++++++++++++++++++++++++++++++++++++++
}

function uploadSubmit(imgmId, isSample) {
	if (validateImgSuffix) {
		var form = document.getElementById("imgmsgImgUpload");
		if (!form.uploadFile.value) {
			alert("上传文件不能为空");
			return;
		}
		var ele = form.elements;
		var leng = ele.length;
		if (leng > 2) {
			for (var e = 0; e < leng; e++) {
				var el = ele[e];
				var eName = el.name;
				if ("imgmsgId" == eName)
					el.value = imgmId ? imgmId : (imgmsgId ? imgmsgId : "");
				else if ("itemId" == eName)
					el.value = !clickDivId ? "imgmsgItem1" : clickDivId;
				else if ("isSample" == eName)
					el.value = isSample
			}
		}
		var subData = {
			"imgmsgId" : imgmId ? imgmId : (imgmsgId ? imgmsgId : ""),
			"itemId" : !clickDivId ? "imgmsgItem1" : clickDivId,
			"isSample" : isSample
		};
		formAddElement(form, subData);

		form.submit();
	} else {
		alert("非法的图片格式");
		return;
	}
}
function validateImg(name) {
	var filename = name.substring(name.lastIndexOf(".") + 1).toLowerCase();
	if (filename != "jpg" && filename != "gif" && filename != "png" && filename != "bmp" && filename != "jpeg") {
		alert("非法的图片格式");
		validateImgSuffix = false;
		return;
	}
	validateImgSuffix = true;
	return;
}

function validateTitle(titleContent) {
	if (titleContent) {
		var length = getBytesLength(titleContent);
		if (length > 64) {
			alert("提示:摘要内容不能超过64个字符");
			return 1;
		}
	}
	return -1;
}

function updateTitle(title, isSample) {
	if ("true" == isSample) {
		var sampleImgmsgTitle = document.getElementById("sampleImgmsg_title");
		sampleImgmsgTitle.innerHTML = title;
	} else {
		clickDivId = !clickDivId ? "imgmsgItem1" : clickDivId;
		var itemDiv = document.getElementById(clickDivId);
		if (itemDiv == null)
			clickDivId = "imgmsgItem1";
		itemDiv = document.getElementById(clickDivId);
		var childNs = itemDiv.childNodes;
		if ("imgmsgItem1" == clickDivId) {
			for (var i = 0; i < childNs.length; i++) {
				var seleNode = childNs[i];
				if (seleNode.nodeType == 1) {
					var nodeH4 = seleNode.getElementsByTagName("h4");
					if (nodeH4.length > 0) {
						var nodes = nodeH4[0].childNodes;
						for (var i = 0; i < nodes.length; i++) {
							var nodeSpan = nodes[i];
							if (nodeSpan.nodeType == 1) {
								nodeSpan.innerHTML = title;
								return;
							}
						}
					}
				}
			}
		} else {
			for (var i = 0; i < childNs.length; i++) {
				var itemNodes = childNs[i];
				if (itemNodes.nodeType == 1) {
					var h4Node = itemNodes.nextSibling;
					var spanNode = h4Node.getElementsByTagName("span")[0];
					spanNode.innerHTML = title;
					return;
				}
			}
		}
	}
	validateTitle(title);
}

function updateAuth(authName, isSample) {
	if ("true" == isSample) {
		var sampleImgmsgHtmlAuth = document.getElementById("sampleImgmsg_htmlAuth");
		sampleImgmsgHtmlAuth.value = authName;
	} else {
		clickDivId = !clickDivId ? "imgmsgItem1" : clickDivId;
		var itemDiv = document.getElementById(clickDivId);
		if (itemDiv == null)
			clickDivId = "imgmsgItem1";
		itemDiv = document.getElementById(clickDivId);
		var childNs = itemDiv.childNodes;
		if ("imgmsgItem1" == clickDivId) {
			var sampleImgmsgHtmlAuth = document.getElementById("sampleImgmsg_htmlAuth");
			sampleImgmsgHtmlAuth.value = authName;
		} else {
			var index = clickDivId.replace("appmsgItem", "");
			var sampleImgmsgHtmlAuth = document.getElementById("sampleImgmsg_htmlAuth" + index);
			sampleImgmsgHtmlAuth.value = authName;
		}
	}
}

function validateTag(tagContent) {
	if (tagContent) {
		if (!(/^[a-zA-Z0-9\u4e00-\u9fa5]+([,，][0-9A-Za-z\u4e00-\u9fa5]+)*$/.test(tagContent))) {
			alert('提示：标签格式不合法，(字母、数字、中文字符，多个以逗号间隔)');
			return 1;
		}
		tagContent = tagContent.replace(/，/g, ",");
		var tag = tagContent.split(',');
		if (tag.length > 5) {
			alert('提示：标签最多不能超过5个');
			return 1;
		}
		for (var i = 0; i < tag.length; i++) {
			if (tag[i].length > 5) {
				alert('提示：标签长度不能超过5个字符');
				return 1;
			}
		}
	}
	return -1;
}

function updateSource(url, isSample) {
	if (url && url != "http://") {
		var startTag = url.substring(0, 4);
		url = (("http" != startTag) ? ("http://" + url) : url);// 如果不是以http开头，加上http://
	} else {
		url = "";
	}
	if ("true" == isSample) {
		var sampleImgmsgUrl = document.getElementById("sampleImgmsg_source");
		sampleImgmsgUrl.value = url;
	} else {
		clickDivId = !clickDivId ? "imgmsgItem1" : clickDivId;
		var itemDiv = document.getElementById(clickDivId);
		var childNs = itemDiv.childNodes;
		if ("imgmsgItem1" == clickDivId) {
			for (var i = 0; i < childNs.length; i++) {
				var seleNode = childNs[i];
				if (seleNode.nodeType == 1) {
					var nodeInput = seleNode.getElementsByTagName("input")[0];
					if (nodeInput && "source" == nodeInput.name) {
						nodeInput.value = url;
						return;
					}
				}
			}
		} else {
			for (var i = 0; i < childNs.length; i++) {
				var itemNodes = childNs[i];
				if (itemNodes.nodeType == 1) {
					var inputNode = itemNodes.nextSibling.nextSibling;
					if (inputNode && "source" == inputNode.name) {
						inputNode.value = url;
						return;
					}
				}
			}
		}
	}
}

function updateDescription(content, isSample) {
	if ("true" == isSample) {
		var length = getBytesLength(content);
		if (length > 120) {
			alert("提示:摘要内容不能超过120个字符");
			return;
		} else {
			var descriptionInput = document.getElementById("sampleImgmsg_description");
			descriptionInput.textContent = content;
		}
	}
}

function updateContent(content, isSample) {
	// var contentHtml = "<html><head><meta http-equiv=\"Content-Type\"
	// content=\"text/html; charset=UTF-8\"></head><body>"
	// + content + "</body></html>";
	var contentHtml = content;
	if ("true" == isSample) {
		var sampleTitleContent = document.getElementById("sampleImgmsg_urlContent");
		if (content)
			sampleTitleContent.value = contentHtml;
		else
			sampleTitleContent.value = "";
	} else {
		clickDivId = !clickDivId ? "imgmsgItem1" : clickDivId;
		var itemDiv = document.getElementById(clickDivId);
		var childNs = itemDiv.childNodes;
		if ("imgmsgItem1" == clickDivId) {
			for (var i = 0; i < childNs.length; i++) {
				var seleNode = childNs[i];
				if (seleNode.nodeType == 1) {
					var nodeInput = seleNode.getElementsByTagName("textarea")[0];
					if (nodeInput && "urlContent" == nodeInput.name) {
						if (content) {
							nodeInput.value = contentHtml;
						} else {
							nodeInput.value = "";
						}
						break;
					}
				}
			}
		} else {
			var textareaNode = itemDiv.getElementsByTagName("textarea")[0];
			if (textareaNode && "urlContent" == textareaNode.name) {
				if (content) {
					textareaNode.value = contentHtml;
				} else {
					textareaNode.value = "";
				}
			}
		}
	}
}

function getBytesLength(value) {
	return value.replace(/[^\x00-\xff]/gi, "-").length;
}

function addImgMsg() {
	var addButtonDiv = document.getElementById('addImgMsg');
	var addDiv = document.getElementById('appmsgItemShow');
	addDiv.style.display = "block";
	var parentNode = addButtonDiv.parentNode;
	var previousNode = addButtonDiv;
	while (previousNode.nodeType != 1) {
		previousNode = previousNode.previousSibling;
	}
	var addNode = addDiv.cloneNode();
	var childNodes = parentNode.childNodes;
	var idx = 0;
	for (var i = 0; i < childNodes.length; i++) {
		if (childNodes[i].nodeType == 1)
			idx++;
	}
	if (idx > 8) {
		alert('最多只能加入8条图文消息');
		return;
	}
	var divId = "appmsgItem" + (idx);
	addNode.id = divId;
	addNode.innerHTML = '<span class="thumb"> <span class="default-tip"' + '" style="">缩略图</span>' + '<img class="i-img" style="display: none">'
	  + '</span><h4 class="msg-t"><span class="i-title">标题</span></h4>' + '<input type="hidden" name="source" value=""/>'
	  + '<textarea type="hidden" style="display:none" name="urlContent" value=""></textarea>' + '<input type="hidden" id="sampleImgmsg_htmlAuth' + idx + '" value="" />'
	  + '<ul class="abs tc sub-msg-opr" style="display: none;"><li class="b-dib sub-msg-opr-item">' + '<a href="javascript:updateEditImgMsg(\'' + divId
	  + '\');" class="th icon18 iconEdit" data-rid="2">编辑</a></li>' + '<li class="b-dib sub-msg-opr-item"><a href="javascript:removeItem(\'' + divId
	  + '\');" class="th icon18 iconDel" data-rid="2">删除</a>' + '</li></ul>';
	parentNode.insertBefore(addNode, addButtonDiv);
	addDiv.style.display = "none";
}

function formAddElement(form, addData) {
	for (var n in addData) {
		var newElement = document.createElement("input");
		newElement.setAttribute("type", "hidden");
		newElement.name = n;
		newElement.value = addData[n];
		form.appendChild(newElement);
	}
}

// 上传图片回调函数
function submitCallback(id, selectedDiv, suffix, isSample) {
	if ("error" == id || !id) {
		alert("文件上传失败");
	} else {
		imgmsgId = id;
		var imgSrc = "../imgmsgfile/" + id + "/" + selectedDiv + suffix + "?t=" + (new Date()).valueOf();
		if ("true" == isSample) {
			var sampleTitleImg = document.getElementById("sampleImgmsg_img");
			var sampleTitleImgMask = document.getElementById("sampleImgmsg_imgMask");
			sampleTitleImgMask.style.display = "none";
			sampleTitleImg.style.display = "block";
			sampleTitleImg.src = imgSrc;
		} else {
			var itemDiv = document.getElementById(selectedDiv);
			var childNs = itemDiv.childNodes;
			if ("imgmsgItem1" == selectedDiv) {
				for (var i = 0; i < childNs.length; i++) {
					var seleNode = childNs[i];
					if (seleNode.nodeType == 1) {
						var nodeP = seleNode.getElementsByTagName("p")[0];
						var nodeImg = seleNode.getElementsByTagName("img")[0];
						if (nodeP && nodeImg) {
							nodeP.style.display = "none";
							nodeImg.style.display = "block";
							nodeImg.src = imgSrc;
							return;
						}
					}
				}
			} else {
				for (var i = 0; i < childNs.length; i++) {
					var seleNode = childNs[i];
					if (seleNode.nodeType == 1) {
						var spanNode = seleNode.getElementsByTagName("span")[0];
						var imgNode = seleNode.getElementsByTagName("img")[0];
						if (spanNode && imgNode) {
							spanNode.style.display = "none";
							imgNode.style.display = "block";
							imgNode.src = imgSrc;
							return;
						}
					}
				}
			}
		}
	}
}

Date.prototype.format = function(format) {
	var date = {
		"M+" : this.getMonth() + 1,
		"d+" : this.getDate(),
		"h+" : this.getHours(),
		"m+" : this.getMinutes(),
		"s+" : this.getSeconds(),
		"q+" : Math.floor((this.getMonth() + 3) / 3),
		"S+" : this.getMilliseconds()
	};
	if (/(y+)/i.test(format)) {
		format = format.replace(RegExp.$1, (this.getFullYear() + '').substr(4 - RegExp.$1.length));
	}
	for (var k in date) {
		if (new RegExp("(" + k + ")").test(format)) {
			format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? date[k] : ("00" + date[k]).substr(("" + date[k]).length));
		}
	}
	return format;
}

function autoUpdateContent(isSampleImgmsg) {
	var content = CKEDITOR.instances.imgmsg_edit_urlContent.getData();
	var reg = /<p>\s*<meta (?:.+?)>\s*<\/p>/i;
	content = content.replace(reg, '');
	updateContent(content, isSampleImgmsg);
}

var imgmsgHtmlTitle = "<link rel=\"stylesheet\" type=\"text/css\" href=\"../../imgtxtMsgData/css/imgmsgHtml.css\">";
function saveImgMsg(imgmsgid, isSampleImgmsg) {
	document.getElementById("appmsgmask").style.display = "block";
	document.getElementById("appmsgmaskProgress").style.display = "block";
	autoUpdateContent(isSampleImgmsg); // 修改完正文，直接保存，正文会有没有更新的情况。这里，再次更新一下
	imgmsgId = imgmsgid ? imgmsgid : imgmsgId;
	var imgMsgItem = [];
	var imgmsg1 = {};
	var urlContent1;
	var date = new Date().format('yyyy年MM月dd日');
	if ("true" == isSampleImgmsg) { // 单图文消息
		var title = document.getElementById("sampleImgmsg_title");
		var img = document.getElementById("sampleImgmsg_img");
		var description = document.getElementById("sampleImgmsg_description");
		var source = document.getElementById("sampleImgmsg_source");
		var urlContent = document.getElementById("sampleImgmsg_urlContent");
		var imgValue = img.src;
		if (img.baseURI == imgValue) {
			document.getElementById("appmsgmask").style.display = "none";
			document.getElementById("appmsgmaskProgress").style.display = "none";
			alert("图片不能为空");
			return;
		}
		imgmsg1['date'] = date;
		if (title.innerHTML) {
			var failIdx = validateTitle(title.innerHTML);
			if (failIdx > 0)
				return;
		}
		var titleValue = title.innerHTML;
		imgmsg1['title'] = ("标题" == titleValue ? "" : titleValue);
		imgmsg1['img'] = imgValue;
		imgmsg1['description'] = description.textContent
		var source1 = source.value;
		imgmsg1['source'] = source1;
		urlContent1 = urlContent.value;
		if (urlContent1) {
			if (urlContent1.indexOf("<html><head>") < 0) {
				var sampleImgmsgHtmlAuth = document.getElementById("sampleImgmsg_htmlAuth");
				var authName = sampleImgmsgHtmlAuth.value;
				imgmsg1['htmlAuth'] = authName ? authName : "";
				var reg = /<p>\s*<meta (?:.+?)>\s*<\/p>/i;
				urlContent1 = urlContent1.replace(reg, '');
				urlContent1 = "<!doctype html><html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"><meta name=\"viewport\" content=\"width=device-width, minimum-scale=1.0,maximum-scale=1.0,user-scalable=no\">"
				  + imgmsgHtmlTitle
				  + "<div class=\"shouji_title\">"
				  + titleValue
				  + "</div><div class=\"shouji_zuozhe\"><span>"
				  + date
				  + (authName ? ("【" + authName + "】") : "")
				  + "</span></div>" + "</head><body>" + urlContent1 + "</body></html>";
			}
		}

		imgmsg1['urlContent'] = urlContent1;
		if (!source1 && !urlContent1) {
			document.getElementById("appmsgmask").style.display = "none";
			document.getElementById("appmsgmaskProgress").style.display = "none";
			alert("来源或正文不能为空");
			return;
		}
		imgMsgItem.push(imgmsg1);
	} else { // 多图文消息
		var imgmsgDiv_1 = document.getElementById("imgmsgItem1");
		var items1 = imgmsgDiv_1.childNodes;
		var source1;
		for (var j = 0; j < items1.length; j++) {
			var items1Node = items1[j];
			if (items1Node.nodeType == 1) {
				var nodeH4 = items1Node.getElementsByTagName("h4");
				if (nodeH4.length <= 0)
					continue;
				var nodes = nodeH4[0].childNodes;
				var imgValue = "";
				if (!imgmsg1['title']) {
					for (var l = 0; l < nodes.length; l++) {
						var title = nodes[l];
						if (title.nodeType == 1) {
							var titleValue = title.innerHTML;
							if (title.innerHTML) {
								var failIdx = validateTitle(title.innerHTML);
								if (failIdx > 0)
									return;
							}
							imgmsg1['title'] = ("标题" == titleValue ? "" : titleValue);
							break;
						}
					}
				}
				if (!imgmsg1['img']) {
					var nodeImg = items1Node.getElementsByTagName("img")[0];
					imgValue = nodeImg.src;
					if (!imgValue) {
						document.getElementById("appmsgmask").style.display = "none";
						document.getElementById("appmsgmaskProgress").style.display = "none";
						alert("封面图片不能为空");
						return;
					} else
						imgmsg1['img'] = imgValue;
				}
				if (!imgmsg1['source']) {
					var nodeInput = items1Node.getElementsByTagName("input")[0];
					if ("source" == nodeInput.name) {
						source1 = nodeInput.value;
						if (source1) {
							imgmsg1['source'] = source1;
						}
					}
				}
				if (!imgmsg1['urlContent']) {
					var nodeInput = items1Node.getElementsByTagName("textarea")[0];
					if ("urlContent" == nodeInput.name) {
						urlContent1 = nodeInput.value;
						if (urlContent1) {
							if (urlContent1.indexOf("<html><head>") < 0) {
								var sampleImgmsgHtmlAuth = document.getElementById("sampleImgmsg_htmlAuth");
								var authName = sampleImgmsgHtmlAuth.value;
								imgmsg1['htmlAuth'] = authName ? authName : "";
								var reg = /<p>\s*<meta (?:.+?)>\s*<\/p>/i;
								urlContent1 = urlContent1.replace(reg, '');

								urlContent1 = "<!doctype html><html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"><meta name=\"viewport\" content=\"width=device-width, minimum-scale=1.0,maximum-scale=1.0,user-scalable=no\">"
								  + imgmsgHtmlTitle
								  + "<div class=\"shouji_title\">"
								  + titleValue
								  + "</div><div class=\"shouji_zuozhe\"><span>"
								  + date
								  + (authName ? ("【" + authName + "】") : "")
								  + "</span></div>" + "</head><body>" + urlContent1 + "</body></html>";
							}
							imgmsg1['urlContent'] = urlContent1;
						}
						break;
					}
				}
			}
		}
		imgmsg1['date'] = date;
		if (!source1 && !urlContent1) {
			document.getElementById("appmsgmask").style.display = "none";
			document.getElementById("appmsgmaskProgress").style.display = "none";
			alert("封面来源或正文不能为空");
			return;
		}
		imgMsgItem.push(imgmsg1);
		var itemNextNode = imgmsgDiv_1.nextSibling;
		while (itemNextNode.nodeType && ("addImgMsg" != itemNextNode.id)) {
			if (itemNextNode.nodeType == 1) {
				if (itemNextNode != null && itemNextNode.id && itemNextNode.id.indexOf("appmsgItem") > -1) {
					var itemmsgNext = {};
					var childNs = itemNextNode.childNodes;
					for (var i = 0; i < childNs.length; i++) {
						var seleNode = childNs[i];
						var htmlTitleValue = "";
						if (seleNode.nodeType == 1) {
							if (!itemmsgNext['title']) {
								var h4Node = seleNode.nextSibling;
								var spanNode = h4Node.getElementsByTagName("span")[0];
								var title = spanNode.innerHTML;
								if (title.innerHTML) {
									var failIdx = validateTitle(title.innerHTML);
									if (failIdx > 0)
										return;
								}
								if (!title) {
									document.getElementById("appmsgmask").style.display = "none";
									document.getElementById("appmsgmaskProgress").style.display = "none";
									alert("非封面标题不能为空");
									return;
								} else {
									itemmsgNext['title'] = title;
									htmlTitleValue = title;
								}
							}
							if (!itemmsgNext['img']) {
								var imgNode = seleNode.getElementsByTagName("img")[0];
								var img = imgNode.src;
								if (!img) {
									itemmsgNext['img'] = "";
								} else {
									itemmsgNext['img'] = img;
								}
							}
							var source;
							var sourceNode;
							if (!itemmsgNext['source']) {
								var inputNode = seleNode.parentNode.getElementsByTagName("input");
								for (var i = 0; i < inputNode.length; i++) {
									sourceNode = inputNode[i];
									if ("source" == sourceNode.name) {
										source = sourceNode.value;
										if (source) {
											itemmsgNext['source'] = source;
										}
										break;
									}
								}
							}
							var urlContent;
							if (!itemmsgNext['urlContent']) {
								var textareaNode = seleNode.parentNode.getElementsByTagName("textarea")[0];
								if ("urlContent" == textareaNode.name) {
									urlContent = textareaNode.value;
									if (urlContent) {
										if (urlContent.indexOf("<html><head>") < 0) {
											var index = itemNextNode.id.replace("appmsgItem", "");
											var sampleImgmsgHtmlAuth = document.getElementById("sampleImgmsg_htmlAuth" + index);
											var authName = sampleImgmsgHtmlAuth.value;
											itemmsgNext['htmlAuth'] = authName ? authName : "";
											var reg = /<p>\s*<meta (?:.+?)>\s*<\/p>/i;
											urlContent = urlContent.replace(reg, '');
											urlContent = "<!doctype html><html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"><meta name=\"viewport\" content=\"width=device-width, minimum-scale=1.0,maximum-scale=1.0,user-scalable=no\">"
											  + imgmsgHtmlTitle
											  + "<div class=\"shouji_title\">"
											  + htmlTitleValue
											  + "</div><div class=\"shouji_zuozhe\"><span>"
											  + date
											  + (authName ? ("【" + authName + "】") : "")
											  + "</span></div></head><body>" + urlContent + "</body></html>";
										}
										itemmsgNext['urlContent'] = urlContent;
									}
								}
							}
							if (!source && !urlContent) {
								document.getElementById("appmsgmask").style.display = "none";
								document.getElementById("appmsgmaskProgress").style.display = "none";
								alert("来源或正文不能为空");
								return;
							}
							imgMsgItem.push(itemmsgNext);
							break;
						}

					}
					itemNextNode = itemNextNode.nextSibling;
				}
			} else {
				itemNextNode = itemNextNode.nextSibling;
			}
		}
	}
	// 调用ajax保存数据
	ajaxSubmit(imgMsgItem, imgmsgId);
}

var XMLHttpReq;
function ajaxSubmit(resData, imgmsgId) {
	try {
		XMLHttpReq = new ActiveXObject("Msxml2.XMLHTTP");// IE高版本创建XMLHTTP
	} catch (E) {
		try {
			XMLHttpReq = new ActiveXObject("Microsoft.XMLHTTP");// IE低版本创建XMLHTTP
		} catch (E) {
			XMLHttpReq = new XMLHttpRequest();// 兼容非IE浏览器，直接创建XMLHTTP对象
		}
	}
	var params = encodeURIComponent($.toJSON(resData));
	var postStr = "data=" + params + "&imgmsgId=" + imgmsgId;
	XMLHttpReq.open("post", "img-msg!save.action", true);
	XMLHttpReq.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	XMLHttpReq.onreadystatechange = processResponse; // 指定响应函数
	XMLHttpReq.send(postStr);
	// 保存成功后赋值为空
	// clickDivId = "";
}

// ajax回调函数
function processResponse() {
	document.getElementById("appmsgmask").style.display = "none";
	document.getElementById("appmsgmaskProgress").style.display = "none";
	if (XMLHttpReq.readyState == 4) {
		if (XMLHttpReq.status == 200) {
			var resultObj = XMLHttpReq.responseText;
			var obj = $.parseJSON(resultObj);
			if (obj.success) {
				var dataArr = obj.data.split(',');
				imgmsgId = dataArr[1];
				parent.getTopWindow().ImgMsgSaved(imgmsgId + "," + dataArr[0] + ",UTF-8");
				alert('保存成功');
				parent.getTopWindow().ImgmsgEditWindow.close();
			} else {
				alert('提示:' + obj.message);
			}
		}
	}
}

function addSourceUrl() {
	var description = document.getElementById("addSourceUrlImgmsgDiv");
	description.style.display = "block";
	var addButtonDescription = document.getElementById("addButtonSourceUrlImgmsg");
	if (addButtonDescription)
		addButtonDescription.style.display = 'none';
}

function addDescription() {
	var description = document.getElementById("imgmsg_div_description_div");
	description.style.display = "block";
	var addButtonDescription = document.getElementById("addButtonDescriptionImgmsg");
	addButtonDescription.style.display = 'none';
}
function changeIMG() {
	var imgAddress = document.getElementById("img_addr");
	var sumitImg = document.getElementById("imgmsgImgUpload");

	if (imgAddress.style.display == "none") {
		imgAddress.style.display = "block";
		sumitImg.style.display = "none";
	} else {
		sumitImg.style.display = "block";
		imgAddress.style.display = "none";
	}
}

function updateIMG(imgUrl, isSample) {
	var startTag = imgUrl.substring(0, 4);
	imgUrl = (("http" != startTag) ? ("http://" + imgUrl) : imgUrl);// 如果不是以http开头，加上http://
	if ("true" == isSample) {
		var sampleTitleImg = document.getElementById("sampleImgmsg_img");
		var sampleTitleImgMask = document.getElementById("sampleImgmsg_imgMask");
		sampleTitleImgMask.style.display = "none";
		sampleTitleImg.style.display = "block";
		sampleTitleImg.src = imgUrl;
	} else {
		clickDivId = !clickDivId ? "imgmsgItem1" : clickDivId;
		var itemDiv = document.getElementById(clickDivId);
		var childNs = itemDiv.childNodes;
		if ("imgmsgItem1" == clickDivId) {
			for (var i = 0; i < childNs.length; i++) {
				var seleNode = childNs[i];
				if (seleNode.nodeType == 1) {
					var nodeP = seleNode.getElementsByTagName("p")[0];
					var nodeImg = seleNode.getElementsByTagName("img")[0];
					nodeP.style.display = "none";
					nodeImg.style.display = "block";
					nodeImg.src = imgUrl;
					return;
				}
			}

		} else {
			for (var i = 0; i < childNs.length; i++) {
				var seleNode = childNs[i];
				if (seleNode.nodeType == 1) {
					var spanNode = seleNode.getElementsByTagName("span")[0];
					var imgNode = seleNode.getElementsByTagName("img")[0];
					spanNode.style.display = "none";
					imgNode.style.display = "block";
					imgNode.src = imgUrl;
					return;
				}
			}
		}
	}
}