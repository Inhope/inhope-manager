<%@page import="java.net.URLEncoder"%>
<%@page
	import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="com.eastrobot.commonsapi.domain.FileDescription"%>
<%@page import="com.eastrobot.commonsapi.RemoteFileService"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*,java.io.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<script>
	function funCallback(funcNum, fileUrl) {

		var parentWindow = (window.parent == window) ? window.opener
				: window.parent;

		parentWindow.CKEDITOR.tools.callFunction(funcNum, fileUrl);

		window.close();

	}
</script>
<%
	/**
	 * KindEditor JSP
	 *
	 * 本JSP程序是演示程序，建议不要直接在实际项目中使用。
	 * 如果您确定直接使用本程序，使用之前请仔细确认相关安全设置。
	 *
	 */
	String callback = request.getParameter("CKEditorFuncNum");
    
    String rootUrl  = request.getContextPath() + "/kbmgr/imgmsgData/images/";
	//图片扩展名
	String[] fileTypes = new String[] { "gif", "jpg", "jpeg", "png", "bmp", "swf" };

	//根据path参数，设置各路径和URL
	String path = request.getParameter("path") != null ? request.getParameter("path") : "";

	//排序形式，name or size or type
	String order = request.getParameter("order") != null ? request.getParameter("order")
	.toLowerCase() : "name";

	//不允许使用..移动到上一级目录
	if (path.indexOf("..") >= 0) {
		out.println("Access is not allowed.");
		return;
	}
	//最后一个字符不是/
	if (!"".equals(path) && !path.endsWith("/")) {
		out.println("Parameter is not valid.");
		return;
	}

	//遍历目录取的文件信息
RemoteFileService remoteFileService = WebApplicationContextUtils.getWebApplicationContext(application).getBeansOfType(RemoteFileService.class).values().iterator().next();
FileDescription[] files =  remoteFileService.listFileDescriptions("/imgmsgData/images/");
if(files != null) {
	for (FileDescription file : files) {
		Hashtable<String, Object> hash = new Hashtable<String, Object>();
		String fileName = file.getPath().replaceAll(".*/", "");
		if(!file.isDirectory()){
			String fileExt = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
			String src = rootUrl+ fileName; 
%>
<img width='110px' height='70px' src='<%=URLEncoder.encode(src.replace("/", "**")).replace("**","/")%>' alt='<%=URLEncoder.encode(fileName)%>'
   onclick='funCallback("<%=URLEncoder.encode(callback)%>","<%=URLEncoder.encode(src.replace("/", "**")).replace("**","/")%>")'\>
<%
		}
	}
}
%>
<%!public class NameComparator implements Comparator {
		public int compare(Object a, Object b) {
			Hashtable hashA = (Hashtable) a;
			Hashtable hashB = (Hashtable) b;
			if (((Boolean) hashA.get("is_dir")) && !((Boolean) hashB.get("is_dir"))) {
				return -1;
			} else if (!((Boolean) hashA.get("is_dir")) && ((Boolean) hashB.get("is_dir"))) {
				return 1;
			} else {
				return ((String) hashA.get("filename")).compareTo((String) hashB.get("filename"));
			}
		}
	}

	public class SizeComparator implements Comparator {
		public int compare(Object a, Object b) {
			Hashtable hashA = (Hashtable) a;
			Hashtable hashB = (Hashtable) b;
			if (((Boolean) hashA.get("is_dir")) && !((Boolean) hashB.get("is_dir"))) {
				return -1;
			} else if (!((Boolean) hashA.get("is_dir")) && ((Boolean) hashB.get("is_dir"))) {
				return 1;
			} else {
				if (((Long) hashA.get("filesize")) > ((Long) hashB.get("filesize"))) {
					return 1;
				} else if (((Long) hashA.get("filesize")) < ((Long) hashB.get("filesize"))) {
					return -1;
				} else {
					return 0;
				}
			}
		}
	}

	public class TypeComparator implements Comparator {
		public int compare(Object a, Object b) {
			Hashtable hashA = (Hashtable) a;
			Hashtable hashB = (Hashtable) b;
			if (((Boolean) hashA.get("is_dir")) && !((Boolean) hashB.get("is_dir"))) {
				return -1;
			} else if (!((Boolean) hashA.get("is_dir")) && ((Boolean) hashB.get("is_dir"))) {
				return 1;
			} else {
				return ((String) hashA.get("filetype")).compareTo((String) hashB.get("filetype"));
			}
		}
	}%>