<%@page import="java.net.URLEncoder"%>
<%@page import="com.eastrobot.commonsapi.domain.FileDescription"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="com.eastrobot.commonsapi.RemoteFileService"%>
<%@page import="com.eastrobot.commonsapi.domain.RemoteFile"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="org.apache.commons.io.IOUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*,java.io.*"%>
<%@ page import="org.apache.commons.fileupload.*"%>
<%@ page import="org.apache.commons.fileupload.disk.*"%>
<%@ page import="org.apache.commons.fileupload.servlet.*"%>
<%@ page import="org.apache.commons.lang.StringUtils"%>
<%
	/**
 * KindEditor JSP
 * 
 * 本JSP程序是演示程序，建议不要直接在实际项目中使用。
 * 如果您确定直接使用本程序，使用之前请仔细确认相关安全设置。
 * 
 */
 
//定义允许上传的文件扩展名
String[] fileTypes = new String[]{"gif", "jpg", "jpeg", "png", "bmp", "swf"};
//最大文件大小
// long maxSize = 1000000;
long maxSize = 1000000;

response.setContentType("text/html; charset=UTF-8");

if(!ServletFileUpload.isMultipartContent(request)){
	out.println(getError("请选择文件。"));
	return;
}
//文件加载URL
String loadUrl  = request.getContextPath() + "/kbmgr/imgmsgData/images/";
//检查目录
RemoteFileService remoteFileService = WebApplicationContextUtils.getWebApplicationContext(application).getBeansOfType(RemoteFileService.class).values().iterator().next();
FileItemFactory factory = new DiskFileItemFactory();
ServletFileUpload upload = new ServletFileUpload(factory);
upload.setHeaderEncoding("UTF-8");

List items = upload.parseRequest(request);
Iterator itr = items.iterator();
while (itr.hasNext()) {
	FileItem item = (FileItem) itr.next();
	String fileName = item.getName();
	long fileSize = item.getSize();
	if (!item.isFormField()) {
		//检查文件大小
		if(item.getSize() > maxSize){
	out.println("上传文件大小超过1M限制。");
	return;
		}
		//检查扩展名
		String fileExt = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
		if(!Arrays.<String>asList(fileTypes).contains(fileExt)){
			out.println("上传文件扩展名是不允许的扩展名。");
			return;
		}
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
		String newFileName = df.format(new Date()) + "_" + new Random().nextInt(1000) + "." + fileExt;
		InputStream in = item.getInputStream();
	    //upload
		try{
			RemoteFile rf = new RemoteFile();
			rf.setData(IOUtils.toByteArray(in));
			rf.setDescription(new FileDescription());
			rf.getDescription().setPath("/imgmsgData/images/"+newFileName);
			remoteFileService.saveFiles(rf);
		}catch(Exception e){
			e.printStackTrace();
			out.println(getError("上传文件失败。"));
			return;
		}finally{
			IOUtils.closeQuietly(in);
		}
       String callback = request.getParameter("CKEditorFuncNum");//获取回调JS的函数Num  
%>
<script type='text/javascript'>
window.parent.CKEDITOR.tools.callFunction(<%=URLEncoder.encode(callback)%>,'<%=URLEncoder.encode((loadUrl+newFileName).replace("/", "**")).replace("**","/")%>')</script>
<%
	}
}
%>
<%!
private String getError(String message) {
    return message;
}
%>