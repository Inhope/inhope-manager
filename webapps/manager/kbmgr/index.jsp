<%@page import="org.springframework.context.ApplicationContext"%>
<%@page import="org.codehaus.jackson.map.ObjectMapper"%>
<%@page import="com.incesoft.xmas.commons.LicenceHolder"%>
<%@page
	import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="res" uri="/WEB-INF/tld/import-res.tld"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="usr" uri="/WEB-INF/tld/login-user.tld"%>
<%@taglib prefix="mdl" uri="/WEB-INF/tld/mdl-ft.tld"%>
<%
	ApplicationContext ctx = WebApplicationContextUtils
			.getWebApplicationContext(application);
	LicenceHolder licenceholder = ctx.getBean(LicenceHolder.class);
	if (licenceholder != null && licenceholder.getLicence() != null) {
		request.setAttribute("supportPlatforms", licenceholder
				.getLicence().getSupportedPlatforms());
	}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>知识库管理</title>
<script type="text/javascript">
	var supportPlatforms =<%=new ObjectMapper().writeValueAsString(request.getAttribute("supportPlatforms"))%>;
</script>
<usr:checkPerm jsExportVar="kb_check_perm_result" permissionNames="kb"
	dump="true" />
<usr:name jsExportVar="kb_username" />
<mdl:dump name="kb" jsExportVar="kb_mdl_ft" />
<mdl:dump name="kb" jsExportVar="kb_mdl_props" type="props" />
<mdl:dump name="ibot" jsExportVar="kb_mdl_props1" type="props" />
<%@include file="/commons/extjsvable.jsp"%>
<!-- 加载extjs4.2.1 -->
<script src="<c:url value="/ext4/ext-all-sandbox.js"/>"
	type="text/javascript"></script>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/ext4/ext-sandbox.css"/>" />
<!-- 加载extjs4.2.1结束 -->
<link rel="stylesheet" type="text/css" href="custom.css" />
<link rel="stylesheet" type="text/css" href="multitools.css" />
<link rel="stylesheet" type="text/css" href="data-view.css" />
<script type="text/javascript">
var isAllow = http_get('abstract-semantic-task!isAllow.action');
</script>
<res:import dir="script" recursive="true" include=".*\.js" />
<script src="<c:url value="/commons/scripts/raphael/generatorPicPre.js"/>" type="text/javascript"></script>
<script type="text/javascript">
	var arVersion = navigator.appVersion.split("MSIE");
	function correctPNG() // correctly handle PNG transparency in Win IE 5.5 & 6.
	{
		var version = parseFloat(arVersion[1]);
		if ((7 > version >= 5.5) && (document.body.filters)) {
			for ( var j = 0; j < document.images.length; j++) {
				var img = document.images[j];
				var imgName = img.src.toUpperCase();
				if (imgName.substring(imgName.length - 3, imgName.length) == "PNG") {
					var imgID = (img.id) ? "id='" + img.id + "' " : "";
					var imgClass = (img.className) ? "class='" + img.className + "' " : "";
					var imgTitle = (img.title) ? "title='" + img.title + "' " : "title='" + img.alt + "' ";
					var imgStyle = "display:inline-block;" + img.style.cssText;
					if (img.align == "left")
						imgStyle = "float:left;" + imgStyle;
					if (img.align == "right")
						imgStyle = "float:right;" + imgStyle;
					if (img.parentElement.href)
						imgStyle = "cursor:hand;" + imgStyle;
					var strNewHTML = "<span " + imgID + imgClass + imgTitle + " style=\"" + "width:" + img.width + "px; height:" + img.height + "px;"
							+ imgStyle + ";" + "filter:progid:DXImageTransform.Microsoft.AlphaImageLoader" + "(src=\'" + img.src
							+ "\', sizingMethod='scale');\"></span>";
					img.outerHTML = strNewHTML;
					j = j - 1;
				}
			}
		}
	}
	if (window.attachEvent)
		window.attachEvent("onload", correctPNG);
</script>
</head>
<body>
      <div id="multiToolId" class="multiToolId">
		<div class="tips1" id="tipToolIcon">
			<a href="javaScript:void(0);" title="功能"><img
				src="images/multifunClick.png" width="43" height="43" /> </a>
		</div>
		<div class="tips_quan">
		<div class="tips" id="tipToolUnfold" style="display:none;"
			onpropertychange="">
			<ul>
				<li class="ico1"><a href="javaScript:void(0);" title="分析标准问"><img
						src="images/multiRepeadKb.png" onclick="ShortcutToolRegistry.execAction('analyseStandQuestion');" width="24" height="24" /> </a></li>
				<li class="ico2"><a href="javaScript:void(0);" title="新词发现"><img
						src="images/multiAddWord.png" onclick="ShortcutToolRegistry.execAction('addSegAnalysePanel');" width="24" height="24" /> </a></li>
				<li class="ico3"><a href="javaScript:void(0);" title="引擎调试器"><img
						src="images/multiAiEngingTool.png"  onclick="ShortcutToolRegistry.execAction('aiTesterWindow');" width="24" height="24" /> </a></li>
				<li class="ico4"><a href="javaScript:void(0);" title="词类管理"><img
						src="images/multiWordMana.png" onclick="ShortcutToolRegistry.execAction('wordclassManager');" width="24" height="24" /> </a></li>
				<li class="ico5"><a href="javaScript:void(0);" title="替换"><img
						src="images/multiReaplace.png" onclick="ShortcutToolRegistry.execAction('replaceField');" width="24" height="24" /> </a></li>
				<li class="ico6"><a href="javaScript:void(0);" title="功能"><img
						src="images/multifunClick.png" width="43" height="43" /> </a></li>
			</ul>
		</div>
		</div>
		
	</div>
</body>
<script type="text/javascript">
	Ext.onReady(function() {
		var initIdx = 0;
		var resizer = new Ext.Resizable('multiToolId', { //构造函数
			wrap : false,
			pinned : false,
			minWidth : 50,
			minHeight : 50,
			maxWidth : 50,
			maxHeight : 50,
			preserveRatio : true,
			transparent : true,
			dynamic : true,
			handles : 'all',
			draggable : true
		});

		var imgEl = resizer.getEl();
		imgEl.show(true);

		// move to the body to prevent overlap on my blog
		document.body.insertBefore(imgEl.dom, document.body.firstChild);//插入节点

		var flag = 0;
		var imgDom = imgEl.dom;
		Ext.get('multiToolId').addListener("mousedown",function(){
		     flag = 0;
		});
		Ext.get('multiToolId').addListener("mousemove",function(){
		    flag = 1;
		});
		Ext.get('multiToolId').addListener("mouseup",function(){
		    if (flag === 0) {
		    	__display();
		    }
		});

		var __display = function() {
			var tipToolIconStyle = document.getElementById("tipToolIcon").style.display;
			var tipToolUnfoldStyle = document.getElementById("tipToolUnfold").style.display;
			if ('block' != tipToolIconStyle) {
				document.getElementById("tipToolUnfold").style.display = 'none';
				document.getElementById("tipToolIcon").style.display = 'block';
			} else if ('block' != tipToolUnfoldStyle) {
				document.getElementById("tipToolUnfold").style.display = 'block';
				document.getElementById("tipToolIcon").style.display = 'none';
			}
		};
	});
</script>
</html>