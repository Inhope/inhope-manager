Ext.onReady(function() {
	
	var _pageSize = 20;

	Ext.define('sentence', {
		extend : 'Ext.data.Model',
		fields : Ext.decode(http_get('abstract-semantic-task!buildField.action')),
		proxy : {
			type : 'ajax',
			url : 'abstract-semantic-task!countSmtAttr.action',
			reader: {
	             type: 'json',
	             root: 'result',
	             totalProperty: 'totalCount'
	         }
		}
	});

	var _store = new Ext.data.Store({
		pageSize : _pageSize,
		model : 'sentence',
		autoLoad : true
	});
	
	
	this.selectMode = new Ext.selection.Model({
		singleSelect : true
	});
	
	var pagingBar = this.bbar = new Ext.PagingToolbar({
		store : _store,
		displayInfo : true,
		pageSize : 20,
		prependButtons : true,
		beforePageText : '第',
		afterPageText : '页，共{0}页',
		displayMsg : '总计问题{2}',
		emptyMsg : "没有记录"
	});

	Ext.create('Ext.grid.Panel', {
		style : 'border-right: 1px solid ' + sys_bdcolor,
		stripeRows : true,
		columnLines : true,
		autoscroll: true,
		sm : self.selectMode,
		height : p.getHeight(),
		border : false,
		renderTo : Ext.getBody(),
		store : _store,
		loadMask : true,
		columns : Ext.decode(http_get('abstract-semantic-task!buildHeader.action')),
//		forceFit : true,
		bbar : pagingBar
	});

});