var load = function(question) {
//	parent.ValueGraph.tabPanel.dl.getEl().mask('正在加载"' + question + '"的分析结果...');
	$.ajax({
				url : 'value-graph!valueGraphDetail.action',
				type : "POST",
				data : {
					question : question
				},
				dataType : "json",
				async : false,
				success : function(result) {
					$('#analyse').empty();
					CreateWordRow(result.data.second);
					CreateSemanticRow(result.data.first, result.data.second.length);
					
//					$('#analyse').html(result.data.second);
//					parent.ValueGraph.tabPanel.dl.getEl().unmask();
//					$('#resText').empty();
//					var html = '';
//					$.each(data, function(commentIndex, comment) {
//								html += '<div class="comment"><h6>' + comment['username'] + ':</h6><p class="para"' + comment['content'] + '</p></div>';
//							});
//					$('#resText').html(html);
				}
			});
}

function CreateWordRow(records) {
	var $table = $('#analyse');
	var wordName = "<tr>";
	var pos = "<tr>";
	var path = "<tr>";
	var weight = "<tr>";
	$.each(records, function(i, record) {
				wordName += "<td>" + record.wordName + "</td><td>&nbsp;</td>";
				pos += "<td>" + record.pos + "</td><td>&nbsp;</td>";
				path += "<td>" + record.path + "</td><td>&nbsp;</td>";
				weight += "<td>" + record.staticweight  + "</td><td>&nbsp;</td>";
			});
	wordName += "</tr>";
	pos += "</tr>";
	path += "</tr>";
	weight += "</tr>";
	$table.append(wordName).append(pos).append(path).append(weight);
}

function CreateSemanticRow(record, length) {
	$('#semantic').html(record);
}