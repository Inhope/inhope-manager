Ext.namespace('ChatCount');
ChatCount = function(_cfg) {
	
	var me = this;
	var _pageSize = 30;
	var _ChatCountModel = Ext.define('ChatCountModel', {
				extend : 'Ext.data.Model',
				fields : [{
							name : 'id',
							type : 'string'
						}, {
							name : 'question',
							type : 'string'
						}, {
							name : 'createTime',
							type : 'date'
						}, {
							name : 'categoryId',
							type : 'string'
						}, {
							name : 'answer',
							type : 'string'
						}, {
							name : 'typeId',
							type : 'string'
						}, {
							name : 'type',
							type : 'string'
						}],
				proxy : {
					type : 'ajax',
					url : 'chat-details!list.action',
					reader : {
						type : 'json',
						root : 'result',
						totalProperty : 'totalCount'
					}
				}
			});
	this.ChatCountModel = _ChatCountModel;

	var _ChatCountStore = Ext.create('Ext.data.Store', {
				pageSize : _pageSize,
				model : 'ChatCountModel',
				autoLoad : true,
				listeners : {
					"beforeload" : function() {
						Ext.apply(_ChatCountStore.proxy.extraParams, {
									limit : _pageSize,
									categoryId : me.currentTabID,
									leaf : me.leaf
								});
					}
				}
			});
	this.ChatCountStore = _ChatCountStore;

	var _ChatCountPanel = Ext.create('Ext.grid.Panel', {
				stripeRows : true,
				columnLines : true,
				border : false,
				forceFit : true,
				store : _ChatCountStore,
				viewConfig : {
							forceFit : true,
							markDirty : false,
							enableTextSelection : true
						},
				loadMask : true,
				columns : [{
							xtype : 'rownumberer',
							width : 40,
							sortable : true,
							renderer : function(value, metadata, record, rowIndex) {
								var indexs = rowIndex + 1;
								var page = record.store.lastOptions.page;
								var limit = record.store.lastOptions.limit;
								if (page > 1) {
									indexs = (page - 1) * limit + rowIndex + 1;
								}
								return indexs;
							}
						}, {
							text : '问题',
							flex : 2,
							dataIndex : 'question'
						}, {
							text : '维度',
							flex : 1,
							dataIndex : 'typeId'
						}, {
							text : '答案',
							flex : 4,
							dataIndex : 'answer'
						}],
				bbar : new Ext.toolbar.Paging({
							store : _ChatCountStore,
							displayInfo : true,
							pageSize : _pageSize,
							prependButtons : true,
							beforePageText : '第',
							afterPageText : '页，共{0}页',
							displayMsg : '聊天记录总计{2}个',
							emptyMsg : "没有记录"
						}),
				dockedItems : [{
							xtype : 'toolbar',
							dock : 'top',
							items : ['请输入关键字：', {
										xtype : 'textfield',
										emptyText : '支持回车搜索',
										listeners : {
											specialkey : function(field, e) {
												if (e.getKey() == Ext.EventObject.ENTER) {
													me.search(this);
												}
											}
										}
									}, '-', {
										text : '搜索',
										iconCls : 'icon-search',
										handler : function() {
											me.search(this.up('panel').down('textfield'));
										}
									}, '-', {
										text : '保存',
										iconCls : 'icon-save',
										handler : function() {
											me.save();
										}
									}, '-', {
										text : '删除',
										iconCls : 'icon-delete',
										handler : function() {
											me.remove();
										}
									}, '-', {
										text : '新增问题',
										iconCls : 'icon-add',
										handler : function(btn) {
											var _grid = btn.up('panel');
											var rows = _grid.getSelectionModel().getSelection();
											var r = _ChatCountStore.data.length;
											_ChatCountStore.insert(0, {
													});
										}
									}, '-', {
										text : '新增维度',
										iconCls : 'icon-add',
										handler : function(btn) {
											var _grid = btn.up('panel');
											var rows = _grid.getSelectionModel().getSelection();
											if (rows.length > 0) {
												_ChatCountStore.insert(_ChatCountStore.indexOf(rows[0]) + 1, {
															question : rows[0].data.question
														});
												_grid.getView().refresh();
											} else {
												Ext.Msg.alert('提示：', '请选择一条问题!');
											}
										}
									} ]
						}]
			});
	this.ChatCountPanel = _ChatCountPanel;

	var config = {
		layout : 'fit',
		border : 0,
		items : _ChatCountPanel
	};
	ChatCount.superclass.constructor.call(this, config);
};
Ext.extend(ChatCount, Ext.panel.Panel, {

})