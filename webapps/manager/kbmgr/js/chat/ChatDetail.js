Ext.namespace('ChatDetail');
ChatDetail = function(_cfg) {
	
	var me = this, currentTabID = me.currentTabID = _cfg.id, leaf = me.leaf = _cfg.leaf;
	var _pageSize = 30;
	this.name = '';
	var chatTypeStore = this.chatTypeStore = prefabHelper.getChatTypeStore();   
	var _chatDetailModel = Ext.define('chatDetailModel', {
				extend : 'Ext.data.Model',
				fields : [{
							name : 'id',
							type : 'string'
						}, {
							name : 'question',
							type : 'string'
						}, {
							name : 'createTime',
							type : 'date'
						}, {
							name : 'categoryId',
							type : 'string'
						}, {
							name : 'answer',
							type : 'string'
						}, {
							name : 'typeId',
							type : 'string'
						}, {
							name : 'type',
							type : 'string'
						}],
				proxy : {
					type : 'ajax',
					url : 'chat-details!list.action',
					reader : {
						type : 'json',
						root : 'result',
						totalProperty : 'totalCount'
					}
				}
			});
	this.chatDetailModel = _chatDetailModel;

	var _chatDetailStore = Ext.create('Ext.data.Store', {
				pageSize : _pageSize,
				model : 'chatDetailModel',
				autoLoad : true,
				listeners : {
					"beforeload" : function() {
						Ext.apply(_chatDetailStore.proxy.extraParams, {
									limit : _pageSize,
									categoryId : me.currentTabID,
									leaf : me.leaf
								});
					}
				}
			});
	this.chatDetailStore = _chatDetailStore;

	var _chatDetailPanel = Ext.create('Ext.grid.Panel', {
				id : 'chat' + this.currentTabID,
				stripeRows : true,
				columnLines : true,
				border : false,
				forceFit : true,
				store : _chatDetailStore,
				viewConfig : {
							forceFit : true,
							markDirty : false,
							enableTextSelection : true
						},
				plugins : [Ext.create('Ext.grid.plugin.CellEditing', {
									clicksToEdit : 2
								})],
				loadMask : true,
				listeners : {
					beforeedit : function(editor, e) {
						return ((!e.rowIdx) || !(e.field == 'question' && e.value == _chatDetailStore.getAt(e.rowIdx - 1).get(e.field)));
					},
					edit : function(editor, e) {
						if (e.field == 'question') {
							_chatDetailStore.each(function(record) {
										if (record.get(e.field) == e.originalValue) {
											record.set(e.field, e.value);
										}
									});
						}
						return true;
					},
					validateedit : function(editor, e) {
						var res = true;
						if (e.field == 'question') {
							_chatDetailStore.each(function(record) {
										if (e.field == 'question' && record.get(e.field) == e.value) {
											res = false;
											return;
										}
									});
						}
						return res;
					},

					cellclick : function(view, td, cellIndex, record, tr, rowIndex, e, o) {
						if (cellIndex == 2) {
							//console.info(view);
						}
					}
				},
				columns : [{
							xtype : 'rownumberer',
							width : 40,
							sortable : true,
							renderer : function(value, metadata, record, rowIndex) {
								var indexs = rowIndex + 1;
								var page = record.store.lastOptions.page;
								var limit = record.store.lastOptions.limit;
								if (page > 1) {
									indexs = (page - 1) * limit + rowIndex + 1;
								}
								return indexs;
							}
						}, {
							text : '问题',
							flex : 2,
							editor : {
								xtype : 'textfield',
								allowBlank : false
							},
							dataIndex : 'question',
							renderer : function(value, meta, record, rowIndex, colIndex, store) {
								var currentValue = store.getAt(rowIndex).get('question');
								var previous = !rowIndex || currentValue != store.getAt(rowIndex - 1).get('question');
								if (previous) {
									if ((rowIndex + 1) < store.data.length) {
										if (currentValue == store.getAt(rowIndex + 1).get('question')) {
											meta.tdAttr = 'style="border-bottom-style:none;background-color:white;"';
										}
									}
								} else {
									meta.tdAttr = 'style="border-top-style:none;border-bottom-style:none;background-color:white;"';
								}
								return previous ? value : '';
							}
						}, {
							text : '维度',
							flex : 1,
							editor : {
								id : 'typeCombo' + this.currentTabID,
								xtype : 'combobox',
		                        typeAhead: true,
		                        triggerAction: 'all',
		                        queryMode: 'local',
		                        selectOnTab: true,
		                        lazyRender: true,
		                        displayField: 'name',
		                        valueField: 'id',
								editable : false,
								allowBlank : false,
								store : prefabHelper.getChatTypeStore()
							},
							dataIndex : 'typeId',
							renderer : function(value, meta, record, rowIndex, columnIndex, store) {
								if(value != '') {
									var _store = chatTypeStore;
									var index = _store.find('id', value);
									if (index >= 0) {
										return _store.getAt(index).get('name');
									} else {
										return value;
									}
								}
							}
						}, {
							text : '答案',
							flex : 4,
							editor : {
								xtype : 'textfield',
								allowBlank : false
							},
							dataIndex : 'answer',
							renderer : function(data, meta, record, rowIndex, columnIndex, store) {
								meta.style = 'white-space:normal;';
								return data;
							}  
						}],
				bbar : new Ext.toolbar.Paging({
							store : _chatDetailStore,
							displayInfo : true,
							pageSize : _pageSize,
							prependButtons : true,
							beforePageText : '第',
							afterPageText : '页，共{0}页',
							displayMsg : '聊天记录总计{2}个',
							emptyMsg : "没有记录"
						}),
				dockedItems : [{
							xtype : 'toolbar',
							dock : 'top',
							items : ['请输入关键字：', {
										xtype : 'textfield',
										emptyText : '支持回车搜索',
										listeners : {
											specialkey : function(field, e) {
												if (e.getKey() == Ext.EventObject.ENTER) {
													me.search(this);
												}
											}
										}
									}, '-', {
										text : '搜索',
										iconCls : 'icon-search',
										handler : function() {
											me.search(this.up('panel').down('textfield'));
										}
									}, '-', {
										text : '保存',
										iconCls : 'icon-save',
										handler : function() {
											me.save();
										}
									}, '-', {
										text : '删除',
										iconCls : 'icon-delete',
										handler : function() {
											me.remove();
										}
									}, '-', {
										text : '新增问题',
										iconCls : 'icon-add',
										handler : function(btn) {
											var _grid = btn.up('panel');
											var rows = _grid.getSelectionModel().getSelection();
											var r = _chatDetailStore.data.length;
											_chatDetailStore.insert(0, {
													});
										}
									}, '-', {
										text : '新增维度',
										iconCls : 'icon-add',
										handler : function(btn) {
											var _grid = btn.up('panel');
											var rows = _grid.getSelectionModel().getSelection();
											if (rows.length > 0) {
												_chatDetailStore.insert(_chatDetailStore.indexOf(rows[0]) + 1, {
															question : rows[0].data.question
														});
												_grid.getView().refresh();
											} else {
												Ext.Msg.alert('提示：', '请选择一条问题!');
											}
										}
									}
//									, '-', {
//										text : '删除维度',
//										handler : function(btn) {
//											var _grid = btn.up('panel');
//											var rows = _grid.getSelectionModel().getSelection();
//											if (rows.length > 0) {
//												_chatDetailStore.remove(rows);
//											}
//											_grid.getView().refresh();
//										}
//									}
									]
						}]
			});
	this.chatDetailPanel = _chatDetailPanel;

	var config = {
		layout : 'fit',
		border : 0,
		items : _chatDetailPanel
	};
	ChatDetail.superclass.constructor.call(this, config);
};
Ext.extend(ChatDetail, Ext.panel.Panel, {
			search : function(cont) {
				var me = this, _store = me.chatDetailStore;
				Ext.apply(_store.proxy.extraParams, {
							question : cont.getValue()
						});
				_store.reload();
			},
			save : function() {
				var me = this, _store = me.chatDetailStore, _resultArr = [];
				var tempName = '';
				var ansTypeArr = [];
				var flag = true;
				_store.each(function(record, index) {
							var isQuestion = true;
							if (_store.getAt(index - 1) && record.data.question == _store.getAt(index - 1).get('question')) {
								isQuestion = false;
							}
							if (isQuestion) {
								ansTypeArr = [];
							} else {
								var isContains = false;
								for (var i = 0; i < ansTypeArr.length; i++) {
									if (ansTypeArr[i] == record.data.typeId) {
										isContains = true;
									}
								}
								if (isContains) {
									flag = false;
									var typeName = '';
									var index = me.chatTypeStore.find('id', record.data.typeId);
									if (index >= 0) {
										typeName = me.chatTypeStore.getAt(index).get('name');
									}
									Ext.Msg.alert("提示", record.data.question + "包含重复的维度:" + typeName);
									return false;
								}
							}
							ansTypeArr.push(record.data.typeId);

							if (record.dirty) {
								record.set('categoryId', me.currentTabID);
								_resultArr.push(record.data);
							}
						});
				if (flag) {
					Ext.Ajax.request({
								url : 'chat-details!merge.action',
								params : {
									content : Ext.JSON.encode(_resultArr)
								},
								success : function(response) {
									var json = Ext.JSON.decode(response.responseText);
									if (json.success) {
										_store.reload();
										Ext.Msg.alert("提示", "保存成功!");
									} else {
										Ext.Msg.alert("提示", json.message);
									}
								},
								failure : function(response) {
									Ext.Msg.alert("错误", response.responseText);
								}
							});
				}
			},
			remove : function() {
				var me = this, _store = me.chatDetailStore;
				var rows = me.chatDetailPanel.getSelectionModel().getSelection();
				if (rows.length == 0) {
					Ext.Msg.alert('提示', '请先选择要删除的记录！');
					return false;
				}
				
				if (rows[0].data.id == '') {
					_store.remove(rows[0]);
					return false;
				}
				
				Ext.Msg.confirm('确定框', '确定要删除"' + rows[0].data.name + '"维度吗？', function(result) {
							if (result == 'yes') {
								Ext.Ajax.request({
											url : 'chat-details!delete.action',
											params : {
												id : rows[0].data.id
											},
											success : function(resp) {
												if (resp.responseText) {
													var obj = Ext.JSON.decode(resp.responseText);
													if (obj.success) {
														Ext.Msg.alert("提示：", '删除成功！');
														_store.reload();
													} else {
														Ext.Msg.alert('删除失败', obj.message);
													}
												}
											},
											failure : function(resp) {
												Ext.Msg.alert('错误', resp.responseText);
											}
										});
							}
						});
			}
		})