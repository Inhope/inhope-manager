Ext.namespace('ChatNav');
ChatNav = function(_cfg) {
	var me = this;
	var _pageSize = 30;
	this.exportNode;

	var _chatCategoryModel = Ext.define('chatCategoryModel', {
				extend : 'Ext.data.Model',
				fields : [{
							name : 'id',
							type : 'string'
						}, {
							name : 'text',
							type : 'string'
						}, {
							name : 'pid',
							type : 'string'
						}, {
							name : 'leaf',
							type : 'boolean'
						}, {
							name : 'bh',
							type : 'string'
						}, {
							name : 'expanded',
							type : 'boolean'
						}, {
							name : 'saveOrUpdate',
							type : 'boolean'
						}, {
							name : 'status',
							type : 'boolean'
						}, {
							name : 'draggable',
							type : 'boolean'
						}]
			});
	this.chatCategoryModel = _chatCategoryModel;

	var _chatCategoryStore = Ext.create('Ext.data.TreeStore', {
				model : 'chatCategoryModel',
				root : {
					id : '0',
					text : 'root'
				},
				proxy : {
					type : 'ajax',
					url : 'chat-category!tree.action',
					reader : {
						type : 'json'
					}
				}
			});
	this.chatCategoryStore = _chatCategoryStore;

	var cfg = {
		title : '通用聊天',
		rootVisible : false,
		border : 0,
		autoScroll : true,
		store : me.chatCategoryStore,
		draggable : true,
		folderSort : false,
		listeners : {
			'expand' : function(p) {
				var node = p.getSelectedNode();
				if (!node) {
					me.refresh(p.getRootNode());
				} else {
					if (!node.isExpanded()) {
						me.refresh(node);
					}
				}
			}
		},
		viewConfig : {
			plugins : {
				ptype : 'treeviewdragdrop',
				appendOnly : true,
				containerScroll : true
			}
		}
	};

	ChatNav.superclass.constructor.call(this, cfg);

	var menu = new Ext.menu.Menu({
				items : [{
							itemId : 'freshMenu',
							text : '刷新',
							iconCls : 'icon-refresh',
							width : 100,
							handler : function() {
								me.refreshNode(menu.currentNode);
							}
						}, {
							itemId : 'addDic',
							text : '新建目录',
							iconCls : 'icon-add',
							width : 100,
							handler : function() {
								me.addNode(menu.currentNode, false);
							}
						}, {
							itemId : 'addleaf',
							text : '新建叶子',
							iconCls : 'icon-add',
							width : 100,
							handler : function() {
								me.addNode(menu.currentNode, true);
							}
						}, {
							itemId : 'importChat',
							text : '导入',
							width : 100,
							iconCls : 'icon-ontology-import',
							handler : function() {
								me.importChat(menu.currentNode);
							}
						}, {
							itemId : 'exportChat',
							text : '导出',
							width : 100,
							iconCls : 'icon-ontology-export',
							handler : function() {
								me.exportNode = menu.currentNode;
								me.formWinexport.show();
							}
						}, {
							itemId : 'editNode',
							text : '修改',
							iconCls : 'icon-edit',
							width : 100,
							handler : function() {
								me.editNode(menu.currentNode);
							}
						}, {
							itemId : 'removeLogicNode',
							text : '删除',
							iconCls : 'icon-delete',
							width : 100,
							handler : function() {
								me.removeNode(menu.currentNode, false);
							}
						}, {
							itemId : 'removePhysicsNode',
							text : '完全删除',
							iconCls : 'icon-delete',
							width : 100,
							handler : function() {
								me.removeNode(menu.currentNode, true);
							}
						}]
			});
	this.menu = menu;

	this.on('itemcontextmenu', function(node, record, item, index, event) {
				menu.showAt(event.getXY());
				event.preventDefault();
				menu.currentNode = record;
				menu.show();
				if (record.data.leaf) {
					menu.query('#addleaf')[0].hide();
					menu.query('#addDic')[0].hide();
					menu.query('#importChat')[0].hide();
				} else {
					menu.query('#addleaf')[0].show();
					menu.query('#addDic')[0].show();
					menu.query('#importChat')[0].show();
				}
				if (record.data.status) {// 是否被删除
					menu.query('#removeLogicNode')[0].hide();
					menu.query('#removePhysicsNode')[0].show();
					menu.query('#addleaf')[0].hide();
					menu.query('#addDic')[0].hide();
					menu.query('#editNode')[0].hide();
				} else {
					menu.query('#removeLogicNode')[0].show();
					menu.query('#removePhysicsNode')[0].hide();
					menu.query('#editNode')[0].show();
				}
				
				if(isAdd == 0) {
					menu.query('#addleaf')[0].hide();
					menu.query('#addDic')[0].hide();
				}
				if(isDel == 0) {
					menu.query('#removeLogicNode')[0].hide();
					menu.query('#removePhysicsNode')[0].hide();
				}
				if(isExpt == 0) {
					menu.query('#exportChat')[0].hide();
				}
				if(isImpt == 0) {
					menu.query('#importChat')[0].hide();
				}
			});

	this.on('itemclick', function(node, record, item, index, event) {
				me.showTab(record.data.id, record.data.leaf, record.data.text, 'icon-sentence');
			});

	Ext.define('parentComboBoxData', {
				extend : 'Ext.data.Model',
				fields : [{
							name : 'id',
							type : 'string'
						}, {
							name : 'name',
							type : 'string'
						}]
			});
	this.parentComboBoxData = Ext.ModelManager.create({
				id : '0',
				name : '顶级目录'
			}, 'parentComboBoxData');

	this.parentComboBox = new Ext.form.field.ComboBox({
				fieldLabel : '父级类别',
				valueField : 'id',
				displayField : 'name',
				editable : false,
				name : 'pid',
				value : '0',
				labelWidth : 70,
				anchor : '100%',
				store : Ext.create('Ext.data.Store', {
							fields : ["name", "id"],
							data : me.parentComboBoxData
						})
			});

	this.hiddenLeaf = new Ext.form.field.Hidden({
				itemId : 'categoryLeaf',
				name : 'leaf'
			});
	this.hiddenBh= new Ext.form.field.Hidden({
				itemId : 'categoryBh',
				name : 'bh'
			});

	var _formPanel = Ext.create('Ext.form.Panel', {
				bodyPadding : '10 10 0',
				border : false,
				defaults : {
					anchor : '100%',
					allowBlank : false,
					msgTarget : 'side',
					labelAlign : 'left',
					labelWidth : 70
				},
				items : [new Ext.form.field.Hidden({
							itemId : 'categoryId',
							name : 'id'
						}), me.hiddenLeaf, me.hiddenBh, me.parentComboBox, {
							itemId : 'name',
							xtype : 'textfield',
							fieldLabel : '目录名称',
							name : 'name'
						}],
				buttons : [{
							text : '保存',
							handler : function() {
								me.submitNode(this);
							}
						}]
			});
	this.formPanel = _formPanel;

	var _windowPanel = Ext.create('Ext.window.Window', {
				width : 350,
				border : false,
				modal : true,
				plain : true,
				shim : true,
				closable : true,
				closeAction : 'hide',
				collapsible : true,
				resizable : false,
				draggable : true,
				animCollapse : true,
				constrainHeader : true,
				listeners : {
					'beforehide' : function(p) {
						p.form.getForm().reset();
					}
				},
				items : _formPanel
			});
	_windowPanel.form = _formPanel;
	this.windowPanel = _windowPanel;

	/** ---------------------------------------------导入------------------------------------------------------- */
	var _formPanelimport = Ext.create('Ext.form.Panel', {
				bodyPadding : '10 10 0',
				border : false,
				defaults : {
					anchor : '100%',
					allowBlank : false,
					msgTarget : 'side',
					labelAlign : 'right',
					labelWidth : 100
				},
				items : [{
					itemId : 'kbsmode',
					xtype : 'radiogroup',
					fieldLabel : '导入方式',
					defaults : {
						flex : 1
					},
					items : [{
						boxLabel : '清空导入',
						name : 'mode',
						inputValue : 1
					}, {
						boxLabel : '增量导入',
						name : 'mode',
						inputValue : 0,
						checked : true
					}]
				}, {
					xtype : 'filefield',
					emptyText : '选择文件',
					fieldLabel : '导入Excel文件',
					name : 'file',
					editable : false,
					regex : /^.*?\.(xls|xlsx)$/,
					regexText : '只能上传.xls,xlsx文件',
					buttonText : '',
					buttonConfig : {
						iconCls : 'icon-search'
					}
				}],
				buttons : [{
					text : '导入',
					handler : function() {
						var form = this.up('form').getForm();
						if (form.isValid()) {
							form.submit({
								url : me.importUrl,
								method : 'POST',
								params : {
									mode : form.getValues()["mode"]
								},
								success : function(form, act) {
									var timer = new ProgressTimer({
											initData : act.result.data,
											progressText : 'percent',
											progressId : 'chatcategory',
											boxConfig : {
												msg : '导入中',
												title : '导入通用聊天'
											},
											finish : function(p, response) {
												_chatCategoryStore.reload();
												_formWinimport.close();
											},
											scope : this
										});
									timer.start();
								},
								failure : function(form, action) {
									var data = Ext.JSON.decode(action.response.responseText);
									Ext.Msg.alert('failure', data.message);
								}
							});
//							Ext.Ajax.request({
//								url : me.importUrl,
//								params : {
//									mode : form.getValues()["mode"]
//								},
//								success : function(response) {
//									var timer = new ProgressTimer({
//										initData : Ext.decode(response.responseText),
//										progressText : 'percent',
//										progressId : 'chatcategory',
//										boxConfig : {
//											msg : '导入中',
//											title : '导入通用聊天'
//										},
//										finish : function() {
//											_chatCategoryStore.reload();
//											_formWinimport.close();
//										},
//										scope : this
//									});
//									timer.start();
//								},
//								failure : function(response) {
//									Ext.Msg.alert('请求异常', response.responseText);
//								},
//								scope : this
//							});
							
							
							
							
//							form.submit({
//								url : me.importUrl,
//								method : 'POST',
//								waitMsg : '正在上传',
//								success : function(response, action) {
//									var data = Ext.JSON.decode(action.response.responseText);
//									Ext.Ajax.request({
//										url : 'chat-category!doCreateTree.action',
//										method : 'POST',
//										params : {
//											fileFileName : data.message,
//											mode : form.getValues()["mode"],
//											pid : data.data
//										},
//										success : function(response) {
//											if (response.responseText) {
//												var timer = new ProgressTimer({
//													initData : Ext.decode(response.responseText),
//													progressText : 'percent',
//													progressId : 'chatcategory',
//													boxConfig : {
//														msg : '导入中',
//														title : '导入通用聊天'
//													},
//													finish : function(p, response) {
//														_chatCategoryStore.reload();
//														_formWinimport.close();
//													},
//													scope : this
//												});
//												timer.start();
//											}
//										}
//									})
//								},
//								failure : function(form, action) {
//									var data = Ext.JSON.decode(action.response.responseText);
//									Ext.Msg.alert('failure', data.message);
//								}
//							});
						}
					}
				}, {
					text : '清空',
					handler : function() {
						this.up('form').getForm().reset();
					}
				}]
			});

	var _formWinimport = new Ext.window.Window({
				width : 400,
				defaults : {
					border : false
				},
				modal : true,
				plain : true,
				shim : true,
				closable : true,
				closeAction : 'hide',
				collapsible : true,
				resizable : false,
				draggable : true,
				animCollapse : true,
				constrainHeader : true,
				listeners : {
					'beforehide' : function(p) {
						p.formPanelimport.getForm().reset();
					}
				},
				items : _formPanelimport
			});
	_formWinimport.formPanelimport = _formPanelimport;
	this.formWinimport = _formWinimport;
	
	var _formPanelexport = Ext.create('Ext.form.Panel', {
				bodyPadding : '1',
				border : false
			});
			
	Ext.Ajax.request({
		url : 'chat-type!checkboxgroup.action',
		callback : function(options, success, response) {
			var data = Ext.decode(response.responseText);
			var itemsGroup = new Ext.form.CheckboxGroup({
				cls: 'x-check-group-alt',
				columns : 4,
				vertical : true,
		        defaults: {
		            anchor: '100%'
		        },
				items : data
			});
			_formPanelexport.add(itemsGroup);
			_formPanelexport.doLayout();
		}
	});
	
	var _formWinexport = new Ext.window.Window({
				width : 600,
				defaults : {
					border : false
				},
				modal : true,
				plain : true,
				title : '选择维度',
				shim : true,
				closable : true,
				closeAction : 'hide',
				collapsible : true,
				resizable : false,
				draggable : true,
				animCollapse : true,
				constrainHeader : true,
				listeners : {
					'beforehide' : function(p) {
						p.formPanelexport.getForm().reset();
					}
				},
				items : _formPanelexport,
				buttons : [{
					text : '确认导出',
					handler : function(btn) {
						var values = btn.up('window').down('form').getForm().getValues()["mode"];
						me.exportChatCategory(values.join(','));
						me.formWinexport.close();
					}
				}]
			});
	_formWinexport.formPanelexport = _formPanelexport;
	this.formWinexport = _formWinexport;
	
	this.on('itemmove', function(target, oldParent, newParent, index, eOpts) {
				Ext.Msg.confirm('提示', '确定要把"' + target.get('text') + '"移动到"' + newParent.getPath('text').replace('/root', '') + '"吗', function(btn, text) {
							if (btn == 'yes') {
								Ext.Ajax.request({
									url : 'chat-category!moveNode.action',
									method : 'POST',
									params : {
										nodeId : target.data.id,
										newParentId : newParent.data.id
									},
									success : function(response) {
									}
								})
							} else {
								oldParent.appendChild(target, true);
								oldParent.collapse(false, function() {
									oldParent.expand();
								});
							}
						});
				return false;
			});

};
Ext.extend(ChatNav, Ext.tree.Panel, {

			importChat : function(node) {
				var me = this;
				if (node.data.id == 'ChatRoot') {
					me.importUrl = "chat-category!doImport.action?";
					me.formWinimport.show();
				} else {
					me.importUrl = "chat-category!doImport.action?pid=" + node.data.id;
					me.formWinimport.show();
				}
			},

			refreshNode : function(node) {
				var me = this;
				me.getStore().load();
				me.refresh(node);
			},

			refresh : function(node) {
				node.expand();
			},
			createNewNode : function(data) {
				var me = this, pid = data.pid, parentNode;
				if (pid == null) {
					parentNode = this.getStore().getNodeById('ChatRoot');
				} else {
					parentNode = this.getStore().getNodeById(pid);
				}
				if (!data.saveOrUpdate) {
					var currentNode = this.getStore().getNodeById(data.id);
					if (currentNode != undefined) {
						currentNode.set('text', data.text);
					}
				} else {
					if (parentNode) {
						var node = {
							expanded : data.expanded,
							id : data.id,
							leaf : data.leaf,
							pid : data.pid,
							text : data.text,
							bh : data.bh,
							iconCls : data.iconCls,
							status : data.status,
							saveOrUpate : data.saveOrUpdate,
							draggable : data.draggable
						};
						if (!data.leaf) {
							node.children = [];
						}
						parentNode.appendChild(node);
						parentNode.expand();
					}
				}
			},
			getSelectedNode : function() {
				return this.getSelectionModel().getLastSelected();
			},
			addNode : function(node, isLeaf) {
				var me = this;
				me.windowPanel.setTitle("新增通用聊天目录");
				var _form = me.windowPanel.form;
				if (node.data && !node.data.root) {
					this.parentComboBoxData = Ext.ModelManager.create({
								id : node.data.id,
								name : node.getPath('text').replace("/root", "")
							}, 'parentComboBoxData');
					this.parentComboBox.setValue(this.parentComboBoxData);
				}
				me.hiddenLeaf.setValue(isLeaf);
				me.parentComboBox.setReadOnly(true);
				me.windowPanel.show();
			},
			editNode : function(node) {
				var me = this;
				me.windowPanel.setTitle("修改");
				var _form = me.windowPanel.form;
				var rec = Ext.data.Model(Ext.apply({}, node.data));
				if (node.data && !node.data.root) {
					this.parentComboBoxData = Ext.ModelManager.create({
								id : rec.pid,
								name : node.parentNode.getPath('text').replace("/root", "")
							}, 'parentComboBoxData');
					this.parentComboBox.setValue(this.parentComboBoxData);
				}
				_form.query('#categoryId')[0].setValue(rec.id);
				_form.query('#categoryBh')[0].setValue(rec.bh);
				_form.query('#categoryLeaf')[0].setValue(rec.leaf);
				_form.query('#name')[0].setValue(rec.text);
				me.parentComboBox.setReadOnly(true);
				me.windowPanel.show();
			},
			removeNode : function(node, isLogic) {
				var me = this;
				Ext.Msg.confirm('提示', '确定要删除"' + node.data.text + '"吗', function(btn, text) {
					if (btn == 'yes') {
						Ext.Ajax.request({
							url : 'chat-category!remove.action',
							method : 'POST',
							params : {
								id : node.data.id,
								isLogic : isLogic
							},
							success : function(response, action) {
								if (response.responseText) {
									var obj = Ext.JSON.decode(response.responseText);
									if (obj.success) {
										me.getStore().reload();
										Ext.Msg.alert("提示：", obj.message);
									} else {
										Ext.Msg.alert('删除失败', obj.message);
									}
								}
							}
						})
					}
				});
			},
			submitNode : function(btn) {
				var me = this;
				var f = btn.up('form').getForm();
				if (!f.isValid()) {
					Ext.Msg.alert('提示', '您尚未正确完整的输入表单内容，请检查！');
					return false;
				}
				var data = f.getValues();
				data.pid = this.parentComboBox.getValue();
				if (data.pid.length == 1) {
					data.pid = null;
				}
				Ext.Ajax.request({
					url : 'chat-category!merge.action',
					params : {
						data : Ext.encode(data)
					},
					success : function(resp, action) {
						if (resp.responseText) {
							var obj = Ext.JSON.decode(resp.responseText);
							if (obj.success) {
								Ext.Msg.alert("提示：", "操作成功！");
								me.createNewNode(obj.data);
								me.windowPanel.close();
							}
						}
					},
					failure : function(response) {
						Ext.Msg.alert("错误", response.responseText);
					}
				});
			},
			showTab : function(tabId, leaf, title, iconCls) {
				var me = this, data = {
					id : tabId,
					title : title,
					leaf : leaf
				};
				var tabMainPanel = Ext.getCmp('centerControlPanel');
				var tabPanel = tabMainPanel.child('#' + tabId);
				if (!tabPanel) {
					var currentPanel;
//					if (leaf) {
						currentPanel = new ChatDetail(data);
						tabMainPanel.add({
								itemId : tabId,
								title : title,
								closable : true,
								iconCls : iconCls,
								layout : 'fit',
								border : false,
								items : currentPanel
							});
//					} else {
//						return ;
//					}
					
				}
				if (!tabPanel)
					tabPanel = tabMainPanel.child('#' + tabId);
				tabPanel.tab.show();
				tabMainPanel.setActiveTab(tabPanel);
			},
			
			exportChatCategory : function(values) {
				var me = this, exportUrl, node = me.exportNode;
				if (node.data.id == 'ChatRoot') {
					exportUrl = "chat-category!doExport.action?modes=" + values;
				} else {
					exportUrl = "chat-category!doExport.action?id=" + node.data.id + "&modes=" + values;
				}
				Ext.Ajax.request({
					url : exportUrl,
					method : 'POST',
					success : function(response) {
						if (response.responseText) {
							var timer = new ProgressTimer({
								initData : Ext.decode(response.responseText),
								progressText : 'percent',
								progressId : 'exportChatCategory',
								boxConfig : {
									msg : '导出中',
									title : '导出通用聊天'
								},
								finish : function(p, response) {
									window.location.href = "chat-category!export.action";
								},
								scope : this
							});
							timer.start();
						}
					}
				})
			}
		});
