Ext.namespace('ChatType');
ChatType = function(_cfg) {
	var me = this;
	var _pageSize = 30;
	this.name = '';

	var _chatTypeModel = Ext.define('chatTypeModel', {
				extend : 'Ext.data.Model',
				fields : [{
							name : 'id',
							type : 'string'
						}, {
							name : 'name',
							type : 'string'
						}, {
							name : 'createTime',
							type : 'date'
						}],
				proxy : {
					type : 'ajax',
					url : 'chat-type!page.action',
					reader : {
						type : 'json',
						root : 'result',
						totalProperty : 'totalCount'
					}
				}
			});
	this.chatTypeModel = _chatTypeModel;

	var _chatTypeStore = Ext.create('Ext.data.Store', {
				pageSize : _pageSize,
				model : 'chatTypeModel',
				autoLoad : true,
				listeners : {
					"beforeload" : function() {
						Ext.apply(_chatTypeStore.proxy.extraParams, {
									limit : _pageSize,
									name : this.name
								});
					}
				}
			});
	this.chatTypeStore = _chatTypeStore;

	var _chatTypePanel = Ext.create('Ext.grid.Panel', {
		stripeRows : true,
		columnLines : true,
		border : false,
		forceFit : true,
		store : _chatTypeStore,
		plugins : [Ext.create('Ext.grid.plugin.CellEditing', {
					clicksToEdit : 2
				})],
		loadMask : true,
		columns : [new Ext.grid.RowNumberer({
							width : 30
						}), {
					text : '聊天类型',
					flex : 1,
					editor : {
						xtype : 'textfield',
						allowBlank : false
					},
					dataIndex : 'name'
				}, {
					text : '创建时间',
					flex : 1,
					dataIndex : 'createTime',
					renderer : Ext.util.Format.dateRenderer('Y-m-d')
				}],
		bbar : new Ext.toolbar.Paging({
					store : _chatTypeStore,
					displayInfo : true,
					pageSize : _pageSize,
					prependButtons : true,
					beforePageText : '第',
					afterPageText : '页，共{0}页',
					displayMsg : '聊天类型总计{2}个',
					emptyMsg : "没有记录"
				}),
		dockedItems : [{
					xtype : 'toolbar',
					dock : 'top',
					items : ['请输入关键字：', '-', {
								xtype : 'textfield',
								emptyText : '支持回车搜索',
								listeners : {
									specialkey : function(field, e) {
										if (e.getKey() == Ext.EventObject.ENTER) {
											me.search(this);
										}
									}
								}
							}, '-', {
								text : '搜索',
								iconCls : 'icon-search',
								handler : function() {
									me.search(this.up('panel')
											.down('textfield'));
								}
							}, '-', {
								text : '新增',
								iconCls : 'icon-add',
								handler : function() {
									var r = _chatTypeStore.data.length;
									_chatTypeStore.insert(r, {});
								}
							}, '-', {
								text : '保存',
								iconCls : 'icon-save',
								handler : function() {
									me.save();
								}
							}, '-', {
								text : '删除',
								iconCls : 'icon-delete',
								handler : function() {
									me.remove();
								}
							}]
				}]
	});
	this.chatTypePanel = _chatTypePanel;

	var config = {
		layout : 'fit',
		border : 0,
		items : _chatTypePanel
	};
	ChatType.superclass.constructor.call(this, config);
};
Ext.extend(ChatType, Ext.panel.Panel, {
			search : function(cont) {
				var me = this, _store = me.chatTypeStore;
				Ext.apply(_store.proxy.extraParams, {
							name : cont.getValue()
						});
				_store.reload();
			},
			save : function() {
				var me = this, _store = me.chatTypeStore, _resultArr = [];
				_store.each(function(record) {
							if (record.dirty) {
								_resultArr.push(record.data);
							}
						});
				Ext.Ajax.request({
							url : 'chat-type!save.action',
							params : {
								'name' : Ext.JSON.encode(_resultArr)
							},
							success : function(response) {
								var json = Ext.JSON
										.decode(response.responseText);
								if (json.success) {
									_store.reload();
									Ext.Msg.alert("提示", "保存成功!");
								} else {
									Ext.Msg.alert("提示", json.message);
								}
							},
							failure : function(response) {
								Ext.Msg.alert("错误", response.responseText);
							}
						});
			},
			remove : function() {
				var me = this, _store = me.chatTypeStore;
				var rows = me.chatTypePanel.getSelectionModel().getSelection();
				if (rows.length == 0) {
					Ext.Msg.alert('提示', '请先选择要删除的记录！');
					return false;
				}
				Ext.Msg.confirm('确定框', '确定要删除"' + rows[0].data.name + '"维度吗？', function(result) {
					if (result == 'yes') {
						Ext.Ajax.request({
								url : 'chat-type!remove.action',
								params : {
									id : rows[0].data.id
								},
								success : function(resp) {
									if (resp.responseText) {
										var obj = Ext.JSON.decode(resp.responseText);
										if (obj.success) {
											Ext.Msg.alert("提示：", '删除成功！');
											_store.reload();
										} else {
											Ext.Msg.alert('删除失败', obj.message);
										}
									}
								},
								failure : function(resp) {
									Ext.Msg.alert('错误', resp.responseText);
								}
							});
					}
				});
			}
		})