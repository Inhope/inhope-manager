var prefabHelper = function() {

	return {
		getChatTypeStore : function() {
			var chatTypeStore = Ext.create('Ext.data.Store', {
						fields : [{
									name : 'id',
									type : 'string'
								}, {
									name : 'name',
									type : 'string'
								}],
						autoLoad : true,
						proxy : {
							type : 'ajax',
							url : 'chat-type!list.action',
							reader : {
								type : 'json'
							}
						}
					});
			return chatTypeStore;
		}
	}
}();
