var __hide_loading_mask_1 = function(conn, response, options) {
	if (options.maskEl) {
		options.maskEl.unmask();
	}
};

function getTopWindow() {
	var topWin = window;
	try {
		while (topWin.parent && topWin.parent != topWin
				&& topWin.parent.location.href.indexOf('wrapper') == -1) {
			topWin = topWin.parent;
		}
	} catch (e) {
	}
	return topWin;
}

var __error_alert_1 = function(conn, response, options) {
	__hide_loading_mask_1(conn, response, options);
	if (response.status == 403) {
		if (options.failure)
			delete options.failure;
		var ec = response.getResponseHeader('error_code');
		if (ec == '1') {// logged in but target resource restricted
			Ext.Msg.alert('错误', '请求的资源未授权给当前登陆用户,请联系管理员');
		} else {
			var redirect_url = response.getResponseHeader('redirect_url');
			if (redirect_url) {
				Ext.Msg.alert('错误', '您还没有登陆，或者登陆已经超时。正在为您重定向到登陆页面...');
				getTopWindow().location = redirect_url;
			} else {
				Ext.Msg.alert('错误', '您还没有登陆，或者登陆已经超时。请重新登陆！');
			}
		}
	} else if (options && !options.failure)
		Ext.Msg.alert('错误', '服务器发生错误:'
						+ (response.responseText
								? response.responseText
								: '<br>'));
};
Ext.Ajax.on('requestcomplete', __hide_loading_mask_1);
Ext.Ajax.on('requestexception', __error_alert_1);

/** class for retrieving progress status of import/export */
function ProgressTimer(cfg) {
	var self = this;
	this.progressId = cfg.progressId;
	this.progress = cfg.progress;
	if (!this.progressId) {
		throw new Error('progressId is required!');
	}
	if (!cfg.initData || cfg.initData.active == null) {
		throw new Error('initData.active is required!');
	}
	this.active = cfg.initData.active;
	var progressText = cfg.progressText == 'percent' ? function(c, t) {
		return (t <= 0 ? 0 : parseInt(c * 100 / t)) + '%';
	} : function(c, t) {
		return c + "/" + t;
	};
	var cb_finish = cfg.finish;
	var cb_inactive = cfg.inactive;
	var cb_error = cfg.error;
	var cb_poll = cfg.poll;
	var cb_scope = cfg.scope || this;
	var ok = this.ok = cfg.ok;

	cfg.boxConfig = cfg.boxConfig || {};
	this.updateButtonVisibility = function(progress) {
		if (!this.msgbox)
			return
		// this.msgbox.getDialog().getFooterToolbar().hide()
		Ext.each(this.msgbox.msgButtons, function(btn) {
					if (progress && btn.text != '取消' && progress.finished)
						btn.enable();
					else if (progress && btn.text == '取消' && !progress.finished
							&& progress.cancelable)
						btn.enable();
					else
						btn.disable();
				});
	};
	this.createMsgbox = function(config) {
		config = Ext.apply(config || {}, cfg.boxConfig);
		var m = Ext.Msg.show(Ext.apply({
			title : '导入',
			progress : typeof(self.progress) == 'undefined'
					? true
					: self.progress,
			msg : '同步中...',
			buttons : Ext.MessageBox.OKCANCEL,
			buttonText : {
				ok : '确定',
				cancel : '取消'
			},
			width : 300,
			animEl : 'dialog',
			fn : function(btn) {
				if (btn == 'cancel') {
					Ext.Ajax.request({
						url : 'progress-timer!cancelProgress.action?progressId='
								+ self.progressId,
						success : function() {
						},
						failure : function() {
						}
					});
					//					m.show();
				};
				if(btn == 'ok') {//modify by baoy since 2015-03-12
					if(ok != undefined && ok != '' && ok != null) {
						window.location.href = ok;
					}
				}
			}
		}, config));
		if (!window.__before_show_bind) {
			window.__before_show_bind = true;
			m.on('beforeshow', function() {
						Ext.each(m.buttons, function(btn) {
									btn.enable();
								});
					});
		}
		return m;
	};
	this.msgbox = this.createMsgbox();
	this.updateButtonVisibility();
	var _requestImportProgress = function() {
		Ext.Ajax.request({
			url : 'progress-timer!getProgress.action?progressId='
					+ self.progressId,
			success : function(resp) {
				if (!this.msgbox)
					return;
				var progress = Ext.JSON.decode(resp.responseText);
				var value = progress.totalCount
						? (progress.currentCount / progress.totalCount)
						: 0;
				var text = progressText(progress.currentCount,
						progress.totalCount);
				this.msgbox.updateProgress(value, text, progress.message);
				if (progress.finished) {
					if (progress.data) {
						this.createMsgbox({
									width : 600
								});
						this.msgbox.updateProgress(1, progressText(
										progress.totalCount,
										progress.totalCount), progress.message);
						addMsgBoxDetail(progress.data);
					} else {
						this.msgbox.updateProgress(1, progressText(
										progress.totalCount,
										progress.totalCount), progress.message);
					}
					clearTimeout(this.importTimer);
					if (typeof cb_finish == 'function') {
						cb_finish.apply(cb_scope, [progress, resp]);
					}
					this.updateButtonVisibility(progress);
				} else {
					if (typeof cb_poll == 'function')
						cb_poll.apply(cb_scope, [progress, resp]);
					this.beginSchedule();
					this.updateButtonVisibility(progress);
				}
			},
			failure : function(resp) {
				if (!this.msgbox)
					return;
				if (typeof cb_error == 'function')
					cb_error.call(cb_scope, resp);
				else
					clearTimeout(this.importTimer);
				this.updateButtonVisibility(progress);
			},
			scope : self
		});
	};
	this.pause = function() {
		if (this.importTimer)
			clearTimeout(this.importTimer);
		if (this.msgbox) {
			this.msgbox.hide();
			delete this.msgbox;
		}
	};
	this.restart = function(delay) {
		this.msgbox = this.createMsgbox();
		if (delay)
			_requestImportProgress.defer(delay, this);
		else
			this.beginSchedule();
	};
	this.beginSchedule = function() {
		if (this.importTimer)
			clearTimeout(this.importTimer);
		this.importTimer = window.setTimeout(function() {
					_requestImportProgress();
				}, 1000);
	};

	this.start = function() {
		if (!this.active)
			if (typeof cb_inactive == 'function')
				cb_inactive.call(cb_scope);
			else
				Ext.Msg.alert('提示', '当前有其他互斥操作正在进行，请稍后再试！');
		else {
			this.msgbox.updateProgress(0, progressText(0, 0), '');
			this.beginSchedule();
		}
		return this.active;
	};
}

ProgressTimer.createTimer = function(config, cb) {
	if (!(config instanceof Array)) {
		config = [config];
	}
	function poll() {
		if (!config.length)
			return
		var c = config.shift();
		if (c.initData) {
			if (cb) {
				var tm = new ProgressTimer(c);
				if (typeof cb == 'function')
					cb(tm);
				else
					tm.start();
			}
			return
		}
		Ext.Ajax.request({
					url : 'progress-timer!getProgress.action?progressId='
							+ c.progressId,
					success : function(resp) {
						var progress = Ext.JSON.decode(resp.responseText);
						if (!progress)
							return;
						if (!progress.finished && cb) {
							c.initData = progress;
							var tm = new ProgressTimer(c);
							if (typeof cb == 'function') {
								cb(tm);
							} else {
								tm.start();
							}
						} else
							poll();
					}
				});
	}
	poll();
};


function mergeCells(grid, cols) {
    var arrayTr = document.getElementById(grid.getId() + "-body").firstChild.firstChild.lastChild
            .getElementsByTagName('tr');
    //var arrayTr = document.getElementById(grid.getId()+"-body").firstChild.nextSibling;  
    //var arrayTr = Ext.get(grid.getId()+"-body").first().first().first().select("tr");  
    var trCount = arrayTr.length;
	var arrayTd;
	var td;
	var merge = function(rowspanObj, removeObjs) { // 定义合并函数
		if (rowspanObj.rowspan != 1) {
			arrayTd = arrayTr[rowspanObj.tr].getElementsByTagName("td"); // 合并行
			td = arrayTd[rowspanObj.td - 1];
			td.rowSpan = rowspanObj.rowspan;
			td.vAlign = "middle";
			Ext.each(removeObjs, function(obj) { // 隐身被合并的单元格
						arrayTd = arrayTr[obj.tr].getElementsByTagName("td");
						arrayTd[obj.td - 1].style.display = 'none';
					});
		}
	};
	var rowspanObj = {}; // 要进行跨列操作的td对象{tr:1,td:2,rowspan:5}
	var removeObjs = []; // 要进行删除的td对象[{tr:2,td:2},{tr:3,td:2}]
	var col;

    Ext.each(cols, function(colIndex) { // 逐列去操作tr
				var rowspan = 1;
				var divHtml = null; // 单元格内的数值
				for (var i = 0; i < trCount; i++) { // i=0表示表头等没用的行
					arrayTd = arrayTr[i].getElementsByTagName("td");
					var cold = 0;
					Ext.each(arrayTd, function(Td) { // 获取RowNumber列和check列
								if (Td.getAttribute("class")
										.indexOf("x-grid-cell-special") != -1)
									cold++;
							});
                    col = colIndex + cold; // 跳过RowNumber列和check列
					if (!divHtml) {
						divHtml = arrayTd[col - 1].innerHTML;
						rowspanObj = {
							tr : i,
							td : col,
							rowspan : rowspan
						}
					} else {
                        var cellText = arrayTd[col - 1].innerHTML;
						var addf = function() {
							rowspanObj["rowspan"] = rowspanObj["rowspan"] + 1;
							removeObjs.push({
										tr : i,
										td : col
									});
							if (i == trCount - 1)
								merge(rowspanObj, removeObjs);// 执行合并函数
						};
                        var mergef = function() {
							merge(rowspanObj, removeObjs);// 执行合并函数
							divHtml = cellText;
							rowspanObj = {
								tr : i,
								td : col,
								rowspan : rowspan
							}
							removeObjs = [];
						};
                        if (cellText == divHtml) {
							if (colIndex != 1) {
								var leftDisplay = arrayTd[col - 2].style.display;// 判断左边单元格值是否已display
								if (leftDisplay == 'none') {
									addf();
								} else {
									mergef();
								}
							} else {
								addf();
							}
						} else {
							mergef();
						}
                    }
                }
            });
};


function getxhr() {
		var _xhr = false;
		try {
			_xhr = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				_xhr = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e2) {
				_xhr = false;
			}
		}
		if (!_xhr && window.XMLHttpRequest)
			_xhr = new XMLHttpRequest();
		return _xhr;
}
function http_get(url){
	var xhr = getxhr();
	xhr.open('GET', url, false);
	xhr.send();
	return xhr.responseText;
}

Ext.define('Ext.form.field.ClearableComboBox', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.clearablecombobox',


    trigger2Cls: 'x-form-clear-trigger',


    initComponent: function() {
        var me = this;


        me.callParent(arguments);
    },


    onTrigger2Click: function(event) {
        var me = this;
        me.clearValue();
    }


});