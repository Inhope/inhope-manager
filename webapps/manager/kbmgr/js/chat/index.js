Dashboard = {
	getModuleFeature : function(key) {
		return window.kb_mdl_ft ? window.kb_mdl_ft[key] : null;
	},
	getModuleProp : function(key) {
		return window.kb_mdl_props ? window.kb_mdl_props[key] : null;
	},
	setAlert : function(msg, status, delay) {
		if (!status)
			status = 'ok';
		if (!delay)
			delay = 3;
		if (!this.msgCt) {
			this.msgCt = Ext.DomHelper.insertFirst(document.body, {
						id : 'msg-div'
					}, true);
			this.msgCt.setStyle('position', 'absolute');
			this.msgCt.setStyle('z-index', 9999);
			this.msgCt.setWidth(300);
		}
		this.msgCt.alignTo(document, 't-t');
		var html = [
				'<div class="app-msg">',
				'<div class="x-box-tl"><div class="x-box-tr"><div class="x-box-tc"></div></div></div>',
				'<div class="x-box-ml"><div class="x-box-mr"><div class="x-box-mc"><h3 class="x-icon-text icon-status-'
						+ status + '">',
				msg,
				'</h3>',
				'</div></div></div>',
				'<div class="x-box-bl"><div class="x-box-br"><div class="x-box-bc"></div></div></div>',
				'</div>'].join('');
		var fxEl = Ext.DomHelper.append(this.msgCt, {
					'html' : html
				}, true);
		var endCb = function() {
			var dom = fxEl.dom;
			if (dom) {
				fxEl.stopFx();
				fxEl.removeAllListeners();
				dom.parentNode.removeChild(dom);
				fxEl.remove();
			}
		};
		fxEl.on('click', endCb);
		fxEl.slideIn('t').pause(delay).ghost("t", {
					callback : endCb
				});
	}
};

Ext.onReady(function() {
	Ext.create('Ext.container.Viewport', {
		layout : 'border',
		items : [{
			region : 'west',
			collapsible : true,
			split : true,
			collapseMode : 'mini',
			title : '控制台',
			width : 210,
			layout : 'accordion',
			items : [new ChatNav()]
		}, {
			region : 'center',
			xtype : 'tabpanel',
			id : 'centerControlPanel',
			closeAction : 'hide',
			layout : 'fit'
		}]
	});
});

function showTab(id, title, corpusBytes, vocabSize, wordsCount, vectorSize,
		windowSize, algorithm) {// 添加新的tabPanel
	var tabMainPanel = Ext.getCmp('centerControlPanel');
	var tabPanel = tabMainPanel.child('#' + id);
	if (!tabPanel) {
		var mainPanel = new MainPanel();
		mainPanel.initTab(id, title, corpusBytes, vocabSize, wordsCount,
				vectorSize, windowSize, algorithm);
		tabMainPanel.add({
					itemId : id,
					title : title,
					closable : true,
					iconCls : 'icon-om-mgr',
					layout : 'fit',
					border : false,
					items : mainPanel
				});
	}
	if (!tabPanel)
		tabPanel = tabMainPanel.child('#' + id);
	tabPanel.tab.show();
	tabMainPanel.setActiveTab(tabPanel);
}