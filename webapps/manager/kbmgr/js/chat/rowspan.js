//传入要合并的grid，和要合并的列，cloArr为数组，如[0,1,2]    
function mergeCells(grid, colArr) {
	var gridNextTd;

	var n = 0;//n是 始终记住要合并单元格的第一个值的标志
	var currentTdValue;//要合并起始单元格的值
	var currentTd; //当前单元格
	var preTdValue; //前一个单元格的值
	var preCurrentTdValue;// 要合并起始单元格的前一个值
	//找到grid的所有行
	var gridTrArr = document.getElementById(grid.getId() + "-body").firstChild.firstChild
			.getElementsByTagName("tr");
	//循环要合并的列
	for (var j = 0; j < colArr.length; j++) {
		var count = 0;
		//遍历grid的所有行
		for (var i = 0; i < gridTrArr.length; i++) {
			//查找grid某一行的所有列
			var gridTdArr = gridTrArr[i].getElementsByTagName("td");
			if (colArr[j] > 0) {
				//var preGridTdArr=gridTrArr[i-1].getElementsByTagName("td");
				//获取当前单元格，前一列的值
				preCurrentTdValue = gridTdArr[colArr[j] - 1]
						.getElementsByTagName("div")[0].textContent;
			}
			//记住要合并的第一个单元格的值
			if (n == i) {
				currentTd = gridTdArr[colArr[j]];
				currentTdValue = gridTdArr[colArr[j]]
						.getElementsByTagName("div")[0].textContent;
				if (colArr[j] > 0) {
					preTdValue = gridTdArr[colArr[j] - 1]
							.getElementsByTagName("div")[0].textContent;
				}
				count++;
				continue;
			}
			//获取这一行中要合并的这一列的值
			var gridTdValue = gridTdArr[colArr[j]].getElementsByTagName("div")[0].textContent;
			if (currentTdValue === gridTdValue) {
				//若是本列中，这两行相等，前一列中，这两行不相等，则不应该合并
				if (preTdValue == null || preTdValue === preCurrentTdValue) {
					gridTdArr[colArr[j]].style.display = "none";
					count++;
				}
				//若是不相等，则合以上的单元格
				else {
					currentTd.rowSpan = count;
					currentTd.style.verticalAlign = "middle";
					n = i;
					i--;
					count = 0;
				}
				//第一页合并完毕
				if (i == gridTrArr.length - 1) {
					//第二页n清0
					n = 0;
					currentTd.rowSpan = count;
					currentTd.style.verticalAlign = "middle";
				}
			} else {
				n = i;
				//比较最后一次，i会自动+1,所以此处减一
				i--;
				//若是本列中，这两行相等，前一列中，这两行不相等，则不应该合并,判断null是为了，复选框的情况
				//                if(preTdValue==null||preTdValue===preCurrentTdValue) {
				currentTd.rowSpan = count;
				currentTd.style.verticalAlign = "middle";
				//                }
				//count清0
				count = 0;
			}
		}
	}
}