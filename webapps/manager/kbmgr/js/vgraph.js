var generateRandomID = function() {
	return 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
		var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
		return v.toString(16);
	});
};
var getRandomInt = function(min, max) {
	return Math.floor(Math.random() * (max - min + 1) + min);
};

// var styles = [ {
// r : [ 35, 35 ],
// color : '#FFF',
// borderColor : '#8fd422',
// borderWidth : 3,
// fontColor : '#8fd422',
// fontSize : 12
// }, {
// r : [ 25, 25 ],
// color : '#FFF',
// borderColor : '#8fd422',
// borderWidth : 3,
// fontColor : '#8fd422',
// fontSize : 12
// }, {
// r : [ 35, 35 ],
// color : '#FFF',
// borderColor : '#cbe6ff',
// borderWidth : 3,
// fontColor : '#4fa6f6',
// fontSize : 12
// } ];

var styles = parent.ValueGraph.styles;

var width = 1200;
var height = 800;

var value = null;

var load = function(val) {
//	parent.ValueGraph.tabPanel.getEl().mask('正在加载"' + val + '"的分析结果...');
	$.ajax({
		url : 'value-graph.action?limit_obj_val=6&input=' + val,
		type : 'GET',
		// async : false,
		success : function(respTxt) {
			if (respTxt) {
				eval('value = ' + respTxt);
				renderView();
			} else {
				parent.Dashboard.setAlert('没有与"' + val + '"相关的知识', 'error');
			}
//			parent.ValueGraph.tabPanel.getEl().unmask();
		}
	});
};

var svg = null;
var selectionGlove = glow("selectionGlove").rgb("#0000A0").stdDeviation(7);

var renderView = function() {
	var nodes = [], nodesMap = {}, lines = [];

	var addNode = function(node) {
		nodes.push(node);
		nodesMap[node.id] = node;
	};
	var addLine = function(source, target, color) {
		lines.push({
			target : nodesMap[target],
			source : nodesMap[source],
			id : generateRandomID(),
			color : color ? color : '#CCC'
		});
	};
	var node = function(data) {
		var n = {
			id : data.id,
			name : data.name,
			type : data.type,
			input : data.input,
			answer : data.answer
		};
		if (data.name.length > 3)
			n.displayName = data.name.substring(0, 3) + '...';
		return n;
	};
	if (value) {
		var vnodes = value.nodes;
		var relations = value.relations;
		if (vnodes) {
			var root = node(vnodes[0]);
			root.type = 'rvalue';
			root.x = 480;
			root.y = 360;
			root.fixed = true;
			addNode(root);
			for ( var i = 1; i < vnodes.length; i++) {
				var n = node(vnodes[i]);
				addNode(n);
			}
		}
		if (relations) {
			for ( var i = 0; i < relations.length; i++) {
				var st = relations[i].split("-");
				addLine(st[0], st[1]);
			}
		}
	}

	if (svg)
		svg.remove();

	svg = d3.select("#graph").append("svg").attr("width", width).attr("height", height);

	var force = d3.layout.force().nodes(nodes).links(lines).size([ width, height ]).linkDistance(100).charge([ -700 ]).gravity(0.05).start();

	var links0 = force.links(), nodes0 = force.nodes();

	var getStyle = function(d) {
		var s = styles[d.type];
		if (!s)
			s = styles.other;
		return s;
	};

	function buildForce(a) {
		// force.nodes(nodes0).links(links0);
		var xx = svg.selectAll(".line").data(links0);

		xx.enter().append("line").each(function(d) {
			d3.select(this).attr("class", "line").style("stroke", function(d) {
				return d.color;
			}).style("stroke-width", 2);
		});

		// xx.exit().remove();
		if (a)
			svg.selectAll(".gnode").remove();
		var yy = svg.selectAll(".gnode").data(nodes0);
		yy.enter().append('g').classed('gnode', true).attr('id', function(d) {
			return d.id;
		}).attr('title', function(d) {
			var content = d.displayName ? d.name : '';
			if (d.type == 'value' || d.type == 'rvalue')
				content = 'Q: ' + content + '<br/><br/>A: ' + d.answer;
			return content;

		}).each(function(d) {
			var newG = d3.select(this);

			newG.append("circle").attr("class", "node").attr("r", function(d) {
				return getStyle(d).r[0];
			}).style("fill", function(d) {
				return getStyle(d).borderColor;
			}).style("stroke", function(d) {
				return getStyle(d).borderColor;
			}).style("stroke-width", function(d) {
				return getStyle(d).borderWidth;
			}).style("stroke-dasharray", function(d) {
				return getStyle(d).borderDasharray || null;
			});

			newG.append("circle").attr("class", "node0").attr("r", function(d) {
				return getStyle(d).r[0];
			}).style("fill", function(d) {
				return getStyle(d).borderColor;
			}).style("cursor", "pointer");

			newG.append("text").attr("dy", 4).attr("text-anchor", "middle").attr('class', 'text').style("fill", function(d) {
				return '#FFF';
			}).text(function(d) {
				return d.displayName ? d.displayName : d.name;
			}).style("font-size", function(d) {
				return getStyle(d).fontSize;
			}).style("font-family", "微软雅黑").style("font-weight", "bold").style("cursor", "pointer");

			newG.attr("transform", function(d) {
				return 'translate(' + [ d.x, d.y ] + ')';
			}).on("dblclick", function(d0, i0) {
				onNodeDBLClick.call(this, d0);
			}).on("click", function(d0, i0) {
				onNodeClick.call(this, d0);
			}).call(force.drag);
			$(newG[0][0]).trigger('click');
		});
		// yy.exit().remove();

		force.start();

		buildTips();
	}

	var buildTips = function() {
		$('g[title]').qtip({
			show : {
				solo : true,
				event : 'click'
			},
			hide : 'unfocus',
			position : {
				my : 'bottom center'
			}
		});
	};

	var selectedNode = null;

	var onNodeDBLClick = function(d0) {
		selectedNode = d0;
		if (selectedNode.expanded)
			return false;
		if (d0.type == 'value') {
			parent.ValueGraph.tabPanel.gp.getTopToolbar().f_value.setValue(d0.name);
			load(d0.name);
			return;
		}
		var input = d0.input;
		if (input) {
			if (input.indexOf('@') == 0)
				input = d0[input.substring(1)];
			$.ajax({
				url : 'value-graph!graph' + d0.type + '.action?limit_clazz_obj=6&input=' + encodeURIComponent(input),
				type : 'GET',
				async : false,
				success : function(respTxt) {
					try {
						if (respTxt) {
							var _nodes = null;
							eval('_nodes=' + respTxt);
							for ( var i = 1; i < _nodes.length; i++) {
								if (nodesMap[_nodes[i].id]) {
									links0.push({
										source : nodesMap[_nodes[i].id],
										target : selectedNode,
										color : '#CCC',
										id : generateRandomID()
									});
								} else {
									var n = node(_nodes[i]);
									n.x = selectedNode.x + getRandomInt(-70, 70);
									n.y = selectedNode.y + getRandomInt(-70, 70);
									nodes0.push(n);
									nodesMap[n.id] = n;
									links0.push({
										source : n,
										target : selectedNode,
										color : '#CCC',
										id : generateRandomID()
									});
								}
							}

							buildForce(true);

							// var ddd = $('#' + selectedNode.id);
							// var dom = ddd.get(0);
							// var dp = dom.parentNode;
							// ddd.remove();
							// dp.appendChild(dom);

							selectedNode.expanded = true;
						}
					} catch (e) {
						alert(e.message);
					}
				}
			});
		}
	};

	var onNodeClick = function(d0) {
		svg.selectAll('.node0').filter(function(d) {
			return true;
		}).style("fill", function(d, i) {
			return getStyle(d).borderColor;
		});
		d3.select(this.childNodes[1]).style("fill", function(d) {
			// var s = getStyle(d);
			// return s.fontColor;
			return '#FFF';
		});
		svg.selectAll('text').filter(function(d) {
			return true;
		}).style("fill", function(d, i) {
			// return getStyle(d).fontColor;
			return '#FFF';
		});
		d3.select(this.childNodes[2]).style('fill', function(d) {
			return getStyle(d).borderColor;
		});
		selectedNode = d0;
		if (selectedNode.level == 1) {
		} else if (selectedNode.level == 2) {
		} else if (selectedNode.level == 3) {
		}
	};

	var tick = function() {
		// Update the links
		svg.selectAll(".line").attr("x1", function(d) {
			return d.source.x;
		}).attr("y1", function(d) {
			return d.source.y;
		}).attr("x2", function(d) {
			return d.target.x;
		}).attr("y2", function(d) {
			return d.target.y;
		});
		// Translate the groups
		svg.selectAll(".gnode").attr("transform", function(d) {
			return 'translate(' + [ d.x, d.y ] + ')';
		});
	};

	buildForce();
	force.on("tick", tick);
};
