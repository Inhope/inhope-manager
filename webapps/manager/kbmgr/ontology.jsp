<%@page import="org.springframework.context.ApplicationContext"%>
<%@page import="org.codehaus.jackson.map.ObjectMapper"%>
<%@page import="com.incesoft.xmas.commons.LicenceHolder"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="res" uri="/WEB-INF/tld/import-res.tld"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="usr" uri="/WEB-INF/tld/login-user.tld"%>
<%@taglib prefix="mdl" uri="/WEB-INF/tld/mdl-ft.tld"%>
<%
	ApplicationContext ctx = WebApplicationContextUtils
			.getWebApplicationContext(application);
	LicenceHolder licenceholder = ctx.getBean(LicenceHolder.class);
	if (licenceholder != null && licenceholder.getLicence() != null) {
		request.setAttribute("supportPlatforms", licenceholder
				.getLicence().getSupportedPlatforms());
	}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>本体管理</title>
<script type="text/javascript">
	var supportPlatforms =
<%=new ObjectMapper().writeValueAsString(request
					.getAttribute("supportPlatforms"))%>
	;
</script>
<usr:checkPerm jsExportVar="kb_check_perm_result" permissionNames="kb" dump="true" />
<usr:name jsExportVar="kb_username" />
<mdl:dump name="kb" jsExportVar="kb_mdl_ft" />
<mdl:dump name="kb" jsExportVar="kb_mdl_props" type="props" />
<%@include file="/commons/extjsvable.jsp"%>
<script src="<c:url value="/ext4/ext-all-sandbox.js"/>" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="<c:url value="/ext4/ext-sandbox.css"/>" />
<link rel="stylesheet" type="text/css" href="<c:url value="/kbmgr/custom.css"/>" />
<link rel="stylesheet" type="text/css" href="<c:url value="/kbmgr/multitools.css"/>" />

<script src="<c:url value="/kbmgr/script/LocalProxy.js"/>" type="text/javascript"></script>
<script src="<c:url value="/kbmgr/script/LocalStore.js"/>" type="text/javascript"></script>
<res:import dir="script/ux" recursive="true" include=".*\.js" />
<res:import dir="script/obj/datatrans/" recursive="true" include=".*\.js" />
<script type="text/javascript">
window.OntologyNavPanel_classtype = '${param.classtype}'?parseInt('${param.classtype}'):null;
</script>
<script src="<c:url value="/kbmgr/script/index.js"/>" type="text/javascript"></script>
<script src="<c:url value="/kbmgr/script/obj/DrawDiagramWin.js"/>" type="text/javascript"></script>
<script src="<c:url value="/kbmgr/script/NavOntology.js"/>" type="text/javascript"></script>
<script src="<c:url value="/kbmgr/script/OntologyClassPanel.js"/>" type="text/javascript"></script>
<script src="<c:url value="/kbmgr/script/ClazzAttrDrawTabPanel.js"/>" type="text/javascript"></script>
<script src="<c:url value="/commons/scripts/raphael/generatorPicPre.js"/>" type="text/javascript"></script>
</head>

<body>
</body>
</html>