<%@page import="java.net.URLEncoder"%>
<%@page import="com.eastrobot.robotface.util.Simp2TranUtils"%>
<%@page import="com.eastrobot.commonsapi.ibotcluster.ClusterConfigServiceHelper"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="com.eastrobot.commonsapi.domain.FileDescription"%>
<%@page import="com.eastrobot.commonsapi.domain.RemoteFile"%>
<%@page import="com.eastrobot.commonsapi.RemoteFileService"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.UUID"%>
<%@page import="java.io.*"%>
<%@page import="com.incesoft.xmas.kbmgr.util.FileServiceImpl"%>
<%@page import="org.apache.commons.io.FileUtils"%>
<%
request.setCharacterEncoding("UTF-8");

//当前设置的faq信息
String question = request.getParameter("question") != null ? request.getParameter("question") : "";
//当前P4设置信息
String p4id = request.getParameter("p4id") != null ? request.getParameter("p4id") : "";
String p4name = request.getParameter("p4name") != null ? request.getParameter("p4name") : "";
String p4content = request.getParameter("p4content") != null ? request.getParameter("p4content") : "";
String savePath = pageContext.getServletContext().getRealPath("/") + "/kbmgr/p4data/";
String url = request.getParameter("url") != null ? request.getParameter("url") : "";
RemoteFileService remoteFileService = WebApplicationContextUtils.getWebApplicationContext(application).getBeansOfType(RemoteFileService.class).values().iterator().next();
boolean isSave = false;
//判断是否需要保存数据
String fileName = "index.html";
if (p4content != null && !p4content.trim().equals("")) {//执行保存操作
	final String name = p4name;
	final String content = p4content;
	p4content = p4content.replaceAll("(?si)<p>\\s*<meta (?:.+?)>\\s*</p>","");
	//src="../images/20120508163823_171.jpg"
	if (p4id != null && !p4id.trim().equals("")) {//更新
		savePath = savePath+p4id+"/";
		final String id = p4id;
		
		p4content = "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"></head><body style='font-size:12px'>"+p4content+"</body></html>";
		RemoteFile rf = new RemoteFile();
		FileDescription desc = new FileDescription();
		desc.setPath("/p4data/"+p4id+"/index.html");
		rf.setDescription(desc);
		String rfontent = p4content.replaceAll("(?si)(value|src|href)=\"[^\"]+?/p4data/images/([^\"]+?)\"","$1=\"../images/$2\"");
		rf.setData(rfontent.getBytes("utf-8"));
		remoteFileService.saveFiles(rf);
		//FileUtils.writeStringToFile(new File(savePath,fileName),p4content,"utf-8");
	} else {//新建
		p4id = UUID.randomUUID().toString().replace("-","");
		savePath = savePath+p4id+"/";
		p4content = "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"></head><body style='font-size:12px'>"+p4content+"</body></html>";
		
		RemoteFile rf = new RemoteFile();
		FileDescription desc = new FileDescription();
		desc.setPath("/p4data/"+p4id+"/index.html");
		rf.setDescription(desc);
		String rfontent = p4content.replaceAll("(?si)(value|src|href)=\"[^\"]+?/p4data/images/([^\"]+?)\"","$1=\"../images/$2\"");
		rf.setData(rfontent.getBytes("utf-8"));
		remoteFileService.saveFiles(rf);
		//FileUtils.writeStringToFile(new File(savePath,fileName),p4content,"utf-8");
	}
	isSave = true;
} else {//查询对应的P4设置信息
	savePath = savePath+p4id+"/";
	RemoteFile rf = remoteFileService.getFile("/p4data/"+p4id+"/index.html");
	if (rf != null){
		p4content = new String(rf.getData(),"utf-8");
	}
	/*
	if(FileServiceImpl.isFileExist(savePath+fileName)){
		p4content = FileUtils.readFileToString(new File(savePath,fileName),"utf-8");
	}*/
	if (p4content!=null)
		p4content = p4content.replaceAll("(?s)<html>(?:.+?)<body(?:.*?)>(.+)</body></html>","$1").replaceAll("(?si)<p>\\s*<meta (?:.+?)>\\s*</p>","");
	p4content = p4content.replaceAll("\\.\\./images/", "/manager/kbmgr/p4data/images/");
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	<meta charset="utf-8" />
	<!--用户自定义CSS引入-->
	<style type="text/css">
		*{font-family:Tahoma, Verdana, Georgia; font-size:12px;}
	</style>
	<script type="text/javascript" src="../../commons/ckeditor4/ckeditor.js"></script>
	<script type="text/javascript">
		function check() {
			CKEDITOR.instances.p4content.updateElement();
			var pcontent = document.getElementById("p4content").value;
			if(!pcontent) {
				alert("<%=Simp2TranUtils.simp2tran("p4设计内容不能为空！")%>");
				document.getElementById("p4content").focus();
				return false;
			}
			return true;
		}
	</script>
</head>
<body style="overflow:hidden;margin: 0">
	<table width="100%" height="100%" cellpadding="0" cellspacing="0">
		<form name="designP4" method="post" action="index.jsp" onsubmit="return check();" style="margin-bottom:0;">
			<input type="hidden" name="p4id" value="<%=URLEncoder.encode(p4id)%>" />
			<input type="hidden" name="url" value="<%=URLEncoder.encode(url.replace("/", "**")).replace("**","/")%>" />
		<tr><td align="center">
		<div style="width:100%;height:100%">
			<table cellpadding="0" cellspacing="0" width="100%" height="100%" border="0" align="center">
				<colgroup>
					<col style="width: 50px;">
					<col style="">
				</colgroup>
				<tr>
					<td colspan="2" width="100%" height="100%">
						<textarea id="p4content" name="p4content" style="width: 100%; height:100%;visibility:hidden;"><%=htmlspecialchars(p4content)%></textarea>
						<script>
						CKEDITOR.replace('p4content',
						{
							height:0,width:'100%',
							toolbar:
							[
							    { name: 'document',    items : [ 'Source','-','NewPage','DocProps','-','Templates' ] },
							    { name: 'clipboard',   items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
							    { name: 'editing',     items : [ 'Find','Replace','-','SelectAll'] },
							    '/',
							    { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
							    { name: 'paragraph',   items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
							    { name: 'links',       items : [ 'Link','Unlink','Anchor' ] },
							    { name: 'insert',      items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','Word2HTML'] },
							    '/',
							    { name: 'styles',      items : [ 'Format','Font','FontSize' ] },
							    { name: 'colors',      items : [ 'TextColor','BGColor'] }   
							],
							extraPlugins : 'word2html',
							filebrowserBrowseUrl : '<%=request.getContextPath()+(ClusterConfigServiceHelper.getAppURI(request)!=null?"/app/"+URLEncoder.encode(ClusterConfigServiceHelper.getAppURI(request)):"")%>/kbmgr/p4editor/jsp/file_manager_json.jsp?p4id=<%=URLEncoder.encode(p4id)%>',
					        filebrowserUploadUrl : '<%=request.getContextPath()+(ClusterConfigServiceHelper.getAppURI(request)!=null?"/app/"+URLEncoder.encode(ClusterConfigServiceHelper.getAppURI(request)):"")%>/kbmgr/p4editor/jsp/upload_json.jsp?p4id=<%=URLEncoder.encode(p4id)%>'
						});
						CKEDITOR.on( 'instanceReady', function( ev )
						{
							var editor = ev.editor;
							var editorContainer = document.getElementById('p4content').parentNode;
							var resizeFn = function(){
								editor.resize('100%',0);
								editor.resize('100%',document.documentElement.clientHeight-25);
							}
							if (editorContainer.addEventListener)
							 window.addEventListener('resize',resizeFn,false)
							else
							 window.attachEvent('onresize',resizeFn)
							resizeFn()
							editor.focus()
						});
						</script>
					</td>
				</tr>
				<tr>
					<td align="center" colspan="2">
						<input type="submit" name="button" value="<%=Simp2TranUtils.simp2tran("保存设置")%>" />&nbsp;&nbsp;&nbsp;&nbsp;
						<%
						if (p4id != null && !p4id.trim().equals("")) {
						%>
						<script type="text/javascript">
						function viewp4() {
							var p4url = "../p4data/<%=URLEncoder.encode(p4id)%>/index.html";
							window.open(p4url);
						}
						window.p4id = '<%=p4id%>'
						</script>
						<input type="button" value="<%=Simp2TranUtils.simp2tran("预览效果")%>" onclick="viewp4();"/>
						<%
						}
						%>
					</td>
				</tr>
			</table>
		</form>
	</div>
	</td></tr></table>
	<iframe id="callbackIframe" width="0" height="0" style="display: none;"></iframe>
</body>
</html>
<script type="text/javascript">
	var isSave = <%=isSave%>; 
	if(isSave){
		var url = '<%=URLEncoder.encode(url.replace("/", "**")).replace("**","/")%>';
		url = url +'?p4id=<%=URLEncoder.encode(p4id)%>'; 
		url = url+'&v='+Math.random();
		//alert(url);
		document.getElementById("callbackIframe").src = url;
	}
	var word2htmlCallback = function(id) {
		if (!id)
			alert('转换失败，请换其他文档重试');
		else 
			p4Win.editP4(id);
	}
</script>
<%!
private String htmlspecialchars(String str) {
	str = str.replaceAll("&", "&amp;");
	str = str.replaceAll("<", "&lt;");
	str = str.replaceAll(">", "&gt;");
	str = str.replaceAll("\"", "&quot;");
	return str;
}
%>
