<%@page import="com.incesoft.ai.commonsapi.service.FAQBaseManager"%>
<%@page import="com.incesoft.xmas.commons.StringUtils"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@page import="org.codehaus.jackson.map.ObjectMapper"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="res" uri="/WEB-INF/tld/import-res.tld"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="usr" uri="/WEB-INF/tld/login-user.tld"%>
<%@taglib prefix="mdl" uri="/WEB-INF/tld/mdl-ft.tld"%>
<%
	ApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(application);
	FAQBaseManager faqBaseManager  = ctx.getBean("faqBaseManager",FAQBaseManager.class);
	String faqId = request.getParameter("faqId");
	String module = request.getParameter("module");
	if (StringUtils.isBlank(faqId )){
		out.write("缺少参数 faqId");
		return;
	}else if (StringUtils.isBlank(module)){
		out.write("缺少参数 module");
		return;
	}
	faqBaseManager.removeNodes(module, new String[]{faqId});
	out.write("ok");
%>