AISyswordOperationPanel = function(_cfg) {
	var self = this;
	var _pageSize = 20;

	this.getPageSize = _pageSize;
	var _store = new Ext.data.Store({
				proxy : new Ext.data.HttpProxy({
							url : 'sys-wordclass!listOperation.action'
						}),
				reader : new Ext.data.JsonReader({
							idProperty : 'id',
							root : 'data',
							fields : [{
										name : 'id',
										type : 'string'
									}, {
										name : 'path',
										type : 'string'
									}, {
										name : 'word',
										type : 'string'
									}, {
										name : 'action',
										type : 'string'
									}, {
										name : 'actiontime',
										type : 'string'
									}]
						}),
				writer : new Ext.data.JsonWriter()
			});
	var rowNumber;
	var ds = _store;
	var checkboxSelectionModel = new Ext.grid.CheckboxSelectionModel({})
	var myExpander = new Ext.grid.RowExpander({
		tpl : new Ext.Template('<p style="color:#999;margin:2px 0px 2px 48px;">{answer}</p>')
	});

	var myColumnModel = new Ext.grid.ColumnModel([
			rowNumber = new Ext.grid.RowNumberer(), checkboxSelectionModel, {
				header : 'id',
				dataIndex : 'id',
				hidden : true
			}, {
				header : '系统词',
				dataIndex : 'word'
			}, {
				header : '所属词类',
				dataIndex : 'path'
			}, {
				header : '操作状态',
				dataIndex : 'action',
				renderer : function(v) {
					return v == '1' ? '新增' : '删除'
				}
			}, {
				header : '操作时间',
				dataIndex : 'actiontime'
			}]);

	myColumnModel.defaultSortable = false;

	var actionTbar = new Ext.Toolbar({
				items : [{
							text : '操作记录导入',
							iconCls : 'icon-ontology-import',
							handler : function() {
								self.getDataTransToolkit().doImport()
							}
						}, {
							text : '操作记录导出',
							iconCls : 'icon-ontology-export',
							handler : function() {
								self.getDataTransToolkit().doExport()
							}
						}, {
							text : '执行',
							iconCls : 'icon-ontology-import',
							handler : function() {
								self.exec();
							}
						}, {
							text : '刷新',
							iconCls : 'icon-refresh',
							width : 50,
							handler : function() {
								_store.reload()
							}
						}]
			});

	var _fileForm = new Ext.FormPanel({
				layout : "fit",
				frame : true,
				border : false,
				autoHeight : true,
				waitMsgTarget : true,
				defaults : {
					bodyStyle : 'padding:10px'
				},
				margins : '0 0 0 0',
				labelAlign : "left",
				labelWidth : 50,
				fileUpload : true,
				items : [{
							xtype : 'fieldset',
							title : '选择文件',
							autoHeight : true,
							items : [{
										name : 'baseWordFile',
										xtype : "textfield",
										fieldLabel : '文件',
										inputType : 'file',
										allowBlank : false,
										anchor : '96%'
									}]
						}]
			});
	var _importPanel = new Ext.Panel({
				border : false,
				layout : "fit",
				layoutConfig : {
					animate : true
				},
				items : [_fileForm],
				buttons : [{
					text : "开始导入",
					handler : function() {
						var btn = this;
						this.disable();
						// 开始导入
						var fileForm = _fileForm.getForm();
						if (fileForm.isValid()) {
							fileForm.submit({
										url : 'ai-wordclass-manager!importData.action',
										success : function(form, action) {
											btn.enable();
											var result = action.result.data;
											var timer = new ProgressTimer({
														initData : result,
														progressId : 'importBaseWord',
														boxConfig : {
															title : ' 正在导入基础词内容...'
														},
														finish : function(p,
																response) {
															Ext.MessageBox
																	.hide();
															Ext.Msg.alert(
																	'成功提示',
																	'导入成功!');
															self.wordText
																	.setValue("");
															self.loadData();
														}
													});
											timer.start();
										},
										failure : function(form) {
											btn.enable();
											Ext.MessageBox.hide();
											Ext.Msg.alert('导入失败', '文件上传失败');
										}
									});
						}
					}
				}, {
					text : "关闭",
					handler : function() {
						importWindow.hide();
					}
				}]
			});

	var importWindow = new Ext.Window({
				border : false,
				width : 350,
				title : '基础词导入',
				defaults : {// 表示该窗口中所有子元素的特性
					border : false
					// 表示所有子元素都不要边框
				},
				plain : true,// 方角 默认
				modal : true,
				plain : true,
				shim : true,
				closeAction : 'hide',
				collapsible : true,// 折叠
				closable : true, // 关闭
				resizable : false,// 改变大小
				draggable : true,// 拖动
				minimizable : false,// 最小化
				maximizable : false,// 最大化
				animCollapse : true,
				constrainHeader : true,
				autoHeight : false,
				items : [_importPanel]
			});
	this.importWindow = importWindow;
	var pagingBar = new Ext.PagingToolbar({
				store : _store,
				displayInfo : true,
				pageSize : _pageSize,
				prependButtons : true,
				beforePageText : '第',
				afterPageText : '页，共{0}页',
				displayMsg : '第{0}到{1}条记录，共{2}条',
				emptyMsg : "没有记录",
				prependButtons : false
			})
	var cfg = {
		region : 'center',
		border : false,
		frame : false,
		store : _store,
		loadMask : true,
		colModel : myColumnModel,
		tbar : actionTbar,
		sm : checkboxSelectionModel,
		bbar : pagingBar,

		viewConfig : {
			forceFit : true
		}
	}
	AISyswordOperationPanel.superclass.constructor.call(this, Ext.applyIf(_cfg
							|| {}, cfg));

}
Ext.extend(AISyswordOperationPanel, Ext.grid.GridPanel, {
			loadData : function() {
				this.getStore().load({
							params : {
								start : 0,
								limit : this.getPageSize
							}
						});
			},
			exec : function() {
				Ext.Msg.confirm('提示', '确定执行?', function(btn) {
							if (btn == 'yes') {
								Ext.Ajax.request({
											url : 'sys-wordclass!execOperation.action',
											success : function(response) {
												Ext.Msg.alert('提示', '执行成功');
											},
											failure : function(response) {
												Ext.Msg.alert("提示", "出错或者超时.<br>"+response.responseText);
											},
											scope : this
										});
							}
						});
			},
			getDataTransToolkit : function() {
				if (!this.dstoolkit) {
					this.dstoolkit = new datatrans.DataTransToolkit({
								dataName : '系统词操作记录',
								baseActionUrl : 'sys-wordclass.action',
								baseProgressId : 'ai-sysword'
							});
					this.dstoolkit.on('importSuccess', function() {
								this.store.reload()
							})
				}
				return this.dstoolkit
			},
			destroy : function() {
				try {
					AISyswordOperationPanel.superclass.destroy.call(this)
				} catch (e) {
				}
				if (this.dstoolkit) {
					this.dstoolkit.destroy();
					delete this.dstoolkit
				}
			}
		})