AITesterWindow = function(cfg) {
	var self = this;
	self.serverUrl = cfg.serverUrl;
	var answerField = new Ext.form.TextArea({
		  hideLabel : true,
		  readOnly : true,
		  anchor : '100% 100%'
	  });
	this.answerField = answerField;
	var questionField = new Ext.form.TextArea({
		  hideLabel : true,
		  anchor : '100% -29',
		  enableKeyEvents : true,
		  autoScroll : true,
		  listeners : {
			  specialkey : function(field, e) {
				  if (e.getKey() == e.ENTER) {
					  var msg = self.createQuestionMgs(this.currentUserId);
					  if (msg) {
						  self.lastestQuestion = self.lastQuestion;
						  self.lastQuestion = msg;
					  }
					  self.send();
				  }
			  }
		  }
	  });
	this.questionField = questionField;
	// var categoriesField = new Ext.form.TextField({
	// anchor : '98%',
	// fieldLabel : '类别(逗号分隔)'
	// });
	// this.categoriesField = categoriesField;

	var multiCondition = Dashboard.createDimChooserEditable(110, false, true);
	this.multiCondition = multiCondition;
	this.categoriesField = multiCondition;

	var userIdField = new Ext.form.TextField({
		  anchor : '98%',
		  fieldLabel : '用户ID'
	  });
	this.userIdField = userIdField;

	var dimensionField = new Ext.form.TextField({
		  anchor : '98%',
		  fieldLabel : '维度'
	  });
	this.dimensionField = dimensionField;

	var modelComboBox = new Ext.form.ComboBox({
		  anchor : '98%',
		  width : 200,
		  fieldLabel : '模式',
		  triggerAction : 'all',
		  editable : false,
		  mode : 'local',
		  value : 'RENDER_TYPE_DEBUG',
		  store : new Ext.data.ArrayStore({
			    fields : ['name', 'value'],
			    data : [['调试模式', 'RENDER_TYPE_DEBUG'], ['一般模式', 'RENDER_TYPE_RAW'], ['列表模式', 'RENDER_TYPE_LIST0']]
		    }),
		  valueField : 'value',
		  displayField : 'name'
	  });
	this.modelComboBox = modelComboBox;

	var maxResultSize = new Ext.form.NumberField({
		  fieldLabel : '最大回复数',
		  anchor : '98%',
		  value : '10'
	  })
	this.maxResultSize = maxResultSize;

	var segmentResultField = new Ext.form.TextArea({
		  hideLabel : true,
		  style : 'background:#ffffc8',
		  readOnly : true,
		  anchor : '100% -25'
	  });
	this.segmentResultField = segmentResultField;
	var matchResultField = new Ext.form.TextArea({
		  hideLabel : true,
		  readOnly : true,
		  style : 'background:#ffffc8',
		  anchor : '100% -29'
	  });
	this.matchResultField = matchResultField;
	var extraResultField = new Ext.form.TextField({
		  hideLabel : true,
		  readOnly : true,
		  style : 'background:#ffffc8',
		  anchor : '100%'
	  });
	this.extraResultField = extraResultField;

	this.lastQuestion = "";
	this.lastestQuestion = "";

	var sendButton = new Ext.SplitButton({
		  width : 40,
		  text : '发送',
		  handler : function() {
			  var msg = self.createQuestionMgs(this.currentUserId);
			  if (msg) {
				  self.lastestQuestion = self.lastQuestion;
				  self.lastQuestion = msg;
			  }
			  self.send();
		  },
		  menu : new Ext.menu.Menu({
			    border : false,
			    showSeparator : false,
			    width : 50,
			    items : [new Ext.Button({
				        width : 40,
				        text : '前一句',
				        handler : function() {
					        if (self.lastQuestion) {
						        self.questionField.setValue(self.lastQuestion.q);
						        self.userIdField.setValue(self.lastQuestion.userId);
						        self.modelComboBox.setValue(self.lastQuestion.renderType);
						        self.dimensionField.setValue(self.lastQuestion.dimension);
						        self.maxResultSize.setValue(self.lastQuestion.maxResultSize);
						        self.send();
					        } else {
						        Ext.Msg.alert('提示', '当前还没有前一句内容');
						        return;
					        }
				        }
			        }), new Ext.Button({
				        width : 40,
				        text : '最前句',
				        handler : function() {
					        if (self.lastestQuestion) {
						        self.questionField.setValue(self.lastestQuestion.q);
						        self.userIdField.setValue(self.lastestQuestion.userId);
						        self.modelComboBox.setValue(self.lastestQuestion.renderType);
						        self.dimensionField.setValue(self.lastestQuestion.dimension);
						        self.maxResultSize.setValue(self.lastestQuestion.maxResultSize);
						        self.send();
					        } else {
						        Ext.Msg.alert('提示', '当前还没有最前一句内容');
						        return;
					        }
				        }
			        })]
		    })
	  });
	this.sendButton = sendButton;

	var multiButton = new Ext.Button({
		  hidden : !Dashboard.u().allow('kb.misc.aiconsole.advance'),
		  width : 20,
		  iconCls : 'icon-multi',
		  handler : function() {
			  if (Ext.getCmp("aiTestWindowAdvancedFun").hidden) {
				  questionField.setHeight(questionField.getHeight() - 59);
				  Ext.getCmp("aiTestWindowAdvancedFun").show();
			  } else {
				  questionField.setHeight(questionField.getHeight() + 59);
				  Ext.getCmp("aiTestWindowAdvancedFun").hide();
			  }
		  }
	  });

	var restartButton = new Ext.Button({
		  width : 70,
		  text : '重新开始',
		  handler : function() {
			  self.restart();
		  }
	  });
	this.restartButton = restartButton;

	this.isVersionStandard = isVersionStandard();
	var eastPanel = {
		region : self.isVersionStandard ? 'center' : 'west',
		width : 380,
		layout : 'anchor',
		items : [{
			  xtype : 'fieldset',
			  title : '回答',
			  anchor : '100% -160',
			  items : answerField
		  }, {
			  xtype : 'fieldset',
			  title : '提问',
			  anchor : '100%',
			  height : 150,
			  items : [questionField, {
				    layout : 'form',
				    border : 0,
				    items : [{
					      layout : 'column',
					      items : [{
						        columnWidth : .57,
						        layout : 'form',
						        labelWidth : 88,
						        items : multiCondition
					        }, {
						        columnWidth : .21,
						        layout : 'anchor',
						        items : restartButton
					        }, {
						        columnWidth : .14,
						        layout : 'anchor',
						        items : sendButton
					        }, {
						        columnWidth : .08,
						        layout : 'anchor',
						        items : multiButton
					        }]
				      }, {
					      id : 'aiTestWindowAdvancedFun',
					      hidden : true,
					      style : 'border:solid 1px #D5D5D5;padding-top:2px;',
					      items : [{
						        border : 1,
						        items : [{
							          layout : 'column',
							          items : [{
								            columnWidth : .4,
								            layout : 'form',
								            items : userIdField,
								            labelWidth : 40
							            }, {
								            columnWidth : .4,
								            layout : 'form',
								            style : 'margin-left:20px;',
								            labelWidth : 30,
								            items : dimensionField
							            }]
						          }, {
							          layout : 'column',
							          items : [{
								            columnWidth : .4,
								            layout : 'form',
								            items : modelComboBox,
								            labelWidth : 40
							            }, {
								            columnWidth : .4,
								            layout : 'form',
								            style : 'margin-left:20px;',
								            labelWidth : 65,
								            items : maxResultSize
							            }]
						          }]
					        }]
				      }]
			    }]
		  }]
	};

	var westPane = {
		region : 'center',
		layout : 'anchor',
		bodyStyle : 'padding: 0 0 0 10px;',
		items : [{
			xtype : 'fieldset',
			title : '分词结果',
			anchor : '100% -220',
			layout : 'anchor',
			items : [{
				  id : 'isShow',
				  xtype : 'checkbox',
				  hideLabel : true,
				  checked : false,
				  height : 25,
				  boxLabel : '是否显示小i词类'
			  }, new Ext.Panel({
				    id : 'aiSegmentResultPanel',
				    anchor : '100% -25',
				    bodyStyle : 'border:solid 1px #b5b8c8;margin-bottom:10px;padding-top:2px;background:rgb(255,255,200);',
				    html : '',
				    autoScroll : true
			    })]
			 // }, segmentResultField]
		 }, {
			xtype : 'fieldset',
			title : '匹配结果',
			height : 210,
			anchor : '100%',
			items : [new Ext.Panel({
				    id : 'aiMatchResultPanel',
				    bodyStyle : 'border:solid 1px #b5b8c8;margin-bottom:5px;padding-top:2px;background:rgb(255,255,200);height:140px;',
				    html : '',
				    autoScroll : true
			    }), extraResultField]
		}]
	};
	var panel = new Ext.Panel({
		  anchor : '100% 100%',
		  layout : 'border',
		  frame : true,
		  items : self.isVersionStandard ? [eastPanel] : [eastPanel, westPane]
	  });

	AITesterWindow.superclass.constructor.call(this, {
		  title : 'AI测试终端',
		  iconCls : 'icon-aitester',
		  closeAction : 'hide',
		  plain : true,
		  modal : false,
		  width : self.isVersionStandard ? 420 : 800,
		  height : 500,
		  layout : 'fit',
		  maximizable : true,
		  collapsible : true,
		  items : panel,
		  bodyStyle : 'padding: 0 1px 1px 0px;',
		  listeners : {
			  show : function() {
				  questionField.getEl().dom.onkeydown = function() {
					  var keyCode = arguments.length > 0 ? arguments[0].which : event.keyCode;
					  try {
						  if (13 == keyCode) {
							  var msg = self.createQuestionMgs(this.currentUserId);
							  if (msg) {
								  self.lastestQuestion = self.lastQuestion;
								  self.lastQuestion = msg;
							  }
							  self.send();
						  }
					  } catch (exception) {
					  }
				  };
				  questionField.focus(true, true);
			  }
		  }
	  });
	this.currentUserId = this.createUser();
	userIdField.setValue(this.createUser());

	var traceContentTextarea = new Ext.form.TextArea({
		  id : 'AITestertraceContentTextarea',
		  name : "traceContentArea",
		  width : '98%',
		  height : '99%',
		  readOnly : true,
		  preventScrollbars : true,
		  style : 'background:none repeat scroll 0 0 #D9F1C9;overflow:scroll;overflow-x:hidden;color:black;'
	  });
	var traceContentWindow = new Ext.Window({
		  title : "引擎轨迹",
		  closable : true,
		  width : 400,
		  height : 450,
		  // autoWidth : true,
		  // autoHeight : true,
		  maximizable : true,
		  collapsible : true,
		  modal : false,
		  closeAction : 'hide',
		  plain : true,
		  items : [traceContentTextarea],
		  listeners : {
			  'deactivate' : function() {
				  this.setZIndex(9999);
			  }
		  }
	  });
	this.traceContentTextarea = traceContentTextarea;
	this.traceContentWindow = traceContentWindow;

}
Ext.extend(AITesterWindow, Ext.Window, {
	  send : function() {
		  this.sendButton.disable();
		  var msg = this.createQuestionMgs(this.currentUserId);

		  if (!msg || !msg.q) {
			  Ext.Msg.alert('提示', '提问内容不能为空');
			  this.sendButton.enable();
			  return;
		  } else
			  this.sendMessagePOST(msg);

		  this.questionField.focus(true, true);
	  },
	  restart : function() {
		  this.currentUserId = this.createUser();
		  this.userIdField.setValue(this.createUser());
		  Ext.getCmp("aiSegmentResultPanel").body.update('');
		  Ext.getCmp("aiMatchResultPanel").body.update('');
	  },
	  cleanCmpValue : function() {
		  this.questionField.setValue("");
		  this.matchResultField.setValue("");
		  this.extraResultField.setValue("");
		  // this.segmentResultField.setValue("");
	  },
	  createUser : function() {
		  this.cleanCmpValue();
		  this.answerField.setValue("");
		  this.questionField.focus(true, true);
		  this.sendButton.enable();
		  return new Date().getTime();
	  },
	  createQuestionMgs : function(user) {
		  var question = this.clearBr(this.questionField.getValue());
		  if (question == null || question == "")
			  return null;

		  var userId = this.userIdField.getValue();
		  var model = this.modelComboBox.getValue();
		  var dimension = this.dimensionField.getValue();
		  var maxResultSize = this.maxResultSize.getValue();

		  var categories = this.categoriesField.getRawValue();
		  return {
			  'u' : user + "",
			  'q' : question + "",
			  'c' : categories + "",
			  'userId' : userId + "",
			  'renderType' : model + "",
			  'dimension' : dimension + "",
			  'maxResultSize' : maxResultSize + ""
		  };
	  },

	  sendMessagePOST : function(message) {
		  var self = this;
		  Ext.Ajax.request({
			    url : self.serverUrl,
			    params : message,
			    success : function(form, action) {
				    var resultObj = Ext.util.JSON.decode(form.responseText);
				    if (resultObj)
					    self.updateCmpData(resultObj);
			    },
			    failure : function(response) {
				    Ext.Msg.alert('错误', response.responseText);
				    self.sendButton.enable();
			    }

		    });
	  },

	  updateCmpData : function(_data) {
		  data = _data.debugResponse;
		  var self = this;
		  var suggestSemantic = _data.suggetSemantic;
		  // answerResult
		  var SPLIT_LINE = this.addBr("------------------------------------------------------", 3);
		  var answerResultStr;
		  var answerResult = data.response.answer;
		  if ("ReverseAsk" == _data.responseType)
			  answerResult = data.response.question;
		  var questionStr = "提问: " + this.addBr(this.clearBr(this.questionField.getValue()), 2);

		  // var traceContent = data.response.trace; // 添加trace信息显示
		  var traceContent = data.trace; // 添加trace信息显示
		  if (traceContent) {
			  this.showTraceContent(traceContent);
		  }

		  this.cleanCmpValue();

		  if (answerResult == null)
			  answerResult = "****没有回答***";
		  answerResultStr = questionStr + "回答: " + this.addBr(answerResult, 1) + SPLIT_LINE;

		  var oldAnswerRsStr = this.answerField.getValue();
		  this.answerField.setValue(oldAnswerRsStr + answerResultStr);

		  // segmentResult
		  var segmentResult = new Array();
		  if (data.segmentResult)
			  segmentResult = data.segmentResult;
		  var segmentResultStr = "";
		  var semanticAnalyseResult = "";
		  for (var i = 0; i < segmentResult.length; i++) {
			  var segmentItem = segmentResult[i];
			  segmentResultStr += "词: " + this.addBr(segmentItem.content, 1) + "<br />";
			  var semanticResult = "[";
			  var isBrackets = true;
			  if (segmentItem.attributes) {
				  var itemSement = null;
				  for (var k in segmentItem.attributes) {
					  if (k.indexOf("path") == 0 || k.indexOf("semantic") == 0) {
						  var isClick = !(segmentItem.attributes[k].indexOf('@') == 0 || segmentItem.attributes[k].indexOf('基础词/') == 0);
						  segmentResultStr += (isClick ? "<a style='text-decoration:underline;' title='点击打开词类' href='javaScript:AssistNavPanel.aiTesterWindow.openWordclass(\""
						    + segmentItem.attributes[k] + "\");'>" : "")
						    + this.addBr(segmentItem.attributes[k], 2) + (isClick ? "</a>" : "") + "<br /><br />";
						  itemSement = (itemSement ? (itemSement + "|") : "") + this.searchWordlcass(segmentItem.attributes[k]);
						  if ("基础词" == itemSement) {
							  semanticResult = segmentItem.content;
							  isBrackets = false;
						  }
					  }
				  }
				  if (isBrackets) {
					  if (itemSement)
						  semanticResult += itemSement;
					  else {
						  semanticResult = this.searchWordlcass(segmentItem.content);
						  isBrackets = false;
					  }
				  }
			  } else {
				  semanticResult = this.searchWordlcass(segmentItem.content);
				  isBrackets = false;
			  }
			  if (isBrackets)
				  semanticResult += "]";
			  semanticAnalyseResult += semanticResult;
		  }
		  // this.segmentResultField.setValue(segmentResultStr);
		  Ext.getCmp("aiSegmentResultPanel").body.update(segmentResultStr);

		  // matchResult
		  var matchResultStr = "";
		  if (data.response && _data.responseType == 'Standard') {
			  var standardQuestionStr = data.response.targetQuestions;
			  if (standardQuestionStr.length > 0) {
				  var self = this;
				  matchResultStr += "标准问题: ";
				  // for (var i = 0; i < standardQuestionStr.length; i++) {
				  var standardQue = standardQuestionStr[0];
				  if (standardQue.indexOf(" ≈≈≈ ") > -1) {
					  var standardQues = standardQue.split(" ≈≈≈ ");
					  var clickStandard = '';
					  Ext.each(standardQues, function(sq) {
						    var openStandQues = sq;
						    if (sq.lastIndexOf(")") == sq.length - 1) {
							    openStandQues = sq.substring(0, sq.lastIndexOf("("));
						    }
						    clickStandard += "<a style='text-decoration:underline;'  title='点击打开标准问题' href='javaScript:AssistNavPanel.aiTesterWindow.openStanQuestion(\"" + openStandQues + "\");'>"
						      + self.addBr(sq, 1) + "</a>" + " ≈≈≈ ";
					    });
					  // matchResultStr += clickStandard.substring(0, clickStandard.length -
					  // 5) + (i < (standardQuestionStr.length - 1) ? "," : "");
					  matchResultStr += clickStandard.substring(0, clickStandard.length - 5);
					  // i++;
				  } else {
					  matchResultStr += "<a style='text-decoration:underline;'  title='点击打开标准问题' href='javaScript:AssistNavPanel.aiTesterWindow.openStanQuestion(\"" + standardQue + "\");'>"
					    + this.addBr(standardQue, 1) + "</a>";

				  }
				  // }
				  // matchResultStr = matchResultStr.substring(0, matchResultStr.length - 1)
				  // + "<br />";
				  matchResultStr = matchResultStr + "<br />";
			  }
			  // else
			  // matchResultStr += "标准问题: <a style='text-decoration:underline;'
			  // title='点击打开标准问题'
			  // href='javaScript:AssistNavPanel.aiTesterWindow.openStanQuestion(\"" +
			  // standardQuestionStr
			  // + "\");'>" + this.addBr(standardQuestionStr, 1) + "</a><br />";
			  var matchQuestion = new Array();
			  if (data.matchedQuestion)
				  matchQuestion = data.matchedQuestion;
			  if (semanticAnalyseResult != suggestSemantic)
				  matchResultStr += "分析语义: " + this.addBr(semanticAnalyseResult, 1) + "<br />";
			  if (Dashboard.u().allow('kb.misc.aiconsole.advance'))
				  matchResultStr += "推荐语义: " + this.addBr(suggestSemantic, 1) + "<br />";
			  for (var i = 0; i < matchQuestion.length; i++) {
				  var matchQuestionItem = matchQuestion[i];
				  matchResultStr += "匹配问题: " + this.addBr(matchQuestionItem.content, 1) + "<br />";
				  if (matchQuestionItem.categories)
					  matchResultStr += "类别:       " + this.addBr(matchQuestionItem.categories, 1);
				  if (i == 0) {
					  var attach = data.response.attachment;
					  if (attach) {
						  var dim = attach.dim;
						  var cmd = attach.cmd;
						  if (dim) {
							  var dimStr = '维度:       ';
							  for (var k in dim) {
								  dimStr += k + '(' + dim[k].join(',') + ');'
							  }
							  matchResultStr += this.addBr(dimStr, 1) + "<br />";
						  }
						  if (cmd) {
							  var cmdStr = '指令:       ';
							  for (var k in cmd) {
								  if (k != null) {
									  if (cmd[k] != null)
										  cmdStr += k + '(' + cmd[k].join(',') + ');'
									  else
										  cmdStr += k + ';'
								  }
							  }
							  matchResultStr += this.addBr(cmdStr, 1) + "<br />";
						  }
					  }
				  }
			  }
			  var similaritiesStr = data.similarities;
			  if (similaritiesStr != null)
				  matchResultStr += "相似度:    " + similaritiesStr + "<br />";
			  // this.matchResultField.setValue(matchResultStr);
			  Ext.getCmp("aiMatchResultPanel").body.update(matchResultStr);
		  } else {
			  if (semanticAnalyseResult != suggestSemantic)
				  matchResultStr += "分析语义: " + this.addBr(semanticAnalyseResult, 1) + "<br />";
			  if (Dashboard.u().allow('kb.misc.aiconsole.advance'))
				  matchResultStr += "推荐语义: " + this.addBr(suggestSemantic, 1);
			  // this.matchResultField.setValue(matchResultStr);
			  Ext.getCmp("aiMatchResultPanel").body.update(matchResultStr);
		  }
		  var extraResultStr = "回答类型:" + _data.responseType;
		  if (data.response && data.response.moduleId)
			  extraResultStr += "  知识库ID:" + data.response.moduleId;
		  if (data.response && data.response.nodeId)
			  extraResultStr += "  标准问ID:" + data.response.nodeId;
		  extraResultStr += "  处理时间:" + data.executeTime + " ms ";
		  this.extraResultField.setValue(extraResultStr);

		  //
		  this.answerField.getEl().dom.scrollTop += this.answerField.getEl().dom.scrollHeight;
		  this.sendButton.enable();
	  },
	  showTraceContent : function(traceContent) {
		  if (this.traceContentWindow.hidden)
			  this.traceContentWindow.show();

		  if (!this.traceContentTextarea.getValue()) {
			  this.traceContentTextarea.setValue(traceContent);
		  } else
			  this.traceContentTextarea.setValue(this.traceContentTextarea.getValue() + "\r\n" + traceContent);
		  this.traceContentTextarea.el.dom.scrollTop = this.traceContentTextarea.el.dom.scrollHeight;
	  },
	  addBr : function(context, rowNumber) {
		  var br = "\n";
		  for (var i = 1; i < rowNumber; i++) {
			  br += br;
		  }
		  return context + br;
	  },
	  clearBr : function(key) {
		  key = key.replace(/<\/?.+?>/g, "");
		  key = key.replace(/[\f\n\r\t\v]/g, "");
		  return key;
	  },
	  searchWordlcass : function(value) {
		  var lastSize = value.lastIndexOf("/");
		  if (lastSize > 0) {
			  value = value.substring(0, lastSize);
			  value = value.substring(value.lastIndexOf("/") > 0 ? value.lastIndexOf("/") + 1 : 0, lastSize);
		  }
		  var s = value.indexOf("=");
		  if (s > 0) {
			  value = value.substring(0, s);
		  }
		  return value;
	  },
	  openWordclass : function(wordPath) {
		  var wordArrys = wordPath.split('/');
		  var wordclassName = wordArrys[wordArrys.length - 2];
		  Dashboard.openWordclassName = wordclassName;
		  Ext.Ajax.request({
			    url : 'wordclass!getWordclassData.action',
			    params : {
				    name : wordclassName
			    },
			    success : function(form, action) {
				    var resultObj = Ext.util.JSON.decode(form.responseText);
				    if (resultObj.success) {
					    var data = resultObj.data;
					    Dashboard.wordclassPresetData = data;
					    Dashboard.wordclassPresetData.wordclassName = wordclassName;
					    ShortcutToolRegistry.execAction("wordclassManager");
				    }
			    },
			    failure : function(response) {
				    Ext.Msg.alert("错误", response.responseText);
				    btn.enable();
			    }
		    })
	  },
	  openStanQuestion : function(question) {
		  Ext.Ajax.request({
			    url : 'ontology-object-value!queryFaqQuestion.action',
			    params : {
				    question : question
			    },
			    success : function(form, action) {
				    var resultObj = Ext.util.JSON.decode(form.responseText);
				    if (resultObj.success) {
					    // ShortcutToolRegistry.execAction("knowledgeManager");
					    parent.navToKB(resultObj.data.objectId, resultObj.data.dimValueId, resultObj.data.valueId, true);
				    }
			    },
			    failure : function(response) {
				    Ext.Msg.alert("错误", response.responseText);
				    btn.enable();
			    }
		    })
	  }
  });
