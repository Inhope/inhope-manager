AIWordClassPanel = function(_cfg) {
	var self = this;
	var _pageSize = 20;

	this.isRepeat = false;

	this.getPageSize = _pageSize;
	var _store = new Ext.data.Store({
		  proxy : new Ext.data.HttpProxy({
			    url : 'ai-wordclass-manager!loadData.action'
		    }),
		  reader : new Ext.data.JsonReader({
			    idProperty : 'id',
			    root : 'data',
			    fields : [{
				      name : 'id',
				      type : 'string'
			      }, {
				      name : 'baseword',
				      type : 'string'
			      }, {
				      name : 'action',
				      type : 'string'
			      }, {
				      name : 'actiontime',
				      type : 'string'
			      }]
		    }),
		  writer : new Ext.data.JsonWriter()
	  });
	var wordText = new Ext.form.TextField({
		  fieldLabel : "基础词",
		  name : "word",
		  allowBlank : true,
		  anchor : '96%'
	  });

	var rowNumber;
	var ds = _store;
	var checkboxSelectionModel = new Ext.grid.CheckboxSelectionModel({
		  singleSelect : true
	  })
	var myExpander = new Ext.grid.RowExpander({
		  tpl : new Ext.Template('<p style="color:#999;margin:2px 0px 2px 48px;">{answer}</p>')
	  });

	var myColumnModel = new Ext.grid.ColumnModel([rowNumber = new Ext.grid.RowNumberer(), {
		  header : 'id',
		  dataIndex : 'id',
		  hidden : true,
		  sortable : false
	  }, {
		  header : '基础词',
		  dataIndex : 'baseword',
		  hidden : false,
		  sortable : false
	  }, {
		  header : '操作状态',
		  dataIndex : 'action',
		  sorttable : false
	  }, {
		  header : '操作时间',
		  dataIndex : 'actiontime',
		  sorttable : false
	  }]);

	myColumnModel.defaultSortable = true;
	this.wordText = wordText;

	var myAIWordGridColumnModel = new Ext.grid.ColumnModel([rowNumber = new Ext.grid.RowNumberer(), checkboxSelectionModel, {
		  header : '基础词',
		  dataIndex : 'baseword',
		  hidden : false,
		  sortable : false
	  }]);
	var myAIWordTbar = new Ext.Toolbar({
		  items : [{
			    text : '删除基础词',
			    iconCls : 'icon-wordclass-del',
			    handler : function() {
				    var records = checkboxSelectionModel.getSelections();
				    if (records.length == 0) {
					    Ext.Msg.alert('友情提示', '请选择要删除的词类!');
					    return;
				    } else {
					    Ext.Msg.confirm("友情提示", "您确定要删除所有选中的基础词吗?", function(clickType) {
						      if (clickType == "yes") {
							      var ids = "<remove>";
							      for (var i = 0; i < records.length; i++) {
								      ids += "<baseword>" + records[i].get("baseword") + "</baseword>";
							      }
							      ids += "</remove>";
							      self.deleteAIWord(ids);
						      }
					      });
				    }
			    }

		    }]
	  })
	var myAIWordStore = new Ext.data.Store({
		  proxy : new Ext.data.HttpProxy({
			    url : 'ai-wordclass-manager!query.action'
		    }),
		  reader : new Ext.data.JsonReader({
			    idProperty : 'id',
			    root : 'data',
			    fields : [{
				      name : 'baseword',
				      type : 'string'
			      }]
		    }),
		  writer : new Ext.data.JsonWriter()
	  });
	this.myAIWordStore = myAIWordStore;
	var gridPanel = new Ext.grid.GridPanel({
		  width : 360,
		  height : 280,
		  region : 'center',
		  border : false,
		  frame : false,
		  store : myAIWordStore,
		  loadMask : true,
		  colModel : myAIWordGridColumnModel,
		  tbar : myAIWordTbar,
		  sm : checkboxSelectionModel,
		  viewConfig : {
			  forceFit : true
		  }
	  })
	var AIWordWindow = new Ext.Window({
		  width : 380,
		  height : 300,
		  title : '基础词查询',
		  defaults : {
			  border : false
		  },
		  modal : true,
		  plain : true,
		  shim : true,
		  closable : true,
		  closeAction : 'hide',
		  collapsible : true,
		  resizable : false,
		  draggable : true,
		  animCollapse : true,
		  constrainHeader : true,
		  items : [gridPanel]
	  })
	this.AIWordWindow = AIWordWindow;
	var actionTbar = new Ext.Toolbar({
		  items : ['词:', wordText, {
			    text : '查询基础词',
			    iconCls : 'icon-wordclass-search',
			    handler : function() {
				    self.AIWordWindow.show();
				    var word = wordText.getValue();
				    self.queryXiaoIWord(word);
			    }
		    }, {
			    text : '查询记录',
			    iconCls : 'icon-search',
			    tooltip : '查询操作记录的词',
			    handler : function() {
				    self.isRepeat = false;
				    self.loadData();
			    }
		    }, {
			    text : '查询重复',
			    ref : 'searchBtn',
			    xtype : 'splitbutton',
			    tooltipType:"qtip",
			    tooltip : '查询和词类库重复的基础词',
			    iconCls : 'icon-grid',
			    handler : function() {
				    if (actionTbar.isCountRepeat.checked)
					    self.wordRepeatCount();
				    else
					    self.wordRepeatShow();
			    },
			    menu : {
				    items : [{
					      text : '重新统计',
					      checked : true,
					      ref : '../../isCountRepeat'
				      }]
			    }
		    }, '-', {
			    text : '删除记录',
			    iconCls : 'icon-delete',
			    handler : function() {
				    self.deleteRecord();
			    }
		    }, {
			    text : '添加基础词',
			    iconCls : 'icon-wordclass-add',
			    handler : function() {
				    var _formPanel = new Ext.form.FormPanel({
					      frame : false,
					      border : false,
					      labelWidth : 100,
					      // autoHeight : true,
					      height : 120,
					      waitMsgTarget : true,
					      bodyStyle : 'padding : 5px 10px',
					      defaults : {
						      blankText : '',
						      invalidText : '',
						      anchor : '92%',
						      xtype : 'textfield'
					      },
					      items : [new Ext.form.Hidden({
						          name : 'dictid'
					          }), {
						        fieldLabel : '基础词1',
						        name : 'word1',
						        allowBlank : false
					        }, {
						        fieldLabel : '基础词2',
						        name : 'word2',
						        allowBlank : true
					        }, {
						        fieldLabel : '基础词3',
						        name : 'word3',
						        allowBlank : true
					        }],
					      buttons : [{
						        text : '保存',
						        handler : function() {
							        if (!_formPanel.getForm().isValid()) {
								        return;
							        }
							        var word1 = _formPanel.getForm().items.get(1).getValue();
							        var word2 = _formPanel.getForm().items.get(2).getValue();
							        var word3 = _formPanel.getForm().items.get(3).getValue();
							        if (word2.length != 0) {
								        if (word1 == word2 || word2 == word3 || word1 == word3) {
									        Ext.Msg.alert('错误提示', '词类重复!');
									        return;
								        }
								        word1 += "," + word2;
							        }
							        if (word3.length != 0) {
								        word1 += "," + word3;
							        }
							        self.addWord(word1);
						        }
					        }, {
						        text : '关闭',
						        handler : function() {
							        _addWin.hide();
							        _formPanel.getForm().reset();
						        }
					        }]
				      });
				    var _addWin = new Ext.Window({
					      width : 380,
					      title : '基础词添加',
					      defaults : {
						      border : false
					      },
					      modal : true,
					      plain : true,
					      shim : true,
					      closable : true,
					      closeAction : 'hide',
					      collapsible : true,
					      resizable : false,
					      draggable : true,
					      animCollapse : true,
					      constrainHeader : true,
					      items : [_formPanel]
				      });
				    _addWin.show();
			    }
		    }, {
			    text : '批量导入',
			    iconCls : 'icon-ontology-import',
			    handler : function() {
				    self.importData();
			    }
		    }, {
			    text : '数据导出',
			    iconCls : 'icon-ontology-export',
			    handler : function() {
				    self.exportData();
			    }
		    }, {
			    text : '执行',
			    iconCls : 'icon-ontology-import',
			    handler : function() {
				    self.exec();
			    }
		    }, {
			    text : '恢复基础词',
			    iconCls : 'icon-ontology-export',
			    handler : function() {
				    self.restore();
			    }
		    }]
	  });

	var _fileForm = new Ext.FormPanel({
		  layout : "fit",
		  frame : true,
		  border : false,
		  autoHeight : true,
		  waitMsgTarget : true,
		  defaults : {
			  bodyStyle : 'padding:10px'
		  },
		  margins : '0 0 0 0',
		  labelAlign : "left",
		  labelWidth : 50,
		  fileUpload : true,
		  items : [{
			    xtype : 'fieldset',
			    title : '选择文件',
			    autoHeight : true,
			    items : [{
				      name : 'baseWordFile',
				      xtype : "textfield",
				      fieldLabel : '文件',
				      inputType : 'file',
				      allowBlank : false,
				      anchor : '96%'
			      }]
		    }]
	  });
	var _importPanel = new Ext.Panel({
		  border : false,
		  layout : "fit",
		  layoutConfig : {
			  animate : true
		  },
		  items : [_fileForm],
		  buttons : [{
			    text : "开始导入",
			    handler : function() {
				    var btn = this;
				    this.disable();
				    // 开始导入
				    var fileForm = _fileForm.getForm();
				    if (fileForm.isValid()) {
					    fileForm.submit({
						      url : 'ai-wordclass-manager!importData.action',
						      success : function(form, action) {
							      btn.enable();
							      var result = action.result.data;
							      var timer = new ProgressTimer({
								        initData : result,
								        progressId : 'importBaseWord',
								        boxConfig : {
									        title : ' 正在导入基础词内容...'
								        },
								        finish : function(p, response) {
									        Ext.MessageBox.hide();
									        Ext.Msg.alert('成功提示', '导入成功!');
									        self.wordText.setValue("");
									        self.isRepeat = false;
									        self.loadData();
								        }
							        });
							      timer.start();
						      },
						      failure : function(form) {
							      btn.enable();
							      Ext.MessageBox.hide();
							      Ext.Msg.alert('导入失败', '文件上传失败');
						      }
					      });
				    }
			    }
		    }, {
			    text : "关闭",
			    handler : function() {
				    importWindow.hide();
			    }
		    }]
	  });

	var importWindow = new Ext.Window({
		  border : false,
		  width : 350,
		  title : '基础词导入',
		  defaults : {// 表示该窗口中所有子元素的特性
			  border : false
			  // 表示所有子元素都不要边框
		  },
		  plain : true,// 方角 默认
		  modal : true,
		  plain : true,
		  shim : true,
		  closeAction : 'hide',
		  collapsible : true,// 折叠
		  closable : true, // 关闭
		  resizable : false,// 改变大小
		  draggable : true,// 拖动
		  minimizable : false,// 最小化
		  maximizable : false,// 最大化
		  animCollapse : true,
		  constrainHeader : true,
		  autoHeight : false,
		  items : [_importPanel]
	  });
	this.importWindow = importWindow;
	var pagingBar = new Ext.PagingToolbar({
		  store : _store,
		  displayInfo : true,
		  pageSize : _pageSize,
		  prependButtons : true,
		  beforePageText : '第',
		  afterPageText : '页，共{0}页',
		  displayMsg : '第{0}到{1}条记录，共{2}条',
		  emptyMsg : "没有记录",
		  prependButtons : false
	  })
	_store.on('beforeload', function() {
		  var word = self.wordText.getValue();
		  Ext.apply(this.baseParams, {
			    isRepeat : self.isRepeat,
			    word : word
		    });
		  scope : self
	  });
	var cfg = {
		region : 'center',
		border : false,
		frame : false,
		store : _store,
		loadMask : true,
		colModel : myColumnModel,
		tbar : actionTbar,
		sm : checkboxSelectionModel,
		bbar : pagingBar,

		viewConfig : {
			forceFit : true
		}
	}
	AIWordClassPanel.superclass.constructor.call(this, Ext.applyIf(_cfg || {}, cfg));

}
Ext.extend(AIWordClassPanel, Ext.grid.GridPanel, {
	  loadData : function() {
		  this.getStore().load({
			    params : {
				    start : 0,
				    limit : this.getPageSize
			    }
		    });
	  },
	  deleteAIWord : function(ids) {
		  Ext.Ajax.request({
			    url : 'ai-wordclass-manager!delete.action',
			    success : function(response) {
				    var result = Ext.util.JSON.decode(response.responseText).message;
				    if (result == null) {
					    Ext.Msg.alert('提示信息', '删除成功!');
					    this.myAIWordStore.reload();
					    this.loadData();
				    } else {
					    Ext.Msg.alert('失败', '删除成功!');
				    }
			    },
			    failure : function(response) {
				    Ext.Msg.alert("提示", "出错或者超时!");
			    },
			    xmlData : ids,
			    scope : this
		    });
	  },
	  queryXiaoIWord : function(word) {
		  this.myAIWordStore.load({
			    params : {
				    'word' : word
			    }
		    });
	  },
	  addWord : function(words) {
		  Ext.Ajax.request({
			    url : 'ai-wordclass-manager!addWrod.action',
			    params : {
				    'words' : words
			    },
			    success : function(response) {
				    var result = Ext.util.JSON.decode(response.responseText).message;
				    if (result == null) {
					    Ext.Msg.alert('提示消息', '新增成功');
					    this.wordText.setValue("");
					    this.isRepeat = false;
					    this.loadData();
				    } else {
					    Ext.Msg.alert('失败', result);
				    }
			    },
			    failure : function(response) {
				    Ext.Msg.alert("提示", "出错或者超时!");
			    },
			    scope : this
		    });
	  },
	  exec : function() {
		  Ext.Msg.confirm('提示', '确定执行?', function(btn) {
			    if (btn == 'yes') {
				    Ext.Ajax.request({
					      url : 'ai-wordclass-manager!exec.action',
					      success : function(response) {
						      Ext.Msg.alert('提示', '执行成功');
					      },
					      failure : function(response) {
						      Ext.Msg.alert("提示", "出错或者超时!");
					      },
					      scope : this
				      });
			    }
		    });
	  },
	  restore : function() {
		  var self = this;
		  Ext.Msg.confirm('提示', '确定恢复基础词?', function(btn) {
			    if (btn == 'yes') {
				    self.getEl().mask('恢复中，请稍等...');
				    Ext.Ajax.request({
					      url : 'ai-wordclass-manager!restore.action',
					      success : function(response) {
						      var result = Ext.util.JSON.decode(response.responseText);
						      self.getEl().unmask();
						      if (result.success) {
							      Ext.Msg.alert('提示', '恢复成功');
						      } else {
							      Ext.Msg.alert("提示", "恢复基础词失败!");
						      }
					      },
					      failure : function(response) {
						      self.getEl().unmask();
						      Ext.Msg.alert("提示", "恢复基础词失败!");
					      },
					      scope : this
				      });
			    }
		    });
	  },
	  importData : function() {
		  this.importWindow.show();
	  },
	  exportData : function() {
		  // 开始下载
		  Ext.Ajax.request({
			    url : 'ai-wordclass-manager!exportData.action',
			    success : function(response) {
				    var self = this;
				    var result = Ext.util.JSON.decode(response.responseText);
				    var timer = new ProgressTimer({
					      initData : result.data,
					      progressId : 'exportBaseWordStatus',
					      boxConfig : {
						      title : '正在导出词类...'
					      },
					      finish : function() {
						      if (!self.downloadIFrame) {
							      self.downloadIFrame = self.getEl().createChild({
								        tag : 'iframe',
								        style : 'display:none;'
							        })
						      }
						      self.downloadIFrame.dom.src = 'ai-wordclass-manager!downExportFile.action?_t=' + new Date().getTime();
						      Ext.MessageBox.hide();
					      }
				      });
				    timer.start();
			    },
			    failure : function(response) {
				    Ext.MessageBox.hide();
				    Ext.Msg.alert('错误提示', response.responseText);
			    },
			    scope : this
		    });
	  },
	  deleteRecord : function() {
		  var rows = this.getSelectionModel().getSelections();
		  if (rows.length == 0) {
			  Ext.Msg.alert('提示', '请先选择要删除的记录！');
			  return false;
		  }
		  var self = this;
		  Ext.Msg.confirm('确定框', '确定要删除这' + rows.length + '记录吗？', function(sel) {
			    if (sel == 'yes') {
				    Ext.Ajax.request({
					      url : 'ai-wordclass-manager!deleteRecord.action',
					      params : {
						      id : rows[0].data.id,
						      name : rows[0].data.baseword
					      },
					      success : function(response) {
						      var result = Ext.util.JSON.decode(response.responseText);
						      if (result.success) {
							      self.loadData();
							      Ext.Msg.alert('提示', '删除成功');
						      } else {
							      Ext.Msg.alert("提示", result.message);
						      }
					      },
					      failure : function(response) {
						      self.getEl().unmask();
						      Ext.Msg.alert("提示", "恢复基础词失败!");
					      },
					      scope : this
				      });
			    }
		    });
	  },
	  wordRepeatCount : function() {
		  var self = this;
		  Ext.Ajax.request({
			    url : 'ai-wordclass-manager!repeatCount.action',
			    success : function(response) {
				    var result = Ext.util.JSON.decode(response.responseText);
				    var timer = new ProgressTimer({
					      initData : result.data,
					      progressId : 'aiWordclasRepeatStatus',
					      boxConfig : {
						      title : '正在统计词库存在的基础词...'
					      },
					      finish : function() {
						      self.isRepeat = true
						      self.loadData();
					      }
				      });
				    timer.start();
			    },
			    failure : function(response) {
				    Ext.MessageBox.hide();
				    Ext.Msg.alert('错误提示', response.responseText);
			    },
			    scope : this
		    });
	  },
	  wordRepeatShow : function() {
		  this.isRepeat = true
		  this.loadData();
	  }
  })