AbstractSemanticTaskAssignmentPanel = function(cfg) {
	var me = this;
	var uri = this.uri = cfg._id;

	var store = this.store = new Ext.data.GroupingStore({
				url : 'abstract-semantic-task!lists.action',
				reader : new Ext.data.JsonReader({
							root : 'data',
							fields : [{
										name : 'taskId',
										type : 'string'
									}, {
										name : 'uri',
										type : 'string'
									},{
										name : 'parentId',
										type : 'string'
									}, {
										name : 'pname',
										type : 'string'
									}, {
										name : 'bh',
										type : 'string'
									}, {
										name : 'name',
										type : 'string'
									}, {
										name : 'createTime',
										type : 'date'
									}, {
										name : 'status',
										type : 'int'
									}, {
										name : 'totalNum',
										type : 'int'
									}, {
										name : 'expectNum',
										type : 'int'
									}, {
										name : 'factNum',
										type : 'int'
									}, {
										name : 'notNullNum',
										type : 'int'
									}, {
										name : 'eqNum',
										type : 'int'
									}, {
										name : 'score',
										type : 'int'
									}, {
										name : 'fixNum',
										type : 'int'
									}, {
										name : 'username',
										type : 'string'
									}, {
										name : 'remark',
										type : 'string'
									}],
							autoLoad : false
						}),
				groupField : 'pname'
			});
			
	this.tbar = ['->', 
//			'-', {
//				text : '搜索',
//				iconCls : 'icon-search',
//				handler : function(btn) {
//					var nav = Dashboard.navPanel.items.itemAt(0);
//			 		var p = nav.showTab(AbstractSemanticTaskDetailSearchPanel, Ext.id(), '高级搜索', 'icon-ontology-class', true);
//					p.loadData(id); 
//				}
//			},
			'-', {
				text : '一键推荐',
				iconCls : 'icon-refresh',
				menu : {
					items : [{
								itemId : 'syn_all',
								text : '推荐全部',
								handler : function(menu) {
									me.recommendSelection(menu, 0);
								}
							}, {
								itemId : 'syn_exsit',
								text : '推荐已标注',
								handler : function(menu) {
									me.recommendSelection(menu, 1);
								}
							}]
				}
			}];
			
 	showTaskTab = function(id, text, uri) {
 		var nav = Dashboard.navPanel.items.itemAt(0);
 		var p = nav.showTab(AbstractSemanticTaskDetailPanel, id, text, 'icon-wordclass-root', true);
		p.loadData(uri);
 	};
 	
 	var checkboxSelectionModel = new Ext.grid.CheckboxSelectionModel({})
	var columns = this.columns = [checkboxSelectionModel, {
				header : "pname",
				sortable : true,
				dataIndex : 'pname'
			}, {
				header : "任务",
				sortable : true,
				dataIndex : 'name',
				renderer : function(value, meta, rec) {
					return '<a href="javascript:void(0);" onclick="showTaskTab(\''+rec.data.taskId+'\',\''+rec.data.name+'\',\''+rec.data.uri+'\')">'+value+'</a>'
				},
				summaryRenderer : function(value, summaryData, dataIndex) {
					return "合计";
				}
			},{
				header : '分配用户',
				dataIndex : 'username'
			}, {
				header : '<center>语料总数</center>',
				align : 'right',
				dataIndex : 'totalNum',
				width : 55,
				summaryType : 'sum',
				summaryRenderer : function(value, summaryData, meta) {
					return "<font color='red'>" + Ext.util.Format.number(value, '00') + "</font>";
				}
			}, {
				header : '<center>期待定位数</center>',
				align : 'right',
				dataIndex : 'expectNum',
				width : 50,
				summaryType : 'sum',
				summaryRenderer : function(value, summaryData, meta) {
					return "<font color='red'>" + Ext.util.Format.number(value, '00') + "</font>";
				}
			}, {
				header : '<center>实际定位数</center>',
				align : 'right',
				dataIndex : 'factNum',
				width : 55,
				summaryType : 'sum',
				summaryRenderer : function(value, summaryData, meta) {
					return "<font color='red'>" + Ext.util.Format.number(value, '00') + "</font>";
				}
			}, {
				header : '<center>notnull</center>',
				align : 'right',
				dataIndex : 'notNullNum',
				hidden : true,
				width : 50,
				summaryType : 'sum',
				summaryRenderer : function(value, summaryData, meta) {
					return "<font color='red'>" + Ext.util.Format.number(value, '00') + "</font>";
				}
			}, {
				header : '<center>eqNum</center>',
				align : 'right',
				dataIndex : 'eqNum',
				hidden : true,
				width : 50,
				summaryType : 'sum',
				summaryRenderer : function(value, summaryData, meta) {
					return "<font color='red'>" + Ext.util.Format.number(value, '00') + "</font>";
				}
			}, {
				header : '<center>fixNum</center>',
				align : 'right',
				dataIndex : 'fixNum',
				hidden : true,
				width : 50,
				summaryType : 'sum',
				summaryRenderer : function(value, summaryData, meta) {
					return "<font color='red'>" + Ext.util.Format.number(value, '00') + "</font>";
				}
			}, {
				header : '<center>召回率</center>',
				align : 'right',
				renderer : function(value, meta, rec) {
					if (rec.data.expectNum == null || rec.data.expectNum == 0) {
						return "0%";
					} else {
						return Ext.util.Format.number(rec.data.notNullNum * 100 / rec.data.expectNum, '00.0') + "%";
					}
				},
				width : 48,
				summaryRenderer : function(value, summaryData, meta) {
					if (meta.data.notNullNum == null || meta.data.notNullNum == 0) {
						return "<font color='red'>" + "0%" + "</font>";;
					} else {
						return "<font color='red'>" + Ext.util.Format.number(meta.data.notNullNum * 100 / meta.data.expectNum, '00.0') + "%" + "</font>";
					}
				}
			}, {
				header : '<center>准确率</center>',
				align : 'right',
				renderer : function(value, meta, rec) {
					var accuracyRate = Ext.util.Format.number(rec.data.fixNum * 100 / rec.data.notNullNum, '00.0')
					var completeRate = Ext.util.Format.number(rec.data.eqNum * 100 / rec.data.notNullNum, '00.0');
					if (accuracyRate == '') {
						accuracyRate = 0;
					}
					if (completeRate == '') {
						completeRate = 0;
					}
					return accuracyRate + "%-" + completeRate + "%";
				},
				width : 55,
				summaryRenderer : function(value, summaryData, meta) {
					var accuracyRate = Ext.util.Format.number(meta.data.fixNum * 100 / meta.data.notNullNum, '00.0')
					var completeRate = Ext.util.Format.number(meta.data.eqNum * 100 / meta.data.notNullNum, '00.0');
					if (accuracyRate == '') {
						accuracyRate = 0;
					}
					if (completeRate == '') {
						completeRate = 0;
					}
					return "<font color='red'>" + accuracyRate + "%-" + completeRate + "%" + "</font>";

				}
			}, {
				align : 'right',
				header : '<center>综合评价指标</center>',
				renderer : function(value, meta, rec) {
					if (rec.data.expectNum == 0 || rec.data.notNullNum == 0) {
						return "0%-0%";
					} else {
						// 定位准确率
						var curPercent = rec.data.eqNum / rec.data.notNullNum;
						// 完全准确率
						var comPercent = rec.data.fixNum / rec.data.notNullNum;
						// 召回率
						var recallPercent = rec.data.notNullNum / rec.data.expectNum;

						return Ext.util.Format.number(comPercent * recallPercent * 200 / (comPercent + recallPercent), '00.0') + "%-" + Ext.util.Format.number(curPercent * recallPercent * 200 / (curPercent + recallPercent), '00.0') + "%";
					}
				},
				width : 55,
				summaryRenderer : function(value, summaryData, meta) {
					if (meta.data.expectNum == 0 || meta.data.notNullNum == 0) {
						return "<font color='red'>" + "0%-0%";
						+"</font>";
					} else {
						var curPercent = meta.data.eqNum / meta.data.notNullNum;
						var comPercent = meta.data.fixNum / meta.data.notNullNum;
						var recallPercent = meta.data.notNullNum / meta.data.expectNum;
						return "<font color='red'>" + Ext.util.Format.number(comPercent * recallPercent * 200 / (comPercent + recallPercent), '00.0') + "%-" + Ext.util.Format.number(curPercent * recallPercent * 200 / (curPercent + recallPercent), '00.0') + "%" + "</font>";
					}
				}
			}];

	var config = {
		store : store,
		sm : checkboxSelectionModel,
		loadMask : true,
		columnLines : true,
		stripeRows : true,
		margins : '0 5 5 5',
		border : false,
		columns : columns,
		plugins : new Ext.ux.grid.GroupSummary(),
		view : new Ext.grid.GroupingView({
					forceFit : true,
					showGroupName: false,
		            enableNoGroups: false,
					enableGroupingMenu: false,
		            hideGroupedColumn: true,
					groupTextTpl : '{text} (<font color="red">{[values.rs.length]} {[values.rs.length > 1 ? "个任务" : "个任务"]}</font>)'
				})
	};

	AbstractSemanticTaskAssignmentPanel.superclass.constructor.call(this, config);
};

Ext.extend(AbstractSemanticTaskAssignmentPanel, Ext.grid.EditorGridPanel, {
			loadData : function(taskId) {
				this.store.load({
							params : {
								taskId : taskId
							}
						})
			},
			
			recommendSelection : function(menu, num) {
				var me = this, params = '';
				var grid = menu.ownerCt.ownerCt.ownerCt.ownerCt;
				var records = grid.getSelectionModel().getSelections();
				if (records.length == 0) {
					Dashboard.setAlert("请选择数据后再推荐!");
					return;
				}
				var params = [];

				for (var i = 0; i < records.length; i++) {
					var p = {
						taskId : records[i].get('taskId'),
						id : records[i].get('uri')
					}
					params.push(p);
				}
				Ext.Ajax.request({
					url : '../../g/kbmgr/abstract-semantic-task!recommends.action',
					params : {
						'mode' : num,
						'node' : Ext.util.JSON.encode(params)
					},
					success : function(response) {
						if (response.responseText) {
							var timer = new ProgressTimer({
								initData : Ext
										.decode(response.responseText),
								progressText : 'percent',
								progressId : 'recommend',
								boxConfig : {
									msg : '一键推荐中',
									title : '正在推荐...'
								},
								finish : function(p, response) {
									me.store.reload();
									Dashboard
											.setAlert('全部推荐成功！');
								},
								scope : this
							});
							timer.start();
						}
					},
					failure : function(response) {
						Dashboard.setAlert(response.responseText);
					}
				});
			}

		})