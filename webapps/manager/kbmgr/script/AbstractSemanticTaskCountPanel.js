AbstractSemanticTaskCountPanel = function(_cfg) {
	

	var cfg = {
		id : 'count_p',
		border : false,
		html : "<iframe id='"
				+ _cfg._id
				+ "' src= './count.jsp' height='100%' width='100%' frameborder='no' border='0' marginwidth='0' marginheight='0' scrolling='auto' allowtransparency='yes'></iframe>"

	}

	AbstractSemanticTaskCountPanel.superclass.constructor.call(this, cfg);
};

Ext.extend(AbstractSemanticTaskCountPanel, Ext.Panel, {

})
