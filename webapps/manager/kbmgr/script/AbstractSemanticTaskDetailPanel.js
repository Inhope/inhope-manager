AbstractSemanticTaskDetailPanel = function(cfg) {
	var me = this;
	var taskId = this.taskId = cfg._id;
	this.uri = null;

	var store = this.store = new Ext.data.JsonStore({
				remoteSort : false,
				idProperty : 'id',
				totalProperty : 'totalCount',
				url : 'abstract-semantic-task-detail!page.action',
				root : 'result',
				fields : [{
							name : 'id',
							type : 'string'
						}, {
							name : 'userQuestion',
							type : 'string'
						}, {
							name : 'expectSemanticAbs',
							type : 'string'
						}, {
							name : 'expectSemanticMiss',
							type : 'string'
						}, {
							name : 'expectRule',
							type : 'string'
						}, {
							name : 'expectSemanticFact',
							type : 'string'
						}, {
							name : 'expectSemanticFactMiss',
							type : 'string'
						}, {
							name : 'expectSemanticRule',
							type : 'string'
						}, {
							name : 'taskId',
							type : 'string'
						}, {
							name : 'standardAnswer',
							type : 'string'
						}, {
							name : 'remark',
							type : 'string'
						}],
				autoLoad : false,
				listeners : {
					beforeload : function(store, records, options) {
						store.baseParams = {
							mode : me.combobox.getValue(),
							uri : me.uri,
							taskId : me.taskId
						};
						me.countNumber(me.taskId);
					}
					
				}
			});

	var pagingBar = this.bbar = new Ext.PagingToolbar({
				store : this.store,
				displayInfo : true,
				pageSize : 20,
				prependButtons : true,
				beforePageText : '第',
				afterPageText : '页，共{0}页',
				displayMsg : '总计问题{2}',
				emptyMsg : "没有记录"
			});

	pagingBar.add('-', {
				id : 'bbar' + this.taskId,
				xtype : 'label',
				ref : 'statLabel',
				style : 'color: red;',
				text : '期待定位抽象语义【0/0/0】条实际定位【0】条抽象语义定位准确率【0%】完全定位准确率:【0%】'
			});

	var columns = this.columns = [new Ext.grid.RowNumberer(), {
				editor : {},
				dataIndex : 'userQuestion',
				header : '用户问题'
			}, {
				editor : {},
				dataIndex : 'expectSemanticAbs',
				flex : 1,
				header : '期待抽象语义'
			}, {
				editor : {},
				dataIndex : 'expectSemanticMiss',
				flex : 1,
				header : '期待缺失语义'
			}, {
				hidden : true,
				editor : {},
				dataIndex : 'expectRule',
				flex : 1,
				header : '期待规则'
			}, {
				dataIndex : 'expectSemanticFact',
				flex : 1,
				header : '实际定位抽象语义',
				renderer : function(v, m, rec) {
					var s = '';
					if (rec.data.expectSemanticFactMiss != '') {
						if (rec.data.expectSemanticFactMiss.indexOf("XXX") > 0) {
							m.tdAttr = 'style=padding:0;height:100%;background-color:#FFB5B5';
							s += '<div style="height:100%;position:relative;font-weight:bold;color:#000093;">' + v + '<div>'
						} else {
							m.tdAttr = 'style=padding:0;height:100%;background-color:#FFDC35';
							s += '<div style="height:100%;position:relative;font-weight:bold;color:#000093;">' + v + '<div>'
						}
						return s;
					} else {
						return v;
					}
				}
			}, {
				dataIndex : 'expectSemanticFactMiss',
				flex : 4,
				header : '实际缺失语义'
			}, {
				dataIndex : 'expectSemanticRule',
				flex : 2,
				header : '定位抽象语义规则'
			}, {
				hidden : true,
				dataIndex : 'standardAnswer',
				flex : 1,
				header : '标准答案'
			}, {
				hidden : true,
				dataIndex : 'remark',
				header : '备注'
			}]
			
	this.combobox = new Ext.form.ComboBox({
				typeAhead : true,
				editable : false,
				triggerAction : 'all',
				value : '0',
				store : [['0', '显示全部'], ['1', '隐藏抽象语义定位'], ['2', '隐藏完全定位'], ['3', '显示错误标注']],
				lazyRender : true

			})

	this.tbar = ['->', this.combobox, {
				text : '搜索:',
				iconCls : 'icon-search',
				handler : function(btn) {
					me.doFilter(btn, me.taskId);
				}
			}, '-', {
				text : '保存',
				iconCls : 'icon-add',
				handler : function() {
					me.doMerge(this);
				}
			}, '-', {
				text : '重新推荐',
				iconCls : 'icon-refresh',
				menu : {
					items : [{
								itemId : 'syn_all',
								text : '推荐全部',
								iconCls : 'icon-refresh',
								handler : function(menu) {
									me.doRecommend(menu, 0, me.taskId);
								}
							}, {
								itemId : 'syn_exsit',
								text : '推荐已标注',
								iconCls : 'icon-refresh',
								handler : function(menu) {
									me.doRecommend(menu, 1, me.taskId);
								}
							}]
				}
			}, '-', {
				text : '导入',
				iconCls : 'icon-refresh',
				handler : function() {
					me.createWindow(this);
				}
			}, '-', {
				text : '导出',
				iconCls : 'icon-refresh',
				handler : function() {
					me.doExport(this)
				}
			}];

	var _form = this.form = new Ext.form.FormPanel({
				fileUpload : true,
				frame : false,
				border : false,
				enctype : 'multipart/form-data',
				waitMsgTarget : true,
				bodyStyle : 'padding : 10px 20px; background-color:' + sys_bgcolor,
				defaults : {
					labelSeparator : " ： ",
					allowBlank : false,
					msgTarget : 'side',
					labelAlign : 'right',
					anchor : '100%'
				},
				items : [{
							xtype : 'fileuploadfield',
							emptyText : '选择文件',
							fieldLabel : '文件路径',
							name : 'file',
							buttonText : '',
							buttonCfg : {
								iconCls : 'icon-upload-excel'
							},
							validator : function(v) {
								v = v.trim();
								if (v) {
									if (v.indexOf('.xls') < 0)
										return '文件格式不正确';
									return true;
								}
								return '请选择文件';
							}
						}],
				buttons : [{
							text : '确认',
							handler : function() {
								me.doUpload(this);
							}
						}]
			});

	this.win = new Ext.Window({
				closable : true,
				closeAction : 'hide',
				title : '上传文件',
				width : 500,
				minWidth : 350,
				modal : true,
				items : _form
			});

	var config = {
		store : this.store,
		sm : new Ext.grid.RowSelectionModel(),
		loadMask : true,
		columnLines : true,
		stripeRows : true,
		margins : '0 5 5 5',
		border : false,
		columns : this.columns,
		viewConfig : {
			enableTextSelection : true,
			forceFit : true,
			getRowClass : function(record, rowIndex, rowParams, store) {
				if (record.data.expectSemanticAbs == record.data.expectSemanticFact) {
					if (me.changeColor(record.data.expectSemanticMiss, record.data.expectSemanticFactMiss)) {
						return 'x-grid-record-green'
					} else {
						return 'x-grid-record-yellow';
					}
				} else {
					return 'x-grid-record-common';
				}
			}
		}
	};

	AbstractSemanticTaskDetailPanel.superclass.constructor.call(this, config);
};

Ext.extend(AbstractSemanticTaskDetailPanel, Ext.grid.EditorGridPanel, {
			loadData : function(uri) {
				this.uri = uri;
				this.store.load({
							params : {
								uri : uri,
								mode : 0,
								taskId : this.taskId
							}
						})
				this.countNumber(this.taskId);
			},
			doMerge : function(btn) {
				var me = this;
				var store = me.store, parmas = [];
				var isDirty = true;
				store.each(function(record) {
							if (record.dirty) {
								parmas.push(record.data);
								isDirty = false;
							}
						});
				if (isDirty) {
					Dashboard.setAlert("请修改数据后再保存!");
					return;
				}
				Ext.Ajax.request({
					url : 'abstract-semantic-task-detail!merge.action',
					params : {
						uri : me.uri,
						'params' : Ext.util.JSON.encode(parmas)
					},
					success : function(response) {
						var json = Ext.util.JSON.decode(response.responseText);
						if (json.success) {
							Dashboard.setAlert("保存成功!");
						} else {
							Dashboard.setAlert("保存失败!");
						}
						store.reload();
					},
					failure : function(response) {
						Dashboard.setAlert(response.responseText);
					}
				});
			},
			createWindow : function(btn) {
				this.win.show();
			},
			doUpload : function(btn) {
				var upform = btn.ownerCt.ownerCt.getForm(), me = this;
				if (upform.isValid()) {
					upform.submit({
								params : {
									uri : me.uri,
									taskId : me.taskId
								},
								waitMsg : '正在上传',
								url : 'abstract-semantic-task-detail!importDetail.action',
								success : function(form, act) {
									me.win.hide();
									var timer = new ProgressTimer({
												initData : act.result.data,
												progressText : 'percent',
												progressId : 'importDetail',
												boxConfig : {
													msg : '导入中',
													title : '导入语料'
												},
												finish : function(p, response) {
													me.getStore().reload();
												},
												scope : this
											});
									timer.start();
								},
								failure : function(form, action) {
									var data = Ext.util.JSON.decode(action.response.responseText);
									Dashboard.setAlert(data.message);
								}
							});
				}
			},
			doExport : function(btn) {
				Ext.Ajax.request({
							url : 'abstract-semantic-task-detail!export.action',
							params : {
								uri : this.uri,
								taskId : this.taskId
							},
							success : function(response) {
									var timer = new ProgressTimer({
											initData : Ext.decode(response.responseText),
											progressText : 'percent',
											progressId : 'exportSemanticDetail',
											boxConfig : {
												title : '正在准备文件'
											},
											finish : function() {
												if (!this.downloadIFrame) {
													this.downloadIFrame = this.getEl().createChild({
																tag : 'iframe',
																style : 'display:none;'
															});
												}
												this.downloadIFrame.dom.src = 'abstract-semantic-task-detail!exportDetail.action?&_t' + new Date().getTime();
											},
											scope : this
										});
								timer.start();
							},
							failure : function(response) {
								Ext.Msg.alert('请求异常', response.responseText);
							},
							scope : this
						});
			},
			changeColor : function(str1, str2) {
				String.prototype.replaceAll = function(str1, str2) {
					var str = this;
					var result = str.replace(eval("/" + str1 + "/gi"), str2);
					return result;
				}
				if (str1 == null || str1 == '') {
					return false;
				}
				var str3 = str2.replaceAll("&", "").replace(/<\s*\/?br>/g, '').replaceAll(",", "");
				if (str3.indexOf(str1.replaceAll(",", "")) != -1) {
					return true;
				} else {
					return false;
				}
			},
			doRecommend : function(menu, num, taskId) {
				var store = menu.ownerCt.ownerCt.ownerCt.ownerCt.getStore();
				var parmas = [];
				store.each(function(record) {
							if (record.dirty) {
								parmas.push(record.data);
							}
						});
				Ext.Ajax.request({
							url : 'abstract-semantic-task-detail!recommend.action',
							params : {
								uri : this.uri,
								'mode' : num,
								'params' : Ext.util.JSON.encode(parmas),
								'taskId' : taskId
							},
							success : function(response) {
								if (response.responseText) {
									var timer = new ProgressTimer({
												initData : Ext.decode(response.responseText),
												progressText : 'percent',
												progressId : 'recommend',
												boxConfig : {
													msg : '重新推荐中...',
													title : '重新推荐语义'
												},
												finish : function(p, response) {
													store.reload();
												},
												scope : this
											});
									timer.start();
								};
							},
							failure : function(response) {
								Ext.Msg.alert("错误", response.responseText);
							}
						});
			},
			doFilter : function(btn, taskId) {
				this.store.load({
							params : {
								uri : this.uri,
								mode : this.combobox.getValue(),
								taskId : taskId
							}
						});
			},
			countNumber : function(taskId) {
				Ext.Ajax.request({
							url : 'abstract-semantic-task!countMap.action',
							params : {
								url : this.uri,
								'taskId' : taskId
							},
							success : function(resp, opts) {
								var respText = Ext.util.JSON.decode(resp.responseText);
								Ext.getCmp('bbar' + taskId).setText(respText.countStr);
							},
							failure : function(resp, opts) {
								Dashboard.setAlert("ajax请求出现异常!");
							}
						})
			}

		})