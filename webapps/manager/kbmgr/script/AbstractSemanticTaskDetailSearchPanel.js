AbstractSemanticTaskDetailSearchPanel = function(cfg) {
	var me = this;
	var taskId = this.taskId = cfg._id;
	this.uri = '';
	
	var store = this.store = new Ext.data.JsonStore({
				remoteSort : false,
				idProperty : 'id',
				totalProperty : 'totalCount',
				url : '../../g/kbmgr/abstract-semantic-task-detail!search.action',
				root : 'result',
				fields : [{
							name : 'id',
							type : 'string'
						}, {
							name : 'userQuestion',
							type : 'string'
						}, {
							name : 'expectSemanticAbs',
							type : 'string'
						}, {
							name : 'expectSemanticMiss',
							type : 'string'
						}, {
							name : 'expectRule',
							type : 'string'
						}, {
							name : 'expectSemanticFact',
							type : 'string'
						}, {
							name : 'expectSemanticFactMiss',
							type : 'string'
						}, {
							name : 'expectSemanticRule',
							type : 'string'
						}, {
							name : 'taskId',
							type : 'string'
						}, {
							name : 'standardAnswer',
							type : 'string'
						}, {
							name : 'remark',
							type : 'string'
						}],
				autoLoad : false,
				listeners : {
					"beforeload" : function() {
						var condition1 = Ext.getCmp('all_search_condition1').getValue();
						var condition2 = Ext.getCmp('all_search_condition2').getValue();
						var value1 = Ext.getCmp('all_search_value1').getValue();
						var value2 = Ext.getCmp('all_search_value2').getValue();
						Ext.apply(store.baseParams, {
									uri : me.uri,
									value1 : value1,
									value2 : value2,
									condition1 : condition1,
									condition2 : condition2
								});
					}
				}
			});

	var pagingBar = this.bbar = new Ext.PagingToolbar({
				store : this.store,
				displayInfo : true,
				pageSize : 20,
				prependButtons : true,
				beforePageText : '第',
				afterPageText : '页，共{0}页',
				displayMsg : '总计问题{2}个',
				emptyMsg : "没有记录"
			});

	var columns = this.columns = [new Ext.grid.RowNumberer(), {
				editor : {},
				dataIndex : 'userQuestion',
				header : '用户问题'
			}, {
				editor : {},
				dataIndex : 'expectSemanticAbs',
				flex : 1,
				header : '期待抽象语义'
			}, {
				editor : {},
				dataIndex : 'expectSemanticMiss',
				flex : 1,
				header : '期待缺失语义'
			}, {
				editor : {},
				dataIndex : 'expectRule',
				flex : 1,
				header : '期待规则'
			}, {
				dataIndex : 'expectSemanticFact',
				flex : 1,
				header : '实际定位抽象语义',
				renderer : function(v, m, rec) {
					var s = '';
					if (rec.data.expectSemanticFactMiss != '') {
						if (rec.data.expectSemanticFactMiss.indexOf("XXX") > 0) {
							m.tdAttr = 'style=padding:0;height:100%;background-color:#FFB5B5';
							s += '<div style="height:100%;position:relative;font-weight:bold;color:#000093;">' + v + '<div>'
						} else {
							m.tdAttr = 'style=padding:0;height:100%;background-color:#FFDC35';
							s += '<div style="height:100%;position:relative;font-weight:bold;color:#000093;">' + v + '<div>'
						}
						return s;
					} else {
						return v;
					}
				}
			}, {
				dataIndex : 'expectSemanticFactMiss',
				flex : 4,
				header : '实际缺失语义'
			}, {
				dataIndex : 'expectSemanticRule',
				flex : 2,
				header : '定位抽象语义规则'
			}, {
				dataIndex : 'standardAnswer',
				flex : 1,
				header : '标准答案'
			}, {
				hidden : true,
				dataIndex : 'remark',
				header : '备注'
			}];

	this.domainStore = new Ext.data.JsonStore({
		fields : [ {
			name : 'id',
			type : 'string'
		}, {
			name : 'name',
			type : 'string'
		} ],
		autoLoad : false,
		url : '../../g/kbmgr/abstract-semantic-task-detail!comboAllowDomain.action'
	});
	
	
	var _treeCombo = new TreeComboBox({
		emptyText : '请选择领域库',
		editable : false,
		minHeight : 250,
		root : {
			id : '-1',
			text : 'root',
			iconCls : 'icon-ontology-root'
		},
		loader : new Ext.tree.TreeLoader({
			dataUrl : '../../g/kbmgr/abstract-semantic-task-detail!queryTreeDomain.action'
		}),
		listeners : {
			select : function(combobox1) {
				me.uri = combobox1.getValue();
			},
			change : function(combobox1) {
				me.uri = combobox1.getValue();
			}
		}
	});
	this.treeCombo = _treeCombo;

	this.tbar = [
			me.treeCombo,
			'-',
			{
				id : 'all_search_condition1',
				name : 'mode',
				xtype : 'combo',
				typeAhead : true,
				emptyText : '请选择条件',
				editable : false,
				triggerAction : 'all',
				selectOnTab : true,
				width : 120,
				value : 'userQuestion',
				store : [ [ 'userQuestion', '用户问题' ],
						[ 'expectSemanticAbs', '期待抽象语义' ],
						[ 'expectSemanticMiss', '期待缺失语义' ],
						[ 'expectRule', '期待规则 ' ],
						[ 'expectSemanticFact', '实际定位抽象语义' ],
						[ 'expectSemanticRule', '定位抽象语义规则' ],
						[ 'expectSemanticFactMiss', '实际缺失语义' ],
						[ 'standardAnswer', '标准答案' ], [ 'remark', '备注' ] ],
				listeners : {
					change : function(combobox1) {
						var value1 = combobox1.getValue();
						var combobox2 = Ext.getCmp('all_search_condition2');
						combobox2Store = combobox2.getStore();
						combobox2Store.clearFilter();
						combobox2Store.filterBy(function(data) {
							return data.get('field1') != value1;
						})
					}
				},
				lazyRender : true
			},
			'-',
			{
				id : 'all_search_value1',
				msgTarget : 'side',
				labelWidth : 70,
				anchor : '100%',
				emptyText : '支持回车Enter',
				xtype : 'textfield',
				name : 'value1',
				listeners : {
					specialkey : function(field, e) {
						if (e.getKey() == Ext.EventObject.ENTER) {
							me.loadData();
						}
					}
				}
			},
			'-',
			{
				id : 'all_search_condition2',
				name : 'mode',
				xtype : 'combo',
				typeAhead : true,
				emptyText : '请选择条件',
				editable : false,
				triggerAction : 'all',
				width : 120,
				selectOnTab : true,
				value : 'remark',
				store : [ [ 'userQuestion', '用户问题' ],
						[ 'expectSemanticAbs', '期待抽象语义' ],
						[ 'expectSemanticMiss', '期待缺失语义' ],
						[ 'expectRule', '期待规则 ' ],
						[ 'expectSemanticFact', '实际定位抽象语义' ],
						[ 'expectSemanticRule', '定位抽象语义规则' ],
						[ 'expectSemanticFactMiss', '实际缺失语义' ],
						[ 'standardAnswer', '标准答案' ], [ 'remark', '备注' ] ],
				listeners : {
					change : function(combobox2) {
						var value2 = combobox2.getValue();
						var combobox1 = Ext.getCmp('all_search_condition1');
						combobox1Store = combobox1.getStore();
						combobox1Store.clearFilter();
						combobox1Store.filterBy(function(data) {
							return data.get('field1') != value2;
						})
					}
				},
				lazyRender : true
			}, '-', {
				id : 'all_search_value2',
				msgTarget : 'side',
				labelWidth : 70,
				anchor : '100%',
				emptyText : '支持回车Enter',
				xtype : 'textfield',
				name : 'value2',
				listeners : {
					specialkey : function(field, e) {
						if (e.getKey() == Ext.EventObject.ENTER) {
							self.doSearch();
						}
					}
				}
			}, '-', {
				text : '搜索',
				iconCls : 'icon-search',
				xtype : 'button',
				handler : function() {
					me.loadData();
				}
			}, '-', {
				text : '导出',
				iconCls : 'icon-upload-excel',
				xtype : 'button',
				handler : function() {
					me.exportD();
				}
			}];



	var config = {
		store : this.store,
		sm : new Ext.grid.RowSelectionModel(),
		loadMask : true,
		columnLines : true,
		stripeRows : true,
		margins : '0 5 5 5',
		border : false,
		columns : this.columns,
		viewConfig : {
			enableTextSelection : true,
			forceFit : true,
			getRowClass : function(record, rowIndex, rowParams, store) {
				if (record.data.expectSemanticAbs == record.data.expectSemanticFact) {
					if (me.changeColor(record.data.expectSemanticMiss, record.data.expectSemanticFactMiss)) {
						return 'x-grid-record-green'
					} else {
						return 'x-grid-record-yellow';
					}
				} else {
					return 'x-grid-record-common';
				}
			}
		}
	};

	AbstractSemanticTaskDetailSearchPanel.superclass.constructor.call(this, config);
};

Ext.extend(AbstractSemanticTaskDetailSearchPanel, Ext.grid.EditorGridPanel, {
	changeColor : function(str1, str2) {
		String.prototype.replaceAll = function(str1, str2) {
			var str = this;
			var result = str.replace(eval("/" + str1 + "/gi"), str2);
			return result;
		}
		if (str1 == null || str1 == '') {
			return false;
		}
		var str3 = str2.replaceAll("&", "").replace(/<\s*\/?br>/g, '')
				.replaceAll(",", "");
		if (str3.indexOf(str1.replaceAll(",", "")) != -1) {
			return true;
		} else {
			return false;
		}
	},
	loadData : function() {
		var me = this;
		var condition1 = Ext.getCmp('all_search_condition1').getValue();
		var condition2 = Ext.getCmp('all_search_condition2').getValue();
		var value1 = Ext.getCmp('all_search_value1').getValue();
		var value2 = Ext.getCmp('all_search_value2').getValue();
		me.store.load({
			params : {
				uri : me.uri,
				value1 : value1,
				value2 : value2,
				condition1 : condition1,
				condition2 : condition2
			}
		});
	},
	exportD : function() {
		var me = this;
		var condition1 = Ext.getCmp('all_search_condition1').getValue();
		var condition2 = Ext.getCmp('all_search_condition2').getValue();
		var value1 = Ext.getCmp('all_search_value1').getValue();
		var value2 = Ext.getCmp('all_search_value2').getValue();
		Ext.Ajax.request({
			url : 'kbmgr/abstract-semantic-task-detail!globleExport.action',
			params : {
				uri : me.uri
			},
			success : function(response) {
				var timer = new ProgressTimer({
					initData : Ext.decode(response.responseText),
					progressId : 'globleExport',
					boxConfig : {
						title : '正在准备文件'
					},
					finish : function() {
						Ext.MessageBox.confirm('提示', "是否下载文件", function(btn) {
							if (btn == 'yes') {
								if (!me.downloadIFrame) {
									me.downloadIFrame = me.getEl().createChild({
														tag : 'iframe',
														style : 'display:none;'
													});
								}
								me.downloadIFrame.dom.src = 'kbmgr/abstract-semantic-task-detail!globleExportDetail.action?&_t'
										+ new Date().getTime();
							}
						});
					},
					scope : this
				});
				timer.start();
			},
			failure : function(response) {
				Ext.Msg.alert('请求异常', response.responseText);
			},
			scope : this
		});
	}
})