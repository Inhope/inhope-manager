Ext.namespace('addSegAnalysePanel');
AddSegAnalysePanel = function(_cfg) {
	var self = this;
	this.data = {};
	this.extraAttrs = {};
	var _pageSize = 20;
	this.isFilter = false;
	this.importType = '';

	var _store = new Ext.data.Store({
		  proxy : new Ext.data.HttpProxy({
			    params : {
				    'isFilter' : self.isFilter
			    },
			    url : 'add-seg-analyse!list.action'
		    }),
		  reader : new Ext.data.JsonReader({
			    idProperty : 'id',
			    root : 'data',
			    fields : [{
				      name : 'segWord',
				      type : 'string'
			      }, {
				      name : 'score',
				      type : 'string'
			      }]
		    }),
		  writer : new Ext.data.JsonWriter()
	  });

	var pagingBar = new Ext.PagingToolbar({
		  store : _store,
		  pageSize : _pageSize,
		  displayInfo : true,
		  prependButtons : true,
		  beforePageText : '第',
		  afterPageText : '页，共{0}页',
		  displayMsg : '第{0}到{1}条记录，总数共{2}条',
		  emptyMsg : "没有记录"
	  })
	var paramText1 = new Ext.form.TextField({
		  fieldLabel : '参数1',
		  allowBlank : false,
		  name : 'param1',
		  blankText : '参数不能为空',
		  anchor : '100%',
		  value : '10'
	  })
	var paramText2 = new Ext.form.TextField({
		  fieldLabel : '参数2',
		  allowBlank : false,
		  name : 'param2',
		  blankText : '参数不能为空',
		  anchor : '100%',
		  value : '10'
	  })
	var suggestion = '要导入数据的excel的列名必须是 "问题"';

	var suggestionText = new Ext.form.TextArea({
		  fieldLabel : '提示',
		  name : 'suggestion',
		  xtype : 'textarea',
		  border : false,
		  anchor : '98%',
		  value : suggestion,
		  height : 40,
		  disabled : true
	  });
	this.suggestionText = suggestionText;
	// 定义个formPanel
	var confFormPanel = new Ext.FormPanel({
		  region : 'center',
		  layout : 'fit',
		  frame : false,
		  enctype : 'multipart/form-data',
		  bodyStyle : 'padding:5px 5px 0',
		  width : 100,
		  height : 180,
		  fileUpload : true,
		  items : [{
			    xtype : 'fieldset',
			    title : '选择文件',
			    autoHeight : true,
			    items : [{
				      border : false,
				      ref : '/importAddSegAnalyseFile',
				      name : 'importFile',
				      xtype : "textfield",
				      fieldLabel : '文件',
				      inputType : 'file',
				      anchor : '96%'
			      }, suggestionText]
		    }]
	  });

	// 打开窗口
	var analyseDataWindow = new Ext.Window({
		  width : 400,
		  height : 200,
		  modal : true,
		  plain : true,
		  shim : true,
		  title : '导入信息',
		  closeAction : 'hide',
		  collapsible : true,// 折叠
		  closable : true, // 关闭
		  resizable : false,// 改变大小
		  draggable : true,// 拖动
		  minimizable : false,// 最小化
		  maximizable : false,// 最大化
		  animCollapse : true,
		  constrainHeader : true,
		  layout : 'border',
		  bodyStyle : 'padding:5px;',
		  items : [confFormPanel],
		  buttonAlign : "center",
		  buttons : [{
			    text : '提交',
			    handler : function() {
				    var btn = this;
				    if (confFormPanel.importAddSegAnalyseFile.getValue() != null && confFormPanel.importAddSegAnalyseFile.getValue()) {
					    var paramText1 = self.paramText1.getValue();
					    var paramText2 = self.paramText2.getValue();
					    if (confFormPanel.getForm().isValid()) {
						    if (paramText1 && paramText2) {
							    confFormPanel.getForm().submit({
								      params : {
									      'importType' : self.importType,
									      'configParam1' : paramText1,
									      'configParam2' : paramText2
								      },
								      url : 'add-seg-analyse!importDataAnalyse.action',
								      success : function(form, action) {
									      btn.enable();
									      var result = action.result.data;
									      var timer = new ProgressTimer({
										        initData : result,
										        progressId : 'importDataAddSegAnalyseStatus',
										        boxConfig : {
											        title : '正在导入文件...'
										        },
										        finish : function(p, response) {
											        confFormPanel.importAddSegAnalyseFile.setValue('');
											        self.loadData(true);
										        }.dg(this)
									        });
									      timer.start();
								      },
								      failure : function(form, action) {
									      btn.enable();
									      Ext.MessageBox.hide();
									      Ext.Msg.alert('导入失败', action.result.message);
								      }
							      });
						    } else {
							    Ext.Msg.alert('错误提示', '请输入你要分析的参数');
							    return;
						    }
					    }
				    } else {
					    Ext.Msg.alert("警告", "上传文件不能为空");
				    }
			    }
		    }, {
			    text : '重置',
			    type : 'reset',
			    handler : function() {
				    confFormPanel.form.reset();
			    }
		    }]
	  });

	var actionTbarArray = ['最小共现次数:', paramText1, '最大词长度:', paramText2, '<span style="color:red;size:10px;">调小共现次数调大词长度提高召回率</span>', '|', {
		  text : '开始分析',
		  iconCls : 'icon-search',
		  handler : function() {
			  if (paramText1.getValue() && paramText2.getValue()) {
				  self.startAddSegAnalyse();
			  } else {
				  Ext.Msg.alert('错误提示', '请输入你要分析的参数');
				  return;
			  }
		  }
	  }, {
		  text : '显示全部',
		  iconCls : 'icon-ontology-sync',
		  hidden : true,
		  handler : function() {
			  self.isFilter = false;
			  self.loadData();
		  }
	  }, {
		  text : '显示过滤结果',
		  hidden : true,
		  iconCls : 'icon-ontology-reidx',
		  handler : function() {
			  self.isFilter = true;
			  self.loadData();
		  }
	  }, '&nbsp;', {
		  text : '导出数据',
		  iconCls : 'icon-ontology-export',
		  handler : function() {
			  Ext.Msg.confirm('提示信息', '导出只能是当前分析的结果，你确定要导出吗？', function(choice) {
				    if (choice == 'yes') {
					    self.exportData();
				    }
			    });
		  }
	  }, '&nbsp;', {
		  text : '导入数据分析',
		  iconCls : 'icon-ontology-import',
		  handler : function() {
			  Ext.Msg.show({
				    title : '导入文件类型',
				    msg : '您选择要导入文件格式',
				    buttons : {
					    no : 'Excel',
					    yes : 'txt',
					    cancel : true
				    },
				    fn : function(choice) {
					    if (choice == 'cancel')
						    return
					    choice == 'yes' ? self.importType = 'txt' : '';
					    choice == 'no' ? self.importType = 'excel' : '';
					    analyseDataWindow.show();
				    }
			    });
		  }.dg(this)
	  }]
	var actionTbar = new Ext.Toolbar({
		  items : actionTbarArray
	  });

	var cfg = {
		region : 'center',
		border : false,
		frame : false,
		store : _store,
		loadMask : true,
		colModel : new Ext.grid.ColumnModel({
			  defaults : {
				  sortable : true
			  },
			  columns : [new Ext.grid.RowNumberer(), {
				    header : '分词内容',
				    dataIndex : 'segWord',
				    sortable : false
			    }, {
				    header : '向量值',
				    dataIndex : 'score',
				    hidden : true,
				    sortable : false
			    }]
		  }),
		tbar : actionTbar,
		bbar : pagingBar,
		viewConfig : {
			forceFit : true
		}

	}
	AddSegAnalysePanel.superclass.constructor.call(this, Ext.applyIf(_cfg || {}, cfg));
	this.cfg = cfg;
	this.getPageSize = function() {
		return _pageSize;
	}
	this.actionTbar = actionTbar;

	this.paramText1 = paramText1;
	this.paramText2 = paramText2;

	this.on('afterrender', function() {
		  this.getAnalyseTimerConfig(null, function(config) {
			    ProgressTimer.createTimer(config, true);
		    });
	  })
}
/**
 * @class AddSegAnalysePanel
 * @extends Ext.grid.GridPanel
 */
Ext.extend(AddSegAnalysePanel, Ext.grid.GridPanel, {
	  loadData : function() {
		  var self = this;
		  var store = this.getStore();
		  store.baseParams['isFilter'] = self.isFilter;
		  store.load({
			    params : {
				    start : 0,
				    limit : this.getPageSize()
			    },
			    add : false,
			    scope : this
		    });
	  },
	  getAnalyseTimerConfig : function(initdata, cb) {
		  var self = this;
		  var isCancelBtnDisable = false;
		  Ext.Ajax.request({
			    url : 'add-seg-analyse!getCurrentAnalyseUser.action',
			    success : function(response) {
			    },
			    failure : function() {
			    },
			    callback : function(options, success, response) {
				    if (success) {
					    var result = Ext.util.JSON.decode(response.responseText);
					    if ("false" == result.message) {
						    isCancelBtnDisable = true;
					    }
				    }
				    cb({
					      isCancelDisabled : isCancelBtnDisable,
					      initData : initdata,
					      progressId : 'addSegAnalyseStatus',
					      boxConfig : {
						      title : '分析进度'
					      },
					      finish : function() {
						      Ext.Ajax.request({
							        url : 'add-seg-analyse!removeCurrentUserName.action',
							        success : function() {
							        },
							        failure : function() {
							        }
						        })
						      this.loadData();
					      }.dg(self)
				      })
			    }
		    });
	  },
	  startAddSegAnalyse : function() {
		  var self = this;
		  var paramText1 = this.paramText1.getValue();
		  var paramText2 = this.paramText2.getValue();
		  if (paramText1 && paramText2) {
			  Ext.Ajax.request({
				    params : {
					    'configParam1' : paramText1,
					    'configParam2' : paramText2
				    },
				    url : 'add-seg-analyse!startAnalyse.action',
				    success : function(response) {
					    var result = Ext.util.JSON.decode(response.responseText);
					    this.getAnalyseTimerConfig(result.data, function(config) {
						      ProgressTimer.createTimer(config, true);
					      });
				    },
				    failure : function(response) {
					    alert('请求失败');
				    },
				    scope : this
			    })
		  }

	  },
	  exportData : function() {
		  var self = this;
		  var excelVersion = '';
		  Ext.Msg.show({
			    title : '导出文件类型',
			    msg : '您选择要导出的文件类型',
			    buttons : {
				    yes : 'Excel07',
				    no : 'Excel03',
				    cancel : true
			    },
			    fn : function(choice) {
				    if (choice == 'cancel')
					    return
				    choice == 'yes' ? excelVersion = true : false;
				    choice == 'no' ? excelVersion = false : true;
				    Ext.Ajax.request({
					      params : {
						      'excelVersion' : excelVersion
					      },
					      url : 'add-seg-analyse!exportResult.action',
					      success : function(response) {
						      var result = Ext.util.JSON.decode(response.responseText);
						      var filename = result.message;

						      var param = Ext.urlEncode({
							        'filename' : filename
						        })
						      var timer = new ProgressTimer({
							        initData : result.data,
							        progressId : 'addSegAnalyseExport',
							        boxConfig : {
								        title : '正在准备文件'
							        },
							        finish : function() {
								        // 准备下载
								        if (!self.downloadIFrame) {
									        self.downloadIFrame = self.getEl().createChild({
										          tag : 'iframe',
										          style : 'display:none;'
									          })
								        }
								        self.downloadIFrame.dom.src = 'add-seg-analyse!downExportResult.action?' + param;
							        },
							        scope : this
						        });
						      timer.start();
					      },
					      failure : function(response) {
						      alert("请求失败!");
					      },
					      scope : this
				      });
			    }
		    });
	  }
  })
