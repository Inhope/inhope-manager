AnalyzerManager = function(_cfg) {
	var self = this;

	var _fileForm = new Ext.FormPanel(
			{
				layout : "fit",
				frame : true,
				border : false,
				autoHeight : true,
				waitMsgTarget : true,
				defaults : {
					bodyStyle : 'padding:5px'
				},
				margins : '0 0 0 0',
				labelAlign : "left",
				labelWidth : 50,
				fileUpload : true,
				items : [
						{
							xtype : 'fieldset',
							title : '选择文件',
							autoHeight : true,
							items : [
									{
										name : 'analyzerFile',
										xtype : "textfield",
										fieldLabel : '文件',
										inputType : 'file',
										allowBlank : false,
										anchor : '96%',
										validator : function(v) {
											if (v) {
												if (v.substring(v.length - 4) != '.xls' && v.substring(v.length - 5) != '.xlsx')
													return '请选择excel文件';
												else
													return true;
											} else
												return false;
										}
									},
									{
										text : "导入分析",
										xtype : 'button',
										style : 'padding-top:8px;padding-right:11px;float:right;',
										handler : function() {
											var btn = this;
											var fileForm = _fileForm.getForm();
											if (fileForm.isValid()) {
												btn.disable();
												fileForm
														.submit({
															url : 'analyzer-file!analyzer.action',
															success : function(
																	form,
																	act) {
																btn.enable();
																if (act.response.responseText) {
																	var timer = new ProgressTimer({
																				initData : Ext.decode(act.response.responseText),
																				progressText : 'percent',
																				progressId : 'analyzerFile',
																				boxConfig : {
																					title : '分析文件中'
																				},
																				finish : function(p, response) {
																					if (!this.downloadIFrame) {
																						this.downloadIFrame = self.el.createChild({
																									tag : 'iframe',
																									style : 'display:none;'
																								})
																					}
																					this.downloadIFrame.dom.src = 'analyzer-file!down.action';
																				},
																				scope : this
																			});
																	timer.start();
																}
															},
															failure : function(
																	form,
																	action) {
																btn.enable();
																Ext.MessageBox
																		.hide();
																Ext.Msg
																		.alert(
																				'导入失败',
																				action.response.responseText);
															}
														});
											}
										}
									} ]
						} ]
			});
	var _importPanel = new Ext.Panel({
		border : false,
		layout : "fit",
		layoutConfig : {
			animate : true
		},
		items : [ _fileForm ]
	});

	var cfg = new Ext.Window({
		border : false,
		width : 350,
		title : '文件分析',
		defaults : {
			border : false
		},
		plain : true,
		modal : true,
		plain : true,
		shim : true,
		closeAction : 'hide',
		collapsible : true,
		closable : true,
		resizable : false,
		draggable : true,
		minimizable : false,
		maximizable : false,
		animCollapse : true,
		constrainHeader : true,
		autoHeight : false,
		items : [ _importPanel ]
	});
	AnalyzerManager.superclass.constructor.call(this, Ext.applyIf(_cfg
			|| {}, cfg));
};
Ext.extend(AnalyzerManager, Ext.Window, {
	showWindow : function() {
		this.show();
	}
});