AttachmentTreePanel = function(_cfg) {

	var self = this;
	this.attachmentPanel = '';
	var cfg = {
		title : '附件管理',
		width : 200,
		minSize : 175,
		maxSize : 400,
		xtype : 'treepanel',
		collapsible : true,
		rootVisible : false,
		lines : false,
		border : false,
		autoScroll : true,
		listeners : {},
		root : {
			id : '0',
			text : 'root',
			iconCls : 'icon-ontology-root',
			lazyLoad : true,
			children : [{
				  id : '1',
				  text : "实例目录"
			  }, {
				  id : '3',
				  text : "答案目录"
			  }, {
				  id : '2',
				  text : "公共池目录",
				  leaf : true
			  }]

		},
		dataUrl : 'attachment!list.action',
		tbar : new Ext.Toolbar({
			  enableOverflow : true,
			  height : '20',
			  items : [{
				    ref : 'MN_EXP',
				    text : '导出列表',
				    iconCls : 'icon-ontology-import',
				    width : 50,
				    handler : function() {
					    Ext.Ajax.request({
						      url : 'attachment!getChecklist.action',
						      success : function(response) {
							      var result = response.responseText;
							      if (result) {
								      Ext.Msg.alert('提示', '以下是文件列表下载地址，请复制：<br/>' + result)
							      } else {
								      Ext.Msg.alert('错误', '获取附件列表失败')
							      }
						      }
					      });
				    }.dg(this)
			    }, {
				    ref : 'MN_IMP',
				    text : '导出',
				    iconCls : 'icon-ontology-export',
				    width : 50,
				    handler : function() {
					    Ext.Msg.prompt('提示', '请输入文件列表的地址', function(result, v) {
						      if (result != 'ok' || !v || !v.trim())
							      return
						      Ext.Ajax.request({
							        url : 'attachment!doExport.action',
							        params : {
								        checklistURL : v
							        },
							        success : function(response) {
								        var result = Ext.decode(response.responseText);
								        if (result) {
									        var timer = new ProgressTimer({
										          initData : result,
										          progressId : result.data,
										          progressURL : 'attachment!getProgress.action',
										          boxConfig : {
											          title : '导出附件'
										          },
										          finish : function(progress, res) {
											          var addr = http_get('property!get.action?key=attachment.addr')
											          Ext.Msg.alert('提示', '以下是附件导出文件的下载地址，请复制：<br/>http://' + addr + '/robot-search/downExportFile?id=' + result.data);
										          }.dg(this)
									          });
									        timer.start();
								        } else {
									        Ext.Msg.alert('错误', '获取失败')
								        }
							        }
						        });
					      })

				    }.dg(this)
			    }, {
				    ref : 'MN_EXP',
				    text : '导入',
				    iconCls : 'icon-ontology-import',
				    width : 50,
				    handler : function() {
					    Ext.Msg.prompt('提示', '请输入导出文件的地址', function(result, v) {
						      if (result != 'ok' || !v || !v.trim())
							      return
						      Ext.Ajax.request({
							        url : 'attachment!doImport.action',
							        params : {
								        exportURL : v
							        },
							        success : function(response) {
								        var result = Ext.decode(response.responseText);
								        if (result) {
									        var timer = new ProgressTimer({
										          initData : result,
										          progressURL : 'attachment!getProgress.action',
										          progressId : result.data,
										          boxConfig : {
											          title : '导入附件'
										          }
									          });
									        timer.start();
								        } else {
									        Ext.Msg.alert('错误', '获取附件列表失败')
								        }
							        }
						        });
					      })
				    }.dg(this)
			    }, {
				    text : '刷新',
				    iconCls : 'icon-refresh',
				    width : 50,
				    handler : function() {
					    self.getSelectedNode().reload();
				    }
			    }]
		  })
	};

	AttachmentTreePanel.superclass.constructor.call(this, Ext.applyIf(_cfg || {}, cfg));
	this.on('beforeload', function(n) {
		  var tn = n;
		  while (tn.id.length > 1) {
			  tn = tn.parentNode;
		  }
		  this.getLoader().baseParams.topNodeId = tn.id
	  })
	this.on('click', function(n) {
		  if (!n)
			  return false;

		  this.attachmentPanel = self.showTab(AttachmentPanel, 'AttachmentTab', '附件管理', 'icon-wordclass-category', true);
		  var tn = n;
		  while (tn.id.length > 1) {
			  tn = tn.parentNode;
		  }
		  this.attachmentPanel.loadAttachments(n.id, '', tn.id);
	  })
}

Ext.extend(AttachmentTreePanel, Ext.tree.TreePanel, {
	  authName : 'kb.ra',
	  isRoot : function(node) {
		  return node.id.length == 1;
	  },
	  getSelectedNode : function() {
		  return this.getSelectionModel().getSelectedNode();
	  },

	  searchAttachment : function(focusTarget) {
		  var categoryName = this.getTopToolbar().getComponent(0).getValue();
		  if (!categoryName || !(categoryName = categoryName.trim())) {
			  Ext.Msg.alert('提示', '请输入附件名称');
			  return;
		  }
		  if (!this.getSelectionModel().getSelectedNode()) {
			  this.getRootNode().childNodes[0].select();
		  }
		  var self = this;
		  Ext.Ajax.request({
			    url : 'attachment!search.action',
			    params : {
				    categoryName : categoryName,
				    matchExactly : false
			    },
			    success : function(form, action) {
				    if (!form.responseText) {
					    Dashboard.setAlert('没有查找到更多的"' + categoryName + '"，请修改后重新查找');
					    return;
				    }
				    var idPath = Ext.util.JSON.decode(form.responseText);
				    if (idPath) {
					    var targetId = idPath.pop();
					    self.expandPath('/0/1/' + idPath.join('/'), '', function(suc, n) {
						      var selNode = self.getNodeById(targetId);
						      selNode.select();
						      selNode.fireEvent('click', selNode);
						      if (focusTarget)
							      focusTarget.focus()
					      });
				    }
			    },
			    failure : function(form, action) {
				    Ext.Msg.alert('提示', '查找错误');
			    }
		    });
	  }
  });

if (http_get('property!get0.action?key=attachment.addr')) {
	Dashboard.registerNav(AttachmentTreePanel, 19);
}
