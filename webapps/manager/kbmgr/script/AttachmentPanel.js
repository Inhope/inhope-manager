AttachmentPanel = function(_cfg) {
	var self = this;
	this.data = {};
	this.extraAttrs = {};
	var _pageSize = 20;

	var reader = new Ext.data.JsonReader({
		  idProperty : 'id1',
		  root : 'data',
		  fields : ['id', {
			    name : 'name',
			    type : 'string'
		    }, 'objectName', {
			    name : 'createTime',
			    type : 'string'
		    }, {
			    name : 'createTime',
			    type : 'string'
		    }, {
			    name : 'physicalAttachments',
			    type : 'object'
		    }]
	  });
	var _store = new Ext.data.Store({
		  proxy : new Ext.data.HttpProxy({
			    url : 'attachment!listEx.action'
		    }),
		  reader : reader,
		  writer : new Ext.data.JsonWriter()
	  });
	_store.on('load', function() {

	  });

	var myExpander = new Ext.grid.RowExpander({
		lazyRender : false,
		enableCaching : false,
		tpl : new Ext.XTemplate('{[this.getContent(values.physicalAttachments)]}', {
			getContent : function(physicalAttachments) {
				var tableStr = '<table style="margin:2px 0px 2px 48px;border:1px solid #C0C0C0;width:800px;">';
				tableStr += '<tr><td width="20%" style="vertical-align:middle; text-align:center;">版本号</td><td width="15%" style="vertical-align:middle; text-align:center;">创建时间</td><td width="15%" style="vertical-align:middle; text-align:center;">修改人</td><td width="30%" style="vertical-align:middle; text-align:center;">备注</td><td width="10%"></td><td width="10%"></td></tr>';
				for (var pi = 0; pi < physicalAttachments.length; pi++) {
					tableStr += '<tr>';
					tableStr += '<td style="vertical-align:middle; text-align:center;">' + physicalAttachments[pi]['name'] + '</td><td style="vertical-align:middle; text-align:center;">'
					  + physicalAttachments[pi]['createTime'] + '</td>';
					tableStr += '<td style="vertical-align:middle; text-align:center;">' + ((physicalAttachments[pi]['editor'] == null) ? '' : physicalAttachments[pi]['editor']) + '</td>'
					  + '<td style="vertical-align:middle; text-align:center;">' + (physicalAttachments[pi]['remark'] == null ? '' : physicalAttachments[pi]['remark']) + '</td>';
					tableStr += '<td style="vertical-align:middle; text-align:center;"><a href="javascript:void(0);" onclick="javscript:attachmentObj.download(\'' + physicalAttachments[pi]['id']
					  + '\');">下载</a></td><td style="vertical-align:middle; text-align:center;"><a href="javascript:void(0);" onclick="javscript:attachmentObj.removeFile(\''
					  + physicalAttachments[pi]['id'] + '\');">删除</a></td>';
					tableStr += '</tr>';
				}
				tableStr += '</table>';
				return tableStr;
			}
		})
	});

	var pagingBar = new Ext.PagingToolbar({
		  store : _store,
		  displayInfo : true,
		  pageSize : _pageSize,
		  prependButtons : true,
		  beforePageText : '第',
		  afterPageText : '页，共{0}页',
		  displayMsg : '附件总共{2}个',
		  emptyMsg : "没有记录"
	  });

	this.pagingBar = pagingBar;

	// var _sm = new Ext.grid.CheckboxSelectionModel();
	var _sm = new Ext.grid.RowSelectionModel();

	// var _columns = [ _sm,new Ext.grid.RowNumberer(),myExpander, {
	var _columns = [new Ext.grid.RowNumberer(), myExpander, {
		  header : 'id',
		  dataIndex : 'id',
		  hidden : true
	  }, {
		  header : '附件名称',
		  dataIndex : 'name',
		  width : 75
	  }, {
		  header : '关联路径',
		  dataIndex : 'objectName'
	  }, {
		  header : '创建时间',
		  dataIndex : 'createTime',
		  width : 80
	  }];

	var cfg = {
		columnLines : true,
		store : _store,
		margins : '0 5 5 5',
		border : false,
		style : 'border-right: 1px solid ' + sys_bdcolor,
		colModel : new Ext.grid.ColumnModel({
			  columns : _columns
		  }),
		sm : _sm,
		tbar : [{
			  ref : 'searchField',
			  emptyText : "请输入附件名称",
			  xtype : 'textfield',
			  width : 120,
			  listeners : {
				  specialkey : function(f, e) {
					  if (e.getKey() == e.ENTER) {
						  self.searchAttachment();
					  }
				  }
			  }
		  }, {
			  text : '查找',
			  iconCls : 'icon-search',
			  width : 50,
			  handler : function() {
				  self.searchAttachment();
			  }
		  }, '-', {
			  text : '下载',
			  iconCls : 'icon-add',
			  handler : function() {
				  self.download();
			  }
		  }, '', {
			  text : '删除',
			  iconCls : 'icon-delete',
			  handler : function() {
				  self.removeFile();
			  }
		  }, '', {
			  text : '解除关系',
			  iconCls : 'icon-delete',
			  handler : function() {
				  self.relieveRelation();
			  }
		  }, '', {
			  text : '上传',
			  iconCls : 'icon-add',
			  handler : function(btn) {
				  btn.disable();
				  self.fileUpLoadWindowSingle.upload('attachment!uploadSingle.action', function(success, uploadFileId, act, filename) {
					    if (success == null) {

					    } else {
						    self.fileUpLoadWindowSingle.hide();
						    self.store.reload();
					    }
					    btn.enable();
				    });
			  }
		  }],
		bbar : pagingBar,
		loadMask : true,
		viewConfig : {
			forceFit : true
		},
		plugins : [myExpander]
	};
	AttachmentPanel.superclass.constructor.call(this, Ext.applyIf(_cfg || {}, cfg));

	this.getPageSize = function() {
		return _pageSize;
	};

	RelieveRelationWindow = Ext.extend(Ext.Window, {
		  initComponent : function() {
			  this.items = this._buildForm();
			  RelieveRelationWindow.superclass.initComponent.call(this);
			  this.on('close', function(p) {
			    })
			  this.on('hide', function(p) {
				    _store.reload();
			    })
		  },
		  currentAttachmentId : null,
		  _buildForm : function() {
			  var readerRelation = new Ext.data.JsonReader({
				    idProperty : 'id',
				    root : 'data',
				    fields : [{
					      name : 'name',
					      type : 'string'
				      }]
			    });
			  var _storeRelation = new Ext.data.Store({
				    autoLoad : false,
				    proxy : new Ext.data.HttpProxy({
					      url : 'attachment!listRelation.action'
				      }),
				    reader : readerRelation
			    });
			  var _smRelation = new Ext.grid.CheckboxSelectionModel();
			  var _columnsRelation = [_smRelation, new Ext.grid.RowNumberer(), {
				    header : 'id',
				    dataIndex : 'id',// objectid
				    hidden : true
			    }, {
				    header : '关联对象名称',
				    dataIndex : 'name',
				    width : 150
			    }];

			  return this.relieveRelationPanel = new Ext.grid.GridPanel({
				    columnLines : true,
				    store : _storeRelation,
				    margins : '0 5 5 5',
				    border : false,
				    style : 'border-right: 1px solid ' + sys_bdcolor,
				    colModel : new Ext.grid.ColumnModel({
					      columns : _columnsRelation
				      }),
				    sm : _smRelation,
				    tbar : [{
					      text : '解除关系',
					      iconCls : 'icon-delete',
					      handler : function(btn) {
						      var rows = this.relieveRelationPanel.getSelectionModel().getSelections();
						      // console.log('rows',rows);
						      if (rows.length == 0) {
							      Ext.Msg.alert('提示', '请先选择实例！');
							      return false;
						      }

						      btn.disable();
						      Ext.Msg.confirm('确定框', '确定解除附件与所选中对象的关系吗?', function(result) {
							        if (result == 'yes') {
								        var objectIds = '';
								        for (var i = 0; i < rows.length; i++) {
									        objectIds += rows[i]['id'] + ',';
								        }
								        Ext.Ajax.request({
									          url : 'attachment!relieveRelation.action',
									          params : {
										          "attachmentId" : this.currentAttachmentId,
										          "objectIds" : objectIds,
										          "topNodeId" : this.topNodeId
									          },
									          success : function(response) {
										          this.relieveRelationPanel.store.reload();
										          Dashboard.setAlert('解除关系成功！');
										          btn.enable();
									          },
									          failure : function(response) {
										          Ext.Msg.alert("错误", response.responseText);
										          btn.enable();
									          },
									          scope : this
								          });
							        } else {
								        btn.enable();
							        }
						        }, this);
					      },
					      scope : this
				      }],
				    loadMask : true,
				    viewConfig : {
					    forceFit : true
				    },
				    autoHeight : true
			    });
		  },
		  title : '解除关系',
		  defaults : {
			  border : false
		  },
		  modal : true,
		  plain : true,
		  shim : true,
		  closable : true,
		  closeAction : 'hide',
		  collapsible : true,
		  animCollapse : true,
		  constrainHeader : true,
		  width : 400
	  });

	this.store = _store;
	this.myExpander = myExpander;
	window.attachmentObj = this;
	this.relieveRelationWindow = new RelieveRelationWindow();
	this.fileUpLoadWindowSingle = new commons.FileUploadWindow();

};

Ext.extend(AttachmentPanel, Ext.grid.GridPanel, {
	  searchAttachment : function() {
		  var v = this.getTopToolbar().searchField.getValue()
		  this.loadAttachments('', {
			    data : Ext.encode({
				      name : v && v.trim() ? v : ''
			      })
		    })
	  },
	  loadAttachments : function(nodeId, params, topNodeId) {
		  if (nodeId.length == 1) {
			  this.getTopToolbar().find('text', '解除关系')[0].disable();
		  } else {
			  this.getTopToolbar().find('text', '解除关系')[0].enable();
		  }
		  if (nodeId == '2') {
			  this.getTopToolbar().find('text', '上传')[0].enable();
		  } else {
			  this.getTopToolbar().find('text', '上传')[0].disable();
		  }
		  if (nodeId)
			  this.store.baseParams['nodeId'] = nodeId;
		  if (topNodeId)
			  this.store.baseParams['topNodeId'] = topNodeId;
		  this.store.baseParams['data'] = params.data?params.data:'';
		  this.store.load({
			    params : {
				      start : 0,
				      limit : this.getPageSize()
			      }
		    });
	  },
	  download : function(attachmentId) {
		  var rows = this.getSelectionModel().getSelections();
		  if (rows.length == 0 && !attachmentId) {
			  Ext.Msg.alert('提示', '请先选择要下载的附件！');
			  return false;
		  }
		  if (!attachmentId && rows.length > 1) {
			  Ext.Msg.alert('提示', '每次只能下载一个附件！');
			  return false;
		  }
		  if (!attachmentId)
			  attachmentId = rows[0]['data']['physicalAttachments'][0]['id'];
		  if (!this.downloadIFrame) {
			  this.downloadIFrame = this.getEl().createChild({
				    tag : 'iframe',
				    style : 'display:none;'
			    });
		  }
		  this.downloadIFrame.dom.src = search_context_path + '/attachmentDown?attachmentId=' + attachmentId + '&_t=' + new Date().getTime();
	  },
	  removeFile : function(attachmentId) {
		  var rows = this.getSelectionModel().getSelections();
		  if (rows.length == 0 && !attachmentId) {
			  Ext.Msg.alert('提示', '请先选择要删除的附件！');
			  return false;
		  }
		  // console.log('attachmentId',attachmentId);
		  // console.log('rows[0]["id"]',rows[0]['id']);
		  if (!attachmentId && rows.length > 1) {
			  Ext.Msg.alert('提示', '每次只能选择一个附件删除！');
			  return false;
		  }
		  Ext.Msg.confirm('确定框', '确定删除选中的附件吗?', function(result) {
			    if (result == 'yes') {
				    var removeType;
				    var removeId;
				    if (!attachmentId) {
					    removeType = '2';
					    removeId = rows[0].get('id');
				    } else {
					    removeType = '1';
					    removeId = attachmentId;
				    }
				    if (removeType == '2') {
					    Ext.Msg.confirm('确定框', '确定要同时将历史版本全部删除?', function(btn) {
						      if (btn == 'yes') {
							      attachmentObj.removeHandle(removeType, removeId);
						      }
					      }, this);
				    } else {
					    attachmentObj.removeHandle(removeType, removeId);
				    }
			    }
		    }, this);
	  },
	  removeHandle : function(removeType, removeId) {
		  var attachmentSelf = this;
		  Ext.Ajax.request({
			    url : 'attachment!remove.action',
			    params : {
				    "removeType" : removeType,
				    "topNodeId" : this.store.baseParams['topNodeId'],
				    "removeId" : removeId
			    },
			    success : function(response) {
				    Dashboard.setAlert('删除成功！');
				    attachmentSelf.store.reload();
			    },
			    failure : function(response) {
				    Ext.Msg.alert("错误", response.responseText);
			    },
			    scope : this
		    });
	  },
	  relieveRelation : function() {
		  var rows = this.getSelectionModel().getSelections();
		  if (rows.length == 0) {
			  Ext.Msg.alert('提示', '请先选择要解除关系的附件！');
			  return false;
		  }
		  if (rows.length > 1) {
			  Ext.Msg.alert('提示', '每次只能选择一个要解除关系的附件！');
			  return false;
		  }
		  var r = rows[0]
		  this.relieveRelationWindow.relieveRelationPanel.store.removeAll();
		  this.relieveRelationWindow.relieveRelationPanel.store.load({
			    params : {
				    attachmentId : rows[0].get('id'),
				    topNodeId : this.store.baseParams['topNodeId']
			    }
		    });
		  this.relieveRelationWindow.currentAttachmentId = rows[0].get('id')
		  this.relieveRelationWindow.topNodeId = this.store.baseParams['topNodeId']
		  this.relieveRelationWindow.show();
	  }
  });

var attachmentAddr=http_get('property!get0.action?key=attachment.addr');
if(attachmentAddr.indexOf(",")>-1)
   attachmentAddr = attachmentAddr.split(',')[0];
var search_context_path = 'http://' + attachmentAddr + '/robot-search';