Ext.namespace('checkWrongPanel');
CheckWrongPanel = function(_cfg) {
	var self = this;
	this.data = {};
	this.extraAttrs = {};
	var _pageSize = 20;
	var dictType;

	var _store = new Ext.data.Store({
		  proxy : new Ext.data.HttpProxy({
			    url : 'check-wrong!list.action'
		    }),
		  reader : new Ext.data.JsonReader({
			    idProperty : 'id',
			    root : 'data',
			    fields : [{
				      name : 'inputques',
				      type : 'string'
			      }, {
				      name : 'inputcate',
				      type : 'string'
			      }, {
				      name : 'sampletemp',
				      type : 'string'
			      }, {
				      name : 'hopestan',
				      type : 'string'
			      }, {
				      name : 'hopecate',
				      type : 'string'
			      }, {
				      name : 'locaques',
				      type : 'string'
			      }, {
				      name : 'locacate',
				      type : 'string'
			      }, {
				      name : 'matchques',
				      type : 'string'
			      }, {
				      name : 'similar',
				      type : 'string'
			      }, {
				      name : 'attributeName',
				      type : 'string'
			      }, 'answer']
		    }),
		  writer : new Ext.data.JsonWriter()
	  });

	var info = new Ext.Toolbar.TextItem({
		  text : '跑错未进行'
	  });
	var pagingBar = new Ext.PagingToolbar({
		  store : _store,
		  displayInfo : true,
		  pageSize : _pageSize,
		  prependButtons : true,
		  beforePageText : '第',
		  afterPageText : '页，共{0}页',
		  displayMsg : '第{0}到{1}条记录，有错误共{2}条',
		  emptyMsg : "没有记录",
		  prependButtons : false,
		  items : ['-', info]
	  })
	var _sm = new Ext.grid.CheckboxSelectionModel({});
	var isStandQue = new Ext.form.Radio({
		  name : 'isquestion',
		  boxLabel : '标准问',
		  inputValue : 1
	  })
	var isSimQue = new Ext.form.Radio({
		  name : 'isquestion',
		  boxLabel : '扩展问',
		  inputValue : 2,
		  plugins : [new Ext.DomObserver({
			    click : function(e, el) {
				    if (e.shiftKey && e.ctrlKey) {
					    Ext.Msg.confirm('确认', '是否将语义块中词类下的同义词展开跑错', function(btn, text) {
						      if (btn == 'yes') {
							      self.expandSampleByWord = true
						      } else {
							      if (self.expandSampleByWord)
								      delete self.expandSampleByWord
						      }
					      });
				    }
			    }.dg(this)
		    })]
	  })
	var isTemp = new Ext.form.Radio({
		  name : 'isquestion',
		  boxLabel : '模板',
		  inputValue : 3,
		  plugins : [new Ext.DomObserver({
			    click : function(e, el) {
				    if (e.shiftKey && e.ctrlKey) {
					    Ext.Msg.confirm('确认', '是否将语义块中词类下的同义词展开跑错', function(btn, text) {
						      if (btn == 'yes') {
							      self.expandSampleByWord = true
						      } else {
							      if (self.expandSampleByWord)
								      delete self.expandSampleByWord
						      }
					      });
				    }
			    }.dg(this)
		    })],
		  listeners : {
	// 'check' : function(ths, checked) {
		  // if (checked) {
		  // self.optionData[1][1] = "虚拟扩展问";
		  // self.templateClasssOption.setValue(1);
		  // self.templateClasssOption.getStore().loadData(self.optionData);
		  // self.templateClasssOption.show();
		  // } else if (!isKbClass.getValue())
		  // self.templateClasssOption.hide();
		  //
		  // }
		  }
	  })

	var optionData = [['1', '全部'], ['2', "虚拟扩展问"]];
	var optionStore = new Ext.data.ArrayStore({
		  fields : ["id", "name"],
		  data : optionData
	  })
	var templateClasssOption = new Ext.form.ComboBox({
		  hidden : true,
		  editable : false,
		  triggerAction : 'all',
		  valueField : 'id',
		  displayField : 'name',
		  mode : 'local',
		  value : '1',
		  width : 100,
		  store : optionStore
	  });
	this.optionData = optionData;
	this.templateClasssOption = templateClasssOption;
	var dimension = new DimensionBox({
		  width : 150,
		  fieldLabel : '默认维度',
		  name : 'checkTagIds',
		  valueTransformer : function(value) {
			  var tags = this.tags;
			  var groupTags = {};
			  if (tags)
				  Ext.each(tags, function(t) {
					    var groupTag = groupTags[t.dimId];
					    if (!groupTag) {
						    groupTag = [];
						    groupTags[t.dimId] = groupTag;
					    }
					    groupTag.push(t.id)
				    });
			  return groupTags;
		  }
	  });
	var _checkWrongTreeCombo = new TreeComboBox({
		  fieldLabel : '父级分类',
		  emptyText : '请选择父级分类...',
		  editable : false,
		  name : 'parentId',
		  valueField : TreeComboBox.FIELD_PATH,
		  anchor : '95%',
		  allowBlank : true,
		  minHeight : 250,
		  width : 230,
		  root : {
			  id : '0',
			  text : 'root',
			  iconCls : 'icon-ontology-root'
		  },
		  loader : new Ext.tree.TreeLoader({
			    dataUrl : 'ontology-category!list.action',
			    listeners : {
				    'beforeload' : function(t, n) {
					    _checkWrongTreeCombo.loader.baseParams.ispub = self.getParentNodePub(n);
				    }
			    }
		    })
	  });
	var isKbClass = new Ext.form.Radio({
		  name : 'isquestion',
		  boxLabel : '本体类',
		  inputValue : 4,
		  listeners : {
			  'check' : function(ths, checked) {
				  if (checked) {
					  dimension.setDisabled(true);
					  _checkWrongTreeCombo.setDisabled(true);
					  self.optionData[1][1] = "非XXX样例";
					  // self.templateClasssOption.setValue(1);
					  // self.templateClasssOption.getStore().loadData(self.optionData);
					  // self.templateClasssOption.show();
				  } else {
					  dimension.setDisabled(false);
					  _checkWrongTreeCombo.setDisabled(false);
					  // if (!isTemp.getValue())
					  // self.templateClasssOption.hide();
				  }
			  }
		  }
	  })

	var suggestion = '要导入数据的列名必须是 "输入问题","期望标准问"';

	var suggestionText = new Ext.form.TextArea({
		  fieldLabel : '提示',
		  name : 'suggestion',
		  xtype : 'textarea',
		  border : false,
		  anchor : '98%',
		  value : suggestion,
		  height : 60,
		  disabled : true
	  });
	// 定义个formPanel
	var confFormPanel = new Ext.FormPanel({
		  region : 'center',
		  layout : 'fit',
		  frame : false,
		  enctype : 'multipart/form-data',
		  bodyStyle : 'padding:5px 5px 0',
		  width : 100,
		  height : 180,
		  fileUpload : true,
		  items : [{
			    xtype : 'fieldset',
			    title : '选择文件',
			    autoHeight : true,
			    items : [{
				      border : false,
				      // id : 'importCheckWrongFileId',
				      ref : '/importCheckWrongFile',
				      name : 'importFile',
				      xtype : "textfield",
				      fieldLabel : '文件',
				      inputType : 'file',
				      anchor : '96%'
			      }, suggestionText]
		    }]
	  });

	// 打开窗口
	var checkDataWindow = new Ext.Window({
		  width : 400,
		  height : 200,
		  modal : true,
		  plain : true,
		  shim : true,
		  title : '导入校验信息',
		  closeAction : 'hide',
		  collapsible : true,// 折叠
		  closable : true, // 关闭
		  resizable : false,// 改变大小
		  draggable : true,// 拖动
		  minimizable : false,// 最小化
		  maximizable : false,// 最大化
		  animCollapse : true,
		  constrainHeader : true,
		  layout : 'border',
		  bodyStyle : 'padding:5px;',
		  items : [confFormPanel],
		  buttonAlign : "center",
		  buttons : [{
			    text : '提交',
			    handler : function() {
				    var btn = this;
				    if (confFormPanel.importCheckWrongFile.getValue() != null && confFormPanel.importCheckWrongFile.getValue()) {
					    if (confFormPanel.getForm().isValid()) {
						    confFormPanel.getForm().submit({
							      url : 'check-wrong!importQuestionCheckWrong.action',
							      success : function(form, action) {
								      btn.enable();
								      var result = action.result.data;
								      var timer = new ProgressTimer({
									        initData : result,
									        progressId : 'importQuestionCheckWrongStatus',
									        boxConfig : {
										        title : '正在导入文件...'
									        },
									        finish : function(p, response) {
										        if (p.currentCount == -2) {
											        // 下载标准问未对应的信息
											        if (!self.downloadIFrame) {
												        self.downloadIFrame = self.getEl().createChild({
													          tag : 'iframe',
													          style : 'display:none;'
												          })
											        }
											        self.downloadIFrame.dom.src = "check-wrong!downValidateErrorFile.action?_t=" + new Date().getTime();
										        } else
											        self.loadData(true);
									        }.dg(this)
								        });
								      timer.start();
							      },
							      failure : function(form, action) {
								      btn.enable();
								      Ext.MessageBox.hide();
								      Ext.Msg.alert('导入失败', action.result.message);
							      }
						      });
					    }
				    } else {
					    Ext.Msg.alert("警告", "上传文件不能为空");
				    }
			    }
		    }, {
			    text : '重置',
			    type : 'reset',
			    handler : function() {
				    confFormPanel.form.reset();
			    }
		    }]
	  });

	var resultCountStore = new Ext.data.ArrayStore({
		  fields : ['matchType', 'checkWrongCount', 'standQuestionCount', 'averTestQuestion', 'accuracyRate']
	  });
	var resultCountGrid = new Ext.grid.GridPanel({
		  region : 'center',
		  border : false,
		  store : resultCountStore,
		  loadMask : true,
		  bodyStyle : 'background-color: #99bbe8;',
		  colModel : new Ext.grid.ColumnModel({
			    columns : [{
				      header : '知识匹配类别',
				      dataIndex : 'matchType',
				      sortable : false
			      }, {
				      header : '跑错数据(条)',
				      dataIndex : 'checkWrongCount',
				      sortable : false
			      }, {
				      header : '对应标准问(条)',
				      dataIndex : 'standQuestionCount',
				      sortable : false
			      }, {
				      header : '平均测试问(条)',
				      dataIndex : 'averTestQuestion',
				      sortable : false
			      }, {
				      header : '准确率',
				      dataIndex : 'accuracyRate',
				      sortable : false
			      }]
		    }),
		  viewConfig : {
			  forceFit : true
		  }
	  });
	// 打开窗口
	var resultCountWindow = new Ext.Window({
		  width : 600,
		  height : 300,
		  modal : true,
		  plain : true,
		  shim : true,
		  title : '类别统计信息',
		  closeAction : 'hide',
		  closable : true, // 关闭
		  resizable : false,// 改变大小
		  draggable : true,// 拖动
		  layout : 'border',
		  bodyStyle : 'padding:5px;background-color:#99bbe8;',
		  items : [resultCountGrid]
	  });

	var timerTask = new Ext.form.Checkbox({
		  boxLabel : '定时任务',
		  checked : false,
		  name : 'timerTask',
		  hidden : Dashboard.isTimerTaskEnabled() ? false : true,
		  anchor : '100% 100% '
	  });

	var taskTimeField = new ClearableDateTimeField({
		  editable : false,
		  hidden : Dashboard.isTimerTaskEnabled() ? false : true,
		  width : 180
	  });
	this.taskTimeField = taskTimeField;

	var actionTbarArray = ['跑错范围:', '&nbsp;', isStandQue, '&nbsp;', isSimQue, '&nbsp;', isTemp, '&nbsp;', isKbClass, '&nbsp;', templateClasssOption, '&nbsp;', '维度:', dimension,
	  '&nbsp;', '分类:', _checkWrongTreeCombo];
	if (Dashboard.isTimerTaskEnabled())
		actionTbarArray.push('-', timerTask, '&nbsp;执行时间:', taskTimeField);
	this.tbarEx = new Ext.Toolbar({
		  items : [{
			    text : '开始跑错',
			    iconCls : 'icon-search',
			    handler : function() {
				    if (isStandQue.getValue() || isSimQue.getValue() || isTemp.getValue() || isKbClass.getValue()) {
					    self.startCheckWrong();
				    } else {
					    Ext.Msg.alert('错误提示', '请选择跑错范围');
					    return;
				    }
			    }
		    }, '&nbsp;', {
			    text : '导入数据跑错',
			    iconCls : 'icon-ontology-import',
			    handler : function() {
				    checkDataWindow.show();
			    }
		    }, '&nbsp;', '-', {
			    text : '导出数据',
			    iconCls : 'icon-ontology-export',
			    handler : function() {
				    Ext.Msg.confirm('提示信息', '导出只能是当前跑错的结果，你确定要导出吗？', function(choice) {
					      if (choice == 'yes') {
						      self.exportData();
					      }
				      });
			    }
		    }, '-', {
			    xtype : 'splitbutton',
			    text : '语义推荐',
			    iconCls : 'icon-participle',
			    menu : {
				    width : 87,
				    plain : true,
				    items : [{
					      ref : '/allData',
					      text : '全部数据',
					      name : 'suggestSemantic',
					      checked : false
				      }]
			    },
			    handler : function() {
				    var isAll = this.allData.checked ? 1 : 0;
				    var selectData = [];
				    if (isAll == 0) {
					    var rows = self.getSelectionModel().getSelections();
					    if (rows.length == 0) {
						    Ext.Msg.alert('提示', '请先选择要推荐的内容！');
						    return false;
					    }
					    Ext.each(rows, function(_item) {
						      var selectResult = {};
						      selectResult['inputques'] = _item.data.inputques;
						      selectResult['hopestan'] = _item.data.hopestan;
						      selectData.push(selectResult);
					      })
				    }
				    var radio = self.actionTbar.findByType('radio');
				    var scopeType;
				    for (var r in radio) {
					    if (radio[r].checked) {
						    scopeType = radio[r].getGroupValue()
					    }
				    }
				    if (typeof(scopeType) == 'undefined') {
					    scopeType = 0;
				    }
				    Ext.Ajax.request({
					      url : 'check-wrong!suggestSemantic.action',
					      params : {
						      isAll : isAll,
						      scopetype : scopeType,
						      suggestData : Ext.encode(selectData)
					      },
					      success : function(resp) {
						      var result = Ext.util.JSON.decode(resp.responseText);
						      if (result.success && 'nonImportData' != result.message) {
							      var timer = new ProgressTimer({
								        initData : result.data,
								        progressId : 'importDataCheckWrongSuggestSemantic',
								        boxConfig : {
									        title : '导入跑错结果推荐语义'
								        },
								        finish : function(p, response) {
								        }
							        });
							      timer.start();
						      } else
							      Ext.Msg.alert('提示', "跑错结果不是导入数据跑错，不能进行语义分析");
					      },
					      failure : function(resp) {
						      Ext.Msg.alert('错误', resp.responseText);
					      }
				      });
			    }
		    }, Dashboard.isLabs() ? {
			    xtype : 'button',
			    text : '类别统计',
			    iconCls : 'icon-grid',
			    handler : function() {
				    Ext.Ajax.request({
					      url : 'check-wrong!resultTypeCount.action',
					      success : function(resp) {
						      var result = Ext.decode(resp.responseText);
						      if (result.success) {
							      resultCountStore.loadData(result.data);
							      resultCountWindow.show();
						      }
					      },
					      failure : function(resp) {
						      Ext.Msg.alert('错误', resp.responseText);
					      }
				      });
			    }
		    } : '']
	  })
	var actionTbar = new Ext.Toolbar({
		  items : actionTbarArray
	  });

	var cfg = {
		region : 'center',
		border : false,
		frame : false,
		store : _store,
		loadMask : true,
		colModel : new Ext.grid.ColumnModel({
			  defaults : {
				  sortable : true
			  },
			  columns : [new Ext.grid.RowNumberer(), _sm, {
				    header : '输入问题',
				    dataIndex : 'inputques',
				    sortable : false
			    }, {
				    header : '输入类别',
				    dataIndex : 'inputcate',
				    sortable : false,
				    hidden : true
			    }, {
				    header : '样例模板',
				    dataIndex : 'sampletemp',
				    sortable : false,
				    hidden : true
			    }, {
				    header : '期望标准问',
				    dataIndex : 'hopestan',
				    sortable : false
			    }, {
				    header : '期望类别',
				    dataIndex : 'hopecate',
				    sortable : false,
				    hidden : true
			    }, {
				    header : '定位标准问',
				    dataIndex : 'locaques',
				    sortable : false,
				    renderer : function(v) {
					    if (v.indexOf(" ≈≈≈ ") > -1) {
						    var clickStandard = "";
						    var standardQues = v.split(" ≈≈≈ ");
						    var v = '';
						    Ext.each(standardQues, function(sq) {
							      var openStandQues = sq;
							      if (sq.lastIndexOf(")") == sq.length - 1) {
								      openStandQues = sq.substring(0, sq.lastIndexOf("("));
							      }
							      clickStandard += "<a style='text-decoration:underline;'  title='点击打开标准问题' href='javaScript:AssistNavPanel.checkWrongPanel.openStanQuestion(\"" + openStandQues
							        + "\");'>" + sq + "</a>" + " ≈≈≈ ";
						      });
						    clickStandard = clickStandard.substring(0, clickStandard.length - 5);
						    return clickStandard;
					    } else
						    return "<a style='text-decoration:underline;'  title='点击打开标准问题' href='javaScript:AssistNavPanel.checkWrongPanel.openStanQuestion(\"" + v + "\");'>" + v + "</a>"
				    }
			    }, {
				    header : '定位类别',
				    dataIndex : 'locacate',
				    sortable : false,
				    hidden : true
			    }, {
				    header : '匹配的问题',
				    dataIndex : 'matchques',
				    sortable : false
			    }, {
				    header : '匹配的答案',
				    dataIndex : 'answer',
				    sortable : false,
				    hidden : true
			    }, {
				    header : '相似度',
				    dataIndex : 'similar',
				    sortable : false,
				    hidden : true
			    }, {
				    header : '属性名',
				    dataIndex : 'attributeName',
				    sortable : false
			    }]
		  }),
		tbar : actionTbar,
		sm : _sm,
		bbar : pagingBar,

		viewConfig : {
			forceFit : true
		}

	}
	CheckWrongPanel.superclass.constructor.call(this, Ext.applyIf(_cfg || {}, cfg));
	this.cfg = cfg;
	this.getPageSize = function() {
		return _pageSize;
	}
	this.info = info;
	this.actionTbar = actionTbar;

	this.isStandQue = isStandQue;
	this.isSimQue = isSimQue;
	this.isTemp = isTemp;
	this.isKbClass = isKbClass;
	this.dimension = dimension;
	this.checkWrongTreeCombo = _checkWrongTreeCombo;
	this.timerTask = timerTask;

	this.on('render', function() {
		  self.tbarEx.render(this.tbar);
	  })
	this.on('afterrender', function() {
		  this.getStartCheckTimerConfig(null, function(config) {
			    ProgressTimer.createTimer(config, true);
		    });
	  })
}
/**
 * @class CheckWrongPanel
 * @extends Ext.grid.GridPanel
 */
Ext.extend(CheckWrongPanel, Ext.grid.GridPanel, {
	  loadData : function(isImport) {
		  var store = this.getStore();
		  store.load({
			    params : {
				    start : 0,
				    limit : this.getPageSize()
			    },
			    add : false,
			    callback : function(r, op, success) {
				    if (store.reader.jsonData.message)
					    this.info.setText(store.reader.jsonData.message);
				    if (success) {
					    if (isImport) {
						    this.isStandQue.setValue(false);
						    this.isSimQue.setValue(false);
						    this.isTemp.setValue(false);
						    this.isKbClass.setValue(false);
					    } else {
						    if (store.reader.jsonData.message) {
							    if (store.reader.jsonData.message.indexOf("标准问") > -1) {
								    this.isStandQue.setValue(true);
							    }
							    if (store.reader.jsonData.message.indexOf("扩展问") > -1) {
								    this.isSimQue.setValue(true);
							    }
							    if (store.reader.jsonData.message.indexOf("模板") > -1) {
								    this.isTemp.setValue(true);
								    this.cfg.colModel.setHidden(4, false);
							    }
							    if (store.reader.jsonData.message.indexOf("本体类") > -1) {
								    this.isKbClass.setValue(true);
							    }
						    }
					    }
				    }
			    },
			    scope : this
		    });
	  },
	  getStartCheckTimerConfig : function(initdata, cb) {
		  var self = this;
		  var isCancelBtnDisable = false;
		  Ext.Ajax.request({
			    url : 'check-wrong!getCurrentCheckWrongUser.action',
			    success : function(response) {
			    },
			    failure : function() {
			    },
			    callback : function(options, success, response) {
				    if (success) {
					    var result = Ext.util.JSON.decode(response.responseText);
					    if ("false" == result.message) {
						    isCancelBtnDisable = true;
					    }
				    }
				    cb({
					      isCancelDisabled : isCancelBtnDisable,
					      initData : initdata,
					      progressId : 'checkWrongStatus',
					      boxConfig : {
						      title : '跑错进度'
					      },
					      finish : function() {
						      Ext.Ajax.request({
							        url : 'check-wrong!removeCurrentUserName.action',
							        success : function() {
							        },
							        failure : function() {
							        }
						        })
						      this.loadData();
					      }.dg(self)
				      })
			    }
		    });
	  },
	  startCheckWrong : function() {
		  var self = this;
		  var radio = this.actionTbar.findByType('radio');
		  var categoryId = self.checkWrongTreeCombo.getValue();
		  var node = null;
		  var isPub = null
		  if (typeof(categoryId) != 'undefined') {
			  var node = self.checkWrongTreeCombo.tree.getNodeById(categoryId)
			  isPub = this.getParentNodePub(node);
		  }
		  var scopetype;
		  for (var r in radio) {
			  if (radio[r].checked) {
				  scopetype = radio[r].getGroupValue();
				  break;
			  }
		  }
		  var taskTime = null;
		  if (this.timerTask.checked) {
			  taskTime = this.taskTimeField.getRawValue();
			  if (!taskTime) {
				  Ext.Msg.alert('提示', '任务执行时间不能为空');
				  return false;
			  }
		  } else
			  this.info.setText('跑错进行中...');

		  Ext.Ajax.request({
			    params : {
				    'scopetype' : scopetype,
				    // 'optionType' : self.templateClasssOption.getValue(),
				    'categoryid' : categoryId,
				    'dimensions' : Ext.encode(self.dimension.getValue()),
				    'ispub' : isPub,
				    expandSampleByWord : this.expandSampleByWord ? true : false,
				    'taskTime' : this.timerTask.checked ? taskTime : ''
			    },
			    url : 'check-wrong!startCheck.action',
			    success : function(response) {
				    var result = Ext.util.JSON.decode(response.responseText);
				    if ("task" == result.message) {
					    Dashboard.setAlert('跑错任务添加成功');
				    } else {
					    this.getStartCheckTimerConfig(result.data, function(config) {
						      ProgressTimer.createTimer(config, true);
					      });
				    }
			    },
			    failure : function(response) {
				    alert('请求失败');
			    },
			    scope : this
		    })
	  },
	  exportData : function() {
		  var self = this;
		  var radio = this.actionTbar.findByType('radio');
		  var scopeType;
		  var excelVersion = '';
		  Ext.Msg.show({
			    title : '导出文件类型',
			    msg : '您选择要导出的文件类型',
			    buttons : {
				    yes : 'Excel07',
				    no : 'Excel03',
				    cancel : true
			    },
			    fn : function(choice) {
				    if (choice == 'cancel')
					    return
				    choice == 'yes' ? excelVersion = true : false;
				    choice == 'no' ? excelVersion = false : true;
				    for (var r in radio) {
					    if (radio[r].checked) {
						    scopeType = radio[r].getGroupValue()
					    }
				    }
				    if (typeof(scopeType) == 'undefined') {
					    scopeType = 0;
				    }
				    Ext.Ajax.request({
					      params : {
						      'scopetype' : scopeType,
						      'excelVersion' : excelVersion
					      },
					      url : 'check-wrong!exportResult.action',
					      success : function(response) {
						      var result = Ext.util.JSON.decode(response.responseText);
						      var timer = new ProgressTimer({
							        initData : result.data,
							        progressId : 'checkWrongExport',
							        boxConfig : {
								        title : '正在准备文件'
							        },
							        finish : function() {
								        // 准备下载
								        if (!self.downloadIFrame) {
									        self.downloadIFrame = self.getEl().createChild({
										          tag : 'iframe',
										          style : 'display:none;'
									          })
								        }
								        self.downloadIFrame.dom.src = 'check-wrong!downExportResult.action?_t=' + new Date().getTime();
							        },
							        scope : this
						        });
						      timer.start();
					      },
					      failure : function(response) {
						      alert("请求失败!");
					      },
					      scope : this
				      });
			    }
		    });
	  },
	  getParentNodePub : function(node) {
		  if (node != null) {
			  var ispub = node.attributes.ispub;
			  if (!ispub) {
				  return this.getParentNodePub(node.parentNode);
			  } else {
				  return ispub;
			  }
		  } else {
			  return null
		  }
	  },
	  openStanQuestion : function(question) {
		  Ext.Ajax.request({
			    url : 'ontology-object-value!queryFaqQuestion.action',
			    params : {
				    question : question
			    },
			    success : function(form, action) {
				    var resultObj = Ext.util.JSON.decode(form.responseText);
				    if (resultObj.success) {
					    // ShortcutToolRegistry.execAction("knowledgeManager");
					    parent.navToKB(resultObj.data.objectId, resultObj.data.dimValueId, resultObj.data.valueId, true);
				    }
			    },
			    failure : function(response) {
				    Ext.Msg.alert("错误", response.responseText);
				    btn.enable();
			    }
		    })
	  }
  })
