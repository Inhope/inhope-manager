Ext.namespace('kb.cls.attrlist')
kb.cls.attrlist.Type = {
	fields : ['name', 'standardRule'],
	emptyRecord : function() {
		var d = {}
		Ext.each(this.fields, function(f) {
			  d[f] = ''
		  });
		return new (this.getRecordType())(d);
	},
	getRecordType : function() {
		if (!this.rt)
			this.rt = Ext.data.Record.create(this.fields);
		return this.rt;
	},
	createStore : function(cfg) {
		cfg = cfg || {}
		return new Ext.data.JsonStore(Ext.apply({
			  remoteSort : false,
			  fields : this.getRecordType()
		  }, cfg));
	}
}
kb.cls.attrlist.Panel = Ext.extend(Ext.grid.GridPanel, {
	  border : false,
	  initComponent : function() {
		  this.store = kb.cls.attrlist.Type.createStore({
			    url : 'ontology-class!listAttrs.action',
			    root : 'data',
			    autoLoad : false
		    })
		  this.columns = [ {
			    header : '属性名',
			    dataIndex : 'name',
			    width : 150
		    }, {
			    header : '样例',
			    width : 150,
			    dataIndex : 'standardRule'
		    }]
		  this.selModel = new Ext.grid.RowSelectionModel();

		  kb.cls.attrlist.Panel.superclass.initComponent.call(this)
	  },
	  loadData : function(classid) {
		  this.store.load({
			    params : {
				    classid : classid
			    }
		    })
	  }
  })
