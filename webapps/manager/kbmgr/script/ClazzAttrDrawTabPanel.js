ClazzAttrDrawTabPanel = Ext.extend(Ext.grid.EditorGridPanel, {
	initComponent : function() {
		this.sm = new Ext.grid.RowSelectionModel();
		this.loadMask = true;
		this.columnLines = true;
		this.stripeRows = true;
		this.clazz = null;
		this.nav = null;
		this.border = false;
		this.exact = 0;
		this.viewConfig = { forceFit : true };
		this.columns = [new Ext.grid.RowNumberer(), {	
			dataIndex : 'rule',
			header : '规则名称',
			width : 200
		}, {
			dataIndex : 'type',
			header : '类型',
			renderer : function(value) {
				if (value == 0) {
					return '普通问题'
				} else if (value == 1) {
					return '肯定模板'
				} else if (value == 2) {
					return '否定模板'
				}
			}
		}, {
			dataIndex : 'standardRule',
			header : '标准问模板' 
		}, {
			dataIndex : 'attributeName',
			header : '属性名称'
		}, {
			dataIndex : 'path',
			header : '路径',
			width : 200
		}];
		this.content = new Ext.form.TextField({
			msgTarget : 'side',
			labelWidth : 70,
			anchor : '100%',
			emptyText : '支持回车Enter',
			xtype : 'textfield',
			name : 'name',
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == Ext.EventObject.ENTER) {
						me.store.load();
					}
				}
			}
		});
		this.window = null;
		var me = this;
		this.store = new Ext.data.JsonStore({
			remoteSort : false,
			url : 'ontology-class!findChildrenOntologyRule.action',
			root : 'data',
			fields : [{
				name : 'rule',
				type : 'string'
			}, {
				name : 'type',
				type : 'integer'
			}, {
				name : 'standardRule',
				type : 'string'
			}, {
				name : 'attributeName',
				type : 'string'
			}, {
				name : 'path',
				type : 'string'
			}],
			autoLoad : false,
			listeners : {
					beforeload : function(store, records, options) {
						store.baseParams = {
							name : me.content.getValue(),
							classid : me.clazz,
							exact : me.exact
						};
					}
				}
		});
		this.bbar = new Ext.PagingToolbar({
			store : this.store,
			displayInfo : true,
			pageSize : 20,
			prependButtons : true,
			beforePageText : '第',
			afterPageText : '页，共{0}页',
			displayMsg : '总计{2}个',
			emptyMsg : "没有记录"
		});
		this.tbar = ['规则:', this.content, {
			text : '搜索',
			iconCls : 'icon-search',
			xtype : 'button',
			menu : {
				items : [{
					text : '模糊搜索',
					iconCls : 'icon-search',
					handler : function(menu) {
						me.exact = 0;
						me.store.load();
					}
				}, {
					text : '精确搜索',
					iconCls : 'icon-search',
					handler : function(menu) {
						me.exact = 1;
						me.store.load();
					}
				}]
			}
		}, '-', {
			text : '查看类图',
			iconCls : 'icon-dimension',
			xtype : 'button',
			handler : function() {
				var p = me.nav.showTab(ClassDrawDiagramPanel, 
					'class-diagram-tab', me.ownerCt.title.replace('类属性','类图'), 
					'icon-ontology-class', true);
				p.load(me.clazz);
			}
		}];
		ClazzAttrDrawTabPanel.superclass.initComponent.call(this);
	},
	load : function(clazzId, nav) {
		this.clazz = clazzId;
		this.nav = nav;
		this.store.load();
	}
	
});