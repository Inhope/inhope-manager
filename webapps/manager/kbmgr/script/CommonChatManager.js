CommonChatManager = function(_cfg) {
	var self = this;
	var _fileForm = new Ext.FormPanel({
				layout : "fit",
				frame : true,
				border : false,
				autoHeight : true,
				waitMsgTarget : true,
				defaults : {
					bodyStyle : 'padding:10px'
				},
				margins : '0 0 0 0',
				labelAlign : "left",
				labelWidth : 50,
				fileUpload : true,
				items : [{
					ref : 'statField',
					html : '加载中...',
					xtype : 'fieldset',
					title : '统计信息',
					autoHeight : true,
					style : 'padding:0'
						// items : [{
						// ref:'../',
						// xtype : "label",
						// height : 25,
						// readOnly:true,
						// fieldLabel:'总文档数',
						// value:,
						// anchor : '96%'
						// }]
					}, {
					xtype : 'fieldset',
					title : '选择文件',
					autoHeight : true,
					items : [{
								name : 'uploadFile',
								xtype : "textfield",
								fieldLabel : '文件',
								inputType : 'file',
								allowBlank : false,
								anchor : '96%'
							}, {
								name : 'rebuild',
								xtype : "checkbox",
								inputValue : true,
								checked : true,
								boxLabel : '清空原有数据',
								anchor : '96%'
							}]
				}]
			});
	var _importPanel = new Ext.Panel({
		border : false,
		layout : "fit",
		layoutConfig : {
			animate : true
		},
		items : [_fileForm],
		buttons : [{
			text : "开始导入",
			handler : function() {
				this.disable();
				var btn = this;
				// 开始导入
				var fileForm = _fileForm.getForm();
				if (fileForm.isValid()) {
					fileForm.submit({
						url : 'common-chat-manager!importData.action',
						success : function(form, action) {
							btn.enable();
							if (action.result.success) {
								var result = action.result.data;
								var timer = new ProgressTimer({
											initData : result,
											progressId : 'importCommonChatData',
											boxConfig : {
												title : '正在导入通用聊天内容...'
											},
											finish : function(p, response) {
												self.doStat();
											}
										});
								timer.start();
							}else{
								Ext.Msg.alert('错误','导入失败.'+action.result.message)
							}
						},
						failure : function(form, action) {
							btn.enable();
							Ext.MessageBox.hide();
							Ext.Msg.alert('错误','导入失败.' +action.result.message);
						}
					});
				}
			}
		}, {
			text : "关闭",
			handler : function() {
				self.hide();
			}
		}]
	});

	var cfg = new Ext.Window({
				border : false,
				width : 350,
				title : '通用聊天内容导入',
				defaults : {// 表示该窗口中所有子元素的特性
					border : false
					// 表示所有子元素都不要边框
				},

				plain : true,// 方角 默认

				modal : true,
				plain : true,
				shim : true,

				closeAction : 'hide',
				collapsible : true,// 折叠
				closable : true, // 关闭
				resizable : false,// 改变大小
				draggable : true,// 拖动
				minimizable : false,// 最小化
				maximizable : false,// 最大化

				animCollapse : true,
				constrainHeader : true,
				autoHeight : false,
				items : [_importPanel]
			});
	CommonChatManager.superclass.constructor.call(this, Ext.applyIf(_cfg || {},
					cfg));
	this.doStat = function() {
		Ext.Ajax.request({
			url : 'common-chat-manager!stat.action',
			success : function(response) {
				var result = Ext.decode(response.responseText);
				if (result.success)
					_fileForm.statField.body.dom.innerHTML = '总条数: '
							+ (parseInt(result.message) - 1);
				else
					_fileForm.statField.body.dom.innerHTML = ('查询失败:' + result.message);
			}
		});
	}
	this.on('render', function() {
				this.doStat();
			});
};
Ext.extend(CommonChatManager, Ext.Window, {
			showWindow : function() {
				this.show();
			}
		});