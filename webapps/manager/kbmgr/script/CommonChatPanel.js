CommonChatPanel = function(_cfg) {
	var self = this;
	var _pageSize = 20;

	this.getPageSize = _pageSize;
	var _store = new Ext.data.Store({
		  proxy : new Ext.data.HttpProxy({
			    url : 'common-chat-manager!listHistory.action'
		    }),
		  reader : new Ext.data.JsonReader({
			    idProperty : 'id',
			    root : 'data',
			    fields : [{
				      name : 'id',
				      type : 'string'
			      }, {
				      name : 'question',
				      type : 'string'
			      }, {
				      name : 'answer',
				      type : 'string'
			      }, {
				      name : 'action',
				      type : 'string'
			      }, {
				      name : 'operateUser',
				      type : 'string'
			      }, {
				      name : 'actiontime',
				      type : 'string'
			      }]
		    }),
		  writer : new Ext.data.JsonWriter()
	  });
	var questionText = new Ext.form.TextField({
		  fieldLabel : "通用聊天",
		  name : "word",
		  allowBlank : true,
		  anchor : '96%'
	  });

	var rowNumber;
	var ds = _store;
	var checkboxSelectionModel = new Ext.grid.CheckboxSelectionModel({
		  singleSelect : true
	  })
	var myExpander = new Ext.grid.RowExpander({
		  tpl : new Ext.Template('<p style="color:#999;margin:2px 0px 2px 48px;">{answer}</p>')
	  });

	var myColumnModel = new Ext.grid.ColumnModel([rowNumber = new Ext.grid.RowNumberer(), checkboxSelectionModel, {
		  header : 'id',
		  dataIndex : 'id',
		  hidden : true,
		  sortable : false
	  }, {
		  header : '问题',
		  dataIndex : 'question',
		  hidden : false,
		  sortable : false
	  }, {
		  header : '答案',
		  dataIndex : 'answer',
		  hidden : false,
		  sortable : false
	  }, {
		  header : '操作状态',
		  dataIndex : 'action',
		  sorttable : false,
		  renderer : function(v) {
			  return v == 1 ? '新增' : '删除'
		  }
	  }, {
		  header : '操作人',
		  dataIndex : 'operateUser',
		  sorttable : false
	  }, {
		  header : '操作时间',
		  dataIndex : 'actiontime',
		  sorttable : false
	  }]);

	myColumnModel.defaultSortable = true;
	this.questionText = questionText;

	var myAIWordGridColumnModel = new Ext.grid.ColumnModel([rowNumber = new Ext.grid.RowNumberer(), checkboxSelectionModel, {
		  header : '通用聊天问题',
		  dataIndex : 'question',
		  sortable : false
	  }, {
		  header : '答案',
		  dataIndex : 'answer',
		  sortable : false
	  }]);
	var myAIWordTbar = new Ext.Toolbar({
		  items : [{
			    text : '删除问题',
			    iconCls : 'icon-wordclass-del',
			    handler : function() {
				    var records = checkboxSelectionModel.getSelections();
				    if (records.length == 0) {
					    Ext.Msg.alert('友情提示', '请选择要删除的通用问题!');
					    return;
				    } else {
					    Ext.Msg.confirm("友情提示", "您确定要删除所有选中的通用聊天吗?", function(clickType) {
						      if (clickType == "yes") {
							      var ids = [];
							      Ext.each(records, function(r) {
								        ids.push(r.get("question"))
							        })
							      self.deleteRecord(ids);
						      }
					      });
				    }
			    }

		    }]
	  })
	var commonchatStore = new Ext.data.Store({
		  proxy : new Ext.data.HttpProxy({
			    url : 'common-chat-manager!queryHistory.action'
		    }),
		  reader : new Ext.data.JsonReader({
			    idProperty : 'id',
			    root : 'data',
			    fields : ['question', 'answer']
		    }),
		  writer : new Ext.data.JsonWriter()
	  });
	this.commonchatStore = commonchatStore;
	var gridPanel = new Ext.grid.GridPanel({
		  width : 360,
		  height : 280,
		  region : 'center',
		  border : false,
		  frame : false,
		  store : commonchatStore,
		  loadMask : true,
		  colModel : myAIWordGridColumnModel,
		  tbar : myAIWordTbar,
		  sm : checkboxSelectionModel,
		  viewConfig : {
			  forceFit : true
		  }
	  })
	var questionQueryWin = new Ext.Window({
		  width : 380,
		  height : 300,
		  title : '问题查询',
		  defaults : {
			  border : false
		  },
		  modal : true,
		  plain : true,
		  shim : true,
		  closable : true,
		  closeAction : 'hide',
		  collapsible : true,
		  resizable : false,
		  draggable : true,
		  animCollapse : true,
		  constrainHeader : true,
		  items : [gridPanel]
	  })
	this.questionQueryWin = questionQueryWin;
	var dimbox = Ext.create({
		  width : 140,
		  name : 'dimension',
		  xtype : 'dimensionbox',
		  clearBtn : true,
		  menuConfig : {
			  labelAlign : 'top'
		  }
	  })
	var actionTbar = new Ext.Toolbar({
		  items : ['查询问题:', questionText, '维度:', dimbox, {
			    text : '查询',
			    iconCls : 'icon-wordclass-search',
			    handler : function() {
				    self.questionQueryWin.show();
				    var word = questionText.getValue();
				    var tags = dimbox.tags;
				    var taglist = []
				    if (dimbox.tags && dimbox.tags.length) {
					    Ext.each(dimbox.tags, function(t) {
						      taglist.push(t.name)
					      })
				    }
				    self.queryCommChatQuestion(word, taglist);
			    }
		    }, '-', {
			    text : '添加通用聊天',
			    iconCls : 'icon-wordclass-add',
			    handler : function() {
				    var _formPanel = new Ext.form.FormPanel({
					      frame : false,
					      border : false,
					      labelWidth : 50,
					      // autoHeight : true,
					      height : 120,
					      waitMsgTarget : true,
					      bodyStyle : 'padding : 5px 10px',
					      defaults : {
						      blankText : '',
						      invalidText : '',
						      anchor : '90%',
						      border : false,
						      bodyStyle : 'padding : 0px 1px',
						      defaults : {
							      xtype : 'textfield',
							      anchor : '100%'
						      }
					      },
					      layout : 'column',
					      items : [{
						        layout : 'form',
						        columnWidth : .5,
						        anchor : '100%',
						        items : [{
							          fieldLabel : '问题1',
							          name : 'question1',
							          allowBlank : false
						          }, {
							          fieldLabel : '问题2',
							          name : 'question2'
						          }, {
							          fieldLabel : '问题3',
							          name : 'question3'
						          }]
					        }, {
						        layout : 'form',
						        columnWidth : .5,
						        items : [{
							          fieldLabel : '答案1',
							          name : 'answer1',
							          allowBlank : false
						          }, {
							          fieldLabel : '答案2',
							          name : 'answer2'
						          }, {
							          fieldLabel : '答案3',
							          name : 'answer3'
						          }]
					        }],
					      buttons : [{
						        text : '保存',
						        handler : function() {
							        if (!_formPanel.getForm().isValid()) {
								        return;
							        }
							        var values = _formPanel.getForm().getFieldValues();
							        var faqs = [];
							        for (var i = 1; i <= 3; i++) {
								        if (values['question' + i] && values['answer' + i]) {
									        faqs.push({
										          question : values['question' + i],
										          answer : values['answer' + i]
									          })
								        }
							        }
							        if (faqs.length == 0) {
								        Ext.Msg.alert('提示', '没有有效的问题可以新增');
								        return;
							        }
							        self.addCommChatFaq(faqs);
						        }
					        }, {
						        text : '关闭',
						        handler : function() {
							        _addWin.hide();
							        _formPanel.getForm().reset();
						        }
					        }]
				      });
				    var _addWin = new Ext.Window({
					      width : 380,
					      title : '通用聊天添加',
					      defaults : {
						      border : false
					      },
					      modal : true,
					      plain : true,
					      shim : true,
					      closable : true,
					      closeAction : 'hide',
					      collapsible : true,
					      resizable : true,
					      draggable : true,
					      animCollapse : true,
					      constrainHeader : true,
					      items : [_formPanel]
				      });
				    _addWin.show();
			    }
		    }, {
			    text : '导入数据',
			    iconCls : 'icon-ontology-import',
			    handler : function() {
				    self.importChatData();
			    }
		    }, '-', {
			    text : '导入操作记录',
			    iconCls : 'icon-ontology-import',
			    handler : function() {
				    self.importData();
			    }
		    }, {
			    text : '导出操作记录',
			    iconCls : 'icon-ontology-export',
			    handler : function() {
				    self.exportData();
			    }
		    }, {
			    text : '执行操作记录',
			    iconCls : 'icon-ontology-import',
			    handler : function() {
				    self.exec();
			    }
		    }]
	  });

	var _fileForm = new Ext.FormPanel({
		  layout : "fit",
		  frame : true,
		  border : false,
		  autoHeight : true,
		  waitMsgTarget : true,
		  defaults : {
			  bodyStyle : 'padding:10px'
		  },
		  margins : '0 0 0 0',
		  labelAlign : "left",
		  labelWidth : 50,
		  fileUpload : true,
		  items : [{
			    xtype : 'fieldset',
			    title : '选择文件',
			    autoHeight : true,
			    items : [{
				      name : 'uploadFile',
				      xtype : "textfield",
				      fieldLabel : '文件',
				      inputType : 'file',
				      allowBlank : false,
				      anchor : '96%'
			      }]
		    }]
	  });
	var _importPanel = new Ext.Panel({
		  border : false,
		  layout : "fit",
		  layoutConfig : {
			  animate : true
		  },
		  items : [_fileForm],
		  buttons : [{
			    text : "开始导入",
			    handler : function() {
				    var btn = this;
				    // 开始导入
				    var fileForm = _fileForm.getForm();
				    if (fileForm.isValid()) {
					    fileForm.submit({
						      url : 'common-chat-manager!importHistory.action',
						      success : function(form, action) {
							      btn.enable();
							      var result = action.result.data;
							      var timer = new ProgressTimer({
								        initData : result,
								        progressId : 'importCommonChatHistory',
								        boxConfig : {
									        title : ' 正在导入通用聊天内容...'
								        },
								        finish : function(p, response) {
									        Ext.MessageBox.hide();
									        Ext.Msg.alert('成功提示', '导入成功!');
									        self.questionText.setValue("");
									        self.loadData();
								        }
							        });
							      timer.start();
						      },
						      failure : function(form) {
							      btn.enable();
							      Ext.MessageBox.hide();
							      Ext.Msg.alert('导入失败', '文件上传失败');
						      }
					      });
				    }
			    }
		    }, {
			    text : "关闭",
			    handler : function() {
				    importWindow.hide();
			    }
		    }]
	  });

	var importWindow = new Ext.Window({
		  border : false,
		  width : 350,
		  title : '通用聊天导入',
		  defaults : {// 表示该窗口中所有子元素的特性
			  border : false
			  // 表示所有子元素都不要边框
		  },
		  plain : true,// 方角 默认
		  modal : true,
		  plain : true,
		  shim : true,
		  closeAction : 'hide',
		  collapsible : true,// 折叠
		  closable : true, // 关闭
		  resizable : false,// 改变大小
		  draggable : true,// 拖动
		  minimizable : false,// 最小化
		  maximizable : false,// 最大化
		  animCollapse : true,
		  constrainHeader : true,
		  autoHeight : false,
		  items : [_importPanel]
	  });
	this.importWindow = importWindow;
	var pagingBar = new Ext.PagingToolbar({
		  store : _store,
		  displayInfo : true,
		  pageSize : _pageSize,
		  prependButtons : true,
		  beforePageText : '第',
		  afterPageText : '页，共{0}页',
		  displayMsg : '第{0}到{1}条记录，共{2}条',
		  emptyMsg : "没有记录",
		  prependButtons : false
	  })
	_store.on('beforeload', function() {
		  var word = self.questionText.getValue();
		  Ext.apply(this.baseParams, {
			    word : word
		    });
		  scope : self
	  });
	var cfg = {
		region : 'center',
		border : false,
		frame : false,
		store : _store,
		loadMask : true,
		colModel : myColumnModel,
		tbar : actionTbar,
		sm : checkboxSelectionModel,
		bbar : pagingBar,

		viewConfig : {
			forceFit : true
		}
	}
	CommonChatPanel.superclass.constructor.call(this, Ext.applyIf(_cfg || {}, cfg));

}
Ext.extend(CommonChatPanel, Ext.grid.GridPanel, {
	  loadData : function() {
		  this.getStore().load({
			    params : {
				    start : 0,
				    limit : this.getPageSize
			    }
		    });
	  },
	  deleteRecord : function(ids) {
		  Ext.Ajax.request({
			    url : 'common-chat-manager!delete.action',
			    params : {
				    data : Ext.encode(ids)
			    },
			    success : function(response) {
				    var result = Ext.util.JSON.decode(response.responseText);
				    if (result.success) {
					    Ext.Msg.alert('提示信息', '删除成功!');
					    this.commonchatStore.reload();
					    this.loadData();
				    } else {
					    Ext.Msg.alert('失败', '删除成功!');
				    }
			    },
			    failure : function(response) {
				    Ext.Msg.alert("提示", "出错或者超时!");
			    },
			    scope : this
		    });
	  },
	  queryCommChatQuestion : function(q, tags) {
		  tags ? tags = tags.join(',') : 0
		  this.commonchatStore.load({
			    params : {
				    'data' : q,
				    dim : tags
			    }
		    });
	  },
	  addCommChatFaq : function(faqs) {
		  Ext.Ajax.request({
			    url : 'common-chat-manager!add.action',
			    params : {
				    'data' : Ext.encode(faqs)
			    },
			    success : function(response) {
				    var result = Ext.decode(response.responseText);
				    if (result.success) {
					    Ext.Msg.alert('提示消息', '新增成功');
					    this.questionText.setValue("");
					    this.loadData();
				    } else {
					    Ext.Msg.alert('失败', result);
				    }
			    },
			    failure : function(response) {
				    Ext.Msg.alert("提示", "出错或者超时!");
			    },
			    scope : this
		    });
	  },
	  exec : function() {
		  Ext.Msg.confirm('提示', '确定执行?', function(btn) {
			    if (btn == 'yes') {
				    Ext.Ajax.request({
					      url : 'common-chat-manager!exec.action',
					      success : function(response) {
						      Ext.Msg.alert('提示', '执行成功');
					      },
					      failure : function(response) {
						      Ext.Msg.alert("提示", "出错或者超时!");
					      },
					      scope : this
				      });
			    }
		    });
	  },
	  importChatData : function() {
		  if (!this._importChatDataWin) {
			  this._importChatDataWin = new CommonChatManager();
		  }
		  this._importChatDataWin.show();
	  },
	  importData : function() {
		  this.importWindow.show();
	  },
	  exportData : function() {
		  // 开始下载
		  Ext.Ajax.request({
			    url : 'common-chat-manager!exportHistory.action',
			    success : function(response) {
				    var self = this;
				    var result = Ext.util.JSON.decode(response.responseText);
				    var timer = new ProgressTimer({
					      initData : result.data,
					      progressId : 'exportCommonChatHistory',
					      boxConfig : {
						      title : '正在导出通用聊天...'
					      },
					      finish : function() {
						      if (!self.downloadIFrame) {
							      self.downloadIFrame = self.getEl().createChild({
								        tag : 'iframe',
								        style : 'display:none;'
							        })
						      }
						      self.downloadIFrame.dom.src = 'common-chat-manager!downExportFile.action?_t=' + new Date().getTime();
						      Ext.MessageBox.hide();
					      }
				      });
				    timer.start();
			    },
			    failure : function(response) {
				    Ext.MessageBox.hide();
				    Ext.Msg.alert('错误提示', response.responseText);
			    },
			    scope : this
		    });
	  }
  })