ContextNodePanel = function() {

	var self = this;
	var myid = Ext.id();
	this.myid = myid;
	var _store = new Ext.data.Store({
				proxy : new Ext.data.HttpProxy({
							url : 'context-node!list.action'
						}),
				reader : new Ext.data.JsonReader({
							root : 'data',
							fields : [{
										name : 'name',
										type : 'string'
									}, {
										name : 'wordClassesStr',
										type : 'string',
										convert : function(v) {
											return v.replace(new RegExp(',',
															'g'), '');
										}
									}]
						}),
				listeners : {}
			});
	var _sm = new Ext.grid.RowSelectionModel({
				moveEditorOnEnter : false,
				singleSelect : true
			});
	var fieldReg = /^(\[[^\]]+\])+$/;
	var _dataGrid = new ExcelLikeGrid({
		region : 'west',
		width : 600,
		border : false,
		store : _store,
		columnLines : true,
		clicksToEdit : 1,
		sm : _sm,
		loadMask : true,
		pasteable : true,
		colModel : new Ext.grid.ColumnModel({
					columns : [new Ext.grid.RowNumberer(), {
								header : '节点名称',
								dataIndex : 'name',
								editor : new Ext.Editor({
											autoSize : true,
											field : {
												xtype : 'textarea',
												allowBlank : false,
												blankText : '请输入节点名称'
											}
										})
							}, {
								header : '词类名/词类目录名',
								dataIndex : 'wordClassesStr',
								width : 180,
								editor : new Ext.Editor({
											autoSize : true,
											field : {
												xtype : 'textarea',
												validator : function(val) {
													val = val.trim();
													if (val) {
														if (!fieldReg.test(val))
															return '格式不正确';
													}
												}
											}
										})
							}, new DeleteButtoner()]
				}),
		viewConfig : {
			forceFit : true
		}
	});

	var editR;
	_dataGrid.on('rowclick', function(grid, rowIndex, evt) {
				var row = grid.getSelectionModel().getSelected();
				editR = row;
				if (row)
					_setFormData(row.data);
			}, _dataGrid);

	_store.on("load", function(e) {
				_dataGrid.batchAdd(25);
			});

	_dataGrid.on("scrollbottomclick", function(e) {
				this.batchAdd(25);
			});

	var _wordClassesArea = new Ext.form.TextArea({
				fieldLabel : '词类名/词类目录名',
				name : 'wordClasses',
				anchor : '100% 100%',
				listeners : {
					'blur' : function() {
						if (editR) {
							editR
									.set('wordClassesStr', this.getValue()
													.replace(
															new RegExp('\n',
																	'g'), ''));
						}
					}
				}
			});
	var _nodeNameField = new Ext.form.TextField({
				fieldLabel : '节点名称',
				labelStyle : 'padding : 3px 10px;',
				name : 'name',
				anchor : '88%',
				listeners : {
					'blur' : function() {
						if (editR) {
							editR.set('name', this.getValue());
						}
					}
				}
			});
	var _formPanel = new Ext.form.FormPanel({
				frame : false,
				border : false,
				region : 'center',
				labelWidth : 60,
				waitMsgTarget : true,
				bodyStyle : 'padding : 15px 0px; background-color:' + sys_bgcolor,
				style : 'border-left-width:1px',
				items : [_nodeNameField, {
							xtype : 'fieldset',
							layout : 'form',
							anchor : '100% 100%',
							border : false,
							bodyStyle : 'background-color:' + sys_bgcolor,
							labelAlign : 'top',
							items : [_wordClassesArea]
						}]
			});

	var _setFormData = function(data) {
		_formPanel.getForm().reset();
		if (data.name)
			_nodeNameField.setValue(data.name);
		if (data.wordClassesStr)
			_wordClassesArea.setValue(data.wordClassesStr.replace(new RegExp(
							']', 'g'), ']\n'));
	}

	this.dataGrid = _dataGrid;
	this.formPanel = _formPanel;

	// 定义一个set集合
	var fieldSet = new Ext.form.FieldSet({
				autoHeight : true,
				autoWeight : true,
				defaults : {
					width : 270,
					labelWidth : 30
				},
				items : [{
							xtype : 'fieldset',
							autoHeight : true,
							items : [{
										// 定义一个上传信息的文本框
										id : myid + 'uploadFile',
										name : 'uploadFile',
										xtype : 'textfield',
										fieldLabel : '文件',
										inputType : 'file',
										anchor : '96%',
										allowBlank : false
									}]
						}

				]
			});
	// 定义个formPanel，把set信息添加此中
	var confFormPanel = new Ext.FormPanel({
				region : 'center',
				layout : 'fit',
				enctype : 'multipart/form-data',
				border : false,
				width : 280,
				fileUpload : true,
				items : [fieldSet]
			});
	this.confFormPanel = confFormPanel;
	// 打开窗口
	var configWindow = new Ext.Window({
				width : 300,
				height : 150,
				plain : true,
				border : false,
				title : '节点信息',
				closeAction : 'hide',
				collapsible : true,// 折叠
				closable : true, // 关闭
				resizable : false,// 改变大小
				draggable : true,// 拖动
				minimizable : false,// 最小化
				maximizable : false,// 最大化
				animCollapse : true,
				// layout : 'border',
				// bodyStyle : 'padding:5px;',
				items : [confFormPanel],
				buttonAlign : "center",
				buttons : [{
							id : myid + 'sub_button',
							text : '提交',
							handler : function() {
								self.postConfig(this); // 单击提交时响应事件
							}
						}, {
							text : '重置',
							type : 'reset',
							id : myid + 'clear',
							handler : function() {
								confFormPanel.form.reset();
							}
						}]
			});

	var config = {
		id : 'contextNodePanel',
		layout : 'border',
		border : false,
		tbar : [{
					iconCls : 'icon-save',
					text : '同步保存',
					handler : function() {
						_dataGrid.stopEditing();
						var total = _store.getCount();
						var valPair = [];
						for (var i = 0, j = 0; i < total; i++) {
							var row = _store.getAt(i);
							var name = row.data.name;
							if (name)
								name = name.trim();
							var wordClasses = row.data.wordClassesStr;
							if (!name && !wordClasses)
								continue
							if ((name && !wordClasses)
									|| (wordClasses && !name)) {
								Ext.Msg.alert('错误', '您填写的数据不完整！');
								return false;
							}
							wordClasses = wordClasses.trim();
							if (!fieldReg.test(wordClasses)) {
								Ext.Msg.alert('错误', '节点{' + name
												+ '}所对应的词类名格式不正确！');
								return false;
							}
							valPair[j] = name + '--@--' + wordClasses;
							j++;
						}
						Ext.Ajax.request({
									url : 'context-node!save.action',
									params : {
										data : valPair.join('|')
									},
									success : function(resp) {
										Ext.Msg.alert('提示', '保存成功！',
												function() {
													_store.reload()
												});
									},
									failure : function(resp) {
										Ext.Msg.alert('错误', resp.responseText);
									}
								});
					}
				}, {
					iconCls : 'icon-refresh',
					text : '刷新',
					handler : function() {
						_store.reload();
					}
				}, {
					iconCls : 'icon-ontology-import',
					text : '导入',
					handler : function() {
						configWindow.show();
					}
				}, {
					iconCls : 'icon-ontology-export',
					text : '导出',
					handler : function() {
						self.exportContext();
					}
				}, '-', '<font color="red">编辑后请点击【同步保存】按钮</font>'],
		items : [_dataGrid, _formPanel]
	};
	ContextNodePanel.superclass.constructor.call(this, config);
}

Ext.extend(ContextNodePanel, Ext.Panel, {
	loadData : function() {
		this.dataGrid.getStore().load({})
	},// 提交时要响应的信息
	postConfig : function() {
		var self = this;
		if (this.confFormPanel.getForm().isValid()) {
			var fileName = Ext.getCmp(this.myid + "uploadFile").getValue(); // 得到上传文件名
			var Type = fileName.split("."); // 截取后缀名
			var fileType = Type[Type.length - 1]; // 得到后缀名
			// 判断上传的文件是否为.xml格式的
			if (fileType == "xml") {
				this.confFormPanel.getForm().submit({
					url : 'context-node!importData.action',
					success : function(form, action) {
						var result = action.result.data;
						var timer = new ProgressTimer({
									initData : result,
									progressId : 'importContextNodeStatus',
									boxConfig : {
										title : '正在上传文件...'
									},
									finish : function() {
										Ext.Msg.alert('提示', '上传成功', function() {
													self.loadData();
												})
									}
								});
						timer.start();
					},
					failure : function() {
						Ext.Msg.alert('错误提示', '导入失败');
						Ext.getCmp(self.myid + "sub_button").setDisabled(false);
					},
					scope : this
				});
			} else {
				Ext.Msg.alert('格式错误', '只能上传.xml的文件');
			}
		} else {
			Ext.Msg.alert('错误提示', '地址不能为空!');
		}

	},
	exportContext : function() {
		window.location = 'context-node!exportData.action';
	}
});
