ContextRulePanel = function() {

	var self = this;
	var _pageSize = 20;

	var _store = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({
			url : 'context-rule!list.action'
		}),
		reader : new Ext.data.JsonReader({
			root : 'data',
			fields : [ {
				name : 'rule',
				type : 'object'
			} ]
		}),
		listeners : {}
	});
	_store.on('beforeload', function() {
		addingAsk = false;
	});
	var sm = new Ext.grid.RowSelectionModel();
	var addingAsk = false;
	// 定义一个set集合
	var fieldSet = new Ext.form.FieldSet({
		autoHeight : true,
		autoWeight : true,
		defaults : {
			width : 270,
			labelWidth : 30
		},
		items : [ {
			xtype : 'fieldset',
			autoHeight : true,
			items : [ {
				// 定义一个上传信息的文本框
				id : 'uploadFile',
				name : 'uploadFile',
				xtype : 'textfield',
				fieldLabel : '文件',
				inputType : 'file',
				anchor : '96%',
				allowBlank : false
			} ]
		}

		]
	});
	// 定义个formPanel，把set信息添加此中
	var confFormPanel = new Ext.FormPanel({
		region : 'center',
		layout : 'fit',
		enctype : 'multipart/form-data',
		border : false,
		width : 280,
		fileUpload : true,
		items : [ fieldSet ]
	});
	this.confFormPanel = confFormPanel;
	// 打开窗口
	var configWindow = new Ext.Window({
		width : 300,
		height : 150,
		plain : true,
		border : false,
		title : '规则信息',
		closeAction : 'hide',
		collapsible : true,// 折叠
		closable : true, // 关闭
		resizable : false,// 改变大小
		draggable : true,// 拖动
		minimizable : false,// 最小化
		maximizable : false,// 最大化
		animCollapse : true,
		// layout : 'border',
		// bodyStyle : 'padding:5px;',
		items : [ confFormPanel ],
		buttonAlign : "center",
		buttons : [ {
			id : 'sub_button',
			text : '提交',
			handler : function() {
				self.postConfig(this); // 单击提交时响应事件
			}
		}, {
			text : '重置',
			type : 'reset',
			id : 'clear',
			handler : function() {
				confFormPanel.form.reset();
			}
		} ]
	});
	var config = {
		id : 'contextPanel',
		store : _store,
		stripeRows : true,
		border : false,
		frame : false,
		margins : '0 5 5 5',
		tbar : [ {
			iconCls : 'icon-save',
			text : '保存',
			handler : function() {
				self.save();
			}
		}, {
			iconCls : 'icon-add',
			text : '新建反问',
			handler : function() {
				addingAsk = true;
				var ask = new _store.recordType({
					rule : ''
				});
				_store.insert(0, ask);
			}
		}, {
			iconCls : 'icon-add',
			text : '新建上下文',
			handler : function() {
				addingAsk = false;
				var ask = new _store.recordType({
					rule : ''
				});
				_store.insert(0, ask);
			}
		}, {
			iconCls : 'icon-refresh',
			text : '刷新',
			handler : function() {
				_store.reload();
			}
		}, {
			iconCls : 'icon-ontology-import',
			text : '导入',
			handler : function() {
				configWindow.show();
			}
		}, {
			iconCls : 'icon-ontology-export',
			text : '导出',
			handler : function() {
				self.exportContext();
			}
		}, {
			iconCls : 'icon-ontology-sync',
			text : '重载',
			handler : function() {
				Ext.Msg.confirm('提示', '确认重载？', function(sel) {
					if (sel == 'yes') {
						Ext.Ajax.request({
							url : 'context-rule!reload.action',
							success : function(resp) {
								Ext.Msg.alert('提示', '重载成功！');
							},
							failure : function(resp) {
								Ext.Msg.alert('错误', resp.responseText);
							}
						});
					}
				});
			}
		} ],
		sm : sm,
		loadMask : true,
		colModel : new Ext.grid.ColumnModel(
				{
					columns : [
							{
								header : '上下文及反问列表',
								dataIndex : 'rule',
								renderer : function(value, metaData, record,
										rowIndex, colIndex, store) {
									if (!record.json)
										record.json = {};
									if (record.json.isReverseAsk || addingAsk)
										return FormUtil
												.createAskForm(record.json).innerHTML;
									else
										return FormUtil
												.createCtxForm(record.json).innerHTML;
								}
							},
							{
								header : '操作',
								width : 50,
								dataIndex : 'operations',
								renderer : function(value, metaData, record,
										rowIndex, colIndex, store) {
									return "<a class='_delButton' href='javascript:void(0);' onclick='javscript:return false;'>删除</a>";
								}
							} ]
				}),
		viewConfig : {
			forceFit : true
		}
	};

	var FormUtil = function() {
		var _count = 0;

		function getInputCmp(config) {
			var _label = document.createElement('label');
			_label.className = 'x-form-item-label';
			_label.style.width = (config.labelWidth || 60) + 'px';
			_label.innerHTML = config.label;
			var _field;
			var _fieldVal = config.value ? config.value : '';
			if (config.type == 'input') {
				if (Ext.isIE6 || Ext.isIE7) {
					_field = document
							.createElement('<input name="'
									+ config.name
									+ '" type="text" class="x-form-text x-form-field" value="'
									+ _fieldVal + '" />');
				} else {
					_field = document.createElement('input');
					_field.type = 'text';
					_field.className = 'x-form-text x-form-field';
					if (config.value)
						_field.setAttribute("value", config.value);
				}
			} else if (config.type == 'textarea') {
				if (Ext.isIE6 || Ext.isIE7) {
					_field = document
							.createElement('<textarea name="'
									+ config.name
									+ '" type="text" class="x-form-textarea x-form-field" value="'
									+ _fieldVal + '"></textarea>');
				} else {
					_field = document.createElement('textarea');
					_field.className = 'x-form-textarea x-form-field';
				}
				if (config.value)
					_field.innerHTML = config.value;
			}
			_field.style.width = (config.fieldWidth || 350) + 'px';
			_field.setAttribute('name', config.name);
			var fieldDiv = document.createElement('div');
			fieldDiv.className = 'x-form-element';
			fieldDiv.style.paddingLeft = '65px';

			var clearDiv = document.createElement('div');
			clearDiv.className = 'x-form-clear-left';

			var wrapDiv = document.createElement('div');
			wrapDiv.className = 'x-form-item';
			wrapDiv.style.padding = '3px 0';

			wrapDiv.appendChild(_label);
			wrapDiv.appendChild(fieldDiv);
			wrapDiv.appendChild(fieldDiv);
			wrapDiv.appendChild(clearDiv);
			fieldDiv.appendChild(_field);

			return wrapDiv;
		}

		var getIdField = function(data) {
			var idField = document.createElement('input');
			idField.type = 'hidden';
			idField.name = 'id';
			if (data.id)
				idField.setAttribute('value', data.id);
			return idField;
		}

		return {
			createAskForm : function(data) {
				var wrapDiv = document.createElement('div');
				wrapDiv.style.position = 'relative';
				var finalDiv = document.createElement('div');
				finalDiv.appendChild(wrapDiv);
				wrapDiv.setAttribute('name', '_item');
				wrapDiv.appendChild(getIdField(data));
				wrapDiv.appendChild(getInputCmp({
					label : '反问名称',
					name : 'name',
					value : data.name,
					fieldWidth : 250,
					type : 'input'
				}));
				wrapDiv.appendChild(getInputCmp({
					label : '缺失要素',
					name : 'lack',
					value : data.lack,
					fieldWidth : 250,
					type : 'input'
				}));
				wrapDiv.appendChild(getInputCmp({
					label : '反问句',
					name : 'reverseAsk',
					value : data.reverseAsk,
					type : 'textarea'
				}));
				wrapDiv.appendChild(getInputCmp({
					label : '触发要素',
					name : 'triggers',
					value : data.triggers,
					type : 'input'
				}));
				wrapDiv.appendChild(getInputCmp({
					label : '否定要素',
					name : 'negatives',
					value : data.negatives,
					type : 'input'
				}));
				_count++;
				return finalDiv;
			},
			createCtxForm : function(data) {
				var wrapDiv = document.createElement('div');
				wrapDiv.style.position = 'relative';
				var finalDiv = document.createElement('div');
				finalDiv.appendChild(wrapDiv);
				wrapDiv.setAttribute('name', '_item');
				wrapDiv.appendChild(getIdField(data));
				wrapDiv.appendChild(getInputCmp({
					label : '上下文名称',
					name : 'name',
					value : data.name,
					fieldWidth : 250,
					type : 'input'
				}));
				wrapDiv.appendChild(getInputCmp({
					label : '触发要素',
					name : 'triggers',
					value : data.triggers,
					type : 'input'
				}));
				return finalDiv;
			}
		}
	}();

	var fieldReg = /^((\[[^\]]+\],?)|(\{[^\}]+\}),?)+$/;
	var clearErrors = function(parent) {
		var inputs = parent.getElementsByTagName('input');
		for ( var i = 0; i < inputs.length; i++) {
			inputs[i].className = inputs[i].className.replace(
					' x-form-invalid', '');
		}
		var areas = parent.getElementsByTagName('textarea');
		if (areas.length > 0)
			areas[0].className = areas[0].className.replace(' x-form-invalid',
					'');
	}
	var emptyError = function(field) {
		markError(getLable(field) + '不能为空！', field);
	}
	var formatError = function(field) {
		markError(getLable(field) + '格式不正确！', field);
	}
	var markError = function(msg, field) {
		Ext.Msg.alert('提示', msg, function() {
			field.className += ' x-form-invalid';
		});
	}
	var getLable = function(field) {
		var parent = field.parentNode.parentNode;
		return parent.getElementsByTagName('label')[0].innerHTML;
	}

	this.save = function() {
		Ext.Msg.confirm('提示', '确认保存？', function(sel) {
			if (sel == 'yes') {
				var divs = document.getElementsByTagName('div');
				var forms = [];
				for ( var j = 0; j < divs.length; j++) {
					if (divs[j].getAttribute('name') == '_item')
						forms.push(divs[j]);
				}
				var valArray = [];
				for ( var i = 0; i < forms.length; i++) {
					var _item = forms[i];
					var inputs = _item.getElementsByTagName('input');
					var __addingAsk = false;
					if (inputs.length > 3)
						__addingAsk = true;
					var values = {};
					for ( var j = 0; j < inputs.length; j++) {
						var field = inputs[j];
						var fieldName = field.getAttribute('name');
						var fieldVal = field.value.trim();
						if (fieldName == 'lack' || fieldName == 'triggers') {
							if (!fieldVal) {
								emptyError(field);
								return false;
							}
							if (!fieldReg.test(fieldVal)) {
								formatError(field);
								return false;
							}
						}
						if (fieldName == 'negatives') {
							if (fieldVal && !fieldReg.test(fieldVal)) {
								formatError(field);
								return false;
							}
						}
						values[fieldName] = fieldVal;
					}
					if (__addingAsk) {
						var ask = _item.getElementsByTagName('textarea')[0];
						values[ask.name] = ask.value;
					}
					valArray.push(values);
				}
				Ext.Ajax.request({
					url : 'context-rule!save.action',
					params : {
						data : Ext.encode(valArray)
					},
					success : function(resp) {
						Ext.Msg.alert('提示', '保存成功！', function() {
							_store.reload()
						});
					},
					failure : function(resp) {
						Ext.Msg.alert('错误', resp.responseText);
					}
				});
			}
		});
	}

	ContextRulePanel.superclass.constructor.call(this, config);
	this.on('cellclick', function(grid, row, col, e) {
		var delBtn = e.getTarget('._delButton');
		var rec = this.getStore().getAt(row);
		if (delBtn) {
			var self = this;
			Ext.Msg.confirm('提示', '确认删除？', function(btn) {
				if (btn == 'yes')
					self.getStore().remove(rec);
			})
		}
	}, this);
}

Ext.extend(ContextRulePanel, Ext.grid.GridPanel, {
	loadData : function() {
		this.getStore().load();
	},
	// 提交时要响应的信息
	postConfig : function() {
		var self = this;
		if (this.confFormPanel.getForm().isValid()) {
			var fileName = Ext.getCmp("uploadFile").getValue(); // 得到上传文件名
			var Type = fileName.split("."); // 截取后缀名
			var fileType = Type[Type.length - 1]; // 得到后缀名
			// 判断上传的文件是否为.xml格式的
			if (fileType == "xml") {
				this.confFormPanel.getForm().submit({
					url : 'context-rule!importData.action',
					success : function(form, action) {
						var result = action.result.data;
						var timer = new ProgressTimer({
							initData : result,
							progressId : 'importContextRuleStatus',
							boxConfig : {
								title : '正在上传文件...'
							},
							finish : function() {
								Ext.Msg.alert('提示', '上传成功', function() {
									self.loadData();
								})
							}
						});
						timer.start();
					},
					failure : function() {
						Ext.Msg.alert('错误提示', '导入失败');
						Ext.getCmp("sub_button").setDisabled(false);
					},
					scope : this
				});
			} else {
				Ext.Msg.alert('格式错误', '只能上传.xml的文件');
			}
		} else {
			Ext.Msg.alert('错误提示', '地址不能为空!');
		}

	},
	exportContext : function() {
		window.location = 'context-rule!exportData.action';
	}
});
