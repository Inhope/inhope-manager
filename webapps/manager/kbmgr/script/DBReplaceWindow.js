DBReplaceWindow = function(cfg) {

	var panel = new Ext.form.FormPanel({
		  anchor : '100% 100%',
		  frame : true,
		  labelWidth : 60,
		  items : [{
			    xtype : 'combo',
			    hiddenName : 'replaceTarget',
			    fieldLabel : '替换对象',
			    mode : 'local',
			    store : new Ext.data.JsonStore({
				      url : 'replace-field!getconfig.action',
				      idProperty : 'fieldname',
				      fields : ['fieldname', 'name'],
				      root : 'data',
				      autoLoad:true
			      }),
			    anchor : '100%',
			    valueField : 'fieldname',
			    displayField : 'name',
			    triggerAction : 'all',
			    editable : false
		    }, {
			    xtype : 'textfield',
			    name : 'matchText',
			    fieldLabel : '查找内容',
			    anchor : '100%'

		    }, {
			    xtype : 'textfield',
			    name : 'replaceText',
			    fieldLabel : '替换内容',
			    anchor : '100%'
		    }, {
			    xtype : 'fieldset',
			    title : '选项',
			    anchor : '100%',
			    hideLabel : true,
			    labelWidth : 1,
			    style : 'padding:0;margin:0;',
			    items : {
				    xtype : 'checkboxgroup',
				    anchor : '100% 100%',
				    height : 18,
				    columns : 3,
				    items : [{
					      xtype : 'checkbox',
					      name : 'bIgnoreCase',
					      boxLabel : '不区分大小写'
				      }, {
					      xtype : 'checkbox',
					      name : 'bRegexp',
					      boxLabel : '正则表达式'
				      }]
			    }
		    }],
		  buttons : [{
			    text : '替换',
			    handler : function() {
				    var v = panel.getForm().getValues();
				    if (Ext.isEmpty(v.matchText)) {
					    Ext.Msg.alert('查找内容不能为空!');
					    return;
				    }
				    if (Ext.isEmpty(v.replaceText)) {
					    Ext.Msg.alert('替换内容不能为空!');
					    return;
				    }

				    Ext.Ajax.request({
					      url : 'replace-field!replace.action',
					      params : {
						      fieldname : v.replaceTarget,
						      searchText : v.matchText,
						      replacement : v.replaceText
					      },
					      success : function(response) {
						      if (response && response.responseText) {
							      var progress = Ext.decode(response.responseText).data;
							      var pgTimer = new ProgressTimer({
								        initData : progress,
								        progressText : 'percent',
								        progressId : 'replaceField',
								        boxConfig : {
									        title : '替换'
								        },
								        scope : this
							        });
							      pgTimer.start();
						      }
					      },
					      scope : this
				      })
			    },
			    scope : this
		    }, {
			    text : '取消',
			    handler : function() {
				    this.close();
			    },
			    scope : this
		    }]
	  });

	DBReplaceWindow.superclass.constructor.call(this, {
		  title : '替换',
		  plain : true,
		  modal : true,
		  width : 350,
		  height : 216,
		  layout : 'fit',
		  maximizable : true,
		  collapsible : true,
		  items : panel,
		  iconCls : 'icon-text-replace',
		  bodyStyle : 'padding: 0 1px 1px 0px;',
		  listeners : {
			  show : function() {

			  }
		  }
	  });
}

Ext.extend(DBReplaceWindow, Ext.Window, {

});
ShortcutToolRegistry.register('replaceField',function(){
new DBReplaceWindow().show()
})

