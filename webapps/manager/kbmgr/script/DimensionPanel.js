DimensionPanel = function(cfg) {
	cfg = cfg || {};
	var self = this;

	this.data = {
		tags : []
	};
	this.dimId;
	var platformSupported = window.supportPlatforms && window.supportPlatforms.length > 0;
	this.DimensionRecord = Ext.data.Record.create(['id', 'code', 'name', 'priority']);
	this.dimensionProxy = new LocalProxy({});
	var dimensionEditGrid = this.dimensionEditGrid = new ExcelLikeGrid({
		  flex : 5,
		  autoExpandColumn : 'name',
		  style : 'border-right: 1px solid ' + sys_bdcolor,
		  border : false,
		  clicksToEdit : 1,
		  frame : false,
		  columnLines : true,
		  selModel : new Ext.grid.RowSelectionModel(),
		  subgridLoader : {
			  load : self.loadTags,
			  scope : self
		  },
		  pasteable : true,
		  colModel : new Ext.grid.ColumnModel({
			    getOwnerPanel : function() {
				    return dimensionEditGrid
			    },
			    columns : [new Ext.grid.RowNumberer(), {
				      header : '维度代码',
				      id : 'code',
				      dataIndex : 'code',
				      width : 120,
				      editor : new Ext.form.TextField()
			      }, {
				      header : '维度名称',
				      id : 'name',
				      dataIndex : 'name',
				      width : 120,
				      editor : new Ext.form.TextField()
			      }, {
				      header : '顺序',
				      dataIndex : 'priority',
				      width : 50,
				      editor : new Ext.form.TextField()
			      }, new DeleteButtoner({
				        getOwnerPanel : function() {
					        return dimensionEditGrid
				        },
				        isDeleteable : function(value, metaData, r, row, col, store) {
					        if (platformSupported && r.get('code') == 'platform')
						        return false;
				        }
			        })],
			    isCellEditable : function(col, row) {
				    if (platformSupported) {
					    var r = this.getOwnerPanel().getStore().getAt(row);
					    var column = this.getColumnAt(col);
					    if (column.dataIndex == 'code' && r.get('code') == 'platform') {
						    return false;
					    }
				    }
				    return Ext.grid.ColumnModel.prototype.isCellEditable.call(this, col, row);
			    }
		    }),
		  store : new Ext.data.Store({
			    proxy : self.dimensionProxy,
			    reader : new Ext.data.JsonReader({
				      idProperty : 'id',
				      root : 'data',
				      fields : self.DimensionRecord
			      }),
			    writer : new Ext.data.JsonWriter()
		    })
	  });

	this.TagRecord = Ext.data.Record.create(['id', 'tag', 'name']);
	this.tagProxy = new LocalProxy({});
	var pluginOpCol = new OperationColumn({
		  operationConfig : [{
			    name : '删除',
			    handler : function(grid, rowIndex, columnIndex, e) {
				    grid.getStore().removeAt(rowIndex)
			    }
		    }, {
			    name : '上移',
			    handler : function(grid, rowIndex, columnIndex, e) {
				    if (rowIndex > 0) {
					    var r = grid.getStore().getAt(rowIndex)
					    grid.getStore().removeAt(rowIndex)
					    r.markDirty()
					    grid.getStore().insert(rowIndex - 1, r)
					    grid.getView().refreshRow(rowIndex - 1)
					    grid.getView().refreshRow(rowIndex)
				    }
			    }
		    }, {
			    name : '下移',
			    handler : function(grid, rowIndex, columnIndex, e) {
				    if (rowIndex < grid.getStore().getCount() - 1) {
					    var r = grid.getStore().getAt(rowIndex + 1)
					    grid.getStore().removeAt(rowIndex + 1)
					    r.markDirty()
					    grid.getStore().insert(rowIndex, r)
					    grid.getView().refreshRow(rowIndex + 1)
					    grid.getView().refreshRow(rowIndex)
				    }
			    }
		    }]
	  })
	var tagEditGrid = this.tagEditGrid = new ExcelLikeGrid({
		  autoExpandColumn : 'name',
		  anchor : '100% -60',
		  border : true,
		  clicksToEdit : 1,
		  frame : false,
		  columnLines : true,
		  plugins : [pluginOpCol],
		  selModel : new Ext.grid.RowSelectionModel(),
		  pasteable : true,
		  colModel : new Ext.grid.ColumnModel({
			    getOwnerPanel : function() {
				    return tagEditGrid
			    },
			    columns : [new Ext.grid.RowNumberer(), {
				      header : 'TagID',
				      dataIndex : 'tag',
				      id : 'tag',
				      width : 120,
				      editor : new Ext.form.TextField()
			      }, {
				      header : 'Tag名称',
				      dataIndex : 'name',
				      id : 'name',
				      width : 120,
				      editor : new Ext.form.TextField()
			      }, pluginOpCol],
			    isCellEditable : function(col, row) {
				    if (platformSupported && this.getOwnerPanel().getStore().proxy.data['code'] == 'platform') {
					    var r = this.getOwnerPanel().getStore().getAt(row);
					    var column = this.getColumnAt(col);
					    if (column.dataIndex == 'tag') {
						    return false;
					    }
				    }
				    return Ext.grid.ColumnModel.prototype.isCellEditable.call(this, col, row);
			    }
		    }),
		  store : new Ext.data.Store({
			    proxy : self.tagProxy,
			    reader : new Ext.data.JsonReader({
				      idProperty : 'id',
				      root : 'tags',
				      fields : self.TagRecord
			      }),
			    writer : new Ext.data.JsonWriter()
		    })
	  });

	var tagForm = this.tagForm = new Ext.form.FormPanel({
		  flex : 3,
		  margins : '0 0 0 5',
		  padding : 8,
		  style : 'border-left: 1px solid ' + sys_bdcolor,
		  border : false,
		  frame : false,
		  labelAlign : 'top',
		  bodyStyle : 'background-color:' + sys_bgcolor,
		  items : [{
			    xtype : 'textfield',
			    anchor : '100%',
			    fieldLabel : '维度名称',
			    ref : 'dimensionNameField',
			    listeners : {
				    blur : function() {
					    var r = dimensionEditGrid.getSelectionModel().getSelected();
					    if (r)
						    r.set("name", this.getValue())
				    }
			    }
		    }, tagEditGrid]
	  });

	var _importForm = new Ext.FormPanel({
		  region : 'center',
		  layout : 'fit',
		  frame : false,
		  enctype : 'multipart/form-data',
		  bodystyle : 'padding:5px 5px 0',
		  width : 300,
		  height : 100,
		  labelWidth : 40,
		  fileUpload : true,
		  items : [{
			    xtype : 'fieldset',
			    title : '选择文件',
			    autoHeight : true,
			    items : [{
				      border : false,
				      id : 'demensionDataImport',
				      xtype : 'textfield',
				      name : 'dimensionFile',
				      fieldLabel : '文件',
				      allowBlank : false,
				      inputType : 'file',
				      anchor : '96%'
			      }]
		    }]
	  });

	var _importWindow = new Ext.Window({
		  width : 310,
		  height : 150,
		  plain : true,
		  shim : true,
		  title : '维度导入',
		  closeAction : 'hide',
		  closable : true,
		  draggable : true,
		  collapsible : true,// 折叠
		  animCollapse : true,
		  constrainHeader : true,
		  items : [_importForm],
		  buttonAlign : "center",
		  buttons : [{
			    text : '提交',
			    handler : function() {
				    self.importExcel();
			    }
		    }, {
			    text : '清除',
			    handler : function() {
				    _importForm.form.reset();
			    }

		    }]
	  });

	this.importForm = _importForm;
	this.importWindow = _importWindow;

	this.groupRecord = Ext.data.Record.create(['id', 'name', 'dimId', 'dimTagIds']);
	this.groupProxy = new LocalProxy({});
	var groupEditGrid = this.groupEditGrid = new ExcelLikeGrid({
		  region : 'center',
		  clicksToEdit : 1,
		  columnLines : true,
		  plugins : [pluginOpCol],
		  border : false,
		  selModel : new Ext.grid.RowSelectionModel({
			    singleSelect : true
		    }),
		  // subgridLoader : {
		  // load : self.loadGroupTags,
		  // scope : self
		  // },
		  pasteable : true,
		  colModel : new Ext.grid.ColumnModel({
			    getOwnerPanel : function() {
				    return groupEditGrid;
			    },
			    columns : [new Ext.grid.RowNumberer(), {
				      header : 'id',
				      dataIndex : 'id',
				      hidden : true
			      }, {
				      header : '组别名称',
				      dataIndex : 'name',
				      editor : new Ext.form.TextField()
			      }, pluginOpCol]
		    }),
		  store : new Ext.data.Store({
			    proxy : self.groupProxy,
			    reader : new Ext.data.JsonReader({
				      idProperty : 'id',
				      root : 'data',
				      fields : self.groupRecord
			      }),
			    writer : new Ext.data.JsonWriter()
		    }),
		  listeners : {
			  'rowclick' : function() {
				  self.loadGroupTags();
			  }
		  },
		  viewConfig : {
			  forceFit : true
		  }
	  });

	groupEditGrid.on("scrollbottomclick", function(e) {
		  groupEditGrid.batchAdd(10);
	  });

	groupEditGrid.on("shiftright", function(e) {
		  var g = groupEditGrid
		  var col = g.findMostLeftEditCol()
		  g.getSelectionModel().selectRow(0)
		  g.stopEditing()
		  g.startEditing(0, col)
	  });

	this.unSelectTagProxy = new LocalProxy({});
	var _unSelectedTagStore = new Ext.data.Store({
		  proxy : self.unSelectTagProxy,
		  reader : new Ext.data.JsonReader({
			    idProperty : 'id',
			    root : 'data',
			    fields : [{
				      name : 'id',
				      type : 'string'
			      }, {
				      name : 'tag',
				      type : 'string'
			      }, {
				      name : 'name',
				      type : 'string'
			      }]
		    }),
		  writer : new Ext.data.JsonWriter()
	  });

	var _unSelectedTagsm = new Ext.grid.CheckboxSelectionModel({});

	var unSelectedPanel = new Ext.grid.GridPanel({
		  title : '待选维度',
		  region : 'west',
		  width : 400,
		  border : false,
		  style : 'border-right:1px solid ' + sys_bdcolor,
		  store : _unSelectedTagStore,
		  loadMask : true,
		  colModel : new Ext.grid.ColumnModel({
			    defaults : {
				    sortable : false
			    },
			    columns : [new Ext.grid.RowNumberer(), _unSelectedTagsm, {
				      header : 'id',
				      dataIndex : 'id',
				      hidden : true
			      }, {
				      header : 'TagId',
				      dataIndex : 'tag'
			      }, {
				      header : 'Tag名称',
				      dataIndex : 'name'
			      }]
		    }),
		  tbar : [{
			    text : '添加到我的维度',
			    iconCls : 'icon-right',
			    handler : function() {
				    self.addMyTags();
			    }
		    }],
		  sm : _unSelectedTagsm,
		  viewConfig : {
			  forceFit : true
		  }
	  });
	this.unSelectedPanel = unSelectedPanel;

	this.myTagProxy = new LocalProxy({});
	var _myTagStore = new Ext.data.Store({
		  proxy : self.myTagProxy,
		  reader : new Ext.data.JsonReader({
			    idProperty : 'id',
			    root : 'data',
			    fields : [{
				      name : 'id',
				      type : 'string'
			      }, {
				      name : 'tag',
				      type : 'string'
			      }, {
				      name : 'name',
				      type : 'string'
			      }]
		    }),
		  writer : new Ext.data.JsonWriter()
	  });

	var _myTagsm = new Ext.grid.CheckboxSelectionModel({});

	var myTagPanel = new Ext.grid.GridPanel({
		  title : '我的维度',
		  region : 'center',
		  frame : false,
		  border : false,
		  store : _myTagStore,
		  loadMask : true,
		  colModel : new Ext.grid.ColumnModel({
			    defaults : {
				    sortable : true
			    },
			    columns : [new Ext.grid.RowNumberer(), _myTagsm, {
				      header : 'id',
				      dataIndex : 'id',
				      sortable : false,
				      hidden : true
			      }, {
				      header : 'TagId',
				      dataIndex : 'tag',
				      sortable : false
			      }, {
				      header : 'Tag名称',
				      dataIndex : 'name',
				      sortable : false
			      }]
		    }),
		  tbar : [{
			    text : '删除',
			    iconCls : 'icon-left',
			    handler : function() {
				    self.removeMyTags();
			    }
		    }],
		  sm : _myTagsm,
		  viewConfig : {
			  forceFit : true
		  }
	  });
	this.myTagPanel = myTagPanel;
	var _groupWindow = new Ext.Window({
		  width : 900,
		  height : 600,
		  plain : true,
		  shim : true,
		  title : '设置组别',
		  closeAction : 'hide',
		  closable : true,
		  draggable : true,
		  collapsible : true,// 折叠
		  animCollapse : true,
		  constrainHeader : true,
		  layout : 'fit',
		  items : [{
			    border : false,
			    anchor : '100% 100%',
			    layout : 'border',
			    defaults : {
				    split : true
			    },
			    items : [groupEditGrid, {
				      region : 'south',
				      layout : 'border',
				      height : 300,
				      border : false,
				      items : [unSelectedPanel, myTagPanel]
			      }]
		    }],
		  tbar : [{
			    text : '保存',
			    iconCls : 'icon-table-save',
			    handler : function() {
				    self.saveGroup();
			    }
		    }]
	  });

	this.groupWindow = _groupWindow;

	DimensionPanel.superclass.constructor.call(this, {
		  border : false,
		  frame : false,
		  // bodyStyle : 'background-color:' + sys_bgcolor,
		  // style : 'border-top: 1px solid ' + sys_bdcolor,
		  layout : {
			  type : 'hbox',
			  align : 'stretch'
		  },
		  items : [dimensionEditGrid, tagForm],
		  buttonAlign : 'center',
		  tbar : [{
			    text : '刷新',
			    iconCls : 'icon-refresh',
			    handler : function() {
				    self.loadData();
			    }
		    }, {
			    text : '导入',
			    iconCls : 'icon-ontology-import',
			    handler : function() {
				    _importWindow.show();
			    }
		    }, {
			    text : '导出',
			    iconCls : 'icon-ontology-export',
			    handler : function() {
				    self.exportExcel();
			    }
		    }, {
			    text : '保存',
			    iconCls : 'icon-table-save',
			    handler : function() {
				    self.save();
			    }
		    }, {
			    text : '设置组别',
			    iconCls : 'icon-attr-link',
			    handler : function() {
				    var r = dimensionEditGrid.getSelectionModel().getSelections();
				    if (r.length == 1) {
					    var sRow = dimensionEditGrid.getSelectionModel().getSelected();
					    self.setGroup(sRow.data);
				    } else
					    Ext.Msg.alert('提示', '请选择你要设置组别的维度');
			    }
		    }]
	  });

	dimensionEditGrid.on("scrollbottomclick", function(e) {
		  dimensionEditGrid.batchAdd(28);
	  });
	tagEditGrid.on("scrollbottomclick", function(e) {
		  tagEditGrid.batchAdd(18);
	  });

	dimensionEditGrid.on("shiftright", function(e) {
		  var g = tagEditGrid
		  var col = g.findMostLeftEditCol()
		  g.getSelectionModel().selectRow(0)
		  this.stopEditing()
		  g.startEditing(0, col)
	  });
	tagEditGrid.on("shiftleft", function(e) {
		  var g = dimensionEditGrid
		  var col = g.findMostRightEditCol()
		  var r = g.getSelectionModel().getSelected()
		  var row = g.getStore().indexOf(r)
		  this.stopEditing()
		  g.startEditing(row, col)
	  });

};

Ext.extend(DimensionPanel, Ext.Panel, {

	  loadData : function() {
		  Ext.Ajax.request({
			    url : 'ontology-dimension!list.action',
			    success : function(form, action) {
				    this.data = Ext.util.JSON.decode(form.responseText);
				    this._refresh();
			    },
			    failure : function(form, action) {
				    Ext.Msg.alert('提示', '获取维度列表失败');
			    },
			    scope : this
		    });
	  },
	  loadTags : function(record) {
		  this.tagForm.dimensionNameField.setValue(record.data.name)
		  if (!record.data.tags) {
			  record.data.tags = [];
		  }
		  this.tagProxy.data = record.data
		  this.tagEditGrid.getStore().reload();
		  this.tagEditGrid.batchAdd(18);
		  if (this.groupWindow.isVisible()) {
			  if (record.data.id) {
				  this.loadMyGroups(record.data.id, record.data.id != this.dimId);
				  this.dimId = record.data.id;
			  } else {
				  Ext.Msg.alert('提示', '请先保存你的维度信息后设置组别');
				  return;
			  }
		  }
	  },
	  _refresh : function() {
		  this.dimensionProxy.data = this.data;
		  var grid = this.dimensionEditGrid;
		  grid.getStore().load();
		  grid.batchAdd(28);
		  var i = grid.defaultSelected
		  if (!i)
			  i = 0;
		  grid.getSelectionModel().selectRow(i);
	  },
	  save : function() {
		  Ext.each(this.data.data, function(dd) {
			    var tags = []
			    Ext.each(dd.tags, function(dt) {
				      for (var key in dt) {
					      if (dt[key]) {
						      tags.push(dt)
						      break;
					      }
				      }
				      dd.tags = tags;
			      })
		    })
		  this.getEl().mask('保存...')
		  Ext.Ajax.request({
			    url : 'ontology-dimension!save.action',
			    params : {
				    data : Ext.encode(this.data.data)
			    },
			    success : function(form, action) {
				    this.getEl().unmask()
				    Dashboard.setAlert("保存成功!");
				    var data = Ext.util.JSON.decode(form.responseText);
				    this.loadData()
			    },
			    failure : function(response) {
				    this.getEl().unmask()
				    Ext.Msg.alert("错误", response.responseText);
			    },
			    scope : this
		    });
	  },
	  exportExcel : function() {
		  Ext.Ajax.request({
			    url : 'ontology-dimension!export.action',
			    success : function(response) {
				    result = Ext.util.JSON.decode(response.responseText);
				    var timer = new ProgressTimer({
					      initData : result.data,
					      progressId : 'dimensionExportStatus',
					      boxConfig : {
						      title : '正在准备文件'
					      },
					      finish : function() {
						      if (!this.downloadIFrame) {
							      this.downloadIFrame = this.getEl().createChild({
								        tag : 'iframe',
								        style : 'display:none;'
							        });
						      }
						      this.downloadIFrame.dom.src = 'ontology-dimension!exportExcel.action?&_t' + new Date().getTime();
					      },
					      scope : this
				      });
				    timer.start();
			    },
			    failure : function(response) {
				    Ext.Msg.alert('请求异常', response.responseText);
			    },
			    scope : this
		    });
	  },
	  importExcel : function() {
		  if (Ext.getCmp('demensionDataImport').getValue() != null && Ext.getCmp('demensionDataImport').getValue()) {
			  if (this.importForm.isVisible()) {
				  var self = this;
				  this.importForm.getForm().submit({
					    url : 'ontology-dimension!importExcel.action',
					    success : function(form, action) {
						    var result = action.result.data;
						    var timer = new ProgressTimer({
							      initData : result,
							      progressId : 'importDimensionExcelStatus',
							      boxconfig : {
								      title : '维度导入'
							      },
							      finish : function(p, response) {
								      self.loadData();
							      }
						      });
						    timer.start();
					    },
					    failure : function(form) {
						    Ext.Msg.alert('异常提示', '连接失败');
					    },
					    scope : this
				    });
			  }
		  } else {
			  Ext.Msg.alert('错误提示', '提交文件不能为空');
		  }
	  },
	  destroy : function() {
		  DimensionPanel.superclass.destroy.call(this)
		  if (this.importWindow) {
			  this.importWindow.destroy();
			  delete this.importWindow
		  }
	  },
	  setGroup : function(dim) {
		  var self = this;
		  var dimId = dim.id;
		  if (dimId) {
			  self.loadMyGroups(dimId, dimId != self.dimId);
			  self.dimId = dimId;
		  } else {
			  Ext.Msg.alert('提示', '请先保存你的维度信息后设置组别');
			  return;
		  }
		  this.groupWindow.show();
	  },
	  loadMyGroups : function(dimId, unSelectLoad) {
		  var self = this;
		  self.dimId = dimId;
		  Ext.Ajax.request({
			    url : 'ontology-dimension-group!getGroup.action',
			    params : {
				    'dimId' : self.dimId
			    },
			    success : function(form, action) {
				    self.groupWindow.getEl().unmask();
				    var obj = Ext.util.JSON.decode(form.responseText);
				    if (obj.success) {
					    if (!obj.data)
						    obj.data = [];
					    self.groupProxy.data = obj;
					    self.groupEditGrid.getStore().reload();
					    self.groupEditGrid.batchAdd(10);
					    self.loadGroupTags(null, unSelectLoad);
				    } else
					    Ext.Msg.alert("错误", obj.message);
			    },
			    failure : function(response) {
				    self.groupWindow.getEl().unmask()
				    Ext.Msg.alert("错误", response.responseText);
			    }
		    });
	  },
	  loadGroupTags : function(r, unSelectLoad) {
		  var self = this;
		  if (unSelectLoad)
			  self.loadUnSelectedTag(self.dimId);
		  var gRow = self.groupEditGrid.getSelectionModel().getSelected();
		  self.loadMyTags((gRow && gRow.data) ? gRow.data.dimTagIds ? gRow.data.dimTagIds : '' : '');
	  },
	  loadUnSelectedTag : function(dimId) {
		  var self = this;
		  Ext.Ajax.request({
			    url : 'ontology-dimension-group!unSelectedTag.action',
			    params : {
				    'dimId' : dimId
			    },
			    success : function(form, action) {
				    var obj = Ext.util.JSON.decode(form.responseText);
				    if (obj.success) {
					    if (!obj.data)
						    obj.data = [];
					    self.unSelectTagProxy.data = obj;
					    self.unSelectedPanel.getStore().reload();
				    } else
					    Ext.Msg.alert("错误", obj.message);
			    },
			    failure : function(response) {
				    self.groupWindow.getEl().unmask()
				    Ext.Msg.alert("错误", response.responseText);
			    }
		    });
	  },
	  loadMyTags : function(tagIds) {
		  var self = this;
		  Ext.Ajax.request({
			    url : 'ontology-dimension-group!myTags.action',
			    params : {
				    'tagIds' : tagIds
			    },
			    success : function(form, action) {
				    var obj = Ext.util.JSON.decode(form.responseText);
				    if (obj.success) {
					    if (!obj.data)
						    obj.data = [];
					    self.myTagProxy.data = obj;
					    self.myTagPanel.getStore().reload();
				    } else
					    Ext.Msg.alert("错误", obj.message);
			    },
			    failure : function(response) {
				    self.groupWindow.getEl().unmask()
				    Ext.Msg.alert("错误", response.responseText);
			    }
		    });
	  },
	  addMyTags : function() {
		  var r = this.groupEditGrid.getSelectionModel().getSelections();
		  if (r.length == 1) {
			  if (r[0].data.name) {
				  var records = this.unSelectedPanel.getSelectionModel().getSelections();
				  if (records.length > 0) {
					  this.unSelectedPanel.getStore().remove(records);
					  var data = this.myTagProxy.data;
					  Ext.each(records, function(r) {
						    data.data.push(r.data);
					    })
					  this.myTagProxy.setData(data);
					  this.myTagPanel.getStore().reload();
					  var addTagIds = [];
					  Ext.each(records, function(rc) {
						    addTagIds.push(rc.data.id);
					    })
					  r[0].data.dimTagIds = (r[0].data.dimTagIds ? r[0].data.dimTagIds : '') + (addTagIds.length > 0 ? "," : "") + addTagIds.join(",");
				  } else {
					  Ext.Msg.alert('提示', '请选择要添加的维度信息');
					  return;
				  }
			  } else {
				  Ext.Msg.alert('提示', '选择的组别信息不能为空');
				  return;
			  }
		  } else {
			  Ext.Msg.alert('提示', '请选择你的组别');
			  return;
		  }
	  },
	  removeMyTags : function() {
		  var records = this.myTagPanel.getSelectionModel().getSelections();
		  if (records.length > 0) {
			  this.myTagPanel.getStore().remove(records);
			  var data = this.unSelectTagProxy.data;
			  Ext.each(records, function(r) {
				    data.data.push(r.data);
			    })
			  this.unSelectTagProxy.setData(data);
			  this.unSelectedPanel.getStore().reload();
			  var r = this.groupEditGrid.getSelectionModel().getSelections();
			  var myTagIds = r[0].data.dimTagIds;
			  var tagIdArr = myTagIds.split(",");
			  Ext.each(records, function(r) {
				    if (r.data.id)
					    tagIdArr.remove(r.data.id);
			    })
			  r[0].data.dimTagIds = tagIdArr.join(",");
		  } else {
			  Ext.Msg.alert('提示', '请选择要删除的维度信息');
			  return;
		  }
	  },
	  saveGroup : function() {
		  var self = this;
		  var records = this.groupEditGrid.getStore().getRange();
		  if (records.length > 0) {
			  self.groupWindow.getEl().mask('保存...')
			  var data = [];
			  var priority = 0;
			  var names = [];
			  var isValidErr = false;
			  Ext.each(records, function(re) {
				    if (re.data.name) {
					    if (names.indexOf(re.data.name) == -1)
						    names.push(re.data.name);
					    else {
						    self.groupWindow.getEl().unmask();
						    Ext.Msg.alert('提示', '组别名称不能重复');
						    isValidErr = true;
						    return false;
					    }
					    if (!re.data.dimId)
						    re.data.dimId = self.dimId;
					    if (re.data.id < 32)
						    re.data.id = '';
					    priority++;
					    re.data.priority = priority;
					    data.push(re.data);
				    }
			    });
			  if (isValidErr)
				  return;
			  Ext.Ajax.request({
				    url : 'ontology-dimension-group!save.action',
				    params : {
					    'dimId' : self.dimId,
					    data : Ext.encode(data)
				    },
				    success : function(form, action) {
					    self.groupWindow.getEl().unmask();
					    var obj = Ext.util.JSON.decode(form.responseText);
					    if (obj.success) {
						    Dashboard.setAlert("保存成功!");
						    self.loadMyGroups(self.dimId, true);
					    } else
						    Ext.Msg.alert("错误", obj.message);
				    },
				    failure : function(response) {
					    self.groupWindow.getEl().unmask()
					    Ext.Msg.alert("错误", response.responseText);
				    }
			    });
		  } else {
			  Ext.Msg.alert("提示", "保存的内容不能为空");
		  }
	  }
  });