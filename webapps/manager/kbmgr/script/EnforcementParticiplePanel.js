EnforcementParticiplePanel = function() {

	var self = this;
	var _pageSize = 25;

	var store = new Ext.data.Store({
		  proxy : new Ext.data.HttpProxy({
			    api : {
				    read : 'enforcement-participle!list.action',
				    create : 'enforcement-participle!invalid.action',
				    update : 'enforcement-participle!invalid.action',
				    destroy : 'enforcement-participle!delete.action'
			    }
		    }),
		  reader : new Ext.data.JsonReader({
			    idProperty : 'id',
			    root : 'data',
			    fields : [{
				      name : 'participle',
				      type : 'string'
			      }]
		    }),
		  writer : new Ext.data.JsonWriter({
			    writeAllFields : true
		    }),
		  listeners : {
			  beforewrite : function(store, data, rs) {
				  if (typeof(rs)== 'Object' && !rs.get('participle'))
					  return false;
			  }
		  }
	  });
	var editor = new Ext.ux.grid.RowEditor({
		  saveText : '保存',
		  cancelText : '取消',
		  errorSummary : false
	  });
	editor.on({
		  scope : this,
		  afteredit : function(roweditor, changes, record, rowIndex) {
			  if (!self.isEnAndNum(record.data.participle)) {
				  Ext.Msg.alert("错误", '分词内容只能是字母或者数字，必须以","分割');
				  return false;
			  }
			  Ext.Ajax.request({
				    url : 'enforcement-participle!save.action',
				    params : {
					    data : Ext.encode(record.data)
				    },
				    success : function(resp) {
					    var ret = Ext.util.JSON.decode(resp.responseText);
					    if (ret.success) {
						    store.commitChanges();
						    store.reload();
					    } else
						    Ext.Msg.alert('提示', ret.message);
				    }
			    });
		  }
	  });
	editor.on('canceledit', function(rowEditor, changes, record, rowIndex) {
		  var r, i = 0;
		  while ((r = store.getAt(i++))) {
			  if (r.phantom) {
				  store.remove(r);
			  }
		  }
	  });
	var sm = new Ext.grid.CheckboxSelectionModel();
	var pagingBar = new Ext.PagingToolbar({
		  store : store,
		  displayInfo : true,
		  pageSize : _pageSize,
		  prependButtons : true,
		  beforePageText : '第',
		  afterPageText : '页，共{0}页',
		  displayMsg : '第{0}到{1}条记录，共{2}条',
		  emptyMsg : "没有记录"
	  });

	// 定义一个set集合
	var fieldSet = new Ext.form.FieldSet({
		  autoHeight : true,
		  autoWeight : true,
		  defaults : {
			  width : 270,
			  labelWidth : 30
		  },
		  items : [{
			    xtype : 'fieldset',
			    autoHeight : true,
			    items : [{
				      // 定义一个上传信息的文本框
				      id : 'participleUploadFile',
				      name : 'uploadFile',
				      xtype : 'textfield',
				      fieldLabel : '文件',
				      inputType : 'file',
				      anchor : '96%',
				      allowBlank : false
			      }]
		    }

		  ]
	  });
	// 定义个formPanel，把set信息添加此中
	var confFormPanel = new Ext.FormPanel({
		  region : 'center',
		  layout : 'fit',
		  enctype : 'multipart/form-data',
		  border : false,
		  width : 280,
		  fileUpload : true,
		  items : [fieldSet]
	  });
	this.confFormPanel = confFormPanel;
	// 打开窗口
	var configWindow = new Ext.Window({
		  width : 300,
		  height : 150,
		  plain : true,
		  border : false,
		  title : '强制分词信息',
		  closeAction : 'hide',
		  collapsible : true,// 折叠
		  closable : true, // 关闭
		  resizable : false,// 改变大小
		  draggable : true,// 拖动
		  minimizable : false,// 最小化
		  maximizable : false,// 最大化
		  animCollapse : true,
		  // layout : 'border',
		  // bodyStyle : 'padding:5px;',
		  items : [confFormPanel],
		  buttonAlign : "center",
		  buttons : [{
			    id : 'sub_button_participle',
			    text : '提交',
			    handler : function() {
				    self.postParticiple(this); // 单击提交时响应事件
			    }
		    }, {
			    text : '重置',
			    type : 'reset',
			    id : 'clear_participle',
			    handler : function() {
				    confFormPanel.form.reset();
			    }
		    }]
	  });
	var config = {
		id : 'enforcementParticiplePanel',
		store : store,
		margins : '0 5 5 5',
		border : false,
		autoExpandColumn : 'participle',
		plugins : [editor],
		tbar : [{
			  iconCls : 'icon-wordclass-add',
			  text : '添加分词',
			  handler : function() {
				  var participle = new store.recordType({
					    participle : ''
				    });
				  editor.stopEditing();
				  store.insert(0, participle);
				  self.getSelectionModel().selectRow(0);
				  editor.startEditing(0, 1);
			  }
		  }, {
			  iconCls : 'icon-wordclass-del',
			  text : '删除分词',
			  handler : function() {
				  editor.stopEditing();
				  var rows = self.getSelectionModel().getSelections();
				  if (rows.length == 0) {
					  Ext.Msg.alert('提示', '请至少选择一条记录！');
					  return false;
				  }
				  Ext.Msg.confirm('提示', '确认删除这' + rows.length + '条记录？', function(btn) {
					    if (btn == 'yes') {
						    var ids = [];
						    Ext.each(rows, function() {
							      ids.push(this.id)
						      });
						    Ext.Ajax.request({
							      url : 'enforcement-participle!delete.action',
							      params : {
								      ids : ids.join(',')
							      },
							      success : function(resp) {
								      Ext.Msg.alert('提示', '删除成功！', function() {
									        store.reload();
								        });
							      },
							      failure : function(resp) {
								      Ext.Msg.alert('错误', resp.responseText);
							      }
						      })
					    }
				    });
			  }
		  }, {
			  iconCls : 'icon-refresh',
			  text : '刷新',
			  handler : function() {
				  store.reload();
			  }
		  }, {
			  iconCls : 'icon-ontology-sync',
			  text : '同步',
			  handler : function() {
				  editor.stopEditing();
				  Ext.Ajax.request({
					    url : 'enforcement-participle!sync.action',
					    success : function(resp) {
						    var ret = Ext.util.JSON.decode(resp.responseText);
						    if (ret.success) {
							    Ext.Msg.alert('提示', ret.message);
						    } else {
							    Ext.Msg.alert('错误', ret.message);
						    }
					    },
					    failure : function(resp) {
						    Ext.Msg.alert('错误', resp.responseText);
					    }
				    })
			  }
		  }, '-', '关键字:', {
			  id : 's_participle',
			  xtype : 'textfield',
			  width : 100,
			  emptyText : '输入关键字搜索...'
		  }, {
			  text : '搜索',
			  iconCls : 'icon-search',
			  handler : function() {
				  self.search();
			  }
		  }, ' ', {
			  text : '全部',
			  iconCls : 'icon-search',
			  handler : function() {
				  self.search(true);
			  }
		  }, {
			  iconCls : 'icon-ontology-import',
			  text : '导入',
			  handler : function() {
				  configWindow.show();
			  }
		  }, {
			  iconCls : 'icon-ontology-export',
			  text : '导出',
			  handler : function() {
				  self.exportParticiple();
			  }
		  }],
		bbar : pagingBar,
		sm : sm,
		loadMask : true,
		colModel : new Ext.grid.ColumnModel({
			  columns : [{
				    id : 'participle',
				    header : '<div style="text-align:center">分词</div>',
				    dataIndex : 'participle',
				    editor : {
					    xtype : 'textfield',
					    allowBlank : false,
					    blankText : '请输入分词内容'
				    }
			    }]
		  })
	};
	this.getPageSize = function() {
		return _pageSize;
	}
	EnforcementParticiplePanel.superclass.constructor.call(this, config);
}

Ext.extend(EnforcementParticiplePanel, Ext.grid.GridPanel, {
	  loadData : function() {
		  this.getStore().load({
			    params : {
				    start : 0,
				    limit : this.getPageSize()
			    }
		    })
	  },
	  search : function(showAll) {
		  var tbar = this.getTopToolbar();
		  var _nameField = tbar.findById('s_participle');
		  if (showAll) {
			  _nameField.setValue('');
		  }
		  var store = this.getStore();
		  for (var key in store.baseParams) {
			  if (key && key.indexOf('queryParam.') != -1)
				  delete store.baseParams[key];
		  }
		  var _participle = _nameField.getValue().trim();
		  if (_participle)
			  store.setBaseParam('queryParam.participle', _participle);
		  store.load();
	  },
	  // 提交时要响应的信息
	  postParticiple : function() {
		  var self = this;
		  if (this.confFormPanel.getForm().isValid()) {
			  var fileName = Ext.getCmp("participleUploadFile").getValue(); // 得到上传文件名
			  this.confFormPanel.getForm().submit({
				    url : 'enforcement-participle!importData.action',
				    success : function(form, action) {
					    var res = action.result.message;
					    if (res != null) {
						    Ext.Msg.alert('导入失败', res);
					    } else {
						    Ext.Msg.alert('导入提示', '数据导入成功');
						    self.loadData();
					    }
				    },
				    failure : function() {
					    Ext.Msg.alert('错误提示', '导入失败');
					    Ext.getCmp("sub_button_participle").setDisabled(false);
				    },
				    scope : this
			    });
		  } else {
			  Ext.Msg.alert('错误提示', '地址不能为空!');
		  }

	  },
	  exportParticiple : function() {
		  window.location = 'enforcement-participle!exportData.action';
	  },
	  isEnAndNum : function(value) {
		  var reg = /^[a-z0-9\,\*]+$/i;
		  if (reg.test(value) && value.indexOf(",") > -1)
			  return true
		  else {
			  return false;
		  }
	  }
  });
