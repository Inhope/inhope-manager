FileTransportManager = function(_cfg) {
	var self = this;
	var exportTargetCombo = new Ext.form.ComboBox({
		name : 'exportTarget',
		xtype : "combo",
		fieldLabel : '类型',
		allowBlank : true,
		triggerAction : 'all',
		editable : false,
		mode : 'local',
		value : 'p4',
		store : new Ext.data.ArrayStore({
			fields : [ 'type', 'name' ],
			data : [ [ 'attachments', '附件' ], [ 'p4', 'P4页面' ], ['imgtxtmsg', '图文消息'] ]
		}),
		valueField : 'type',
		displayField : 'name',
		anchor : '96%'
	});
	var importTargetCombo = new Ext.form.ComboBox({
		name : '_importTarget',
		hiddenName : 'importTarget',
		xtype : "combo",
		fieldLabel : '类型',
		allowBlank : false,
		triggerAction : 'all',
		editable : false,
		mode : 'local',
		value : 'p4',
		store : new Ext.data.ArrayStore({
			fields : [ 'type', 'name' ],
			data : [ [ 'attachments', '附件' ], [ 'p4', 'P4页面' ], ['imgtxtmsg', '图文消息'] ]
		}),
		valueField : 'type',
		displayField : 'name',
		anchor : '96%'
	});

	var _fileForm = new Ext.FormPanel(
			{
				layout : "fit",
				frame : true,
				border : false,
				autoHeight : true,
				waitMsgTarget : true,
				defaults : {
					bodyStyle : 'padding:5px'
				},
				margins : '0 0 0 0',
				labelAlign : "left",
				labelWidth : 50,
				fileUpload : true,
				items : [
						{
							ref : 'statField',
							xtype : 'fieldset',
							title : '文件导出',
							style : 'padding-top:8px',
							autoHeight : true,
							items : [
									exportTargetCombo,
									{
										xtype : 'button',
										text : '开始导出',
										style : 'padding-top:8px;padding-right:11px;float:right;',
										handler : function() {
											this.disable();
											var btn = this;
											var target = exportTargetCombo
													.getValue();
											if (!target) {
												Ext.Msg.alert('提示',
														'请选择导出目标文件夹');
												btn.enable();
												return false;
											}
											Ext.Ajax
													.request({
														url : 'file-upload!checkFile.action',
														params : {
															target : target
														},
														success : function(resp) {
															var ret = Ext
																	.decode(resp.responseText);
															if (ret.success) {
																if (!self.downloadIFrame) {
																	self.downloadIFrame = self
																			.getEl()
																			.createChild(
																					{
																						tag : 'iframe',
																						style : 'display:none;'
																					})
																}
																self.downloadIFrame.dom.src = 'file-upload!exportZip.action?ts='
																		+ new Date()
																				.getTime()
																		+ '&target='
																		+ target;
															} else {
																Ext.Msg
																		.alert(
																				'错误',
																				'目标文件夹不存在!');
															}
															btn.enable();
														}
													})
										}
									} ]
						},
						{
							xtype : 'fieldset',
							title : '文件导入',
							autoHeight : true,
							items : [
									importTargetCombo,
									{
										name : 'file',
										xtype : "textfield",
										fieldLabel : '文件',
										inputType : 'file',
										allowBlank : false,
										anchor : '96%',
										validator : function(v) {
											if (v) {
												if (v.substring(v.length - 4) != '.zip')
													return '请选择zip文件';
												else
													return true;
											} else
												return false;
										}
									},
									{
										text : "开始导入",
										xtype : 'button',
										style : 'padding-top:8px;padding-right:11px;float:right;',
										handler : function() {
											var btn = this;
											var fileForm = _fileForm.getForm();
											if (fileForm.isValid()) {
												btn.disable();
												fileForm
														.submit({
															url : 'file-upload!importZip.action',
															success : function(
																	form,
																	action) {
																btn.enable();
																var succ = action.result.success;
																if (succ) {
																	Ext.Msg
																			.alert(
																					'提示',
																					'导入成功');
																} else {
																	Ext.Msg
																			.alert(
																					'错误',
																					'导入失败');
																}
															},
															failure : function(
																	form,
																	action) {
																btn.enable();
																Ext.MessageBox
																		.hide();
																Ext.Msg
																		.alert(
																				'导入失败',
																				action.response.responseText);
															}
														});
											}
										}
									} ]
						} ]
			});
	var _importPanel = new Ext.Panel({
		border : false,
		layout : "fit",
		layoutConfig : {
			animate : true
		},
		items : [ _fileForm ]
	});

	var cfg = new Ext.Window({
		border : false,
		width : 350,
		title : '文件导入导出',
		defaults : {
			border : false
		},
		plain : true,
		modal : true,
		plain : true,
		shim : true,
		closeAction : 'hide',
		collapsible : true,
		closable : true,
		resizable : false,
		draggable : true,
		minimizable : false,
		maximizable : false,
		animCollapse : true,
		constrainHeader : true,
		autoHeight : false,
		items : [ _importPanel ]
	});
	FileTransportManager.superclass.constructor.call(this, Ext.applyIf(_cfg
			|| {}, cfg));
};
Ext.extend(FileTransportManager, Ext.Window, {
	showWindow : function() {
		this.show();
	}
});