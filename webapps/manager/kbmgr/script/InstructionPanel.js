

InstructionPanel = function(cfg) {
	cfg = cfg || {};
	var self = this;

	this.data = {
		params : []
	};

	this.InstructionRecord = Ext.data.Record.create(['id', 'code', 'name',
			'intro']);
	this.instructionProxy = new LocalProxy({});
	var instructionEditGrid = this.instructionEditGrid = new ExcelLikeGrid({
				flex : 5,
				autoExpandColumn : 'code',
				style : 'border-right: 1px solid ' + sys_bdcolor,
				border : false,
				clicksToEdit : 1,
				frame : false,
				columnLines : true,
				selModel : new Ext.grid.RowSelectionModel(),
				subgridLoader : {
					load : self.loadParams,
					scope : self
				},
				pasteable : true,
				columns : [new Ext.grid.RowNumberer(), {
							id : 'code',
							header : '指令代码',
							dataIndex : 'code',
							width : 120,
							editor : new Ext.form.TextField()
						}, {
							header : '名称',
							dataIndex : 'name',
							width : 120,
							editor : new Ext.form.TextField()
						}, {
							header : '描述',
							dataIndex : 'intro',
							width : 120,
							editor : new Ext.form.TextField()
						}, new DeleteButtoner],
				store : new Ext.data.Store({
							proxy : self.instructionProxy,
							reader : new Ext.data.JsonReader({
										idProperty : 'id',
										root : 'data',
										fields : self.InstructionRecord
									}),
							writer : new Ext.data.JsonWriter()
						})
			});

	this.ParamRecord = Ext.data.Record.create(['id', 'name']);
	this.paramProxy = new LocalProxy({});
	var paramEditGrid = this.paramEditGrid = new ExcelLikeGrid({
				autoExpandColumn : 'id',
				anchor : '100% -110',
				border : true,
				clicksToEdit : 1,
				frame : false,
				columnLines : true,
				selModel : new Ext.grid.RowSelectionModel(),
				pasteable : true,
				columns : [new Ext.grid.RowNumberer(), {
							id : 'id',
							header : '名称',
							dataIndex : 'name',
							editor : new Ext.form.TextField()
						}, {
							header : '类型',
							dataIndex : 'type',
							width : 120,
							editor : {
								xtype : 'combo',
								store : [[0, '文本'], [1, '文件'], [2, 'HTML'],[3,'图文消息']],
								triggerAction : 'all'
							},
							renderer : function(v, meta, r) {
								for (var key in r.data) {
									if (v == 0)
										return '文本'
									else if (v == 1)
										return '文件'
									else if (v == 2)
										return 'HTML'
									else if(v==3)
									    return '图文消息'
								}
								return ''
							}
						}, new DeleteButtoner()],
				store : new Ext.data.Store({
							proxy : self.paramProxy,
							reader : new Ext.data.JsonReader({
										idProperty : 'id',
										root : 'params',
										fields : self.ParamRecord
									}),
							writer : new Ext.data.JsonWriter()
						})
			});

	var _importForm = new Ext.FormPanel({
				region : 'center',
				layout : 'fit',
				enctype : 'multipart/form-data',
				bodystyle : 'padding:5px 5px 0',
				width : 300,
				height : 100,
				labelWidth : 40,
				fileUpload : true,
				items : [{
							xtype : 'fieldset',
							title : '选择文件',
							autoHeight : true,
							items : [{
										border : false,
										id : 'instructionDataImport',
										name : 'instructionFile',
										xtype : 'textfield',
										fieldLabel : '文件',
										allowBlank : false,
										inputType : 'file',
										anchor : '96%'
									}]
						}]
			});

	var _importWindow = new Ext.Window({
				width : 310,
				height : 150,
				modal : true,
				plain : true,
				shim : true,
				title : '指令导入',
				closable : true,
				draggable : true,
				closeAction : 'hide',
				collapsible : true,// 折叠
				animCollapse : true,
				constrainHeader : true,
				items : [_importForm],
				buttonAlign : "center",
				buttons : [{
							text : '提交',
							handler : function() {
								self.importExcel();
							}
						}, {
							text : '清除',
							handler : function() {
								_importForm.form.reset();
							}

						}]
			});

	this.importForm = _importForm;
	this.importWindow = _importWindow
	var paramForm = this.paramForm = new Ext.form.FormPanel({
				flex : 3,
				margins : '0 0 0 5',
				padding : 8,
				style : 'border-left: 1px solid ' + sys_bdcolor,
				border : false,
				frame : false,
				labelAlign : 'top',
				bodyStyle : 'background-color:' + sys_bgcolor,
				items : [{
					xtype : 'textfield',
					anchor : '100%',
					fieldLabel : '指令代码',
					ref : 'instructionCodeField',
					listeners : {
						blur : function() {
							var r = instructionEditGrid.getSelectionModel()
									.getSelected();
							if (r)
								r.set("code", this.getValue())
						}
					}
				}, {
					xtype : 'textfield',
					anchor : '100%',
					fieldLabel : '指令名',
					ref : 'instructionNameField',
					listeners : {
						blur : function() {
							var r = instructionEditGrid.getSelectionModel()
									.getSelected();
							if (r)
								r.set("name", this.getValue())
						}
					}
				}, paramEditGrid]
			});

	InstructionPanel.superclass.constructor.call(this, {
				border : false,
				frame : false,
				// bodyStyle : 'background-color:' + sys_bgcolor,
				// style : 'border-top: 1px solid ' + sys_bdcolor,
				layout : {
					type : 'hbox',
					align : 'stretch'
				},
				items : [instructionEditGrid, paramForm],
				buttonAlign : 'center',
				tbar : [{
							text : '刷新',
							iconCls : 'icon-refresh',
							handler : function() {
								self.loadData();
							}
						}, {
							text : '导入',
							iconCls : 'icon-ontology-import',
							handler : function() {
								self.importWindow.show();
							}
						}, {
							text : '导出',
							iconCls : 'icon-ontology-export',
							handler : function() {
								self.exportExcel();
							}
						}, {
							text : '保存',
							iconCls : 'icon-table-save',
							handler : function() {
								self.save();
							}
						}]
			});

	instructionEditGrid.on("scrollbottomclick", function(e) {
				instructionEditGrid.batchAdd(28);
			});
	paramEditGrid.on("scrollbottomclick", function(e) {
				paramEditGrid.batchAdd(18);
			});

	instructionEditGrid.on("shiftright", function(e) {
				var g = paramEditGrid
				var col = g.findMostLeftEditCol()
				g.getSelectionModel().selectRow(0)
				this.stopEditing()
				g.startEditing(0, col)
			});
	paramEditGrid.on("shiftleft", function(e) {
				var g = instructionEditGrid
				var col = g.findMostRightEditCol()
				var r = g.getSelectionModel().getSelected()
				var row = g.getStore().indexOf(r)
				this.stopEditing()
				g.startEditing(row, col)
			});
};

Ext.extend(InstructionPanel, Ext.Panel, {
	loadData : function() {
		Ext.Ajax.request({
					url : 'ontology-instruction!list.action',
					success : function(form, action) {
						this.data = Ext.util.JSON.decode(form.responseText);
						this._refresh();
					},
					failure : function(form, action) {
						Ext.Msg.alert('提示', '获取指令列表失败');
					},
					scope : this
				});
	},
	loadParams : function(record) {
		this.paramForm.instructionCodeField.setValue(record.data.code)
		this.paramForm.instructionNameField.setValue(record.data.name)
		if (!record.data.params) {
			record.data.params = [];
		}
		this.paramProxy.data = record.data
		this.paramEditGrid.getStore().reload();
		this.paramEditGrid.batchAdd(18);
	},
	_refresh : function() {
		this.instructionProxy.data = this.data;
		var grid = this.instructionEditGrid;
		grid.getStore().load();
		grid.batchAdd(28);
		var i = grid.defaultSelected
		if (!i)
			i = 0;
		grid.getSelectionModel().selectRow(i);
	},
	save : function() {
		this.getEl().mask('保存...')
		for (var j = 0; j < this.data.data.length; j++) {
			for (var i = 0; i < this.data.data[j].params.length; i++) {
				this.data.data[j].params[i].param = i;
			}
		}
		var self = this;
		Ext.Ajax.request({
					url : 'ontology-instruction!save.action',
					params : {
						data : Ext.encode(this.data.data)
					},
					success : function(form, action) {
						this.getEl().unmask()
						Dashboard.setAlert("保存成功!");
						// var data = Ext.util.JSON
						// .decode(form.responseText);
						// this.data = data;
						// this._refresh();
						self.loadData();
					},
					failure : function(response) {
						this.getEl().unmask()
						Ext.Msg.alert("错误", response.responseText);
					},
					scope : this
				});
	},
	exportExcel : function() {
		Ext.Ajax.request({
			url : 'ontology-instruction!export.action',
			success : function(response) {
				result = Ext.util.JSON.decode(response.responseText);
				var timer = new ProgressTimer({
					initData : result.data,
					progressId : 'instructionExportStatus',
					boxConfig : {
						title : '正在准备文件'
					},
					finish : function() {
						if (!this.downloadIFrame) {
							this.downloadIFrame = this.getEl().createChild({
										tag : 'iframe',
										style : 'display:none;'
									});
						}
						this.downloadIFrame.dom.src = 'ontology-instruction!exportExcel.action?&_t'
								+ new Date().getTime();
					},
					scope : this
				});
				timer.start();
			},
			failure : function(response) {
				Ext.Msg.alert('请求异常', response.responseText);
			},
			scope : this
		});
	},
	importExcel : function() {
		if (Ext.getCmp('instructionDataImport').getValue() != null
				&& Ext.getCmp('instructionDataImport').getValue()) {
			if (this.importForm.isVisible()) {
				var self = this;
				this.importForm.getForm().submit({
					url : 'ontology-instruction!importExcel.action',
					success : function(form, action) {
						var result = action.result.data;
						var timer = new ProgressTimer({
									initData : result,
									progressId : 'importInstructionExcelStatus',
									boxconfig : {
										title : '指令导入'
									},
									finish : function(p, response) {
										Ext.MessageBox.hide();
										self.loadData();
									}
								});
						timer.start();
					},
					failure : function(form) {
						Ext.Msg.alert('异常提示', '连接失败');
					},
					scope : this
				});
			}
		} else {
			Ext.Msg.alert('错误提示', '提交文件不能为空');
		}
	}
});