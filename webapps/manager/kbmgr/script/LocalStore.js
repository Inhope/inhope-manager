var LocalStore = Ext.extend(Ext.data.Store, {
			localData : [],
			recordIdMap : {},
			isModified : false,
			processChange : function(action, rs) {
				Ext.each(rs, function(r) {
							if (action == 'add') {
								var hasProperty = false;
								for (var key in r.data) {
									hasProperty = true
								}
								if (hasProperty) {
									this.recordIdMap[r.id] = r.data;
									this._getLocalData(true)
											.push(this.recordIdMap[r.id]);
									this._r_count++;
									this.isModified = true;
								}
							} else if (action == 'update') {
								var d = this.recordIdMap[r.id];
								if (!d) {
									this.recordIdMap[r.id] = r.data;
									this._getLocalData(true)
											.push(this.recordIdMap[r.id]);
									this._r_count++;
								} /*
									 * else { Ext.apply(d, r.data); }
									 */
								this.isModified = true;
							} else if (action == 'remove') {
								var ds = this._getLocalData()
								if (ds)
									Ext.each(ds, function(_d, index) {
												if (_d == this.recordIdMap[r.id]) {
													ds.splice(index, 1);
													delete this.recordIdMap[r.id];
													this._r_count--;
													this.isModified = true;
													return false;
												}
											}, this);
							}
						}, this)
			},
			constructor : function(cfg) {
				LocalStore.superclass.constructor.call(this, cfg || {});
			},
			_bindEvents : function() {
				if (this.__binded) {
					return;
				}
				this.__binded = true;
				var ls = {
					remove : function(store, record) {
						this.processChange('remove', [record])
					},
					add : function(store, records, index) {
						this.processChange('add', records)
					},
					update : function(store, record) {
						this.processChange('update', [record])
					}
				};
				for (var key in ls) {
					this.on(key, ls[key], this);
				}
			},
			_getLocalData : function(create) {
				this._bindEvents();
				if (!this.localData) {
					this.localData = this._data[this.reader.meta.root];
					if (!this.localData && create) {
						if (this.reader.meta.root) {
							this._data[this.reader.meta.root] = this.localData = [];
						}
					}
				}
				return this.localData;
			},
			loadLocalData : function(data) {
				this.recordIdMap = {};
				this._data = data;
				this.localData = null;
				delete this.isModified
				var ds = this._getLocalData(true);
				delete this.modified
				this.modified = []
				LocalStore.superclass.loadData.call(this, this._data)
				this.localData.length = 0
				if (this.getCount() > 0) {
					this.each(function(r) {
								var hasProperty = false;
								for (var key in r.data) {
									hasProperty = true
								}
								if (hasProperty) {
									this.recordIdMap[r.id] = r.data;
									this.localData.push(r.data)
								}
							}, this)
				}
			},
			_r_count : 0,
			getRealCount : function() {
				return this._r_count;
			}
		});