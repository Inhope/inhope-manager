ModuleConfigPanel = function() {
	var self = this;
	this.data = {};
	this.propsHolder = {};
	var _store = new Ext.data.Store({
				proxy : new Ext.data.HttpProxy({
							url : 'module-config!list.action'
						}),
				reader : new Ext.data.JsonReader({
							idProperty : 'id',
							root : 'data',
							fields : [{
										name : 'moduleId',
										type : 'string'
									}, {
										name : 'name',
										type : 'string'
									}, {
										name : 'priority',
										type : 'int'
									}, {
										name : 'enable',
										type : 'boolean'
									}, {
										name : 'contextAwared',
										type : 'boolean'
									}, {
										name : 'type',
										type : 'string'
									}, {
										name : 'ignoreCategory',
										type : 'int'
									}, {
										name : 'wordclassSearchDepth',
										type : 'int'
									}, {
										name : 'scoreThreshold',
										type : 'number'
									}, {
										name : 'searchThreshold',
										type : 'number'
									}, {
										name : 'maxRetryTimes',
										type : 'int'
									}, {
										name : 'excludedWordNodes',
										type : 'string'
									}, {
										name : 'contextObjectBoost',
										type : 'number'
									}, {
										name : 'contextClassAttrBoost',
										type : 'number'
									}, {
										name : 'contextAskNodeBoost',
										type : 'number'
									}, {
										name : 'nodeId',
										type : 'String'
									}, {
										name : 'nodeAddrs',
										type : 'String'
									}, {
										name : 'moduleAwared',
										type : 'boolean'
									}]
						})
			});
	var _idMap;
	_store.on('load', function(s, records, opts) {
				_idMap = {};
				for (var i = 0; i < records.length; i++) {
					_idMap[records[i].json.moduleId] = true;
					if(records[i].data.nodeId){
						self.getView().getRow(i).style.backgroundColor='#FFFFBB';
					}else{
						//self.getView().getRow(i).style.backgroundColor='#D2E9FF';
					}
				}
			});
	var myExpander = new Ext.grid.RowExpander(
			{
				expandOnDblClick:false,
				tpl : new Ext.XTemplate(
					'<tpl if="!values.nodeId">'
					+ '<p style="color:#999;margin:2px 0px 2px 48px;">词类搜索深度: {[values.wordclassSearchDepth]}</p>'
					+ '<p style="color:#999;margin:2px 0px 2px 48px;">忽略词类节点列表: {[values.excludedWordNodes]}</p>'
					+ '<p style="color:#999;margin:2px 0px 2px 48px;">忽略类别匹配: {[this.formatIgnoreCategory(values.ignoreCategory)]}</p>'
					+ '<p style="color:#999;margin:2px 0px 2px 48px;">上下文实例附加得分: {[values.contextObjectBoost]}</p>'
					+ '<p style="color:#999;margin:2px 0px 2px 48px;">上下文类属性附加得分: {[values.contextClassAttrBoost]}</p>'
					+ '<p style="color:#999;margin:2px 0px 2px 48px;">反问缺失要素附加得分: {[values.contextAskNodeBoost]}</p>'
					+ '</tpl>'
					+ '<tpl if="values.nodeId">'
					+ '<p style="color:#999;margin:2px 0px 2px 48px;">外部模块节点地址: {[values.nodeAddrs]}</p>'
					+ '</tpl>',{formatIgnoreCategory:function(val){
						return val == 1 ? '是' : '否';
					}
				}),
				renderer : function(v, p, record){
					p.cellAttr = 'rowspan="2"';
                    return '<div class="x-grid3-row-expander"></div>';
				}
			});
	var gridHeader = new Ext.ux.grid.ColumnHeaderGroup({
        rows: [[
                { header: '', colspan: 1, align: 'center' },
				{ header: '', colspan: 1, align: 'center' },
				{ header: '公共参数', colspan: 4, align: 'center' },
				{ header: '内部模块', colspan: 3, align: 'center' },
				{ header: '外部模块', colspan: 2, align: 'center' }
        ]]
    });
	var cfg = {
		id : 'moduleConfigPanel',
		//stripeRows : true,
		border : false,
		store : _store,
		loadMask : true,
		colModel : new Ext.grid.ColumnModel({
					columns : [new Ext.grid.RowNumberer(), myExpander, {
								header : '模块ID',
								dataIndex : 'moduleId'
							}, {
								header : '模块名称',
								dataIndex : 'name'
							}, {
								header : '优先级',
								dataIndex : 'priority'
							}, {
								header : '是否启用',
								dataIndex : 'enable',
								renderer : function(val, meta, rec, rowIdx,
										colIdx, store) {
									return val ? '是' : '否';
								}
							}, {
								header : '答案阈值',
								dataIndex : 'scoreThreshold',
								renderer : function(val, meta, rec, rowIdx,colIdx, store) {
									return rec.data.nodeId ? "" : val;
								}
							},  {
								header : '搜索阈值',
								dataIndex : 'searchThreshold',
								renderer : function(val, meta, rec, rowIdx,colIdx, store) {
									return rec.data.nodeId ? "" : val;
								}
							}, {
								header : '上下文反问',
								dataIndex : 'contextAwared',
								renderer : function(val, meta, rec, rowIdx,
										colIdx, store) {
									return rec.data.nodeId ? "" : (val ? '支持' : '不支持');
								}
							}, {
								header : '节点ID',
								dataIndex : 'nodeId'
							}, {
								header : '模块约束',
								dataIndex : 'moduleAwared',
								renderer : function(val, meta, rec, rowIdx,
										colIdx, store) {
									return rec.data.nodeId ? ( val ? "是" : "否") : "";
								}
							}
					]
				}),
		sm : new Ext.grid.RowSelectionModel({
					singleSelect : true
				}),
		tbar : [{
					text : '新增内部模块',
					iconCls : 'icon-module-config-add',
					handler : function() {
						self.addModule(0);
					}
				},{
					text : '新增外部模块',
					iconCls : 'icon-module-config-add',
					handler : function() {
						self.addModule(1);
					}
				}, '', {
					text : '编辑模块',
					iconCls : 'icon-module-config-edit',
					handler : function() {
						self.modifyModule();
					}
				}, '', {
					text : '删除模块',
					iconCls : 'icon-module-config-delete',
					handler : function() {
						self.deleteModule();
					}
				}, '', {
					text : '刷新列表',
					iconCls : 'icon-refresh',
					handler : function() {
						self.getStore().reload();
					}
				}],
		viewConfig : {
			forceFit : true
		},
		plugins : [ gridHeader,myExpander]
	};
	ModuleConfigPanel.superclass.constructor.call(this, cfg);

	var _validateInt = function(val) {
		if(val == null) return false;
		val = val.trim();
		if (val == '')
			return false;
		else
			return /[0-9]/.test(val)
	};
	var _typeData = [['default', '默认类型'], ['nav', '导航类型']];
	var _typeDataMap = {};
	for (var i = 0; i < _typeData.length; i++)
		_typeDataMap[_typeData[i][0]] = _typeData[i][1];
	var commonFields  = function(){
		return {
			xtype: 'fieldset',
			title: '公共参数',
			autoHeight: true,
			defaultType: 'textfield',
			items:[new Ext.form.TextField({
				fieldLabel : '模块ID',
				allowBlank : false,
				validator : function(val) {
					if (self.propsHolder.isAdd) {
						val = val.trim();
						if (!val)
							return false;
						if (_idMap && _idMap[val])
							return '此模块标识已经存在！';
					}
				},
				name : 'moduleId',
				blankText : '请输入模块标识。',
				anchor : '95%'
			}), new Ext.form.TextField({
				fieldLabel : '模块名称',
				allowBlank : false,
				name : 'name',
				blankText : '请输入模块名称。',
				anchor : '95%'
			}), new Ext.form.TextField({
				fieldLabel : '优先级',
				allowBlank : false,
				name : 'priority',
				emptyText : '输入一个整数，数值越大优先级越高。',
				anchor : '95%',
				validator : _validateInt
			}),new Ext.form.RadioGroup({
	            fieldLabel: '是否启用',
	            name: 'enable',
	            items: [
	                {boxLabel: '启用模块', name: 'enable',inputValue: true,checked: true},
	                {boxLabel: '禁用模块', name: 'enable',inputValue: false}
	            ]
	        })]
	};}
	
	var internalFields  = {
			xtype: 'fieldset',
			title: '私有参数',
			autoHeight: true,
			defaultType: 'textfield',
			items:[new Ext.form.TextField({
				fieldLabel : '答案阈值',
				allowBlank : false,
				name : 'scoreThreshold',
				emptyText : '请输入答案阈值，0至1之间的浮点数。',
				anchor : '95%',
				validator : function(val) {
					val = val.trim();
					if (val == '')
						return false;
					else
						return (val >= 0 && val <= 1)
				}
			}), new Ext.form.TextField({
				fieldLabel : '搜索阈值',
				allowBlank : false,
				name : 'searchThreshold',
				emptyText : '请输入搜索阈值，0至1之间的浮点数。',
				anchor : '95%',
				validator : function(val) {
					val = val.trim();
					if (val == '')
						return false;
					else
						return (val >= 0 && val <= 1)
				}
			}), new Ext.ux.form.SpinnerField({
				fieldLabel : '词类搜索深度',
				allowBlank : false,
				name : 'wordclassSearchDepth',
				emptyText : '请输入深度值，0至9之间的数字',
				minValue: 0,
            	maxValue: 9,
            	incrementValue: 1,
            	accelerate: true,
            	validator : function(val) {
					val = val.trim();
					if (val == '')
						return false;
					else
						return (val >= 0 && val <= 9)
				}
			}), new Ext.form.TextField({
				fieldLabel : '重试次数',
				allowBlank : false,
				name : 'maxRetryTimes',
				emptyText : '请输入最大重试次数。',
				anchor : '95%',
				value : "1",
				validator : _validateInt
			}), new Ext.form.CheckboxGroup({
				anchor : '100%',
				columns : 2,
				items : [{
							boxLabel : '支持上下文',
							name : 'contextAwared',
							inputValue : 'true',
							anchor : '100%'
						}, {
							boxLabel : '忽略类别匹配',
							name : 'ignoreCategory',
							inputValue : 1,
							anchor : '100%'
						}]
			}), new Ext.form.TextArea({
				fieldLabel : '忽略词类节点列表',
				name : "excludedWordNodes",
				anchor : '95%',
				height : 80,
				ref : 'wordNodesField'
			}),new Ext.form.TextField({
				fieldLabel : '上下文实例附加得分',
				allowBlank : false,
				name : 'contextObjectBoost',
				emptyText : '-1至1之间的浮点数。',
				anchor : '95%',
				validator : function(val) {
					val = val.trim();
					if (val == '')
						return false;
					else
						return (val>-1 && val < 1)
				}
			}),new Ext.form.TextField({
				fieldLabel : '上下文类属性附加得分',
				allowBlank : false,
				name : 'contextClassAttrBoost',
				emptyText : '-1至1之间的浮点数。',
				anchor : '95%',
				validator : function(val) {
					val = val.trim();
					if (val == '')
						return false;
					else
						return (val>-1 && val < 1)
				}
			}),new Ext.form.TextField({
				fieldLabel : '反问缺失要素附加得分',
				allowBlank : false,
				name : 'contextAskNodeBoost',
				emptyText : '-1至1之间的浮点数。',
				anchor : '95%',
				validator : function(val) {
					val = val.trim();
					if (val == '')
						return false;
					else
						return (val>-1 && val < 1)
				}
			})]};
	
	var externalFields  =  {
			xtype: 'fieldset',
			title: '私有参数',
			autoHeight: true,
			defaultType: 'textfield',
			items:[new Ext.form.TextField({
				fieldLabel : '节点ID',
				allowBlank : false,
				name : 'nodeId',
				emptyText : '',
				anchor : '95%'
			}),new Ext.form.TextField({
				fieldLabel : '节点地址',
				allowBlank : false,
				name : 'nodeAddrs',
				emptyText : '',
				anchor : '95%'
			}),new Ext.form.RadioGroup({
	            fieldLabel: '模块约束',
	            name: 'moduleAwared',
	            items: [
	                {boxLabel: '约束', name: 'moduleAwared',inputValue: true,checked: true},
	                {boxLabel: '不约束', name: 'moduleAwared',inputValue: false}
	            ]
	        }),new Ext.form.TextArea({
				fieldLabel : '忽略词类节点列表',
				name : "excludedWordNodes",
				anchor : '95%',
				height : 80,
				ref : 'wordNodesField'
			})]
		};

	var externalFormPanel = new Ext.form.FormPanel({
				frame : false,
				border : false,
				labelWidth : 80,
				autoHeight : true,
				waitMsgTarget : true,
				bodyStyle : 'padding : 3px 10px; background-color:' + sys_bgcolor,
				items : [commonFields(),externalFields],
				tbar : [{
					text : '保存',
					iconCls : 'icon-wordclass-add',
					handler : function() {
						self.saveModule(this);
					}
				}]
			});
	var internalFormPanel = new Ext.form.FormPanel({
		frame : false,
		border : false,
		labelWidth : 80,
		autoHeight : true,
		waitMsgTarget : true,
		bodyStyle : 'padding : 3px 10px; background-color:' + sys_bgcolor,
		items : [commonFields(),internalFields],
		tbar : [{
			text : '保存',
			iconCls : 'icon-wordclass-add',
			handler : function() {
				self.saveModule(this);
			}
		}]
	});
	
	this.externalFormWin = new Ext.Window({
				width : 400,
				title : '编辑外部模块',
				defaults : {
					border : false
				},
				modal : true,
				plain : true,
				closable : true,
				shim : true,
				closeAction : 'hide',
				collapsible : true,
				resizable : false,
				draggable : true,
				animCollapse : true,
				constrainHeader : true,
				listeners : {
					'beforehide' : function(p) {
						p.formPanel.getForm().reset();
					}
				},
				items : [externalFormPanel]
			});
	this.externalFormWin.formPanel = externalFormPanel;
	
	this.internalFormWin = new Ext.Window({
		width : 400,
		title : '编辑内部模块',
		defaults : {
			border : false
		},
		modal : true,
		plain : true,
		closable : true,
		shim : true,
		closeAction : 'hide',
		collapsible : true,
		resizable : false,
		draggable : true,
		animCollapse : true,
		constrainHeader : true,
		listeners : {
			'beforehide' : function(p) {
				p.formPanel.getForm().reset();
			}
		},
		items : [internalFormPanel]
	});
	this.internalFormWin.formPanel = internalFormPanel;
	
	
	this.addListener('rowdblclick', function() {
				this.modifyModule();
			}, this);
//	this.addListener('rowclick', myExpander.onRowDblClick, myExpander);
};

Ext.extend(ModuleConfigPanel, Ext.grid.GridPanel, {
			getForm : function() {
				if(this.formType == 1)
					return this.externalFormWin.formPanel;
				else 
					return this.internalFormWin.formPanel;
			},
			loadData : function() {
				this.getStore().load();
			},
			endEdit : function() {
				this.getForm().getForm().reset();
				this.externalFormWin.hide();
				this.internalFormWin.hide();
			},
			saveModule : function(btn) {
				btn.disable();
				var form = this.getForm().getForm();
				if(!form.isValid()){
					Ext.Msg.alert('提示', '您尚未正确完整的输入表单内容，请检查！');
					btn.enable();
					return false;
				}
				this.data = form.getValues();
				if(this.data.moduleId && this.data.moduleId.indexOf("@")==0){
				   Ext.Msg.alert('提示', '模块ID不能以@开头！');
				   btn.enable();
				   return;
				}
				
				var capv = 0;

				this.data.excludedWordNodes = this.data['excludedWordNodes']? this.data['excludedWordNodes'].split(/,|，/) : [];
				
				var self = this;
				var url = this.propsHolder.isAdd
						? 'module-config!save.action'
						: 'module-config!update.action'
				Ext.Ajax.request({
							url : url,
							params : {
								'data' : Ext.encode(self.data)
							},
							success : function(resp) {
								btn.enable();
								self.endEdit();
								Ext.Msg.alert('提示', '保存成功！', function() {
											self.getStore().reload();
										})
							},
							failure : function(resp) {
								Ext.Msg.alert('错误', resp.responseText);
								btn.enable();
								self.endEdit();
							}
						});
			},
			addModule : function(type) {
				this.propsHolder.isAdd = true;
				this.formType = type;
				if(type == 0) {
					this.internalFormWin.show();
					this.internalFormWin.formPanel.getForm().findField("moduleId").setReadOnly(false);
				}
				else {
					this.externalFormWin.show();
					this.externalFormWin.formPanel.getForm().findField("moduleId").setReadOnly(false);
				}
			},
			modifyModule : function() {
				this.propsHolder.isAdd = false;
				var rec = this.getSelectionModel().getSelected();
				if (!rec) {
					Ext.Msg.alert('提示', '请选择一条记录进行编辑！');
					return false;
				}
				if(!rec.data.nodeId){
					this.formType = 0;
					this.internalFormWin.show();
					this.internalFormWin.formPanel.getForm().loadRecord(rec);
					this.internalFormWin.formPanel.getForm().findField("moduleId").setReadOnly(true);
				}else{
					this.formType = 1;
					this.externalFormWin.show();
					this.externalFormWin.formPanel.getForm().loadRecord(rec);
					this.externalFormWin.formPanel.getForm().findField("moduleId").setReadOnly(true);
				}
			},
			deleteModule : function() {
				var rec = this.getSelectionModel().getSelected();
				if (!rec) {
					Ext.Msg.alert('提示', '请先选择要删除的记录！');
					return false;
				}
				var self = this;
				Ext.Msg.confirm('确定框', '确定要删除这条记录吗？', function(sel) {
							if (sel == 'yes') {
								Ext.Ajax.request({
											url : 'module-config!delete.action',
											params : {
												moduleId : rec.json.moduleId
											},
											success : function(resp) {
												Ext.Msg.alert('提示', '删除成功！',
														function() {
															self.getStore()
																	.reload();
														});
											},
											failure : function(resp) {
												Ext.Msg.alert('错误',
														resp.responseText)
											}
										});
							}
						});
			}
		});