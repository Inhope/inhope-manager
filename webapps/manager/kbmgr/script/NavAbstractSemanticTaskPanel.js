NavAbstractSemanticTaskPanel = function(cfg) {
	cfg = cfg || {}
	var me = this;
	var finalCfg = Ext.apply({
				id : 'abstract_semantic_task_nav',
				title : '抽象语义任务',
				width : 200,
				minSize : 175,
				maxSize : 400,
				collapsible : true,
				rootVisible : false,
				enableDD : true,
				border : false,
				lines : false,
				autoScroll : true,
				containerScroll : true,
				root : {
					id : '0',
					text : 'root',
					iconCls : 'icon-ontology-root',
					lazyLoad : true
				},
				dataUrl : 'abstract-semantic-task!list.action'
			}, cfg);

	NavAbstractSemanticTaskPanel.superclass.constructor.call(this, finalCfg);

	var menu = new Ext.menu.Menu({
				items : [{
							id : 'refreshTask',
							text : '刷新',
							iconCls : 'icon-refresh',
							width : 150,
							handler : function() {
								me.refreshNode(menu.currentNode);
							}
						}, {
							id : 'createDic',
							text : '新建分类',
							iconCls : 'icon-add',
							width : 150,
							handler : function() {
								me.dicForm.getForm().reset();
								me.dicWin.show();
							}
						}, {
							id : 'createLeaf',
							text : '新建任务',
							iconCls : 'icon-add',
							width : 150,
							handler : function() {
								me.leafWin.show();
							}
						}, {
							text : '修改',
							iconCls : 'icon-wordclass-edit',
							width : 150,
							handler : function() {
								me.updateUI(menu.currentNode);
							}
						}, {
							text : '删除',
							iconCls : 'icon-wordclass-del',
							width : 150,
							handler : function() {
								me.remove(menu.currentNode);
							}
						}]
			});
			
	var _taskId = this._taskId = new Ext.form.Hidden({
				name : 'taskId'
			});

	var dicForm = this.dicForm = new Ext.form.FormPanel({
				frame : false,
				border : false,
				labelWidth : 60,
				waitMsgTarget : true,
				bodyStyle : 'padding : 3px 10px; background-color:' + sys_bgcolor,
				defaults : {
					blankText : '',
					invalidText : '',
					anchor : '100%',
					xtype : 'textfield'
				},
				items : [{
							fieldLabel : '名称',
							allowBlank : false,
							name : 'node',
							blankText : '',
							invalidText : '',
							anchor : '100%'
						}],
				buttons : [{
							text : '保存',
							handler : function() {
								me.saveDic(this, menu.currentNode);
							}
						}, {
							text : '关闭',
							handler : function() {
								me.dicWin.hide();
							}
						}]
			});

	var dicWin = this.dicWin = new Ext.Window({
				width : 300,
				border : false,
				modal : true,
				closable : true,
				closeAction : 'hide',
				resizable : false,
				items : [dicForm]
			});

	
	var updateForm = this.updateForm = new Ext.form.FormPanel({
				frame : false,
				border : false,
				labelWidth : 60,
				waitMsgTarget : true,
				bodyStyle : 'padding : 3px 10px; background-color:' + sys_bgcolor,
				defaults : {
					blankText : '',
					invalidText : '',
					anchor : '100%',
					xtype : 'textfield'
				},
				items : [{
							fieldLabel : '名称',
							allowBlank : false,
							name : 'node',
							blankText : '',
							invalidText : '',
							anchor : '100%'
						},  this._taskId ],
				buttons : [{
							text : '保存',
							handler : function() {
								me.updateDic(menu.currentNode);
							}
						}, {
							text : '关闭',
							handler : function() {
								me.updateWin.hide();
							}
						}]
			});

	var updateWin = this.updateWin = new Ext.Window({
				width : 300,
				border : false,
				title : '修改',
				modal : true,
				closable : true,
				closeAction : 'hide',
				resizable : false,
				items : [updateForm]
			});		
			
	this.endTimeField = new Ext.form.DateField({
				fieldLabel : '结束时间',
				format : 'Y-m-d',
				editable : false,
				name : 'endTime',
				anchor : '100%'
			})

	var leafForm = this.leafForm = new Ext.form.FormPanel({
				fileUpload : true,
				frame : false,
				border : false,
				enctype : 'multipart/form-data',
				labelWidth : 60,
				waitMsgTarget : true,
				bodyStyle : 'padding : 3px 10px; background-color:' + sys_bgcolor,
				defaults : {
					blankText : '',
					invalidText : '',
					allowBlank : false,
					anchor : '100%',
					xtype : 'textfield'
				},
				items : [{
							fieldLabel : '任务名称',
							name : 'node',
							blankText : '',
							invalidText : '',
							anchor : '100%'
						}, {
							xtype : 'fileuploadfield',
							emptyText : '选择文件',
							fieldLabel : '文件路径',
							name : 'file',
							buttonText : '',
							buttonCfg : {
								iconCls : 'icon-upload-excel'
							},
							validator : function(v) {
								v = v.trim();
								if (v) {
									if (v.indexOf('.xls') < 0)
										return '文件格式不正确';
									return true;
								}
								return '请选择文件';
							}
						}, this.endTimeField],
				buttons : [{
							text : '保存',
							handler : function() {
								me.saveLeaf(this, menu.currentNode);
							}
						}, {
							text : '关闭',
							handler : function() {
								me.leafWin.hide();
							}
						}]
			});

	var leafWin = this.leafWin = new Ext.Window({
				width : 400,
				border : false,
				title : '新建任务',
				modal : true,
				closable : true,
				closeAction : 'hide',
				resizable : false,
				items : [leafForm]
			});

	this.on('contextmenu', function(node, event) {
				if (node.leaf) {
					Ext.getCmp('createLeaf').hide();
					Ext.getCmp('createDic').hide();
				} else {
					Ext.getCmp('createLeaf').show();
					Ext.getCmp('createDic').show();
				}
				menu.currentNode = node;
				menu.showAt(event.getXY());
				event.preventDefault();
			});

	this.on('click', function(node) {
				if (this.isAttr(node)) {
					var p = this.showTab(AbstractSemanticTaskDetailPanel, node.id, node.text, 'icon-ontology-class', true);
					p.loadData(node.id);
				} else {
					var p = this.showTab(AbstractSemanticTaskAssignmentPanel, node.id, node.text, 'icon-ontology-class', true);
					p.loadData(node.id);
				}
			});
}

Ext.extend(NavAbstractSemanticTaskPanel, Ext.tree.TreePanel, {
			refreshNode : function(root) {
				if (root)
					node = this.getRootNode();
				else
					node = this.getSelectionModel().getSelectedNode();
				this.getLoader().load(node);
				node.expand();
			},
			getSelectedNode : function() {
				return this.getSelectionModel().getSelectedNode()
			},
			isAttr : function(node) {
				return node.attributes.leaf == true;
			},
			saveDic : function(btn, node) {
				var dicform = btn.ownerCt.ownerCt.getForm(), me = this;
				if (dicform.isValid()) {
					Ext.Ajax.request({
						url : 'abstract-semantic-task!saveDic.action',
						params : {
							node : dicform.getValues().node,
							pid : node.id
						},
						success : function(response, action) {
							me.dicWin.hide();
							me.refreshNode(node);
							var data = Ext.util.JSON.decode(response.responseText);
							Dashboard.setAlert(data.message);
						},
						failure : function(response) {
							var data = Ext.util.JSON.decode(response.responseText);
							Dashboard.setAlert(data.message);
						}
					});
				}
			},
			saveLeaf : function(btn, node) {
				var leafForm = btn.ownerCt.ownerCt.getForm(), me = this;
				if (leafForm.isValid()) {
					leafForm.submit({
						waitMsg : '正在上传',
						params : {
							pid : node.id
						},
						url : 'abstract-semantic-task!saveLeaf.action',
						success : function(form, act) {
								me.leafWin.hide();
								var timer = new ProgressTimer({
									initData : act.result.data,
									progressText : 'percent',
									progressId : 'importDetail',
									boxConfig : {
										msg : '导入中',
										title : '导入语义任务'
									},
									finish : function(p, response) {
										Dashboard.setAlert("任务新建成功！");
									},
									scope : this
								});
								timer.start();
						},
						failure : function(response, action) {
							var data = Ext.util.JSON.decode(response.responseText);
							Dashboard.setAlert(data.message);
						}
					});
				}
			},
			remove : function(node) {
				var me = this;
				Ext.Msg.confirm('提示：', node.leaf ? '确认删除【' + node.text + '】' : '确定要删除【' + node.getPath('text').replace('/root', '') + '】及以下所有分类吗', function(btn, text) {
							if (btn == 'yes') {
								Ext.Ajax.request({
											url : 'abstract-semantic-task!remove.action',
											params : {
												node : node.id
											},
											success : function(response) {
												me.refreshNode(node);
												var data = Ext.util.JSON.decode(response.responseText);
												Dashboard.setAlert(data.message);
											},
											failure : function(response, action) {
												var data = Ext.util.JSON.decode(response.responseText);
												Dashboard.setAlert(data.message);
											}
										});
							}
						})
			},
			updateUI : function(node) {
				console.info(node);
				var me = this;
				var rec = new Ext.data.Record(Ext.apply({}, node.attributes));
				rec.set('node', rec.get('text'));
				rec.set('taskId', rec.get('id'));
				rec.set('ispub', rec.get('ispub'))
				me.updateForm.getForm().loadRecord(rec);
				me.updateWin.show();
			},
			updateDic : function(node) {
				var me = this, updateForm = me.updateForm.getForm();
				if (updateForm.isValid()) {
					Ext.Ajax.request({
						url : 'abstract-semantic-task!updateDic.action',
						params : {
							node : updateForm.getValues().node,
							taskId : updateForm.getValues().taskId,
							ispub :  updateForm.getValues().ispub
						},
						success : function(response, action) {
							me.updateWin.hide();
							me.refreshNode(node);
							var data = Ext.util.JSON.decode(response.responseText);
							Dashboard.setAlert(data.message);
						},
						failure : function(response) {
							var data = Ext.util.JSON.decode(response.responseText);
							Dashboard.setAlert(data.message);
						}
					});
				}
			
			}
		});

//Dashboard.registerNav(NavAbstractSemanticTaskPanel, 1);