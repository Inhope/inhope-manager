AssistNavPanel = function() {
	var me = this;
	var menu = Dashboard.u().filterAllowed([{
		  id : '1',
		  authName : 'kb.misc.aiconsole',
		  name : '引擎调试器',
		  icon : 'icon-aitester',
		  handler : function() {
			  if (!AssistNavPanel.aiTesterWindow)
				  AssistNavPanel.aiTesterWindow = new AITesterWindow({
					    serverUrl : 'aitester!ask.action'
				    });
			  AssistNavPanel.aiTesterWindow.show();
		  }
	  }, {
		  id : '7',
		  authName : 'kb.misc.ctx',
		  name : '上下文辅助',
		  icon : 'icon-context'
	  }, {
		  id : '71',
		  name : '规则管理',
		  authName : "kb.misc.ctx.rule",
		  parent : '7',
		  module : 'context',
		  icon : 'icon-context-reverse-ask',
		  panelClass : ContextRulePanel
	  }, {
		  id : '72',
		  name : '节点管理',
		  authName : "kb.misc.ctx.node",
		  parent : '7',
		  module : 'contextNode',
		  icon : 'icon-context-node',
		  panelClass : ContextNodePanel
	  }, {
		  id : '2',
		  name : '指令管理',
		  authName : "kb.misc.ins",
		  module : 'instruction',
		  icon : 'icon-instruction',
		  panelClass : InstructionPanel
	  }, {
		  id : '3',
		  name : '维度管理',
		  authName : "kb.misc.dim",
		  module : 'dimension',
		  icon : 'icon-dimension',
		  panelClass : DimensionPanel
	  }, {
		  id : '4',
		  name : '停词管理',
		  authName : "kb.misc.stopword",
		  module : 'stopWord',
		  icon : 'icon-wordclass-category',
		  panelClass : StopWordPanel
	  }, {
		  id : '5',
		  name : '前后缀管理',
		  authName : "kb.misc.presuffix",
		  module : 'preSuffix',
		  icon : 'icon-presuffix',
		  panelClass : PreSuffixPanel
	  }, {
		  id : '6',
		  name : '模块管理',
		  authName : "kb.misc.module",
		  module : 'moduleConfig',
		  icon : 'icon-module-config',
		  panelClass : ModuleConfigPanel
	  }, {
		  id : '8',
		  name : '知识库跑错',
		  authName : "kb.misc.chkw",
		  module : 'checkWrong',
		  icon : 'icon-ontology-sync',
		  panelClass : CheckWrongPanel
	  }, {
		  id : '9',
		  name : '基础词管理',
		  authName : "kb.misc.bword",
		  module : 'basewordManager',
		  icon : 'icon-wordclass-root-sync',
		  panelClass : AIWordClassPanel
	  }, {
		  id : '11',
		  name : '系统词管理',
		  authName : "kb.misc.sysword",
		  icon : 'icon-wordclass-root-sync'
	  }, {
		  id : '111',
		  name : '数据管理',
		  authName : "kb.misc.sysword.1",
		  module : 'kb.misc.sysword.1',
		  icon : 'icon-wordclass-root-sync',
		  parent : '11',
		  panelClass : SysWordClassPanel
	  }, {
		  id : '112',
		  name : '操作管理',
		  authName : "kb.misc.sysword.2",
		  module : 'kb.misc.sysword.2',
		  icon : 'icon-wordclass-root-sync',
		  parent : '11',
		  panelClass : AISyswordOperationPanel
	  }, {
		  id : '10',
		  name : '通用聊天管理',
		  authName : "kb.misc.commchat",
		  module : 'commonChatManager',
		  icon : 'icon-common-chat',
		  panelClass : CommonChatPanel
	  }, {
		  'id' : '12',
		  name : '知识附件管理',
		  authName : 'kb.misc.fileexp',
		  module : 'FileExportManager',
		  'icon' : 'icon-context',
		  handler : function() {
			  new FileTransportManager().showWindow();
		  }
	  }, {
		  'id' : '13',
		  name : '语义表达式工具',
		  authName : 'kb.misc.analyzer',
		  module : 'FileAnalyzerManager',
		  'icon' : 'icon-context',
		  handler : function() {
			  new AnalyzerManager().showWindow();
		  }
	  }, {
		  'id' : '14',
		  name : '强制分词管理',
		  authName : 'kb.misc.participle',
		  module : 'enforcementParticipleManager',
		  'icon' : 'icon-participle',
		  panelClass : EnforcementParticiplePanel
	  }, {
		  'id' : '15',
		  name : '新词发现',
		  authName : 'kb.misc.addseganalyse',
		  module : 'addSegAnalysePanel',
		  'icon' : 'icon-wordclass-root-add',
		  panelClass : AddSegAnalysePanel
	  }
	  // {
	  // id : '16',
	  // authName : 'kb.misc.wordsegment',
	  // name : '分词工具',
	  // icon : 'icon-module-config-edit',
	  // handler : function() {
	  // if (!AssistNavPanel.wordsegmentWindow)
	  // AssistNavPanel.wordSegmentWindow = new WordSegmentWindow({
	  //									
	  // });
	  // AssistNavPanel.wordSegmentWindow.show();
	  // }
	  // },
	  ]);
	if (sys_labs || Dashboard.u().isSupervisor()){
		menu.push({
				id : '17',
				name : '推荐工具',
				icon : 'icon-module-config-edit',
				handler : function() {
					if (!AssistNavPanel.semanticRecommendWindow)
						AssistNavPanel.semanticRecommendWindow = new SemanticRecommendWindow({
									
								});
					AssistNavPanel.semanticRecommendWindow.show();
				}
			})
	}
	if (Dashboard.isTimerTaskEnabled())
		menu.push({
			  id : '18',
			  name : '定时任务',
			  icon : 'icon-timer',
			  module : 'timerTaskListPanel',
			  panelClass : TimerTaskListPanel
		  });
	if (Dashboard.getModuleProp('kbmgr.biz_mode_host_addr'))
		menu.push({
			  id : '19',
			  name : '业务文章管理',
			  icon : 'icon-context',
			  module : 'timerTaskListPanel',
			  handler : function() {
			  	var objectPanel = this.showTab(IFramePanel, 'misc.biz-mgr-article', '业务文章管理' , 'icon-context', true);
				  var baseurl = Dashboard.getModuleProp('kbmgr.biz_mode_host_addr')
				  if (!baseurl){
				  	alert('please config "kbmgr.biz_mode_host_addr" first!')
				  	return
				  }
				  objectPanel.setLocation(baseurl+encodeURIComponent(String.format('/app/biztpl/biz-tpl.htm?username={0}', encodeURIComponent(Dashboard.u().getUsername()))))
			  }.dg(this)
		  });
		menu.push({
			  id : '20',
			  name : '业务属性管理',
			  icon : 'icon-ontology-attr',
			  module : 'timerTaskListPanel',
			  handler : function() {
			  	var objectPanel = this.showTab(IFramePanel, 'misc.biz-mgr-attr', '业务属性管理' , 'icon-ontology-attr', true);
				  var baseurl = Dashboard.getModuleProp('kbmgr.biz_mode_host_addr')
				  if (!baseurl){
				  	alert('please config "kbmgr.biz_mode_host_addr" first!')
				  	return
				  }
				  objectPanel.setLocation(baseurl+encodeURIComponent(String.format('/app/biztpl/biz-tpl-property.htm?username={0}', encodeURIComponent(Dashboard.u().getUsername()))))
			  }.dg(this)
		  });
	var rootNode = new Ext.tree.TreeNode({
		  id : '0',
		  text : 'root',
		  leaf : false
	  });
	var _nodeMap = {};

	var versionStandard = isVersionStandard();
	for (var i = 0; i < menu.length; i++) {
		var _item = menu[i];
		if (versionStandard) {
			if ('checkWrong' != _item.module && 'commonChatManager' != _item.module) {
				continue;
			}
		}
		var _node = new Ext.tree.TreeNode({
			  id : _item.id,
			  text : _item.name,
			  iconCls : _item.icon,
			  module : _item.module,
			  panelClass : _item.panelClass,
			  handler : _item.handler
		  });
		_nodeMap[_item.id] = _node;
		if (!_item.parent)
			rootNode.appendChild(_node);
		else
			_nodeMap[_item.parent].appendChild(_node);
	}

//	if (sys_labs || Dashboard.u().isSupervisor()){
//		var taskNode = this.taskNode = new Ext.tree.AsyncTreeNode({
//					text : "语义任务",
//					id : '0',
//					iconCls : 'icon-user-login',
//					loader : new Ext.tree.TreeLoader({
//								dataUrl : 'abstract-semantic-task!list.action'
//							})
//				});
//		
//		rootNode.appendChild(taskNode);
//	}

	var config = {
		id : 'assistNavId',
		title : '高级功能管理',
		border : false,
		autoScroll : true,
		rootVisible : false,
		lines : false,
		root : rootNode
	};
	AssistNavPanel.superclass.constructor.call(this, config);

	this.modulePanels = {};
	this.on('click', function(n) {
		  if (!n)
			  return false;
		  if (n.attributes.handler) {
			  n.attributes.handler.call(n, arguments);
			  return false;
		  }
		  if (n.attributes.cls == '1') {
			  if (this.isAttr(n)) {
				  var p = this.showTab(AbstractSemanticTaskDetailPanel, n.id, n.text, 'icon-wordclass-root', true);
				  p.loadData(n.parentNode.id);
			  } else {
				  var p = this.showTab(AbstractSemanticTaskAssignmentPanel, n.id, n.text, 'icon-wordclass-category', true);
				  p.loadData(n.id);
			  }
		  } else {
			  var module = n.attributes.module;
			  var panelClass = n.attributes.panelClass;
			  var p = me.showTab(panelClass, module + 'Tab', n.text, n.attributes.iconCls, true);
			  if(module == 'checkWrong')
			     AssistNavPanel.checkWrongPanel = p;
			  if (p)
				  p.loadData();
		  }
	  });

//	var menu = new Ext.menu.Menu({
//		  items : [{
//			    id : 'createLeaf',
//			    text : '新建任务',
//			    iconCls : 'icon-add',
//			    width : 150,
//			    handler : function() {
//				    me.leafWin.show();
//			    }
//		    }, {
//			    id : 'updateLeaf',
//			    text : '修改',
//			    iconCls : 'icon-wordclass-edit',
//			    width : 150,
//			    handler : function() {
//				    me.updateUI(menu.currentNode);
//			    }
//		    }, {
//			    id : 'removeLeaf',
//			    text : '删除',
//			    iconCls : 'icon-wordclass-del',
//			    width : 150,
//			    handler : function() {
//				    me.remove(menu.currentNode);
//			    }
//		    }]
//	  });

//	var _taskId = this._taskId = new Ext.form.Hidden({
//		  name : 'taskId'
//	  });
//
//	var updateForm = this.updateForm = new Ext.form.FormPanel({
//		  frame : false,
//		  border : false,
//		  labelWidth : 60,
//		  waitMsgTarget : true,
//		  bodyStyle : 'padding : 3px 10px; background-color:#d3e1f1;',
//		  defaults : {
//			  blankText : '',
//			  invalidText : '',
//			  anchor : '100%',
//			  xtype : 'textfield'
//		  },
//		  items : [{
//			    fieldLabel : '名称',
//			    allowBlank : false,
//			    name : 'node',
//			    blankText : '',
//			    invalidText : '',
//			    anchor : '100%'
//		    }, this._taskId, new Ext.form.Hidden({
//			      name : 'ispub'
//		      }), this._taskId, new Ext.form.Hidden({
//			      name : 'url'
//		      })],
//		  buttons : [{
//			    text : '保存',
//			    handler : function() {
//				    me.updateDic(menu.currentNode);
//			    }
//		    }, {
//			    text : '关闭',
//			    handler : function() {
//				    me.updateWin.hide();
//			    }
//		    }]
//	  });
//
//	var updateWin = this.updateWin = new Ext.Window({
//		  width : 300,
//		  border : false,
//		  title : '修改',
//		  modal : true,
//		  closable : true,
//		  closeAction : 'hide',
//		  resizable : false,
//		  items : [updateForm]
//	  });
//
//	this.endTimeField = new Ext.form.DateField({
//		  fieldLabel : '结束时间',
//		  format : 'Y-m-d',
//		  editable : false,
//		  name : 'endTime',
//		  anchor : '100%'
//	  })
//
//	this.userstore = new Ext.data.JsonStore({
//		  fields : [{
//			    name : 'username',
//			    type : 'string'
//		    }],
//		  autoLoad : true,
//		  url : 'abstract-semantic-task!getUsers.action'
//	  });
//
//	var leafForm = this.leafForm = new Ext.form.FormPanel({
//		  fileUpload : true,
//		  frame : false,
//		  border : false,
//		  enctype : 'multipart/form-data',
//		  labelWidth : 60,
//		  waitMsgTarget : true,
//		  bodyStyle : 'padding : 3px 10px; background-color:#d3e1f1;',
//		  defaults : {
//			  blankText : '',
//			  invalidText : '',
//			  allowBlank : false,
//			  anchor : '100%',
//			  xtype : 'textfield'
//		  },
//		  items : [{
//			    fieldLabel : '任务名称',
//			    name : 'node',
//			    blankText : '',
//			    invalidText : '',
//			    anchor : '100%'
//		    }, {
//			    fieldLabel : '分配用户',
//			    xtype : 'combo',
//			    displayField : 'username',
//			    name : 'username',
//			    typeAhead : true,
//			    mode : 'local',
//			    forceSelection : true,
//			    triggerAction : 'all',
//			    selectOnFocus : true,
//			    editable : false,
//			    store : me.userstore
//		    }, {
//			    xtype : 'fileuploadfield',
//			    emptyText : '选择文件',
//			    fieldLabel : '文件路径',
//			    name : 'file',
//			    buttonText : '',
//			    buttonCfg : {
//				    iconCls : 'icon-upload-excel'
//			    },
//			    validator : function(v) {
//				    v = v.trim();
//				    if (v) {
//					    if (v.indexOf('.xls') < 0)
//						    return '文件格式不正确';
//					    return true;
//				    }
//				    return '请选择文件';
//			    }
//		    }, this.endTimeField],
//		  buttons : [{
//			    text : '保存',
//			    handler : function() {
//				    me.saveLeaf(this, menu.currentNode);
//			    }
//		    }, {
//			    text : '关闭',
//			    handler : function() {
//				    me.leafWin.hide();
//			    }
//		    }]
//	  });
//
//	var leafWin = this.leafWin = new Ext.Window({
//		  width : 400,
//		  border : false,
//		  title : '新建任务',
//		  modal : true,
//		  closable : true,
//		  closeAction : 'hide',
//		  resizable : false,
//		  items : [leafForm]
//	  });
//
//	this.on('contextmenu', function(node, event) {
//		  if (node.attributes.cls == '1') {
//			  // 有分配权限
//			  if (isAllow == '1') {
//				  if (node.leaf) {
//					  Ext.getCmp('createLeaf').hide();
//					  Ext.getCmp('updateLeaf').show();
//				  } else {
//					  Ext.getCmp('createLeaf').show();
//					  Ext.getCmp('updateLeaf').hide();
//				  }
//				  if (node.id == 0) {
//					  Ext.getCmp('removeLeaf').hide();
//				  } else {
//					  if (node.leaf) {
//						  Ext.getCmp('removeLeaf').show();
//					  } else {
//						  Ext.getCmp('removeLeaf').hide();
//					  }
//				  }
//				  menu.currentNode = node;
//				  menu.showAt(event.getXY());
//				  event.preventDefault();
//			  } else {
//				  Ext.getCmp('createLeaf').hide();
//				  Ext.getCmp('updateLeaf').hide();
//				  Ext.getCmp('removeLeaf').hide();
//			  }
//		  }
//	  })
}

Ext.extend(AssistNavPanel, Ext.tree.TreePanel, {
	  authName : 'kb.misc',
	  isAttr : function(node) {
		  return node.attributes.leaf == true;
	  },
	  refreshNode : function(root) {
		  if (root)
			  node = this.getRootNode();
		  else
			  node = this.getSelectionModel().getSelectedNode();
		  this.getLoader().load(node);
		  node.expand();
	  },
	  getSelectedNode : function() {
		  return this.getSelectionModel().getSelectedNode();
	  },
	  isAttr : function(node) {
		  return node.attributes.leaf == true;
	  },
	  saveLeaf : function(btn, node) {
		  var leafForm = btn.ownerCt.ownerCt.getForm(), me = this;
		  if (leafForm.isValid()) {
			  leafForm.submit({
				    waitMsg : '正在上传',
				    params : {
					    url : node.id
				    },
				    url : 'abstract-semantic-task!saveLeaf.action',
				    success : function(form, act) {
					    me.leafWin.hide();
					    var timer = new ProgressTimer({
						      initData : act.result.data,
						      progressText : 'percent',
						      progressId : 'importDetail',
						      boxConfig : {
							      msg : '导入中',
							      title : '导入语料'
						      },
						      finish : function(p, response) {
							      node.reload();
							      Dashboard.setAlert("任务新建成功！");
						      },
						      scope : this
					      });
					    timer.start();
				    },
				    failure : function(response) {
					    var data = Ext.util.JSON.decode(response.responseText);
					    Dashboard.setAlert(data.message);
				    }
			    });
		  }
	  },
	  remove : function(node) {
		  var me = this;
		  Ext.Msg.confirm('提示：', node.leaf ? '确认删除【' + node.text + '】' : '确定要删除【' + node.getPath('text').replace('/root', '') + '】及以下所有分类吗', function(btn, text) {
			    if (btn == 'yes') {
				    Ext.Ajax.request({
					      url : 'abstract-semantic-task!remove.action',
					      params : {
						      node : node.id,
						      url : node.parentNode.id
					      },
					      success : function(response) {
						      var source = node.parentNode
						      source.removeChild(node);
						      var data = Ext.util.JSON.decode(response.responseText);
						      Dashboard.setAlert(data.message);
					      },
					      failure : function(response, action) {
						      var data = Ext.util.JSON.decode(response.responseText);
						      Dashboard.setAlert(data.message);
					      }
				      });
			    }
		    })
	  },
	  updateUI : function(node) {
		  var me = this;
		  var rec = new Ext.data.Record(Ext.apply({}, node.attributes));
		  rec.set('node', rec.get('text'));
		  rec.set('taskId', rec.get('id'));
		  rec.set('ispub', rec.get('ispub'));
		  rec.set('url', node.parentNode.id);
		  me.updateForm.getForm().loadRecord(rec);
		  me.updateWin.show();
	  },
	  updateDic : function(node) {
		  var me = this, updateForm = me.updateForm.getForm();
		  if (updateForm.isValid()) {
			  Ext.Ajax.request({
				    url : 'abstract-semantic-task!updateDic.action',
				    params : {
					    node : updateForm.getValues().node,
					    taskId : updateForm.getValues().taskId,
					    ispub : updateForm.getValues().ispub,
					    url : updateForm.getValues().url
				    },
				    success : function(response, action) {
					    me.updateWin.hide();
					    var data = Ext.util.JSON.decode(response.responseText);
					    Dashboard.setAlert(data.message);
				    },
				    failure : function(response) {
					    var data = Ext.util.JSON.decode(response.responseText);
					    Dashboard.setAlert(data.message);
				    }
			    });
		  }

	  }
  });
ShortcutToolRegistry.register('addSegAnalysePanel', function() {
	  var p = Dashboard.navPanel.items.get(0).showTab(AddSegAnalysePanel, 'addSegAnalysePanelTab', '新词发现', 'icon-wordclass-root-add', true);
	  if (p)
		  p.loadData();
  }.dg(this));
ShortcutToolRegistry.register('aiTesterWindow', function() {
	  if (!AssistNavPanel.aiTesterWindow)
		  AssistNavPanel.aiTesterWindow = new AITesterWindow({
			    serverUrl : 'aitester!ask.action'
		    });
	  AssistNavPanel.aiTesterWindow.show();
  }.dg(this));

Dashboard.registerNav(AssistNavPanel, 4);