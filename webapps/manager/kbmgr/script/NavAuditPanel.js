NavAuditPanel = function() {
	var menuModule = [];
	menuModule = Dashboard.getModuleProp('kbmgr.enableAudit') == "true" ? [{
		  id : '1',
		  name : '知识点',
		  icon : 'icon-expand'
	  }, {
		  id : '11',
		  parent : '1',
		  // authNamePattern :
		  // /ALL|(kb.obj.instance|kb.obj|kb).ALL|kb.catecontent.*.ALL|.*\.AUD/,
		  authNamePattern : /^(ALL|(kb.obj.instance|kb.obj|kb).ALL|kb.catecontent.*.ALL|.*\.AUD)$/,
		  name : '待我审批的',
		  panelClass : function() {
			  return new obj.list.AuditPanel({
				    auditState : STATE_AUD_SUBMIT
			    })
		  },
		  icon : 'icon-context'
	  }, {
		  id : '12',
		  parent : '1',
		  name : '我提交审批的',
		  panelClass : function() {
			  return new obj.list.AuditPanel({
				    myView : true,
				    auditState : STATE_AUD_SUBMIT,
				    readMode : true
			    })
		  },
		  icon : 'icon-context'
	  }, {
		  id : '13',
		  parent : '1',
		  name : '被驳回的',
		  panelClass : function() {
			  return new obj.list.AuditPanel({
				    myView : true,
				    auditState : STATE_AUD_REJECT,
				    readMode : true
			    })
		  },
		  icon : 'icon-context'
	  }, {
		  id : '2',
		  name : '实例',
		  icon : 'icon-expand'
	  }, {
		  id : '21',
		  parent : '2',
		  authNamePattern : /^(ALL|(kb.obj.instance|kb.obj|kb).ALL|kb.catecontent.*.ALL|.*\.AUD)$/,
		  name : '待我审批的',
		  panelClass : function() {
			  return new AuditCatObjPanel({
				    auditState : STATE_AUD_SUBMIT,
				    isObject : 1
			    })
		  },
		  icon : 'icon-context'
	  }, {
		  id : '22',
		  parent : '2',
		  name : '我提交审批的',
		  panelClass : function() {
			  return new AuditCatObjPanel({
				    auditState : STATE_AUD_SUBMIT,
				    readMode : true,
				    mySubmit : true,
				    isObject : 1
			    })
		  },
		  icon : 'icon-context'
	  }, {
		  id : '23',
		  parent : '2',
		  name : '被驳回的',
		  panelClass : function() {
			  return new AuditCatObjPanel({
				    auditState : STATE_AUD_REJECT,
				    readMode : true,
				    isObject : 1
			    })
		  },
		  icon : 'icon-context'
	  }, {
		  id : '3',
		  name : '分类',
		  icon : 'icon-expand'
	  }, {
		  id : '31',
		  parent : '3',
		  authNamePattern : /^(ALL|(kb.obj.instance|kb.obj|kb).ALL|kb.catecontent.*.ALL|.*\.AUD)$/,
		  name : '待我审批的',
		  panelClass : function() {
			  return new AuditCatObjPanel({
				    auditState : STATE_AUD_SUBMIT,
				    isObject : 0
			    })
		  },
		  icon : 'icon-context'
	  }, {
		  id : '32',
		  parent : '3',
		  name : '我提交审批的',
		  panelClass : function() {
			  return new AuditCatObjPanel({
				    auditState : STATE_AUD_SUBMIT,
				    readMode : true,
				    mySubmit : true,
				    isObject : 0
			    })
		  },
		  icon : 'icon-context'
	  }, {
		  id : '33',
		  parent : '3',
		  name : '被驳回的',
		  panelClass : function() {
			  return new AuditCatObjPanel({
				    auditState : STATE_AUD_REJECT,
				    readMode : true,
				    isObject : 0
			    })
		  },
		  icon : 'icon-context'
	  }] : [];
	if (Dashboard.getModuleProp('kbmgr.wordclass.enableAudit') == "true") {
		menuModule.push({
			  id : '4',
			  name : '词类',
			  icon : 'icon-expand'
		  }, {
			  id : '41',
			  parent : '4',
			  name : '待审核详情',
			  // authName: 'kb.word.AUD',
			  panelClass : function() {
				  return new PendingWordclassAuditPanel();
			  },
			  icon : 'icon-word-audit'
		  }, {
			  id : '42',
			  parent : '4',
			  name : '已审核详情',
			  panelClass : function() {
				  return new AuditedWordclassPanel();
			  },
			  icon : 'icon-user-login'
		  });
	}
	var menu = Dashboard.u().filterAllowed(menuModule);
	var rootNode = new Ext.tree.TreeNode({
		  id : '0',
		  text : 'root'
	  });
	var _nodeMap = {};
	for (var i = 0; i < menu.length; i++) {
		var _item = menu[i];
		var _node = new Ext.tree.TreeNode({
			  id : _item.id,
			  text : _item.name,
			  iconCls : _item.icon,
			  module : _item.module,
			  panelClass : _item.panelClass,
			  handler : _item.handler
		  });
		_nodeMap[_item.id] = _node;
		if (!_item.parent)
			rootNode.appendChild(_node);
		else
			_nodeMap[_item.parent].appendChild(_node);
	}
	var config = {
		title : '审批管理',
		border : false,
		autoScroll : true,
		rootVisible : false,
		lines : false,
		root : rootNode
	};
	NavAuditPanel.superclass.constructor.call(this, config);

	this.on('afterrender', function(treePanel) {
		  this.getRootNode().expand(true, true);
		  if (treePanel.ownerCt.items.length == 1 && this.isVisible()) {
			  treePanel.fireEvent('expand', treePanel);
		  }
	  });
	this.on('expand', function() {
		  if (!this.getSelectionModel().getSelectedNode()) {
			  this.getRootNode().childNodes[0].childNodes[0].select();
			  this.getRootNode().childNodes[0].childNodes[0].fireEvent('click', this.getRootNode().childNodes[0].childNodes[0]);
		  }
	  });
	// this.on('expandnode', function(node) {
	// if (firstInit && 2 == node.id.length) {
	// node.select();
	// node.fireEvent('click', node);
	// firstInit = false;
	// }
	// });

	this.modulePanels = {};
	this.on('click', function(n) {
		  if (!n)
			  return false;
		  if (n.attributes.handler) {
			  n.attributes.handler.call(n, arguments);
			  return false;
		  }
		  var module = n.attributes.module ? n.attributes.module : 'kb.audit.' + n.id;
		  var panelClass = n.attributes.panelClass;
		  panelClass.creator = true
		  var p = this.showTab(panelClass, module + 'Tab', n.text, n.attributes.iconCls, true);
		  if (p.onTabShow)
			  p.onTabShow();
		  if (p.loadData)
			  p.loadData();
	  });
}

Ext.extend(NavAuditPanel, Ext.tree.TreePanel, {});
if (Dashboard.getModuleProp('kbmgr.enableAudit') || Dashboard.getModuleProp('kbmgr.wordclass.enableAudit'))
	Dashboard.registerNav(NavAuditPanel, 7);