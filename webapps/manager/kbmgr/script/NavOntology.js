

OntologyNavPanel = function(cfg) {
	cfg = cfg || {}
	this.otclasstype = window.OntologyNavPanel_classtype;
	var self = this;
	var finalCfg = Ext.apply({
		  title : '本体类管理',
		  width : 200,
		  minSize : 175,
		  maxSize : 400,
		  collapsible : true,
		  rootVisible : false,
		  enableDD : true,
		  border : false,
		  lines : false,
		  autoScroll : true,
		  containerScroll : true,
		  root : {
			  id : '0',
			  text : 'root',
			  iconCls : 'icon-ontology-root',
			  lazyLoad : true
		  },
		  dataUrl : 'ontology-class!list.action'
	  }, cfg);
	var tbaritems = [{
		  blankText : "请输入本体类别名称",
		  xtype : 'textfield',
		  ref : 'searchNodeText',
		  width : cfg.tbar_searchTextWidth ? cfg.tbar_searchTextWidth : 115,
		  listeners : {
			  specialkey : function(f, e) {
				  if (e.getKey() == e.ENTER) {
					  self.searchNode(f);
				  }
			  }
		  }
	  }, {
		  text : cfg.compact ? '' : '查找',
		  iconCls : 'icon-search',
		  xtype : 'splitbutton',
		  menu : {
			  items : [{
				    text : '精确匹配',
				    checked : false,
				    checkHandler : function(btn, checked) {
					    this.matchExactly = checked
				    }.dg(this)
			    }]
		  },
		  width : cfg.compact ? 20 : 50,
		  handler : function() {
			  self.searchNode();
		  }
	  }, {
		  text : cfg.compact ? '' : '刷新',
		  iconCls : 'icon-refresh',
		  width : cfg.compact ? 20 : 60,
		  handler : function() {
			  self.refreshNode(true);
		  }
	  }]
	if (!cfg.compact) {
		tbaritems = tbaritems.concat(['-', {
			  text : '添加本体基类',
			  authName : 'kb.cls.C',
			  iconCls : 'icon-ontology-root-add',
			  width : 60,
			  handler : function() {
				  self.addNode(self.root)
			  }
		  }, this.otclasstype == 3 ? '' : {
			  text : '导入本体类',
			  authName : 'kb.cls.IMP',
			  iconCls : 'icon-ontology-import',
			  handler : this.doImport.dg(this, [])
		  }, !this.allowAbsSemantic() || this.otclasstype == 0 ? '' : {
			  text : '导入抽象语义',
			  authName : 'kb.cls.IMP',
			  iconCls : 'icon-ontology-import',
			  handler : this.doImport.dg(this, [null, 3])
		  }, this.otclasstype == 3 ? '' : {
			  text : '导出本体类',
			  authName : 'kb.cls.EXP',
			  iconCls : 'icon-ontology-export',
			  handler : this.doExport.dg(this, [])
		  }, !this.allowAbsSemantic() || this.otclasstype == 0 ? '' : {
			  text : '导出抽象语义',
			  authName : 'kb.cls.IMP',
			  iconCls : 'icon-ontology-export',
			  width : 50,
			  handler : function() {
				  this.doExport(null, 3)
			  }.dg(this)
		  }, this.otclasstype == 3 ? '' : {
			  text : '同步本体类',
			  authName : 'kb.cls.S',
			  iconCls : 'icon-ontology-sync',
			  handler : function() {
				  Ext.Ajax.request({
					    url : 'ontology-class!syncAI.action',
					    params : {
						    type : 0
					    },
					    success : function(response) {
						    if (response.responseText) {
							    var timer = new ProgressTimer({
								      initData : Ext.decode(response.responseText),
								      progressText : 'percent',
								      progressId : 'classSyncAI',
								      boxConfig : {
									      title : '同步本体类'
								      },
								      scope : this
							      });
							    timer.start();
						    }
					    },
					    scope : this
				    })
			  }
		  }, this.otclasstype == 0 ? '' : {
			  text : '同步抽象语义',
			  authName : 'kb.cls.S',
			  iconCls : 'icon-ontology-sync',
			  handler : function() {
				  Ext.Ajax.request({
					    url : 'ontology-class!syncAI.action',
					    params : {
						    type : 3
					    },
					    success : function(response) {
						    if (response.responseText) {
							    var timer = new ProgressTimer({
								      initData : Ext.decode(response.responseText),
								      progressText : 'percent',
								      progressId : 'classSyncAI',
								      boxConfig : {
									      title : '同步抽象语义'
								      },
								      scope : this
							      });
							    timer.start();
						    }
					    },
					    scope : this
				    })
			  }
		  }])
	}
	if (cfg.tbar_items)
		tbaritems = tbaritems.concat(cfg.tbar_items);

	finalCfg.tbar = new Ext.Toolbar({
		  enableOverflow : cfg.tbar_disableOverflow ? false : true,
		  items : Dashboard.u().filterAllowed(tbaritems)
	  })
	OntologyNavPanel.superclass.constructor.call(this, finalCfg);
	if (cfg.compact)
		return;
	var menu = new Ext.menu.Menu({
		  items : Dashboard.u().filterAllowed([{
			    text : '刷新',
			    iconCls : 'icon-refresh',
			    width : 50,
			    handler : function() {
				    self.refreshNode();
			    }
		    }, '-', {
			    text : '添加子类',
			    authName : 'kb.cls.C',
			    iconCls : 'icon-ontology-class-add',
			    width : 50,
			    handler : function() {
				    self.addNode(menu.currentNode);
			    }
		    }, {
			    text : '修改本体类',
			    authName : 'kb.cls.M',
			    iconCls : 'icon-ontology-class-edit',
			    width : 50,
			    handler : function() {
				    self.editNode(menu.currentNode);
			    }
		    }, {
			    ref : 'delItem',
			    text : '删除本体类',
			    authName : 'kb.cls.D',
			    iconCls : 'icon-ontology-class-delete',
			    width : 50,
			    handler : function() {
				    self.delNode(menu.currentNode);
			    }
		    }, '-', {
			    text : '添加属性',
			    authName : 'kb.cls.C',
			    iconCls : 'icon-ontology-attr-add',
			    width : 50,
			    handler : function() {
				    self.addAttr(menu.currentNode);
			    }
		    }, {
			    text : '粘贴属性',
			    authName : 'kb.cls.C',
			    ref : 'pasteButton',
			    iconCls : 'icon-ontology-attr-add',
			    disabled : true,
			    width : 50,
			    handler : function() {
				    self.pasteAttr(menu.currentNode);
			    }
		    }, {
			    ref : 'MN_IMP',
			    text : '导入数据',
			    authName : 'kb.cls.IMP',
			    iconCls : 'icon-ontology-import',
			    width : 50,
			    handler : function() {
				    this.doImport(this.getSelectionModel().getSelectedNode());
			    }.dg(this)
		    }, {
			    ref : 'MN_EXP',
			    text : '导出数据',
			    authName : 'kb.cls.IMP',
			    iconCls : 'icon-ontology-export',
			    width : 50,
			    handler : function() {
				    this.doExport(this.getSelectionModel().getSelectedNode())
			    }.dg(this)
		    }, '-', {
			    text : '查看类关系图',
			    authName : 'kb.cls.pic',
			    iconCls : 'icon-ontology-cate',
			    width : 50,
			    handler : function() {
				    var node = menu.currentNode;
				    node.expand();
				    // draw class diagram
				    if (node.isLoaded && node.isLoaded()) {
					    this.drawClassDiagram(node)
				    } else {
					    this._drawClassDiagramFlag = true
				    }
			    }.dg(this)
		    }])
	  });
	var attrMenu = new Ext.menu.Menu({
		  items : Dashboard.u().filterAllowed([{
			    text : '修改属性',
			    authName : 'kb.cls.M',
			    iconCls : 'icon-ontology-attr-edit',
			    width : 50,
			    handler : function() {
				    self.editAttr(attrMenu.currentNode);
			    }
		    }, {
			    text : '删除属性',
			    authName : 'kb.cls.D',
			    iconCls : 'icon-ontology-attr-del',
			    width : 50,
			    handler : function() {
				    self.delAttr(attrMenu.currentNode);
			    }
		    }, {
			    text : '复制属性',
			    authName : 'kb.cls.C',
			    iconCls : 'icon-ontology-attr',
			    width : 50,
			    handler : function() {
				    self.copyId = attrMenu.currentNode.id;
				    menu.pasteButton.setDisabled(false);
			    }
		    }])
	  });
	this.menu = menu;

	var _formPanel = new Ext.form.FormPanel({
		  frame : false,
		  border : false,
		  labelWidth : 60,
		  // autoHeight : true,
		  height : 150,
		  waitMsgTarget : true,
		  bodyStyle : 'background-color:' + sys_bgcolor,
		  bodyStyle : 'padding : 3px 10px',
		  defaults : {
			  blankText : '',
			  invalidText : '',
			  anchor : '95%',
			  xtype : 'textfield'
		  },
		  items : [new Ext.form.Hidden({
			      name : 'classId'
		      }), new Ext.form.Hidden({
			      name : 'parentClassId'
		      }), {
			    fieldLabel : '本体类名',
			    name : 'name',
			    allowBlank : false
		    }, {
			    xtype : 'textfield',
			    fieldLabel : '语义块',
			    name : 'sb'
		    }, {
			    xtype : 'checkbox',
			    fieldLabel : '抽象语义',
			    inputValue : 3,
			    name : 'type'
		    }, {
			    xtype : 'textfield',
			    fieldLabel : '领域',
			    name : 'domainId'
		    }].concat(!sys_labs?[]:{
			    xtype : 'checkbox',
			    fieldLabel : '真实本体',
			    inputValue : 100,
			    name : 'flag'
		    }),
		  buttons : [{
			    text : '保存',
			    handler : function() {
				    var obj = self.classFormWin.formPanel.getForm().getValues();
				    if (!self.validateName(obj.classId, obj.parentClassId, obj.name)) {
					    Ext.Msg.alert('提示', '类名称重复！');
					    return;
				    }
				    self.saveNode(obj, function() {
					      self.classFormWin.hide();
					      self.classFormWin.formPanel.getForm().reset();
				      });
			    }
		    }, {
			    text : '关闭',
			    handler : function() {
				    self.classFormWin.hide();
				    self.classFormWin.formPanel.getForm().reset();
			    }
		    }]
	  });
	this.classFormWin = new Ext.Window({
		  width : 300,
		  title : '本体类',
		  defaults : {
			  border : false
		  },
		  modal : true,
		  plain : true,
		  shim : true,
		  closable : false,
		  closeAction : 'hide',
		  collapsible : true,
		  resizable : false,
		  draggable : true,
		  animCollapse : true,
		  constrainHeader : true,
		  items : [_formPanel]
	  });
	this.classFormWin.formPanel = _formPanel;

	this.on('contextmenu', function(node, event) {
		  event.preventDefault();
		  node.select();
		  if (this.isAbsNode(node) && !this.allowAbsSemantic()) {
			  return
		  }
		  if (!this.isAttr(node)) {
			  menu.currentNode = node;
			  menu.showAt(event.getXY());
		  } else {
			  attrMenu.currentNode = node;
			  attrMenu.showAt(event.getXY());
		  }
	  });
	this.on('load', function(node) {
		  if (this._drawClassDiagramFlag) {
			  delete this._drawClassDiagramFlag
			  this.drawClassDiagram(node)
		  }
	  })
	this.on('click', function(node) {
		  if (!this.isAttr(node)) {
			  node.expand();
			  if (isVersionStandard()) {
				  var p = this.showTab(kb.cls.attrlist.Panel, 'class-attrlist-tab', '属性列表', 'icon-ontology-class', true);
				  p.loadData(node.id);
			  } else {
//				  var p = this.showTab(ClassDrawDiagramPanel, 'class-diagram-tab', '类关系图:' + node.text, 'icon-ontology-class', true);
				  var p = this.showTab(ClazzAttrDrawTabPanel, 'class-attrs-tab', '类属性:' + node.text, 'icon-ontology-class', true);
				  p.load(node.id, this);
			  }
		  } else if (!this.isAbsNode(node) || this.allowAbsSemantic()) {
			  this.editAttr(node);
		  }
	  })
	this.on('beforenodedrop', function(e) {
		  var node = e.dropNode;
		  var target = !this.isAttr(e.target) ? e.target : e.target.parentNode;
		  var source = node.parentNode

		  var c = Ext.Msg.confirm('提示', '确定要移动到&nbsp&nbsp' + this.getNodePath(target) + '&nbsp&nbsp吗', function(btn, text) {
			    if (btn == 'yes') {
				    Ext.Ajax.request({
					      url : 'ontology-class!moveAttr.action?attrId=' + node.id + "&classId=" + node.parentNode.id,
					      success : function(form, action) {

					      },
					      failure : function(response) {
						      e.cancel = true;
						      source.appendChild(node);
						      source.expand();
						      Ext.Msg.alert("错误", response.responseText);
					      }
				      });
			    } else {
				    e.cancel = true;
				    source.appendChild(node);
				    source.expand();
			    }
		    });
	  });
	var sv = isVersionStandard();
	var self = this
	this.getLoader().on('beforeload', function(loader) {
		  sv ? this.baseParams.listClassOnly = true : 0
		  self.otclasstype != null ? this.baseParams.classtype = self.otclasstype : 0
	  })
}

Ext.extend(OntologyNavPanel, Ext.tree.TreePanel, {
	  drawClassDiagram : function(node) {
		  if (this.noClassDiagram)
			  return;
		  if (node.id == '0')
			  return;
		  var drawCfg = {
			  'picType' : 'clz',
			  data : []
		  }
		  var path = node.getPath('text').replace('root', '');
		  var part = path.replace(/^\/+|\/+$/, '').split('/');
		  while (node && node.id && node.id != '0') {
			  var n = node.text
			  var attrs = []
			  Ext.each(node.childNodes, function(cn) {
				    if (this.isAttr(cn))
					    attrs.push(cn.text)
			    }, this)
			  drawCfg.data.push({
				    id : encodeURIComponent(n),
				    text : n,
				    parentId : node.parentNode.id != '0' ? encodeURIComponent(node.parentNode.text) : null,
				    attrs : attrs
			    })
			  node = node.parentNode
		  }
		  if (!this._canvas) {
			  this._canvas = new DrawDiagramWin({
				    listeners : {
					    close : function() {
						    delete this._canvas
					    }.dg(this)
				    }
			    })
		  }
		  this._canvas.show()
		  this._canvas.draw(drawCfg)
	  },
	  destroy : function() {
		  if (this._canvas) {
			  this._canvas.destroy()
			  delete this._canvas
		  }
		  OntologyNavPanel.superclass.destroy.call(this)
	  },
	  _footerHeight : 200,
	  getFooterDom : function() {
		  return Ext.getDom(this.footerId)
	  },
	  noClassDiagram : false,
	  initComponent : function() {
		  OntologyNavPanel.superclass.initComponent.call(this)
		  this.on('collapse', function() {
			    if (this._canvas)
				    this._canvas.close()
			    delete this._canvas
		    })
	  },
	  ClassRecord : Ext.data.Record.create(['classId', 'parentClassId', 'name']),
	  isAttr : function(node) {
		  return node.attributes.iconCls == 'icon-ontology-attr';
	  },
	  isAbsNode : function(node) {
		  return node.attributes.attachment.type == 3;
	  },
	  addNode : function(node) {
		  this.classFormWin.show();
		  this.classFormWin.formPanel.find('name', 'type')[0][node.id != '0' || this.otclasstype != null || !this.allowAbsSemantic() ? 'hide' : 'show']()
		  this.classFormWin.formPanel.getForm().loadRecord(new this.ClassRecord({
			    parentClassId : node.id,
			    type : node.id != '0' ? node.attributes.attachment.type : (this.otclasstype != null ? this.otclasstype : 0)
		    }));
	  },
	  hasChildClass : function(node) {
		  var hasChildClass = false;
		  for (var i = 0; i < node.childNodes.length; i++) {
			  var n = node.childNodes[i];
			  if (!this.isAttr(n)) {
				  hasChildClass = true;
				  break;
			  }
		  }
		  return hasChildClass;
	  },
	  delNode : function(node) {
		  // if (this.hasChildClass(node)) {
		  // Ext.Msg.alert('错误', '禁止删除含有子类的本体类，请先删除该类的所有子类');
		  // return;
		  // }
		  Ext.Msg.confirm('提示', '确定要删除&nbsp&nbsp' + this.getNodePath(node) + '&nbsp&nbsp吗', function(btn, text) {
			    if (btn == 'yes') {
				    Ext.Ajax.request({
					      url : 'ontology-class!del.action?classId=' + node.id + "&parentId=" + node.parentNode.id,
					      success : function(response) {
						      var result = Ext.decode(response.responseText);
						      if (result.success)
							      node.parentNode.removeChild(node);
						      else
							      Ext.Msg.alert('保存失败', result.message);
					      },
					      failure : function(response) {
						      Ext.Msg.alert("错误", response.responseText);
					      }
				      });
			    }
		    });
	  },
	  editNode : function(node) {
		  this.classFormWin.show();
		  this.classFormWin.formPanel.getForm().loadRecord(new this.ClassRecord(node.attributes.attachment));
		  this.classFormWin.formPanel.find('name', 'type')[0].hide()
	  },
	  saveNode : function(obj, successCB) {
		  if (obj.classId == "")
			  obj.classId = null;
		  Ext.Ajax.request({
			    url : 'ontology-class!save.action',
			    params : {
				    data : Ext.encode(obj)
			    },
			    success : function(form, action) {
				    var data = Ext.util.JSON.decode(form.responseText);
				    this.updateNode(data);
				    Dashboard.setAlert('保存成功');
				    successCB ? successCB() : 0
			    },
			    failure : function(response) {
				    Ext.Msg.alert("错误", response.responseText);
			    },
			    scope : this
		    });
	  },
	  updateNode : function(data) {
		  var node = this.getNodeById(data.id);
		  if (node) {
			  node.setText(data.text);
			  node.attributes = data;
		  } else {
			  var pid = data.attachment.parentClassId
			  if (!pid)
				  pid = '0';
			  var parentNode = this.getNodeById(pid);
			  node = new Ext.tree.TreeNode(data);
			  parentNode.appendChild(node);
		  }
	  },
	  editAttr : function(node) {
		  var attrPanel = this.showTab(OntologyClassPanel, (node.tabId ? node.tabId : 'attr-tab-' + node.id), node.text, 'icon-ontology-attr', true);
		  attrPanel.classId = node.parentNode.id;
		  attrPanel.classtype = node.parentNode.attributes.attachment.type;
		  attrPanel.load(node.id);
	  },
	  addAttr : function(node) {
		  var attrPanel = this.showTab(OntologyClassPanel, 'attr-tab-' + Ext.id(), '新建属性', 'icon-ontology-attr', true);
		  attrPanel.classId = node.id;
		  attrPanel.classtype = node.attributes.attachment.type;
		  attrPanel.load();
	  },
	  pasteAttr : function(node) {
		  var attrPanel = this.showTab(OntologyClassPanel, 'attr-tab-' + Ext.id(), '新建属性', 'icon-ontology-attr', true);
		  attrPanel.classId = node.id;
		  attrPanel.load(this.copyId, true);
	  },
	  delAttr : function(node) {
		  Ext.Msg.confirm('提示', '确定要删除&nbsp&nbsp' + this.getNodePath(node) + '&nbsp&nbsp吗', function(btn, text) {
			    if (btn == 'yes') {
				    Ext.Ajax.request({
					      url : 'ontology-class!delAttr.action?attrId=' + node.id,
					      success : function(form, action) {
						      this.closeTab(node.tabId ? node.tabId : 'attr-tab-' + node.id)
						      node.parentNode.removeChild(node);

					      },
					      failure : function(response) {
						      Ext.Msg.alert("错误", response.responseText);
					      },
					      scope : this
				      });
			    }
		    }, this);
	  },
	  updateAttr : function(data, classId, tabId) {
		  var node = this.getNodeById(data.id);
		  if (node) {
			  node.setText(data.text);
		  } else {
			  var parentNode = this.getNodeById(classId);
			  node = new Ext.tree.TreeNode(data);
			  if (tabId)
				  node.tabId = tabId;
			  parentNode.appendChild(node);
		  }
		  this.showTab(OntologyClassPanel, (node.tabId ? node.tabId : 'attr-tab-' + node.id), node.text, 'icon-ontology-attr', true);
	  },

	  refreshNode : function(root) {
		  if (root)
			  node = this.getRootNode();
		  else
			  node = this.getSelectionModel().getSelectedNode();
		  this.getLoader().load(node);
		  node.expand();
	  },
	  getSelectedNode : function() {
		  return this.getSelectionModel().getSelectedNode()
	  },
	  searchNode : function(focusTarget) {
		  var categoryName = this.getTopToolbar().searchNodeText.getValue();
		  if (!categoryName || !(categoryName = categoryName.trim())) {
			  Ext.Msg.alert('提示', '请输入分类名称');
			  return;
		  }
		  var self = this;
		  Ext.Ajax.request({
			    url : 'ontology-class!search.action',
			    params : {
				    categoryName : categoryName,
				    matchExactly : this.matchExactly ? true : false
			    },
			    success : function(form, action) {
				    if (!form.responseText) {
					    Dashboard.setAlert('没有查找到更多的"' + categoryName + '"，请修改后重新查找');
					    return;
				    }
				    var idPath = Ext.util.JSON.decode(form.responseText);
				    if (idPath) {
					    var targetId = idPath.pop();
					    self.expandPath('/0/' + idPath.join('/'), '', function(suc, n) {
						      self.getNodeById(targetId).select()
						      if (focusTarget)
							      focusTarget.focus()
					      });
				    }
			    },
			    failure : function(form, action) {
				    Ext.Msg.alert('提示', '查找错误');
			    }
		    });
	  },
	  expandNode : function(node, bh) {
		  var self = this;
		  if (node.attributes.bh == bh || node.leaf) {
			  this.getSelectionModel().select(node);
			  return;
		  }
		  node.expand(false, true, function() {
			    var childs = node.childNodes;
			    for (var i = 0; i < childs.length; i++) {
				    if (bh.indexOf(childs[i].attributes.bh) != -1)
					    self.expandNode(childs[i], bh);
			    }
		    });
	  },
	  getNodePath : function(node) {
		  var path = '/';
		  path = path + node.text;
		  var parent = node.parentNode;
		  while (parent && parent.text != 'root') {
			  path = parent.text + path;
			  path = '/' + path;
			  parent = parent.parentNode;
		  }
		  return path;
	  },
	  validateName : function(id, parentId, name) {
		  var node = parentId ? this.getNodeById(parentId) : '0';
		  if (!node)
			  return true;
		  if (!node.childNodes)
			  return true;
		  for (var i = 0; i < node.childNodes.length; i++) {
			  var n = node.childNodes[i]
			  if (n.text == name && n.id != id)
				  return false;
		  }
		  return true;
	  },
	  doImport : function(node, classtype) {
		  if (!this._importwin) {
			  this._importwin = new obj.datatrans.ClassImportWin();
			  this._importwin.on('importSuccess', function(cateId) {
				    var n = this.getNodeById(cateId);
				    if (n) {
					    n.reload();
				    }
			    }, this)
		  }
		  this._importwin.doImport(node ? node.id : '', null, null, {
			    classtype : classtype ? classtype : (node ? node.attributes.attachment.type : null)
		    })
	  },
	  doExport : function(node, classtype) {
		  var msgbox = Ext.Msg.show({
			    title : '导出Excel版本选择',
			    msg : '您选择要导出的excel版本',
			    buttons : {
				    yes : '07',
				    no : '03',
				    cancel : true
			    },
			    fn : function(choice) {
				    if (choice == 'cancel')
					    return
				    var self = this;
				    var includeOntologyRelateWordclass = Ext.getCmp('export_ontology_relate_wordclass').getValue();
				    // 开始下载
				    Ext.Ajax.request({
					      url : 'ontology-class-data-trans!doExport.action',
					      params : {
						      type : 'yes' == choice ? 'xlsx' : 'xls',
						      classtype : classtype ? classtype : (node ? node.attributes.attachment.type : null),
						      categoryId : node ? node.id : '',
						      includeOntologyRelateWordclass : includeOntologyRelateWordclass
					      },
					      success : function(response) {
						      var timer = new ProgressTimer({
							        initData : Ext.decode(response.responseText),
							        progressId : 'classExport',
							        progressText : 'percent',
							        boxConfig : {
								        title : '导出本体类'
							        },
							        finish : function(p, response) {
								        if (!this.downloadIFrame) {
									        this.downloadIFrame = self.getEl().createChild({
										          tag : 'iframe',
										          style : 'display:none;'
									          })
								        }
								        this.downloadIFrame.dom.src = 'ontology-class-data-trans!downFile.action?_t=' + new Date().getTime();
								        if (includeOntologyRelateWordclass) {
									        setTimeout(function() {
										          this.downloadIFrame.dom.src = 'ontology-class-data-trans!downFile.action?type=wordclass&_t=' + new Date().getTime();
									          }.dg(this), 1000)
								        }
							        },
							        scope : this
						        });
						      timer.start();
					      },
					      failure : function(response) {
					      },
					      scope : this
				      });
			    },
			    scope : this,
			    icon : Ext.Msg.QUESTION,
			    minWidth : Ext.Msg.minWidth
		    });
		  if (!Ext.get('_dlg_ontology_items')) {
			  var ctn = msgbox.getDialog().body.createChild({
				    id : '_dlg_ontology_items',
				    tag : 'div'
			    })
			  ctn.setVisibilityMode(Ext.Element.DISPLAY)
			  var includeOntologyRelateWordclass = new Ext.form.Checkbox({
				    inputValue : true,
				    defaultValue : false,
				    checked : false,
				    fieldLabel : '导出相关词类',
				    id : 'export_ontology_relate_wordclass'
			    })
			  var fp = Ext.create({
				    xtype : 'panel',
				    border : false,
				    layout : 'form',
				    labelWidth : 130,
				    bodyStyle : 'background-color:#CCD9E8;margin:0 0 0 10px;padding:0;',
				    items : includeOntologyRelateWordclass,
				    renderTo : '_dlg_ontology_items',
				    labelAlign : 'right'
			    });
			  msgbox.getDialog().on('hide', function() {
				    Ext.get('_dlg_ontology_items').hide();
			    });
		  } else {
			  Ext.get('_dlg_ontology_items').show();
		  }
		  msgbox.getDialog().show();
	  },
	  allowAbsSemantic : function() {
		  return Dashboard.u().allowPrivilege(2);
	  },
	  authName : 'kb.cls'
  });
!window.OntologyNavPanel_noregist ? Dashboard.registerNav(OntologyNavPanel, 2) : 0
