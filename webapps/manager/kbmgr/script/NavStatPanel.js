NavStatPanel = function() {
	var self = this;
	var menu = [{
				id : '1',
				name : '维度统计',
				icon : 'icon-dimension',
				panelClass : stat.dim.MainPanel
			}, {
				id : '2',
				name : '类型统计',
				icon : 'icon-ontology-cate',
				panelClass : stat.valuetype.MainPanel
			},{
				id : '3',
				name : '知识点语料统计-Labs',
				icon : 'icon-ontology-cate',
				panelClass : KBDataStatPanel
			},{
				id : '4',
				name : '知识点语料统计-领域',
				icon : 'icon-ontology-cate',
				panelClass : {doCreate:function(){
					return new KBCommonDataStatPane({statname:'Domainx'})
				}}
			},{
				id : '5',
				name : '知识点语料统计-通用',
				icon : 'icon-ontology-cate',
				panelClass : {doCreate:function(){
					return new KBCommonDataStatPane({statname:'Common'})
				}}
			}];
	var rootNode = new Ext.tree.TreeNode({
				id : '0',
				text : 'root'
			});
	var _nodeMap = {};
	// auth
	var r = menu;
	for (var i = r.length - 1; i >= 0; i--) {
		if (r[i].authName && window.kb_check_perm_result) {
			if (kb_check_perm_result.indexOf(r[i].authName) == -1) {
				r.splice(i, 1);
			}
		}
	}

	for (var i = 0; i < menu.length; i++) {
		var _item = menu[i];
		var _node = new Ext.tree.TreeNode({
					id : _item.id,
					text : _item.name,
					iconCls : _item.icon,
					module : _item.module ? _item.module : _item.id,
					panelClass : _item.panelClass,
					handler : _item.handler
				});
		_nodeMap[_item.id] = _node;
		if (!_item.parent)
			rootNode.appendChild(_node);
		else
			_nodeMap[_item.parent].appendChild(_node);
	}
	var config = {
		title : '知识点统计',
		border : false,
		autoScroll : true,
		rootVisible : false,
		lines : false,
		root : rootNode
	};
	NavStatPanel.superclass.constructor.call(this, config);

	this.modulePanels = {};
	this.on('click', function(n) {
				if (!n)
					return false;
				if (n.attributes.handler) {
					n.attributes.handler.call(n, arguments);
					return false;
				}
				var panelClass = n.attributes.panelClass;
				var p = panelClass.doCreate?panelClass.doCreate():new panelClass();
				var winfn = p.extversion == 3 || panelClass.prototype && panelClass.prototype.extversion == 3 ?Ext.Window:Ext4.Window
				var win = new winfn({
							title : n.text,
							height : 500,
							width : 600,
							closeAction : 'hide',
							closable : true,
							layout : 'fit',
							region : 'center',
							maximizable : true,
							draggable : true,
							shadow : false,
							frame : true,
							shim : false,
							animCollapse : true,
							constrainHeader : true,
							collapsible : true,
							modal : false,
							plain : true,
							shim : true,
							items : [p]
						})
				win.show();
			});
}

Ext.extend(NavStatPanel, Ext.tree.TreePanel, {
			authName : 'kb.stat'
		});

!isVersionStandard()?Dashboard.registerNav(NavStatPanel, 10):'';