NavTaskPanel = function(cfg) {
	cfg = cfg || {}
	var me = this;
	
	var rootNode = new Ext.tree.TreeNode({
		id : '0',
		text : 'root',
		leaf : false
	});
	
	var taskNode = new Ext.tree.AsyncTreeNode({
		text : "语义任务",
		id : 'g',
		iconCls : 'icon-user-login',
		cls : '1',
		loader : new Ext.tree.TreeLoader({
					dataUrl : 'abstract-semantic-task!list.action'
				})
	});
	
	var countNode = new Ext.tree.AsyncTreeNode({
		text : "语义统计",
		id : '-2',
		leaf : true,
		iconCls : 'icon-ontology-cate'
	});
	
	var queryNode = new Ext.tree.AsyncTreeNode({
		text : "语义查询",
		id : '-3',
		leaf : true,
		iconCls : 'icon-ontology-object'
	});

	rootNode.appendChild(taskNode);
	rootNode.appendChild(countNode);
	rootNode.appendChild(queryNode);
	
	var finalCfg = Ext.apply({
				title : '抽象语义',
				width : 200,
				minSize : 175,
				maxSize : 400,
				collapsible : true,
				rootVisible : false,
				enableDD : true,
				border : false,
				lines : false,
				autoScroll : true,
				containerScroll : true,
				root : rootNode,
				dataUrl : 'abstract-semantic-task!list.action'
			}, cfg);

	NavTaskPanel.superclass.constructor.call(this, finalCfg);

	this.on('click', function(n) {
		if(n.id=='-2')
			this.showTab(AbstractSemanticTaskCountPanel,
					-2, n.text, 'icon-wordclass-root', true);
		if (n.attributes.cls == '1') {
			if (this.isAttr(n)) {
				var p = this.showTab(AbstractSemanticTaskDetailPanel,
						n.id, n.text, 'icon-wordclass-root', true);
				p.loadData(n.parentNode.id);
			} else {
				var p = this.showTab(
						AbstractSemanticTaskAssignmentPanel, n.id,
						n.text, 'icon-wordclass-category', true);
				p.loadData(n.id);
			}
		}
		if(n.id=='-3')
			this.showTab(AbstractSemanticTaskDetailSearchPanel,
					-3, n.text, 'icon-wordclass-root', true);
	});

	var menu = new Ext.menu.Menu({
		items : [{
			id : 'createLeaf',
			text : '新建任务',
			iconCls : 'icon-add',
			width : 150,
			handler : function() {
				me.leafWin.show();
			}
		}, {
			id : 'updateLeaf',
			text : '修改',
			iconCls : 'icon-wordclass-edit',
			width : 150,
			handler : function() {
				me.updateUI(menu.currentNode);
			}
		}, {
			id : 'removeLeaf',
			text : '删除',
			iconCls : 'icon-wordclass-del',
			width : 150,
			handler : function() {
				me.remove(menu.currentNode);
			}
		},{
			id : 'assignTask',
			text : '分配任务',
			iconCls : 'icon-edit',
			width : 100,
			menu: {
				items : [_userItems]
			}
		}]
	});

	var _taskId = this._taskId = new Ext.form.Hidden({
				name : 'taskId'
			});

	var updateForm = this.updateForm = new Ext.form.FormPanel({
				frame : false,
				border : false,
				labelWidth : 60,
				waitMsgTarget : true,
				bodyStyle : 'padding : 3px 10px; background-color:' + sys_bgcolor,
				defaults : {
					blankText : '',
					invalidText : '',
					anchor : '100%',
					xtype : 'textfield'
				},
				items : [{
						fieldLabel : '名称',
						allowBlank : false,
						name : 'node',
						blankText : '',
						invalidText : '',
						anchor : '100%'
					}, this._taskId, new Ext.form.Hidden({
						name : 'ispub'
					}), this._taskId, new Ext.form.Hidden({
						name : 'url'
					})],
				buttons : [{
					text : '保存',
					handler : function() {
						me.updateDic(menu.currentNode);
					}
				}, {
					text : '关闭',
					handler : function() {
						me.updateWin.hide();
					}
				}]
			});

	var updateWin = this.updateWin = new Ext.Window({
				width : 300,
				border : false,
				title : '修改',
				modal : true,
				closable : true,
				closeAction : 'hide',
				resizable : false,
				items : [updateForm]
			});

	this.endTimeField = new Ext.form.DateField({
				fieldLabel : '结束时间',
				format : 'Y-m-d',
				editable : false,
				name : 'endTime',
				anchor : '100%'
			})

	this.userstore = new Ext.data.JsonStore({
				fields : [{
							name : 'username',
							type : 'string'
						}],
				autoLoad : true,
				url : '../app/g/abstract-semantic-task!getUsers.action'
			});

	var leafForm = this.leafForm = new Ext.form.FormPanel({
				fileUpload : true,
				frame : false,
				border : false,
				enctype : 'multipart/form-data',
				labelWidth : 60,
				waitMsgTarget : true,
				bodyStyle : 'padding : 3px 10px; background-color:' + sys_bgcolor,
				defaults : {
					blankText : '',
					invalidText : '',
					allowBlank : false,
					anchor : '100%',
					xtype : 'textfield'
				},
				items : [{
							fieldLabel : '任务名称',
							name : 'node',
							blankText : '',
							invalidText : '',
							anchor : '100%'
						}, {
							fieldLabel : '分配用户',
							xtype : 'combo',
							displayField : 'username',
							name : 'username',
							typeAhead : true,
							mode : 'local',
							forceSelection : true,
							triggerAction : 'all',
							selectOnFocus : true,
							editable : false,
							store : me.userstore
						}, {
							xtype : 'fileuploadfield',
							emptyText : '选择文件',
							fieldLabel : '文件路径',
							name : 'file',
							buttonText : '',
							buttonCfg : {
								iconCls : 'icon-upload-excel'
							},
							validator : function(v) {
								v = v.trim();
								if (v) {
									if (v.indexOf('.xls') < 0)
										return '文件格式不正确';
									return true;
								}
								return '请选择文件';
							}
						}, this.endTimeField],
				buttons : [{
							text : '保存',
							handler : function() {
								me.saveLeaf(this, menu.currentNode);
							}
						}, {
							text : '关闭',
							handler : function() {
								me.leafWin.hide();
							}
						}]
			});

	var leafWin = this.leafWin = new Ext.Window({
				width : 400,
				border : false,
				title : '新建任务',
				modal : true,
				closable : true,
				closeAction : 'hide',
				resizable : false,
				items : [leafForm]
			});
			
	var userData = Ext.decode(http_get("../authmgr/abstract-semantic-task!getUserList.action"));
	if (!userData)
		userData = [];
	var _userItems = [];
	for (var i = 0; i < userData.length; i++) {
		var _username = userData[i].username;
		_userItems.push({
			text: _username,
	        handler : function(_username){
				self.assignTask(teskMenu.currentNode,_username);
	        } 
		});
	}

	this.on('contextmenu', function(node, event) {
				if (node.attributes.cls == '1') {
					// 有分配权限
					if (isAllow == '1') {
						if (node.leaf) {
							Ext.getCmp('createLeaf').hide();
							Ext.getCmp('updateLeaf').show();
						} else {
							Ext.getCmp('createLeaf').show();
							Ext.getCmp('updateLeaf').hide();
						}
						if (node.id == 0) {
							Ext.getCmp('removeLeaf').hide();
						} else {
							if (node.leaf) {
								Ext.getCmp('removeLeaf').show();
							} else {
								Ext.getCmp('removeLeaf').hide();
							}
						}
						menu.currentNode = node;
						menu.showAt(event.getXY());
						event.preventDefault();
					} else {
						Ext.getCmp('createLeaf').hide();
						Ext.getCmp('updateLeaf').hide();
						Ext.getCmp('removeLeaf').hide();
					}
				}
				Ext.getCmp('assignTask').show();
			})
};

Ext.extend(NavTaskPanel, Ext.tree.TreePanel, {
			authName : 'kb.misc',
			isAttr : function(node) {
				return node.attributes.leaf == true;
			},
			refreshNode : function(root) {
				if (root)
					node = this.getRootNode();
				else
					node = this.getSelectionModel().getSelectedNode();
				this.getLoader().load(node);
				node.expand();
			},
			getSelectedNode : function() {
				return this.getSelectionModel().getSelectedNode();
			},
			isAttr : function(node) {
				return node.attributes.leaf == true;
			},
			saveLeaf : function(btn, node) {
				var leafForm = btn.ownerCt.ownerCt.getForm(), me = this;
				if (leafForm.isValid()) {
					leafForm.submit({
						waitMsg : '正在上传',
						params : {
							url : node.id
						},
						url : 'abstract-semantic-task!saveLeaf.action',
						success : function(form, act) {
								me.leafWin.hide();
								var timer = new ProgressTimer({
									initData : act.result.data,
									progressText : 'percent',
									progressId : 'importDetail',
									boxConfig : {
										msg : '导入中',
										title : '导入语料'
									},
									finish : function(p, response) {
										node.reload();
										Dashboard.setAlert("任务新建成功！");
									},
									scope : this
								});
								timer.start();
						},
						failure : function(response) {
							var data = Ext.util.JSON.decode(response.responseText);
							Dashboard.setAlert(data.message);
						}
					});
				}
			},
			remove : function(node) {
				var me = this;
				Ext.Msg.confirm('提示：', node.leaf ? '确认删除【' + node.text + '】' : '确定要删除【' + node.getPath('text').replace('/root', '') + '】及以下所有分类吗', function(btn, text) {
					if (btn == 'yes') {
						Ext.Ajax.request({
							url : 'abstract-semantic-task!remove.action',
							params : {
								node : node.id,
								url : node.parentNode.id
							},
							success : function(response) {
								var source = node.parentNode
								source.removeChild(node);
								var data = Ext.util.JSON.decode(response.responseText);
								Dashboard.setAlert(data.message);
							},
							failure : function(response, action) {
								var data = Ext.util.JSON.decode(response.responseText);
								Dashboard.setAlert(data.message);
							}
						});
					}
				})
			},
			updateUI : function(node) {
				var me = this;
				var rec = new Ext.data.Record(Ext.apply({}, node.attributes));
				rec.set('node', rec.get('text'));
				rec.set('taskId', rec.get('id'));
				rec.set('ispub', rec.get('ispub'));
				rec.set('url', node.parentNode.id);
				me.updateForm.getForm().loadRecord(rec);
				me.updateWin.show();
			},
			updateDic : function(node) {
				var me = this, updateForm = me.updateForm.getForm();
				if (updateForm.isValid()) {
					Ext.Ajax.request({
						url : 'abstract-semantic-task!updateDic.action',
						params : {
							node : updateForm.getValues().node,
							taskId : updateForm.getValues().taskId,
							ispub :  updateForm.getValues().ispub,
							url :  updateForm.getValues().url
						},
						success : function(response, action) {
							me.updateWin.hide();
							var data = Ext.util.JSON.decode(response.responseText);
							Dashboard.setAlert(data.message);
						},
						failure : function(response) {
							var data = Ext.util.JSON.decode(response.responseText);
							Dashboard.setAlert(data.message);
						}
					});
				}
			
			}
		});
