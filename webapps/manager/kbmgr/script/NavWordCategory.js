var isAudit = http_get('property!get.action?key=kbmgr.wordclass.enableAudit');
isAudit = isAudit ? ("true" == isAudit ? true : false) : false;

WordNavPanel = function(_cfg) {

	var self = this;
	this.wordclassType;
	this.data = {};
	this.ispub = null;
	this.wordPanel = '';
	this.isVersionStandard = isVersionStandard();
	this.isSuperUser = Dashboard.u().isSupervisor();

	var cfg = {
		title : '词类管理',
		width : 200,
		minSize : 175,
		maxSize : 400,
		xtype : 'treepanel',
		collapsible : true,
		rootVisible : false,
		lines : false,
		border : false,
		autoScroll : true,
		root : self.isVersionStandard ? {
			id : '2',
			text : '专有词类',
			iconCls : 'icon-ontology-root',
			lazyLoad : false
		} : {
			id : '0',
			text : 'root',
			iconCls : 'icon-ontology-root',
			lazyLoad : true,
			children : Dashboard.isLabs() ? [{
				  id : '1',
				  text : "公用词类",
				  iconCls : 'icon-comment',
				  type : '0'
			  }, {
				  id : '3',
				  text : "基础词类",
				  iconCls : 'icon-wordclass-root-sync',
				  ispub : 'cm1',
				  type : '1'
			  }, {
				  id : '4',
				  text : "抽象语义词类",
				  iconCls : 'icon-participle',
				  ispub : 'cm',
				  type : '2'
			  }, {
				  id : '2',
				  text : "专有词类",
				  type : '0'
			  }] : [{
				  id : '1',
				  text : "公用词类",
				  iconCls : 'icon-comment',
				  type : '0'
			  }, {
				  id : '4',
				  text : "抽象语义词类",
				  iconCls : 'icon-participle',
				  hidden : !self.isSuperUser,
				  type : '2'
			  }, {
				  id : '2',
				  text : "专有词类",
				  type : '0'
			  }]
		},
		listeners : {},
		dataUrl : 'wordclass-category!list.action',
		tbar : new Ext.Toolbar({
			  enableOverflow : true,
			  height : '20',
			  items : Dashboard.u().filterAllowed([{
				    blankText : "请输入词类分类名称",
				    xtype : 'textfield',
				    width : 120,
				    listeners : {
					    specialkey : function(f, e) {
						    if (e.getKey() == e.ENTER) {
							    self.searchNode(f);
						    }
					    }
				    }
			    }, {
				    text : '查找',
				    iconCls : 'icon-search',
				    width : 50,
				    handler : function() {
					    self.searchNode();
				    }
			    },
			    // '-', {
			    // text : '新建',
			    // iconCls : 'icon-wordclass-root-add',
			    // width : 50,
			    // handler : function() {
			    // self.addNode(self.getRootNode());
			    // }
			    // }, '-',
			    {
				    text : '同步所有',
				    authName : 'kb.word.S',
				    iconCls : 'icon-wordclass-root-sync',
				    width : 50,
				    handler : function() {
					    self.SyncAllAI();
				    }
			    }, '-', {
				    text : '刷新',
				    iconCls : 'icon-refresh',
				    width : 50,
				    handler : function() {
					    self.refresh(self.getRootNode());
				    }
			    }, Dashboard.isLabs() ? ['-', {
				      text : '多义词统计',
				      iconCls : 'icon-module-config',
				      width : 50,
				      handler : function() {
					      self.polysemyCount();
				      }
			      }, {
				      text : '查看多义词',
				      iconCls : 'icon-grid',
				      width : 50,
				      handler : function() {
					      self.showPolysemyResult();
				      }
			      }] : ''
			    // , '-', {
			    // text : '导入数据',
			    // iconCls : 'icon-ontology-import',
			    // width : 50,
			    // handler : function() {
			    // self.importData();
			    // }
			    // }, {
			    // text : '导出数据',
			    // iconCls : 'icon-ontology-export',
			    // width : 50,
			    // handler : function() {
			    // self.exportWordclassData();
			    // }
			    // }
			    ])

		  })
	};

	WordNavPanel.superclass.constructor.call(this, Ext.applyIf(_cfg || {}, cfg));

	var menu = new Ext.menu.Menu({
		  items : Dashboard.u().filterAllowed([{
			    text : '刷新',
			    iconCls : 'icon-refresh',
			    width : 50,
			    handler : function() {
				    self.refreshNode();
			    }
		    }, {
			    text : '新建子分类',
			    authName : 'kb.word.C',
			    iconCls : 'icon-wordclass-add',
			    width : 50,
			    handler : function() {
				    self.addNode(menu.currentNode);
			    }
		    }, {
			    ref : 'updateCategory',
			    authName : 'kb.word.M',
			    text : '修改分类',
			    iconCls : 'icon-wordclass-edit',
			    width : 50,
			    handler : function() {
				    if (menu.currentNode.id == 1 || menu.currentNode.id == 2) {
					    Ext.Msg.alert('错误提示', '根节点禁止修改');
					    return;
				    }
				    self.modifyNode(menu.currentNode);
			    }
		    }, {
			    ref : 'removeCategory',
			    authName : 'kb.word.D',
			    text : '删除分类',
			    iconCls : 'icon-wordclass-del',
			    width : 50,
			    handler : function() {
				    self.deleteNode(menu.currentNode);
			    }
		    }, '-', {
			    text : '导入数据',
			    authName : 'kb.word.IMP',
			    iconCls : 'icon-ontology-import',
			    width : 50,
			    handler : function() {
				    self.importData(menu.currentNode);
			    }
		    }, {
			    text : '导出数据',
			    authName : 'kb.word.EXP',
			    iconCls : 'icon-ontology-export',
			    width : 50,
			    handler : function() {
				    self.exportWordclassData(menu.currentNode);
			    }
		    }, {
			    text : '选择性导出数据',
			    authName : 'kb.word.EXP',
			    iconCls : 'icon-ontology-export',
			    width : 50,
			    handler : function() {
				    Ext.state.Manager.setProvider(new Ext.state.CookieProvider());
				    var filter = Ext.state.Manager.get('kb_wc_export_filter')
				    Ext.Msg.prompt('输入框', '请输入<b>不需要</b>导出的词类名', function(v, text) {
					      if (v == 'ok') {
						      self.exportWordclassData(menu.currentNode, text);
						      Ext.state.Manager.set('kb_wc_export_filter', text)
					      }
				      }, this, false, filter);
			    }
		    }, {
			    ref : 'importDynData',
			    id : 'importDynData',
			    text : '导入动态数据',
			    authName : 'kb.word.IMP',
			    iconCls : 'icon-ontology-import',
			    width : 50,
			    menu : {
				    items : [{
					      ref : '../../syncAI',
					      text : '同步到引擎',
					      checked : true,
					      hideOnClick : false
				      }]
			    }
		    }, '-', {
			    id : 'synCurrentWord',
			    text : '同步当前分类',
			    authName : 'kb.word.S',
			    iconCls : 'icon-wordclass-root-sync',
			    width : 50,
			    handler : function() {
				    self.SyncAI(menu.currentNode);
			    }
		    }, {
			    id : 'synAllWord',
			    text : '同步所有分类',
			    authName : 'kb.word.S',
			    iconCls : 'icon-wordclass-root-sync',
			    width : 50,
			    handler : function() {
				    self.SyncAllAI();
			    }
		    }])
	  });
	this.menu = menu;

	this.on('contextmenu', function(node, event) {
		  if (node.attributes.id.length !== 1) {
			  if (Ext.getCmp('synCurrentWord')) {
				  if (1 == node.attributes.type || (!Dashboard.isLabs() && 2 == node.attributes.type)) {
					  Ext.getCmp('synCurrentWord').hide();
				  } else
					  Ext.getCmp('synCurrentWord').show();
			  }
			  if (Ext.getCmp('synAllWord'))
				  Ext.getCmp('synAllWord').hide();
			  if (Ext.getCmp('importDynData'))
				  Ext.getCmp('importDynData').hide();
		  } else {
			  if (Ext.getCmp('importDynData'))
				  Ext.getCmp('importDynData').show();
			  if (Ext.getCmp('synCurrentWord'))
				  Ext.getCmp('synCurrentWord').show();
			  if (Ext.getCmp('synAllWord'))
				  Ext.getCmp('synAllWord').show();
		  }
		  if (('icon-similar' == node.attributes.iconCls && '同类词' == node.attributes.text) || ('icon-synonymy' == node.attributes.iconCls && '同义词' == node.attributes.text)) {
			  if (menu.updateCategory)
				  menu.updateCategory.hide();
			  if (menu.removeCategory)
				  menu.removeCategory.hide();
		  } else {
			  if (menu.updateCategory)
				  menu.updateCategory.show();
			  if (menu.removeCategory)
				  menu.removeCategory.show();
		  }
		  event.preventDefault();
		  menu.currentNode = node;
		  node.select();
		  menu.showAt(event.getXY());
		  if (!this._importDynDataMenuLoaded) {
			  Ext.Ajax.request({
				    url : 'property!get.action',
				    params : {
					    key : 'kb.wc.external_src_wsdl'
				    },
				    success : function(response) {
					    var v = response.responseText;
					    if (v) {
						    var configs = v.split(/\r\n|\r|\n/);
						    if (v.indexOf('http') == 0) {
							    configs = ['default,' + v];
						    }
						    Ext.each(configs, function(c) {
							      var pair = c.split(',');
							      menu.importDynData.menu.add({
								        text : pair[0],
								        handler : function() {
									        self.importDynData(menu, menu.currentNode, this.text == 'default' ? '' : this.text);
								        },
								        hideOnClick : false
							        })
						      });

					    }
				    },
				    failure : function(response) {
					    Ext.Msg.alert("错误", response.responseText);
				    }
			    })
			  this._importDynDataMenuLoaded = true;
		  }
	  });

	this.on('beforeload', function(node, event) {
		  this.getLoader().baseParams['ispub'] = node.attributes.ispub;
		  this.getLoader().baseParams['type'] = node.attributes.type;
	  });

	var _treeCombo = new TreeComboBox({
		  fieldLabel : '父级词类',
		  emptyText : '请选择父级词类...',
		  editable : false,
		  anchor : '95%',
		  allowBlank : false,
		  rootVisible : true,
		  minHeight : 250,
		  name : 'parentCategoryId',
		  root : {
			  id : '0',
			  text : 'root'
		  },
		  loader : new Ext.tree.TreeLoader({
			    dataUrl : 'wordclass-category!list.action',
			    listeners : {
				    'beforeload' : function(th, n, cb) {
					    if (n.isRoot) {
						    if (self.ispub) {
							    var type = self.formWin.formPanel.find('hiddenName', 'type')[0].getValue();
							    if (1 == type) {
								    n.setId('3');
								    n.setText('基础词类');
								    self.setNodeIconCls(n, 'icon-wordclass-root-sync');
							    } else if (2 == type) {
								    n.setId('4');
								    n.setText('抽象语义词类');
								    self.setNodeIconCls(n, 'icon-participle');
							    } else {
								    n.setId('1');
								    n.setText('公用词类');
								    self.setNodeIconCls(n, 'icon-comment');
							    }
						    } else {
							    n.setId('2');
							    n.setText('专有词类');
						    }
					    }
				    }
			    }
		    })
	  });
	this.treeCombo = _treeCombo;
	var _formPanel = new Ext.form.FormPanel({
		  frame : false,
		  border : false,
		  labelWidth : 75,
		  autoHeight : true,
		  waitMsgTarget : true,
		  bodyStyle : 'padding : 3px 10px; background-color:' + sys_bgcolor,
		  items : [new Ext.form.Hidden({
			      name : 'categoryId'
		      }), new Ext.form.Hidden({
			      name : 'type',
			      hiddenName : 'type'
		      }), new Ext.form.Hidden({
			      name : 'bh'
		      }), _treeCombo, new Ext.form.TextField({
			      fieldLabel : '类型名称',
			      allowBlank : false,
			      name : 'name',
			      blankText : '请输入类型名称',
			      anchor : '100%'
		      }), this.pubSchemaSelector = new PubSchemaSelector()],
		  tbar : [{
			    text : '保存',
			    iconCls : 'icon-wordclass-add',
			    handler : function() {
				    self.saveNode(this);
			    }
		    }]
	  });
	var _formWin = new Ext.Window({
		  width : 495,
		  title : '类型管理',
		  defaults : {
			  border : false
		  },
		  modal : true,
		  plain : true,
		  shim : true,
		  closable : true,
		  closeAction : 'hide',
		  collapsible : true,
		  resizable : false,
		  draggable : true,
		  animCollapse : true,
		  constrainHeader : true,
		  listeners : {
			  'beforehide' : function(p) {
				  p.formPanel.getForm().reset();
			  }
		  },
		  items : [_formPanel],
		  showPubSelector : function(show) {
			  if (show)
				  this.pubSchemaSelector.show()
			  else
				  this.pubSchemaSelector.hide()
		  }.dg(this)
	  });
	var _fileForm = new Ext.FormPanel({
		  layout : "fit",
		  frame : true,
		  border : false,
		  autoHeight : true,
		  waitMsgTarget : true,
		  defaults : {
			  bodyStyle : 'padding:10px'
		  },
		  margins : '0 0 0 0',
		  labelAlign : "left",
		  labelWidth : 50,
		  fileUpload : true,
		  items : [{
			    xtype : 'fieldset',
			    title : '选择文件',
			    autoHeight : true,
			    items : [{
				      id : 'wordUploadFileId',
				      name : 'uploadFile',
				      xtype : "textfield",
				      fieldLabel : '文件',
				      inputType : 'file',
				      anchor : '96%'
			      }]
		    }, {
			    xtype : 'fieldset',
			    title : '设置参数',
			    autoHeight : true,
			    items : [{
				      name : 'cleanTypeAndData',
				      xtype : 'checkbox',
				      hideLabel : true,
				      boxLabel : '是否清空子分类及相关词类名',
				      inputValue : true,
				      checked : true
			      }, {
				      name : 'cleanOtherData',
				      xtype : 'checkbox',
				      hideLabel : true,
				      hidden : true,
				      boxLabel : '是否清空其他分类中的相关词类名',
				      checked : false,
				      inputValue : true
			      }, {
				      name : 'validate',
				      xtype : 'checkbox',
				      hideLabel : true,
				      boxLabel : '是否验证词类重复性',
				      inputValue : true,
				      checked : true
			      }]
		    }]
	  });
	var _importPanel = new Ext.Panel({
		  layout : "fit",
		  layoutConfig : {
			  animate : true
		  },
		  items : [_fileForm],
		  buttons : [{
			    id : "btn_import_wordclass",
			    text : "开始导入",
			    handler : function() {
				    if (!Ext.getCmp("wordUploadFileId").getValue()) {
					    Ext.Msg.alert("错误提示", "请选择你要上传的文件");
				    } else {
					    this.disable();
					    var btn = this;
					    // 开始导入
					    var keyclassForm = self.fileForm.getForm();
					    var typePath = encodeURIComponent(self.wordclassType.id.length != 1 ? self.wordclassType.getPath("text") : "词类导入")
					    if (keyclassForm.isValid()) {
						    keyclassForm.submit({
							      params : {
								      'keyclassTypeId' : (self.wordclassType == null || self.wordclassType.id.length == 1) ? null : self.wordclassType.id,
								      'ispub' : self.ispub,
								      'allpub' : self.allpub,
								      wordclassCategoryType : self.wordclassType.attributes.type ? self.wordclassType.attributes.type : 0
							      },
							      url : 'wordclass-category!importWordclass.action?typepath=' + typePath,
							      success : function(form, action) {
								      btn.enable();
								      var result = action.result.data;
								      var timer = new ProgressTimer({
									        initData : result,
									        progressId : 'importWordclassStatus',
									        boxConfig : {
										        title : '正在导入词类...'
									        },
									        finish : function(p, response) {
										        if (p.currentCount == -2) {
											        // 下载验证失败的信息
											        if (!self.downloadIFrame) {
												        self.downloadIFrame = self.getEl().createChild({
													          tag : 'iframe',
													          style : 'display:none;'
												          })
											        }
											        self.downloadIFrame.dom.src = "wordclass-category!downExportFile.action?_t=" + new Date().getTime();
										        }
										        self.refresh(self.wordclassType);
									        }
								        });
								      timer.start();
							      },
							      failure : function(form, action) {
								      btn.enable();
								      Ext.MessageBox.hide();
								      Ext.Msg.alert('错误', '导入初始化失败.' + (action.result.message ? '详细错误:' + action.result.message : ''));
							      }
						      });
					    }
				    }
			    }
		    }, {
			    text : "关闭",
			    handler : function() {
				    self.importKeywordWin.hide();
			    }
		    }]
	  });

	var importKeywordWin = new Ext.Window({
		  width : 520,
		  title : '词类导入',
		  defaults : {// 表示该窗口中所有子元素的特性
			  border : false
			  // 表示所有子元素都不要边框
		  },

		  plain : true,// 方角 默认

		  modal : true,
		  plain : true,
		  shim : true,
		  closeAction : 'hide',
		  collapsible : true,// 折叠
		  closable : true, // 关闭
		  resizable : false,// 改变大小
		  draggable : true,// 拖动
		  minimizable : false,// 最小化
		  maximizable : false,// 最大化

		  animCollapse : true,
		  constrainHeader : true,
		  autoHeight : false,
		  items : [_importPanel]
	  });
	this.fileForm = _fileForm;
	this.importKeywordWin = importKeywordWin;
	_formWin.formPanel = _formPanel;
	this.formWin = _formWin;

	var _polysemyStore = new Ext.data.Store({
		  proxy : new Ext.data.HttpProxy({
			    url : 'wordclass!polysemyWordList.action'
		    }),
		  reader : new Ext.data.JsonReader({
			    idProperty : '',
			    root : 'data',
			    fields : [{
				      name : 'word',
				      type : 'string'
			      }, {
				      name : 'path',
				      type : 'string'
			      }, {
				      name : 'wordclassId',
				      type : 'string'
			      }, {
				      name : 'categoryId',
				      type : 'string'
			      }, {
				      name : 'ispub',
				      type : 'string'
			      }, {
				      name : 'type',
				      type : 'string'
			      }, {
				      name : 'orderName',
				      type : 'string'
			      }, {
				      name : 'categoryPath',
				      type : 'string'
			      }]
		    }),
		  writer : new Ext.data.JsonWriter()
	  });
	_polysemyStore.baseParams['limit'] = 20;
	this.polysemyStore = _polysemyStore;

	var pagingBar = new Ext.PagingToolbar({
		  store : _polysemyStore,
		  pageSize : 20,
		  displayInfo : true,
		  prependButtons : true,
		  beforePageText : '第',
		  afterPageText : '页，共{0}页',
		  displayMsg : '第{0}到{1}条记录，共{2}条',
		  emptyMsg : "没有记录"
	  });

	var defAuditRenderer = function(v, meta, r) {
		if (1 == r.get('state')) {
			meta.attr = 'style=font-weight:bold;color:green';
		} else if (2 == r.get('state')) {
			meta.attr = 'style=font-weight:bold;color:orange';
		} else if (3 == r.get('state')) {
			meta.attr = 'style=font-weight:bold;color:red';
		}
		return v;
	}

	var _polysemyWordGrid = new Ext.grid.GridPanel({
		  region : 'center',
		  style : 'border-right: 1px solid #8db2e3;',
		  stripeRows : true,
		  columnLines : true,
		  border : false,
		  layout : 'fit',
		  store : _polysemyStore,
		  loadMask : true,
		  colModel : new Ext.grid.ColumnModel({
			    defaults : {
				    sortable : true
			    },
			    columns : [new Ext.grid.RowNumberer(), {
				      header : 'wordclassId',
				      dataIndex : 'wordclassId',
				      hidden : true
			      }, {
				      header : 'ispub',
				      dataIndex : 'ispub',
				      hidden : true
			      }, {
				      header : 'categoryId',
				      dataIndex : 'categoryId',
				      hidden : true
			      }, {
				      header : 'type',
				      dataIndex : 'type',
				      hidden : true
			      }, {
				      header : 'orderName',
				      dataIndex : 'orderName',
				      hidden : true
			      }, {
				      header : 'categoryPath',
				      dataIndex : 'categoryPath',
				      hidden : true
			      }, {
				      header : '词',
				      dataIndex : 'word',
				      width : 20,
				      renderer : function(v, meta, r) {
					      defAuditRenderer(v, meta, r);
					      return v;
				      }
			      }, {
				      header : '路径',
				      dataIndex : 'path'
			      }]
		    }),
		  sm : new Ext.grid.RowSelectionModel(),
		  bbar : pagingBar,
		  viewConfig : {
			  forceFit : true
		  }
	  });
	this.polysemyWordGrid = _polysemyWordGrid;

	_polysemyWordGrid.getSelectionModel().on('rowselect', function(sm, rowIdx, rec) {
		  var self = this;
		  self.polysemyformPanel.limitExceed = false;
		  var f = this.polysemyformPanel;
		  if (!f.isVisible()) {
			  f.toggleCollapse(true);
		  }
		  if (!this.mask) {
			  f.getEl().unmask();
		  }
		  f.getForm().reset();
		  var rows = self.polysemyWordGrid.getSelectionModel().getSelections();
		  var rec = rows[0];
		  var ispub = rec.get('ispub');
		  var node = {
			  'id' : rec.json.categoryId,
			  'path' : rec.json.categoryPath,
			  'type' : rec.json.type,
			  'ispub' : ispub
		  };
		  self._setPolysemyPubDataUrl(node, self);
		  self.polysemyTreeCombo.setValue(node);
		  self.polysemyformPanel.find('name', 'name')[0].el.applyStyles(ispub ? "font-weight:bold;color:#15428B" : "font-weight:normal;color:black")
		  self.polysemyTreeCombo.enable();
		  self.polysemyformPanel.getTopToolbar().save.enable();
		  self.polysemyformPanel.wordclassname.el.dom.readOnly = false;
		  var path = rec.data.path.split(/\//);
		  rec.data.name = path[path.length - 2];
		  rec.data.id = rec.data.wordclassId;
		  f.getForm().loadRecord(rec);
		  self.polysemyformPanel.getTopToolbar().save.enable();
		  var _synoArr = [];
		  var _pinyinArr = [];
		  var _params = {
			  wordclassId : rec.get('wordclassId')
		  };
		  if (rec.get('ispub'))
			  _params.ispub = ispub
		  if (isAudit) {
			  if (rec.get('opType') > 0) {
				  f.wordclassname.el.dom.readOnly = (rec.get('opType') & 2) == 2;
				  self.polysemyTreeCombo[(rec.get('opType') & 4) == 4 ? 'disable' : 'enable']();
			  }
			  if (rec.get('state') > 2) {
				  f.wordclassname.el.dom.readOnly = true;
				  self.polysemyTreeCombo.disable();
				  f.getTopToolbar().save.disable();
			  }
			  self.loadWordclassWord(rec.get('ispub') ? rec.get('ispub') : '', rec.get('wordclassId'), isAudit);
		  } else {
			  Ext.Ajax.request({
				    url : 'wordclass!findWordclassWords.action',
				    params : _params,
				    success : function(resp) {
					    var total = Ext.decode(resp.responseText).total;
					    var _words = Ext.decode(resp.responseText).data.result;
					    if (total < 5000) {
						    self.polysemyformPanel.bToolbar.countSynoword.setText('同义词共 ' + total + ' 条,当前显示 ' + total + '条');
						    self.polysemyformPanel.getTopToolbar().save.enable();
						    for (var i = 0; i < _words.length; i++) {
							    _synoArr.push(_words[i].name);
							    _pinyinArr.push(_words.pinyin);
						    }
						    self.polysemyformPanel.synoArea.setValue(_synoArr.join('\n'));
					    } else {
						    self.polysemyformPanel.bToolbar.countSynoword.setText("同义词共 " + total + " 条，当前显示 0 条");
						    self.polysemyformPanel.getTopToolbar().save.disable();
						    self.polysemyformPanel.limitExceed = true;
					    }
				    }
			    });
		  }
	  }, this);
	var _bToolbar = new Ext.Toolbar({
		  height : '20px',
		  items : {
			  xtype : 'label',
			  ref : 'countSynoword',
			  text : ''
		  }
	  });

	var _syncCheckbox = new Ext.form.Checkbox({
		  boxLabel : '同步',
		  checked : isAudit ? false : true,
		  name : 'sync',
		  flex : 5,
		  anchor : '100% 100% '
	  });

	var wordclassname = new Ext.form.TextField({
		  fieldLabel : '词类名称',
		  allowBlank : false,
		  name : 'name',
		  ref : 'wordclassname',
		  blankText : '',
		  invalidText : '',
		  anchor : '100%'
	  })
	var orderName = new Ext.form.TextField({
		  fieldLabel : '组别',
		  allowBlank : true,
		  name : 'orderName',
		  hidden : !Dashboard.isLabs(),
		  blankText : '',
		  invalidText : '',
		  anchor : '100%'
	  })
	var _polysemyTreeCombo = new TreeComboBox({
		  fieldLabel : '父级目录',
		  emptyText : '请选择父级目录...',
		  editable : false,
		  anchor : '100%',
		  name : 'categoryId',
		  allowBlank : false,
		  minHeight : 250,
		  root : {
			  id : '0',
			  text : 'root',
			  iconCls : 'icon-ontology-root'
		  },
		  loader : new Ext.tree.TreeLoader({
			    dataUrl : 'wordclass-category!list.action',
			    listeners : {
				    'beforeload' : function(th, n, cb) {
				    }
			    }
		    })
	  });
	this.polysemyTreeCombo = _polysemyTreeCombo;

	var _synoArea = new Ext.form.TextArea({
		  fieldLabel : '同义词',
		  name : "synos",
		  anchor : '100% 100% '
	  });

	this.wordProxy = new LocalProxy({});
	var wordEditStore = new Ext.data.Store({
		  proxy : self.wordProxy,
		  reader : new Ext.data.JsonReader({
			    idProperty : 'wordId',
			    root : 'data',
			    fields : [{
				      name : 'wordId',
				      type : 'string'
			      }, {
				      name : 'name',
				      type : 'string'
			      }, {
				      name : 'mark',
				      type : 'string'
			      }, {
				      name : 'state',
				      type : 'int'
			      }]
		    }),
		  writer : new Ext.data.JsonWriter(),
		  listeners : {
			  'load' : function(store, records) {
				  self.polysemyformPanel.bToolbar.countSynoword.setText('同义词共 ' + records.length + ' 条');
				  self.wordEditGrid.batchAdd(18);
			  }
		  }
	  });
	var wordEditGrid = this.wordEditGrid = new ExcelLikeGrid({
		  autoExpandColumn : 'name',
		  // anchor : '100% -60',
		  border : true,
		  clicksToEdit : 1,
		  frame : false,
		  columnLines : true,
		  selModel : new Ext.grid.RowSelectionModel(),
		  pasteable : true,
		  columns : [new Ext.grid.RowNumberer(), {
			    id : "name",
			    header : '同义词',
			    dataIndex : 'name',
			    width : 120,
			    editor : new Ext.form.TextField(),
			    replaceable : true,
			    renderer : function(v, meta, r, rIdx) {
				    defAuditRenderer(v, meta, r);
				    return v;
			    }
		    }, {
			    header : '备注',
			    dataIndex : 'mark',
			    width : 120,
			    editor : new Ext.form.TextField(),
			    replaceable : true
		    }, new DeleteButtoner()],
		  store : wordEditStore,
		  listeners : {
			  'cellclick' : function(grid, rowIndex, columnIndex, e) {
				  var record = grid.getStore().getAt(rowIndex);
				  if (record.get('state') > 0)
					  return false;
			  }
		  }
	  });

	wordEditGrid.on("scrollbottomclick", function(e) {
		  wordEditGrid.batchAdd(20);
	  });

	wordEditGrid.on("shiftleft", function(e) {
		  var g = wordEditGrid;
		  var col = g.findMostRightEditCol();
		  var r = g.getSelectionModel().getSelected();
		  var row = g.getStore().indexOf(r);
		  this.stopEditing();
		  g.startEditing(row, col);
	  });

	var _polysemyformPanel = new Ext.form.FormPanel({
		  frame : true,
		  border : false,
		  region : 'east',
		  height : 800,
		  width : 320,
		  split : true,
		  collapseMode : 'mini',
		  labelWidth : 75,
		  anchor : '100% 100%',
		  waitMsgTarget : true,
		  bodyStyle : 'padding : 5px; background-color:#d3e1f1;',
		  style : 'border-left: 1px solid #8db2e3;',
		  items : [new Ext.form.Hidden({
			      name : 'id'
		      }), new Ext.form.Hidden({
			      name : 'state'
		      }), new Ext.form.Hidden({
			      name : 'opType'
		      }), new Ext.form.Hidden({
			      name : 'type'
		      }), new Ext.form.Hidden({
			      name : 'ispub'
		      }), _polysemyTreeCombo, wordclassname, new Ext.form.ComboBox({
			      triggerAction : 'all',
			      fieldLabel : '词类型别',
			      anchor : '100%',
			      allowBlank : false,
			      mode : 'local',
			      hiddenName : 'type',
			      hidden : true,
			      name : 'type',
			      editable : false,
			      value : 1,
			      store : new Ext.data.ArrayStore({
				        fields : ['vfield', 'displayText'],
				        data : [[1, '词类型别1'], [2, '词类型别2']]
			        }),
			      valueField : 'vfield',
			      displayField : 'displayText'
		      }), orderName, isAudit ? {
			    layout : 'fit',
			    anchor : '100% -60',
			    border : false,
			    items : wordEditGrid
		    } : {
			    layout : 'hbox',
			    layoutConfig : {
				    align : 'stretch'
			    },
			    anchor : '100% -80',
			    border : false,
			    items : [{
				      layout : 'form',
				      labelAlign : 'top',
				      flex : 5,
				      border : false,
				      bodyStyle : 'background-color:#d3e1f1;padding-right:2px;',
				      items : _synoArea
			      }]
		    }],
		  tbar : [_syncCheckbox, {
			    text : '保存',
			    iconCls : 'icon-table-save',
			    ref : 'save',
			    xtype : 'splitbutton',
			    menu : {
				    items : [{
					      text : '忽略重复词',
					      checked : false,
					      name : 'ignoreRepeat',
					      ref : '../ignoreRepeat'
				      }]
			    },
			    handler : function() {
				    self.saveWord(this);
			    }
		    }],
		  bbar : _bToolbar
	  });
	this.polysemyformPanel = _polysemyformPanel;
	this.polysemyformPanel.bToolbar = _bToolbar;
	this.polysemyformPanel.synoArea = _synoArea;
	this.polysemyformPanel.syncCheckbox = _syncCheckbox;

	var polysemyPanel = new Ext.Panel({
		  layout : 'border',
		  border : false,
		  frame : true,
		  width : 1000,
		  height : 580,
		  items : [_polysemyWordGrid, _polysemyformPanel]
	  });

	var _polysemyWordWin = new Ext.Window({
		  width : 1000,
		  height : 580,
		  title : '多义性结果',
		  modal : false,
		  plain : true,
		  shim : true,
		  closable : true,
		  closeAction : 'hide',
		  collapsible : true,
		  animCollapse : true,
		  constrainHeader : true,
		  minimizable : true,// 最小化
		  maximizable : true,// 最大化
		  resizable : true,
		  draggable : true,
		  listeners : {
			  'beforehide' : function(p) {
				  self.polysemyformPanel.getForm().reset();
				  if (isAudit)
					  self.wordEditGrid.getStore().removeAll();
			  }
		  },
		  layout : 'fit',
		  items : [polysemyPanel]
	  });
	this.polysemyWordWin = _polysemyWordWin;

	this.on('click', function(n) {
		  this.wordclassType = n;
		  this.ispub = null;
		  if (!n)
			  return false;
		  this.ispub = n.attributes.ispub;
		  this.wordPanel = self.showTab(WordclassPanel, 'wordclassTab', '词类管理', 'icon-wordclass-category', true);
		  this.wordPanel.loadWords(n.id, n.attributes.ispub, n.attributes.type);
	  })
}

Ext.extend(WordNavPanel, Ext.tree.TreePanel, {
	  endEdit : function() {
		  this.formWin.hide();
		  this.formWin.formPanel.getForm().reset();
	  },
	  isRoot : function(node) {
		  return node.id.length == 1;
	  },
	  saveNode : function(btn) {
		  btn.disable();
		  var f = this.formWin.formPanel.getForm();
		  if (!f.isValid()) {
			  Ext.Msg.alert('提示', '您尚未正确完整的输入表单内容，请检查！');
			  btn.enable();
			  return false;
		  }
		  this.data = f.getValues();
		  this.data.parentCategoryId = this.treeCombo.getValue();
		  var ispub = this.data.ispub;
		  delete this.data.ispub;
		  this.data.capabilities = this.data.important | this.data.similar;
		  delete this.data.important
		  delete this.data.similar
		  if (this.data.parentCategoryId.length == 1) {
			  this.data.parentCategoryId = null;
		  }
		  if (!this.data.categoryId)
			  this.data.categoryId = null;
		  var self = this;
		  Ext.Ajax.request({
			    url : 'wordclass-category!save.action',
			    params : {
				    'ispub' : ispub,
				    data : Ext.encode(ux.util.resetEmptyString(self.data))
			    },
			    success : function(form, action) {
				    Dashboard.setAlert("保存成功!");
				    self.endEdit();
				    self.updateNode(Ext.decode(form.responseText), ispub);
				    btn.enable();
			    },
			    failure : function(response) {
				    Ext.Msg.alert("错误", response.responseText);
				    btn.enable();
			    }
		    })
	  },
	  _setPolysemyPubDataUrl : function(node, self) {
		  var thisdataurl = (node.id || '') + '_' + (node.ispub || '')
		  console.log(thisdataurl, '||', self.lastDataPolysemyUrl)
		  if (!self.treeComboPolysemyExpandL) {
			  self.polysemyTreeCombo.on('expand', self.treeComboPolysemyExpandL = function() {
				    if (thisdataurl != self.lastDataPolysemyUrl) {
					    self.polysemyTreeCombo.reload();
				    }
			    }, self)
		  }
		  self.lastDataPolysemyUrl = thisdataurl;
		  if (node.ispub) {
			  if (self.polysemyTreeCombo.loader.dataUrl.indexOf('ispub') == -1)
				  self.polysemyTreeCombo.loader.dataUrl += '?ispub=' + node.ispub + "&type=" + node.type;
			  else
				  self.polysemyTreeCombo.loader.dataUrl = self.polysemyTreeCombo.loader.dataUrl.replace(/\&type=.*/ig, '') + "&type=" + node.type;
		  } else {
			  self.polysemyTreeCombo.loader.dataUrl = self.polysemyTreeCombo.loader.dataUrl.replace(/\?ispub=.*/ig, '');
		  }

		  // // //
		  // if (!this.treeComboPolysemyExpandL) {
		  // this.polysemyTreeCombo.on('expand', this.treeComboPolysemyExpandL =
		  // function() {
		  // if (this.polysemyTreeCombo.loader.dataUrl != this.lastDataPolysemyUrl) {
		  // this.polysemyTreeCombo.reload();
		  // }
		  // }, this)
		  // }
		  // this.lastDataPolysemyUrl = this.polysemyTreeCombo.loader.dataUrl
		  // if (this.ispub) {
		  // if (this.polysemyTreeCombo.loader.dataUrl.indexOf('ispub') == -1)
		  // this.polysemyTreeCombo.loader.dataUrl += '?ispub=' + this.ispub +
		  // "&type=" + node.attributes.type;
		  // else
		  // this.polysemyTreeCombo.loader.dataUrl =
		  // this.polysemyTreeCombo.loader.dataUrl.replace(/\&type=.*/ig, '') +
		  // "&type=" + node.attributes.type;
		  // } else {
		  // this.polysemyTreeCombo.loader.dataUrl =
		  // this.polysemyTreeCombo.loader.dataUrl.replace(/\?ispub=.*/ig, '');
		  // }
	  },
	  _setPubDataUrl : function(node) {
		  if (!this.treeComboExpandL) {
			  this.treeCombo.on('expand', this.treeComboExpandL = function() {
				    if (this.treeCombo.loader.dataUrl != this.lastDataUrl) {
					    this.treeCombo.reload();
				    }
			    }, this)
		  }
		  this.lastDataUrl = this.treeCombo.loader.dataUrl
		  if (this.ispub) {
			  if (this.treeCombo.loader.dataUrl.indexOf('ispub') == -1)
				  this.treeCombo.loader.dataUrl += '?ispub=' + this.ispub + "&type=" + node.attributes.type;
			  else
				  this.treeCombo.loader.dataUrl = this.treeCombo.loader.dataUrl.replace(/\&type=.*/ig, '') + "&type=" + node.attributes.type;
		  } else {
			  this.treeCombo.loader.dataUrl = this.treeCombo.loader.dataUrl.replace(/\?ispub=.*/ig, '');
		  }
	  },
	  addNode : function(node) {
		  this.formWin.showPubSelector(Dashboard.u().allow('ALL') || Dashboard.isLabs())
		  this.ispub = node.attributes.ispub;
		  var rec = new Ext.data.Record(Ext.apply({}, node.attributes));
		  rec.set('ispub', this.ispub ? this.ispub : '');
		  var fp = this.formWin.formPanel;
		  fp.find('hiddenName', 'type')[0].setValue('');
		  fp.getForm().loadRecord(rec);
		  if (node.id == '1') {// set default pub schema
			  this.pubSchemaSelector.setFirstSchema()
			  fp.find('hiddenName', 'ispub')[0].setReadOnly(false)
		  } else if (node.id == '3') {
			  fp.find('hiddenName', 'ispub')[0].setValue('cm');
			  fp.find('hiddenName', 'ispub')[0].setReadOnly(true);
			  fp.find('hiddenName', 'type')[0].setValue(1);
		  } else if (node.id == '4') {
		  	  if(Dashboard.isLabs())
			   fp.find('hiddenName', 'ispub')[0].setValue('cm');
			  else
			   this.pubSchemaSelector.setFirstSchema();
			  fp.find('hiddenName', 'ispub')[0].setReadOnly(true);
			  fp.find('hiddenName', 'type')[0].setValue(2);
		  } else
			  fp.find('hiddenName', 'ispub')[0].setReadOnly(true)
		  this.formWin.show();
		  if (this.isRoot(node))
			  this.treeCombo.setReadOnly(true);
		  else
			  this.treeCombo.setReadOnly(false);
		  this.treeCombo.setValueEx(node);
		  var _chkGroup = this.formWin.formPanel.capChkGroup;
		  var _chkVals = {};
		  if (!this.isRoot(node)) {
			  var _capabilities = node.attributes.attachment;
			  _chkVals = Dashboard.utils.newWordCap(_capabilities);
		  }
		  this._setPubDataUrl(node);
		  // _chkGroup.setValue(_chkVals);
	  },
	  modifyNode : function(node) {
		  this.formWin.showPubSelector(Dashboard.u().allow('ALL') || Dashboard.isLabs())
		  this.ispub = node.attributes.ispub;
		  this.formWin.show();
		  var fp = this.formWin.formPanel;
		  var rec = new Ext.data.Record(Ext.apply({}, node.attributes));
		  rec.set('name', rec.get('text'));
		  rec.set('categoryId', rec.get('id'));
		  rec.set('ispub', this.ispub ? this.ispub : '');
		  fp.getForm().loadRecord(rec);
		  // if (this.isRoot(node.parentNode))
		  // this.treeCombo.setReadOnly(true);
		  // else
		  // this.treeCombo.setReadOnly(false);
		  this.treeCombo.setValueEx(node.parentNode);
		  this._setPubDataUrl(node);
		  fp.find('hiddenName', 'ispub')[0].setReadOnly(true)
	  },
	  deleteNode : function(node) {
		  var wordPanel = this.wordPanel;
		  var self = this;
		  if (node.id.length == 1) {
			  Ext.Msg.alert('警告', '根节点不能删除！');
			  return;
		  }
		  var isDelValid = Dashboard.getModuleProp('kbmgr.wordclass_category_delete_validation');
		  if("true" == isDelValid){
		  	  var ispub = node.attributes.ispub ? node.attributes.ispub : '';
		  	  var type = node.attributes.type ? node.attributes.type : 0;
              var isChildObj = http_get('wordclass-category!isChildWordclass.action?id='+node.id+'&ispub='+ispub+'&type='+type);
              if("true" == isChildObj){
                  Ext.Msg.alert('提示','目录下有词类存在，禁止删除！');
                  return;
              }
		  }
		  Ext.Msg.confirm('提示', '确定要删除&nbsp&nbsp' + node.getPath('text').replace('/root', '') + '&nbsp&nbsp及以下所有分类吗', function(btn, text) {
			    if (btn == 'yes') {
				    Ext.Ajax.request({
					      url : 'wordclass-category!del.action',
					      params : {
						      'ispub' : node.attributes.ispub,
						      'categoryType' : node.attributes.type,
						      // 'bh' : node.attributes.bh,
						      'categoryid' : node.id,
						      'isAudit' : isAudit
					      },
					      success : function(response) {
						      if (isAudit) {
							      self.setNodeIconCls(node, 'icon-ontology-cate-del');
						      } else {
							      var node4Sel = node.nextSibling ? node.nextSibling : node.previousSibling;
							      if (!node4Sel) {
								      node4Sel = node.parentNode;
								      node4Sel.leaf = true;
							      }
							      node4Sel.select();
							      if (typeof(wordPanel) != 'undefined' && wordPanel != '') {
								      wordPanel.loadWords(node4Sel.id, node.attributes.ispub,node.attributes.type)
							      }
							      node.parentNode.removeChild(node);
						      }
					      },
					      failure : function(response) {
						      Ext.Msg.alert("错误", response.responseText);
					      }
				      });
			    }
		    });
	  },
	  refresh : function(node) {
		  this.ispub = null;
		  this.ispub = node.attributes.ispub;
		  if (node.reload && node.isExpanded())
			  node.reload();
		  else
			  node.expand();
	  },
	  getSelectedNode : function() {
		  return this.getSelectionModel().getSelectedNode();
	  },
	  updateNode : function(data, ispub) {
		  var parentCateId = this.data.parentCategoryId;
		  var parentNode;
		  if (parentCateId == null) {
			  if (ispub) {
				  if ('cm' == ispub) {
					  if (1 == data.type) {
						  parentNode = this.getNodeById(3);
					  } else if (2 == data.type) {
						  parentNode = this.getNodeById(4);
					  } else {
						  parentNode = this.getNodeById(1);
					  }
				  } else
				     if(2 == data.type)
						  parentNode = this.getNodeById(4)
					 else
					      parentNode = this.getNodeById(1);
			  } else {
			  	if(2 == data.type)
					parentNode = this.getNodeById(4)
				else
				    parentNode = this.getNodeById(2);
			  }
		  } else {
			  parentNode = this.getNodeById(parentCateId);
		  }
		  if (!data.saveOrUpdate) {
			  var selNode = this.getSelectedNode();
			  selNode.setText(data.text);
			  var color = 'black';
			  if (data.cls.indexOf('import') > 0)
				  color = 'red';
			  else if (data.cls.indexOf('similar') > 0)
				  color = 'blue';
			  else if (data.cls.indexOf('both') > 0)
				  color = 'purple'
			  var capabilities = data.attachment;
			  this.setNodeClsAndCapa(selNode, color, capabilities);
			  var parentBefore = selNode.parentNode;
			  if (parentNode && selNode.parentNode.id != parentNode.id) {
				  selNode.leaf = data.leaf;
				  // this.setNodeIconCls(selNode, data.iconCls);
				  selNode.attributes.bh = data.bh;
				  parentNode.leaf = false;
				  parentNode.appendChild(selNode);
			  }
			  // if (parentBefore) {
			  // parentBefore.removeChild(selNode);
			  // if (parentBefore.childNodes.length == 0)
			  // parentBefore.leaf = true;
			  // }
		  } else {
			  if (parentNode) {
				  var node = new Ext.tree.TreeNode({
					    id : data.id,
					    iconCls : data.iconCls,
					    cls : data.cls,
					    leaf : true,
					    text : data.text,
					    bh : data.bh,
					    ispub : data.ispub,
					    type : data.type,
					    attachment : data.attachment
				    });
				  if (parentNode.leaf)
					  parentNode.leaf = false
				  parentNode.appendChild(node);
				  parentNode.expand();
				  node.select();
			  }
		  }
	  },
	  setNodeClsAndCapa : function(node, color, capabilities) {
		  node.attributes.attachment = capabilities;
		  var tel = node.getUI().getTextEl();
		  tel.style.color = color;
		  if (!node.leaf) {
			  var children = node.childNodes;
			  for (var i = 0; i < children.length; i++)
				  this.setNodeClsAndCapa(children[i], color, capabilities);
		  }
	  },
	  setNodeIconCls : function(node, clsName) {
		  var iel = node.getUI().getIconEl();
		  if (iel) {
			  var el = Ext.get(iel);
			  if (el) {
				  if (clsName.indexOf('-root') > 0)
					  el.removeClass('icon-wordclass-category');
				  else
					  el.removeClass('icon-wordclass-root');
				  el.addClass(clsName);
			  }
		  }
	  },

	  searchNode : function(focusTarget) {
		  var categoryName = categoryName = this.getTopToolbar().getComponent(0).getValue();
		  if (!categoryName || !(categoryName = categoryName.trim())) {
			  Ext.Msg.alert('提示', '请输入分类名称');
			  return;
		  }
		  var self = this;
		  Ext.Ajax.request({
			    url : 'wordclass-category-search!search.action',
			    params : {
				    'categoryName' : categoryName
			    },
			    success : function(form, action) {
				    if (!form.responseText) {
					    Dashboard.setAlert('没有查找到更多的"' + categoryName + '"，请修改后重新查找');
					    return;
				    }
				    var obj = Ext.util.JSON.decode(form.responseText);
				    var idPath = obj.pathId;
				    var ispub = obj.ispub;
				    var type = obj.type;
				    if (idPath) {
					    var targetId = idPath.pop();
					    var rootPath = '';
					    if (self.isVersionStandard)
						    rootPath = '/2/';
					    else {
						    var dirPath = ispub ? "1/" : "2/";
						    if (!Dashboard.isLabs() && 2 == type)
							    dirPath = "4/";
						    else if (Dashboard.isLabs() && "cm" == ispub) {
							    if (type && 1 == type) {
								    dirPath = "3/";
							    } else if (type && 2 == type) {
								    dirPath = "4/";
							    }
						    }
						    rootPath = '/0/' + dirPath;
					    }
					    self.expandPath(rootPath + idPath.join('/'), '', function(suc, n) {
						      self.getNodeById(targetId).select();
						      if (focusTarget)
							      focusTarget.focus()
					      });
				    }
			    },
			    failure : function(form, action) {
				    Ext.Msg.alert('提示', '查找错误');
			    }
		    });
	  },

	  expandNode : function(node, bh, ispub) {
		  var self = this;
		  if (node.attributes.bh == bh || node.leaf) {
			  this.getSelectionModel().select(node);
			  return;
		  }
		  node.expand(false, true, function() {
			    var childs = node.childNodes;
			    for (var i = 0; i < childs.length; i++) {
				    if (bh.indexOf(childs[i].attributes.bh) != -1)
					    self.expandNode(childs[i], bh);
			    }
		    });
	  },
	  refreshNode : function() {
		  var node = this.getSelectedNode();
		  this.ispub = null;
		  this.ispub = node.attributes.ispub;
		  if (!node)
			  this.refresh(this.getRootNode());
		  else {
			  this.refresh(node);
		  }
	  },
	  importDynData : function(menu, node, dynDataName) {
		  Ext.Ajax.request({
			    url : 'wordclass!importDynData.action',
			    params : {
				    syncAI : menu.syncAI.checked,
				    ispub : node.attributes.ispub,
				    dynDataName : dynDataName
			    },
			    success : function(resp) {
				    var result = Ext.decode(resp.responseText);
				    if (result.success) {
					    var timer = new ProgressTimer({
						      initData : result,
						      progressId : 'wc_external_src_import',
						      boxConfig : {
							      title : '导入动态词类'
						      }
					      });
					    timer.start();
				    } else {
					    Ext.Msg.alert('错误', result.message)
				    }
			    },
			    failure : function(resp) {
				    Ext.Msg.alert('错误', resp.responseText);
			    }
		    });
	  },
	  importData : function(node) {
		  this.wordclassType = node;
		  this.ispub = node.attributes.ispub;
		  this.allpub = (node.id == '1' || node.id == '4');
		  this.importKeywordWin.show();
		  // 恢复导入按钮
		  Ext.getCmp("btn_import_wordclass").setDisabled(false);

	  },
	  exportWordclassData : function(typeNode, filter) {
		  this.ispub = (!Dashboard.isLabs() && 4 == typeNode.attributes.id) ? "PUB" : typeNode.attributes.ispub;
		  this.allpub = typeNode.id == '1';
		  var self = this;
		  self.exportWordclassDataInternal(typeNode, filter);
	  },
	  exportWordclassDataInternal : function(typeNode, filter) {
		  var self = this;
		  Ext.Msg.show({
			    title : '导出Excel版本选择',
			    msg : '您选择要导出的excel版本',
			    buttons : {
				    yes : '07',
				    no : '03',
				    cancel : true
			    },
			    fn : function(choice) {
				    if (choice == 'cancel')
					    return
				    var self = this;
				    // 开始下载
				    Ext.Ajax.request({
					      url : 'wordclass-category!exportWordclass.action',
					      params : {
						      'wordclassCategoryId' : typeNode.id.length == 1 ? null : typeNode.id,
						      'wordclassCategoryType' : typeNode.attributes.type ? typeNode.attributes.type : 0,
						      'typePath' : (typeof(typeNode) == 'undefined' ? '' : typeNode.getPath("text")),
						      'fileType' : 'yes' == choice ? 'xlsx' : 'xls',
						      'ispub' : this.ispub,
						      'allpub' : this.allpub,
						      filter : filter ? filter : ''
					      },
					      success : function(response) {
						      var fileName = Ext.util.JSON.decode(response.responseText).message;
						      var params = Ext.urlEncode({
							        'filename' : fileName
						        })
						      var result = Ext.util.JSON.decode(response.responseText);
						      if (result.success) {
							      var timer = new ProgressTimer({
								        initData : result.data,
								        progressId : 'exportWordclassStatus',
								        boxConfig : {
									        title : '正在导出词类...'
								        },
								        finish : function() {
									        if (!self.downloadIFrame) {
										        self.downloadIFrame = self.getEl().createChild({
											          tag : 'iframe',
											          style : 'display:none;'
										          })
									        }
									        self.downloadIFrame.dom.src = 'wordclass-category!downExportFile.action?' + params + '&_t=' + new Date().getTime();
								        }
							        });
							      timer.start();
						      } else {
							      Ext.Msg.alert('错误', '导出失败:' + result.message)
						      }
					      },
					      failure : function(response) {
						      Ext.Msg.alert('错误', '导出失败')
					      },
					      scope : this
				      });
			    },
			    scope : this,
			    icon : Ext.Msg.QUESTION,
			    minWidth : Ext.Msg.minWidth
		    });
	  },
	  SyncAllAI : function() {
		  Ext.Ajax.request({
			    url : 'wordclass!syncAllAI.action',
			    params : {
				    all : true
			    },
			    success : function(resp) {
				    var timer = new ProgressTimer({
					      initData : Ext.decode(resp.responseText),
					      progressId : 'syncWord',
					      boxConfig : {
						      title : '同步所有词类'
					      }
				      });
				    timer.start();
			    },
			    failure : function(resp) {
				    Ext.Msg.alert('错误', resp.responseText);
			    }
		    });
	  },
	  SyncAI : function(node) {
		  if (node.id.length == 1) {
			  Ext.Ajax.request({
				    url : 'wordclass!syncAllAI.action',
				    params : {
					    isallpub : node.attributes.id == '1',
					    isAbsWordclass : !Dashboard.isLabs() && node.attributes.id == '4' && node.attributes.type == '2',
					    ispub : node.attributes.ispub,
					    type : node.attributes.type
				    },
				    success : function(resp) {
					    var timer = new ProgressTimer({
						      initData : Ext.decode(resp.responseText),
						      progressId : 'syncWord',
						      boxConfig : {
							      title : '同步词类'
						      }
					      });
					    timer.start();
				    },
				    failure : function(resp) {
					    Ext.Msg.alert('错误', resp.responseText);
				    }
			    });
		  } else {
			  Ext.Ajax.request({
				    url : 'wordclass!syncAI.action',
				    params : {
					    'ispub' : node.attributes.ispub,
					    'syncCategoryId' : node.attributes.id,
					    'bh' : node.attributes.bh
				    },
				    success : function(resp) {
					    var timer = new ProgressTimer({
						      initData : Ext.decode(resp.responseText),
						      progressId : 'syncWord',
						      boxConfig : {
							      title : '同步词类'
						      }
					      });
					    timer.start();
				    },
				    failure : function(resp) {
					    Ext.Msg.alert('错误', resp.responseText);
				    }
			    });
		  }
	  },
	  polysemyCount : function() {
		  var self = this;
		  Ext.Ajax.request({
			    url : 'progress-timer!getProgress.action?progressId=polysemyWordCount',
			    success : function(resp) {
				    var progress = Ext.util.JSON.decode(resp.responseText);
				    if (!progress || progress.finished) {
					    Ext.Ajax.request({
						      url : 'wordclass!polysemyCount.action',
						      success : function(resp) {
							      var result = Ext.util.JSON.decode(resp.responseText);
							      self.getPolysemyCountTimerConfig(result, function(config) {
								        ProgressTimer.createTimer(config, true);
							        });
						      },
						      failure : function(resp) {
							      Ext.Msg.alert('错误', resp.responseText);
						      }
					      });
				    } else {
					    self.getPolysemyCountTimerConfig(null, function(config) {
						      ProgressTimer.createTimer(config, true);
					      });
				    }
			    },
			    failure : function(resp) {
			    }
		    });
	  },
	  getPolysemyCountTimerConfig : function(initdata, cb) {
		  var self = this;
		  cb({
			    initData : initdata,
			    progressId : 'polysemyWordCount',
			    boxConfig : {
				    title : '统计所有词的多义性'
			    },
			    finish : function(p) {
				    if (p.currentCount > 0) {
					    Ext.MessageBox.hide();
					    self.showPolysemyResult();
				    }
			    }
		    })
	  },
	  showPolysemyResult : function() {
		  var self = this;
		  self.polysemyStore.load({
			    params : {
				    start : 0,
				    limit : 20
			    }
		    });
		  self.polysemyWordWin.show();
	  },
	  saveWord : function(btn, forceSave) {
		  var self = this;
		  var polysemyTreeCombo = self.polysemyTreeCombo;
		  if (polysemyTreeCombo.getValue().length == 1) {
			  Ext.Msg.alert('错误提示', '根类别下不能直接新增词类');
			  return false;
		  }
		  btn.disable();
		  var f = self.polysemyformPanel;
		  if (!f.getForm().isValid()) {
			  Ext.Msg.alert('提示', '您尚未正确完整的输入表单内容，请检查！');
			  btn.enable();
			  return false;
		  }
		  this.data = f.getForm().getValues();
		  this.data.category = {};
		  this.data.category.categoryId = polysemyTreeCombo.getValue();
		  var self = this;
		  var ispub = this.data.ispub;
		  delete this.data.ispub;
		  delete this.data.categoryId;
		  delete this.data.type;
		  if (isAudit) {
			  var wordStore = self.wordEditGrid.getStore();
			  var wordRecords = [];
			  var records = wordStore.getRange();
			  var isSynosEmpty = 0;
			  Ext.each(records, function(word) {
				    if (word.data.name)
					    isSynosEmpty++;
			    });
			  if (isSynosEmpty < 1) {
				  Ext.Msg.alert('错误提示', '同义词不能为空');
				  btn.enable();
				  return;
			  }
			  for (var i = 0; i < records.length; i++) {
				  var data = records[i].data;
				  if (!data.name)
					  continue;
				  if (!btn.ignoreRepeat.checked) {
					  for (var j = records.length - 1; j > -1; j--) {
						  if (i == j) {
							  continue;
						  }
						  var word = records[j].data;
						  if (!word.name)
							  continue;
						  if (self.toTrim(data.name) === self.toTrim(word.name) && self.toTrim(data.name) != '' && self.toTrim(word.name) != '') {
							  Ext.Msg.alert('重复提示', '同义词&nbsp;' + self.toTrim(data.name) + '&nbsp;有重复!');
							  btn.enable();
							  return;
						  }
						  if (self.toTrim(data.name).toUpperCase() === self.toTrim(word.name).toUpperCase() && self.toTrim(data.name) != '' && self.toTrim(word.name) != '') {
							  Ext.Msg.alert('重复提示', '同义词&nbsp;' + self.toTrim(data.name) + '&nbsp;大小写有重复!');
							  btn.enable();
							  return;
						  }
					  }
				  }
				  wordRecords.push(data);
			  };
			  this.data.synoWords = wordRecords;
		  } else {
			  var _synos = f.limitExceed ? '' : this.data.synos;
			  if (_synos == '' || _synos.replace(/(^\s*)|(\s*$)/g, "").length == 0) {
				  if (!f.limitExceed) {
					  Ext.Msg.alert('错误提示', '同义词不能为空');
					  btn.enable();
					  return;
				  }
			  }
			  var _pinyins = this.data.pinyins;
			  delete this.data.synos;
			  delete this.data.pinyins;
		  }
		  _synos = _synos ? _synos.replace(/\r\n/g, "\n") : _synos;
		  if (!btn.ignoreRepeat.checked) {
			  var str = Ext.encode(_synos);
			  var arr = str.substring(1, str.length - 1).split("\\n")
			  if (arr != null) {
				  for (var i = 0; i < arr.length; i++) {
					  for (var j = arr.length - 1; j >= 0; j--) {
						  if (i == j) {
							  continue;
						  }
						  if (self.toTrim(arr[i]) === self.toTrim(arr[j]) && self.toTrim(arr[i]) != '' && self.toTrim(arr[j]) != '') {
							  Ext.Msg.alert('重复提示', '同义词&nbsp;' + self.toTrim(arr[i]) + '&nbsp;有重复!');
							  btn.enable();
							  return;
						  }
						  if (self.toTrim(arr[i]).toUpperCase() === self.toTrim(arr[j]).toUpperCase() && self.toTrim(arr[i]) != '' && self.toTrim(arr[j]) != '') {
							  Ext.Msg.alert('重复提示', '同义词&nbsp;' + self.toTrim(arr[i]) + '&nbsp;大小写有重复!');
							  btn.enable();
							  return;
						  }
					  }
				  }
			  }
		  }
		  if (polysemyTreeCombo.lastSelectedNode && ispub && ispub != polysemyTreeCombo.lastSelectedNode.attributes.ispub) {
			  Ext.Msg.alert('提示', '不能改变词类所在的数据库:' + ispub + '->' + polysemyTreeCombo.lastSelectedNode.attributes.ispub);
			  return;
		  }

		  if (this.treeCombo.lastSelectedNode)
			  ispub = this.treeCombo.lastSelectedNode.attributes.ispub;
		  Ext.Ajax.request({
			    url : 'wordclass!save.action',
			    params : {
				    'ispub' : ispub,
				    'data' : Ext.encode(ux.util.resetEmptyString(self.data)),
				    'synos' : _synos,
				    'keepPrevWords' : f.limitExceed ? 'true' : 'false',
				    'pinyins' : _pinyins,
				    'sync' : f.syncCheckbox.getValue(),
				    'categoryId' : self.data.category.categoryId,
				    'forceSave' : forceSave ? forceSave : false,
				    'ignoreRepeatWord' : btn.ignoreRepeat.checked,
				    'isAudit' : isAudit
			    },
			    success : function(resp) {
				    var isvali = Ext.util.JSON.decode(resp.responseText).message;
				    if (typeof isvali != 'undefined') {
					    if (isvali.length > 3) {
						    Ext.Msg.alert('错误信息', isvali);
						    btn.enable();
						    return;
					    }
				    }
				    btn.enable();
				    // self.endEdit();
				    Dashboard.setAlert('保存成功！')
				    if (this.saveMode)
					    this.addWord(false)
				    else {
					    this.noAutoSelect = true
				    }
			    },
			    failure : function(resp) {
				    if (resp.responseText.indexOf('qtip:') == 0) {
					    Ext.Msg.confirm('确定框', resp.responseText.replace('qtip:', ''), function(sel) {
						      if (sel == 'yes') {
							      self.saveWord(btn, true);
						      }
					      });
				    } else {
					    Ext.Msg.alert('错误', resp.responseText);
					    btn.enable();
				    }
			    },
			    scope : this
		    });
	  },
	  loadWordclassWord : function(ispub, wordclassId, isAudit) {
		  var self = this;
		  var _params = {
			  wordclassId : wordclassId
		  };
		  _params.ispub = ispub;
		  _params.isAudit = isAudit;
		  Ext.Ajax.request({
			    url : 'wordclass!findWordclassWords.action',
			    params : _params,
			    success : function(resp) {
				    var total = Ext.decode(resp.responseText).total;
				    var _words = Ext.decode(resp.responseText);
				    self.wordProxy.setData(_words);
				    self.wordEditGrid.getStore().load();
			    }
		    })
	  },
	  toTrim : function(str) {
		  return str.replace(/(^\s*)|(\s*$)/g, '');
	  },
	  authName : 'kb.word'
  });

Dashboard.registerNav(WordNavPanel, 3);

ShortcutToolRegistry.register('wordclassManager', function() {
	  Dashboard.navPanel.items.each(function(navPanel) {
		    if ('词类管理' == navPanel.title) {
			    // navPanel.load()
			    navPanel.expand();
			    var node = navPanel.getRootNode();
			    navPanel.expandNode(node);
			    node.expand();
			    if (node.childNodes && node.childNodes.length) {
				    node.childNodes[0].expand();
			(function() {
					    node.childNodes[0].fireEvent('click', node.childNodes[0]);
				    }).defer(100);
			    }
		    }
	    });
  }.dg(this));