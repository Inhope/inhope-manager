OntologyClassPanel = function(cfg) {
	cfg = cfg || {};
	var self = this;

	this.data = {
		ontologyRules : []
	};

	this.nameField = new Ext.form.TextField({
		  width : 100
	  });

	this.standardRuleField = new Ext.form.TextField({
		  width : 200
	  });
	this.answerTemplateField = new Ext.form.TextArea({
		  width : 200,
		  height : 300
	  });
	this.attrsample = new Ext.form.TextField({
		  width : 150
	  });
	this.sampleData = new Ext.form.TextField({
		  width : 200
	  });

	this.rqCheckbox = new Ext.form.Checkbox({
		  width : 15,
		  height : 22
	  });

	this.newTab = false;

	this.RuleRecord = Ext.data.Record.create(['ruleId', 'rule', 'flag']);
	this.ruleProxy = new LocalProxy({});
	this.disabledRuleIds = {}
	var pluginOpCol = new OperationColumn({
		  operationConfig : [{
			    getName : function(v, m, r, row, col, store) {
				    return r.get('attributeName') ? (this.disabledRuleIds[r.get('ruleId')] ? '启用' : '禁用') : ''
			    }.dg(this),
			    handler : function(grid, row, col, e) {
				    var r = grid.store.getAt(row)
				    if (this.disabledRuleIds[r.get('ruleId')]) {
					    delete this.disabledRuleIds[r.get('ruleId')]
				    } else {
					    this.disabledRuleIds[r.get('ruleId')] = true
				    }
				    grid.getView().refreshRow(row)
			    }.dg(this)
		    }, {
			    getName : function(v, m, r, row, col, store) {
				    return !r.get('attributeName') ? '删除' : ''
			    }.dg(this),
			    handler : function(grid, row, col, e) {
				    grid.store.removeAt(row)
			    }.dg(this)
		    }]
	  })
	var ruleEditGrid = this.ruleEditGrid = new ExcelLikeGrid({
		  flex : 5,
		  plugins : pluginOpCol,
		  autoExpandColumn : 'rule',
		  style : 'border-right: 1px solid ' + sys_bdcolor,
		  border : false,
		  clicksToEdit : 1,
		  frame : false,
		  columnLines : true,
		  selModel : new Ext.grid.RowSelectionModel(),
		  subgridLoader : {
			  load : self.loadSamples,
			  scope : self
		  },
		  pasteable : true,
		  columns : [new Ext.grid.RowNumberer(), {
			    id : 'rule',
			    header : '语义表达式',
			    dataIndex : 'rule',
			    width : 120,
			    editor : new Ext.form.TextField(),
			    replaceable : true,
			    renderer : Dashboard.utils.newTemplateRenderer()
		    }, {
			    header : '引用抽象语义',
			    dataIndex : 'attributeName',
			    width : 120,
			    hidden:!(sys_labs || Dashboard.u().isSupervisor())
		    }, {
			    header : '类型',
			    dataIndex : 'type',
			    width : 120,
			    editor : Dashboard.utils.newFaqTypeCombo(),
			    renderer : Dashboard.utils.newFaqTypeRenderer()
		    }, {
			    header : '排序',
			    dataIndex : 'flag',
			    width : 50,
			    editor : {
				    xtype : 'textfield'
			    },
			    hidden:true
		    }, pluginOpCol],
		  store : new Ext.data.Store({
			    proxy : self.ruleProxy,
			    reader : new Ext.data.JsonReader({
				      idProperty : 'ruleId',
				      root : 'ontologyRules',
				      fields : self.RuleRecord
			      }),
			    writer : new Ext.data.JsonWriter()
		    })
	  });
	ruleEditGrid.on('afteredit', function(e) {
		  var r = e.record
		  var v = e.value
		  if (e.field == 'rule') {
			  if (v.indexOf('[') != -1) {
				  if (!r.get('type'))
					  r.set('type', 1)
			  } else {
				  r.set('type', 0)
			  }
		  }
	  })

	this.SampleRecord = Ext.data.Record.create(['sampleId', 'sample']);
	this.sampleProxy = new LocalProxy({});
	var sampleEditGrid = this.sampleEditGrid = new ExcelLikeGrid({
		  autoExpandColumn : 'sample',
		  anchor : '100% -60',
		  border : true,
		  clicksToEdit : 1,
		  frame : false,
		  columnLines : true,
		  selModel : new Ext.grid.RowSelectionModel(),
		  pasteable : true,
		  columns : [new Ext.grid.RowNumberer(), {
			    id : 'sample',
			    header : '模板样例',
			    dataIndex : 'sample',
			    width : 120,
			    editor : new Ext.form.TextField(),
			    replaceable : true
		    }, new DeleteButtoner()],
		  store : new Ext.data.Store({
			    proxy : self.sampleProxy,
			    reader : new Ext.data.JsonReader({
				      idProperty : 'sampleId',
				      root : 'samples',
				      fields : self.SampleRecord
			      }),
			    writer : new Ext.data.JsonWriter()
		    })
	  });

	var sampleForm = this.sampleForm = new Ext.form.FormPanel({
		  flex : 3,
		  margins : '0 0 0 5',
		  padding : 8,
		  style : 'border-left: 1px solid ' + sys_bdcolor,
		  border : false,
		  frame : false,
		  labelAlign : 'top',
		  bodyStyle : 'background-color:' + sys_bgcolor,
		  items : [{
			    xtype : 'textfield',
			    anchor : '100%',
			    fieldLabel : '模板',
			    ref : 'ruleField',
			    listeners : {
				    blur : function() {
					    var r = ruleEditGrid.getSelectionModel().getSelected();
					    if (r)
						    r.set("rule", this.getValue())
				    }
			    }
		    }, sampleEditGrid]
	  });

	OntologyClassPanel.superclass.constructor.call(this, {
		  border : false,
		  frame : false,
		  // bodyStyle : 'background-color:' + sys_bgcolor,
		  // style : 'border-top: 1px solid ' + sys_bdcolor,
		  layout : {
			  type : 'hbox',
			  align : 'stretch'
		  },
		  items : [ruleEditGrid, sampleForm],
		  buttonAlign : 'center',
		  tbar : [{
			    xtype : 'label',
			    html : '&nbsp;属性名称:&nbsp;'
		    }, this.nameField, '-', {
			    xtype : 'label',
			    html : '&nbsp;标准问模板:&nbsp;'
		    }, this.standardRuleField, '-'].concat(!isVersionStandard(true) ? [] : [{
			    xtype : 'label',
			    html : '&nbsp;属性样例:&nbsp;'
		    }, this.attrsample, '-']).concat([!Dashboard.u().isSupervisor() && !Dashboard.isLabs() ? '' : {
			    xtype : 'label',
			    html : '&nbsp;语义示例:&nbsp;'
		    }, !Dashboard.u().isSupervisor() && !Dashboard.isLabs() ? '' : this.sampleData, !Dashboard.u().isSupervisor() ? '' : '-', {
			    text : '刷新',
			    iconCls : 'icon-refresh',
			    handler : function() {
				    self.load(self.data.attributeId);
			    }
		    }]).concat(Dashboard.u().allow("kb.cls.M") ? [{
			    text : '替换...',
			    iconCls : 'icon-text-replace',
			    handler : function() {
				    var win = new ReplaceWindow({
					      grid : [ruleEditGrid, sampleEditGrid]
				      });
				    win.show();
			    }
		    }] : []).concat([{
			    text : '保存',
			    ref : '../tname',
			    iconCls : 'icon-table-save',
			    handler : function() {
				    self.save();
			    }
		    }, {
			    xtype : 'label',
			    ref : '../ttitle',
			    html : '(重新生成标准问'
		    }, this.rqCheckbox, {
			    xtype : 'label',
			    html : ')'
		    }])
	  });

	ruleEditGrid.getSelectionModel().on('selectionchange', function() {
		  var r = this.getSelected();
		  if (!r || !r.get('type'))
			  sampleEditGrid.disable()
		  else
			  sampleEditGrid.enable()
	  })
	ruleEditGrid.on('rowclick', function(g, row, e) {
		  var r = g.store.getAt(row);
		  if (!r || !r.get('type'))
			  sampleEditGrid.disable();
		  else
			  sampleEditGrid.enable()
	  })
	ruleEditGrid.on("scrollbottomclick", function(e) {
		  ruleEditGrid.batchAdd(28);
	  });
	sampleEditGrid.on("scrollbottomclick", function(e) {
		  sampleEditGrid.batchAdd(18);
	  });

	ruleEditGrid.on("shiftright", function(e) {
		  var g = sampleEditGrid
		  if (g.disabled)
			  return;
		  var col = g.findMostLeftEditCol()
		  g.getSelectionModel().selectRow(0)
		  this.stopEditing()
		  g.startEditing(0, col)
	  });
	sampleEditGrid.on("shiftleft", function(e) {
		  var g = ruleEditGrid
		  var col = g.findMostRightEditCol()
		  var r = g.getSelectionModel().getSelected()
		  var row = g.getStore().indexOf(r)
		  this.stopEditing()
		  g.startEditing(row, col)
	  });

	this.on("beforeclose", function() {
		  if (self.forceClose)
			  return;
		  sampleEditGrid.stopEditing()
		  ruleEditGrid.stopEditing()
		  if (this.isModified()) {
			  Ext.Msg.show({
				    title : '提示',
				    msg : '数据未保存，是否要保存?',
				    buttons : Ext.Msg.YESNOCANCEL,
				    fn : function(result) {
					    if (result == 'yes') {
						    self.save(function() {
							      self.forceClose = true;
							      self.close();
						      })
					    } else if (result == 'no') {
						    self.forceClose = true;
						    self.close();
					    }
				    },
				    scope : this,
				    icon : Ext.Msg.QUESTION,
				    minWidth : Ext.Msg.minWidth
			    });
			  return false;
		  }
	  }, this);
	// >>> 引用属性 >>>
	var parentAttributes = this.parentAttributes = new TreeComboCheck({
		  ref : 'parentAttributes',
		  dataUrl : 'ontology-class!list.action?attrCheckable=true&classtype=3',
		  name : 'parentAttributes',
		  anchor : '100%',
		  fieldLabel : '本体分类',
		  listWidth : 250,
		  width : 150,
		  treeConfig : {
			  tbar : [{
				    text : '清空',
				    iconCls : 'icon-refresh',
				    handler : function() {
					    parentAttributes.setValueEx();
				    }
			    }, {
				    text : '刷新',
				    iconCls : 'icon-refresh',
				    handler : function() {
					    parentAttributes.reload();
				    }
			    }]
		  },
		  onValueChange : function(values, v, checked) {
		  }
	  });
	parentAttributes.loader.strictCheckable = true
	this.parentAttributeSb = Ext.create({
		  name : 'parentAttributeSb',
		  xtype : 'textfield',
		  width : 300
	  })
	this.tbarEx = new Ext.Toolbar({
		  items : ['引用属性:&nbsp;', parentAttributes, '&nbsp;替换内容:&nbsp;', this.parentAttributeSb, '-', !Dashboard.u().isSupervisor() ? '' : {
			    text : '答案模板',
			    menu : {
				    items : [this.answerTemplateField]
			    },
			    width : 40
		    }]
	  });
	this.tbarSearchInput = new Ext.create({
		  xtype : 'textfield',
		  emptyText:'输入语义表达式搜索...',
		  width : 200
	  })
	this.tbarSearch = new Ext.Toolbar({
		  items : [{
			    xtype : 'splitbutton',
			    text : '搜索',
			    iconCls : 'icon-search',
			    handler : function() {
				    var v = this.tbarSearchInput.getValue()
				    if (!v || !v.trim()) {
					    ruleEditGrid.store.clearFilter()
				    } else {
					    ruleEditGrid.store.filter('rule', v.trim(),!this.matchExactly,false,this.matchExactly)
				    }
			    }.dg(this),
			    menu : {
				    items : [{
					      text : '精确匹配',
					      checked : false,
					      checkHandler : function(btn, checked) {
						      this.matchExactly = checked
					      }.dg(this)
				      }]
			    }
		    }, this.tbarSearchInput]
	  });

	this.on('render', function() {
		  Dashboard.u().isSupervisor() || sys_labs ? this.tbarEx.render(this.tbar) : 0 //chengxin需求
		  this.tbarSearch.render(this.tbar);
	  }, this);
};

Ext.extend(OntologyClassPanel, Ext.Panel, {

	  _clearIds : function() {
		  this.data.attributeId = null;
		  Ext.each(this.data.ontologyRules, function(rule) {
			    rule.ruleId = null;
			    Ext.each(rule.samples, function(sample) {
				      sample.sampleId = null;
			      });
		    });
	  },
	  isModified : function() {
		  if (this.nameField.getValue() != this.data.name)
			  return true;
		  if (this.standardRuleField.getValue() != this.data.standardRule)
			  return true;
		  if (this.attrsample.getValue() != this.data.sample)
			  return true;
		  if ((this.sampleData.getValue() || '') != (this.data.sampleData || ''))
			  return true;
		  if ((this.parentAttributeSb.getValue() || '') != (this.data.parentAttributeSb || ''))
			  return true;
		  return this.ruleProxy.isModified || this.sampleProxy.isModified
	  },
	  load : function(attrId, isPaste) {
		  if (attrId) {
			  this.newTab = false;
			  Ext.Ajax.request({
				    url : 'ontology-class!loadAttr.action?attrId=' + attrId,
				    success : function(form, action) {
					    this.data = Ext.util.JSON.decode(form.responseText);
					    this.data.ontologyClass = {
						    'classId' : this.classId
					    };
					    if (isPaste)
						    this._clearIds();
					    this._refresh();
				    },
				    failure : function(form, action) {
					    Ext.Msg.alert('提示', '获取本体类别失败');
				    },
				    scope : this
			    });
		  } else {
			  this.newTab = true;
			  this.data = {
				  ontologyRules : []
			  };
			  this._refresh();
		  }
		  if ((Dashboard.u().allow("kb.cls.C") && this.newTab) || (Dashboard.u().allow("kb.cls.M") && !this.newTab)) {
			  this.tname.enable();
			  this.ttitle.enable();
		  } else {
			  this.tname.disable();
			  this.ttitle.disable();
		  }
	  },
	  loadSamples : function(record) {
		  this.sampleForm.ruleField.setValue(record.data.rule)
		  if (!record.data.samples) {
			  record.data.samples = [];
		  }
		  if (!this.ruleProxy.isModified)
			  this.ruleProxy.isModified = this.sampleProxy.isModified
		  this.sampleProxy.setData(record.data)
		  this.sampleEditGrid.getStore().reload();
		  this.sampleEditGrid.batchAdd(18);
	  },
	  _refresh : function() {
		  // filter the disable occupiers
		  if (this.disabledRuleIds)
			  delete this.disabledRuleIds
		  this.disabledRuleIds = {}
		  var rs = this.data.ontologyRules;
		  for (var i = rs.length - 1; i >= 0; i--) {
			  var d = rs[i]
			  if (d.type == 101) {
				  this.disabledRuleIds[d.rule] = true
				  rs.splice(i, 1)
			  }
		  }
		  // load rules
		  this.ruleProxy.setData(this.data);
		  var grid = this.ruleEditGrid;
		  grid.getStore().load();
		  if (this.data.name)
			  this.nameField.setValue(this.data.name);
		  else
			  this.nameField.reset();
		  this.data.standardRule ? this.standardRuleField.setValue(this.data.standardRule) : this.standardRuleField.reset()
		  this.data.answerTemplate ? this.answerTemplateField.setValue(this.data.answerTemplate) : this.answerTemplateField.reset()
		  this.attrsample.setValue(this.data.sample);
		  this.sampleData.setValue(this.data.sampleData);
		  if (this.data.parentAttributes) {
			  Ext.each(this.data.parentAttributes, function(d) {
				    d.id = d.attributeId
				    delete d.attributeId
			    });
			  this.parentAttributes.setValue(this.data.parentAttributes);
		  }
		  this.parentAttributeSb.setValue(this.data.parentAttributeSb);
		  grid.batchAdd(28);
		  var i = grid.defaultSelected
		  if (!i)
			  i = 0;
		  grid.getSelectionModel().selectRow(i);
	  },
	  save : function(cb) {
		  var attrName = this.nameField.getValue();
		  if (!attrName) {
			  Ext.Msg.alert('提示', '属性名称不能为空');
			  return false;
		  }

		  if (!this.navPanel.validateName(this.data.attributeId, this.classId, attrName)) {
			  Ext.Msg.alert('信息', '属性名重复！');
			  return;
		  }

		  this.data.name = attrName;
		  this.data.standardRule = this.standardRuleField.getValue();
		  this.data.answerTemplate = this.answerTemplateField.getValue();
		  this.data.sample = this.attrsample.getValue();
		  this.data.sampleData = this.sampleData.getValue();
		  this.data.parentAttributes = this.parentAttributes.getValue();
		  if (this.data.parentAttributes) {
			  var ds = []
			  Ext.each(this.data.parentAttributes, function(d) {
				    var d = Ext.apply({}, d)
				    d.attributeId = d.id
				    delete d.id
				    ds.push(d)
			    })
			  this.data.parentAttributes = ds
		  }
		  this.data.parentAttributeSb = this.parentAttributeSb.getValue();
		  if (this.tbarEx.ownerCt && !this.data.parentAttributeSb && this.data.parentAttributes && this.data.parentAttributes.length) {
			  Dashboard.setAlert('请先填写"替换内容"后重新保存！', 'error');
			  return;
		  }
		  if (this.classtype) {
			  if (this.classtype == 3) {
				  if (!this.data.sampleData || !this.data.sampleData.trim()) {
					  Dashboard.setAlert('请先填写"语义示例"后重新保存！', 'error');
					  return;
				  }
			  }
			  this.data.type = this.classtype;
		  } else {
			  this.data.type = 1;
			  if (this.data.ontologyRules && this.data.ontologyRules.length) {
				  Ext.each(this.data.ontologyRules, function(d) {
					    if (d.rule && d.rule.indexOf('YYY') != -1) {
						    this.data.type = 2;
						    return false;
					    }
				    }, this)
			  }
		  }
		  var allRuleIds = {}
		  if (this.data.ontologyRules && this.data.ontologyRules.length) {
			  var rs = this.data.ontologyRules;
			  for (var i = rs.length - 1; i >= 0; i--) {
				  var d = rs[i]
				  if (d.ruleId)
					  allRuleIds[d.ruleId] = true
				  if (d.attributeName) {
					  rs.splice(i, 1)
				  }
			  }
		  }
		  for (var k in this.disabledRuleIds) {
			  if (allRuleIds[k])
				  this.data.ontologyRules.push({
					    rule : k,
					    type : 101
				    })
		  }
		  this.data.ontologyClass = {
			  'classId' : this.classId
		  };

		  this.getEl().mask('保存...')
		  Ext.Ajax.request({
			    url : 'ontology-class!saveAttr.action',
			    params : {
				    regenq : this.rqCheckbox.getValue(),
				    data : Ext.encode(this.data)
			    },
			    success : function(form, action) {
				    this.getEl().unmask()
				    Dashboard.setAlert("保存成功!");
				    var data = Ext.util.JSON.decode(form.responseText);
				    this.data = data.attachment;
				    this._refresh();
				    data.attachment = null;
				    this.navPanel.updateAttr(data, this.classId, this.tabId);
				    if (cb)
					    cb();
			    },
			    failure : function(response) {
				    this.getEl().unmask()
				    Ext.Msg.alert("错误", response.responseText);
			    },
			    scope : this
		    });
	  }

  });