PreSuffixPanel = function() {

	var self = this;
	var _pageSize = 25;

	var store = new Ext.data.Store({
		  proxy : new Ext.data.HttpProxy({
			    api : {
				    read : 'pre-suffix!list.action',
				    create : 'pre-suffix!invalid.action',
				    update : 'pre-suffix!invalid.action',
				    destroy : 'pre-suffix!delete.action'
			    }
		    }),
		  reader : new Ext.data.JsonReader({
			    idProperty : 'id',
			    root : 'data',
			    fields : [{
				      name : 'id',
				      type : 'string'
			      }, {
				      name : 'rule',
				      type : 'string'
			      }, {
				      name : 'type',
				      type : 'int'
			      }]
		    }),
		  writer : new Ext.data.JsonWriter({
			    writeAllFields : true
		    }),
		  listeners : {
			  beforewrite : function(store, data, rs) {
				  if (!rs.get('rule'))
					  return false;
			  }
		  }
	  });
	var editor = new Ext.ux.grid.RowEditor({
		  saveText : '保存',
		  cancelText : '取消',
		  errorSummary : false
	  });
	editor.on({
		  scope : this,
		  afteredit : function(roweditor, changes, record, rowIndex) {
			  Ext.Ajax.request({
				    url : 'pre-suffix!save.action',
				    params : {
					    data : Ext.encode(record.data)
				    },
				    success : function(resp) {
					    var ret = Ext.util.JSON.decode(resp.responseText);
					    if (ret.success) {
						    store.commitChanges();
						    store.reload();
					    } else
						    Ext.Msg.alert('提示', ret.message);
				    }
			    });
		  }
	  });
	editor.on('canceledit', function(rowEditor, changes, record, rowIndex) {
		  var r, i = 0;
		  while ((r = store.getAt(i++))) {
			  if (r.phantom) {
				  store.remove(r);
			  }
		  }
	  });
	var sm = new Ext.grid.CheckboxSelectionModel();
	var pagingBar = new Ext.PagingToolbar({
		  store : store,
		  displayInfo : true,
		  pageSize : _pageSize,
		  prependButtons : true,
		  beforePageText : '第',
		  afterPageText : '页，共{0}页',
		  displayMsg : '第{0}到{1}条记录，共{2}条',
		  emptyMsg : "没有记录"
	  });
	var typeData = [[1, '前缀'], [2, '后缀']];
	var typeMap = {};
	for (var i = 0; i < typeData.length; i++)
		typeMap[typeData[i][0]] = typeData[i][1];
	// 定义一个set集合
	var fieldSet = new Ext.form.FieldSet({
		  autoHeight : true,
		  autoWeight : true,
		  defaults : {
			  width : 270,
			  labelWidth : 30
		  },
		  items : [{
			    xtype : 'fieldset',
			    autoHeight : true,
			    items : [{
				      // 定义一个上传信息的文本框
				      id : 'preSuffixUploadFile',
				      name : 'uploadFile',
				      xtype : 'textfield',
				      fieldLabel : '文件',
				      inputType : 'file',
				      anchor : '96%',
				      allowBlank : false
			      }]
		    }

		  ]
	  });
	// 定义个formPanel，把set信息添加此中
	var confFormPanel = new Ext.FormPanel({
		  region : 'center',
		  layout : 'fit',
		  enctype : 'multipart/form-data',
		  border : false,
		  width : 280,
		  fileUpload : true,
		  items : [fieldSet]
	  });
	this.confFormPanel = confFormPanel;
	// 打开窗口
	var configWindow = new Ext.Window({
		  width : 300,
		  height : 150,
		  plain : true,
		  border : false,
		  title : '前后缀信息',
		  closeAction : 'hide',
		  collapsible : true,// 折叠
		  closable : true, // 关闭
		  resizable : false,// 改变大小
		  draggable : true,// 拖动
		  minimizable : false,// 最小化
		  maximizable : false,// 最大化
		  animCollapse : true,
		  // layout : 'border',
		  // bodyStyle : 'padding:5px;',
		  items : [confFormPanel],
		  buttonAlign : "center",
		  buttons : [{
			    id : 'sub_button_preSuffix',
			    text : '提交',
			    handler : function() {
				    self.postPreSuffix(this); // 单击提交时响应事件
			    }
		    }, {
			    text : '重置',
			    type : 'reset',
			    id : 'clear_preSuffix',
			    handler : function() {
				    confFormPanel.form.reset();
			    }
		    }]
	  });
	var config = {
		id : 'preSuffixPanel',
		store : store,
		margins : '0 5 5 5',
		autoExpandColumn : 'rule',
		border : false,
		plugins : [editor],
		tbar : [{
			  iconCls : 'icon-presuffix-add',
			  text : '添加规则',
			  handler : function() {
				  var fix = new store.recordType({
					    rule : '',
					    type : 1
				    });
				  editor.stopEditing();
				  store.insert(0, fix);
				  self.getSelectionModel().selectRow(0);
				  editor.startEditing(0, 1);
			  }
		  }, {
			  iconCls : 'icon-presuffix-delete',
			  text : '删除规则',
			  handler : function() {
				  editor.stopEditing();
				  var rows = self.getSelectionModel().getSelections();
				  if (rows.length == 0) {
					  Ext.Msg.alert('提示', '请至少选择一条记录！');
					  return false;
				  }
				  Ext.Msg.confirm('提示', '确认删除这' + rows.length + '条记录？', function(btn) {
					    if (btn == 'yes') {
						    var ids = [];
						    Ext.each(rows, function() {
							      ids.push(this.id)
						      });
						    Ext.Ajax.request({
							      url : 'pre-suffix!delete.action',
							      params : {
								      ids : ids.join(',')
							      },
							      success : function(resp) {
								      Ext.Msg.alert('提示', '删除成功！', function() {
									        store.reload();
								        });
							      },
							      failure : function(resp) {
								      Ext.Msg.alert('错误', resp.responseText);
							      }
						      })
					    }
				    });
			  }
		  }, {
			  iconCls : 'icon-refresh',
			  text : '刷新',
			  handler : function() {
				  store.reload();
			  }
		  }, {
			  iconCls : 'icon-ontology-sync',
			  text : '同步',
			  handler : function() {
				  editor.stopEditing();
				  Ext.Ajax.request({
					    url : 'pre-suffix!sync.action',
					    success : function(resp) {
						    var ret = Ext.util.JSON.decode(resp.responseText);
						    if (ret.success) {
							    Ext.Msg.alert('提示', ret.message);
						    } else {
							    Ext.Msg.alert('错误', ret.message);
						    }
					    },
					    failure : function(resp) {
						    Ext.Msg.alert('错误', resp.responseText);
					    }
				    })
			  }
		  }, '-', '关键字:', {
			  id : 's_name',
			  xtype : 'textfield',
			  width : 100,
			  emptyText : '输入关键字搜索...'
		  }, ' 类型:', new Ext.form.ComboBox({
			    id : 's_type_suf',
			    triggerAction : 'all',
			    width : 80,
			    editable : false,
			    value : '',
			    mode : 'local',
			    store : new Ext.data.ArrayStore({
				      fields : ['type', 'displayText'],
				      data : [['', '全部']].concat(typeData)
			      }),
			    valueField : 'type',
			    displayField : 'displayText'
		    }), ' ', {
			  text : '搜索',
			  iconCls : 'icon-search',
			  handler : function() {
				  self.search();
			  }
		  }, ' ', {
			  text : '全部',
			  iconCls : 'icon-search',
			  handler : function() {
				  self.search(true);
			  }
		  }, {
			  iconCls : 'icon-ontology-import',
			  text : '导入',
			  handler : function() {
				  configWindow.show();
			  }
		  }, {
			  iconCls : 'icon-ontology-export',
			  text : '导出',
			  handler : function() {
				  self.exportPreSuffix();
			  }
		  }],
		bbar : pagingBar,
		sm : sm,
		loadMask : true,
		colModel : new Ext.grid.ColumnModel({
			  columns : [{
				    header : 'id',
				    dataIndex : 'id',
				    hidden : true
			    }, {
				    id : 'rule',
				    header : '规则',
				    dataIndex : 'rule',
				    editor : {
					    xtype : 'textfield',
					    allowBlank : false,
					    blankText : '请输入规则'
				    }
			    }, {
				    header : '类型',
				    dataIndex : 'type',
				    width : 200,
				    renderer : function(value, metaData, record, rowIndex, colIndex, store) {
					    return typeMap[value];
				    },
				    editor : {
					    xtype : 'combo',
					    allowBlank : false,
					    editable : false,
					    triggerAction : 'all',
					    mode : 'local',
					    store : new Ext.data.ArrayStore({
						      fields : ['id', 'displayText'],
						      data : typeData
					      }),
					    valueField : 'id',
					    displayField : 'displayText'
				    }
			    }]
		  })
	};
	this.getPageSize = function() {
		return _pageSize;
	}
	PreSuffixPanel.superclass.constructor.call(this, config);
}

Ext.extend(PreSuffixPanel, Ext.grid.GridPanel, {
	  loadData : function() {
		  this.getStore().load({
			    params : {
				    start : 0,
				    limit : this.getPageSize()
			    }
		    })
	  },
	  search : function(showAll) {
		  var tbar = this.getTopToolbar();
		  var _nameField = tbar.findById('s_name');
		  var _typeCombo = tbar.findById('s_type_suf');
		  if (showAll) {
			  _typeCombo.setValue('');
			  _nameField.setValue('');
		  }
		  var store = this.getStore();
		  for (var key in store.baseParams) {
			  if (key && key.indexOf('queryParam.') != -1)
				  delete store.baseParams[key];
		  }
		  var _name = _nameField.getValue().trim();
		  var _type = _typeCombo.getValue();
		  if (_name)
			  store.setBaseParam('queryParam.rule', _name);
		  if (_type)
			  store.setBaseParam('queryParam.type', _type);
		  store.load();
	  },
	  // 提交时要响应的信息
	  postPreSuffix : function() {
		  var self = this;
		  if (this.confFormPanel.getForm().isValid()) {
			  var fileName = Ext.getCmp("preSuffixUploadFile").getValue(); // 得到上传文件名
			  this.confFormPanel.getForm().submit({
				    url : 'pre-suffix!importData.action',
				    success : function(form, action) {
					    var res = action.result.message;
					    if (res != null) {
						    Ext.Msg.alert('导入失败', res);
					    } else {
						    Ext.Msg.alert('导入提示', '数据导入成功');
						    self.loadData();
					    }
				    },
				    failure : function() {
					    Ext.Msg.alert('错误提示', '导入失败');
					    Ext.getCmp("sub_button_preSuffix").setDisabled(false);
				    },
				    scope : this
			    });
		  } else {
			  Ext.Msg.alert('错误提示', '地址不能为空!');
		  }

	  },
	  exportPreSuffix : function() {
		  window.location = 'pre-suffix!exportData.action';
	  }
  });
