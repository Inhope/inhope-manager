ReplaceWindow = function(cfg) {

	var columns = [];

	columns.push({
				'id' : '_ALL_COL',
				name : '所有可选列'
			});
	columns['_ALL_COL'] = []

	Ext.each(cfg.grid, function(grid) {
				var cm = grid.getColumnModel().config
				for (var i = 0; i < cm.length; i++) {
					if (cm[i].replaceable) {
						var id = grid.getId() + cm[i].dataIndex
						var name = cm[i].headerName
						if (!name)
							name = cm[i].header
						columns.push({
									'id' : id,
									'name' : name
								});
						var d = {
							dataIndex : cm[i].dataIndex,
							store : grid.getStore()
						}
						columns[id] = d
						columns['_ALL_COL'].push(d)
					}
				}
			});

	var panel = new Ext.form.FormPanel({
				anchor : '100% 100%',
				frame : true,
				labelWidth : 60,
				items : [{
							xtype : 'combo',
							hiddenName : 'replaceTarget',
							fieldLabel : '替换对象',
							mode : 'local',
							store : new Ext.data.JsonStore({
										idProperty : 'id',
										fields : ['id', 'name'],
										data : columns
									}),
							anchor : '100%',
							value : columns[0].id,
							valueField : 'id',
							displayField : 'name',
							triggerAction : 'all',
							editable : false
						}, {
							xtype : 'textfield',
							name : 'matchText',
							fieldLabel : '查找内容',
							anchor : '100%'

						}, {
							xtype : 'textfield',
							name : 'replaceText',
							fieldLabel : '替换内容',
							anchor : '100%'
						}, {
							xtype : 'fieldset',
							title : '选项',
							anchor : '100%',
							hideLabel : true,
							labelWidth : 1,
							style : 'padding:0;margin:0;',
							items : {
								xtype : 'checkboxgroup',
								anchor : '100% 100%',
								height : 18,
								columns : 3,
								items : [{
											xtype : 'checkbox',
											name : 'bIgnoreCase',
											boxLabel : '不区分大小写'
										}, {
											xtype : 'checkbox',
											name : 'bRegexp',
											boxLabel : '正则表达式'
										}]
							}
						}],
				buttons : [{
							text : '替换',
							handler : function() {
								var v = panel.getForm().getValues();
								if (Ext.isEmpty(v.matchText)) {
									Ext.Msg.alert('查找内容不能为空!');
									return;
								}
								if (Ext.isEmpty(v.replaceText)) {
									Ext.Msg.alert('替换内容不能为空!');
									return;
								}

								var ts = columns[v.replaceTarget];

								var re = /([\(\)\[\]\{\}\^\$\+\-\*\?\.\"\'\|\/\\])/g
								var m = v.matchText
								if (v.bRegexp) {
									var opt = (v.bIgnoreCase ? 'ig' : 'g')
									m = new RegExp(m, opt)
								} else if (v.bIgnoreCase) {
									m = new RegExp(m.replace(re, "\\$1"), "ig");
								} else {
									m = new RegExp(m.replace(re, "\\$1"), "g");
								}

								Ext.each(ts, function(t) {
											var c = t.store.getCount()
											for (var i = 0; i < c; i++) {
												var r = t.store.getAt(i);
												var x = r.get(t.dataIndex)
												if (x) {
													var y = x.replace(m,
															v.replaceText)
													if (x != y)
														r.set(t.dataIndex, y)
												}
											}
										})

								this.close();
							},
							scope : this
						}, {
							text : '取消',
							handler : function() {
								this.close();
							},
							scope : this
						}]
			});

	AITesterWindow.superclass.constructor.call(this, {
				title : (cfg.title ? cfg.title : '替换'),
				plain : true,
				modal : true,
				width : 350,
				height : 216,
				layout : 'fit',
				maximizable : true,
				collapsible : true,
				items : panel,
				iconCls : 'icon-text-replace',
				bodyStyle : 'padding: 0 1px 1px 0px;',
				listeners : {
					show : function() {

					}
				}
			});
}

Ext.extend(ReplaceWindow, Ext.Window, {

});