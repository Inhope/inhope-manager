SemanticRecommendWindow = function(cfg) {

	var me = this;
	var toolPanel = this.toolPanel = new Ext.Panel({
				layout : 'border',
				border : false,
				items : [{
							split : true,
							region : 'south',
							bodyPadding : '10px',
							autoHeight : true,
							layout : 'form',
							defaults : {
								border : false,
								msgTarget : 'side',
								labelAlign : 'right',
								labelSeparator : " ： ",
								labelWidth : 80
							},
							items : 
								new Ext.FormPanel({
										frame : false,
										bodyStyle : 'padding : 5px 5px; background-color:' + sys_bgcolor,
										fit : true,
										items : [{
											anchor : '100%',
											emptyText : '支持回车Enter',
											xtype : 'textfield',
											fieldLabel : '用户问题',
											allowBlank : false,
											name : 'userQuestion',
											enableKeyEvents : true,
											listeners : {
												specialkey : function(field, e) {
													if (e.getKey() == e.ENTER) {
														me.doSubmit();
													}
												}
											}
										}]
									})
						}, {
							split : true,
							region : 'west',
							title : '定位抽象语义',
							width : 355,
							itemId : 'answer',
							xtype : 'textarea',
							readOnly : true,
							hideLabel : true
						}, {
							region : 'center',
							itemId : 'answer2',
							title : '定位抽象语义规则',
							xtype : 'textarea',
							readOnly : true,
							hideLabel : true
						}],
				buttons : [{
							id : 'recommendQuestion',
							text : '提交',
							handler : function(button, b) {
								button.disable();
								var formpanel = me.toolPanel;
								formpanel.items.get(1).setValue('');
								formpanel.items.get(2).setValue('');
								me.doSubmit();
								button.enable();
							}
						}]
			});

	var _cfg = {
		width : 700,
		height : 400,
		layout : 'fit',
		border : false,
		plain : true,
		shim : true,
		closable : true,
		closeAction : 'hide',
		collapsible : true,
		resizable : false,
		draggable : true,
		animCollapse : true,
		constrainHeader : true,
		title : '推荐工具',
		iconCls : 'icon-ontology-object',
		layout : 'fit',
		items : toolPanel

	}

	SemanticRecommendWindow.superclass.constructor.call(this, Ext.applyIf(_cfg || {}, cfg));

}
Ext.extend(SemanticRecommendWindow, Ext.Window, {
			doSubmit : function() {
				var me = this;
				var form = me.toolPanel.items.get(0).items.get(0).getForm();
				if (form.isValid()) {
					form.submit({
								url : 'wordclass!recommendQuestion.action',
								method : 'POST',
								success : function(form, action) {
									var data = Ext.util.JSON.decode(action.response.responseText);
									if (data.success) {
										var formpanel = me.toolPanel;
										formpanel.items.get(1).setValue(data.data.first);
										formpanel.items.get(2).setValue(data.data.second);
									}
								},
								failure : function(form, action) {
									Ext.Msg.alert("错误", "推荐失败!");
								}
							});
				}
			}
		});
