StopWordPanel = function() {

	var self = this;
	var _pageSize = 25;

	var store = new Ext.data.Store({
		  proxy : new Ext.data.HttpProxy({
			    api : {
				    read : 'stop-word!list.action',
				    create : 'stop-word!invalid.action',
				    update : 'stop-word!invalid.action',
				    destroy : 'stop-word!delete.action'
			    }
		    }),
		  reader : new Ext.data.JsonReader({
			    idProperty : 'id',
			    root : 'data',
			    fields : [{
				      name : 'id',
				      type : 'string'
			      }, {
				      name : 'name',
				      type : 'string'
			      }, {
				      name : 'type',
				      type : 'int'
			      }]
		    }),
		  writer : new Ext.data.JsonWriter({
			    writeAllFields : true
		    }),
		  listeners : {
			  beforewrite : function(store, data, rs) {
				  if (!rs.get('name'))
					  return false;
			  }
		  }
	  });
	var editor = new Ext.ux.grid.RowEditor({
		  saveText : '保存',
		  cancelText : '取消',
		  errorSummary : false
	  });
	editor.on({
		  scope : this,
		  afteredit : function(roweditor, changes, record, rowIndex) {
			  Ext.Ajax.request({
				    url : 'stop-word!save.action',
				    params : {
					    data : Ext.encode(record.data)
				    },
				    success : function(resp) {
					    var ret = Ext.util.JSON.decode(resp.responseText);
					    if (ret.success) {
						    store.commitChanges();
						    store.reload();
					    } else
						    Ext.Msg.alert('提示', ret.message);
				    }
			    });
		  }
	  });
	editor.on('canceledit', function(rowEditor, changes, record, rowIndex) {
		  var r, i = 0;
		  while ((r = store.getAt(i++))) {
			  if (r.phantom) {
				  store.remove(r);
			  }
		  }
	  });
	var sm = new Ext.grid.CheckboxSelectionModel();
	var pagingBar = new Ext.PagingToolbar({
		  store : store,
		  displayInfo : true,
		  pageSize : _pageSize,
		  prependButtons : true,
		  beforePageText : '第',
		  afterPageText : '页，共{0}页',
		  displayMsg : '第{0}到{1}条记录，共{2}条',
		  emptyMsg : "没有记录"
	  });
	var typeData = [[1, '严格停词'], [2, '一般停词'], [3, '宽泛停词']];
	var typeMap = {};
	for (var i = 0; i < typeData.length; i++)
		typeMap[typeData[i][0]] = typeData[i][1];

	// 定义一个set集合
	var fieldSet = new Ext.form.FieldSet({
		  autoHeight : true,
		  autoWeight : true,
		  defaults : {
			  width : 270,
			  labelWidth : 30
		  },
		  items : [{
			    xtype : 'fieldset',
			    autoHeight : true,
			    items : [{
				      // 定义一个上传信息的文本框
				      id : 'stopWorduploadFile',
				      name : 'uploadFile',
				      xtype : 'textfield',
				      fieldLabel : '文件',
				      inputType : 'file',
				      anchor : '96%',
				      allowBlank : false
			      }]
		    }

		  ]
	  });
	// 定义个formPanel，把set信息添加此中
	var confFormPanel = new Ext.FormPanel({
		  region : 'center',
		  layout : 'fit',
		  enctype : 'multipart/form-data',
		  border : false,
		  width : 280,
		  fileUpload : true,
		  items : [fieldSet]
	  });
	this.confFormPanel = confFormPanel;
	// 打开窗口
	var configWindow = new Ext.Window({
		  width : 300,
		  height : 150,
		  plain : true,
		  border : false,
		  title : '停词信息',
		  closeAction : 'hide',
		  collapsible : true,// 折叠
		  closable : true, // 关闭
		  resizable : false,// 改变大小
		  draggable : true,// 拖动
		  minimizable : false,// 最小化
		  maximizable : false,// 最大化
		  animCollapse : true,
		  // layout : 'border',
		  // bodyStyle : 'padding:5px;',
		  items : [confFormPanel],
		  buttonAlign : "center",
		  buttons : [{
			    id : 'sub_button_stopWord',
			    text : '提交',
			    handler : function() {
				    self.postStopWord(this); // 单击提交时响应事件
			    }
		    }, {
			    text : '重置',
			    type : 'reset',
			    id : 'clear_stopWord',
			    handler : function() {
				    confFormPanel.form.reset();
			    }
		    }]
	  });
	var config = {
		id : 'stopWordPanel',
		store : store,
		margins : '0 5 5 5',
		border : false,
		autoExpandColumn : 'name',
		plugins : [editor],
		tbar : [{
			  iconCls : 'icon-wordclass-add',
			  text : '添加停词',
			  handler : function() {
				  var word = new store.recordType({
					    name : '',
					    type : 1
				    });
				  editor.stopEditing();
				  store.insert(0, word);
				  self.getSelectionModel().selectRow(0);
				  editor.startEditing(0, 1);
			  }
		  }, {
			  iconCls : 'icon-wordclass-del',
			  text : '删除停词',
			  handler : function() {
				  editor.stopEditing();
				  var rows = self.getSelectionModel().getSelections();
				  if (rows.length == 0) {
					  Ext.Msg.alert('提示', '请至少选择一条记录！');
					  return false;
				  }
				  Ext.Msg.confirm('提示', '确认删除这' + rows.length + '条记录？', function(btn) {
					    if (btn == 'yes') {
						    var ids = [];
						    Ext.each(rows, function() {
							      ids.push(this.id)
						      });
						    Ext.Ajax.request({
							      url : 'stop-word!delete.action',
							      params : {
								      ids : ids.join(',')
							      },
							      success : function(resp) {
								      Ext.Msg.alert('提示', '删除成功！', function() {
									        store.reload();
								        });
							      },
							      failure : function(resp) {
								      Ext.Msg.alert('错误', resp.responseText);
							      }
						      })
					    }
				    });
			  }
		  }, {
			  iconCls : 'icon-refresh',
			  text : '刷新',
			  handler : function() {
				  store.reload();
			  }
		  }, {
			  iconCls : 'icon-ontology-sync',
			  text : '同步',
			  handler : function() {
				  editor.stopEditing();
				  Ext.Ajax.request({
					    url : 'stop-word!sync.action',
					    success : function(resp) {
						    var ret = Ext.util.JSON.decode(resp.responseText);
						    if (ret.success) {
							    Ext.Msg.alert('提示', ret.message);
						    } else {
							    Ext.Msg.alert('错误', ret.message);
						    }
					    },
					    failure : function(resp) {
						    Ext.Msg.alert('错误', resp.responseText);
					    }
				    })
			  }
		  }, '-', '关键字:', {
			  id : 's_name_stopword',
			  xtype : 'textfield',
			  width : 100,
			  emptyText : '输入关键字搜索...'
		  },
		  // ' 类型:',
		  new Ext.form.ComboBox({
			    id : 's_type',
			    triggerAction : 'all',
			    width : 80,
			    editable : false,
			    hidden : true,
			    value : '',
			    mode : 'local',
			    store : new Ext.data.ArrayStore({
				      fields : ['type', 'displayText'],
				      data : [['', '全部']].concat(typeData)
			      }),
			    valueField : 'type',
			    displayField : 'displayText'
		    }), ' ', {
			  text : '搜索',
			  iconCls : 'icon-search',
			  handler : function() {
				  self.search();
			  }
		  }, ' ', {
			  text : '全部',
			  iconCls : 'icon-search',
			  handler : function() {
				  self.search(true);
			  }
		  }, {
			  iconCls : 'icon-ontology-import',
			  text : '导入',
			  handler : function() {
				  configWindow.show();
			  }
		  }, {
			  iconCls : 'icon-ontology-export',
			  text : '导出',
			  handler : function() {
				  self.exportStopWord();
			  }
		  }],
		bbar : pagingBar,
		sm : sm,
		loadMask : true,
		colModel : new Ext.grid.ColumnModel({
			  columns : [{
				    header : 'id',
				    dataIndex : 'id',
				    hidden : true
			    }, {
				    id : 'name',
				    header : '名称',
				    dataIndex : 'name',
				    editor : {
					    xtype : 'textfield',
					    allowBlank : false,
					    blankText : '请输入停词名称'
				    }
			    }, {
				    header : '类型',
				    dataIndex : 'type',
				    hidden : true,
				    width : 200,
				    renderer : function(value, metaData, record, rowIndex, colIndex, store) {
					    return typeMap[value];
				    },
				    editor : {
					    xtype : 'combo',
					    allowBlank : false,
					    editable : false,
					    triggerAction : 'all',
					    mode : 'local',
					    store : new Ext.data.ArrayStore({
						      fields : ['id', 'displayText'],
						      data : typeData
					      }),
					    valueField : 'id',
					    displayField : 'displayText'
				    }
			    }]
		  })
	};
	this.getPageSize = function() {
		return _pageSize;
	}
	StopWordPanel.superclass.constructor.call(this, config);
}

Ext.extend(StopWordPanel, Ext.grid.GridPanel, {
	  loadData : function() {
		  this.getStore().load({
			    params : {
				    start : 0,
				    limit : this.getPageSize()
			    }
		    })
	  },
	  search : function(showAll) {
		  var tbar = this.getTopToolbar();
		  var _nameField = tbar.findById('s_name_stopword');
		  var _typeCombo = tbar.findById('s_type');
		  if (showAll) {
			  _typeCombo.setValue('');
			  _nameField.setValue('');
		  }
		  var store = this.getStore();
		  for (var key in store.baseParams) {
			  if (key && key.indexOf('queryParam.') != -1)
				  delete store.baseParams[key];
		  }
		  var _name = _nameField.getValue().trim();
		  var _type = _typeCombo.getValue();
		  if (_name)
			  store.setBaseParam('queryParam.name', _name);
		  if (_type)
			  store.setBaseParam('queryParam.type', _type);
		  store.load();
	  },
	  // 提交时要响应的信息
	  postStopWord : function() {
		  var self = this;
		  if (this.confFormPanel.getForm().isValid()) {
			  var fileName = Ext.getCmp("stopWorduploadFile").getValue(); // 得到上传文件名
			  this.confFormPanel.getForm().submit({
				    url : 'stop-word!importData.action',
				    success : function(form, action) {
					    var res = action.result.message;
					    if (res != null) {
						    Ext.Msg.alert('导入失败', res);
					    } else {
						    Ext.Msg.alert('导入提示', '数据导入成功');
						    self.loadData();
					    }
				    },
				    failure : function() {
					    Ext.Msg.alert('错误提示', '导入失败');
					    Ext.getCmp("sub_button_stopWord").setDisabled(false);
				    },
				    scope : this
			    });
		  } else {
			  Ext.Msg.alert('错误提示', '地址不能为空!');
		  }

	  },
	  exportStopWord : function() {
		  window.location = 'stop-word!exportData.action';
	  }
  });
