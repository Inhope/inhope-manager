SysWordClassPanel = function(_cfg) {
	var self = this;
	var _pageSize = 20;

	this.getPageSize = _pageSize;
	var _store = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({
			url : 'sys-wordclass!search.action'
		}),
		reader : new Ext.data.JsonReader({
			idProperty : 'id',
			root : 'data',
			fields : [ {
				name : 'id',
				type : 'string'
			}, {
				name : 'path',
				type : 'string'
			}, {
				name : 'name',
				type : 'string'
			}, {
				name : 'keyword',
				type : 'boolean'
			}, {
				name : 'children',
				type : 'string'
			} ]
		}),
		writer : new Ext.data.JsonWriter()
	});
	var wordText = new Ext.form.TextField({
		fieldLabel : "系统词",
		name : "word",
		allowBlank : true,
		anchor : '96%'
	});

	var rowNumber;
	var ds = _store;
	var checkboxSelectionModel = new Ext.grid.CheckboxSelectionModel({
		singleSelect : false
	})
	this.sm = checkboxSelectionModel;
	var myExpander = new Ext.grid.RowExpander({
		tpl : new Ext.Template(
				'<p style="color:#999;margin:2px 0px 2px 48px;">{answer}</p>')
	});

	var myColumnModel = new Ext.grid.ColumnModel(
			[
					checkboxSelectionModel,
					rowNumber = new Ext.grid.RowNumberer(),
					{
						header : 'id',
						dataIndex : 'id',
						hidden : true,
						sortable : false
					},
					{
						header : '词类',
						dataIndex : 'path',
						hidden : false,
						sortable : false,
						renderer : function(v, meta, rec) {
							if ('--summary' == rec.get('name'))
								return v + ' (' + rec.get('children') + ')';
							return v;
						}
					},
					{
						header : '系统词',
						dataIndex : 'name',
						width : 80,
						sorttable : false,
						renderer : function(v, meta, rec) {
							if ('--summary' == rec.get('name'))
								return '';
							return v;
						}
					},
					{
						header : '词性',
						dataIndex : 'keyword',
						width : 60,
						renderer : function(v, meta, rec) {
							if ('--summary' == rec.get('name'))
								return '';
							if (v)
								return '重点词';
							return '非重点词';
						}
					},
					{
						header : '操作',
						dataIndex : 'opr',
						width : 60,
						renderer : function(v, meta, rec) {
							if ('--summary' == rec.get('name'))
								return '';
							return "<a class='_delButton' href='javascript:void(0);' onclick='javscript:return false;'>删除</a>&nbsp;&nbsp;&nbsp;";
						}
					} ]);

	myColumnModel.defaultSortable = true;
	this.wordText = wordText;

	var actionTbar = new Ext.Toolbar({
		items : [
				'系统词:',
				wordText,
				{
					text : '查询',
					iconCls : 'icon-wordclass-search',
					handler : function() {
						var word = wordText.getValue();
						self.queryXiaoIWord(word);
					}
				},
				{
					text : '添加系统词',
					iconCls : 'icon-wordclass-add',
					handler : function() {
						var _formPanel = new Ext.form.FormPanel(
								{
									frame : false,
									border : false,
									labelWidth : 100,
									height : 300,
									waitMsgTarget : true,
									bodyStyle : 'padding : 5px 10px',
									defaults : {
										blankText : '',
										invalidText : '',
										anchor : '92%',
										xtype : 'textfield'
									},
									items : [ {
										fieldLabel : '词类名',
										name : 'wordclass',
										allowBlank : false,
										ref:'wordclass'
									}, {
										fieldLabel : '系统词',
										name : 'words',
										allowBlank : false,
										xtype : 'textarea',
										height : 220
									} ],
									buttons : [
											{
												text : '保存',
												handler : function() {
													if (!_formPanel.getForm()
															.isValid()) {
														return;
													}
													var wordclass = _formPanel
															.getForm().items
															.get(0).getValue();
													var words = _formPanel
															.getForm().items
															.get(1).getValue();
													self.addWord(wordclass,
															words);
												}
											},
											{
												text : '关闭',
												handler : function() {
													_addWin.hide();
													_formPanel.getForm()
															.reset();
												}
											} ]
								});
						var _addWin = new Ext.Window({
							width : 380,
							title : '基础词添加',
							defaults : {
								border : false
							},
							modal : true,
							plain : true,
							shim : true,
							closable : true,
							closeAction : 'hide',
							collapsible : true,
							resizable : false,
							draggable : true,
							animCollapse : true,
							constrainHeader : true,
							items : [ _formPanel ]
						});
						_addWin.show();
						var r = self.getSelectionModel().getSelected();
						if (r){
							_formPanel.wordclass.setValue(r.get('path'))
						}
					}
				}, {
					text : '批量导入',
					iconCls : 'icon-ontology-import',
					handler : function() {
						self.importData();
					}
				}, {
					ref : 'expBtn',
					text : '数据导出',
					iconCls : 'icon-ontology-export',
					handler : function() {
						self.exportData();
					}
				}, {
					ref : 'backBtn',
					text : '显示统计',
					iconCls : 'icon-ontology-cate',
					disabled : true,
					handler : function() {
						var b = this;
						self.getStore().load({
							params : {
								'summary' : true
							},
							callback : function() {
								b.setDisabled(true);
								self.expBtn.setDisabled(false);
							}
						});
					}
				} ]
	});
	this.expBtn = actionTbar.expBtn;
	this.backBtn = actionTbar.backBtn;

	var _fileForm = new Ext.FormPanel({
		layout : "fit",
		frame : true,
		border : false,
		autoHeight : true,
		waitMsgTarget : true,
		defaults : {
			bodyStyle : 'padding:10px'
		},
		margins : '0 0 0 0',
		labelAlign : "left",
		labelWidth : 55,
		fileUpload : true,
		items : [ {
			xtype : 'fieldset',
			title : '选择文件',
			autoHeight : true,
			items : [ {
				name : 'uploadFile',
				xtype : "textfield",
				fieldLabel : '文件',
				inputType : 'file',
				allowBlank : false,
				anchor : '96%'
			}, {
				xtype : 'checkbox',
				fieldLabel : '清空导入',
				checked : true,
				name : 'clean'
			} ]
		} ]
	});
	var _importPanel = new Ext.Panel({
		border : false,
		layout : "fit",
		layoutConfig : {
			animate : true
		},
		items : [ _fileForm ],
		buttons : [
				{
					text : "开始导入",
					handler : function() {
						var btn = this;
						this.disable();
						var fileForm = _fileForm.getForm();
						if (fileForm.isValid()) {
							fileForm.submit({
								url : 'sys-wordclass!importData.action',
								success : function(form, action) {
									btn.enable();
									if (action.result.message
											&& action.result.message
													.indexOf('error:') != -1) {
										Ext.Msg.alert('错误',
												action.result.message
														.substring(6));
									}
									var result = action.result.data;
									var timer = new ProgressTimer({
										initData : result,
										progressId : 'importSysWord',
										boxConfig : {
											title : ' 正在导入...'
										},
										finish : function(p, response) {
											Ext.MessageBox.hide();
											Ext.Msg.alert('成功提示', '导入成功!');
											self.wordText.setValue("");
											self.getStore().load({
												params : {
													'summary' : true
												}
											});
										}
									});
									timer.start();
								},
								failure : function(form) {
									btn.enable();
									Ext.MessageBox.hide();
									Ext.Msg.alert('导入失败', '文件上传失败');
								}
							});
						}
					}
				}, {
					text : "关闭",
					handler : function() {
						importWindow.hide();
					}
				} ]
	});

	var importWindow = new Ext.Window({
		border : false,
		width : 350,
		title : '基础词导入',
		defaults : {// 表示该窗口中所有子元素的特性
			border : false
		// 表示所有子元素都不要边框
		},
		plain : true,// 方角 默认
		modal : true,
		plain : true,
		shim : true,
		closeAction : 'hide',
		collapsible : true,// 折叠
		closable : true, // 关闭
		resizable : false,// 改变大小
		draggable : true,// 拖动
		minimizable : false,// 最小化
		maximizable : false,// 最大化
		animCollapse : true,
		constrainHeader : true,
		autoHeight : false,
		items : [ _importPanel ]
	});
	this.importWindow = importWindow;
	_store.on('beforeload', function() {
		var word = self.wordText.getValue();
		Ext.apply(this.baseParams, {
			word : word
		});
		scope: self
	});
	var cfg = {
		region : 'center',
		border : false,
		frame : false,
		store : _store,
		loadMask : true,
		colModel : myColumnModel,
		tbar : actionTbar,
		sm : checkboxSelectionModel,

		viewConfig : {
			forceFit : true
		}
	}
	SysWordClassPanel.superclass.constructor.call(this, Ext.applyIf(_cfg || {},
			cfg));
	this.on('cellclick', function(grid, row, col, e) {
		var delBtn = e.getTarget('._delButton');
		var rec = this.getStore().getAt(row);
		var self = this;
		if (delBtn) {
			Ext.Msg.confirm('提示', '确认删除？', function(btn) {
				if (btn == 'yes') {
					self.getEl().mask('删除中...');
					Ext.Ajax.request({
						url : 'sys-wordclass!delete.action',
						params : {
							path : rec.get('path'),
							word : rec.get('name')
						},
						success : function(resp) {
							self.getEl().unmask();
							Dashboard.setAlert('删除成功！');
							grid.getStore().reload();
						},
						failure : function(resp) {
							self.getEl().unmask();
							Ext.Msg.alert('错误', resp.responseText);
						}
					})
				}
			});
		}
	}, this);

}
Ext
		.extend(
				SysWordClassPanel,
				Ext.grid.GridPanel,
				{
					loadData : function() {
						this.getStore().load({
							params : {
								'summary' : true
							}
						});
						this.expBtn.setDisabled(false);
						this.backBtn.setDisabled(true);
					},
					deleteRecord : function(ids) {
						Ext.Ajax.request({
							url : 'ai-wordclass-manager!delete.action',
							success : function(response) {
								var result = Ext.util.JSON
										.decode(response.responseText).message;
								if (result == null) {
									Ext.Msg.alert('提示信息', '删除成功!');
									this.myAIWordStore.reload();
									this.loadData();
								} else {
									Ext.Msg.alert('失败', '删除成功!');
								}
							},
							failure : function(response) {
								Ext.Msg.alert("提示", "出错或者超时.<br>"+response.responseText);
							},
							xmlData : ids,
							scope : this
						});
					},
					queryXiaoIWord : function(word) {
						var self = this;
						this.getStore().load({
							params : {
								'word' : word
							},
							callback : function() {
								self.expBtn.setDisabled(true);
								self.backBtn.setDisabled(false);
							}
						});
					},
					addWord : function(wordclass, words) {
						Ext.Ajax.request({
							url : 'sys-wordclass!save.action',
							params : {
								'wordclass' : wordclass,
								'words' : words
							},
							success : function(response) {
								var result = Ext.util.JSON
										.decode(response.responseText).message;
								if (result == null) {
									Ext.Msg.alert('提示消息', '新增成功');
									this.wordText.setValue("");
									this.loadData();
								} else {
									Ext.Msg.alert('失败', result);
								}
							},
							failure : function(response) {
								Ext.Msg.alert("提示", "出错或者超时.<br>"+response.responseText);
							},
							scope : this
						});
					},
					importData : function() {
						this.importWindow.show();
					},
					exportData : function() {
						var self = this;
						var rows = this.getSelectionModel().getSelections();
						if (!rows || rows.length == 0) {
							Ext.Msg.alert('提示', '请先勾选要导出的词类名称');
							return false;
						}
						var wordclasses = '';
						Ext.each(rows, function(item) {
							wordclasses += item.get('path') + ',';
						})
						wordclasses = wordclasses.substring(0,
								wordclasses.length - 1);
						Ext.Ajax
								.request({
									url : 'sys-wordclass!exportData.action',
									params : {
										wordclasses : wordclasses
									},
									success : function(response) {
										var result = Ext.util.JSON
												.decode(response.responseText);
										if (result.message
												&& result.message
														.indexOf('err:') == 0) {
											Ext.Msg.alert('错误', result.message
													.substring(4));
											return false;
										}
										var timer = new ProgressTimer(
												{
													initData : result.data,
													progressId : 'exportSysWordStatus',
													boxConfig : {
														title : '正在导出...'
													},
													finish : function() {
														if (!self.downloadIFrame) {
															self.downloadIFrame = self
																	.getEl()
																	.createChild(
																			{
																				tag : 'iframe',
																				style : 'display:none;'
																			})
														}
														self.downloadIFrame.dom.src = 'sys-wordclass!downExportFile.action?_t='
																+ new Date()
																		.getTime();
														Ext.MessageBox.hide();
													}
												});
										timer.start();
									},
									failure : function(response) {
										Ext.MessageBox.hide();
										Ext.Msg.alert('错误提示',
												response.responseText);
									},
									scope : this
								});

					}
				})