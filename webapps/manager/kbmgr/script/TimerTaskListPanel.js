TimerTaskListPanel = function(_cfg) {
	var self = this;
	var _pageSize = 20;

	this.getPageSize = _pageSize;
	var _store = new Ext.data.Store({
		  proxy : new Ext.data.HttpProxy({
			    url : 'timer-task-list!list.action'
		    }),
		  reader : new Ext.data.JsonReader({
			    idProperty : 'id',
			    root : 'data',
			    fields : [{
				      name : 'id',
				      type : 'string'
			      }, {
				      name : 'taskName',
				      type : 'string'
			      }, {
				      name : 'taskTimeStr',
				      type : 'string'
			      }, {
				      name : 'moduleName',
				      type : 'string'
			      }, {
				      name : 'createUser',
				      type : 'string'
			      }, {
				      name : 'createTimeStr',
				      type : 'string'
			      }, {
				      name : 'ex',
				      type : 'string'
			      }, {
				      name : 'taskParams',
				      type : 'string'
			      }, {
				      name : 'state',
				      type : 'int'
			      }]
		    }),
		  writer : new Ext.data.JsonWriter()
	  });
	var taskName = new Ext.form.TextField({
		  fieldLabel : "任务名称",
		  name : "taskName",
		  allowBlank : true,
		  anchor : '96%'
	  });

	var rowNumber;
	var ds = _store;

	var myExpander = new Ext.grid.RowExpander({
		  tpl : new Ext.Template('<p style="color:#999;margin:2px 0px 2px 48px;">{taskParams}</p>')
	  });

	var myColumnModel = new Ext.grid.ColumnModel([rowNumber = new Ext.grid.RowNumberer(), myExpander, {
		  header : 'id',
		  dataIndex : 'id',
		  hidden : true,
		  sortable : false
	  }, {
		  header : '任务名称',
		  dataIndex : 'taskName',
		  hidden : false,
		  sortable : false
	  }, {
		  header : '任务时间',
		  dataIndex : 'taskTimeStr',
		  sorttable : false
	  }, {
		  header : '模块名称',
		  dataIndex : 'moduleName',
		  sorttable : false
	  }, {
		  header : '创建人',
		  dataIndex : 'createUser',
		  width : 50
	  }, {
		  header : '创建时间',
		  dataIndex : 'createTimeStr',
		  width : 60
	  }, {
		  header : '状态',
		  dataIndex : 'state',
		  width : 50,
		  renderer : function(val) {
			  return 1 == val ? "执行结束" : "未执行";
		  }
	  }]);

	myColumnModel.defaultSortable = true;
	this.taskName = taskName;

	var actionTbar = new Ext.Toolbar({
		  items : ['任务名称:', taskName, {
			    text : '查询',
			    iconCls : 'icon-wordclass-search',
			    handler : function() {
				    var taskName = self.taskName.getValue();
				    self.loadData(taskName);
			    }
		    }]
	  });
	_store.on('beforeload', function() {
		  var taskName = self.taskName.getValue();
		  Ext.apply(this.baseParams, {
			    taskName : taskName
		    });
		  scope : self
	  });

	var pagingBar = new Ext.PagingToolbar({
		  store : _store,
		  displayInfo : true,
		  pageSize : _pageSize,
		  prependButtons : true,
		  beforePageText : '第',
		  afterPageText : '页，共{0}页',
		  displayMsg : '第{0}到{1}条记录，共{2}条',
		  emptyMsg : "没有记录"
	  })
	var cfg = {
		region : 'center',
		border : false,
		frame : false,
		store : _store,
		loadMask : true,
		colModel : myColumnModel,
		tbar : actionTbar,
		plugins : [myExpander],
		bbar : pagingBar,
		viewConfig : {
			forceFit : true
		}
	}
	TimerTaskListPanel.superclass.constructor.call(this, Ext.applyIf(_cfg || {}, cfg));

}
Ext.extend(TimerTaskListPanel, Ext.grid.GridPanel, {
	  loadData : function(taskName) {
		  this.getStore().currentPage = 1;
		  this.getStore().load({
			    params : {
				    'taskName' : taskName,
				    start : 0,
				    limit : 20

			    }
		    });
	  }
  })