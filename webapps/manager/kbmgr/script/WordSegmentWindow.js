WordSegmentWindow = function(cfg) {

	var me = this;
	var toolFormPanel = this.toolFormPanel = new Ext.FormPanel({
				frame : false,
				border : false,
				bodyStyle : 'padding : 3px 10px; background-color:' + sys_bgcolor,
				layout : 'form',
				autoHeight : true,
				items : [{
							layout : 'column',
							border : 0,
							frame : true,
							bodyStyle : 'padding:5px 0 5px 5px',
							items : [new Ext.form.TextArea({
												columnWidth : .35,
												itemId : 'inputContent1',
												emptyText : '请输入句子'
											}), new Ext.form.TextArea({
												columnWidth : .5,
												style : "margin-left:5px;",
												itemId : 'outputContent1',
												emptyText : '显示查询分词结果',
												readOnly : true
											}), {
										columnWidth : .15,
										style : "margin-left:5px;",
										text : '分词',
										height : 62,
										xtype : 'button',
										handler : function() {
											me.searchSegment();
										}
									}]
						}, {
							layout : 'column',
							border : 0,
							frame : true,
							bodyStyle : 'padding:5px 0 5px 5px',
							items : [new Ext.form.TextField({
												columnWidth : .35,
												itemId : 'inputContent2',
												emptyText : '请输入词语'
											}), new Ext.form.TextField({
												columnWidth : .35,
												style : "margin-left:5px;",
												itemId : 'outputContent2',
												emptyText : '显示查询结果',
												readOnly : true
											}), {
										columnWidth : .2,
										style : "margin-left:5px;",
										text : '分词引擎查询',
										xtype : 'button',
										iconCls : 'icon-search',
										handler : function() {
											me.searchSegWord();
										}
									}, {
//										columnWidth : .1,
//										itemId : 'deleteWord',
//										style : "margin-left:5px;",
//										text : '删除',
//										xtype : 'button',
//										hidden : false,
//										disabled : true,
//										iconCls : 'icon-delete',
//										handler : function() {
//											me.removeSegWord();
//										}
//									}, {
										columnWidth : .1,
										itemId : 'addWord',
										style : "margin-left:5px;",
										text : '新增',
										xtype : 'button',
										hidden : true,
										iconCls : 'icon-add',
										handler : function() {
											me.addSegWord();
										}
									}]
						}, {
							layout : 'column',
							border : 0,
							frame : true,
							bodyStyle : 'padding:5px 0 5px 5px',
							items : [new Ext.form.TextField({
												columnWidth : .35,
												itemId : 'inputContent3',
												emptyText : '请输入词语'
											}), new Ext.form.TextField({
												columnWidth : .35,
												style : "margin-left:5px;",
												itemId : 'outputContent3',
												emptyText : '显示查询结果',
												readOnly : true
											}), {
										columnWidth : .15,
										style : "margin-left:5px;",
										text : '互信息',
										xtype : 'button',
										iconCls : 'icon-search',
										handler : function() {
											me.searchVector();
										}
									}, {
										columnWidth : .15,
										style : "margin-left:5px;",
										text : '自定义',
										xtype : 'button',
										iconCls : 'icon-defined',
										handler : function() {
											me.defined();
										}
									}]
						}]
			});

	var _cfg = {
		width : 600,
		title : '词类工具',
		iconCls : 'icon-ontology-object',
		closable : true,
		closeAction : 'hide',
		resizable : false,
		draggable : true,
		animCollapse : true,
		constrainHeader : true,
		layout : 'fit',
		items : toolFormPanel
	}

	WordSegmentWindow.superclass.constructor.call(this, Ext.applyIf(_cfg || {}, cfg));

}
Ext.extend(WordSegmentWindow, Ext.Window, {
			searchSegment : function() {
				var me = this;
				var _form = me.toolFormPanel.getForm();
				var content = _form.items.get(0).getValue();
				if (content.trim()) {
					Ext.Ajax.request({
								url : 'wordclass!searchSegment.action',
								params : {
									content : content.trim()
								},
								success : function(form, action) {
									var obj = Ext.util.JSON.decode(form.responseText);
									_form.items.get(1).setValue(obj.message);
								},
								failure : function(response) {
									Ext.Msg.alert("错误", response.responseText);
								}
							});
				} else {
					Ext.Msg.alert('提示', '输入内容不能为空');
				}
			},
			searchSegWord : function() {
				var me = this;
				var _form = me.toolFormPanel.getForm();
				var word = _form.items.get(2).getValue();
				if (word) {
					Ext.Ajax.request({
								url : 'wordclass!searchSegWord.action',
								params : {
									word : word.trim()
								},
								success : function(response) {
									var obj = Ext.util.JSON.decode(response.responseText);
									if (obj) {
										_form.items.get(3).setValue(obj);
//										me.toolFormPanel.query('#deleteWord')[0].setDisabled(false);
//										me.toolFormPanel.query('#deleteWord')[0].setVisible(true);
//										me.toolFormPanel.query('#addWord')[0].setVisible(false);
									} else {
										_form.items.get(3).setValue('未找到结果');
//										me.toolFormPanel.query('#deleteWord')[0].setDisabled(true);
//										me.toolFormPanel.query('#deleteWord')[0].setVisible(false);
//										me.toolFormPanel.query('#addWord')[0].setVisible(true);
									}
								},
								failure : function(response) {
									Ext.Msg.alert("错误", response.responseText);
								}
							});
				} else {
					Ext.Msg.alert('提示', '输入内容不能为空');
				}
			},
			searchVector : function() {
				var me = this;
				var _form = me.toolFormPanel.getForm();
				var word = _form.items.get(4).getValue();
				if (word) {
					Ext.Ajax.request({
								url : 'wordclass!searchVector.action',
								params : {
									word : word.trim()
								},
								success : function(form, action) {
									var obj = Ext.util.JSON.decode(form.responseText);
									if (obj.message) {
										_form.items.get(5).setValue(obj.message);
									}
								},
								failure : function(response) {
									Ext.Msg.alert("错误", response.responseText);
								}
							});
				} else {
					Ext.Msg.alert('提示', '查询内容不能为空');
				}
			},
			defined : function() {
				var me = this;
				var _form = me.toolFormPanel.getForm();
				Ext.Ajax.request({
							url : 'wordclass!defined.action',
							success : function(form, action) {
								var obj = Ext.util.JSON.decode(form.responseText);
								if (obj.message) {
									_form.items.get(5).setValue(obj.message);
								}
							},
							failure : function(response) {
								Ext.Msg.alert("错误", response.responseText);
							}
						});
			}
		});
