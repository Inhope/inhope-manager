var isAudit = http_get('property!get.action?key=kbmgr.wordclass.enableAudit');
isAudit = isAudit ? ("true" == isAudit ? true : false) : false;

WordclassPanel = function(_cfg) {
	this.mask;
	this.ispub;
	this.categoryType;
	var self = this;
	this.data = {};
	this.extraAttrs = {};
	var _pageSize = 20;
	this.getPageSize = function() {
		return _pageSize;
	}
	this.SYNO_DISPLAY_LIMIT = 50;
	this.wordclassId;

	var _store = new Ext.data.Store({
		  proxy : new Ext.data.HttpProxy({
			    url : 'wordclass!list.action'
		    }),
		  reader : new Ext.data.JsonReader({
			    idProperty : 'id',
			    root : 'data',
			    fields : [{
				      name : 'id',
				      type : 'string'
			      }, {
				      name : 'name',
				      type : 'string'
			      }, {
				      name : 'orderName',
				      type : 'string'
			      }, {
				      name : 'path',
				      type : 'string'
			      }, {
				      name : 'categoryid',
				      type : 'string'
			      }, {
				      name : 'type',
				      type : 'int'
			      }, {
				      name : 'state',
				      type : 'int'
			      }, {
				      name : 'opType',
				      type : 'int'
			      }, {
				      name : 'ispub'
			      }, {
				      name : 'synoWords',
				      type : 'object',
				      convert : function(v, rec) {
					      if (v && v.length > 0) {
						      var synos = [];
						      for (var i = 0; i < v.length; i++) {
							      var _color = (i % 2 == 0 ? 'blue' : 'red');
							      synos.push('<font color="' + _color + '">' + v[i].name + '</font>');
						      }
						      if (v.length >= self.SYNO_DISPLAY_LIMIT)
							      synos.push(' ...');
						      return synos.join('， ');
					      }
				      }
			      }]
		    }),
		  writer : new Ext.data.JsonWriter()
	  });
	_store.baseParams['limit'] = _pageSize
	var pagingBar = new Ext.PagingToolbar({
		  store : _store,
		  displayInfo : true,
		  pageSize : _pageSize,
		  prependButtons : true,
		  beforePageText : '第',
		  afterPageText : '页，共{0}页',
		  displayMsg : '词类{2}个',
		  emptyMsg : "没有记录"
	  });
	pagingBar.add({
		  xtype : 'label',
		  ref : 'statLabel',
		  text : '暂时没有单词统计信息'
	  });
	_store.reader.readRecords = function(o) {
		if (o.attachment != null) {
			pagingBar.statLabel.setText(new Ext.XTemplate('(同义词{.}个)').apply(o.attachment));
		}
		return Ext.data.JsonReader.prototype.readRecords.call(_store.reader, o);
	}.dg(this);
	var defPubRenderer = function(v, meta, r) {
		if (r.get('ispub'))
			meta.attr = 'style=font-weight:bold;color:#15428B';
		return v;
	}
	var defAuditRenderer = function(v, meta, r) {
		if (1 == r.get('state')) {
			meta.attr = 'style=font-weight:bold;color:green';
		} else if (2 == r.get('state')) {
			meta.attr = 'style=font-weight:bold;color:orange';
		} else if (3 == r.get('state')) {
			meta.attr = 'style=font-weight:bold;color:red';
		}
		return v;
	}
	var cfg = {
		region : 'center',
		style : 'border-right: 1px solid ' + sys_bdcolor,
		stripeRows : true,
		columnLines : true,
		border : false,
		store : _store,
		loadMask : true,
		colModel : new Ext.grid.ColumnModel({
			  defaults : {
				  sortable : true
			  },
			  columns : [new Ext.grid.RowNumberer(), {
				    header : '词类名',
				    dataIndex : 'name',
				    width : !Dashboard.isLabs() ? 50 : 30,
				    renderer : function(v, meta, r) {
					    defAuditRenderer(v, meta, r);
					    return v;
				    }
			    }, {
				    header : '组别',
				    dataIndex : 'orderName',
				    width : 20,
				    hidden : !Dashboard.isLabs()
			    }, {
				    header : '同义词',
				    dataIndex : 'synoWords',
				    width : 100,
				    renderer : function(v, meta, r) {
					    defPubRenderer(v, meta, r);
					    return v;
				    }
			    }]
		  }),
		sm : new Ext.grid.RowSelectionModel(),
		tbar : Dashboard.u().filterAllowed([{
			  text : '新增词类',
			  authName : 'kb.word.C',
			  iconCls : 'icon-add',
			  handler : function() {
				  self.addWord();
			  }
		  }, '', {
			  text : '删除词类',
			  authName : 'kb.word.D',
			  xtype : 'splitbutton',
			  iconCls : 'icon-delete',
			  ref : 'deleteWordclass',
			  menu : {
				  items : {
					  text : '同步',
					  checked : isAudit ? false : true,
					  ref : '../../syncWC'
				  }
			  },
			  handler : function() {
				  self.deleteWords();
			  }
		  }, '', {
			  text : '删除同义词',
			  authName : 'kb.word.D',
			  xtype : 'splitbutton',
			  iconCls : 'icon-delete',
			  ref : 'deleteSynowords',
			  menu : {
				  items : {
					  text : '同步',
					  checked : isAudit ? false : true,
					  ref : '../../syncSyno'
				  }
			  },
			  handler : function() {
				  self.deleteSynoWords();
			  }
		  }, '', {
			  text : '刷新列表',
			  iconCls : 'icon-refresh',
			  handler : function() {
				  self.wordGrid.getStore().reload();
			  }
		  }, '-', '', '词长度:', {
			  id : 'search-word-symbol',
			  xtype : 'combo',
			  editable : false,
			  triggerAction : 'all',
			  valueField : 'id',
			  displayField : 'name',
			  mode : 'local',
			  value : '1',
			  width : 75,
			  store : new Ext.data.ArrayStore({
				    fields : ["id", "name"],
				    data : [['1', '大于'], ['2', '小于'], ['3', '大于等于'], ['4', '小于等于'], ['5', '等于']]
			    })
		  }, {
			  id : 'search-word-length',
			  xtype : 'numberfield',
			  hideTrigger : true,
			  allowNegative : false,
			  nanText : '请输入有效的整数',
			  emptyText : '词长度',
			  allowDecimals : false,
			  width : 45,
			  listeners : {
				  specialkey : function(field, e) {
					  if (e.getKey() == Ext.EventObject.ENTER) {
						  self.searchWordclass();
					  }
				  }
			  }
		  }]),
		bbar : pagingBar,
		viewConfig : {
			forceFit : true
		}
	}
	var _wordGrid = new Ext.grid.GridPanel(Ext.applyIf(_cfg || {}, cfg));
	this.wordGrid = _wordGrid;

	var _treeCombo = new TreeComboBox({
		  // id : 'wordTreeCombo',
		  fieldLabel : '父级词类',
		  emptyText : '请选择父级词类...',
		  editable : false,
		  anchor : '100%',
		  name : 'category',
		  allowBlank : false,
		  minHeight : 250,
		  root : {
			  id : '0',
			  text : 'root',
			  iconCls : 'icon-ontology-root'
		  },
		  loader : new Ext.tree.TreeLoader({
			    dataUrl : 'wordclass-category!list.action',
			    listeners : {
				    'beforeload' : function(th, n, cb) {
				    }
			    }
		    })
	  });
	this.treeCombo = _treeCombo;

	var _synoArea = new Ext.form.TextArea({
		  fieldLabel : '同义词',
		  name : "synos",
		  anchor : '100% 100% '
	  });
	var _pinyinArea = new Ext.form.TextArea({
		  fieldLabel : '对应拼音',
		  name : "pinyins",
		  anchor : '100% 100% '
	  });
	var _syncCheckbox = new Ext.form.Checkbox({
		  boxLabel : '同步',
		  checked : isAudit ? false : true,
		  name : 'sync',
		  flex : 5,
		  anchor : '100% 100% '
	  });
	var _bToolbar = new Ext.Toolbar({
		  height : '20px',
		  items : {
			  xtype : 'label',
			  ref : 'countSynoword',
			  text : ''
		  }
	  });
	var wordclassname = new Ext.form.TextField({
		  fieldLabel : '词类名称',
		  allowBlank : false,
		  name : 'name',
		  blankText : '',
		  invalidText : '',
		  anchor : '100%'
	  })
	var orderName = new Ext.form.TextField({
		  fieldLabel : '组别',
		  allowBlank : true,
		  name : 'orderName',
		  hidden : !Dashboard.isLabs(),
		  blankText : '',
		  invalidText : '',
		  anchor : '100%'
	  })
	this.wordProxy = new LocalProxy({});
	var wordEditStore = new Ext.data.Store({
		  proxy : self.wordProxy,
		  reader : new Ext.data.JsonReader({
			    idProperty : 'wordId',
			    root : 'data',
			    fields : [{
				      name : 'wordId',
				      type : 'string'
			      }, {
				      name : 'name',
				      type : 'string'
			      }, {
				      name : 'mark',
				      type : 'string'
			      }, {
				      name : 'state',
				      type : 'int'
			      }]
		    }),
		  writer : new Ext.data.JsonWriter(),
		  listeners : {
			  'load' : function(store, records) {
				  self.formPanel.bToolbar.countSynoword.setText('同义词共 ' + records.length + ' 条');
				  self.wordEditGrid.batchAdd(18);
			  }
		  }
	  });
	var wordEditGrid = this.wordEditGrid = new ExcelLikeGrid({
		  autoExpandColumn : 'name',
		  // anchor : '100% -60',
		  border : true,
		  clicksToEdit : 1,
		  frame : false,
		  columnLines : true,
		  selModel : new Ext.grid.RowSelectionModel(),
		  pasteable : true,
		  columns : [new Ext.grid.RowNumberer(), {
			    id : "name",
			    header : '同义词',
			    dataIndex : 'name',
			    width : 120,
			    editor : new Ext.form.TextField(),
			    replaceable : true,
			    renderer : function(v, meta, r, rIdx) {
				    defAuditRenderer(v, meta, r);
				    return v;
			    }
		    }, {
			    header : '备注',
			    dataIndex : 'mark',
			    width : 120,
			    editor : new Ext.form.TextField(),
			    replaceable : true
		    }, new DeleteButtoner()],
		  store : wordEditStore,
		  listeners : {
			  'cellclick' : function(grid, rowIndex, columnIndex, e) {
				  var record = grid.getStore().getAt(rowIndex);
				  if (record.get('state') > 0)
					  return false;
			  }
		  }
	  });

	wordEditGrid.on("scrollbottomclick", function(e) {
		  wordEditGrid.batchAdd(20);
	  });

	wordEditGrid.on("shiftleft", function(e) {
		  var g = wordEditGrid;
		  var col = g.findMostRightEditCol();
		  var r = g.getSelectionModel().getSelected();
		  var row = g.getStore().indexOf(r);
		  this.stopEditing();
		  g.startEditing(row, col);
	  });
	var _formPanel = new Ext.form.FormPanel({
		  frame : false,
		  border : false,
		  region : 'east',
		  width : 320,
		  split : true,
		  collapseMode : 'mini',
		  labelWidth : 75,
		  anchor : '100% 100%',
		  waitMsgTarget : true,
		  bodyStyle : 'padding : 5px; background-color:' + sys_bgcolor,
		  style : 'border-left: 1px solid ' + sys_bdcolor,
		  items : [new Ext.form.Hidden({
			      name : 'id'
		      }), new Ext.form.Hidden({
			      name : 'state'
		      }), new Ext.form.Hidden({
			      name : 'opType'
		      }), _treeCombo, wordclassname, new Ext.form.ComboBox({
			      triggerAction : 'all',
			      fieldLabel : '词类型别',
			      anchor : '100%',
			      allowBlank : false,
			      mode : 'local',
			      hiddenName : 'type',
			      hidden : true,
			      name : 'type',
			      editable : false,
			      value : 1,
			      store : new Ext.data.ArrayStore({
				        fields : ['vfield', 'displayText'],
				        data : [[1, '词类型别1'], [2, '词类型别2']]
			        }),
			      valueField : 'vfield',
			      displayField : 'displayText'
		      }), orderName, isAudit ? {
			    layout : 'fit',
			    anchor : '100% -60',
			    border : false,
			    items : wordEditGrid
		    } : {
			    layout : 'hbox',
			    layoutConfig : {
				    align : 'stretch'
			    },
			    anchor : '100% -80',
			    border : false,
			    items : [{
				      layout : 'form',
				      labelAlign : 'top',
				      flex : 5,
				      border : false,
				      bodyStyle : 'background-color:#d3e1f1;padding-right:2px;',
				      items : _synoArea
			      } /*
											 * , { layout : 'form', labelAlign : 'top', flex : 5, border : false,
											 * bodyStyle : 'background-color:#d3e1f1;padding-left:2px;', items :
											 * _pinyinArea }
											 */]
		    }],
		  tbar : [_syncCheckbox, '-', {
			    text : '保存',
			    iconCls : 'icon-table-save',
			    ref : 'save',
			    xtype : 'splitbutton',
			    menu : {
				    items : [{
					      text : '忽略重复词',
					      checked : false,
					      name : 'ignoreRepeat',
					      ref : '../ignoreRepeat'
				      }]
			    },
			    handler : function() {
				    self.saveWord(this);
			    }
		    }, {
			    text : '增量保存',
			    tooltip : '新增同义词并修改词类信息',
			    iconCls : 'icon-wordclass-add',
			    ref : 'addSynoword',
			    handler : function() {
				    self.addSynoword(this);
			    }
		    } /*
								 * , '-', { text : '生成纠错拼音', iconCls : 'icon-correct-pinyin', handler :
								 * function() { self.generatePinyin(this); } }
								 */],
		  bbar : _bToolbar
	  });
	_formPanel.synoArea = _synoArea;
	_formPanel.pinyinArea = _pinyinArea;
	_formPanel.syncCheckbox = _syncCheckbox;
	_formPanel.bToolbar = _bToolbar;
	_formPanel.wordclassname = wordclassname;
	this.formPanel = _formPanel;

	var config = {
		id : 'wordclassPanel',
		layout : 'border',
		border : false,
		items : [_wordGrid, _formPanel]
	};
	WordclassPanel.superclass.constructor.call(this, config);

	_wordGrid.getSelectionModel().on('rowselect', function(sm, rowIdx, rec) {
		if (typeof rec.get("synoWords") !== 'undefined') {
			var str = rec.get("synoWords").replace(new RegExp('<font color="blue">', "g"), "").replace(new RegExp('<font color="red">', "g"), "").replace(new RegExp('</font>', "g"), "")
			  .split("，"); // 取的选择列中的同义词
			if (str[str.length - 1].trim() == "..." & str.length == 51) {
				var _syno = this.tbarEx.get('search-syno').getValue().trim();
				if (_syno) {
					if (this.wordGrid.getTopToolbar().deleteSynowords)
						this.wordGrid.getTopToolbar().deleteSynowords.disable();
				} else {
					if (this.wordGrid.getTopToolbar().deleteWordclass)
						this.wordGrid.getTopToolbar().deleteWordclass.enable();
					if (this.wordGrid.getTopToolbar().deleteSynowords)
						this.wordGrid.getTopToolbar().deleteSynowords.enable();
				}
			}
		}
		this.modifyWord();
	}, this);
	this.tbarEx = new Ext.Toolbar({
		  items : [{
			    text : '搜索',
			    ref : 'searchBtn',
			    xtype : 'splitbutton',
			    iconCls : 'icon-search',
			    handler : function() {
				    self.searchWord();
			    },
			    menu : {
				    items : [{
					      text : '搜索全部词类',
					      checked : true,
					      name : 'all',
					      ref : '../all'
				      }, {
					      text : '显示全部同义词',
					      checked : true,
					      name : 'allSynowrod',
					      ref : '../allSynowrod'
				      }, {
					      text : '精确搜索',
					      checked : true,
					      name : 'preciseSearch',
					      ref : '../preciseSearch'
				      }, {
					      text : '大小写忽略',
					      checked : false,
					      ref : '../ignoreCase',
					      name : 'ignoreCase',
					      flex : 5,
					      anchor : '100% 100% '
				      }]
			    }
		    }, '-', '词类名:', {
			    id : 'search-name',
			    xtype : 'textfield',
			    emptyText : '输入词类名搜索...',
			    listeners : {
				    specialkey : function(field, e) {
					    if (e.getKey() == Ext.EventObject.ENTER) {
						    self.searchWord();
					    }
				    }
			    }
		    }, '同义词:', {
			    id : 'search-syno',
			    xtype : 'textfield',
			    emptyText : '输入同义词搜索...',
			    listeners : {
				    specialkey : function(field, e) {
					    if (e.getKey() == Ext.EventObject.ENTER) {
						    self.searchWord();
					    }
				    }
			    }
		    }, '编辑时间:从', {
			    id : 'search-bdate',
			    xtype : 'datefield',
			    emptyText : '开始时间',
			    format : 'Y-m-d',
			    listeners : {
				    specialkey : function(field, e) {
					    if (e.getKey() == Ext.EventObject.ENTER) {
						    self.searchWord();
					    }
				    }
			    }
		    }, '到', {
			    id : 'search-edate',
			    xtype : 'datefield',
			    emptyText : '结束时间',
			    format : 'Y-m-d',
			    listeners : {
				    specialkey : function(field, e) {
					    if (e.getKey() == Ext.EventObject.ENTER) {
						    self.searchWord();
					    }
				    }
			    }
		    }]
	  });
	_wordGrid.addListener('render', function() {
		  self.tbarEx.render(this.tbar);
	  }, _wordGrid);
	_wordGrid.getStore().on('load', function() {
		  if (self.noAutoSelect) {
			  delete self.noAutoSelect
			  return;
		  }
		  // var selected = this.getSelectionModel().selectPrevious();
		  // if (!selected)
		  // this.getSelectionModel().selectRow(0);
		  var cnode = self.getCategory();
		  var n = cnode;
		  while (n && n.id.length > 1)
			  n = n.parentNode
		  if (!this.getSelectionModel().getSelected()) {
			  self.formPanel.getForm().reset();
			  self.treeCombo.setValueEx({
				    id : cnode.id,
				    path : cnode.getPath('text').replace('/root', '')
			    });
			  self.formPanel.getEl().mask();

			  if (isAudit) {
				  self.wordProxy.setData({
					    data : {}
				    });
				  self.wordEditGrid.getStore().load();
			  }
		  }
		  if (!self.treeCombo.tree) {
			  self.treeCombo.root = {
				  id : n.id,
				  text : 'root',
				  iconCls : 'icon-ontology-root',
				  type : n.attributes.type,
				  ispub : n.attributes.ispub
			  }
		  } else {
			  self.treeCombo.tree.getRootNode().id = n.id;
		  }
		  self.formPanel.getForm().loadRecord(new Ext.data.Record({
			    ispub : cnode.attributes.ispub
		    }));
	  }, _wordGrid);
	this.toTrim = function(str) {
		return str.replace(/(^\s*)|(\s*$)/g, '');
	}
};

Ext.extend(WordclassPanel, Ext.Panel, {
	  loadWords : function(categoryId, ispub, type) {
		  var self = this;
		  if (Dashboard.u().allow("kb.word.C") || Dashboard.u().allow("kb.word.M")) {
			  this.formPanel.getTopToolbar().save.show();
			  this.formPanel.getTopToolbar().addSynoword.show();
		  } else {
			  this.formPanel.getTopToolbar().save.hide();
			  this.formPanel.getTopToolbar().addSynoword.hide();
		  }
		  this.ispub = ispub;
		  this.categoryType = type;
		  var store = this.wordGrid.getStore();
		  for (var key in store.baseParams) {
			  if (key && key.indexOf('queryParam.') != -1) {
				  delete store.baseParams[key];
			  } else {
				  this.mask = false;
				  if (this.wordGrid.getTopToolbar().deleteWordclass)
					  this.wordGrid.getTopToolbar().deleteWordclass.enable();
				  if (this.wordGrid.getTopToolbar().deleteSynowords)
					  this.wordGrid.getTopToolbar().deleteSynowords.enable();
			  }
		  }

		  if (Dashboard.wordclassPresetData) {
			  store.baseParams['categoryId'] = Dashboard.wordclassPresetData.categoryId;
			  store.baseParams['ispub'] = Dashboard.wordclassPresetData.ispub;
			  store.baseParams['categoryType'] = Dashboard.wordclassPresetData.classType;
			  store.setBaseParam('queryParam.name', Dashboard.wordclassPresetData.wordclassName);
			  store.setBaseParam('queryParam.preciseSearch', true);
		  } else {
			  store.baseParams['categoryId'] = categoryId;
			  store.baseParams['ispub'] = ispub;
			  store.baseParams['categoryType'] = type;
		  }
		  store.load({
			    params : {
				    start : 0,
				    limit : this.getPageSize()
			    },
			    callback : function() {
				    Dashboard.wordclassPresetData = null;
				    self.addWord();
			    }
		    });
		  this._setPubDataUrl();
	  },
	  endEdit : function() {
		  this.formPanel.getForm().reset();
	  },
	  saveWord : function(btn, forceSave) {
		  if (this.treeCombo.getValue().length == 1) {
			  Ext.Msg.alert('错误提示', '根类别下不能直接新增词类');
			  return false;
		  }
		  btn.disable();
		  var f = this.formPanel;
		  if (!f.getForm().isValid()) {
			  Ext.Msg.alert('提示', '您尚未正确完整的输入表单内容，请检查！');
			  btn.enable();
			  return false;
		  }
		  this.data = f.getForm().getValues();
		  this.data.category = {};
		  this.data.category.categoryId = this.treeCombo.getValue();
		  var self = this;
		  if (isAudit) {
			  var wordStore = this.wordEditGrid.getStore();
			  var wordRecords = [];
			  var records = wordStore.getRange();
			  var isSynosEmpty = 0;
			  Ext.each(records, function(word) {
				    if (word.data.name)
					    isSynosEmpty++;
			    });
			  if (isSynosEmpty < 1) {
				  Ext.Msg.alert('错误提示', '同义词不能为空');
				  btn.enable();
				  return;
			  }
			  for (var i = 0; i < records.length; i++) {
				  var data = records[i].data;
				  if (!data.name)
					  continue;
				  if (!btn.ignoreRepeat.checked) {
					  for (var j = records.length - 1; j > -1; j--) {
						  if (i == j) {
							  continue;
						  }
						  var word = records[j].data;
						  if (!word.name)
							  continue;
						  if (self.toTrim(data.name) === self.toTrim(word.name) && self.toTrim(data.name) != '' && self.toTrim(word.name) != '') {
							  Ext.Msg.alert('重复提示', '同义词&nbsp;' + self.toTrim(data.name) + '&nbsp;有重复!');
							  btn.enable();
							  return;
						  }
						  if (self.toTrim(data.name).toUpperCase() === self.toTrim(word.name).toUpperCase() && self.toTrim(data.name) != '' && self.toTrim(word.name) != '') {
							  Ext.Msg.alert('重复提示', '同义词&nbsp;' + self.toTrim(data.name) + '&nbsp;大小写有重复!');
							  btn.enable();
							  return;
						  }
					  }
				  }
				  wordRecords.push(data);
			  };
			  this.data.synoWords = wordRecords;
		  } else {
			  var _synos = f.limitExceed ? '' : this.data.synos;
			  if (_synos == '' || _synos.replace(/(^\s*)|(\s*$)/g, "").length == 0) {
				  if (!f.limitExceed) {
					  Ext.Msg.alert('错误提示', '同义词不能为空');
					  btn.enable();
					  return;
				  }
			  }
			  var _pinyins = this.data.pinyins;
			  delete this.data.synos;
			  delete this.data.pinyins;
		  }
		  _synos = _synos ? _synos.replace(/\r\n/g, "\n") : _synos;
		  if (!btn.ignoreRepeat.checked) {
			  var str = Ext.encode(_synos);
			  var arr = str.substring(1, str.length - 1).split("\\n")
			  if (arr != null) {
				  for (var i = 0; i < arr.length; i++) {
					  for (var j = arr.length - 1; j >= 0; j--) {
						  if (i == j) {
							  continue;
						  }
						  if (self.toTrim(arr[i]) === self.toTrim(arr[j]) && self.toTrim(arr[i]) != '' && self.toTrim(arr[j]) != '') {
							  Ext.Msg.alert('重复提示', '同义词&nbsp;' + self.toTrim(arr[i]) + '&nbsp;有重复!');
							  btn.enable();
							  return;
						  }
						  if (self.toTrim(arr[i]).toUpperCase() === self.toTrim(arr[j]).toUpperCase() && self.toTrim(arr[i]) != '' && self.toTrim(arr[j]) != '') {
							  Ext.Msg.alert('重复提示', '同义词&nbsp;' + self.toTrim(arr[i]) + '&nbsp;大小写有重复!');
							  btn.enable();
							  return;
						  }
					  }
				  }
			  }
		  }
		  if (this.treeCombo.lastSelectedNode && this.ispub && this.ispub != this.treeCombo.lastSelectedNode.attributes.ispub) {
			  Ext.Msg.alert('提示', '不能改变词类所在的数据库:' + this.ispub + '->' + this.treeCombo.lastSelectedNode.attributes.ispub);
			  return;
		  }
		  if (this.treeCombo.lastSelectedNode)
			  this.ispub = this.treeCombo.lastSelectedNode.attributes.ispub;
		  Ext.Ajax.request({
			    url : 'wordclass!save.action',
			    params : {
				    'ispub' : this.ispub,
				    'data' : Ext.encode(ux.util.resetEmptyString(self.data)),
				    'synos' : _synos,
				    'keepPrevWords' : f.limitExceed ? 'true' : 'false',
				    'pinyins' : _pinyins,
				    'sync' : f.syncCheckbox.getValue(),
				    'categoryId' : self.data.category.categoryId,
				    'forceSave' : forceSave ? forceSave : false,
				    'ignoreRepeatWord' : btn.ignoreRepeat.checked,
				    'isAudit' : isAudit
			    },
			    success : function(resp) {
				    var isvali = Ext.util.JSON.decode(resp.responseText).message;
				    if (typeof isvali != 'undefined') {
					    if (isvali.length > 3) {
						    Ext.Msg.alert('错误信息', isvali);
						    btn.enable();
						    return;
					    }
				    }
				    btn.enable();
				    // self.endEdit();
				    Dashboard.setAlert('保存成功！')
				    if (this.saveMode)
					    this.addWord(false)
				    else {
					    this.noAutoSelect = true
					    self.wordGrid.getStore().reload({
						      callback : function() {
							      var i = self.wordGrid.getStore().find('id', self.data.id);
							      if (i != -1) {
								      self.wordGrid.getSelectionModel().selectRow(i);
							      }
						      }
					      });
				    }
			    },
			    failure : function(resp) {
				    if (resp.responseText.indexOf('qtip:') == 0) {
					    Ext.Msg.confirm('确定框', resp.responseText.replace('qtip:', ''), function(sel) {
						      if (sel == 'yes') {
							      self.saveWord(btn, true);
						      }
					      });
				    } else {
					    Ext.Msg.alert('错误', resp.responseText);
					    btn.enable();
				    }
			    },
			    scope : this
		    });
	  },
	  addSynoword : function(btn) {
		  btn.disable();
		  var f = this.formPanel;
		  if (!f.getForm().isValid()) {
			  Ext.Msg.alert('提示', '您尚未正确完整的输入表单内容，请检查！');
			  btn.enable();
			  return false;
		  }
		  this.data = f.getForm().getValues();
		  this.data.category = {};
		  this.data.category.categoryId = this.treeCombo.getValue();
		  var _synos = this.data.synos;
		  // if (_synos == '' || _synos.replace(/(^\s*)|(\s*$)/g, "").length == 0) {
		  // Ext.Msg.alert('错误提示', '新增的同义词为空');
		  // btn.enable();
		  // return;
		  // }
		  delete this.data.synos;
		  var str = Ext.encode(_synos);
		  var arr = str.substring(1, str.length - 1).split("\\n")
		  var self = this;
		  if (arr != null) {
			  for (var i = 0; i < arr.length; i++) {
				  for (var j = arr.length - 1; j >= 0; j--) {
					  if (i == j) {
						  continue;
					  }
					  if (self.toTrim(arr[i]) === self.toTrim(arr[j]) && self.toTrim(arr[i]) != '' && self.toTrim(arr[j]) != '') {
						  Ext.Msg.alert('重复提示', '同义词&nbsp;' + self.toTrim(arr[i]) + '&nbsp;有重复!');
						  btn.enable();
						  return;
					  }
				  }
			  }
		  }
		  Ext.Ajax.request({
			    url : 'wordclass!addSynowords.action',
			    params : {
				    'ispub' : this.ispub,
				    // 'wordclassid' : this.data.id,
				    'synos' : _synos,
				    // 'pinyins' : _pinyins,
				    'sync' : f.syncCheckbox.getValue(),
				    'categoryId' : self.data.category.categoryId,
				    'data' : Ext.encode(self.data)
			    },
			    success : function(resp) {
				    var isvali = Ext.util.JSON.decode(resp.responseText).message;
				    if (typeof isvali != 'undefined') {
					    if (isvali.length > 3) {
						    Ext.Msg.alert('错误信息', isvali);
						    btn.enable();
						    return;
					    }
				    }
				    btn.enable();
				    // self.endEdit();
				    Dashboard.setAlert('增量保存成功！')
				    if (this.saveMode)
					    this.addSynoword()
				    else {
					    this.noAutoSelect = true
					    self.wordGrid.getStore().reload({
						      callback : function() {
							      var i = self.wordGrid.getStore().find('id', self.data.id);
							      if (i != -1) {
								      self.wordGrid.getSelectionModel().selectRow(i);
							      }
						      }
					      });
				    }
			    },
			    failure : function(resp) {
				    Ext.Msg.alert('错误', resp.responseText);
				    btn.enable();
				    // self.endEdit();
			    },
			    scope : this
		    });
	  },
	  generatePinyin : function() {
		  var synos = this.formPanel.getForm().getValues().synos;
		  Ext.Ajax.request({
			    url : 'wordclass!genPinyin.action',
			    params : {
				    'synos' : synos
			    },
			    success : function(resp) {
				    this.formPanel.pinyinArea.setValue(Ext.decode(resp.responseText));
			    },
			    failure : function(resp) {
				    alert("error:" + response.responseText);
			    },
			    scope : this
		    });
	  },
	  getCategory : function() {
		  return this.navPanel.getSelectionModel().getSelectedNode();
	  },
	  _setPubDataUrl : function() {
		  var store = this.wordGrid.getStore();
		  var thisdataurl = (store.baseParams['categoryId'] || '') + '_' + (store.baseParams['ispub'] || '') + '_' + (store.baseParams['type'] || '')
		  console.log(thisdataurl, '||', this.lastDataUrl)
		  if (!this.treeComboExpandL) {
			  this.treeCombo.on('expand', this.treeComboExpandL = function() {
				    if (thisdataurl != this.lastDataUrl) {
					    this.treeCombo.reload();
				    }
			    }, this)
		  }
		  this.lastDataUrl = thisdataurl;
		  if (this.ispub) {
			  if (this.treeCombo.loader.dataUrl.indexOf('ispub') == -1)
				  this.treeCombo.loader.dataUrl = this.treeCombo.loader.dataUrl.replace(/\?type=.*/ig, '') + '?ispub=' + this.ispub + "&type=" + this.categoryType;
			  else
				  this.treeCombo.loader.dataUrl = this.treeCombo.loader.dataUrl.replace(/\?type=.*/ig, '').replace(/\&type=.*/ig, '') + "&type=" + this.categoryType;
		  } else
			  this.treeCombo.loader.dataUrl = this.treeCombo.loader.dataUrl.replace(/\?ispub=.*/ig, '').replace(/\?type=.*/ig, '') + "?type=" + this.categoryType;
		  this.treeCombo.reload();
	  },
	  addWord : function(toresetTreeComb) {
		  this.formPanel.limitExceed = false;
		  this.formPanel.getTopToolbar().save.enable();
		  this.formPanel.getTopToolbar().addSynoword.disable();
		  this.formPanel.wordclassname.el.dom.readOnly = false;
		  this.treeCombo.enable();
		  this.saveMode = true
		  var f = this.formPanel;
		  if (!f.isVisible()) {
			  f.toggleCollapse(true);
		  }
		  f.getEl().unmask();
		  if (toresetTreeComb !== false)
			  toresetTreeComb = !this.treeCombo.getValue() || this.ispub && this.treeCombo.loader.dataUrl.indexOf('ispub') == -1 || !this.ispub
			    && this.treeCombo.loader.dataUrl.indexOf('ispub') != -1;
		  var oldTreeCombreset = this.treeCombo.reset;
		  try {
			  if (!toresetTreeComb) {
				  this.treeCombo.reset = function() {
				  }
			  }
			  this.formPanel.getForm().reset();
		  } finally {
			  this.treeCombo.reset = oldTreeCombreset;
		  }
		  var cnode = this.getCategory();
		  if (toresetTreeComb)
			  this.treeCombo.setValueEx({
				    id : cnode.id,
				    path : cnode.getPath('text').replace('/root', '')
			    });
		  this._setPubDataUrl();
		  if (isAudit) {
			  this.loadWordclassWord(this.ispub, '', isAudit);
		  }
	  },
	  modifyWord : function() {
		  var self = this;
		  this.formPanel.limitExceed = false;
		  if (this.saveMode)
			  delete this.saveMode
		  var f = this.formPanel;
		  if (!f.isVisible()) {
			  f.toggleCollapse(true);
		  }
		  if (!this.mask) {
			  f.getEl().unmask();
		  }
		  f.getForm().reset();
		  var cnode = this.getCategory();
		  var rows = this.wordGrid.getSelectionModel().getSelections();
		  var rec = rows[0];
		  this.ispub = rec.get('ispub');
		  var node = {
			  'id' : rec.json.categoryid,
			  'path' : rec.json.path
		  };
		  this.treeCombo.setValue(node);
		  this.formPanel.find('name', 'name')[0].el.applyStyles(this.ispub ? "font-weight:bold;color:#15428B" : "font-weight:normal;color:black")
		  this._setPubDataUrl();
		  self.treeCombo.enable();
		  self.formPanel.getTopToolbar().save.enable();
		  self.formPanel.wordclassname.el.dom.readOnly = false;
		  if (isAudit) {
			  if (rec.get('opType') > 0) {
				  self.formPanel.wordclassname.el.dom.readOnly = (rec.get('opType') & 2) == 2;
				  self.treeCombo[(rec.get('opType') & 4) == 4 ? 'disable' : 'enable']();
			  }
			  if (rec.get('state') > 2) {
				  self.formPanel.wordclassname.el.dom.readOnly = true;
				  self.treeCombo.disable();
				  self.formPanel.getTopToolbar().save.disable();
			  }
			  this.formPanel.getTopToolbar().addSynoword.hide();
			  self.wordclassId = rec.get('id');
			  self.loadWordclassWord(rec.get('ispub') ? rec.get('ispub') : '', self.wordclassId, isAudit);
		  }
		  f.getForm().loadRecord(rec);
		  // if (isAudit)
		  // self.wordEditGrid.batchAdd(18);
		  if (rec.get('synoWords') && !isAudit) {
			  this.formPanel.getTopToolbar().save.enable();
			  this.formPanel.getTopToolbar().addSynoword.enable();
			  // this.formPanel.wordclassname.el.dom.readOnly = false;
			  this.treeCombo.enable();
			  var _synoArr = [];
			  var _pinyinArr = [];
			  var countSynoword = rec.json.synoWords.length;
			  // 个';
			  if (rec.json.synoWords.length < this.SYNO_DISPLAY_LIMIT) {
				  this.formPanel.getTopToolbar().save.enable();
				  this.formPanel.getTopToolbar().addSynoword.disable();
				  this.formPanel.bToolbar.countSynoword.setText('同义词共 ' + countSynoword + ' 条，当前显示 ' + countSynoword + ' 条');
				  for (var i = 0; i < rec.json.synoWords.length; i++) {
					  _synoArr.push(rec.json.synoWords[i].name);
					  _pinyinArr.push(rec.json.synoWords[i].pinyin);
				  }
				  this.formPanel.synoArea.setValue(_synoArr.join('\n'));
				  this.formPanel.pinyinArea.setValue(_pinyinArr.join('\n'));
			  } else {
				  var _self = this;
				  var _params = {
					  wordclassId : rec.get('id')
				  };
				  if (rec.get('ispub'))
					  _params.ispub = rec.get('ispub');
				  Ext.Ajax.request({
					    url : 'wordclass!findWordclassWords.action',
					    params : _params,
					    success : function(resp) {
						    var total = Ext.decode(resp.responseText).total;
						    var _words = Ext.decode(resp.responseText).data.result;
						    if (total < 5000) {
							    _self.formPanel.bToolbar.countSynoword.setText('同义词共 ' + total + ' 条,当前显示 ' + total + '条');
							    _self.formPanel.getTopToolbar().save.enable();
							    _self.formPanel.getTopToolbar().addSynoword.disable();
							    for (var i = 0; i < _words.length; i++) {
								    _synoArr.push(_words[i].name);
								    _pinyinArr.push(_words.pinyin);
							    }
							    _self.formPanel.synoArea.setValue(_synoArr.join('\n'));
							    _self.formPanel.pinyinArea.setValue(_pinyinArr.join('\n'));

						    } else {
							    _self.formPanel.bToolbar.countSynoword.setText("同义词共 " + total + " 条，当前显示 0 条");
							    // _self.formPanel.wordclassname.el.dom.readOnly
							    // = true;
							    // _self.treeCombo.disable();
							    _self.formPanel.getTopToolbar().save.disable();
							    _self.formPanel.getTopToolbar().addSynoword.enable();
							    _self.formPanel.limitExceed = true;
						    }
					    }
				    });
			  }
		  }
	  },
	  searchWord : function() {
		  var self = this;
		  this.mask = false;
		  self.wordGrid.getTopToolbar().deleteWordclass.enable();
		  self.wordGrid.getTopToolbar().deleteSynowords.enable();
		  var _name = this.tbarEx.get('search-name').getValue().trim();
		  var _syno = this.tbarEx.get('search-syno').getValue().trim();
		  var _bdate = this.tbarEx.get('search-bdate').getValue();
		  var _edate = this.tbarEx.get('search-edate').getValue();
		  var wordSymbol = Ext.getCmp('search-word-symbol').getValue();
		  var wordLength = Ext.getCmp('search-word-length').getValue();
		  var store = this.wordGrid.getStore();
		  for (var key in store.baseParams) {
			  if (key && key.indexOf('queryParam.') != -1)
				  delete store.baseParams[key];
		  }
		  if (_name) {
			  self.wordGrid.getTopToolbar().deleteSynowords.disable();
			  store.setBaseParam('queryParam.name', _name);
		  }
		  if (_syno) {
			  if (!this.tbarEx.searchBtn.allSynowrod.checked) {
				  self.formPanel.getEl().mask();
				  self.mask = true;
				  self.wordGrid.getTopToolbar().deleteSynowords.enable();
				  self.wordGrid.getTopToolbar().deleteWordclass.disable();
			  } else {
				  self.mask = false;
				  self.wordGrid.getTopToolbar().deleteWordclass.enable();
				  self.wordGrid.getTopToolbar().deleteSynowords.disable();
			  }
			  store.setBaseParam('queryParam.syno', _syno);
		  }
		  if (_bdate)
			  store.setBaseParam('queryParam.beginDate', _bdate);
		  if (_edate)
			  store.setBaseParam('queryParam.endDate', _edate);
		  if (this.tbarEx.searchBtn.all.checked)
			  store.setBaseParam('queryParam.all', true)
		  if (this.tbarEx.searchBtn.allSynowrod.checked)
			  store.setBaseParam('queryParam.allSynowrod', true)
		  if (this.tbarEx.searchBtn.preciseSearch.checked)
			  store.setBaseParam('queryParam.preciseSearch', true)
		  if (this.tbarEx.searchBtn.ignoreCase.checked)
			  store.setBaseParam('queryParam.ignoreCase', true)
		  if (wordSymbol)
			  store.setBaseParam('queryParam.wordSymbol', wordSymbol)
		  if (wordLength)
			  store.setBaseParam('queryParam.wordLength', wordLength)
		  store.load();
	  },
	  deleteWordFun : function(rows, self, forceDelete) {
		  var ids = [];
		  var isAuditing = false;
		  Ext.each(rows, function(_item) {
			    if (_item.data.state > 0) {
				    Ext.Msg.alert('警告', "\"" + _item.data.name + "\"正在审核中，不能做删除操作！");
				    isAuditing = true;
				    return false;
			    }
			    ids.push(_item.id)
		    })
		  if (isAuditing)
			  return;
		  Ext.Ajax.request({
			    url : 'wordclass!deleteByIds.action',
			    success : function(resp) {
				    Dashboard.setAlert('删除成功！')
				    self.wordGrid.getStore().reload();
			    },
			    failure : function(resp) {
				    if (resp.responseText.indexOf('qtip:') == 0) {
					    Ext.Msg.confirm('确定框', resp.responseText.replace('qtip:', ''), function(sel) {
						      if (sel == 'yes') {
							      self.deleteWords(true);
						      }
					      });
				    } else {
					    Ext.Msg.alert('错误', resp.responseText)
				    }
			    },
			    params : {
				    sync : self.wordGrid.getTopToolbar().syncWC.checked,
				    'ispub' : self.ispub,
				    ids : ids.join(','),
				    'isAudit' : isAudit,
				    forceDelete : forceDelete ? forceDelete : false
			    }
		    });

	  },
	  deleteWords : function(forceDelete) {
		  this.mask = false;
		  var rows = this.wordGrid.getSelectionModel().getSelections();
		  if (rows.length == 0) {
			  Ext.Msg.alert('提示', '请先选择要删除的记录！');
			  return false;
		  }
		  var self = this;
		  if (!forceDelete) {
			  Ext.Msg.confirm('确定框', '确定要删除这' + rows.length + '记录吗？', function(sel) {
				    if (sel == 'yes') {
					    self.deleteWordFun(rows, self);
				    }
			    });
		  } else {
			  self.deleteWordFun(rows, self, forceDelete);
		  }
	  },
	  deleteSynoWords : function() {
		  var self = this;
		  var synoword = self.tbarEx.get('search-syno').getValue();
		  if (!synoword)
			  Ext.Msg.alert('提示', '没有可删除的同义词关键字');
		  this.mask = false;
		  var rows = this.wordGrid.getSelectionModel().getSelections();
		  var data = this.wordGrid.getSelectionModel().getSelections().data
		  if (!isAudit && rows.length == 0) {
			  Ext.Msg.alert('提示', '请先选择要删除的同义词记录！');
			  return false;
		  } else {
			  rows = this.wordGrid.getStore().getRange();
		  }
		  Ext.Msg.confirm('确定框', '确定要删除这' + rows.length + '记录吗？', function(sel) {
			    if (sel == 'yes') {
				    var ids = [];
				    Ext.each(rows, function(_item) {
					      ids.push(_item.id)
				      })
				    Ext.Ajax.request({
					      url : 'wordclass!deleteBySynowordAndIds.action',
					      success : function(resp) {
						      Dashboard.setAlert('删除成功！')
						      self.wordGrid.getStore().reload();
					      },
					      failure : function(resp) {
						      Ext.Msg.alert('错误', resp.responseText)
					      },
					      params : {
						      sync : self.wordGrid.getTopToolbar().syncSyno.checked,
						      'ispub' : self.ispub,
						      ids : ids.join(','),
						      'isAudit' : isAudit,
						      synoword : synoword.trim(),
						      'preciseExc' : self.tbarEx.searchBtn.preciseSearch.checked
					      }
				      });
			    }
		    });
	  },
	  destroy : function() {
		  var dl = ['lastDataUrl', 'treeComboExpandL', 'saveMode'];
		  for (var k in dl)
			  if (dl[k])
				  delete dl[k]
		  WordclassPanel.superclass.destroy.call(this)
	  },
	  loadWordclassWord : function(ispub, wordclassId, isAudit) {
		  var self = this;
		  var _params = {
			  wordclassId : wordclassId
		  };
		  _params.ispub = ispub;
		  _params.isAudit = isAudit;
		  Ext.Ajax.request({
			    url : 'wordclass!findWordclassWords.action',
			    params : _params,
			    success : function(resp) {
				    var total = Ext.decode(resp.responseText).total;
				    var _words = Ext.decode(resp.responseText);
				    self.wordProxy.setData(_words);
				    self.wordEditGrid.getStore().load();
			    }
		    })
	  }
  });