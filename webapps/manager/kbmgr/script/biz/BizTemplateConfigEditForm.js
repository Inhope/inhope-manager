var BizTemplateConfigEditForm = Ext.extend(Ext.FormPanel, {
			border : false,
			bodyStyle : 'background-color:' + sys_bgcolor + ' padding : 3px 3px;',
			defaults : {
				xtype : 'textfield',
				anchor : '100%'
			},
			refresh : function(clearEditor) {
				this.loadData(this.tplId, clearEditor)
			},
			loadData : function(tplId, clearEditor) {
				this.tplId = tplId;
				if (clearEditor !== false) {
					this.attributeStore.removeAll()
					this.attributeStoreAll.removeAll()
					this.dimTagListP.getSelectionModel().clearSelections();
				}
				Ext.Ajax.request({
							url : 'biz-template!loadConfigInitData.action?tplId=' + tplId,
							success : function(response) {
								var result = Ext.decode(response.responseText);
								this.initData = result.data;
								this.dimTagStore.loadData(this.initData);// dimTags
								if (this.initData && this.dimTagListP.getColumnModel())
									this.dimTagListP.getColumnModel().setColumnHeader(0, this.initData.dimName)
							},
							scope : this
						});
			},
			loadConfigData : function(tplId, dimTagId, exists) {
				var cb = function(data) {
					var allattributes = [].concat(this.initData.tpl.attributes)
					if (data) {
						for (var i = allattributes.length - 1; i >= 0; i--) {
							Ext.each(data.attributes, function(attr) {
										if (allattributes[i].attributeId == attr.attributeId) {
											allattributes.splice(i, 1)
											return false;
										}
									})
						}
					} else {
						data = {};
						data.attributes = [].concat(allattributes);
						allattributes.length = 0
					}
					this.attributeStore.loadData(data)
					this.attributeStoreAll.loadData({
								attributes : allattributes
							})
					this._configData = {
						tplId : tplId,
						dimTagId : dimTagId
					}
					if (data.id)
						this._configData.id = data.id
				}
				if (exists) {
					Ext.Ajax.request({
								url : 'biz-template!loadConfig.action?tpl_id=' + tplId + '&dim_tag_id=' + dimTagId,
								success : function(response) {
									var result = Ext.decode(response.responseText);
									cb.call(this, result.data)
								},
								failure : function(response) {
									Ext.Msg.alert("错误", response.responseText);
								},
								scope : this
							});
				} else {
					cb.call(this)
				}
			},
			saveConfigData : function() {
				var values = Ext.apply({}, this._configData);
				var attrsMSel = this.attrMultiSel.fromMultiselect
				values.attributes = [];
				attrsMSel.store.each(function(r, i) {
							values.attributes.push({
										attributeId : r.get('attributeId'),
										name : r.get('name'),
										priority : i
									})
						})
				Ext.Ajax.request({
							url : 'biz-template!saveConfig.action',
							params : {
								data : Ext.encode(values)
							},
							success : function(response) {
								var result = Ext.decode(response.responseText);
								this._configData.id = result.data.id
								Dashboard.setAlert('保存成功')
								this.refresh(false)
								this.fireEvent('templateConfigSaved', result.data)
							},
							failure : function(response) {
								Ext.Msg.alert("错误", response.responseText);
							},
							scope : this
						});
			},
			deleteConfigData : function() {
				var r = this.dimTagListP.getSelectionModel().getSelected()
				if (r && r.get('name').indexOf('*') != -1) {
					Ext.Ajax.request({
								url : 'biz-template!deleteConfig.action',
								params : {
									dim_tag_id : r.get('id'),
									tpl_id : this.tplId
								},
								success : function(response) {
									this.refresh(true)
									Dashboard.setAlert('删除成功')
								},
								scope : this
							});
				} else
					Ext.Msg.alert('提示', '请先选择已配置的维度')
			},
			initComponent : function() {
				// ---- multiselect patch ---->
				this.on('afterlayout', function() {
							if (this.attrsFieldset && this.getForm().findField('attributes').fromMultiselect) {
								Ext.each(['to', 'from'], function(target) {
											var el = this.getForm().findField('attributes')[target + 'Multiselect'].getEl();
											el.setHeight(this.attrsFieldset.getEl().getBox().height);
											el.select('.ux-mselect', true, el).setHeight(this.attrsFieldset.getEl().getBox().height - 40)
										}, this)
								this.attrsFieldset.doLayout()
							}
						})
				// ---- multiselect patch ----<
				this.on('render', function() {
							if (this.initData && this.dimTagListP.getColumnModel())
								this.dimTagListP.getColumnModel().setColumnHeader(0, this.initData.dimName)
						})
				this.tbar = [{
							text : '保存',
							iconCls : 'icon-table-save',
							handler : this.saveConfigData.dg(this)
						}, {
							text : '删除',
							iconCls : 'icon-table-delete',
							handler : this.deleteConfigData.dg(this)
						}, {
							text : '刷新',
							iconCls : 'icon-refresh',
							handler : function() {
								this.refresh(true)
							}.dg(this)
						}]
				this.dimTagStore = new Ext.data.JsonStore({
							pruneModifiedRecords : true,
							root : 'dimTags',
							idProperty : 'id',
							fields : ['name', 'id']
						})
				this.attributeStoreAll = new Ext.data.JsonStore({
							pruneModifiedRecords : true,
							root : 'attributes',
							idProperty : 'attributeId',
							fields : ['name', 'attributeId']
						})
				this.attributeStore = new Ext.data.JsonStore({
							pruneModifiedRecords : true,
							root : 'attributes',
							idProperty : 'attributeId',
							fields : ['name', 'attributeId']
						})
				this.layout = 'border'
				this.defaults = {
					width : 250,
					xtype : 'textfield'
				}
				this.items = [{
							xtype : 'grid',
							border : false,
							split : true,
							region : 'west',
							ref : 'dimTagListP',
							store : this.dimTagStore,
							width : 150,
							sm : new Ext.grid.RowSelectionModel({
										singleSelect : true
									}),
							colModel : new Ext.grid.ColumnModel({
										columns : [{
													header : '维度',
													dataIndex : 'name'
												}]
									}),
							listeners : {
								rowclick : function(grid, row) {
									var r = grid.store.getAt(row);
									if (r)
										this.loadConfigData(this.initData.tpl.id, r.get('id'), r.get('name').indexOf('*') != -1)
								},
								scope : this
							}
						}, {
							xtype : 'fieldset',
							region : 'center',
							ref : 'attrsFieldset',
							style : 'background-color:' + sys_bgcolor + ' padding : 0',
							bodyStyle : 'background-color:' + sys_bgcolor + ' padding : 0',
							title : '属性选择',
							anchor : '100% -80',
							layout : 'border',
							defaults : {
								border : false
							},
							items : [{
										layout : 'form',
										region : 'center',
										bodyStyle : 'background-color:' + sys_bgcolor + ' padding : 0;padding-left:5px',
										labelWidth : 0,
										items : [{
													hideLabel : true,
													anchor : '100%',
													ref : '../../attrMultiSel',
													name : 'attributes',
													bodyStyle : 'background-color:' + sys_bgcolor + ' padding : 0',
													xtype : 'itemselector',
													drawUpIcon : true,
													drawDownIcon : true,
													drawLeftIcon : true,
													drawRightIcon : true,
													drawTopIcon : false,
													drawBotIcon : false,
													imagePath : 'images/ux/',
													multiselects : [{
																bodyStyle : 'background-color:' + sys_bgcolor + ' padding : 0;',
																displayField : 'name',
																valueField : 'attributeId',
																width : 250,
																height : 300,
																store : this.attributeStoreAll,
																legend : '不可见属性'
															}, {
																bodyStyle : 'background-color:' + sys_bgcolor + ' padding : 0;',
																width : 250,
																height : 300,
																legend : '可见属性',
																displayField : 'name',
																valueField : 'attributeId',
																store : this.attributeStore
															}]
												}]
									}]
						}];
				BizTemplateConfigEditForm.superclass.initComponent.call(this)
			}
		})

var BizTemplateConfigEditFormWin = Ext.extend(Ext.Window, {
			initComponent : function() {
				this.items = [this.editform = new BizTemplateConfigEditForm()]
				BizTemplateConfigEditFormWin.superclass.initComponent.call(this)
			},
			loadData : function() {
				return this.editform.loadData.apply(this.editform, arguments)
			},
			layout : 'fit',
			width : 700,
			height : 500,
			closeAction : 'hide'
		})