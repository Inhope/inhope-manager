var BizTemplateEditForm = Ext.extend(Ext.FormPanel, {
			border : false,
			// bodyStyle : 'padding : 3px 10px;',
			bodyStyle : 'background-color:' + sys_bgcolor + ' padding : 3px 3px;',
			defaults : {
				xtype : 'textfield',
				anchor : '100%'
			},
			loadData : function(id, catId) {
				this.getForm().reset();
				if (id) {
					Ext.Ajax.request({
								url : 'biz-template!loadTpl.action?id=' + id,
								success : function(response) {
									var result = Ext.decode(response.responseText);
									this.getForm().loadRecord(new Ext.data.Record(result.data));
									this.attributeStore.loadData(result.data)
								},
								failure : function(response) {
									Ext.Msg.alert("错误", response.responseText);
								},
								scope : this
							});
				} else {
					this.getForm().loadRecord(new Ext.data.Record({
								catId : catId
							}));
				}
			},
			saveData : function() {
				if (!this.getForm().isValid()) {
					return;
				}
				var values = this.getForm().getValues();
				values.attributes = [];
				this.attributeStore.each(function(r, i) {
							var d = {
								id : r.json ? r.json.id : null,
								priority : i,
								attributeId : r.get('attributeId'),
								name : r.get('name')
							}
							if (r.get('parentAttributeId'))
								d.parentAttributeId = r.get('parentAttributeId')
							values.attributes.push(d)
						})
				Ext.Ajax.request({
							url : 'biz-template!saveTpl.action',
							params : {
								data : Ext.encode(values)
							},
							success : function(response) {
								var result = Ext.decode(response.responseText);
								this.getForm().reset()
								this.getForm().loadRecord(new Ext.data.Record(result.data));
								this.attributeStore.loadData(result.data)
								Dashboard.setAlert('保存成功')
								this.fireEvent('templateSaved', result.data)
							},
							failure : function(response) {
								Ext.Msg.alert("错误", response.responseText);
							},
							scope : this
						});
			},
			addBizTplAttr : function(attrNodes) {
				var s = ''
				Ext.each(attrNodes, function(n) {
							if (this.attributeStore.find('parentAttributeId', n.id) != -1) {
								if (s)
									s += '，'
								s += n.text
								return;
							}
							if (this.attributeStore.find('attributeId', n.id) == -1) {
								this.attributeStore.add(new Ext.data.Record({
											attributeId : n.id,
											name : n.text
										}))
							}
						}, this)
				if (s) {
					Dashboard.setAlert('属性组同名属性（' + s + '）已存在，请重新选择属性', 'error')
				}
			},
			addBizTplParentAttr : function(n) {
				if (this.attributeStore.find('attributeId', n.id) != -1) {
					Dashboard.setAlert('属性名已存在，请重新选择属性组', 'error')
					return;
				}
				var rs = this.attrListP.getSelectionModel().getSelections()
				Ext.each(rs, function(r) {
							r.set('parentName', n.text)
							r.set('parentAttributeId', n.id)
						}, this)
			},
			initComponent : function() {
				this.tbar = [{
							text : '保存',
							iconCls : 'icon-table-save',
							handler : this.saveData.dg(this)
						}]
				this.attributeStore = new Ext.data.JsonStore({
							root : 'attributes',
							idProperty : 'id',
							fields : ['id', 'name', 'attributeId', 'parentAttributeId', 'parentName']
						})
				this.defaults = {
					width : 250,
					xtype : 'textfield'
				}
				this.items = [{
							name : 'name',
							fieldLabel : '模板名字',
							allowBlank : false
						}, {
							fieldLabel : '默认输入模板',
							xtype : 'combo',
							hiddenName : 'inRendererId',
							triggerAction : 'all',
							lazyRender : false,
							editable : false,
							mode : 'remote',
							store : new Ext.data.JsonStore({
										url : 'biz-template-category!listAllRenderer.action?cat_id=1',
										root : 'data',
										idProperty : 'id',
										autoLoad : true,
										fields : ['name', 'id'],
										listeners : {
											load : function() {
												this.getForm().findField('inRendererId').setValue(this.getForm().findField('inRendererId').getValue())
											},
											scope : this
										}
									}),
							valueField : 'id',
							displayField : 'name'

						}, {
							fieldLabel : '默认输出模板',
							xtype : 'combo',
							hiddenName : 'outRendererId',
							triggerAction : 'all',
							lazyRender : false,
							editable : false,
							mode : 'remote',
							store : new Ext.data.JsonStore({
										url : 'biz-template-category!listAllRenderer.action?cat_id=2',
										root : 'data',
										idProperty : 'id',
										autoLoad : true,
										fields : ['name', 'id'],
										listeners : {
											load : function() {
												this.getForm().findField('outRendererId').setValue(this.getForm().findField('outRendererId').getValue())
											},
											scope : this
										}
									}),
							valueField : 'id',
							displayField : 'name'
						}, {
							xtype : 'fieldset',
							ref : 'attrsFieldset',
							style : 'background-color:' + sys_bgcolor + ' padding : 3px',
							bodyStyle : 'background-color:' + sys_bgcolor + ' padding : 0',
							title : '属性选择',
							anchor : '100% -80',
							layout : 'border',
							defaults : {
								border : false
							},
							items : [{
								region : 'center',
								xtype : 'grid',
								border : false,
								split : true,
								ref : '../attrListP',
								store : this.attributeStore,
								width : 150,
								listeners : {
									cellclick : function(grid, rowIndex, columnIndex, e) {
										var st = grid.store
										if (e.getTarget('.del')) {
											st.removeAt(rowIndex)
										} else if (e.getTarget('.move_up')) {
											if (rowIndex > 0) {
												var r = st.getAt(rowIndex)
												st.removeAt(rowIndex)
												st.insert(rowIndex - 1, r)
											}
										} else if (e.getTarget('.move_down')) {
											if (rowIndex < st.getCount() - 1) {
												var r = st.getAt(rowIndex)
												st.removeAt(rowIndex)
												st.insert(rowIndex + 1, r)
											}
										}
									},
									scope : this
								},
								colModel : new Ext.grid.ColumnModel({
											columns : [{
														header : '属性名',
														dataIndex : 'name',
														width : 200
													}, {
														header : '属性组名',
														dataIndex : 'parentName',
														width : 200
													}, {
														header : '操作',
														renderer : function() {
															return '<a href="javascript:void(0)" class="del">删除</a>  ' + '<a href="javascript:void(0)" class="move_up">上移</a>  '
																	+ '<a href="javascript:void(0)" class="move_down">下移</a>'
														}
													}]
										})
							}, {
								border : true,
								region : 'west',
								ref : '../classSelector',
								width : 200,
								collapsible : true,
								draggable : true,
								rootVisible : false,
								animCollapse : true,
								autoScroll : true,
								xtype : 'treepanel',
								title : '属性选择工具',
								listeners : {
									dblclick : function(n) {
										if (n.attributes.attachment && n.attributes.attachment.attributeId)
											this.addBizTplAttr(n)
									},
									scope : this
								},
								tbar : [{
											text : '添加到属性列表',
											tooltip : '也可双击属性直接添加',
											iconCls : 'icon-add',
											handler : function() {
												var n = this.classSelector.getSelectionModel().getSelectedNode();
												if (n) {
													if (n.isLeaf()) {
														this.addBizTplAttr(n)
													} else if (n.isLoaded()) {
														if (n.childNodes) {
															Ext.each(n.childNodes, function(n) {
																		if (n.attributes.attachment && n.attributes.attachment.attributeId)
																			this.addBizTplAttr(n)
																	}, this)
														}
													} else {
														n.reload(function() {
																	Ext.each(n.childNodes, function(n) {
																				if (n.attributes.attachment && n.attributes.attachment.attributeId)
																					this.addBizTplAttr(n)
																			}, this)
																}, this)
													}
												} else {
													Dashboard.setAlert('请先选择属性', 'error')
												}
											}.dg(this)
										}, {
											text : '添加属性组',
											iconCls : 'icon-add',
											handler : function() {
												var n = this.classSelector.getSelectionModel().getSelectedNode();
												if (n) {
													if (n.isLeaf()) {
														this.addBizTplParentAttr(n)
													} else {
														Dashboard.setAlert('不能添加本体类，请先选择属性', 'error')
													}
												} else {
													Dashboard.setAlert('请先选择属性', 'error')
												}
											}.dg(this)
										}],
								root : {
									text : 'root',
									id : '0'
								},
								dataUrl : 'ontology-class!list.action'
							}]
						}, {
							name : 'catId',
							hidden : true
						}, {
							name : 'id',
							hidden : true
						}];

				BizTemplateEditForm.superclass.initComponent.call(this)
			}
		})
