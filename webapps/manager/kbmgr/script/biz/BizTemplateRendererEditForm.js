var BizTemplateRendererEditForm = Ext.extend(Ext.FormPanel, {
			border : false,
			// bodyStyle : 'padding : 3px 10px;',
			bodyStyle : 'background-color:' + sys_bgcolor + ' padding : 3px 3px;',
			defaults : {
				xtype : 'textfield',
				anchor : '100%'
			},
			loadData : function(id, catId) {
				this.getForm().reset();
				if (id) {
					Ext.Ajax.request({
								url : 'biz-template!loadRenderer.action?id=' + id,
								success : function(response) {
									var result = Ext.decode(response.responseText);
									this.getForm().loadRecord(new Ext.data.Record(result.data));
								},
								failure : function(response) {
									Ext.Msg.alert("错误", response.responseText);
								},
								scope : this
							});
				} else {
					this.getForm().loadRecord(new Ext.data.Record({
								catId : catId
							}));
				}
			},
			saveData : function() {
				if (!this.getForm().isValid()) {
					return;
				}
				var values = this.getForm().getValues();
				Ext.Ajax.request({
							url : 'biz-template!saveRenderer.action',
							params : {
								data : Ext.encode(values)
							},
							success : function(response) {
								var result = Ext.decode(response.responseText);
								this.getForm().reset()
								this.getForm().loadRecord(new Ext.data.Record(result.data));
								Dashboard.setAlert('保存成功')
								this.fireEvent('templateSaved', result.data)
							},
							failure : function(response) {
								Ext.Msg.alert("错误", response.responseText);
							},
							scope : this
						});
			},
			addToAllSelector : function(attrNodes) {
				Ext.each(attrNodes, function(n) {
							if (this.attributeStore.find('attributeId', n.id) == -1 && this.attributeStoreAll.find('attributeId', n.id) == -1)
								this.attributeStoreAll.add(new Ext.data.Record({
											attributeId : n.id,
											name : n.text
										}))
						}, this)
			},
			initComponent : function() {
				this.tbar = [{
							text : '保存',
							iconCls : 'icon-table-save',
							handler : this.saveData.dg(this)
						}]
				this.items = [{
							name : 'name',
							fieldLabel : '模板名字',
							allowBlank : false
						}, {
							fieldLabel : '模板内容',
							name : 'content',
							xtype : 'textarea',
							anchor : '100% -30'
						}, {
							name : 'catId',
							hidden : true
						}, {
							name : 'id',
							hidden : true
						}];

				BizTemplateRendererEditForm.superclass.initComponent.call(this)
			}
		})
