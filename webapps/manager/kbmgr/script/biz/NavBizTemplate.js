NavBizTemplate = function(_cfg) {

	var self = this;
	this.wordclassType;
	this.data = {};
	this.ispub = null;
	this.wordPanel = '';

	var cfg = {
		title : '业务模版管理',
		width : 200,
		minSize : 175,
		maxSize : 400,
		xtype : 'treepanel',
		collapsible : true,
		rootVisible : false,
		lines : false,
		border : false,
		autoScroll : true,
		listeners : {},
		root : {
			id : '0',
			text : 'root',
			iconCls : 'icon-ontology-root',
			children : [{
						id : '3',
						text : "业务知识模版"
					}, {
						id : '1',
						text : "采编UI模板"
					}, {
						id : '2',
						text : "发布UI模板"
					}]

		},
		dataUrl : 'biz-template-category!list.action'
	};

	NavBizTemplate.superclass.constructor.call(this, Ext.applyIf(_cfg || {}, cfg));

	var menu = new Ext.menu.Menu({
				items : [{
							text : '刷新',
							iconCls : 'icon-refresh',
							width : 50,
							handler : function() {
								self.refreshNode();
							}
						}, {
							text : '新建子分类',
							iconCls : 'icon-wordclass-add',
							ref : '_addCateBtn',
							width : 50,
							handler : function() {
								self.addNode(menu.currentNode);
							}
						}, {
							text : '修改分类',
							ref : '_modifyCateBtn',
							iconCls : 'icon-wordclass-edit',
							width : 50,
							handler : function() {
								if (menu.currentNode.id == 1 || menu.currentNode.id == 2 || menu.currentNode.id == 3) {
									Ext.Msg.alert('错误提示', '根节点禁止修改');
									return;
								}
								self.modifyNode(menu.currentNode);
							}
						}, {
							text : '删除分类',
							ref : '_delCateBtn',
							iconCls : 'icon-wordclass-del',
							width : 50,
							handler : function() {
								self.deleteNode(menu.currentNode);
							}
						}, '-', {
							text : '新增模板',
							ref : '_addInCateBtn',
							iconCls : 'icon-wordclass-add',
							handler : function() {
								this.addTplNode(menu.currentNode)
							}.dg(this)
						}, {
							text : '删除模板',
							ref : '_delBtn',
							iconCls : 'icon-wordclass-del',
							handler : function() {
								this.deleteTplNode(menu.currentNode)
							}.dg(this)
						}, {
							text : '配置模板',
							ref : '_configBtn',
							nodeType : 3,
							iconCls : 'icon-wordclass-edit',
							handler : function() {
								this.configTplNode(menu.currentNode)
							}.dg(this)
						}, '-', {
							text : '生成本体类模板',
							ref : '_generateCateTplBtn',
							nodeType : 3,
							iconCls : 'icon-wordclass-add',
							handler : function() {
								this.generateTpl(menu.currentNode)
							}.dg(this)
						}]
			});

	this.menu = menu;

	this.on('contextmenu', function(node, event) {
				event.preventDefault();
				menu.currentNode = node;
				node.select();
				for (var key in menu) {
					if (key && key.indexOf('Btn') == key.length - 3 && key.indexOf('_') == 0) {
						var nt = menu[key].nodeType;
						if (this.isCategory(node)) {
							menu[key][key.indexOf('Cate') != -1 && (!nt || nt && nt == this.getNodeType(node)) ? 'enable' : 'disable']()
						} else {
							menu[key][key.indexOf('Cate') == -1 && (!nt || nt && nt == this.getNodeType(node)) ? 'enable' : 'disable']()
						}
					}
				}
				menu.showAt(event.getXY());
			});
	var _treeCombo = new TreeComboBox({
				fieldLabel : '父级词类',
				emptyText : '请选择父级词类...',
				editable : false,
				anchor : '95%',
				allowBlank : false,
				minHeight : 250,
				name : 'parentCategoryId',
				root : {
					id : '0',
					text : 'root',
					iconCls : 'icon-ontology-root'
				},
				loader : new Ext.tree.TreeLoader({
							dataUrl : 'biz-template-category!list.action'
						})
			});
	this.treeCombo = _treeCombo;
	var _formPanel = new Ext.form.FormPanel({
				frame : false,
				border : false,
				labelWidth : 75,
				autoHeight : true,
				waitMsgTarget : true,
				bodyStyle : 'padding : 3px 10px; background-color:' + sys_bgcolor,
				items : [new Ext.form.Hidden({
									name : 'categoryId'
								}), new Ext.form.Hidden({
									name : 'bh'
								}), _treeCombo, new Ext.form.TextField({
									fieldLabel : '类型名称',
									allowBlank : false,
									name : 'name',
									blankText : '请输入类型名称',
									anchor : '100%'
								})
				// ,this.pubSchemaSelector = new PubSchemaSelector()
				],
				tbar : [{
							text : '保存',
							iconCls : 'icon-wordclass-add',
							handler : function() {
								self.saveNode(this);
							}
						}]
			});
	var _formWin = new Ext.Window({
				width : 495,
				title : '类型管理',
				defaults : {
					border : false
				},
				modal : true,
				plain : true,
				shim : true,
				closable : true,
				closeAction : 'hide',
				collapsible : true,
				resizable : false,
				draggable : true,
				animCollapse : true,
				constrainHeader : true,
				listeners : {
					'beforehide' : function(p) {
						p.formPanel.getForm().reset();
					}
				},
				items : [_formPanel]
			});
	_formWin.formPanel = _formPanel;
	this.formWin = _formWin;
}

Ext.extend(NavBizTemplate, Ext.tree.TreePanel, {
			initComponent : function() {
				this.on('render', function(n) {
							// n={id:"4028819f3ac00a90013ac01e49730003",text:'hello'}
							// var p = this.showTab(BizTemplateEditForm,
							// 'biz-nav-' + n.id, '模板编辑 - ' + n.text,
							// 'icon-wordclass-category', true);
							// p.loadData(n.id, null);
						})
				this.on('click', this.editTplNode.dg(this))
				this.tools = [{
							id : 'refresh',
							qtip : '刷新',
							handler : function() {
								this.getRootNode().reload()
							}.dg(this)
						}]
				NavBizTemplate.superclass.initComponent.call(this)
			},
			isCategory : function(n) {
				return !n.attributes.attachment || n.attributes.attachment && n.attributes.attachment.bh;
			},
			addEditTplListener : function(editP, n, pn) {
				if (!editP._tpl_save_ls) {
					editP._tpl_save_ls = true
					editP.on('templateSaved', function(d) {
								if (!pn && n && n.parentNode)
									pn = n.parentNode
								if (pn)
									pn.reload()
							})
				}
			},
			editTplNode : function(n) {
				if (this.isCategory(n))
					return;
				var p = this.showTab(this.getNodeType(n) == 3 ? BizTemplateEditForm : BizTemplateRendererEditForm, 'biz-tpl-' + n.id, '模板编辑 - ' + n.text,
						'icon-wordclass-category', true);
				p.loadData(n.id, null);
				this.addEditTplListener(p, n)
			},
			addTplNode : function(pnode) {
				if (!this.isCategory(pnode))
					return;
				var p = this.showTab(this.getNodeType(pnode) == 3 ? BizTemplateEditForm : BizTemplateRendererEditForm, 'biz-tpl-' + Ext.id(), '新建模板', 'icon-wordclass-category',
						true);
				p.loadData(null, pnode.id);
				this.addEditTplListener(p, '', pnode)
			},
			deleteTplNode : function(n) {
				if (!n.isLeaf())
					return
				Ext.Ajax.request({
							url : 'biz-template!delete' + (this.getNodeType(n) == 3 ? 'Tpl' : 'Renderer') + '.action?id=' + n.id,
							success : function(response) {
								Dashboard.setAlert('删除成功')
								n.parentNode.reload()
							},
							scope : this
						});
			},
			generateTpl : function(node) {
				Ext.Ajax.request({
							url : 'biz-template!generateTplFromClass.action?catId=' + node.id,
							success : function(response) {
								Dashboard.setAlert("本体类模板已生成");
							}
						})
			},
			configTplNode : function(node) {
				if (this.getNodeType(node) == 3 && node.isLeaf()) {
					if (!this._biztplcfgedwin) {
						this._biztplcfgedwin = new BizTemplateConfigEditFormWin()
					}
					this._biztplcfgedwin.show()
					this._biztplcfgedwin.loadData(node.id);
					this._biztplcfgedwin.setTitle('配置模板 - ' + node.text);
				}
			},
			getNodeType : function(node) {
				while (node && node.parentNode && node.id && node.id.length > 1) {
					node = node.parentNode
				}
				return parseInt(node.id);
			},
			endEdit : function() {
				this.formWin.hide();
				this.formWin.formPanel.getForm().reset();
			},
			isRoot : function(node) {
				return node.id.length == 1;
			},
			saveNode : function(btn) {
				btn.disable();
				var f = this.formWin.formPanel.getForm();
				if (!f.isValid()) {
					Ext.Msg.alert('提示', '您尚未正确完整的输入表单内容，请检查！');
					btn.enable();
					return false;
				}
				this.data = f.getValues();
				this.data.parentCategoryId = this.treeCombo.getValue();
				// var ispub = this.data.ispub;
				// delete this.data.ispub;
				delete this.data.important
				delete this.data.similar
				// if (this.data.parentCategoryId.length == 1) {
				// this.data.parentCategoryId = null;
				// }
				if (!this.data.categoryId)
					this.data.categoryId = null;
				var self = this;
				Ext.Ajax.request({
							url : 'biz-template-category!save.action',
							params : {
								// 'ispub' : ispub,
								data : Ext.encode(self.data)
							},
							success : function(form, action) {
								Dashboard.setAlert("保存成功!");
								self.endEdit();
								self.updateNode(Ext.decode(form.responseText));
								// self.updateNode(Ext.decode(form.responseText),
								// ispub);
								btn.enable();
							},
							failure : function(response) {
								Ext.Msg.alert("错误", response.responseText);
								btn.enable();
							}
						})
			},
			addNode : function(node) {
				// this.formWin.showPubSelector(Dashboard.u().allow('ALL'))
				// this.ispub = node.attributes.ispub;
				var rec = new Ext.data.Record(Ext.apply({}, node.attributes));
				// rec.set('ispub', this.ispub ? this.ispub : '');
				var fp = this.formWin.formPanel;
				fp.getForm().loadRecord(rec);
				// if (node.id == '1') {// set default pub schema
				// this.pubSchemaSelector.setFirstSchema()
				// fp.find('hiddenName', 'ispub')[0].setReadOnly(false)
				// } else
				// fp.find('hiddenName', 'ispub')[0].setReadOnly(true)
				this.formWin.show();
				if (this.isRoot(node))
					this.treeCombo.setReadOnly(true);
				else
					this.treeCombo.setReadOnly(false);
				this.treeCombo.setValueEx(node);
				var _chkGroup = this.formWin.formPanel.capChkGroup;
				var _chkVals = {};
			},
			modifyNode : function(node) {
				// this.formWin.showPubSelector(Dashboard.u().allow('ALL'))
				// this.ispub = node.attributes.ispub;
				this.formWin.show();
				var fp = this.formWin.formPanel;
				var rec = new Ext.data.Record(Ext.apply({}, node.attributes));
				rec.set('name', rec.get('text'));
				rec.set('categoryId', rec.get('id'));
				// rec.set('ispub', this.ispub ? this.ispub : '');
				fp.getForm().loadRecord(rec);
				if (this.isRoot(node.parentNode))
					this.treeCombo.setReadOnly(true);
				else
					this.treeCombo.setReadOnly(false);
				this.treeCombo.setValueEx(node.parentNode);
			},
			deleteNode : function(node) {
				var wordPanel = this.wordPanel
				if (node.id.length == 1) {
					Ext.Msg.alert('警告', '根节点不能删除！');
					return;
				}
				Ext.Msg.confirm('提示', '确定要删除&nbsp&nbsp' + node.getPath('text').replace('/root', '') + '&nbsp&nbsp及以下所有分类吗', function(btn, text) {
							if (btn == 'yes') {
								Ext.Ajax.request({
											url : 'biz-template-category!del.action',
											params : {
												// 'ispub' :
												// node.attributes.ispub,
												'bh' : node.attributes.bh
											},
											success : function(response) {
												var node4Sel = node.nextSibling ? node.nextSibling : node.previousSibling;
												if (!node4Sel) {
													node4Sel = node.parentNode;
													node4Sel.leaf = true;
												}
												node4Sel.select();
												if (typeof(wordPanel) != 'undefined' && wordPanel != '') {
													wordPanel.loadWords(node4Sel.id)
												}
												node.parentNode.removeChild(node);
											},
											failure : function(response) {
												Ext.Msg.alert("错误", response.responseText);
											}
										});
							}
						});
			},
			refresh : function(node) {
				if (node.isExpanded())
					node.reload();
				else
					node.expand();
			},
			getSelectedNode : function() {
				return this.getSelectionModel().getSelectedNode();
			},
			updateNode : function(data, ispub) {
				var parentCateId = this.data.parentCategoryId;
				var parentNode;
				if (parentCateId == null) {
					// if (ispub) {
					// parentNode = this.getRootNode().childNodes[0];
					// } else {
					// parentNode = this.getRootNode().childNodes[1];
					// }
					parentNode = this.getRootNode().childNodes[1];
				} else {
					parentNode = this.getNodeById(parentCateId);
				}
				if (!data.saveOrUpdate) {
					var selNode = this.getSelectedNode();
					selNode.setText(data.text);
					var color = 'black';
					if (data.cls.indexOf('import') > 0)
						color = 'red';
					else if (data.cls.indexOf('similar') > 0)
						color = 'blue';
					else if (data.cls.indexOf('both') > 0)
						color = 'purple'
					if (selNode.parentNode.id != parentNode.id) {
						var parentBefore = selNode.parentNode;
						if (parentNode) {
							selNode.leaf = data.leaf;
							selNode.attributes.bh = data.bh;
							parentNode.leaf = false;
							parentNode.appendChild(selNode);
						} else {
							parentBefore.removeChild(selNode);
						}
						if (parentBefore.childNodes.length == 0)
							parentBefore.leaf = true;
					}
				} else {
					if (parentNode) {
						parentNode.reload()
//						var node = new Ext.tree.TreeNode({
//									id : data.id,
//									iconCls : data.iconCls,
//									cls : data.cls,
//									leaf :  false ,
//									text : data.text,
//									bh : data.bh,
//									attachment : data.attachment
//								});
//						if (parentNode.leaf)
//							parentNode.leaf = false
//						parentNode.appendChild(node);
//						parentNode.expand();
//						node.select();
					}
				}
			},
			expandNode : function(node, bh, ispub) {
				var self = this;
				if (node.attributes.bh == bh || node.leaf) {
					this.getSelectionModel().select(node);
					return;
				}
				node.expand(false, true, function() {
							var childs = node.childNodes;
							for (var i = 0; i < childs.length; i++) {
								if (bh.indexOf(childs[i].attributes.bh) != -1)
									self.expandNode(childs[i], bh);
							}
						});
			},
			refreshNode : function() {
				var node = this.getSelectedNode();
				if (!node)
					this.refresh(this.getRootNode());
				else {
					this.refresh(node);
				}
			},
			authName : 'kb.biztpl'
		});
//var enable_biz_mode = 'true' == http_get('property!get.action?key=kbmgr.enable_biz_mode')
//if (!Dashboard.isCloudEmbed() && window.enable_biz_mode)
//	Dashboard.registerNav(NavBizTemplate, 1.5);