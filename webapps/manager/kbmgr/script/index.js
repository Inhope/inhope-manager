/**
 * 
 * @include "NavPanel.js"
 */
Dashboard = {
	u : function() {
		return this._usr ? this._usr : this._usr = new LoginUser('kb')
	},
	getModuleFeature : function(key) {
		return window.kb_mdl_ft ? window.kb_mdl_ft[key] : null
	},
	getModuleProp : function(key) {
		var base = window['kb_mdl_props']
		if (base){
			var i=1;
			while(true){
				if (window['kb_mdl_props'+i]){
					Ext.apply(base,window['kb_mdl_props'+i])
					window['kb_mdl_props'+i] = undefined
					i++;
				}else
					break;
			}
		}
		return window.kb_mdl_props ? window.kb_mdl_props[key] : null
	},
	setAlert : function(msg, status, delay) {
		if (!status)
			status = 'ok'
		if (!delay)
			delay = 6
		if (!this.msgCt) {
			this.msgCt = Ext.DomHelper.insertFirst(document.body, {
				  id : 'msg-div'
			  }, true);
			this.msgCt.setStyle('position', 'absolute');
			this.msgCt.setStyle('z-index', 9999);
			this.msgCt.setWidth(300);
		}
		this.msgCt.alignTo(document, 't-t');
		var html = ['<div class="app-msg">', '<div class="x-box-tl"><div class="x-box-tr"><div class="x-box-tc"></div></div></div>',
		  '<div class="x-box-ml"><div class="x-box-mr"><div class="x-box-mc"><h3 class="x-icon-text icon-status-' + status + '">', msg, '</h3>', '</div></div></div>',
		  '<div class="x-box-bl"><div class="x-box-br"><div class="x-box-bc"></div></div></div>', '</div>'].join('');
		var fxEl = Ext.DomHelper.append(this.msgCt, {
			  'html' : html
		  }, true);
		var endCb = function() {
			var dom = fxEl.dom
			if (dom) {
				fxEl.stopFx()
				fxEl.removeAllListeners()
				dom.parentNode.removeChild(dom)
				fxEl.remove();
			}
		}
		fxEl.on('click', endCb)
		fxEl.slideIn('t').pause(delay).ghost("t", {
			  callback : endCb
		  });
	},
	utils : {
		arrayDataToMap : function(arr) {
			var _all = [], ret = {};
			for (var i = 0; i < arr.length; i++) {
				_all = _all.concat(arr[i]);
			}
			for (var i = 0; i < _all.length; i++) {
				ret[_all[i][0]] = _all[i][1];
			}
			return ret;
		},
		newFaqTypeCombo : function() {
			return new Ext.form.ComboBox({
				  store : [[0, '普通问题'], [1, '肯定模板'], [2, '否定模板']],// 普通问,肯定模板,否定模板
				  mode : 'local',
				  triggerAction : 'all',
				  editable : false
			  });
		},
		newFaqTypeRenderer : function() {
			return function(value) {
				if (value == 0) {
					return '普通问题'
				} else if (value == 1) {
					return '肯定模板'
				} else if (value == 2) {
					return '否定模板'
				}
			};
		},
		newTemplateRenderer : function() {
			return function(value) {
				if (!value)
					return;
				var x = '';
				var istmplt = false
				for (var i = 0; i < value.length; i++) {
					var w = value.charAt(i)
					var color = null;
					if (w == '<') {
						w = '&lt;';
						color = 'blue';
					} else if (w == '>') {
						w = '&gt;';
						color = 'blue';
					} else if (w == '(') {
						color = '#c71585';
					} else if (w == ')') {
						color = '#c71585'
					} else if (w == '*') {
						w = "<span style='font-size:13pt'>*</span>"
						color = 'red';
					} else if (w == '?') {
						color = 'red'
					} else if (w == '|') {
						color = 'red'
					} else if (w == ']') {
						color = 'green'
					} else if (w == '[') {
						color = 'green'
					}

					if (color) {
						x += "<div style='display:inline;font-weight:bold;color:" + color + ";'>" + w + "</div>";
						istmplt = true
					} else
						x += w;
				}
				return istmplt ? ("<div style='display:inline;background-color:#ffff99'>" + x + "</div>") : x;
			}
		},
		newWordCap : function(cap) {
			return {
				'important' : (cap & 1) == 1,
				'similar' : (cap & 2) == 2
			};
		},
		newWordCapChkGroup : function(_cfg) {
			var cfg = {
				width : 200,
				columns : 2,
				items : [{
					  boxLabel : '重要',
					  name : 'important',
					  inputValue : 1,
					  anchor : '100%'
				  }, {
					  boxLabel : '相似',
					  name : 'similar',
					  inputValue : 2,
					  anchor : '100%'
				  }]
			};
			if (_cfg) {
				Ext.apply(cfg, _cfg);
			}
			return new Ext.form.CheckboxGroup(cfg);
		},
		newWordCapRenderer : function() {
			var nwcf = this.newWordCap;
			return function(cap) {
				var wcap = nwcf(cap);
				if (wcap.important && wcap.similar)
					return '重要,相似';
				else if (wcap.important)
					return '重要'
				else if (wcap.similar)
					return '相似';
				else
					return '无';
			}
		},
		collapsedPanelTitlePlugin : function() {
			this.init = function(p) {
				if (p.collapsible) {
					var r = p.region;
					if ((r == 'north') || (r == 'south')) {
						p.on('render', function() {
							  var ct = p.ownerCt;
							  ct.on('afterlayout', function() {
								    if (ct.layout[r].collapsedEl) {
									    p.collapsedTitleEl = ct.layout[r].collapsedEl.createChild({
										      tag : 'span',
										      cls : 'x-panel-collapsed-text',
										      html : p.title
									      });
									    p.setTitle = Ext.Panel.prototype.setTitle.createSequence(function(t) {
										      p.collapsedTitleEl.dom.innerHTML = t;
									      });
								    }
							    }, false, {
								    single : true
							    });
							  p.on('collapse', function() {
								    if (ct.layout[r].collapsedEl && !p.collapsedTitleEl) {
									    p.collapsedTitleEl = ct.layout[r].collapsedEl.createChild({
										      tag : 'span',
										      cls : 'x-panel-collapsed-text',
										      html : p.title
									      });
									    p.setTitle = Ext.Panel.prototype.setTitle.createSequence(function(t) {
										      p.collapsedTitleEl.dom.innerHTML = t;
									      });
								    }
							    }, false, {
								    single : true
							    });
						  });
					}
				}
			};
		}
	},
	temp : [],
	temp_grid : '',
	navRegistry : [],
	registerNav : function(navClass, sort) {
		this.navRegistry.push({
			  cls : navClass,
			  'createNav' : function(id) {
				  return new navClass({
					    'id' : id
				    })
			  },
			  'sort' : sort
		  })
	},
	isCloudEmbed : function() {
		return getTopWindow().location.toString().indexOf('clustermgr') != -1
	},
	createDimChooserEditable : function(_width, _excludeDims, _isShowAll) {
		return Ext.create({
			  width : _width,
			  name : 'dimensionBox',
			  xtype : 'dimensionEditableBox',
			  clearBtn : true,
			  emptyText : '请填写类别',
			  fieldLabel : '类别(逗号分隔)',
			  editable : true,
			  menuConfig : {
				  labelAlign : 'top',
				  excludeDims : _excludeDims,
				  isShowAll : _isShowAll
			  },
			  valueTransformer : function(value) {
				  if (value == 'ALLDIM')
					  return '';
				  var tags = this.tags;
				  var groupTags = {};
				  if (tags)
					  Ext.each(tags, function(t) {
						    groupTags[t.dimId] = t.id;
					    });
				  return Ext.encode(groupTags);
			  }
		  });
	},
	isKBase : function() {
		var isKBase = http_get('property!get.action?key=sys.kbase');
		isKBase = isKBase ? ("true" == isKBase ? true : false) : false;
		return isKBase;
	},
	isKBaseTag : function() {
		var isKBaseTag = http_get('property!get.action?key=kbase.label_path');
		return (isKBaseTag && isKBaseTag.length > 0) ? true : false;
	},
	isLabs : function() {
		var isLabs = http_get('property!get.action?key=sys.labs');
		isLabs = isLabs ? ("true" == isLabs ? true : false) : false;
		return isLabs;
	},
	isTimerTaskEnabled : function() {
		var isTimertask = http_get('property!get.action?key=timertask.enable');
		isTimertask = isTimertask ? ("true" == isTimertask ? true : false) : false;
		return isTimertask;
	}
};

Ext.onReady(function() {
	  Ext.QuickTips.init();

	  var mainPanel = new Ext.ux.panel.DDTabPanel({
		    region : 'center',
		    activeTab : 0,
		    border : false,
		    style : 'border-left: 1px solid ' + sys_bdcolor,
		    resizeTabs : true,
		    tabWidth : 150,
		    minTabWidth : 120,
		    enableTabScroll : true,
		    layoutOnTabChange : true,// with afterLayout
		    listeners : {
			    afterLayout : function() {

			    }
		    }
	    });
	  var tabCtxMenu = new Ext.menu.Menu({
		    items : [{
			      text : '关闭其他标签页',
			      iconCls : 'icon-status-cancel',
			      width : 50,
			      handler : function() {
				      var atab = this.getActiveTab();
				      var c = this.items.getCount();
				      for (var i = c - 1; i >= 0; i--) {
					      if (atab != this.items.get(i)) {
						      mainPanel.remove(this.items.get(i).id);
					      }
				      }
			      }.dg(mainPanel)
		      }]
	    });
	  mainPanel.on('contextmenu', function(tabp, targetp, e) {
		    tabCtxMenu.showAt(e.getXY())
	    })
	  var r = Dashboard.navRegistry.sort(function(nav1, nav2) {
		    // if (nav1.cls == WordNavPanel)return -1;
		    // if (nav2.cls == WordNavPanel)return 1;
		    return nav1.sort - nav2.sort
	    });

	  var tabRegistry = {};

	  var navItems = [];

	  Dashboard.u().filterAllowed(r, 'cls.prototype.authName')

	  for (var i = 0; i < r.length; i++) {
		  var nav = r[i].createNav();
		  nav.moduleIndex = i;
		  nav.showTab = function(panelClassOrCreateFn, tabId, title, iconCls, closable) {
			  if (!panelClassOrCreateFn)
				  return false;
			  var tab = mainPanel.get(tabId);
			  if (!tab) {
				  var tabPanel = !panelClassOrCreateFn.creator ? new panelClassOrCreateFn({
					    _id : tabId
				    }) : panelClassOrCreateFn({
					    _id : tabId
				    });
				  tabPanel.navPanel = this;
				  tabPanel.tabId = tabId;
				  tabPanel.tab = tab = mainPanel.add({
					    id : tabId,
					    layout : 'fit',
					    title : title,
					    items : tabPanel,
					    iconCls : iconCls,
					    closable : closable
				    });
				  tabPanel.addEvents('tabactive', 'tabinactive')
				  tabPanel.close = function() {
					  nav.closeTab(this.tabId)
				  }
				  tab.on('close', function() {
					    return tabPanel.fireEvent("close", tabPanel);
				    });
				  tab.on('beforeclose', function() {
					    return tabPanel.fireEvent("beforeclose", tabPanel);
				    });
				  tabRegistry[tabId] = this.moduleIndex;
				  tab.show();
			  } else {
				  mainPanel.activate(tabId);
				  if (title)
					  tab.setTitle(title)
				  if (iconCls)
					  tab.setIconClass(iconCls)
			  }
			  return tab.get(0);
		  };
		  nav.closeTab = function(tabId) {
			  mainPanel.remove(tabId)
		  };
		  nav.activateTab = function(tabId) {
			  mainPanel.activate(tabId)
		  };

		  if (i == 0) {
			  var f = function(node) {
				  // //
				  // this.showObjectTab({id:'00140506706929760552001b787a1bf8',cateIdPath:'/ff8080814723675b0147236b3efd0001/2c380d332f754e2eaeedb2529f4ec31f/'})
				  if (node.childNodes && node.childNodes.length) {
					  node.childNodes[0].expand();
					  (function() {
						  node.childNodes[0].fireEvent('click', node.childNodes[0]);
					  }).defer(100);
				  }
				  var self = this;

				  parent.navToKB = function(orientationObjectId, orientationDimValueId, valueId, editAttrDetail) {
					  Ext.Ajax.request({
						    url : 'ontology-object!getObjectById.action',
						    params : {
							    "objectId" : orientationObjectId
						    },
						    success : function(form, action) {
							    var obj = Ext.decode(form.responseText);
							    if (obj.data) {
							(function() {
									    self.showObjectTab(obj.data, orientationDimValueId);
								    }).defer(100);
								    if (editAttrDetail) {
									    window.editAttrDetail = editAttrDetail;
									    window.standQuesId = valueId;
								    }
							    } else
								    Dashboard.setAlert("没有找到对应的实例信息!", 3000);
						    },
						    failure : function(response) {
							    Ext.Msg.alert("错误", response.responseText);
						    }
					    })
				  }
				  // 根据传递参数打开对应知识点
				  if (parent.orientationObjectId && parent.orientationValueId) {
					  parent.navToKB(parent.orientationObjectId, parent.orientationValueId);
				  }
				  this.removeListener('load', f)
			  }
			  nav.on('load', f, nav)
		  }
		  navItems.push(nav);
	  }

	  var navPanel = new Ext.Panel({
		    region : 'west',
		    width : 200,
		    minSize : 175,
		    maxSize : 400,
		    split : true,
		    collapsible : true,
		    collapseMode : 'mini',
		    border : false,
		    style : 'border-right: 1px solid ' + sys_bdcolor,
		    layout : 'accordion',
		    title : '控制台',
		    items : navItems
	    });
	  Dashboard.navPanel = navPanel;

	  var viewport = new Ext.Viewport({
		    layout : 'border',
		    items : [navPanel, mainPanel]
	    });

	  mainPanel.on('tabchange', function(tabPanel, tab) {
		    if (!tab)
			    return;
		    var navIndex = tabRegistry[tab.id];
		    if (typeof navIndex == 'undefined')
			    return;
		    navPanel.layout.setActiveItem(navIndex);

	    })
	  mainPanel.on('beforetabchange', function(tabPanel, newtab, currenttab) {
		    if (currenttab == newtab)
			    return;
		    if (currenttab)
			    currenttab.get(0).fireEvent('tabinactive')
		    if (newtab)
			    newtab.get(0).fireEvent('tabactive')
	    })
  });