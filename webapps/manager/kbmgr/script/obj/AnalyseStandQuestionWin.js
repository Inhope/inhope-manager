AnalyseStandQuestionWin = function(_cfg) {
	var self = this;
	var _pageSize = 20;
	var _store = new Ext.data.Store({
		  proxy : new Ext.data.HttpProxy({
			    url : 'analyse-stand-question!list.action'
		    }),
		  reader : new Ext.data.JsonReader({
			    idProperty : 'id',
			    root : 'data',
			    fields : [{
				      name : 'question1',
				      type : 'string'
			      }, {
				      name : 'question2',
				      type : 'string'
			      }, {
				      name : 'similarity',
				      type : 'string'
			      }]
		    }),
		  writer : new Ext.data.JsonWriter()
	  });
	this.store = _store;
	var pagingBar = new Ext.PagingToolbar({
		  store : _store,
		  displayInfo : true,
		  pageSize : _pageSize,
		  prependButtons : true,
		  beforePageText : '第',
		  afterPageText : '页，共{0}页',
		  displayMsg : '第{0}到{1}条记录，分析结果共{2}条',
		  emptyMsg : "没有记录",
		  prependButtons : false
	  })
	var paramText = new Ext.form.TextField({
		  fieldLabel : '参数',
		  allowBlank : false,
		  name : 'param',
		  blankText : '参数不能为空',
		  anchor : '100%',
		  value : '0.95'
	  })
	this.paramText = paramText;
	var analyseStandQuestionGrid = Ext.create({
		  xtype : 'grid',
		  region : 'center',
		  border : false,
		  frame : false,
		  store : _store,
		  loadMask : true,
		  colModel : new Ext.grid.ColumnModel({
			    defaults : {
				    sortable : true
			    },
			    columns : [new Ext.grid.RowNumberer(), {
				      header : '内容1',
				      dataIndex : 'question1',
				      sortable : false
			      }, {
				      header : '内容2',
				      dataIndex : 'question2',
				      sortable : false
			      }, {
				      header : '相似度',
				      dataIndex : 'similarity',
				      hidden : true
			      }]
		    }),
		  tbar : [paramText, '-', {
			    text : '开始分析',
			    iconCls : 'icon-search',
			    handler : function() {
				    if (paramText.getValue()) {
					    self.startAnalyse();
				    } else {
					    Ext.Msg.alert('错误提示', '请输入你要分析的参数');
					    return;
				    }
			    }
		    }],
		  bbar : pagingBar,
		  viewConfig : {
			  forceFit : true
		  }
	  });
	analyseStandQuestionGrid.on("celldblclick", function(grid, rowIndex, columnIndex) {
		  var record = grid.getStore().getAt(rowIndex); // Get the Record
		  var fieldName = grid.getColumnModel().getDataIndex(columnIndex); // Get
		  var standQuestion = record.get(fieldName);
		  if (standQuestion) {
			  Ext.Ajax.request({
				    params : {
					    question : standQuestion
				    },
				    url : 'ontology-object-value!queryFaqQuestion.action',
				    success : function(response) {
					    var result = Ext.util.JSON.decode(response.responseText);
					    if (result.success) {
						    var obj = result.data;
						    if (obj.dimValueId && obj.objectId) {
							    Ext.Ajax.request({
								      url : 'ontology-object!getObjectById.action',
								      params : {
									      "objectId" : obj.objectId
								      },
								      success : function(form, action) {
									      var res = Ext.decode(form.responseText);
									      if (res.data) {
											      Ext.getCmp('objectCategoryNav').showObjectTab(res.data, obj.dimValueId);
									      } else
										      Dashboard.setAlert("没有找到对应的标准问题!", 3000);
								      },
								      failure : function(response) {
									      Ext.Msg.alert("错误", response.responseText);
								      }
							      })
						    }
					    }
				    },
				    failure : function() {
				    }
			    });
		  }
	  });
	this.pageSize = _pageSize;
	var cfg = {
		layout : 'fit',
		title : '分析标准问',
		width : 600,
		height : 500,
		defaults : {
			border : false
		},
		plain : true,// 方角 默认
		modal : false,
		shim : true,
		closeAction : 'hide',
		collapsible : true,
		closable : true, // 关闭
		resizable : true,// 改变大小
		constrainHeader : true,
		items : analyseStandQuestionGrid
	}
	AnalyseStandQuestionWin.superclass.constructor.call(this, Ext.applyIf(_cfg || {}, cfg));
}
Ext.extend(AnalyseStandQuestionWin, Ext.Window, {
	  loadData : function() {
		  var self = this;
		  this.store.load({
			    params : {
				    start : 0,
				    limit : self.pageSize
			    }
		    });
	  },
	  startAnalyse : function() {
		  var self = this;
		  var param = this.paramText.getValue();
		  this.getEl().mask('开始分析数据，请耐心等待...');
		  Ext.Ajax.request({
			    params : {
				    param : param
			    },
			    url : 'analyse-stand-question!startAnalyse.action',
			    success : function(response) {
				    self.getEl().unmask();
				    self.loadData();
			    },
			    failure : function() {
			    }
		    });
	  }
  })