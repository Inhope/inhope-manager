AnsEditorWin = Ext.extend(Ext.Window, {
	  p4EditUrl : '../kbmgr/anseditor/page/index.jsp',
	  initComponent : function() {
		  window.answerRendered = function(obj) {
			  this.hide()
			  this.fireEvent('materialSelected', obj)
		  }.dg(this)
		  this._id = Ext.id();
		  this.nameLabel = new Ext.create({xtype:'label',text:'名称:'})
		  this.tbar = [this.nameLabel, {
			    xtype : 'textfield',
			    name : 'name'
		    }, '&nbsp;&nbsp;编辑时间: 从', {
			    xtype : 'datetimefield1',
			    name : 'editTimeStart'
		    }, '&nbsp;到&nbsp;', {
			    xtype : 'datetimefield1',
			    name : 'editTimeEnd'
		    }, '-', {
			    name : 'search',
			    text : '搜索',
			    iconCls : 'icon-search',
			    handler : function() {
				    var name = this.getTopToolbar().find('name', 'name')[0].getValue();
				    var editTimeStart = this.getTopToolbar().find('name', 'editTimeStart')[0].getValue();
				    var editTimeEnd = this.getTopToolbar().find('name', 'editTimeEnd')[0].getValue();
				    editTimeStart ? editTimeStart = editTimeStart.format('Y-m-d H:i:s') : ''
				    editTimeEnd ? editTimeEnd = editTimeEnd.format('Y-m-d H:i:s') : ''
				    var url = ''
				    if (this.curframe.type != null) {// app
					    url = '../matmgr/app!listShow.action?type=' + this.curframe.type + '&'
				    } else {
					    url = '../matmgr/' + this.mtype.replace('msg', '') + '-msg!list.action?'
				    }
				    this.curframe
				      .location(url + 'name=' + encodeURIComponent(name) + '&editTimeStart=' + encodeURIComponent(editTimeStart) + '&editTimeEnd=' + encodeURIComponent(editTimeEnd))
			    }.dg(this)
		    }, '-', {
			    text : '编辑',
			    iconCls : 'icon-edit',
			    handler : function() {
				    getTopWindow().vtab.items.each(function(p) {
					      if (p.iconCls == 'top-module-matmgr') {
						      getTopWindow().vtab.setActiveTab(p)
						      return false;
					      }
				      })
				    setTimeout(function() {
					      if (this.mtype == 'app') {
						      ShortcutToolRegistry.execAction(!this.curframe.type ? 'appcustom' : 'app');
					      } else
						      ShortcutToolRegistry.execAction(this.mtype);
				      }.dg(this), 500)
			    }.dg(this)
		    }]
		  this.msgframe = new IFramePanel({
			    flex : 0
		    })
		  this.apptab = []
		  this.appTabPanel = new Ext.TabPanel({
			    activeTab : 0,
			    border : false,
			    style : 'border-left: 1px solid ' + sys_bdcolor,
			    resizeTabs : true,
			    tabWidth : 150,
			    minTabWidth : 120,
			    enableTabScroll : true,
			    layoutOnTabChange : true,
			    flex : 1
		    })
		  this.appTabPanel.on('tabchange', function(tabPanel, newtab, currenttab) {
			    try {
				    this.curframe = newtab.items.get(0);
				    this.curframe.location('../matmgr/app!listShow.action?&type=' + this.curframe.type)
			    } catch (e) {
			    }
		    }, this)
		  this.apptab[0] = this.appTabPanel.add({
			    layout : 'fit',
			    title : '内置应用',
			    items : new IFramePanel({
				      type : 0
			      }),
			    closable : false
		    });
		  this.apptab[1] = this.appTabPanel.add({
			    layout : 'fit',
			    title : '自定义应用',
			    items : new IFramePanel({
				      type : 1
			      }),
			    closable : false
		    });
		  // this.items = [this.appTabPanel, this.msgframe]
		  AnsEditorWin.superclass.initComponent.call(this)
		  this.getTopToolbar().items.each(function(item) {
			    if (item && item instanceof Ext.form.TextField)
				    item.on('specialkey', function(f, e) {
					      if (e.getKey() == e.ENTER) {
						      this.getTopToolbar().find('name', 'search')[0].handler();
					      }
				      }.dg(this));
		    }.dg(this))
	  },
	  loadData : function(type, id) {
		  id = id || ''
		  this.mtype = type
		  this.nameLabel.setText(type != 'imgtxtmsg'?'名称:':'标签:')
		  if (type == 'app') {
			  this.msgframe.hide()
			  this.add(this.appTabPanel)
			  var apptype = 0
			  if (id) {
				  var app = http_get('app!get.action?id=' + id)
				  app = Ext.decode(app)
				  if (app && app.type != null) {
					  apptype = app.type
				  }
			  }
			  var tab = this.appTabPanel.items.get(apptype)
			  this.appTabPanel.show()
			  this.appTabPanel.activate(tab)
			  this.doLayout();
			  this.curframe.location('../matmgr/app!listShow.action?materialId=' + id + '&type=' + apptype)
			  this.setTitle('应用选择')
		  } else {
			  this.appTabPanel.hide()
			  this.add(this.msgframe)
			  this.curframe = this.msgframe
			  this.msgframe.show()
			  this.doLayout();
			  this.curframe.location('../matmgr/' + type.replace('msg', '') + '-msg!list.action?materialId=' + id)
			  type == 'imagemsg' ? this.setTitle('应用选择') : 0
			  type == 'imgtxtmsg' ? this.setTitle('图文选择') : 0
			  type == 'imagemsg' ? this.setTitle('图片选择') : 0
			  type == 'audiomsg' ? this.setTitle('语音选择') : 0
			  type == 'videomsg' ? this.setTitle('视频选择') : 0
			  type == 'p4msg' ? this.setTitle('P4选择') : 0
		  }
	  },
	  layout : {
		  type : 'fit',
		  padding : '0',
		  align : 'stretch'
	  },
	  title : '素材选择',
	  width : 765,
	  height : 500,
	  defaults : {
		  border : false
	  },

	  plain : true,// 方角 默认
	  modal : true,
	  shim : true,
	  closeAction : 'hide',
	  closable : true, // 关闭
	  resizable : true,// 改变大小
	  constrainHeader : true
  })