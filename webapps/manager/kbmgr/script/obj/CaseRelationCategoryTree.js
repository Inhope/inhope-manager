CaseRelationCategoryTree = function(_cfg) {
	_cfg = _cfg || {}
	var self = this;
	this.data = {};
	var cfg = Ext.apply({
		  width : 200,
		  minSize : 175,
		  maxSize : 400,
		  xtype : 'treepanel',
		  rootVisible : false,
		  enableDD : true,
		  lines : false,
		  border : false,
		  autoScroll : true,
		  containerScroll : true,
		  root : {
			  lazyLoad : false,
			  id : '0',
			  text : 'root',
			  iconCls : 'icon-ontology-root',
		  },
		  dataUrl : 'ontology-category!list.action'
	  }, _cfg)
	cfg.id = Ext.id()
	var tbaritems = [{
		  ref : 'searchNodeText',
		  emptyText : "请输入实例名",
		  xtype : 'textfield',
		  width : 115,
		  listeners : {
			  specialkey : function(f, e) {
				  if (e.getKey() == e.ENTER) {
					  self.searchNode(f);
				  }
			  }
		  }
	  }, {
		  ref : 'searchNodeBtn',
		  text : '查找',
		  iconCls : 'icon-search',
		  width : 50,
		  xtype : 'splitbutton',
		  menu : {
			  items : [{
				    text : '精确匹配',
				    checked : false,
				    checkHandler : function(btn, checked) {
					    this.matchExactly = checked
				    }.dg(this)
			    }]
		  },
		  handler : function() {
			  self.searchNode();
		  }
	  }, {
		  text : '刷新',
		  iconCls : 'icon-refresh',
		  width : 50,
		  handler : function() {
			  self.refresh(self.getRootNode());
		  }
	  }]
	cfg.tbar = new Ext.Toolbar({
		  enableOverflow : true,
		  height : '20',
		  items : Dashboard.u().filterAllowed(tbaritems)
	  })
	CaseRelationCategoryTree.superclass.constructor.call(this, cfg);
	this.on('beforeload', function(node, event) {
		  this.getLoader().baseParams['ispub'] = this.isPub(node);
		  this.getLoader().baseParams['showVirtualDimCate'] = this.showVirtualDimCate;
	  });
	this.loader.on('loadexception', function(loader, node, response) {
		  Ext.Msg.alert('错误', '发生错误:' + response.responseText)
	  })
	if (cfg.compact)
		return
	var categoryMenu = new Ext.menu.Menu({
		  items : Dashboard.u().filterAllowed([{
			    text : '刷新',
			    iconCls : 'icon-refresh',
			    width : 50,
			    handler : function() {
				    self.refreshNode();
			    }
		    }, '-',{
			    ref : 'MN_PROP',
			    text : '属性',
			    authName : 'ALL',
			    iconCls : 'icon-view-switcher',
			    width : 50,
			    handler : function() {
				    if (categoryMenu.currentNode) {
					    var d = categoryMenu.currentNode.attributes
					    var s = '';
					    for (var k in d) {
						    s += k + ':' + d[k] + '<br>'
					    }
				    }
				    Ext.Msg.alert('属性', s)
			    }.dg(this)
		    }])
	  });

	var objectMenu = new Ext.menu.Menu({
		  items : Dashboard.u().filterAllowed([{
			    ref : 'MN_IMP',
			    text : '添加实例对比',
			    iconCls : 'icon-connect',
			    width : 50,
			    handler : function() {
				    this.doAddContrast(this.getSelectedNode());
			    }.dg(this)
		    }, {
			    ref : 'MN_EXP',
			    text : '添加实例关联',
			    iconCls : 'icon-connect',
			    width : 50,
			    handler : function() {
				    this.doAddRelevance(this.getSelectedNode())
			    }.dg(this)
		    }])
	  });

	this.categoryMenu = categoryMenu;
	this.objectMenu = objectMenu;
	this.toggleAuthedMenuItem = function(m, catenode) {
		var pnode = catenode;
		var catePath = ''
		do {
			catePath = pnode.attributes.id + '.' + catePath
		} while ((pnode = pnode.parentNode) && pnode.id != '0')
		if (catePath)
			catePath = catePath.slice(0, -1)
		for (var refName in m) {
			var item = m[refName];
			if (item && typeof item == 'object') {
				var authName = '';
				if (item.authName) {
					item.show();
					continue;
				} else if (item._authName) {
					authName = item._authName;
				} else if (refName.indexOf('MN_') == 0) {
					authName = refName.replace('MN_', '');
				}
				if (authName) {
					var lastIndex = authName.lastIndexOf('.');
					var perm = authName;
					if (lastIndex != -1) {
						perm = authName.substring(lastIndex + 1);
					}
					if (lastIndex != -1 && Dashboard.u().allow(authName) || Dashboard.u().allow('kb.obj.catecontent.' + catePath + '.' + perm)) {
						item.show()
					} else if (typeof item.hide == 'function')
						item.hide()
				} else if (typeof item.show == 'function') {
					// item.show()
				}
			}
		}
	}
	this.on('contextmenu', function(node, event) {
		  event.preventDefault();
		  node.select();
		  var menu;
		  var catenode;
		  if (node.id.indexOf('@') != -1) {
			  return false;
		  } else if (node.attributes.iconCls == 'icon-ontology-object') {
			  menu = objectMenu;
			  catenode = node.parentNode
		  }
		  this.toggleAuthedMenuItem(menu, catenode);
		  if (menu != null && menu.items && menu.items.getCount() > 0) {
			  menu.currentNode = node;
			  menu.showAt(event.getXY());
			  menu.items.each(function(item, i, len) {
				    if (item.isVisible()) {
					    return false;
				    }
				    if (i == len - 1) {
					    menu.hide()
				    }
			    })
		  }
	});
	this.on('click', function(node, event) {
		node.select();
	});
	var _treeCombo = new TreeComboBox({
		  fieldLabel : '父级分类',
		  emptyText : '请选择父级分类...',
		  editable : false,
		  hiddenName : 'parentId',
		  valueField : TreeComboBox.FIELD_PATH,
		  anchor : '95%',
		  allowBlank : true,
		  minHeight : 250,
		  root : {
			  id : '0',
			  text : 'root',
			  iconCls : 'icon-ontology-root'
		  },
		  loader : new Ext.tree.TreeLoader({
			    dataUrl : 'ontology-category!list.action'
		    })
	  });
	var classSelected = new TreeComboCheck({
		  ref : 'classesField',
		  dataUrl : 'ontology-class!list.action?listClassOnly=true',
		  name : 'classes',
		  anchor : '100%',
		  fieldLabel : '默认继承类',
		  treeConfig : {
			  tbar : [{
				    text : '清空',
				    iconCls : 'icon-refresh',
				    handler : function() {
					    classSelected.setValueEx();
				    }
			    }, {
				    text : '刷新',
				    iconCls : 'icon-refresh',
				    handler : function() {
					    classSelected.reload();
				    }
			    }]
		  }
	  });
	var _formPanel = new Ext.form.FormPanel({
		  frame : false,
		  border : false,
		  labelWidth : 100,
		  // autoHeight : true,
		  waitMsgTarget : true,
		  bodyStyle : 'padding : 3px 3px',
		  defaults : {
			  blankText : '',
			  invalidText : '',
			  anchor : '100%',
			  xtype : 'textfield'
		  },
		  formRecordType : Ext.data.Record.create(['id', 'parentId', 'name', 'moduleId', 'tagIds', 'classes', 'tag', 'orderNo', 'ispub']),
		  loadData : function(d) {
			  var moduleIdField = this.find('hiddenName', 'moduleId')[0];
			  d.parentId.id != '0' ? moduleIdField.hideField() : moduleIdField.showField()
			  // var classesField = this.find('name', 'classes')[0];
			  // var tagIdsField = this.find('name', 'tagIds')[0];
			  // if (d.ispub) {
			  // tagIdsField.hideField(), classesField.hideField();
			  // } else {
			  // tagIdsField.showField(), classesField.showField();
			  // }
			  this.getForm().loadRecord(new this.formRecordType(d))
		  },
		  items : [new Ext.form.Hidden({
			      name : 'id'
		      }), _treeCombo, {
			    fieldLabel : '类型名称',
			    name : 'name',
			    allowBlank : false
		    }, new Ext.form.ComboBox({
			      allowBlank : true,
			      store : new Ext.data.JsonStore({
				        autoDestroy : true,
				        autoLoad : true,
				        url : 'module-config!list.action',
				        root : 'data',
				        fields : ['moduleId', 'name', 'nodeId'],
				        listeners : {
					        load : function(store) {
						        var rsToRemove = []
						        store.findBy(function(r) {
							          if ((r.get('moduleId') && r.get('moduleId').indexOf('common') == 0) || r.get('nodeId')) {
								          rsToRemove.push(r)
							          }
						          });
						        Ext.each(rsToRemove, function(r) {
							          store.remove(r)
						          })
					        }
				        }
			        }),
			      hiddenName : 'moduleId',
			      triggerAction : 'all',
			      editable : false,
			      fieldLabel : '所属模块',
			      valueField : 'moduleId',
			      displayField : 'name'
		      }), new DimensionBox({
			      fieldLabel : '默认维度',
			      name : 'tagIds'
		      }), classSelected, {
			    fieldLabel : '默认标签',
			    name : 'tag'
		    }, new Ext.form.NumberField({
			      allowDecimals : false,
			      fieldLabel : '类别排序',
			      invalidText : '只能是整数',
			      name : 'orderNo'
		      }), {
		      	xtype:'textfield',
			    fieldLabel : '虚拟维度目录',
			    name : 'bizDimTags',
			    emptyText : '请填写维度标签的名字，英文逗号间隔',
			    allowBlank : true
		    }, new PubSchemaSelector()],
	  });
	var _formWin = new Ext.Window({
		  width : 495,
		  height : 320,
		  layout : 'fit',
		  title : '类型管理',
		  defaults : {
			  border : false
		  },
		  modal : true,
		  plain : true,
		  shim : true,
		  closable : true,
		  closeAction : 'hide',
		  collapsible : true,
		  draggable : true,
		  animCollapse : true,
		  constrainHeader : true,
		  items : [_formPanel],
		  listeners : {
			  beforehide : function() {
				  _formPanel.getForm().reset();
			  }
		  }
	  });
	_formWin.formPanel = _formPanel;
	this.formWin = _formWin;

	this.on('click', function(n) {
		  if (!n)
			  return false;
		  if (n.attributes.objectId) {
			  this.showObjectTab(n)
		  } else {
			  this.showObjectListTab(n)
		  }
	  }, this)
	this.on('load', function(node, refNode) {
		  if (node.childNodes) {
			  Ext.each(node.childNodes, function(n) {
				    if (n.attributes.objectId) {
					    n.draggable = true
				    }
			    })
		  }
	  });
}

Ext.extend(CaseRelationCategoryTree, Ext.tree.TreePanel, {
	  authName : 'kb.obj',
	  updateAttr : function(data, cateId, tabId) {
		  var node = this.getNodeById(data.id);
		  if (node) {
			  node.setText(data.text);
		  } else {
			  var parentNode = this.getNodeById(cateId);
			  node = new Ext.tree.TreeNode(data);
			  if (tabId)
				  node.tabId = tabId;
			  if (parentNode)
				  parentNode.appendChild(node);
		  }
		  this.showTab(obj.detail.MainPanel, (node.tabId ? node.tabId : 'object-tab-' + node.id), node.text, 'icon-ontology-object', true);
	  },
	  showObjectListTab : function(n) {
		  var self = this;
		  var listPanel = this.showTab(obj.list.MainPanel, 'object-tab-', "实例查询", 'icon-ontology-cate', true);
		  var cateIdPath = n.cateIdPath;
		  var targetId = n.id;
		  listPanel.tab.on('activate', function() {
			    self.expandPath('/0' + cateIdPath, '', function(suc, n) {
				      self.getNodeById(targetId) ? self.getNodeById(targetId).select() : 0;
			      });
		    });
		  listPanel.categoryChanged(this.getCateId(n), n.attributes.bh, this.isPub(n), {
			    dimtagid : this.getDimtagid(n)
		    })
	  },
	  showObjectTab : function(n, dimValueId) {
		  if (this.simpleMode) {
			  var objectPanel = this.showTab(IFramePanel, 'object-tab-s-' + n.id, '编辑业务::' + n.text, 'icon-ontology-object', true);
			  objectPanel.setLocation(String.format('ontology-object!editByBizTemplate.action?data={0}', encodeURIComponent(Ext.encode({
				      objectId : n.id
			      }))));
		  } else {
			  var path;
			  if (n.parentNode) {
				  path = this.getAuthPath(n.parentNode);
			  } else if (n.cateIdPath) {
				  // direct access,not from nav tree,/1/2/3
				  path = n.cateIdPath.substring(1).replace(/\//g, '.')
			  } else {
				  throw new Error('no category id path,objectId=' + n.id)
			  }
			  if (!(Dashboard.u().allow('kb.obj.instance.M') || Dashboard.u().allow('kb.obj.catecontent.' + path + '.M')))
				  return;
			  //this.showObjectTab0(n, dimValueId);
		  }
	  },
	  showObjectTab0 : function(n, dimValueId) {
		  var self = this;
		  var objectPanel = this.showTab(obj.detail.MainPanel, (n.tabId ? n.tabId : 'object-tab-' + n.id), n.text, 'icon-ontology-object', true);
		  var cateIdPath = n.cateIdPath;
		  var targetId = n.id;
		  objectPanel.tab.on('activate', function() {
			    self.expandPath('/0' + cateIdPath, '', function(suc, n) {
				      self.getNodeById(targetId) ? self.getNodeById(targetId).select() : 0;
			      });
		    });
		  if (cateIdPath) {
			  self.expandPath('/0' + cateIdPath, '', function(suc, n) {
				    self.getNodeById(targetId) ? self.getNodeById(targetId).select() : 0;
			    });
		  }
		  objectPanel.objectChanged(new obj.detail.ObjectType({
			      objectId : n.id ? n.id : null
		      }), null, dimValueId, n.ispub != null ? n.ispub : this.isPub(n))
	  },
	  isPub : function(n) {
		  if (!n.attributes)
			  return n.ispub;
		  while (n.parentNode != null && n.parentNode != this.getRootNode()) {
			  n = n.parentNode
		  }
		  return n.attributes.ispub;
	  },
	  modifyInternal : function(d, parentNodeData, enableSchemaSelector) {
		  this.formWin.show();
		  if (!parentNodeData.path) {
			  parentNodeData.path = '/';
		  }
		  d.parentId = parentNodeData
		  this.formWin.formPanel.loadData(d)
		  var pss = this.formWin.formPanel.find('hiddenName', 'ispub')[0]
		  pss[Dashboard.u().allow('ALL') ? 'show' : 'hide']()
		  pss.setReadOnly(enableSchemaSelector ? false : true)
	  },
	  refresh : function(node) {
		  if (node.isExpanded())
			  node.reload();
		  else
			  node.expand();

	  },
	  getAuthPath : function(node) {
		  if (!node)
			  node = this.getSelectedNode()
		  return node.getPath().replace('/0/', '').replace(/\//ig, '.');
	  },
	  getSelectedNode : function() {
		  return this.getSelectionModel().getSelectedNode();
	  },
	  setNodeIconCls : function(node, clsName) {
		  var iel = node.getUI().getIconEl();
		  if (iel) {
			  var el = Ext.get(iel);
			  if (el) {
				  if (clsName.indexOf('-root') > 0) {
					  el.removeClass('icon-ontology-root');
					  el.removeClass('icon-ontology-root-add');
				  } else if (clsName.indexOf('-object') > 0) {
					  el.removeClass('icon-ontology-object');
				  }
				  el.addClass(clsName);
			  }
		  }
	  },
	  searchNode : function(focusTarget) {
		  var categoryName = this.getTopToolbar().searchNodeText.getValue();
		  if (!categoryName || !(categoryName = categoryName.trim())) {
			  Ext.Msg.alert('提示', '请输入分类名称');
			  return;
		  }
		  if (!this.getSelectionModel().getSelectedNode()) {
			  this.getRootNode().childNodes[0].select();
		  }
		  var self = this;
		  Ext.Ajax.request({
			    url : 'ontology-category!search.action',
			    params : {
				    categoryName : categoryName,
				    ispub : this.isPub(this.getSelectedNode()),
				    matchExactly : this.matchExactly ? true : false
			    },
			    success : function(form, action) {
				    if (!form.responseText) {
					    Dashboard.setAlert('没有查找到更多的"' + categoryName + '"，请修改后重新查找');
					    return;
				    }
				    var idPath = Ext.util.JSON.decode(form.responseText);
				    if (idPath) {
					    var targetId = idPath.pop();
					    self.expandPath('/0/' + idPath.join('/'), '', function(suc, n) {
						      self.getNodeById(targetId).select();
						      if (focusTarget)
							      focusTarget.focus()
					      });
				    }
			    },
			    failure : function(form, action) {
				    Ext.Msg.alert('提示', '查找错误');
			    }
		    });
	  },
	  refreshNode : function() {
		  var node = this.getSelectedNode();
		  if (!node)
			  this.refresh(this.getRootNode());
		  else {
			  this.refresh(node);
		  }
	  },
	  doAddContrast : function(node){
		  var w = this.ownerCt.ownerCt;
		  if (w) {
			  var store = w.gridContrast.getStore();
			  var find = false;
			  for(var i = 0;i < store.getCount();i++){
				  var item = store.getAt(i);
				  if(item.data.id == node.id){
					  find = true;
					  break;
				  }
			  }
			  if(!find){
				  w.gridContrast.getStore().add(new Ext.data.Record({
					  id: node.id, 
					  name: node.text,
					  type:'2'
				  }));
			  }
		  }
	  },
	  doAddRelevance : function(node){
		  var w = this.ownerCt.ownerCt;
		  if (w) {
			  var store = w.gridRelevance.getStore();
			  var find = false;
			  for(var i = 0;i < store.getCount();i++){
				  var item = store.getAt(i);
				  if(item.data.id == node.id){
					  find = true;
					  break;
				  }
			  }
			  if(!find){
				  w.gridRelevance.getStore().add(new Ext.data.Record({
					  id: node.id, 
					  name: node.text,
					  type:'1'
				  }));
			  }
		  }
	  },
	  // compact mode
	  switchMode : function() {
		  this.title = this.title.replace(/\(.*\)/, '');
		  if (this.simpleMode) {
			  delete this.simpleMode
			  Ext.QuickTips.register({
				    target : this.tools.gear,
				    text : '经典模式'
			    })
			  this.setTitle(this.title + '(经典模式)')
		  } else {
			  this.simpleMode = true;
			  Ext.QuickTips.register({
				    target : this.tools.gear,
				    text : '业务模式'
			    })
			  this.setTitle(this.title + '(业务模式)')
		  }
		  this.doLayout()
	  },
	  getDimtagid : function(node) {
		  return node.id.indexOf('@') != -1 ? node.id.split('@')[1] : ''
	  },
	  getCateId : function(node) {
		  return node.id.indexOf('@') != -1 ? node.id.split('@')[0] : node.id
	  }
  });
