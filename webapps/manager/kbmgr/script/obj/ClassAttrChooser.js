var ClassAttributeChooserMenu = Ext.extend(Ext.menu.Menu, {
			initComponent : function(cfg) {
				cfg = cfg || {}
				this.combo = new OntologyNavPanel(Ext.apply({
							root : {
								id : '0',
								text : 'root',
								iconCls : 'icon-ontology-root'
							},
							height : 300,
							header : false,
							noClassDiagram:true,
							tbar_disableOverflow : true,
							tbar_searchTextWidth : 80,
							tbar_items : {
								text : '自定义',
								tooltip : '转换为自定义属性',
								handler : function() {
									this.fireEvent('attributeChanged', null);
									this.hide()
								}.dg(this)
							},
							compact : true
						}, cfg.treePanelConfig || {}));
				this.combo.on('click', function(node) {
							if (this.combo.isAttr(node)) {
								var attachment = node.attributes.attachment;
								var attr = {
									attributeName : attachment.name,
									attributeId : attachment.attributeId,
									attributeType : attachment.type,
									attributeClassId : node.parentNode.attributes.bh,
									attributeClassBH : node.parentNode.id,
									attributeStdRule : attachment.standardRule
								}
								this.fireEvent('attributeChanged', attr);
								this.hide();
							}
						}, this);
				var self = this;
				var items = [this.combo]
				this.addEvents('attributeChanged');
				Ext.apply(this, {
							style : {
								overflow : 'visible'
							},
							items : items
						})
				Ext.apply(this, cfg)
				Ext.QuickTips.init();
				return ClassAttributeChooserMenu.superclass.initComponent
						.call(this)

			},
			style : 'padding:0',
			layout : 'fit',
			width : 250,
			height : 310
		});