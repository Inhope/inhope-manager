var CodeTwinTriggerDateField = Ext.extend(TwinTriggerDateField, {
			initComponent : function() {
				this.on('focus', function() {
							this.config.parentMenu.__noBlur = true;
						}, this);
				this.on('select', function(th, date) {
							this.config.parentMenu.__noBlur = false;
						}, this);
				CodeTwinTriggerDateField.superclass.initComponent.call(this);
			}
		});