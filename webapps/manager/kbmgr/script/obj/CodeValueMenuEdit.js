var CodeValueMenuConstructor = function(_cfg) {
	this._cfg = _cfg;
	var cfg = {};

	var self = this;
	this.primordialEdit = false;
	this.addMultiConditionIndex = 0;
	this.elseifSize = 1;

	this.initDefaultField = function(defaultConditionValue) {
		this.elseifSize = 1;
		var items = this.initGroup();
		if (items) {
			for (var i = 0; i < items.length; i++) {
				this.insert(i, items[i]);
			}
		}
		this.fieldCompons = [];
		var childItems = this.items;
		for (var i = 0; i < childItems.length; i++) {
			childItems.items[i].index = i + 1;
			if (i != childItems.length - 1)
				this.fieldCompons.push(childItems.items[i]);
		}
		this.doLayout();
		return items;
	}
	this.addMultiConditionGroup = function(initPostion, defaultValue,
			defaultLogicOperator, defaultRelationalOperator,
			defaultConditionValue) {
		var items = this.renderMultiConditionGroup();
		for (var i = 0; i < items.length; i++) {
			// var index = (initPostion + 1 ? initPostion : 1);
			this.insert(initPostion, items[i]);
			items[i].index = initPostion + 1;
		}
		if (defaultValue) {
			items[0].operatorValue.setValue(defaultValue);
			if (!('@人民币' == defaultValue || "@时间" == defaultValue
					|| "@数字" == defaultValue || "newDate" == defaultValue)) {
				items[0].relationalOperator.store.removeAll(true);
				items[0].relationalOperator.store.loadData([['match', '匹配'],
								['unmatch', '未匹配']], false);
			}
		}
		if (defaultLogicOperator)
			items[0].logicOperator.setValue(defaultLogicOperator);
		if (defaultRelationalOperator)
			items[0].relationalOperator.setValue(defaultRelationalOperator);

		if ('@时间' == items[0].operatorValue.getValue()
				|| "newDate" == items[0].operatorValue.getValue()) {
			if (defaultConditionValue) {
				if ('@时间' == items[0].operatorValue.getValue())
					items[0].conditionDateValue.setValue(Date.parseDate(
							defaultConditionValue, 'YmdHis'));
				else if ("newDate" == items[0].operatorValue.getValue()) {
					var date = new Date();
					date.setTime(defaultConditionValue);
					items[0].conditionDateValue.setValue(date);
				}
			}
			items[0].conditionValue.hide();
			items[0].conditionDateValue.show();
		} else {
			if (defaultConditionValue)
				items[0].conditionValue.setValue(defaultConditionValue);
			items[0].conditionValue.show();
			items[0].conditionDateValue.hide();
		}
		this.fieldCompons = [];
		var childItems = this.items;
		for (var i = 0; i < childItems.length; i++) {
			childItems.items[i].index = i + 1;
			if (i != childItems.length - 1)
				this.fieldCompons.push(childItems.items[i]);
		}
		this.doLayout();
		return items;
	}
	this.addGroup = function(defaultValue, defaultRelationalOperator,
			defaultConditionValue) {
		var items = this.renderGroup();
		for (var i = 0; i < items.length; i++) {
			this.insert(this.items.length - 2, items[i]);
			items[i].index = this.items.length - 2;
		}
		if (defaultValue) {
			items[0].operatorValue.setValue(defaultValue);
			if (!('@人民币' == defaultValue || "@时间" == defaultValue
					|| "@数字" == defaultValue || "newDate" == defaultValue)) {
				items[0].relationalOperator.store.removeAll(true);
				items[0].relationalOperator.store.loadData([['match', '匹配'],
								['unmatch', '未匹配']], false);
			}
		}
		if (defaultRelationalOperator)
			items[0].relationalOperator.setValue(defaultRelationalOperator);
		if ('@时间' == items[0].operatorValue.getValue()
				|| "newDate" == items[0].operatorValue.getValue()) {
			if (defaultConditionValue) {
				if ('@时间' == items[0].operatorValue.getValue())
					items[0].conditionDateValue.setValue(Date.parseDate(
							defaultConditionValue, 'YmdHis'));
				else if ("newDate" == items[0].operatorValue.getValue()) {
					var date = new Date();
					date.setTime(defaultConditionValue);
					items[0].conditionDateValue.setValue(date);
				}
			}

			items[0].conditionValue.hide();
			items[0].conditionDateValue.show();
		} else {
			if (defaultConditionValue)
				items[0].conditionValue.setValue(defaultConditionValue);
			items[0].conditionValue.show();
			items[0].conditionDateValue.hide();
		}
		this.fieldCompons = [];
		var childItems = this.items;
		for (var i = 0; i < childItems.length; i++) {
			childItems.items[i].index = i + 1;
			if (i != childItems.length - 1)
				this.fieldCompons.push(childItems.items[i]);
		}
		this.doLayout();
		return items;
	}
	var saveP4CB = function(id) {
		this.setValue(id)
	}
	this.newMultiConditionField = function() {
		var self = this;
		var multiConditionElement = Ext.create({
			xtype : 'form',
			border : false,
			name : 'multiConditionEle',
			bodyStyle : 'margin-left:18px;',
			items : [{
				layout : 'column',
				border : false,
				bodyStyle : 'background-color:#f0f0f0;',
				items : [{
							columnWidth : .082,
							border : false,
							layout : 'fit',
							items : [{
										xtype : 'combo',
										ref : '../../logicOperator',
										valueField : 'id',
										displayField : 'text',
										mode : 'local',
										editable : false,
										triggerAction : 'all',
										value : '&&',
										store : new Ext.data.SimpleStore({
													fields : ["id", "text"],
													data : [['&&', '且'],
															['||', '或']]
												}),
										getListParent : function() {
											return this.el.up('.x-menu');
										}
									}]
						}, {
							columnWidth : .264,
							border : false,
							layout : 'fit',
							items : [{
								xtype : 'combo',
								ref : '../../operatorValue',
								valueField : 'id',
								displayField : 'text',
								mode : 'local',
								editable : false,
								triggerAction : 'all',
								value : 'newDate',
								store : new Ext.data.SimpleStore({
											fields : ["id", "text"],
											data : [['newDate', '访问时间'],
													['word', '句中词语'],
													['@人民币', '@人民币(问句中)'],
													['@时间', '@时间(问句中)'],
													['@数字', '@数字(问句中)']]
										}),
								getListParent : function() {
									return this.el.up('.x-menu');
								},
								listeners : {
									select : function(combo) {
										var value = combo.getValue();
										multiConditionElement.relationalOperator.store
												.removeAll(true);
										if ('word' == value) {
											multiConditionElement.relationalOperator.store
													.loadData(
															[
																	['match',
																			'匹配'],
																	['unmatch',
																			'未匹配']],
															false);
											multiConditionElement.relationalOperator
													.setValue('match');
										} else {
											multiConditionElement.relationalOperator.store
													.loadData(
															[
																	['>', '大于'],
																	['<', '小于'],
																	['>=',
																			'大于等于'],
																	['<=',
																			'小于等于'],
																	['==', '等于']],
															false);
											multiConditionElement.relationalOperator
													.setValue('>');
										}
										if ('@时间' == value
												|| 'newDate' == value) {
											multiConditionElement.conditionValue
													.hide();
											multiConditionElement.conditionDateValue
													.show();
										} else {
											multiConditionElement.conditionValue
													.show();
											multiConditionElement.conditionDateValue
													.hide();
										}
									}
								}
							}]
						}, {
							columnWidth : .178,
							border : false,
							layout : 'fit',
							items : [{
								xtype : 'combo',
								ref : '../../relationalOperator',
								valueField : 'id',
								displayField : 'text',
								mode : 'local',
								editable : false,
								triggerAction : 'all',
								value : '>',
								store : new Ext.data.SimpleStore({
											fields : ["id", "text"],
											data : [['>', '大于'], ['<', '小于'],
													['>=', '大于等于'],
													['<=', '小于等于'],
													['==', '等于']]
										}),
								getListParent : function() {
									return this.el.up('.x-menu');
								}
							}]
						}, {
							columnWidth : .388,
							border : false,
							layout : 'fit',
							items : [{
								xtype : 'trigger',
								ref : '../../conditionValue',
								hidden : true,
								fieldType : 'textfield',
								name : name,
								triggerClass : 'x-form-clear-trigger',
								listeners : {
									render : function(c) {
										Ext.QuickTips.register({
													target : this.trigger,
													text : "删除多条件"
												});
									}
								},
								onTriggerClick : function() {
									self.fieldCompons.splice(
											multiConditionElement.index, 1);
									self.remove(multiConditionElement);
									self.doLayout();
								}
							}, new CodeTwinTriggerDateField({
										trigger1Class : 'x-form-clear-trigger',
										ref : '../../conditionDateValue',
										onTrigger1Click : function() {
											self.fieldCompons
													.splice(
															multiConditionElement.index,
															1);
											self.remove(multiConditionElement);
											self.doLayout();
										},
										anchor : '100%',
										width : 193,
										config : {
											parentMenu : self
										}
									})]
						}]
			}]
		});
		return [multiConditionElement];
	}
	this.newElseifField = function() {
		var self = this;
		self.elseifSize++;
		var elseifElement = Ext.create({
			xtype : 'form',
			border : false,
			name : 'elseifConditionEle',
			fieldLabel : '条件' + self.elseifSize,
			items : [{
				layout : 'column',
				border : false,
				bodyStyle : 'background-color:#f0f0f0;',
				items : [{
					columnWidth : .3,
					border : false,
					layout : 'fit',
					items : [{
						xtype : 'combo',
						ref : '../../operatorValue',
						valueField : 'id',
						displayField : 'text',
						mode : 'local',
						editable : false,
						triggerAction : 'all',
						value : 'newDate',
						store : new Ext.data.SimpleStore({
									fields : ["id", "text"],
									data : [['newDate', '访问时间'],
											['word', '句中词语'],
											['@人民币', '@人民币(问句中)'],
											['@时间', '@时间(问句中)'],
											['@数字', '@数字(问句中)']]
								}),
						getListParent : function() {
							return this.el.up('.x-menu');
						},
						listeners : {
							select : function(combo) {
								var value = combo.getValue();
								elseifElement.relationalOperator.store
										.removeAll(true);
								if ('word' == value) {
									elseifElement.relationalOperator.store
											.loadData(
													[['match', '匹配'],
															['unmatch', '未匹配']],
													false);
									elseifElement.relationalOperator
											.setValue('match');
								} else {
									elseifElement.relationalOperator.store
											.loadData([['>', '大于'],
															['<', '小于'],
															['>=', '大于等于'],
															['<=', '小于等于'],
															['==', '等于']],
													false);
									elseifElement.relationalOperator
											.setValue('>');
								}
								if ('@时间' == value || 'newDate' == value) {
									elseifElement.conditionValue.hide();
									elseifElement.conditionDateValue.show();
								} else {
									elseifElement.conditionValue.show();
									elseifElement.conditionDateValue.hide();
								}
							}
						}
					}]
				}, {
					columnWidth : .2,
					border : false,
					layout : 'fit',
					items : [{
						xtype : 'combo',
						ref : '../../relationalOperator',
						valueField : 'id',
						displayField : 'text',
						mode : 'local',
						editable : false,
						triggerAction : 'all',
						value : '>',
						store : new Ext.data.SimpleStore({
									fields : ["id", "text"],
									data : [['>', '大于'], ['<', '小于'],
											['>=', '大于等于'], ['<=', '小于等于'],
											['==', '等于']]
								}),
						getListParent : function() {
							return this.el.up('.x-menu');
						}
					}]
				}, {
					columnWidth : .484,
					border : false,
					layout : 'fit',
					items : [new TwinTriggerCombo({
								trigger1Class : 'x-form-add-trigger',
								ref : '../../conditionValue',
								hidden : true,
								onTrigger1Click : function() {
									self.addMultiConditionGroup(
											elseifElement.index,
											elseifElement.operatorValue
													.getValue());
								},
								trigger2Class : 'x-form-clear-trigger',
								onTrigger2Click : function() {
									self.fieldCompons.splice(
											elseifElement.index, 1);
									self.fieldCompons.splice(
											elseifContentField.index, 1);
									self.remove(elseifElement);
									self.remove(elseifContentField);
									self.doLayout();
								},
								fieldType : 'textfield',
								anchor : '100%',
								store : [],
								doQuery : function() {
								}
							}), new CodeTwinTriggerDateField({
								trigger1Class : 'x-form-add-trigger',
								ref : '../../conditionDateValue',
								onTrigger1Click : function() {
									self.addMultiConditionGroup(
											elseifElement.index,
											elseifElement.operatorValue
													.getValue());
								},
								trigger2Class : 'x-form-clear-trigger',
								onTrigger2Click : function() {
									self.fieldCompons.splice(
											elseifContentField.index, 1);
									self.fieldCompons.splice(
											elseifElement.index, 1);
									self.remove(elseifElement);
									self.remove(elseifContentField);
								},
								anchor : '100%',
								width : 193,
								config : {
									parentMenu : self
								}
							})]
				}]
			}]
		});

		var elseifContentField = new Ext.form.TextArea({
					name : 'elseifContent',
					height : 40,
					width : 400,
					listeners : {
						afterrender : function() {
							var me = this;
							me.el.swallowEvent(['keypress', 'keydown']);
						}
					}
				});
		return [elseifElement, elseifContentField];
	}
	this.init = function() {
		this.renderUI();
		this.fireEvent('initcomplete');
	};

	this.BUTTONS = {
		OK : {
			xtype : 'button',
			text : '确定',
			colspan : 3,
			width : 50,
			handler : function() {
				var values = "";
				var hasMultiCondtion = false;
				for (var i = 0; i < self.fieldCompons.length; i++) {
					var eleConpont = self.fieldCompons[i];
					var operatorValue;
					var relationalOperator;
					var conditionValue;
					var conditionDateValue;
					var logicOperator;
					if ('ifConditionEle' == eleConpont.name
							|| 'multiConditionEle' == eleConpont.name
							|| 'elseifConditionEle' == eleConpont.name) {
						if ('multiConditionEle' == eleConpont.name)
							logicOperator = eleConpont.logicOperator.getValue();
						operatorValue = eleConpont.operatorValue.getValue();
						relationalOperator = eleConpont.relationalOperator
								.getValue();
						conditionValue = eleConpont.conditionValue.getValue();
						conditionDateValue = eleConpont.conditionDateValue
								.getValue();
					}
					if ('ifConditionEle' == eleConpont.name) {
						values += "#if ";
						if ('multiConditionEle' == self.fieldCompons[i + 1].name)
							hasMultiCondtion = true;
						if (operatorValue && relationalOperator
								&& (conditionValue || conditionDateValue)) {
							values += hasMultiCondtion ? "(" : "";
							values += self.assembleCondtion(operatorValue,
									relationalOperator, conditionValue,
									conditionDateValue);
							values += (hasMultiCondtion ? ")" : "") + "\n";
						} else {
							Dashboard.setAlert('提示:if判断条件不完整！', true, 5);
							return;
						}
					} else if ('multiConditionEle' == eleConpont.name) {
						if ('elseContent' == self.fieldCompons[i + 1].name) {
							Dashboard.setAlert('提示:请删除无效的多条件判断！', true, 8);
							return;
						}
						if (operatorValue && relationalOperator
								&& (conditionValue || conditionDateValue)) {
							values = values.replace(/\r\n|\r/g,"\n");
							values = values.substring(values.length-1)=="\n"?values.substring(0, values.length-1):values;
							values += logicOperator + "(";
							values += self.assembleCondtion(operatorValue,
									relationalOperator, conditionValue,
									conditionDateValue);
							values += ")\n";
							if (hasMultiCondtion
									&& ('elseifContent' == self.fieldCompons[i
											+ 1].name || 'ifContent' == self.fieldCompons[i
											+ 1].name)) {
								// values = values.substring(0, values.length -
								// 4);
								// values += ")\n";
								hasMultiCondtion = false;
							}
						} else {
							Dashboard.setAlert('提示:判断多条件不完整！', true, 5);
							return;
						}
					} else if ('ifContent' == eleConpont.name) {
						if (eleConpont.getValue()) {
							values += eleConpont.getValue().replace(/\r\n|\r|\n/g,"\n") + "\n";
						} else {
							Dashboard.setAlert('提示:if条件内容不能为空！', true, 5);
							return;
						}
					} else if ('elseifConditionEle' == eleConpont.name) {
						values += "#elseif ";
						if ('multiConditionEle' == self.fieldCompons[i + 1].name)
							hasMultiCondtion = true;
						if (operatorValue && relationalOperator
								&& (conditionValue || conditionDateValue)) {
							values += hasMultiCondtion ? "(" : "";
							values += self.assembleCondtion(operatorValue,
									relationalOperator, conditionValue,
									conditionDateValue);
							values += (hasMultiCondtion ? ")" : "") + "\n";
						} else {
							Dashboard.setAlert('提示:elseif判断条件不完整！', true, 5);
							return;
						}
					} else if ('elseifContent' == eleConpont.name) {
						if (eleConpont.getValue()) {
							values += eleConpont.getValue().replace(/\r\n|\r|\n/g,"\n")  + "\n";
						} else {
							Dashboard.setAlert('提示:elseif条件内容不能为空！', true, 5);
							return;
						}
					} else if ('elseContent' == eleConpont.name) {
						if (eleConpont.getValue())
							values += "#else \n" + eleConpont.getValue().replace(/\r\n|\r|\n/g,"\n");
					} else {
						Dashboard.setAlert('提示:请检查格式是否有效！', true, 8);
						return;
					}
				}
				self.codeValues = values;
				self.fireEvent('codeChanged')
				self.hide();
			}
		},
		CANCEL : {
			xtype : 'button',
			text : '取消',
			colspan : 3,
			width : 50,
			handler : function() {
				self.hide();
			}
		},
		YES : {
			xtype : 'button',
			text : '自定义',
			colspan : 3,
			width : 50,
			handler : function() {
				_cfg.parent.startEditing(_cfg.row, _cfg.col);
				self.hide();
			}

		}
	}
	this.renderUI = function() {
		var buttons = [];
		if (cfg.buttonsToAdd) {
			buttons = buttons.concat(cfg.buttonsToAdd);
		} else {
			buttons = [this.BUTTONS.OK, this.BUTTONS.CANCEL, this.BUTTONS.YES];
		}
		Ext.each(buttons, function(b) {
					if (!b.width) {
						b.width = 50
					}
				})
		var items = [];
		var buttonGroup = {
			xtype : 'buttongroup',
			layout : 'hbox',
			border : false,
			frame : false,
			defaults : {
				xtype : 'label'
			},
			items : [{
						xtype : 'spacer',
						flex : 1
					}].concat(buttons)
		}
		this.fieldCompons = [];
		items.push(buttonGroup);
		self.add(items);
		self.doLayout();
	}
	this.initGroup = function() {
		var ifFieldEle = Ext.create({
			xtype : 'form',
			border : false,
			name : 'ifConditionEle',
			fieldLabel : '条件1',
			items : [{
				layout : 'column',
				border : false,
				items : [{
					columnWidth : .3,
					border : false,
					layout : 'fit',
					items : [{
						xtype : 'combo',
						ref : '../../operatorValue',
						valueField : 'id',
						displayField : 'text',
						mode : 'local',
						editable : false,
						triggerAction : 'all',
						value : 'newDate',
						store : new Ext.data.SimpleStore({
									fields : ["id", "text"],
									data : [['newDate', '访问时间'],
											['word', '句中词语'],
											['@人民币', '@人民币(问句中)'],
											['@时间', '@时间(问句中)'],
											['@数字', '@数字(问句中)']]
								}),
						getListParent : function() {
							return this.el.up('.x-menu');
						},
						listeners : {
							select : function(combo) {
								var value = combo.getValue();
								ifFieldEle.relationalOperator.store
										.removeAll(true);
								if ('word' == value) {
									ifFieldEle.relationalOperator.store
											.loadData(
													[['match', '匹配'],
															['unmatch', '未匹配']],
													false);
									ifFieldEle.relationalOperator
											.setValue('match');
								} else {
									ifFieldEle.relationalOperator.store
											.loadData([['>', '大于'],
															['<', '小于'],
															['>=', '大于等于'],
															['<=', '小于等于'],
															['==', '等于']],
													false);
									ifFieldEle.relationalOperator.setValue('>');
								}
								if ('@时间' == value || 'newDate' == value) {
									ifFieldEle.conditionValue.hide();
									ifFieldEle.conditionDateValue.show();
								} else {
									ifFieldEle.conditionValue.show();
									ifFieldEle.conditionDateValue.hide();
								}
							}
						}
					}]
				}, {
					columnWidth : .2,
					border : false,
					layout : 'fit',
					items : [{
						xtype : 'combo',
						ref : '../../relationalOperator',
						valueField : 'id',
						displayField : 'text',
						mode : 'local',
						editable : false,
						triggerAction : 'all',
						value : '>',
						store : new Ext.data.SimpleStore({
									fields : ["id", "text"],
									data : [['>', '大于'], ['<', '小于'],
											['>=', '大于等于'], ['<=', '小于等于'],
											['==', '等于']]
								}),
						getListParent : function() {
							return this.el.up('.x-menu');
						}
					}]
				}, {
					columnWidth : .44,
					border : false,
					layout : 'fit',
					items : [{
						xtype : 'trigger',
						ref : '../../conditionValue',
						hidden : true,
						fieldType : 'textfield',
						triggerClass : 'x-form-add-trigger',
						listeners : {
							render : function(c) {
								Ext.QuickTips.register({
											target : this.trigger,
											text : "增加多条件"
										});
							}
						},
						onTriggerClick : function() {
							self.addMultiConditionGroup(1,
									ifFieldEle.operatorValue.getValue());
						}
					}, new CodeTwinTriggerDateField({
								trigger1Class : 'x-form-add-trigger',
								ref : '../../conditionDateValue',
								onTrigger1Click : function() {
									self
											.addMultiConditionGroup(1,
													ifFieldEle.operatorValue
															.getValue());
								},
								anchor : '100%',
								width : 193,
								config : {
									parentMenu : self
								}
							})]
				}, {
					columnWidth : .06,
					border : false,
					layout : 'fit',
					items : [{
								xtype : 'button',
								iconCls : 'icon-add',
								tooltip : '增加elseif条件',
								handler : function() {
									self.addGroup(ifFieldEle.operatorValue
											.getValue());
								}
							}]
				}]
			}]
		});

		var ifContentField = new Ext.form.TextArea({
					name : 'ifContent',
					height : 40,
					width : 400,
					enableKeyEvents : true,
					listeners : {
						afterrender : function() {
							var me = this;
							me.el.swallowEvent(['keypress', 'keydown']);
						}
					}
				});
		var elseField = new Ext.form.TextArea({
					fieldLabel : '缺省值',
					name : 'elseContent',
					height : 40,
					width : 400,
					listeners : {
						afterrender : function() {
							var me = this;
							me.el.swallowEvent(['keypress', 'keydown']);
						}
					}
				});

		return [ifFieldEle, ifContentField, elseField];
	}
	this.renderMultiConditionGroup = function() {
		return self.newMultiConditionField();
	}
	this.renderGroup = function() {
		return self.newElseifField();
	}

	this.addEvents('codeChanged');

	CodeValueMenu.superclass.constructor.call(this, Ext.apply({
						style : {
							overflow : 'visible',
							'background-image' : 'none'
						},
						layout : 'form',
						labelWidth : 50,
						frame : true
					}, cfg));

	this.on('show', function() {
			}, this);
	this.on('beforehide', function() {
				return !this.__noBlur;
			}, this);
	this.on('remove', function(th, component) {
				var childItems = th.items;
				this.fieldCompons = [];
				for (var i = 0; i < childItems.length; i++) {
					childItems.items[i].index = i + 1;
					if (i != childItems.length - 1)
						this.fieldCompons.push(childItems.items[i]);
				}
			}, this);
}

var CodeValueMenu = Ext.extend(Ext.menu.Menu, {
	constructor : CodeValueMenuConstructor,
	onBlur : function() {
		if (!this.__noBlur)
			return CodeValueMenu.superclass.onBlur.apply(this, arguments)
	},
	fillValues : function(ifResult, elseifResult, content) {
		var self = this;
		var initItems = self.initDefaultField();
		if (ifResult.length == 0)
			return;
		var firstCondition = ifResult[0].split(";");
		var contentIndex = 0;
		if (firstCondition.length == 3) {
			initItems[0].operatorValue.setValue(firstCondition[0]);
			if (!('@人民币' == firstCondition[0] || "@时间" == firstCondition[0]
					|| "@数字" == firstCondition[0] || "newDate" == firstCondition[0])) {
				initItems[0].relationalOperator.store.removeAll(true);
				initItems[0].relationalOperator.store.loadData([
								['match', '匹配'], ['unmatch', '未匹配']], false);
			}
			initItems[0].relationalOperator.setValue(firstCondition[1]);
			if ('@时间' == firstCondition[0] || "newDate" == firstCondition[0]) {
				if ('@时间' == firstCondition[0])
					initItems[0].conditionDateValue.setValue(Date.parseDate(
							firstCondition[2], 'YmdHis'));
				else if ("newDate" == firstCondition[0]) {
					var date = new Date();
					date.setTime(firstCondition[2]);
					initItems[0].conditionDateValue.setValue(date);
				}
				initItems[0].conditionValue.hide();
				initItems[0].conditionDateValue.show();
			} else {
				initItems[0].conditionValue.setValue(firstCondition[2]);
				initItems[0].conditionValue.show();
				initItems[0].conditionDateValue.hide();
			}
			initItems[1].setValue(content[contentIndex]);
			contentIndex++;
		}
		var lastIfItems;
		for (var i = 1; i < ifResult.length; i++) {
			var multiCondition = ifResult[i].split(";");
			lastItems = self.addMultiConditionGroup(i == 1
							? 1
							: lastItems[0].index, multiCondition[1],
					multiCondition[0], multiCondition[2], multiCondition[3]);
		}
		if (elseifResult.length > 0) {
			var laftElseifItems;
			for (var e = 0; e < elseifResult.length; e++) {
				var items = elseifResult[e].split(";");
				if ('#elseif' == items[0]) {
					laftElseifItems = self.addGroup(items[1], items[2],
							items[3]);
					laftElseifItems[1].setValue(content[contentIndex]);
					contentIndex++;
				} else {
					self.addMultiConditionGroup(laftElseifItems[0].index,
							items[1], items[0], items[2], items[3])
				}
			}
		}
		if (contentIndex == (content.length - 1))
			initItems[2].setValue(content[contentIndex]);
	},
	getValue : function() {
		return this.codeValues;
	},
	clear : function() {
		for (var i = 0; i < this.fieldCompons.length; i++) {
			var fieldCompon = this.fieldCompons[i];
			this.remove(fieldCompon);
		}
		this.fieldCompons = [];
		this.doLayout();
	},
	assembleCondtion : function(operatorValue, relationalOperator,
			conditionValue, conditionDateValue) {
		var value = "";
		if ("@人民币" == operatorValue || "@数字" == operatorValue) {
			value += "parseInt('$[" + operatorValue + "]')";
			value += relationalOperator + conditionValue;
		} else if ("newDate" == operatorValue) {
			value += "new Date().getTime()" + relationalOperator
					+ conditionDateValue.getTime();
		} else if ("word" == operatorValue) {
			conditionValue = conditionValue.replace("[", "").replace("]", "");
			if (relationalOperator == "match")
				value += "'$[" + conditionValue + "]'.length>0";
			else if (relationalOperator == "unmatch")
				value += "'$[" + conditionValue + "]'.length==0";
		} else if ("@时间" == operatorValue) {
			value += "'$[" + operatorValue + "]'" + relationalOperator + "'"
					+ conditionDateValue.format('YmdHis') + "'";
		}
		return value;
	},
	disassemblyCondition : function(conditionValue) {
		var operatorValue = "";
		if (conditionValue.indexOf('new Date().getTime()') > -1) {
			operatorValue = "newDate";
		}
		conditionValue = conditionValue.replace(/\(\(/g, "(").replace(/\)\)/g,
				")").replace(/\'/g, "");
		var con = conditionValue.replace(/\(/g, "").replace(/\)/g, "");
		if (this.primordialEdit)
			return "";
		if (!((con.indexOf('$[') > -1 && (con.indexOf('].length>0') > -1 || con
				.indexOf('].length==0') > -1))
				|| (con.indexOf("$[@时间]") > -1)
				|| (con.indexOf('parseInt$[@人民币]') > -1)
				|| (con.indexOf('parseInt$[@数字]') > -1) || (con
				.indexOf('new Date.getTime') > -1))) {
			this.primordialEdit = true;
			return "";
		}
		if (con.indexOf('].length>0') > 0) {
			return "word;match;"
					+ con.substring(con.indexOf('$[') + 2, con
									.indexOf('].length>0'));
		}
		if (con.indexOf('].length==0') > 0) {
			return "word;unmatch;"
					+ con.substring(con.indexOf('$[') + 2, con
									.indexOf('].length==0'));
		}
		if (con.split('==').length > 1) {
			return (operatorValue.length > 0
					? operatorValue
					: con.split('==')[0].replace("parseInt$[", "").replace("]",
							"").replace("$[", ""))
					+ ";" + '==' + ";" + con.split('==')[1];
		} else if (con.split('>=').length > 1) {
			return (operatorValue.length > 0
					? operatorValue
					: con.split('>=')[0].replace("parseInt$[", "").replace("]",
							"").replace("$[", ""))
					+ ";" + '>=' + ";" + con.split('>=')[1];
		} else if (con.split('>').length > 1) {
			return (operatorValue.length > 0
					? operatorValue
					: con.split('>')[0].replace("parseInt$[", "").replace("]",
							"").replace("$[", ""))
					+ ";" + '>' + ";" + con.split('>')[1];
		} else if (con.split('<=').length > 1) {
			return (operatorValue.length > 0
					? operatorValue
					: con.split('<=')[0].replace("parseInt$[", "").replace("]",
							"").replace("$[", ""))
					+ ";" + '<=' + ";" + con.split('<=')[1];
		} else if (con.split('<').length > 1) {
			return (operatorValue.length > 0
					? operatorValue
					: con.split('<')[0].replace("parseInt$[", "").replace("]",
							"").replace("$[", ""))
					+ ";" + '<' + ";" + con.split('<')[1];
		}
	},
	setValue : function(codeVal) {
		this.primordialEdit = false;
		this.clear();
		if (codeVal == null) {
			codeVal = '';
		}
		var codeValue = codeVal//Ext.util.Format.htmlDecode(codeVal);
//		codeValue = codeValue.replace(/\&nbsp\;/g, " ");
		var arr = codeValue.split(/\r\n|\r|\n/g);
		var ifResult = [];
		var elseifResult = [];
		var content = [];
		if (codeValue) {
			if (codeValue.indexOf("#if ") == -1)
				this.primordialEdit = true;
			var hasElseIfBeging = false;
			for (var i = 0; i < arr.length; i++) {
				if (this.primordialEdit)
					break;
				var valueLine = arr[i];
				if (valueLine.indexOf("#") == 0) {
					if ("#else " == valueLine) {
						hasElseIfBeging = false;
						continue;
					}
					var andConditionValue = valueLine.split("&&");
					var value = "";
					for (var t = 0; t < andConditionValue.length; t++) {
						var orConditionValue = andConditionValue[t].split("||");
						if (orConditionValue.length == 1) {
							if (andConditionValue[t].indexOf("#if") == 0) {
								value = this
										.disassemblyCondition(andConditionValue[t]
												.substring(
														andConditionValue[t]
																.indexOf("#if ")
																+ 4,
														andConditionValue[t].length));
							} else if (andConditionValue[t].indexOf("#elseif") == 0) {
								hasElseIfBeging = true;
								value = "#elseif;"
										+ this
												.disassemblyCondition(andConditionValue[t]
														.substring(
																andConditionValue[t]
																		.indexOf("#elseif ")
																		+ 8,
																andConditionValue[t].length));
							} else {
								value = this
										.disassemblyCondition(andConditionValue[t]);
							}
							if (t > 0)
								value = "&&;" + value;
							if (hasElseIfBeging) {
								elseifResult.push(value);
							} else {
								ifResult.push(value);
							}
						} else {
							for (var j = 0; j < orConditionValue.length; j++) {
								value = "";
								if (orConditionValue[j].indexOf("#if") == 0) {
									value = this
											.disassemblyCondition(orConditionValue[j]
													.substring(
															orConditionValue[j]
																	.indexOf("#if ")
																	+ 4,
															orConditionValue[j].length));
								} else if (orConditionValue[j]
										.indexOf("#elseif") == 0) {
									hasElseIfBeging = true;
									value = "#elseif;"
											+ this
													.disassemblyCondition(orConditionValue[j]
															.substring(
																	orConditionValue[j]
																			.indexOf("#elseif ")
																			+ 8,
																	orConditionValue[t].length));
								} else {
									value = this
											.disassemblyCondition(orConditionValue[j]);
								}
								if (t > 0 && j == 0)
									value = "&&;" + value;
								if (j > 0)
									value = "||;" + value;
								if (hasElseIfBeging)
									elseifResult.push(value);
								else
									ifResult.push(value);
							}
						}
					}
				} else {
					while (i + 1 != arr.length
							&& !(arr[i + 1].indexOf('#if') == 0
									|| arr[i + 1].indexOf('#elseif') == 0 || arr[i
									+ 1].indexOf('#else') == 0)) {
						valueLine += "\n" + arr[i + 1];
						i++;
					}
					content.push(valueLine.replace(/\<br\>/g,"\r\n"));
				}
			}
		}

		if (this.primordialEdit) {
			this._cfg.parent.primordialCodeEdit = true;
			this._cfg.parent.startEditing(this._cfg.row, this._cfg.col);
		} else {
			this.fillValues(ifResult, elseifResult, content);
		}
		this.codeValues = codeValue;
	}
});
