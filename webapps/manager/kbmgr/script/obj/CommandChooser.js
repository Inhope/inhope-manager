var CommandChooserMenuConstructor = function(cfg) {
	cfg = cfg || {}

	var self = this

	function newCombo(name, defaultValue) {
		return new TwinTriggerCombo({
					trigger1Class : 'x-form-clear-trigger',
					onTrigger1Click : function() {
						if (self.combos.length == 1) {
							this.setValue('');
							self.adjustParamsSet(this);
						} else {
							self.combos.remove(this);
							//self.combos.splice(this.index, 1);
							this.paramsSet.removeAll();
							self.remove(this.paramsSet);
							self.remove(this);
						}
					},
					trigger2Class : 'x-form-add-trigger',
					onTrigger2Click : function() {
						self.addGroup();
					},
					store : new Ext.data.JsonStore({
								idProperty : 'id',
								fields : ['id', 'code', 'name']
							}),
					fieldLabel : name,
					anchor : '100%',
					defaultValue : defaultValue,
					typeAhead : true,
					mode : 'local',
					triggerAction : 'all',
					emptyText : '请选择...',
					selectOnFocus : true,
					valueField : 'code',
					displayField : 'name',
					editable : false,
					getListParent : function() {
						return this.el.up('.x-menu');
					}
				});
	}
	this.addGroup = function() {
		var items = this.renderGroup();
		for (var i = 0; i < items.length; i++) {
			this.insert(this.items.length - 1, items[i]);
		}
		this.doLayout();
	}
	var saveP4CB = function(id) {
		this.setValue(id)
	}
	this.newParamField = function(param, value, combo) {
		if (param.type == 1) {
			var self = this;
			return new Ext.form.TwinTriggerField({
						'value' : value,
						fieldLabel : param.name,
						anchor : '100%',
						name : param.param,
						trigger1Class : 'x-form-attachment-trigger',
						trigger2Class : 'x-form-download-trigger',
						onTrigger1Click : function() {
							if (!self.uploadWin) {
								self.uploadWin = new commons.FileUploadWindow();
							}
							var isCheckComobBox;
							if('imgmsg'==combo.value ||'videomsg'==combo.value ||'videomsg'==combo.value||'attachment'==combo.value)
							    isCheckComobBox=true;
							else
								isCheckComobBox=false;
							self.uploadWin.upload('file-upload!upload.action', function(success, uploadFileId, act, filename) {
										if (success == null) {
							(function		() {
												delete self.__noBlur
											}).defer(10);
										} else {
											this.setValue(uploadFileId)
											combo.paramsSet.items.each(function(item, i, total) {
														if (this == item) {
															var namefield = combo.paramsSet.items.get(i + 1);
															var namefieldv = namefield.getValue().trim();
															var namev = filename;
															var hostv = ''
															var namesplitPos = namefieldv.indexOf('>')
															if (namesplitPos >= 0) {
																hostv = namefieldv.substring(namesplitPos + 1)
															}
															if (hostv) {
																namev = namev + '>' + hostv
															}
															namefield.setValue(namev)
															return false;
														}
													}, this)
										}
									}.dg(this),isCheckComobBox)
							self.__noBlur = true
						},
						downloadAttachment : function(robotpath) {
							if (!self.downloadIFrame) {
								self.downloadIFrame = self.getEl().createChild({
											tag : 'iframe',
											style : 'display:none;'
										})
							}
							combo.paramsSet.items.each(function(item, i, total) {
										if (this == item) {
											var namefield = combo.paramsSet.items.get(i + 1);
											var namefieldv = namefield.getValue().trim();
											var namev = namefieldv;
											var hostv = ''
											var namesplitPos = namefieldv.indexOf('>')
											if (namesplitPos >= 0) {
												hostv = namefieldv.substring(namesplitPos + 1)
												namev = namefieldv.substring(0,namesplitPos)
											}
											if (hostv) {
												if('/'!=hostv.substring(hostv.length-1))
												    hostv = hostv+"/";
												if(hostv.indexOf("http://")>-1)
												  robotpath = hostv;
												else 
												  robotpath = robotpath.replace(/http:\/\/[\/]+/, 'http://' + hostv);
											}
											self.downloadIFrame.dom.src = robotpath + 'attachment.action?id=' + this.getValue() + '&name=' + encodeURIComponent(namev)
													+ '&_t=' + new Date().getTime();
											return false;
										}
									}, this)
						},
						onTrigger2Click : function() {// download
							if (!this.getValue())
								return;
							if (!self.robotpath) {
								Ext.Ajax.request({
											url : 'property!get.action?key=robot.context_path',
											success : function(response) {
												self.robotpath = response.responseText.split(',')[0]
												this.downloadAttachment(self.robotpath);
											},
											scope : this
										})
							} else {
								this.downloadAttachment(self.robotpath);
							}
						}
					});
		} else if (param.type == 2) {
			var self = this;
			return new Ext.form.TriggerField({
						'value' : value,
						fieldLabel : param.name,
						anchor : '100%',
						fieldType : param.type,
						name : param.param,
						triggerClass : 'x-form-attachment-trigger',
						_gc : function() {
							if (this._cb && self.p4win)
								self.p4win.removeListener('save', this._cb);
							delete this._cb;
							if (this._hideFn && self.p4win)
								self.p4win.removeListener('hide', this._hideFn);
							delete this._hideFn;
						},
						onTriggerClick : function(btn) {
							this._gc();
							this._cb = saveP4CB.dg(this);
							if (!self.p4win) {
								self.p4win = new obj.detail.P4Win();
							}
							this._hideFn = function() {
								delete self.__noBlur
								this._gc();
							}.dg(this);
							self.p4win.on('hide', this._hideFn);
							self.p4win.on('save', this._cb);
							self.p4win.show();
							self.p4win.editP4(this.getValue());
							self.__noBlur = true
						}
					});
		} else if (param.type == 3) {
			var self = this;
			return new Ext.form.TriggerField({
						'value' : value,
						fieldLabel : param.name,
						anchor : '100%',
						fieldType : param.type,
						name : param.param,
						triggerClass : 'x-form-attachment-trigger',
						_gc : function() {
							if (this._cb && self.imgmsg)
								self.imgmsg.removeListener('save', this._cb);
							delete this._cb;
							if (this._hideFn && self.imgmsg)
								self.imgmsg.removeListener('hide', this._hideFn);
							delete this._hideFn;
						},
						onTriggerClick : function(btn) {
							var isSample;
							if(this.getValue()){
								var dataArr = this.getValue().split(',');
				                imgmsgId = dataArr[0];
				                isSample = (1 == dataArr[1] ? 'true' : 'false');	
				                this._gc();
								this._cb = saveP4CB.dg(this);
								self.imgmsg = new obj.detail.ImgMsg();
								this._hideFn = function() {
									delete self.__noBlur
									this._gc();
								}.dg(this);
								self.imgmsg.on('hide', this._hideFn);
								self.imgmsg.on('save', this._cb);
								self.imgmsg.html='<iframe id="imgtxtMsgEditWindow" frameborder="no" scrolling="yes" border="0" allowtransparency="yes" marginwidth="0" margintop="0" marginheight="0" height="100%" width="100%" src="../kbmgr/imgmsgeditor/imgtxtMsgEdit.jsp?imgmsgId='+imgmsgId+'&isSample='+isSample+'"></iframe>'
								self.imgmsg.show();
								if("false"==isSample)
								  self.imgmsg.setHeight(580);
								else
								  self.imgmsg.setHeight(620);
								self.__noBlur = true;
							}else{
								self.__noBlur=true;
								Ext.Msg.show({
									title : '图文消息类型选择',
									msg : '您选择你要添加的图文消息类型',
									buttons : {
										yes : '单图文',
										no : '多图文',
										cancel : true
									},
									fn : function(choice) {
										if (choice == 'cancel'){
											delete self.__noBlur;
											return
										}
										isSample = ('yes' == choice ? 'true' : 'false');	
										this._gc();
										this._cb = saveP4CB.dg(this);
									  	self.imgmsg = new obj.detail.ImgMsg();
										this._hideFn = function() {
											delete self.__noBlur
											this._gc();
										}.dg(this);
										self.imgmsg.on('hide', this._hideFn);
										self.imgmsg.on('save', this._cb);
										self.imgmsg.html='<iframe id="imgtxtMsgEditWindow" frameborder="no" scrolling="yes" border="0" allowtransparency="yes" marginwidth="0" margintop="0" marginheight="0" height="100%" width="100%" src="../kbmgr/imgmsgeditor/imgtxtMsgEdit.jsp?imgmsgId=&isSample='+isSample+'"></iframe>'
										self.imgmsg.show();
										if("false"==isSample)
										  self.imgmsg.setHeight(580);
										else
										  self.imgmsg.setHeight(620);
										self.__noBlur = true;
									},
									scope : this,
									icon : Ext.Msg.QUESTION,
									minWidth : Ext.Msg.minWidth
								});
							}
						}
					});
		} else {
			return new Ext.form.TextField({
						fieldLabel : param.name,
						anchor : '100%',
						name : param.param,
						'value' : value
					});
		}
	}
	this.createXHR = function() {
		var _xhr = false;
		try {
			_xhr = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				_xhr = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e2) {
				_xhr = false;
			}
		}
		if (!_xhr && window.XMLHttpRequest)
			_xhr = new XMLHttpRequest();
		return _xhr;
	}, this.init = function(cb) {
		var result = window.__cmd_chooser_initdata;
		if (!result) {
			var xhr = this.createXHR()
			xhr.open('GET', '../kbmgr/ontology-instruction!list.action', false);
			xhr.send();
			result = window.__cmd_chooser_initdata = Ext.decode(xhr.responseText);
		}
		self._data = result.data;
		this.renderUI(result.data);
		this.fireEvent('initcomplete')
		// Ext.Ajax.request({
		// url : '',
		// success : function(response) {
		// var result = Ext.decode(response.responseText);
		//						
		// },
		// failure : function() {
		// },
		// scope : this
		// })
	};

	this.BUTTONS = {
		OK : {
			xtype : 'button',
			text : '确定',
			colspan : 2,
			width : 50,
			handler : function() {
				var cmdArray = [];
				for (var i = 0; i < self.combos.length; i++) {
					var _combo = self.combos[i];
					var _paramsSet = _combo.paramsSet;
					var cmd = _combo.getValue();
					if (cmd) {
						var cmdMap = {};
						var args = [];
						var emptyitem = null;
						for (var j = 0; j < _paramsSet.items.getCount(); j++) {
							var item = _paramsSet.items.get(j);
							if (!item.getValue()) {
								emptyitem = item
							} else if (emptyitem) {
								emptyitem.markInvalid();
								return;
							} else {
								args.push(item.getValue())
							}
						}
						cmdMap['cmd'] = cmd;
						if (args.length > 0)
							cmdMap['args'] = args;
						cmdArray.push(cmdMap);
					}
				}
				self.cmdArray = cmdArray;
				self.fireEvent('commandChanged')
				self.hide();
			}
		},
		CANCEL : {
			xtype : 'button',
			text : '取消',
			colspan : 2,
			width : 50,
			handler : function() {
				self.hide()
			}
		}
	}
	this.getParams = function(code) {
		if (!this._data)
			return null;
		for (var i = 0; i < this._data.length; i++) {
			if (this._data[i].code == code) {
				return this._data[i].params;
			}
		}
		return null;
	}
	this.adjustParamsSet = function(combo, code) {
		combo.paramsSet.removeAll()
		var params = this.getParams(code);
		if (params) {
			for (var i = 0; i < params.length; i++) {
				var value = "";
				combo.paramsSet.add(this.newParamField(params[i], value, combo));
			}
		}
		this.doLayout();
	}
	this.renderUI = function(instructions) {
		this.instructions = instructions;
		var buttons = [];
		if (cfg.buttonsToAdd) {
			buttons = buttons.concat(cfg.buttonsToAdd);
		} else {
			buttons = [this.BUTTONS.OK, this.BUTTONS.CANCEL];
		}
		Ext.each(buttons, function(b) {
					if (!b.width) {
						b.width = 50
					}
				})
		var items = [];
		var buttonGroup = {
			xtype : 'buttongroup',
			layout : 'hbox',
			border : false,
			frame : false,
			defaults : {
				xtype : 'label'
			},
			items : [{
						xtype : 'spacer',
						flex : 1
					}].concat(buttons)
		}
		this.combos = [];
		items.push(buttonGroup);
		self.add(items);
		self.doLayout();
	}
	this.renderGroup = function() {
		var paramsSet = new Ext.form.FormPanel({
					border : false,
					frame : false,
					padding : 0,
					labelWidth : 60,
					bodyStyle : 'background:transparent; border-bottom:1px solid #CCCCCC; margin-bottom:4px;',
					anchor : '100%',
					items : []
				});
		var combo = newCombo("指令", "");
		combo.getStore().loadData(this.instructions);
		combo.on('select', function(combo, record, index) {
					self.adjustParamsSet(combo, record.data.code)
				});
		var items = [combo, paramsSet]
		combo.paramsSet = paramsSet;
		if (!this.combos) {
			this.combos = [];
			combo.index = 0;
		} else {
			combo.index = this.combos.length;
		}
		this.combos.push(combo);
		return items;
	}

	this.addEvents('commandChanged');

	CommandChooserMenu.superclass.constructor.call(this, Ext.apply({
						style : {
							overflow : 'visible', // For the Combo popup
							'background-image' : 'none'
						},
						layout : 'form',
						labelWidth : 60,
						frame : true
					}, cfg));

	this.on('show', function() {

			}, this);
	this.on('beforehide', function() {
				return !this.__noBlur;
			}, this);

}

var CommandChooserMenu = Ext.extend(Ext.menu.Menu, {
			constructor : CommandChooserMenuConstructor,
			onBlur : function() {
				if (!this.__noBlur)
					return CommandChooserMenu.superclass.onBlur.apply(this, arguments)
			},
			fillValues : function() {
				if (this.cmdArray.length == 0) {
					this.addGroup();
				} else {
					var i = 0;
					for (var i = 0; i < this.cmdArray.length; i++) {
						var key = this.cmdArray[i].cmd;
						var args = this.cmdArray[i].args;
						if (key) {
							var _combo = this.combos[i];
							if (!_combo) {
								this.addGroup();
								_combo = this.combos[i];
							}
							_combo.setValue(key)
							this.adjustParamsSet(_combo, key);
							var count = _combo.paramsSet.items.getCount();
							_combo.paramsSet.items.each(function(item, idx) {
										if (args && args[idx]) {
											if (count - 1 == idx) {
												var v = '';
												for (var k = idx; k < args.length; k++) {
													v += args[k];
													if (k < args.length - 1)
														v += ','
												}
												if (v.length > 0)
													item.setValue(v);
											} else
												item.setValue(args[idx])
										}
									}, this);
						}
					}
				}
			},
			getCommand : function() {
				var m = this.cmdArray.concat(this.otherCmds)
				var cmd = '';
				for (var i = 0; i < m.length; i++) {
					cmd += m[i].cmd;
					var args = m[i].args;
					if (args)
						cmd += '(' + args.join(',') + ')';
					cmd += ';';
				}
				return cmd;
			},
			getCommandCnString : function(cmdstring) {
				if (typeof cmdstring == 'string') {
					var m = cmdstring.replace(/\(.*?\)/ig, '').split(';');
					var cmd = ''
					for (var i = 0; i < m.length; i++) {
						Ext.each(this.instructions, function(d) {
									if (d.code == m[i]) {
										cmd += d.name;
										cmd += ';';
										return false;
									}
								})
					}
					return cmd;
				} else {
					var m = this.cmdArray.concat(this.otherCmds)
					var cmd = '';
					for (var i = 0; i < m.length; i++) {
						cmd += m[i].cmd;
						Ext.each(this.instructions, function(d) {
									if (d.code == m[i].cmd) {
										cmd += d.name;
										cmd += ';';
										return false;
									}
								})
					}
					return cmd;
				}
			},
			clear : function() {
				for (var i = 0; i < this.combos.length; i++) {
					var combo = this.combos[i];
					combo.paramsSet.removeAll();
					this.remove(combo.paramsSet);
					this.remove(combo);
				}
				this.combos = [];
				this.otherCmds = []
			},
			setCommand : function(cmd) {
				this.clear();
				if (cmd == null) {
					cmd = '';
				}
				var cmdArray = [];
				var arr = cmd.split(";")
				for (var i = 0; i < arr.length; i++) {
					if (!arr[i].trim())
						continue;
					var args = arr[i].match(/\((.*)\)/)
					var cmdMap = {};
					if (!args) {
						cmdMap['cmd'] = arr[i];
					} else {
						var _c = arr[i].substring(0, arr[i].indexOf('('));
						cmdMap['cmd'] = _c;
						cmdMap['args'] = args.slice(1)[0].split(',');
					}

					Ext.each(this.instructions, function(ins, i, list) {
								if (ins.code == cmdMap.cmd) {
									cmdArray.push(cmdMap);
									return false;
								}
								if (list.length - 1 == i) {
									this.otherCmds.push(cmdMap)
								}
							}, this)
				}
				this.cmdArray = cmdArray;
				this.fillValues();
			}
		});
