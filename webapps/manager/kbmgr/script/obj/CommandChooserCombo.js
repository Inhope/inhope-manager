var CommandChooserCombo = Ext.extend(Ext.form.TriggerField, {
			constructor : function(cfg) {
				var self = this;
				var m = new CommandChooserMenu(cfg.menuConfig ? cfg.menuConfig : null);
				m.init();
				m.on('commandChanged', function(v) {
							this.setValue(m.getCommand())
						}, this)
				this.m = m;
				this.addEvents('commandChanged');
				this.relayEvents(m, ["commandChanged"]);

				cfg = cfg || {}
				Ext.apply(cfg, {
							editable : false
						});
				if (cfg.clearBtn) {
					this.triggerClass = 'x-form-clear-trigger'
				}
				return CommandChooserCombo.superclass.constructor.call(this, cfg);

			},
			getMenu : function() {
				return this.m;
			},
			onTriggerClick : function(e) {
				if (this.clearBtn && e.getTarget('.x-form-clear-trigger')) {
					this.cleared = true;
					this.m.clear();
					CommandChooserCombo.superclass.setValue.call(this, null);
				} else {
					this.m.setWidth(this.getWidth())
					this.m.show(this.getEl())
					this.m.setCommand(this.getValue());
				}
			},
			reset : function() {
				CommandChooserCombo.superclass.reset.call(this);
			},
			getValue : function() {
				return this.v;
			},
			setValue : function(cmd) {
				this.v = cmd
				CommandChooserCombo.superclass.setValue.call(this, cmd ? this.m.getCommandCnString(cmd) : cmd);
			},
			destroy : function() {
				CommandChooserCombo.superclass.destroy.call(this)
				this.m ? delete this.m : 0
			}
		});
Ext.reg('cmdchoosercombo', CommandChooserCombo)