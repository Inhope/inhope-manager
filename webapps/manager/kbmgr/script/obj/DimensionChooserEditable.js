var DimensionChooserMenuEditnableConstructor = function(cfg) {
	cfg = cfg || {}
	var EventManager = Ext.extend(Ext.util.Observable, {});
	if (!DimensionChooserMenuEditnableConstructor.evtmgr)
		DimensionChooserMenuEditnableConstructor.evtmgr = new EventManager();
	var evtmgr = DimensionChooserMenuEditnableConstructor.evtmgr;
	var self = this;
	var comboList = [];
	this.init = function(cb) {
		if (this._inited) {
			if (cb)
				cb()
			return;
		}
		this._inited = true;
		this.initProcess = function(_data) {
			var data = _data[0];
			if (cfg.excludeDims && cfg.excludeDims.length) {
				var _data = [];
				for (var i = 0; i < data.length; i++) {
					if (cfg.excludeDims.indexOf(data[i].code) == -1)
						_data.push(data[i]);
				}
				data = _data;
			}
			this.renderUI(data);
			this.allDimension = data;
			if (evtmgr.dims) {
				this.setDimensions(evtmgr.dims);
			} else {
				this
						.fireEvent('dimensionChanged', self
										.getSelectedDimensions());
			}
			if (cb)
				cb()
		}
		this._inited = true;
		if (window._dimChooserInitData) {
			this.initProcess(window._dimChooserInitData)
		} else {
			Ext.Ajax.request({
						url : 'ontology-dimension!listAllowed.action',
						success : function(response) {
							var result = Ext.decode(response.responseText);
							this.initProcess(result.data)
						},
						failure : function() {
							if (cb)
								cb()
						},
						scope : this
					})
		}
	};

	this.BUTTONS = {
		OK : {
			xtype : 'button',
			text : '确定',
			colspan : 2,
			width : 50,
			handler : function() {
				self
						.fireEvent('dimensionChoosed', self
										.getSelectedDimensions())
				self.sd = null;
				self.hide()
			}
		},
		CANCEL : {
			xtype : 'button',
			text : '取消',
			colspan : 2,
			width : 50,
			handler : function() {
				self.hide()
			}
		}
	}
	var comboGroup = this.comboGroup = {};
	this.renderUI = function(dimensions) {
		var viewList = [];
		for (var i = 0; i < dimensions.length; i++) {
			var dimension = dimensions[i];
			if(!cfg.isShowAll && dimension.code
					&& 'platform brand location'.indexOf(dimension.code) == -1)
				continue;
			var combo = this.createCombo(dimension);
			comboGroup[dimension.code] = [combo]
			viewList.push(combo);
		}
		var buttons = [];
		if (cfg.buttonsToAdd) {
			buttons = buttons.concat(cfg.buttonsToAdd);
		} else {
			buttons = [this.BUTTONS.OK, this.BUTTONS.CANCEL];
		}
		for (var i = buttons.length - 1; i >= 0; i--) {
			var b = buttons[i];
			if (typeof b == 'string') {
				buttons[i] = b = this.BUTTONS[buttons[i]];
				if (!b) {
					buttons.splice(i, 1);
					continue
				}
			}
			if (!b.width) {
				b.width = 50
			}
		}
		var items = [viewList, {
					xtype : 'buttongroup',
					layout : 'hbox',
					border : false,
					frame : false,
					defaults : {
						xtype : 'label'
					},
					items : [{
								xtype : 'spacer',
								flex : 1
							}].concat(buttons)
				}]
		self.add(items);
		self.doLayout();
	}
	this.addEvents('dimensionChanged', 'dimensionChoosed');
	this.on('show', function() {
				this.init()
				this.sd = this.getSelectedDimensions();
			}, this);
	if (cfg.labelAlign == 'top')
		this.cls = 'dimension-chooser-vertical';
	return DimensionChooserEditableMenu.superclass.constructor.call(this, Ext
					.apply({
								style : {
									overflow : 'visible', // For the Combo
									// popup
									'background-image' : 'none'
								},
								layout : 'form',
								labelWidth : 40,
								frame : true
							}, cfg))
}

var DimensionChooserEditableMenu = Ext.extend(Ext.menu.Menu, {
	constructor : DimensionChooserMenuEditnableConstructor,
	/**
	 * @return array[tag(id,name,dimId)]
	 */
	getSelectedDimensions : function() {
		var ret = [];
		for (var dimid in this.comboGroup) {
			var combos = this.comboGroup[dimid];
			Ext.each(combos, function(c) {
						if (c.isVisible() && c.getValue()) {
							ret.push({
										id : c.getValue(),
										name : c.getRawValue(),
										dimId : combos[0].hiddenName
									})
						}
					});
		}
		return ret;
	},
	/**
	 * 
	 * @param {object}
	 *            groupValueMap - {key=dimId,value=[tagIds...]}
	 */
	updateUI : function(groupValueMap) {
		for (var dimId in this.comboGroup) {
			var combos = this.comboGroup[dimId];
			var tagIds = groupValueMap[dimId];
			var combosToRm = []
			for (var i = 0; tagIds && i < tagIds.length || i < combos.length; i++) {
				var tagId = tagIds ? tagIds[i] : null
				var combo = combos[i];
				if (tagId) {
					if (!combo) {
						combo = this.addCombo(combos[0].hiddenName, true);
					}
					combo.setValue(tagId)
				} else if (i == 0) {
					combo.setValue('')
				} else {
					combosToRm.push(combo)
				}
			}
			Ext.each(combosToRm, function(c) {
						this.removeCombo(c)
					}, this)
		}
		this.doLayout();
	},
	setDimensions : function(tags) {
		var groupValueMap = {};
		for (var j = 0; j < tags.length; j++) {
			var t = tags[j];
			var vlist = groupValueMap[t.dimId];
			if (!vlist) {
				groupValueMap[t.dimId] = vlist = [];
			}
			vlist.push(t.id);
		}
		this.updateUI(groupValueMap);
		this.fireEvent('dimensionChanged', tags);
	},
	setDimensionsByTagId : function(tagIds) {
		var tags = [];
		if (!tagIds)
			return;
		var groupValueMap = {};
		var combos = this._getBaseCombos();
		loop1 : for (var j = 0; j < tagIds.length; j++) {
			var tagId = tagIds[j];
			for (var i = 0; i < combos.length; i++) {
				var combo = combos[i];// base one
				var r = null;
				combo.getStore().findBy(function(rr) {
							if (rr.get('tag') == tagId) {
								r = rr;
								return false;
							}
						});
				if (r) {
					var vlist = groupValueMap[combo.hiddenName];
					if (!vlist) {
						groupValueMap[combo.hiddenName] = vlist = [];
					}
					vlist.push(tagId);
					tags.push({
								id : r.id,
								dimId : combo.hiddenName,
								tag : tagId
							})
					continue loop1;
				}
			}
		}
		this.updateUI(groupValueMap);
		return tags;
	},
	_getBaseCombos : function() {
		var ret = []
		for (var key in this.comboGroup) {
			ret.push(this.comboGroup[key][0])
		}
		return ret;
	},
	filterAllowed : function(tagIds) {
		if (!tagIds)
			return []
		var dims = {};
		var c = this._getBaseCombos();
		loop : for (var j = tagIds.length; j >= 0; j--) {
			var t = tagIds[j];
			for (var i = 0; i < c.length; i++) {
				if (c[i].getStore().getById(t)) {
					continue loop;
				}
			}
			tagIds.splice(j, 1)
		}
	},
	getNameStrById : function(tagIds) {
		if (!tagIds)
			return []
		var dims = {};
		var dim = this.allDimension;
		for (var i = 0; i < dim.length; i++) {
			var d = dim[i];
			for (var j = 0; j < tagIds.length; j++) {
				var t = tagIds[j];
				for (var m = 0; m < d.tags.length; m++) {
					if (d.tags[m].tag == t) {
						if (!dims[d.tag])
							dims[d.tag] = ''
						dims[d.tag] += d.tags[m].name + ',';
					}
				}
			}
		}

		var ret = ''
		for (var k in dims) {
			ret += dims[k].slice(0, -1) + ';'
		}
		if (!ret)
			return '';
		return ret.slice(0, -1);
	},
	// combo management
	createCombo : function(dimension, child) {
		var cls = 'x-form-add-trigger';
		if (child) {
			cls = 'x-form-clear-trigger'
		}
		cls += ' x-combo-btn';

		var combo = new TwinTriggerCombo({
					trigger1Class : cls,
					hiddenTrigger1 : true,
					onTrigger1Click : function(e) {
						if (e.getTarget('.x-form-add-trigger')) {
							this.ownerCt.addComboByUser(this.hiddenName)
						} else if (e.getTarget('.x-form-clear-trigger')) {
							this.ownerCt.removeCombo(this, true)
						}
					},
					store : new Ext.data.JsonStore({
								root : 'tags',
								idProperty : 'tag',
								fields : ['tag', 'name']
							}),
					hiddenName : dimension.code,
					fieldLabel : !child ? dimension.name : '',
					hideLabel : child && this.labelAlign == 'top',
					// style : 'margin-bottom:0;padding-bottom:0',
					defaultValue : dimension.defaultValue,
					anchor : '100%',
					value : dimension.defaultValue,
					typeAhead : true,
					mode : 'local',
					triggerAction : 'all',
					emptyText : '请选择...',
					selectOnFocus : true,
					valueField : 'tag',
					displayField : 'name',
					editable : false,
					getListParent : function() {
						return this.el.up('.x-menu');
					}
				})
		if (!child) {
			combo.dimension = dimension;
			if (dimension.tags && dimension.tags.length
					&& !dimension.tags[dimension.tags.length - 1].id) {
				dimension.tags[dimension.tags.length - 1].name = '全部'
						+ combo.fieldLabel
			}
		}
		if (!dimension.tags) {
			dimension.tags = []
		}
		combo.getStore().loadData(dimension)
		combo.on('change', function(combo, newValue, oldValue) {
					var dims = this.getSelectedDimensions();
					this.fireEvent('dimensionChanged', dims);
				}, this);
		return combo;
	},
	removeCombo : function(combo, dolayout) {
		var list = this.comboGroup[combo.hiddenName];
		Ext.each(list, function(c, i) {
					if (c == combo) {
						this.remove(combo);
						list.splice(i, 1)
						delete combo
						return false;
					}
				}, this)
		if (dolayout)
			this.doLayout();
	},
	addComboByUser : function(dimId) {
		var c = this.comboGroup[dimId][0]
		var list = this.comboGroup[dimId];
		if (list) {
			var valueSet;
			c.store.each(function(r) {
						for (var i = 0; i < list.length; i++) {
							if (list[i].getValue() == r.get('id')) {
								return true;
							}
						}
						valueSet = r.get('id');
						return false;
					})
			if (valueSet) {
				var combo = this.addCombo(dimId, true);
				combo.setValue(valueSet)
			}
		}
	},
	addCombo : function(dimId, dolayout) {
		var list = this.comboGroup[dimId];
		var newCombo;
		var baseCombo = list[0];
		var basePos = this.items.indexOf(baseCombo);
		var insertPos = basePos + list.length;
		var newCombo = this.createCombo(baseCombo.dimension, true)
		this.insert(insertPos, newCombo);
		list.push(newCombo)
		newCombo.show();
		if (dolayout)
			this.doLayout();
		return newCombo
	}
});

ayyayContains = function(array, obj) {
	var i = array.length;
	if (i == 0)
		return false;
	while (i--) {
		if (array[i] === obj) {
			return true;
		}
	}
	return false;
}

DimensionEditableBox = Ext.extend(Ext.form.TriggerField, {
	constructor : function(cfg) {
		var self = this;

		var m = new DimensionChooserEditableMenu(cfg.menuConfig
				? cfg.menuConfig
				: null);
		m.init();
		m.on('dimensionChoosed', function(v) {
					var tags = this.m.getSelectedDimensions();
					if (tags == null)
						return;
					this.tags = tags;
					var tagIds = []
					for (var i = 0; i < tags.length; i++)
						tagIds.push(tags[i].id);
					this.setValue(tagIds);
					if (this.cleared)
						delete this.cleared
				}, this)
		this.m = m;
		this.addEvents('dimensionChanged');
		this.relayEvents(m, ["dimensionChanged"]);

		cfg = cfg || {}
		Ext.apply(cfg, {
					editable : true
				});
		if (cfg.clearBtn) {
			this.triggerClass = 'x-form-add-trigger'
		}
		return DimensionEditableBox.superclass.constructor.call(this, cfg);

	},
	getMenu : function() {
		return this.m;
	},
	onTriggerClick : function(e) {
		// if (this.clearBtn && e.getTarget('.x-form-clear-trigger')) {
		// this.cleared = true;
		// if (this._value != null)
		// this._value = [];
		// DimensionEditableBox.superclass.setValue.call(this, null);
		// } else {
		this.m.setWidth(this.getWidth())
		this.m.show(this.getEl())
		this.m.setDimensionsByTagId([]);
		// }
	},
	reset : function() {
		delete this._value
		DimensionEditableBox.superclass.reset.call(this);
	},
	setValue : function(tagIds) {
		this.m.init();
		if (typeof tagIds == 'string')
			return;
		this.tags = this.m.setDimensionsByTagId(tagIds);
		var chooseValue = tagIds;

		var protoValue = "";
		if (this._value)
			protoValue = this._value.replace(/，/g, ",");
		var categories = protoValue.split(/,/g);
		var tagsSize = this.tags.length;
		var tagsIndex = 0;
		var replaceIndex = [];
		if (categories.length > 0) {
			if (tagsSize > 0) {
				for (var i = 0; i < categories.length; i++) {
					var cate = categories[i];
					var idx = 0;
					if ((idx = cate.indexOf("=")) > 0) {
						var key = cate.substring(0, idx);
						var value = cate.substring(idx + 1);
						if (key.toLowerCase() == this.tags[tagsIndex].dimId) {
							var category = key + "=" + this.tags[tagsIndex].id;
							protoValue = protoValue.replace(cate, category);
							replaceIndex.push(tagsIndex);
						}
						if (tagsIndex < tagsSize - 1)
							tagsIndex++;
					}
				}
				var catego = "";
				if (protoValue.length > 0
						&& protoValue.lastIndexOf(",") + 1 != protoValue.length)
					catego += ",";
				for (var j = 0; j < tagsSize; j++) {
					if (!ayyayContains(replaceIndex, j)) {
						catego += this.tags[j].dimId + "=" + this.tags[j].id;
						if (j < tagsSize - 1
								&& !ayyayContains(replaceIndex, j + 1))
							catego += ",";
					}
				}
				if ("," != catego)
					protoValue += catego;
			}
		}
		this._value = protoValue;
		if (protoValue)
			DimensionEditableBox.superclass.setValue.call(this, protoValue);
	},
	getValue : function() {
		// if (this.clearBtn) {
		// if (this.cleared)
		// return null;
		// if (this._value && !this._value.length)
		// return this.valueTransformer ? this
		// .valueTransformer('ALLDIM') : 'ALLDIM';
		// }
		// if (this.valueTransformer)
		// return this.valueTransformer(this._value);
		// return this._value;
		return '';
	},
	destroy : function() {
		DimensionEditableBox.superclass.destroy.call(this)
		this.valueTransformer ? delete this.valueTransformer : 0
		this._value ? delete this._value : 0
		this.tags ? delete this.tags : 0
		this.m ? delete this.m : 0
	}
});
Ext.reg('dimensionEditableBox', DimensionEditableBox)