var ALL_DIM_STRING = '所有维度';
var DimensionTabCheckBoxConstructor = function(_cfg) {
	this._cfg = _cfg || {};
	var cfg = {};
	this.allLoadForm = [];
	this.dimValues;
	this.allowDimension;

	var self = this;

	var saveP4CB = function(id) {
		this.setValue(id)
	}
	// this.init = function(cb) {
	if (this._inited) {
		if (cb)
			cb()
		return;
	} else {
		// this.fireEvent('initcomplete');
		Ext.Ajax.request({
			  url : 'ontology-dimension!listAllowed.action',
			  success : function(response) {
				  var result = Ext.decode(response.responseText);
				  this.initProcess(result.data);
				  this._inited = true;
			  },
			  failure : function() {
				  if (cb)
					  cb()
			  },
			  scope : this
		  })
	}
	// };
	this.initProcess = function(data) {
		this.renderUI(data[1], data[0]);
		this.allDimension = data[0];
		this.allowDimension = data[1];
	}

	this.BUTTONS = {
		All : {
			xtype : 'button',
			text : '全选',
			colspan : 5,
			width : 50,
			handler : function() {
				self.selectAll(true);
			}
		},
		NONE : {
			xtype : 'button',
			text : '全不选',
			colspan : 5,
			width : 50,
			handler : function() {
				self.selectAll(false);
			}
		},
		OK : {
			xtype : 'button',
			text : '确定',
			colspan : 2,
			width : 50,
			handler : function() {
				self.submitValue();
				self.fireEvent('dimensionChoosed', self.getValue());
				self.fireEvent('dimensionValueChanged')
				self.hide();
			}
		},
		CANCEL : {
			xtype : 'button',
			text : '取消',
			colspan : 2,
			width : 50,
			handler : function() {
				self.hide();
			}
		}
	}
	this.renderUI = function(allowDimensions, allDimensions) {
		var mainPanel = new Ext.TabPanel({
			  region : 'center',
			  activeTab : 0,
			  border : false,
			  resizeTabs : false,
			  enableTabScroll : true,
			  width : (_cfg && _cfg.width) ? _cfg.width : 480,
			  height : (_cfg && _cfg.height) ? _cfg.height : 300,
			  layoutOnTabChange : true
		  });
		for (var i = 0; i < allowDimensions.length; i++) {
			var dimension = allowDimensions[i];
			var isSelectAllEnable = false;
			for (var t = 0; t < allDimensions.length; t++) {
				var allDimension = allDimensions[t];
				if (dimension.id == allDimension.id) {
					if (dimension.tags.length == (allDimension.tags.length + 1)) {
						isSelectAllEnable = true;
					}
					break;
				}
			}
			var tabPanel = new CheckBoxFormPanel(dimension.tags, dimension.id, isSelectAllEnable);
			self.allLoadForm.push(tabPanel);
			var tab = mainPanel.add({
				  // id : dimension.id,
				  layout : 'fit',
				  title : dimension.name,
				  width : '100%',
				  items : tabPanel
			  });
		}
		self.add(mainPanel);
		var buttons = [];
		if (_cfg && _cfg.buttonsToAdd) {
			buttons = buttons.concat(_cfg.buttonsToAdd);
		} else {
			buttons = [this.BUTTONS.OK, this.BUTTONS.CANCEL];
		}
		for (var i = buttons.length - 1; i >= 0; i--) {
			var b = buttons[i];
			if (typeof b == 'string') {
				buttons[i] = b = this.BUTTONS[buttons[i]];
				if (!b) {
					buttons.splice(i, 1);
					continue
				}
			}
			if (!b.width) {
				b.width = 50
			}
		}
		var items = [];
		var buttonGroup = {
			xtype : 'buttongroup',
			layout : 'hbox',
			border : false,
			frame : false,
			defaults : {
				xtype : 'label'
			},
			items : [this.BUTTONS.All, this.BUTTONS.NONE,{
				  xtype : 'spacer',
				  flex : 2
			  }].concat(buttons)
		};
		items.push(buttonGroup);
		self.add(items);
		self.doLayout();
	}

	this.addEvents('dimensionValueChanged');

	DimensionTabCheckBox.superclass.constructor.call(this, Ext.apply({
		    style : {
			    overflow : 'visible',
			    'background-image' : 'none'
		    },
		    layout : 'form',
		    labelWidth : 0,
		    frame : true
	    }, cfg));

	this.on('show', function() {
	  }, this);
	this.on('beforehide', function() {
		  return !this.__noBlur;
	  }, this);
}

var DimensionTabCheckBox = Ext.extend(Ext.menu.Menu, {
	  constructor : DimensionTabCheckBoxConstructor,
	  onBlur : function() {
		  if (!this.__noBlur)
			  return DimensionTabCheckBox.superclass.onBlur.apply(this, arguments)
	  },
	  setDimensionsByTagId : function(tagIds) {
		  var self = this;
		  if (!tagIds)
			  return;
		  if (this.rendered) {
			  var forms = self.allLoadForm;
			  for (var i = 0; i < forms.length; i++) {
				  var form = forms[i];
				  if (form.selectAll.getValue()) {
					  form.selectAll.setValue(true);
					  form.selectAll.setValue(false);
				  }
				  var checkBoxes = form.findByType("checkbox", false);
				  var setSize = 0;
				  for (var j = 0; j < tagIds.length; j++) {
					  var tagId = tagIds[j];
					  if(tagId == 'selectAll'){
						  continue;
					  }
					  var setCheckBox = form.getForm().findField(tagId);
					  if (setCheckBox) {
						  setCheckBox.setValue(true);
						  setSize++;
					  }
				  }
				  if (form.selectAll.getValue() && (setSize == (checkBoxes.length - 1) || setSize == 0))
					  form.selectAll.setValue(true);
			  }
		  }
	  },
	  submitValue : function() {
		  var self = this;
		  var forms = self.allLoadForm;
		  var dimValue = [];
		  var isAllSelect = true;
		  for(var i = 0; i < forms.length; i++){
			  var form = forms[i];
			  if (!form.selectAll || !form.selectAll.getValue()) {
				  isAllSelect = false;
				  break;
			  }
		  }
		  for (var i = 0; i < forms.length; i++) {
			  var form = forms[i];
			  if (!isAllSelect) {
				  var checkBoxes = form.findByType("checkbox", false);
				  Ext.each(checkBoxes, function(checkbox) {
					    if (checkbox.getValue() && checkbox.name.indexOf("groupName:") == -1) {
						    dimValue.push(checkbox.name);
					    }
				    })
			  }
		  }
		  this.dimValues = dimValue;
	  },
	  selectAll : function(allSelect) {
		  var self = this;
		  var forms = self.allLoadForm;
		  for (var i = 0; i < forms.length; i++) {
			  var form = forms[i];
			  form.selectAll.setValue(true);
			  form.selectAll.setValue(allSelect);
		  }
	  },
	  getValue : function() {
		  return this.dimValues;
	  },
	  getNameById : function(dimValueIds) {
		  var self = this;
		  if (!dimValueIds)
			  return [];
		  var ret = '';
		  Ext.each(dimValueIds, function(dimId) {
			    for (var i = 0; i < self.allowDimension.length; i++) {
				    var tags = self.allowDimension[i].tags;
				    Ext.each(tags, function(dimension) {
					      if (dimId == dimension.id) {
						      ret += dimension.name + ";";
					      }
				      })
			    }
		    });
		  if (!ret)
			  return ALL_DIM_STRING;
		  return ret.slice(0, -1);
	  }
  });

var CheckBoxFormPanel = function(_cfg, parentId, isSelectAllEnable) {
	var self = this;
	self.isSelectAllEnable = isSelectAllEnable;
	var columnItems = [];
	if (isSelectAllEnable) {
		this.selectAllCheck = true;
		var selectAll = new Ext.form.Checkbox({
			  name : 'selectAll',
			  boxLabel : '全选该维度',
			  // fieldLabel : '全选',
			  // id : 'all_' + parentId,
			  listeners : {
				  'check' : function(own, checked) {
					  if (checked)
						  self.selectAllCheck = true;
					  if (self.selectAllCheck) {
						  Ext.each(self.findByType("checkbox", false), function(checkBox) {
							    // if (checkBox.name.indexOf("groupName:") == -1)
							    checkBox.setValue(checked);
						    })
					  }
				  }
			  }
		  });
		this.selectAll = selectAll;
		columnItems.push({
			  layout : 'form',
			  columnWidth : 1,
			  border : false,
			  bodyStyle : 'background-color:#f0f0f0;border-bottom:1px solid #8db2e3;',
			  items : [selectAll]
		  });

		var groupRes = http_get('ontology-dimension-group!getGroup.action?dimId=' + parentId);
		if (groupRes) {
			var objRes = Ext.util.JSON.decode(groupRes);
			if (objRes.success) {
				var groups = objRes.data;
				if (groups.length > 0) {
					var cWidth = groups.length % 3;
					for (var i = 0; i < groups.length; i++) {
						var group = groups[i];
						var groupCheckBox = new Ext.form.Checkbox({
							  name : 'groupName:' + (group.dimTagIds && group.dimTagIds.length > 0 ? group.dimTagIds : group.id),
							  boxLabel : group.name.length > 10 ? group.name.substring(0, 8) + ".." : group.name,
							  listeners : {
								  'check' : function(own, checked) {
									  Ext.each(self.findByType("checkbox", false), function(checkBox) {
										    if (own.name.indexOf(checkBox.name) > -1 && checkBox.name.indexOf("groupName:") == -1)
											    checkBox.setValue(checked);
									    })
								  }
							  }
						  });
						columnItems.push({
							  layout : 'form',
							  columnWidth : (groups.length - 1 == i && cWidth == 1) ? 1 : ((groups.length - 1 == i && cWidth == 2) ? .67 : .33),
							  border : false,
							  bodyStyle : 'background-color:#f0f0f0;' + ((groups.length - i <= cWidth || (cWidth == 0 && groups.length - i <= 3)) ? 'border-bottom:1px solid #8db2e3;' : ''),
							  items : [groupCheckBox]
						  });
					}
				}
			}
		}
	}
	var idx = 1;
	Ext.each(_cfg, function(item) {
		  // if (item.name && item.id) {
		  if (item.name) {
			  var checkBox = new Ext.form.Checkbox({
				    // name : 'select' + item.id,
				    // fieldLabel : item.name,
				    boxLabel : item.name.length > 10 ? item.name.substring(0, 8) + ".." : item.name,
				    name : item.id,
				    listeners : {
					    'check' : function(own, checked) {
						    if (self.isSelectAllEnable) {
							    if (checked) {
								    var isSelectAll = 0;
								    var checkBoxes = self.findByType("checkbox", false);
								    var allTagCheckBoxs = 0;
								    Ext.each(self.findByType("checkbox", false), function(checkBox) {
									      if ("selectAll" != checkBox.name && checkBox.name.indexOf("groupName:") == -1) {
										      allTagCheckBoxs++;
										      if (checkBox.getValue())
											      isSelectAll++;
									      }
								      })
								    if (isSelectAll == allTagCheckBoxs) {
									    selectAll.setValue(true);
									    self.selectAllCheck = true;
								    }
								    // else
								    // self.setGroupChecked(self, item.id);
							    } else {
								    self.selectAllCheck = false;
								    selectAll.setValue(false);
								    // self.setGroupChecked(self, item.id);
							    }
						    }
					    }
				    }
			    });
			  columnItems.push({
				    layout : 'form',
				    columnWidth : .33,
				    border : false,
				    items : [checkBox],
				    bodyStyle : 'background-color:#f0f0f0;'
			    });
		  }
	  });
	var cfg = {
		region : 'center',
		autoHeight : false,
		border : false,
		layout : 'column',
		labelWidth : 5,
		// id : parentId,
		width : '100%',
		bodyStyle : 'overflow-x:hidden; overflow-y:auto;background-color:#f0f0f0;border-bottom:1px solid #8db2e3;margin:5 5 0 5;',
		items : columnItems
	}
	CheckBoxFormPanel.superclass.constructor.call(this, Ext.apply(cfg));
}

DimensionCheckBoxField = Ext.extend(Ext.form.TriggerField, {
	  constructor : function(cfg) {
		  var self = this;

		  var m = new DimensionTabCheckBox(cfg.menuConfig ? cfg.menuConfig : null);
		  // m.initProcess();
		  m.on('dimensionChoosed', function(v) {
			    this.m.submitValue();
			    var tagsId = this.m.getValue();
			    if (!tagsId)
				    return;
			    this.setValue(tagsId);
			    if (this.cleared)
				    delete this.cleared
		    }, this)
		  this.m = m;
		  this.addEvents('dimensionChanged');
		  this.relayEvents(m, ["dimensionChanged"]);

		  cfg = cfg || {}
		  Ext.apply(cfg, {
			    editable : false
		    });
		  if (cfg.clearBtn) {
			  this.triggerClass = 'x-form-clear-trigger'
		  }
		  return DimensionCheckBoxField.superclass.constructor.call(this, cfg);

	  },
	  getMenu : function() {
		  return this.m;
	  },
	  onTriggerClick : function(e) {
		  if (this.readOnly || this.disabled) {
			  return;
		  }
		  if (this.clearBtn && e.getTarget('.x-form-clear-trigger')) {
			  this.cleared = true;
			  if (this._value != null)
				  this._value = [];
			  DimensionCheckBoxField.superclass.setValue.call(this, null);
		  } else {
			  this.m.setWidth(this.getWidth())
			  this.m.show(this.getEl())
			  this.m.setDimensionsByTagId(this._value);
		  }
	  },
	  reset : function() {
		  delete this._value
		  DimensionCheckBoxField.superclass.reset.call(this);
	  },
	  setValue : function(tagIds) {
		  if (typeof tagIds == 'string')
			  return;
		  this._value = tagIds;
		  this.m.setDimensionsByTagId(tagIds);
		  var names = this.m.getNameById(tagIds);
		  if (names)
			  DimensionCheckBoxField.superclass.setValue.call(this, names);
	  },
	  getValue : function() {
		  if (this.clearBtn) {
			  if (this.cleared)
				  return null;
			  if (this._value && !this._value.length)
				  return this.valueTransformer ? this.valueTransformer('ALLDIM') : 'ALLDIM';
		  }
		  if (this.valueTransformer)
			  return this.valueTransformer(this._value);
		  return this._value;
	  },
	  destroy : function() {
		  DimensionCheckBoxField.superclass.destroy.call(this)
		  this.valueTransformer ? delete this.valueTransformer : 0
		  this._value ? delete this._value : 0
		  this.tags ? delete this.tags : 0
		  this.m ? delete this.m : 0
	  }
  });

Ext.extend(CheckBoxFormPanel, Ext.form.FormPanel, {});