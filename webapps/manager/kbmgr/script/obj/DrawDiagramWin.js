var DiagramDrawable = Ext.extend(Ext.util.Observable, {
			constructor : function(comp) {
				this.comp = comp;
				DiagramDrawable.superclass.constructor.call(this)
			},
			destroy : function() {
				if (this._canvas) {
					this._canvas.parentNode.removeChild(this._canvas);
					delete this._canvas
				}
			},
			getCanvas : function() {
				return this._canvas
			},
			draw : function() {
				if (this.comp.rendered)
					this.draw0.apply(this, arguments)
				else {
					var args = arguments
					this.comp.on('afterrender', function() {
								this.draw0.apply(this, args)
							})
				}
			},
			draw0 : function(drawCfg, width, height, domToRender) {
				if (!domToRender)
					domToRender = this.comp.body.dom
				if (this._canvas) {
					this._canvas.parentNode.removeChild(this._canvas);
					delete this._canvas
				}
				this._canvas = generatorIFrame(width, height, Ext.encode(drawCfg), '', domToRender);
			}
		})
var DrawDiagramWin = Ext.extend(Ext.Window, {
			title : '本体类继承关系',
			destroy : function() {
				DrawDiagramWin.superclass.destroy.call(this)
				if (this.drawable)
					this.drawable.destroy(), delete this.drawable
			},
			_fullView : true,
			initComponent : function() {
				this._defaultBox = [this.width, this.height]
				this.tools = [{
							id : 'restore',
							qtip : '切换完整/紧凑类图',
							handler : this.adjustView.dg(this, [])
						}]
				DrawDiagramWin.superclass.initComponent.call(this)
				this.drawable = new DiagramDrawable(this);
			},
			/** @type {DiagramDrawable} */
			drawable : null,
			adjustView : function(fullView) {
				if (this.drawable.getCanvas()) {
					if (fullView != null)
						this._fullView = fullView
					else
						this._fullView = this._fullView ? false : true
					var size = !this._fullView ? this._winBox : this._canvasBox
					this.drawable.getCanvas().style.width = size[0] + 'px';
					this.drawable.getCanvas().style.height = size[1] + 'px';
					this.setSize({
								width : size[0] + this._winBorder,
								height : size[1] + this._winBorder
							})
				}
			},
			draw : function(drawCfg, width, height) {
				if (width)
					this.width = width;
				else
					this.width = this._defaultBox[0]
				if (height)
					this.height = height;
				else
					this.height = this._defaultBox[1]
				this._winBox = [this.width - this._winBorder, this.height - this._winBorder]
				this.drawable.draw(drawCfg, this._winBox[0], this._winBox[1])
				this._canvasBox = [parseInt(this.drawable.getCanvas().dWidth), parseInt(this.drawable.getCanvas().dHeight)]
				this.adjustView(true);
			},
			_winBorder : 14,
			layout : 'fit',
			bodyStyle : 'padding:0;margin:0',
			style : 'padding:0',
			height : 400,
			width : 400,
			resizable : false
		})

var ClassDrawDiagramPanel = Ext.extend(Ext.Panel, {
			border : false,
			destroy : function() {
				ClassDrawDiagramPanel.superclass.destroy.call(this)
				if (this.drawable)
					this.drawable.destroy(), delete this.drawable
			},
			initComponent : function() {
				this.bbar = ['->',{
							xtype : 'label',
							ref : 'stat',
							text : 'helloworld'
						}]
				ClassDrawDiagramPanel.superclass.initComponent.call(this)
				this.drawable = new DiagramDrawable(this);
			},
			/** @type {DiagramDrawable} */
			drawable : null,
			load : function(classId) {
				Ext.Ajax.request({
							url : 'ontology-class!findAttrs.action?data=' + (classId ? classId : '') + '&traverseDown=true',
							scope : this,
							success : function(response) {
								if (!response || !response.responseText)
									return;
								var drawCfg = {
									'picType' : 'clz',
									data : []
								}
								var data = Ext.decode(response.responseText).data;
								Ext.each(data, function(d) {
											d.text = d.name, delete d.name
											drawCfg.data.push(d)
										}, this)
								this.drawable.draw(drawCfg, this.getEl().getWidth(), this.getEl().getHeight())
							}
						});
				Ext.Ajax.request({
							url : 'ontology-class!stat.action?data=' + (classId ? classId : ''),
							scope : this,
							success : function(response) {
								if (!response || !response.responseText)
									return;
								var statData = Ext.decode(response.responseText);
								this.getBottomToolbar().stat.setText(new Ext.XTemplate('[本体类]:{[values[0].c]}/{[values[1].c]}/{[values[2].c]}'
												+ ' [属性]:{[values[0].a]}/{[values[1].a]}/{[values[2].a]}' + ' [语义模板]:{[values[0].r]}/{[values[1].r]}/{[values[2].r]}'
												+ ' [样例]:{[values[0].s]}/{[values[1].s]}/{[values[2].s]} (当前/分类/总数)').apply(statData), false);
							}
						});
			}
		})