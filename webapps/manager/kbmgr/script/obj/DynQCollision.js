Ext.namespace('DynQCollision')
DynQCollision.Type = {
	fields : ['vid', 'question', 'answer', 'allAnswer', 'path', 'objectName', 'attrName', 'cgroup', 'ignored'],
	emptyRecord : function() {
		var d = {}
		Ext.each(this.fields, function(f) {
			  d[f] = ''
		  });
		return new (this.getRecordType())(d);
	},
	getRecordType : function() {
		if (!this.rt)
			this.rt = Ext.data.Record.create(this.fields);
		return this.rt;
	},
	createStore : function(cfg) {
		cfg = cfg || {}
		return new Ext.data.GroupingStore({
			  reader : new Ext.data.JsonReader(Ext.apply({
				    fields : this.getRecordType()
			    }, cfg)),
			  url : cfg.url,
			  groupField : 'cgroup'
		  })
	}
}
DynQCollision.ListPanel = Ext.extend(Ext.grid.GridPanel, {
	border : false,
	pageSize : 50,
	loadMask:true,
	view : new Ext.grid.GroupingView({
		forceFit : true,
		// custom grouping text template to display the number of items per group
		groupTextTpl : '{text} '
		  + '<span style="cursor:pointer;color:blue;text-decoration:underline;font-weight:normal;float:right" cgroup="{[values.rs[0].data.cgroup]}" title="选中该组所有记录" deselect="true" class="selectGroup">全不选</span>'
		  + '<span style="cursor:pointer;color:blue;text-decoration:underline;font-weight:normal;float:right;margin-right:5px;" cgroup="{[values.rs[0].data.cgroup]}" title="选中该组所有记录" class="selectGroup">全选</span>'
		  + '<span style="cursor:pointer;color:blue;text-decoration:underline;font-weight:normal;float:right;margin-right:5px;" cgroup="{[values.rs[0].data.cgroup]}" title="点击标记该组，下次检测动态扩展问时将被忽略" class="ignoreGroup">{[values.rs.length>1 && !values.rs[0].data.ignored?"忽略":""]}</span>',
		enableRowBody : true,
		getRowClass : function(record, index, rowParams, store) {
			return record.get('ignored') ? "row-ignored" : "";
		}
	}),
	initComponent : function() {
		this.store = DynQCollision.Type.createStore({
			  url : 'dyn-validate!list.action',
			  root : 'data',
			  autoLoad : false
		  })
		this.bbar = this._pbar = new Ext.PagingToolbar({
			  store : this.store,
			  displayInfo : true,
			  pageSize : this.pageSize,
			  prependButtons : true,
			  beforePageText : '第',
			  afterPageText : '页，共{0}页',
			  displayMsg : ' ',
			  emptyMsg : "没有记录"
		  })
		this._pbar.add('->', Dashboard.getModuleProp('kbmgr.dynqcollision.disable_delete')?'':{
			  text : '删除选中记录',
			  iconCls : 'icon-delete',
			  handler : function() {
				  var rs = this.getSelectionModel().getSelections()
				  if (rs.length) {
					  var data = []
					  Ext.each(rs, function(r) {
						    data.push(r.get('vid'))
					    })
					  Ext.Ajax.request({
						    url : 'dyn-validate!deleteValue.action',
						    params : {
							    ids : data.join(',')
						    },
						    success : function(response) {
							    if (this.store.baseParams['refresh'])
								    delete this.store.baseParams['refresh']
							    this.store.reload()
						    },
						    scope : this
					    })
				  }
			  }.dg(this)
		  }, {
			  text : '重新检测',
			  iconCls : 'icon-refresh',
			  handler : function() {
				  this.loadData(true)
			  }.dg(this)
		  }, '-')
		this.selModel = new Ext.grid.CheckboxSelectionModel();
		// this.selModel.on('beforerowselect', function(sm, row, keepExisting, r) {
		// return !r.get('state')
		// })
		this.autoExpandColumn = 'answer', this.columns = [this.selModel, {
			  header : '路径',
			  dataIndex : 'path',
			  width : 150
		  }, {
			  header : '实例名',
			  dataIndex : 'objectName'
		  }, {
			  header : '属性名',
			  dataIndex : 'attrName'
		  }, {
			  header : '标准问',
			  dataIndex : 'question',
			  renderer : function(v, meta, r) {
				  if (r.get('state')) {
					  meta.attr = 'style="background:#00BFFF"';
				  }
				  return v
			  },
			  width : 100
		  }, {
			  header : '标准答案',
			  dataIndex : 'answer',
			  id : 'answer'
		  }, {
			  header : '冲突组',
			  dataIndex : 'cgroup',
			  hidden : true
		  }]

		DynQCollision.ListPanel.superclass.initComponent.call(this)
	},
	processEvent : function(name, e) {
		if (name == 'mousedown' && e.button == 0) {
			var btn = e.getTarget('.ignoreGroup');
			var cgroup;
			if (btn && (cgroup = btn.getAttribute('cgroup'))) {
				var ids = []
				this.store.findBy(function(r) {
					  if (r.get('cgroup') == btn.getAttribute('cgroup')) {
						  ids.push(r.get('vid'))
					  }
				  })
				if (ids.length < 2)
					return;
				Ext.Ajax.request({
					  url : 'dyn-validate!ignore.action',
					  params : {
						  ids : ids.join(','),
						  cgroup : cgroup
					  },
					  success : function(response) {
						  if (this.store.baseParams['refresh'])
							  delete this.store.baseParams['refresh']
						  this.store.reload()
						  Dashboard.setAlert('忽略动态冲突组成功')
					  },
					  scope : this
				  })
				return;
			}
			btn = e.getTarget('.selectGroup');
			var cgroup;
			if (btn && (cgroup = btn.getAttribute('cgroup'))) {
				var ids = []
				var i = 0;
				this.store.findBy(function(r) {
					  if (r.get('cgroup') == btn.getAttribute('cgroup')) {
						  ids.push(i)
					  }
					  i++;
				  })
				if (ids.length) {
					this.getSelectionModel()[btn.getAttribute('deselect') ? 'deselectRange' : 'selectRange'](ids[0], ids[ids.length - 1],!btn.getAttribute('deselect'))
					return
				}
			}
		}
		DynQCollision.ListPanel.superclass.processEvent.call(this, name, e);
	},
	loadData : function(redetect) {
		this.store.baseParams['refresh'] = !!redetect
		this.store.load({
			  params : {
				  limit : this.pageSize
			  }
		  });
	},
	getSelectedR : function() {
		return this.getSelectionModel().getSelected();
	}
})
DynQCollision.showStepWin = function() {
	var p;
	var win = new Ext.Window({
		  title : '动态知识冲突检测',
		  height : 500,
		  width : 800,
		  layout : 'fit',
		  items : p = new DynQCollision.ListPanel(),
		  buttons : ActionRegistry.hasNext() ? [{
			    text : '下一步',
			    handler : function() {
				    win.close();
				    ActionRegistry.nextAction()
			    }
		    }] : null
	  })
	win.show()
	p.loadData()
}
ActionRegistry.register('dynvalidate', function() {
	  DynQCollision.showStepWin()
  })
//setTimeout(function() {
//	  DynQCollision.showStepWin()
//  }, 2000)