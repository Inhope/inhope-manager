Ext.namespace('commons')
commons.FileUploadWindow = Ext.extend(Ext.Window, {
	initComponent : function() {
		this.items = this._buildUploadForm();
		commons.FileUploadWindow.superclass.initComponent.call(this);
		this.on('close', function(p) {
					this._uploadFm.getForm().reset();
					if (this.cb)
						this.cb()
				})
		this.on('hide', function(p) {
					this._uploadFm.getForm().reset();
					if (this.cb)
						this.cb()
				})
	},
	upload : function(url, cb, isFtpSftpComob) {
		if (isFtpSftpComob) {
			var self = this;
			Ext.Ajax.request({
				url : 'file-upload!getIsFtpSftp.action',
				success : function(response) {
					var result = Ext.decode(response.responseText);
					self._uploadFm.toolbars[0].items.items[1].show()
					self._uploadFm.toolbars[0].items.items[2].show()
					if (result.success) {
						if (result.data) {
							self._uploadFm.toolbars[0].items.items[2]
									.setValue(1);
							self._uploadFm.toolbars[0].items.items[2].enable();
						} else {
							self._uploadFm.toolbars[0].items.items[2]
									.setValue(0);
							self._uploadFm.toolbars[0].items.items[2].disable();
						}
						self.uploadURL = url
						self.cb = cb;
						self.show();
					} else {
						Ext.Msg.alert('异常提示', result.message);
					}
				},
				failure : function(response) {
					Ext.Msg.alert('错误', response.responseText);
				}
			})
		} else {
			this._uploadFm.toolbars[0].items.items[1].hide()
			this._uploadFm.toolbars[0].items.items[2].hide()
			this._uploadFm.toolbars[0].items.items[2].setValue(0);
			this.uploadURL = url
			this.cb = cb
			this.show()
		}
	},
	validateFile : function() {

	},
	_buildUploadForm : function() {
		return this._uploadFm = new Ext.FormPanel({
			fileUpload : true,
			frame : false,
			border : false,
			labelWidth : 70,
			autoHeight : true,
			waitMsgTarget : true,
			bodyStyle : 'padding : 3px 10px; background-color:' + sys_bgcolor,
			defaults : {
				anchor : '95%',
				allowBlank : false,
				msgTarget : 'side'
			},
			items : [{
						xtype : 'textfield',
						fieldLabel : '文件名',
						allowBlank : true,
						name : 'filename',
						hidden : true
					}, {
						xtype : 'fileuploadfield',
						emptyText : '选择上传文件',
						fieldLabel : '文件路径',
						name : 'file',
						buttonText : '浏览文件'
					}],
			tbar : [{
				text : '上传',
				name : 'uploadBtn',
				iconCls : 'icon-add',
				handler : function(btn) {
					if ('1' == this._uploadFm.toolbars[0].items.items[2]
							.getValue()) {
						if (this.uploadURL.indexOf("?isFtpSftp=true") == -1)
							this.uploadURL += "?isFtpSftp=true"
					} else {
						this.uploadURL = this.uploadURL.replace(
								"?isFtpSftp=true", "");
					}
					btn.disable();
					if (this.validateFile() === false) {
						btn.enable();
						return
					}
					if (this._uploadFm.getForm().isValid()) {
						var filename = this._uploadFm.getForm()
								.findField('file').getValue().replace(
										/^.*[\/\\]/, '');
						if(filename){
							if(this.uploadURL.indexOf("?isFtpSftp=true")>-1)
							    this.uploadURL +="&fileName="+filename;
							else
							    this.uploadURL +="?fileName="+filename;
						}
						this._uploadFm.getForm().submit({
							url : this.uploadURL,
							waitMsg : '正在上传文件...',
							success : function(form, act) {
								btn.enable();
								var result = Ext
										.decode(act.response.responseText);
								if (result.success) {
									Dashboard.setAlert('上传成功！');
									if (this.cb)
										this.cb(result.success, result.message,
												act, filename)
								}else{
									Ext.Msg.alert('失败', act.result.message);
								}
							},
							failure : function(form, act) {
								btn.enable();
								Ext.Msg.alert('错误', act.result.message);
							},
							scope : this,
							params : this.extraUploadParams || {}
						});
					} else {
						btn.enable()
						Ext.Msg.alert('错误', '您尚未正确完整的输入表单内容，请检查！');
					}
				}.dg(this)
			}, '| 上传方式:', new Ext.form.ComboBox({
						typeAhead : true,
						triggerAction : 'all',
						width : 80,
						mode : 'local',
						store : new Ext.data.ArrayStore({
									id : 0,
									fields : ['id', 'text'],
									data : [['1', 'SFTP/FTP'], ['0', '默认方式']]
								}),
						valueField : 'id',
						displayField : 'text'
					})]
		});
	},
	title : '文件上传',
	defaults : {
		border : false
	},
	modal : true,
	plain : true,
	shim : true,
	closable : true,
	closeAction : 'hide',
	collapsible : true,
	animCollapse : true,
	constrainHeader : true,
	width : 400
});
