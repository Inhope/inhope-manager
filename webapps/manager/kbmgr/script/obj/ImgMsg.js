Ext.namespace('obj.detail')
obj.detail.ImgMsg = Ext.extend(Ext.Window, {
			initComponent : function(id) {
				getTopWindow().ImgMsgSaved = this.onSave.dg(this);
				getTopWindow().ImgmsgEditWindow = this;
				if (id)
					this._id = id;
				else
					this._id = Ext.id();
				obj.detail.ImgMsg.superclass.initComponent.call(this)
			},
			editImgMsg : function(id) {
				editPathId = '1234';
			},
			onSave : function(id) {
				this.fireEvent('save', id)
			},
			title : '图文消息编辑器',
			width : 800,
			height : 620,
			style : "background-color:white;",
			defaults : {
				border : false
			},
			plain : true,// 方角 默认
			modal : true,
			closeAction : 'close',
			autoDestroy : true,
			closable : true, // 关闭
			resizable : false,// 改变大小
			autoScroll : false,
			listeners : {
				'beforeclose' : function() {
					if (typeof imgmsgId != 'undefined')
						imgmsgId = '';
				}
			}
		})