var ObjChooserWin = Ext.extend(Ext.Window, {
	  height : 450,
	  width : 300,
	  title : '选择引用实例',
	  layout : 'fit',
	  minimizable : false,// 最小化
	  maximizable : true,// 最大化
	  resizable : true,
	  draggable : true,
	  modal : true,
	  plain : true,
	  animCollapse : true,
	  closeAction : 'hide',
	  initComponent : function() {
		  this.objtree = new OntologyCategoryTree({
			    region : 'west',
			    split : true,
			    lazyLoadRoot : false,
			    compact : true,
			    id : Ext.id(),
			    header:false
		    })
		  this.items = [this.objtree];
		  ObjChooserWin.superclass.initComponent.call(this)
		  this.objtree.on('click', function(n) {
			    if (!n)
				    return false;
			    if (n.attributes.objectId) {
				    this.fireEvent('materialSelected', {
					      type : 'refobj',
					      id : n.id,
					      title : n.text
				      })
				    this.close()
			    }
		    }, this)
	  },
	  loadData0 : function(id, title) {
		  if (id) {
			  Ext.Ajax.request({
				    url : 'ontology-object!getObjectById.action',
				    params : {
					    "objectId" : id
				    },
				    success : function(response) {
					    var obj = Ext.decode(response.responseText);
					    if (obj.data) {
						    this.objtree.expandPath('/0' + obj.data.cateIdPath, '', function(suc, n) {
							      this.objtree.getNodeById(id) ? this.objtree.getNodeById(id).select() : 0;
						      }.dg(this));
					    } else
						    Dashboard.setAlert("没有找到对应的实例,id=" + id, 3000);
				    },
				    scope : this
			    })
		  }
	  },
	  loadData : function(type, id, title) {
		  if (this.objtree.getRootNode().childNodes.length) {
			  this.objtree.getRootNode().childNodes[0].select()
			  this.loadData0(id, title)
		  } else {
			  var tm = setInterval(function() {
				    if (this.objtree.getRootNode().childNodes.length) {
					    this.objtree.getRootNode().childNodes[0].select()
					    this.loadData0(id, title)
					    clearInterval(tm)
				    }
			    }.dg(this), 500)
		  }
	  }
  })

// setTimeout(function() {
// new ObjChooserWin().show()
// }, 1000)
