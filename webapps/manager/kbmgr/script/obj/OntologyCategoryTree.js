var sys_kbase = "true" == http_get('property!get.action?key=sys.kbase')
var enable_biz_mode = 'true' == Dashboard.getModuleProp('kbmgr.enable_biz_mode')
ActionRegistry.register('kb.reload-objcatetree',function(cateid){
	Dashboard.navPanel.items.each(function(navPanel) {
			if (!cateid) return
		    if (navPanel instanceof OntologyCategoryTree) {
			    navPanel.expand();
			    var node = navPanel.getNodeById(cateid);
			    node.reload?node.reload():node.expand()
		    }
	    });
})
OntologyCategoryTree = function(_cfg) {
	_cfg = _cfg || {}
	var self = this;
	this.data = {};
	var cfg = Ext.apply({
		  title : '知识库管理' + (window.enable_biz_mode ? '(经典模式)' : ''),
		  width : 200,
		  minSize : 175,
		  maxSize : 400,
		  xtype : 'treepanel',
		  collapsible : true,
		  rootVisible : false,
		  enableDD : true,
		  lines : false,
		  border : false,
		  autoScroll : true,
		  containerScroll : true,
		  root : {
			  lazyLoad : _cfg.lazyLoadRoot,
			  id : '0',
			  text : 'root',
			  iconCls : 'icon-ontology-root'
		  },
		  tools : [{
			    id : 'gear',
			    qtip : '经典模式',
			    handler : this.switchMode.dg(this),
			    hidden : window.enable_biz_mode ? false : true
		    }],
		  dataUrl : 'ontology-category!list.action'
	  }, _cfg)
	!cfg.id ? cfg.id = 'objectCategoryNav' : 0
	var tbaritems = [{
		  ref : 'searchNodeText',
		  emptyText : "请输入分类或实例名",
		  xtype : 'textfield',
		  width : 115,
		  listeners : {
			  specialkey : function(f, e) {
				  if (e.getKey() == e.ENTER) {
					  self.searchNode(f);
				  }
			  }
		  }
	  }, {
		  ref : 'searchNodeBtn',
		  text : '查找',
		  iconCls : 'icon-search',
		  width : 50,
		  xtype : 'splitbutton',
		  menu : {
			  items : [{
				    text : '精确匹配',
				    checked : false,
				    checkHandler : function(btn, checked) {
					    this.matchExactly = checked
				    }.dg(this)
			    }]
		  },
		  handler : function() {
			  self.searchNode();
		  }
	  }, {
		  text : '刷新',
		  iconCls : 'icon-refresh',
		  width : 50,
		  handler : function() {
			  self.refresh(self.getRootNode());
		  }
	  }]
	this.showValueGraph = function(q) {
		if (!self.vgraph) {
			self.vgraph = new ValueGraph.Window();
			if (q) {
				setTimeout(function() {
					  self.vgraph.tabpanel.gp.ask(q)
				  }, 500)
			}
		} else {
			q ? self.vgraph.tabpanel.gp.ask(q) : 0
		}
		self.vgraph.show();
	}
	if (!_cfg.compact) {
		tbaritems = tbaritems.concat(['-', {
			  text : '新建',
			  authName : 'kb.obj.catecontent.0.C',
			  iconCls : 'icon-wordclass-root-add',
			  width : 50,
			  handler : function() {
				  self.addNode(self.getRootNode());
			  }
		  }, applyIfVersionEnterprise({
			    text : '同步所有分类',
			    authName : 'kb.obj.catecontent.0.S',
			    iconCls : 'icon-ontology-sync',
			    width : 50,
			    handler : function() {
				    self.syncAI(self.getRootNode(), {
					      clusternode : self.getTopToolbar().find('authName', 'kb.obj.catecontent.0.S')[0].clusternode_all
					        && self.getTopToolbar().find('authName', 'kb.obj.catecontent.0.S')[0].clusternode_all.checked ? 'all' : ''
				      });
			    }
		    }, {
			    menu : {
				    items : [{
					      checked : !!Dashboard.getModuleProp('kbmgr.syncai.force_serial'),
					      disabled : !!Dashboard.getModuleProp('kbmgr.syncai.force_serial'),
					      text : '依次同步',
					      ref : '../clusternode_all',
					      hideOnClick : false
				      }]
			    }
		    }), {
			  text : '重建知识索引',
			  authName : 'kb.obj.catecontent.0.S',
			  iconCls : 'icon-ontology-reidx',
			  width : 50,
			  handler : function() {
				  self.rebuildFaqIdx();
			  }
		  }, !sys_kbase ? '' : {
			  text : '展开虚拟节点',
			  iconCls : 'icon-edit',
			  menu : {
				  items : [{
					    checked : false,
					    text : '是否展开',
					    checkHandler : function(item, checked) {
						    self.showVirtualDimCate = true;
					    },
					    hideOnClick : false
				    }]
			  },
			  scope : this
		  }, {
			  text : '分析标准问',
			  authName : 'kb.obj.catecontent.0.S',
			  iconCls : 'icon-participle',
			  width : 50,
			  handler : function() {
				  if (!self.anaWin)
					  self.anaWin = new AnalyseStandQuestionWin();
				  self.anaWin.loadData();
				  self.anaWin.show();
			  }
		  }, {
			  text : '动态知识冲突检测',
			  // authName : 'kb.obj.catecontent.0.DYN',
			  iconCls : 'icon-participle',
			  width : 50,
			  handler : function() {
				  DynQCollision.showStepWin()
			  }.dg(this)
		  }, {
			  text : '小i知识图谱',
			  // authName : 'kb.obj.catecontent.0.G',
			  iconCls : 'icon-graph',
			  width : 50,
			  handler : this.showValueGraph.dg(this, [])
		  }, '-'])
	}
	cfg.tbar = new Ext.Toolbar({
		  enableOverflow : true,
		  height : '20',
		  items : Dashboard.u().filterAllowed(tbaritems)
	  })
	OntologyCategoryTree.superclass.constructor.call(this, cfg);
	this.on('beforeload', function(node, event) {
		  this.getLoader().baseParams['ispub'] = this.isPub(node);
		  this.getLoader().baseParams['showVirtualDimCate'] = this.showVirtualDimCate;
	  });
	this.loader.on('loadexception', function(loader, node, response) {
		  Ext.Msg.alert('错误', '发生错误:' + response.responseText)
	  })
	if (cfg.compact)
		return
	this._addObjectCB = function() {
	}
	this._addObject = function(n) {
		n = n || this.getSelectedNode();
		if (this.simpleMode) {
			var objectPanel = this.showTab(IFramePanel, 'object-tab-s-' + Ext.id(), '新建业务', 'icon-ontology-object', true);
			var baseurl = Dashboard.getModuleProp('kbmgr.biz_mode_host_addr')
			if (!baseurl) {
				alert('please config "kbmgr.biz_mode_host_addr" first!')
				return
			}
			var ec = encodeURIComponent			
			var usr = Dashboard.u().getUsername()		
			var lastParams = '{"username":"'+ec(usr)+'","categoryId":"'+n.id+'"}'
			objectPanel.setLocation(baseurl + '/app/biztpl/article!index.htm&lastParams='+lastParams)
		} else {
			var objectPanel = self.showTab(obj.detail.MainPanel, 'object-tab-' + Ext.id(), '新建实例', 'icon-ontology-object', true);
			var path = this.getAuthPath(n.parentNode);
			var privilege = {
				M : true,
				MFAQ : Dashboard.u().allow('kb.obj.instance.MFAQ') && !Dashboard.u().allow('kb.obj.instance.C')
				  || (Dashboard.u().allow('kb.obj.catecontent.' + path + '.MFAQ') && !Dashboard.u().allow('kb.obj.catecontent.' + path + '.MOBJ'))
			}
			privilege.MFAQ ? privilege.M = false : 0
			objectPanel.objectChanged(new obj.detail.ObjectType({
				    categoryId : n.id ? n.id : null
			    }), this._addObjectCB, null, this.isPub(n), {
				  privilege : privilege
			  });
		}
	}
	var categoryMenu = new Ext.menu.Menu({
		  items : Dashboard.u().filterAllowed([{
			    text : '刷新',
			    iconCls : 'icon-refresh',
			    width : 50,
			    handler : function() {
				    self.refreshNode();
			    }
		    }, {
			    ref : 'MN_C',
			    text : '新建子分类',
			    iconCls : 'icon-ontology-cate-add',
			    width : 50,
			    handler : function() {
				    self.addNode(categoryMenu.currentNode);
			    }
		    }, {
			    ref : 'MN_M',
			    text : '修改分类',
			    iconCls : 'icon-ontology-cate-edit',
			    width : 50,
			    handler : function() {
				    self.modifyNode(categoryMenu.currentNode);
			    }
		    }, {
			    ref : 'MN_D',
			    text : '删除分类',
			    iconCls : 'icon-ontology-cate-del',
			    width : 50,
			    handler : function() {
				    self.deleteNode(categoryMenu.currentNode);
			    }
		    }, {
			    ref : 'MN_OBJ_C',
			    _authName : 'kb.obj.instance.C',
			    text : '新建实例',
			    iconCls : 'icon-ontology-object-add',
			    width : 50,
			    handler : this._addObject.dg(this, [])
		    }, {
			    ref : 'MN_IMP',
			    text : '导入数据',
			    iconCls : 'icon-ontology-import',
			    width : 50,
			    handler : function() {
				    this.doImport(this.getSelectedNode());
			    }.dg(this)
		    }, {
			    ref : 'MN_EXP',
			    text : '导出数据',
			    iconCls : 'icon-ontology-export',
			    width : 50,
			    handler : function() {
				    this.doExport(this.getSelectedNode())
			    }.dg(this)
		    }, '-', {
			    ref : 'MN_IMP_CATE',
			    _authName : 'IMP',
			    text : '导入分类详情数据',
			    authVersion : 'e',
			    iconCls : 'icon-ontology-import',
			    width : 50,
			    handler : function() {
				    this.doCategoryDetailImport(this.getSelectedNode());
			    }.dg(this)
		    }, {
			    ref : 'MN_EXP_CATE',
			    _authName : 'EXP',
			    text : '导出分类详情数据',
			    authVersion : 'e',
			    iconCls : 'icon-ontology-export',
			    width : 50,
			    handler : function() {
				    this.doCategoryDetailExport(this.getSelectedNode())
			    }.dg(this)
		    }, '-', applyIfVersionEnterprise({
			      ref : 'MN_S_CUR',
			      _authName : 'S',
			      text : '同步当前分类',
			      iconCls : 'icon-ontology-sync',
			      width : 50,
			      handler : function() {
				      self.syncAI(categoryMenu.currentNode, {
					        clusternode : categoryMenu.MN_S_CUR.clusternode_all && categoryMenu.MN_S_CUR.clusternode_all.checked ? 'all' : ''
				        })
			      },
			      menu : {
				      items : [{
					        checked : !!Dashboard.getModuleProp('kbmgr.syncai.force_serial'),
					        disabled : !!Dashboard.getModuleProp('kbmgr.syncai.force_serial'),
					        text : '依次同步',
					        ref : '../clusternode_all',
					        hideOnClick : false
				        }]
			      }
		      }), applyIfVersionEnterprise({
			      ref : 'MN_S_ALL',
			      authName : 'kb.obj.catecontent.0.S',
			      text : '同步所有分类',
			      iconCls : 'icon-ontology-sync',
			      width : 50,
			      handler : function() {
				      self.syncAI(self.getRootNode(), {
					        clusternode : categoryMenu.MN_S_ALL.clusternode_all && categoryMenu.MN_S_ALL.clusternode_all.checked ? 'all' : ''
				        });
			      },
			      menu : {
				      items : [{
					        checked : !!Dashboard.getModuleProp('kbmgr.syncai.force_serial'),
					        disabled : !!Dashboard.getModuleProp('kbmgr.syncai.force_serial'),
					        text : '依次同步',
					        ref : '../clusternode_all',
					        hideOnClick : false
				        }]
			      }
		      }), '-', {
			    // ref : 'MN_IMP_LEGACY',
			    // authName : 'kb.obj.catecontent.0.LGCIMP',
			    // text : '导入遗留系统数据',
			    // iconCls : 'icon-ontology-import',
			    // width : 50,
			    // handler : function() {
			    // this.doImportLegacy(this.getSelectedNode());
			    // }.dg(this)
			    // }, {
			    // ref : 'MN_EXP_LEGACY',
			    // authName : 'kb.obj.catecontent.0.LGCEXP',
			    // text : '导出遗留系统数据',
			    // iconCls : 'icon-ontology-export',
			    // width : 50,
			    // handler : function() {
			    // this.doExportLegacy(this.getSelectedNode());
			    // }.dg(this)
			    // }, {
			    ref : 'MN_PROP',
			    text : '属性',
			    authName : 'ALL',
			    iconCls : 'icon-view-switcher',
			    width : 50,
			    handler : function() {
				    if (categoryMenu.currentNode) {
					    var d = categoryMenu.currentNode.attributes
					    var s = '';
					    for (var k in d) {
						    s += k + ':' + d[k] + '<br>'
					    }
				    }
				    Ext.Msg.alert('属性', s)
			    }.dg(this)
		    }])
	  });
	this._deleteObject = function() {
		var selNode = this.getSelectedNode();
		if (!selNode) {
			Ext.Msg.alert('提示', '请先选择需要删除的节点');
			return;
		}
		Ext.Msg.confirm('确定框', '确定删除记录 ' + selNode.text + ' 吗?', function(result) {
			  if (result == 'yes') {
				  Ext.Ajax.request({
					    url : 'ontology-object!del.action',
					    params : {
						    "objectId" : selNode.id,
						    ispub : this.isPub(selNode)
					    },
					    success : function(response) {
						    var result = Ext.util.JSON.decode(response.responseText);
						    Dashboard.setAlert('删除成功');
						    if ("state" == result.message) {
							    self.setNodeIconCls(selNode, 'icon-ontology-object-del');
						    } else if ("delete" == result.message) {
							    selNode.parentNode.removeChild(selNode, true);
							    this.closeTab((selNode.tabId ? selNode.tabId : 'object-tab-' + selNode.id))
						    }
					    },
					    failure : function(response) {
						    Ext.Msg.alert("错误", response.responseText);
					    },
					    scope : this
				    });
			  }
		  }, this);
	}

	var objectMenu = new Ext.menu.Menu({
		  items : Dashboard.u().filterAllowed([{
			    ref : 'MN_O_D',
			    _authName : 'kb.obj.instance.D',
			    text : '删除实例',
			    iconCls : 'icon-ontology-object-del',
			    width : 50,
			    handler : this._deleteObject.dg(this)
		    }, {
			    ref : 'MN_PROP',
			    text : '属性',
			    authName : 'ALL',
			    iconCls : 'icon-view-switcher',
			    width : 50,
			    handler : function() {
				    if (objectMenu.currentNode) {
					    var d = objectMenu.currentNode.attributes
					    var s = '';
					    for (var k in d) {
						    s += k + ':' + d[k] + '<br>'
					    }
				    }
				    Ext.Msg.alert('属性', s)
			    }.dg(this)
		    }, {
			    ref : 'MN_IMP',
			    text : '导入数据',
			    iconCls : 'icon-ontology-import',
			    width : 50,
			    handler : function() {
				    this.doImport(this.getSelectedNode());
			    }.dg(this)
		    }, {
			    ref : 'MN_EXP',
			    text : '导出数据',
			    iconCls : 'icon-ontology-export',
			    width : 50,
			    handler : function() {
				    this.doExport(this.getSelectedNode())
			    }.dg(this)
		    }])
	  });

	this.categoryMenu = categoryMenu;
	this.objectMenu = objectMenu;
	this.toggleAuthedMenuItem = function(m, catenode) {
		var pnode = catenode;
		var catePath = ''
		do {
			catePath = pnode.attributes.id + '.' + catePath
		} while ((pnode = pnode.parentNode) && pnode.id != '0')
		if (catePath)
			catePath = catePath.slice(0, -1)
		for (var refName in m) {
			if (refName.indexOf('MN_') != 0)
				continue;

			var item = m[refName];
			if (item && typeof item == 'object') {
				var authName = '';
				if (item.authName) {
					item.show();
					continue;
				} else if (item._authName) {
					authName = item._authName;
				} else if (refName.indexOf('MN_') == 0) {
					authName = refName.replace('MN_', '');
				}
				if (authName) {
					var lastIndex = authName.lastIndexOf('.');
					var perm = authName;
					if (lastIndex != -1) {
						perm = authName.substring(lastIndex + 1);
					}
					if (lastIndex != -1 && Dashboard.u().allow(authName) || Dashboard.u().allow('kb.obj.catecontent.' + catePath + '.' + perm)) {
						item.show()
					} else if (typeof item.hide == 'function')
						item.hide()
				} else if (typeof item.show == 'function') {
					item.show()
				}
			}
		}
	}
	this.on('contextmenu', function(node, event) {
		  try {
			  event.preventDefault();
			  node.select();
			  var menu;
			  var catenode;
			  if (node.id.indexOf('@') != -1) {
				  return false;
			  } else if (node.attributes.iconCls == 'icon-ontology-object') {
				  menu = objectMenu;
				  catenode = node.parentNode
			  } else {
				  menu = categoryMenu;
				  catenode = node
				  categoryMenu.MN_OBJ_C.setText(this.simpleMode ? '新增业务' : '新建实例')
			  }
			  this.toggleAuthedMenuItem(menu, catenode);
			  if (menu.items && menu.items.getCount() > 0) {
				  menu.currentNode = node;
				  menu.showAt(event.getXY());
				  menu.items.each(function(item, i, len) {
					    if (item.isVisible()) {
						    return false;
					    }
					    if (i == len - 1) {
						    menu.hide()
					    }
				    })
			  }
		  } catch (e) {
			  if (window.console)
				  console.log(e)
		  }
	  });
	var _treeCombo = new TreeComboBox({
		  fieldLabel : '父级分类',
		  emptyText : '请选择父级分类...',
		  editable : false,
		  hiddenName : 'parentId',
		  valueField : TreeComboBox.FIELD_PATH,
		  anchor : '95%',
		  allowBlank : true,
		  minHeight : 250,
		  root : {
			  id : '0',
			  text : 'root',
			  iconCls : 'icon-ontology-root'
		  },
		  loader : new Ext.tree.TreeLoader({
			    dataUrl : 'ontology-category!list.action'
		    })
	  });
	var classSelected = new TreeComboCheck({
		  ref : 'classesField',
		  dataUrl : 'ontology-class!list.action?listClassOnly=true',
		  name : 'classes',
		  anchor : '100%',
		  fieldLabel : '默认继承类',
		  treeConfig : {
			  tbar : [{
				    text : '清空',
				    iconCls : 'icon-refresh',
				    handler : function() {
					    classSelected.setValueEx();
				    }
			    }, {
				    text : '刷新',
				    iconCls : 'icon-refresh',
				    handler : function() {
					    classSelected.reload();
				    }
			    }]
		  }
	  });
	var _formPanel = new Ext.form.FormPanel({
		  frame : false,
		  border : false,
		  labelWidth : 100,
		  // autoHeight : true,
		  waitMsgTarget : true,
		  bodyStyle : 'padding : 3px 3px',
		  defaults : {
			  blankText : '',
			  invalidText : '',
			  anchor : '100%',
			  xtype : 'textfield'
		  },
		  formRecordType : Ext.data.Record.create(['id', 'parentId', 'name', 'moduleId', 'tagIds', 'classes', 'tag', 'orderNo', 'ispub']),
		  loadData : function(d) {
			  var moduleIdField = this.find('hiddenName', 'moduleId')[0];
			  d.parentId.id != '0' ? moduleIdField.hideField() : moduleIdField.showField()
			  // var classesField = this.find('name', 'classes')[0];
			  // var tagIdsField = this.find('name', 'tagIds')[0];
			  // if (d.ispub) {
			  // tagIdsField.hideField(), classesField.hideField();
			  // } else {
			  // tagIdsField.showField(), classesField.showField();
			  // }
			  this.getForm().loadRecord(new this.formRecordType(d))
		  },
		  items : [new Ext.form.Hidden({
			      name : 'id'
		      }), _treeCombo, {
			    fieldLabel : '类型名称',
			    name : 'name',
			    allowBlank : false
		    }, new Ext.form.ComboBox({
			      allowBlank : true,
			      store : new Ext.data.JsonStore({
				        autoDestroy : true,
				        autoLoad : true,
				        url : 'module-config!list.action',
				        root : 'data',
				        fields : ['moduleId', 'name', 'nodeId'],
				        listeners : {
					        load : function(store) {
						        var rsToRemove = []
						        store.findBy(function(r) {
							          if ((r.get('moduleId') && r.get('moduleId').indexOf('common') == 0) || r.get('nodeId')) {
								          rsToRemove.push(r)
							          }
						          });
						        Ext.each(rsToRemove, function(r) {
							          store.remove(r)
						          })
					        }
				        }
			        }),
			      hiddenName : 'moduleId',
			      triggerAction : 'all',
			      editable : false,
			      fieldLabel : '所属模块',
			      valueField : 'moduleId',
			      displayField : 'name'
		      }), new DimensionCheckBoxField({
			      fieldLabel : '默认维度',
			      name : 'tagIds',
			      menuConfig : {}
		      }),
		    // new DimensionBox({
		    // fieldLabel : '默认维度',
		    // name : 'tagIds'
		    // }),
		    classSelected, {
			    fieldLabel : '默认标签',
			    name : 'tag'
		    }, new Ext.form.NumberField({
			      allowDecimals : false,
			      fieldLabel : '类别排序',
			      invalidText : '只能是整数',
			      name : 'orderNo'
		      }), {
			    fieldLabel : '虚拟维度目录',
			    name : 'bizDimTags',
			    hidden : !sys_kbase,
			    emptyText : '请填写维度标签的名字，英文逗号间隔',
			    allowBlank : true
		    }, new PubSchemaSelector()],
		  buttons : [{
			    text : '保存',
			    handler : function() {
				    self.saveNode(this);
			    }
		    }, {
			    text : '关闭',
			    handler : function() {
				    self.endEdit();
			    }
		    }, {
			    text : '重置',
			    handler : function() {
				    _formPanel.getForm().reset();
			    }
		    }]
	  });
	var _formWin = new Ext.Window({
		  width : 615,
		  height : 320,
		  layout : 'fit',
		  title : '类型管理',
		  defaults : {
			  border : false
		  },
		  modal : true,
		  plain : true,
		  shim : true,
		  closable : true,
		  closeAction : 'hide',
		  collapsible : true,
		  draggable : true,
		  animCollapse : true,
		  constrainHeader : true,
		  items : [_formPanel],
		  listeners : {
			  beforehide : function() {
				  _formPanel.getForm().reset();
			  }
		  }
	  });
	_formWin.formPanel = _formPanel;
	this.formWin = _formWin;

	this.on('click', function(n) {
		  if (!n)
			  return false;
		  if (n.attributes.objectId) {
			  this.showObjectTab(n)
		  } else {
			  this.showObjectListTab(n)
		  }
	  }, this)
	this.on('load', function(node, refNode) {
		  if (node.childNodes) {
			  Ext.each(node.childNodes, function(n) {
				    if (n.attributes.objectId) {
					    n.draggable = true
				    }
			    })
		  }
	  });
	this.on('nodedragover', function(e) {
		  if (this.isPub(e.dropNode) != this.isPub(e.target)) {
			  return false;
		  }
	  })
	this.on('beforenodedrop', function(e) {
		  var node = e.dropNode;
		  var target = e.target.attributes.objectId ? e.target.parentNode : e.target;
		  var source = node.parentNode
		  var c = Ext.Msg.confirm('提示', '确定要移动到&nbsp&nbsp' + target.getPath('text').replace('/root', '') + '&nbsp&nbsp吗', function(btn, text) {
			    if (btn == 'yes') {
				    Ext.Ajax.request({
					      url : 'ontology-object!move.action',
					      params : {
						      objectId : node.id,
						      categoryId : node.parentNode.id,
						      ispub : this.isPub(node)
					      },
					      success : function(form, action) {
						      Dashboard.setAlert('移动成功')
					      },
					      failure : function(response) {
						      source.appendChild(node);
						      source.expand();
						      Ext.Msg.alert("错误", response.responseText);
					      }
				      });
			    } else {
				    source.appendChild(node);
				    source.expand();
			    }
		    }, this);
	  });
	// if (!Dashboard.u().allow('kb.obj.instance.EDIT_MODE_SUP')) {
	// this.on('afterrender', this.switchMode.dg(this))
	// }
}

Ext.extend(OntologyCategoryTree, Ext.tree.TreePanel, {
	  authName : 'kb.obj',
	  updateAttr : function(data, cateId, tabId) {
		  var node = this.getNodeById(data.id);
		  if (node) {
			  node.setText(data.text);
		  } else {
			  var parentNode = this.getNodeById(cateId);
			  node = new Ext.tree.TreeNode(data);
			  if (tabId)
				  node.tabId = tabId;
			  if (parentNode)
				  parentNode.appendChild(node);
		  }
		  this.showTab(obj.detail.MainPanel, (node.tabId ? node.tabId : 'object-tab-' + node.id), node.text, 'icon-ontology-object', true);
	  },
	  showObjectListTab : function(n) {
		  var self = this;
		  var listPanel = this.showTab(obj.list.MainPanel, 'object-tab-', "实例查询", 'icon-ontology-cate', true);
		  var cateIdPath = n.cateIdPath;
		  var targetId = n.id;
		  listPanel.tab.on('activate', function() {
			    self.expandPath('/0' + cateIdPath, '', function(suc, n) {
				      self.getNodeById(targetId) ? self.getNodeById(targetId).select() : 0;
			      });
		    });
		  listPanel.categoryChanged(this.getCateId(n), n.attributes.bh, this.isPub(n), {
			    dimtagid : this.getDimtagid(n)
		    })
	  },
	  showObjectTab : function(n, dimValueId, options) {
		  options = options || {}
		  if (this.simpleMode) {
			  var objectPanel = this.showTab(IFramePanel, 'object-tab-s-' + n.id, '编辑业务::' + n.text, 'icon-ontology-object', true);
			  var baseurl = Dashboard.getModuleProp('kbmgr.biz_mode_host_addr')
			  if (!baseurl) {
				  alert('please config "kbmgr.biz_mode_host_addr" first!')
				  return
			  }
			  var ec = encodeURIComponent
			  var usr = Dashboard.u().getUsername()
			  var lastParams = '{"username":"'+ec(usr)+'","articleId":"'+n.id+'"}'
			  objectPanel.setLocation(baseurl + '/app/biztpl/article!index.htm?lastParams='+lastParams)
			  // var objectPanel = this.showTab(IFramePanel, 'object-tab-s-' + n.id,
					// '编辑业务::' + n.text, 'icon-ontology-object', true);
			  // objectPanel.setLocation(String.format('ontology-object!editByBizTemplate.action?data={0}',
					// encodeURIComponent(Ext.encode({
			  // objectId : n.id
			  // }))));
		  } else {
			  var path;
			  if (n.parentNode) {
				  path = this.getAuthPath(n.parentNode);
			  } else if (n.cateIdPath) {
				  // direct access,not from nav tree,/1/2/3
				  path = n.cateIdPath.substring(1).replace(/\//g, '.')
			  } else {
				  throw new Error('no category id path,objectId=' + n.id)
			  }
			  var privilege = {
				  R : Dashboard.u().allow('kb.obj.instance.R') || Dashboard.u().allow('kb.obj.catecontent.' + path + '.R'),
				  M : Dashboard.u().allow('kb.obj.instance.M') || Dashboard.u().allow('kb.obj.catecontent.' + path + '.M'),
				  MFAQ : Dashboard.u().allow('kb.obj.instance.MFAQ') && !Dashboard.u().allow('kb.obj.instance.M')
				    || (Dashboard.u().allow('kb.obj.catecontent.' + path + '.MFAQ') && !Dashboard.u().allow('kb.obj.catecontent.' + path + '.MOBJ'))
			  }
			  privilege.MFAQ ? privilege.M = false : 0
			  if (!(privilege.R || privilege.M || privilege.MFAQ))
				  return;
			  options.privilege = privilege
			  this.showObjectTab0(n, dimValueId, options);
		  }
	  },
	  showObjectTab0 : function(n, dimValueId, options) {
		  var self = this;
		  var objectPanel = this.showTab(obj.detail.MainPanel, (n.tabId ? n.tabId : 'object-tab-' + n.id), n.text, 'icon-ontology-object', true);
		  var cateIdPath = n.cateIdPath;
		  var targetId = n.id;
		  objectPanel.tab.on('activate', function() {
			    self.expandPath('/0' + cateIdPath, '', function(suc, n) {
				      self.getNodeById(targetId) ? self.getNodeById(targetId).select() : 0;
			      });
		    });
		  if (cateIdPath) {
			  self.expandPath('/0' + cateIdPath, '', function(suc, n) {
				    self.getNodeById(targetId) ? self.getNodeById(targetId).select() : 0;
			    });
		  }
		  objectPanel.objectChanged(new obj.detail.ObjectType({
			      objectId : n.id ? n.id : null
		      }), null, dimValueId, n.ispub != null ? n.ispub : this.isPub(n), options)
	  },
	  endEdit : function() {
		  this.formWin.hide();
		  this.formWin.formPanel.getForm().reset();
	  },
	  saveNode : function(btn) {
		  // btn.disable();
		  var fp = this.formWin.formPanel
		  if (!fp.getForm().isValid())
			  return;

		  var values = Ext.apply({}, fp.getForm().getFieldValues());
		  values.orderNo = values.orderNo ? values.orderNo : '0';
		  values.parentId = fp.find('hiddenName', 'parentId')[0].getValue();
		  values.classes = fp.find('name', 'classes')[0].getValue();
		  if (!values.parentId && !values['moduleId']) {
			  Ext.Msg.alert('提示', '顶层节点没有选择模块');
			  fp.find('hiddenName', 'moduleId')[0].markInvalid();
			  return;
		  }
		  delete values.tagIds;
		  var tagIds = fp.find('name', 'tagIds')[0].getValue()
		  if (tagIds) {
			  values.defaultTags = []
			  for (var i = 0; i < tagIds.length; i++) {
				  values.defaultTags.push({
					    id : tagIds[i]
				    })
			  }
		  }
		  var ispub = values.ispub;
		  delete values.ispub;
		  var self = this;
		  Ext.Ajax.request({
			    url : 'ontology-category!save.action',
			    params : {
				    data : Ext.encode(ux.util.resetEmptyString(values)),
				    ispub : ispub
			    },
			    success : function(form, action) {
				    Dashboard.setAlert("保存成功!");
				    self.endEdit();
				    self.updateNode(Ext.decode(form.responseText), values.parentId ? values.parentId : '0');
				    btn.enable();
			    },
			    failure : function(response) {
				    Ext.Msg.alert("错误", response.responseText);
				    btn.enable();
			    }
		    })
	  },

	  deleteNode : function(node, reconfirm) {
		  var self = this;
		  var isDelValid = Dashboard.getModuleProp('kbmgr.kb_category_delete_validation');
		  if ("true" == isDelValid) {
			  var ispub = this.isPub(node) ? this.isPub(node) : '';
			  var isChildObj = http_get('ontology-category!isChildObject.action?id=' + node.id + '&ispub=' + ispub);
			  if ("true" == isChildObj) {
				  Ext.Msg.alert('提示', '目录下有实例存在，禁止删除！');
				  return;
			  }
		  }
		  Ext.Msg.confirm('确认', reconfirm ? '删除后将无法恢复,请确认' : '您确定要删除 ' + node.getPath('text').replace('/root', '') + ' 及以下所有子分类和实例吗?', function(btn, text) {
			    if (btn == 'yes') {
				    if (!reconfirm)
					    this.deleteNode(node, true);
				    else
					    Ext.Ajax.request({
						      url : 'ontology-category!del.action',
						      params : {
							      id : node.id,
							      parentId : node.parentNode.id,
							      ispub : this.isPub(node)
						      },
						      success : function(form, action) {
							      var res = Ext.decode(form.responseText);
							      if ("state" == res.message) {
								      if (node.attributes.iconCls && node.attributes.iconCls.indexOf("-root") > 0) {
									      if (self.isPub(node))
										      self.setNodeIconCls(node, 'icon-ontology-add-root-delete');
									      else
										      self.setNodeIconCls(node, 'icon-ontology-root-delete');
								      } else
									      self.setNodeIconCls(node, 'icon-ontology-cate-del');
							      } else if ("delete" == res.message)
								      node.parentNode.removeChild(node);
						      },
						      failure : function(response) {
							      Ext.Msg.alert("错误", response.responseText);
						      },
						      scope : this
					      });
			    }
		    }, this);
	  },
	  isPub : function(n) {
		  if (!n.attributes)
			  return n.ispub;
		  while (n.parentNode != null && n.parentNode != this.getRootNode()) {
			  n = n.parentNode
		  }
		  return n.attributes.ispub;
	  },
	  addNode : function(node) {
		  this.modifyInternal({
			    ispub : this.isPub(node)
		    }, {
			    id : node.id,
			    path : node.getPath('text').replace('/root', '')
		    }, node.id == '0')
	  },
	  modifyNode : function(node) {
		  var d = node.attributes;
		  d.name = d.text;
		  d.ispub = this.isPub(node)
		  this.modifyInternal(d, {
			    id : node.parentNode.id,
			    path : node.parentNode.getPath('text').replace('/root', '')
		    })
	  },
	  modifyInternal : function(d, parentNodeData, enableSchemaSelector) {
		  this.formWin.show();
		  if (!parentNodeData.path) {
			  parentNodeData.path = '/';
		  }
		  d.parentId = parentNodeData
		  this.formWin.formPanel.loadData(d)
		  var pss = this.formWin.formPanel.find('hiddenName', 'ispub')[0]
		  pss[Dashboard.u().allow('ALL') ? 'show' : 'hide']()
		  pss.setReadOnly(enableSchemaSelector ? false : true)
	  },
	  refresh : function(node) {
		  if (node.isExpanded())
			  node.reload();
		  else
			  node.expand();

	  },
	  getAuthPath : function(node) {
		  if (!node)
			  node = this.getSelectedNode()
		  return node.getPath().replace('/0/', '').replace(/\//ig, '.');
	  },
	  getSelectedNode : function() {
		  return this.getSelectionModel().getSelectedNode();
	  },
	  updateNode : function(data, parentId) {
		  var parentCateId = parentId;
		  var parentNode;
		  if (!parentCateId) {
			  parentNode = this.getRootNode();
		  } else
			  parentNode = this.getNodeById(parentCateId);
		  if (!data.saveOrUpdate) {
			  var selNode = this.getSelectedNode();
			  selNode.setText(data.text);
			  Ext.apply(selNode.attributes, data)
			  // selNode.attributes.tagIds = data.tagIds;
			  // selNode.attributes.moduleId = data.moduleId;
			  // selNode.attributes.classes = data.classes;
			  if (selNode.parentNode.id != parentCateId) {
				  if (parentNode) {
					  selNode.leaf = data.leaf;
					  this.setNodeIconCls(selNode, data.iconCls);
					  selNode.attributes.bh = data.bh;
					  parentNode.appendChild(selNode);
				  } else
					  selNode.parentNode.removeChild(selNode);
			  }
		  } else {
			  if (parentNode) {
				  var node = new Ext.tree.TreeNode(data);
				  if (parentNode.leaf)
					  parentNode.leaf = false
				  parentNode.appendChild(node);
				  parentNode.expand();
				  node.select();
			  }
		  }
	  },
	  setNodeIconCls : function(node, clsName) {
		  var iel = node.getUI().getIconEl();
		  if (iel) {
			  var el = Ext.get(iel);
			  if (el) {
				  if (clsName.indexOf('-root') > 0) {
					  el.removeClass('icon-ontology-root');
					  el.removeClass('icon-ontology-root-add');
				  } else if (clsName.indexOf('-object') > 0) {
					  el.removeClass('icon-ontology-object');
				  }
				  el.addClass(clsName);
			  }
		  }
	  },
	  searchNode : function(focusTarget) {
		  var categoryName = this.getTopToolbar().searchNodeText.getValue();
		  if (!categoryName || !(categoryName = categoryName.trim())) {
			  Ext.Msg.alert('提示', '请输入分类名称');
			  return;
		  }
		  if (!this.getSelectionModel().getSelectedNode()) {
			  this.getRootNode().childNodes[0].select();
		  }
		  var self = this;
		  Ext.Ajax.request({
			    url : 'ontology-category!search.action',
			    params : {
				    categoryName : categoryName,
				    ispub : this.isPub(this.getSelectedNode()),
				    matchExactly : this.matchExactly ? true : false
			    },
			    success : function(form, action) {
				    if (!form.responseText) {
					    Dashboard.setAlert('没有查找到更多的"' + categoryName + '"，请修改后重新查找');
					    return;
				    }
				    var idPath = Ext.util.JSON.decode(form.responseText);
				    if (idPath) {
					    var targetId = idPath.pop();
					    self.expandPath('/0/' + idPath.join('/'), '', function(suc, n) {
						      self.getNodeById(targetId).select();
						      if (focusTarget)
							      focusTarget.focus()
					      });
				    }
			    },
			    failure : function(form, action) {
				    Ext.Msg.alert('提示', '查找错误');
			    }
		    });
	  },
	  expandNode : function(node, bh) {
		  var self = this;
		  if (node.attributes.bh == bh || node.leaf) {
			  this.getSelectionModel().select(node);
			  return;
		  }
		  node.expand(false, true, function() {
			    var childs = node.childNodes;
			    for (var i = 0; i < childs.length; i++) {
				    if (bh.indexOf(childs[i].attributes.bh) != -1)
					    self.expandNode(childs[i], bh);
			    }
		    });
	  },
	  refreshNode : function() {
		  var node = this.getSelectedNode();
		  if (!node)
			  this.refresh(this.getRootNode());
		  else {
			  this.refresh(node);
		  }
	  },
	  rebuildFaqIdx : function() {
		  Ext.Msg.confirm('提示', '确定重建知识索引吗?', function(result) {
			    if (result == 'yes') {
				    Ext.Ajax.request({
					      url : 'sync-ai!rebuildFaqIndex.action',
					      success : function(response) {
						      Dashboard.setAlert('重建知识索引成功');
					      },
					      failure : function(response) {
						      Ext.Msg.alert("错误", response.responseText);
					      },
					      scope : this
				      });
			    }
		    }, this);
	  },
	  syncAI : function(node, options) {
		  var r = this.getRootNode();
		  var p = node;
		  if (p != r)
			  while (p.parentNode != r) {
				  p = p.parentNode
			  }
		  var moduleId = p.attributes.moduleId
		  var categoryId = node.id
		  var syncAI0 = function() {
			  Ext.Ajax.request({
				    url : 'sync-ai!sync.action',
				    params : Ext.apply(categoryId == '0' ? {} : {
					      'categoryId' : categoryId,
					      'moduleId' : moduleId,
					      ispub : this.isPub(node)
				      }, {
					      'clusternode' : options && options.clusternode ? options.clusternode : ''
				      }),
				    success : function(form, action) {
					    var timer = new ProgressTimer({
						      initData : Ext.decode(form.responseText),
						      progressId : 'syncAI',
						      boxConfig : {
							      title : '同步知识库'
						      },
						      finish : function() {
							      var n = this.getSelectedNode()
							      if (n)
								      n.fireEvent('click', n)
						      }.dg(this)
					      });
					    timer.start();
				    },
				    scope : this
			    });
		  }
		  if (categoryId == '0') {
			  Ext.Msg.confirm('同步确认', '您是否要同步所有分类？', function(result) {
				    if (result == 'yes') {
					    syncAI0.call(this);
				    }
			    }, this)
		  } else {
			  syncAI0.call(this);
		  }
	  },
	  doImport : function(node) {
		  if (!this._importwin) {
			  this._importwin = new obj.datatrans.ObjectImportWin();
			  this._importwin.on('importSuccess', function(cateId) {
				    var n = this.getNodeById(cateId);
				    if (n && n.parentNode && typeof n.parentNode.reload == 'function') {
					    n.parentNode.reload();
				    }
			    }, this)
		  }
		  if (node && node.attributes.objectId) {
			  this._importwin.doImport(node.parentNode.id, this.isPub(node), {
				    updateLevel : 104
			    }, node.id)
		  } else {
			  this._importwin.doImport(node ? node.id : '', this.isPub(node))
		  }
	  },
	  doImportLegacy : function(node) {
		  if (!this._importwin_l) {
			  this._importwin_l = new obj.datatrans.LegacyImportWin();
		  }
		  this._importwin_l.doImport(node ? node.id : '', this.isPub(node))
	  },
	  doExportLegacy : function(node) {
		  Ext.Msg.prompt('提示', '需要增加顶级分类的维度,格式为{"新维度名":"旧顶级目录名"}', function(btn, text) {
			    if (btn == 'ok') {
				    Ext.Ajax.request({
					      url : 'legacy-faq-data-trans!doExport.action',
					      params : {
						      newdimToOldCategory : text,
						      categoryId : node.id,
						      ispub : this.isPub(node),
						      type : 'xlsx'
					      },
					      success : function(response) {
						      if (response.responseText) {
							      var timer = new ProgressTimer({
								        initData : Ext.decode(response.responseText),
								        progressText : 'percent',
								        progressId : 'legacyExport',
								        boxConfig : {
									        title : '导出遗留系统数据'
								        },
								        finish : function(p, response) {
									        if (!this.downloadIFrame) {
										        this.downloadIFrame = this.getEl().createChild({
											          tag : 'iframe',
											          style : 'display:none;'
										          })
									        }
									        this.downloadIFrame.dom.src = 'ontology-object-data-trans!downFile.action?_t=' + new Date().getTime();
								        },
								        scope : this
							        });
							      timer.start();
						      }
					      },
					      scope : this
				      })
			    }
		    }, this, true, '{"短信":"短信"}');
	  },
	  doExport : function(node, dataLoaderKey) {
		  var btns = isVersionStandard() ? {
			  yes : '导出',
			  cancel : true
		  } : {
			  yes : '07',
			  no : '03',
			  cancel : true
		  }
		  var msgbox = Ext.Msg.show({
			    title : '导出数据',
			    msg : '请选择您要导出的数据',
			    buttons : btns,
			    fn : function(choice) {
				    if (choice == 'cancel')
					    return
				    var self = this;
				    var includeOntologyclassWordclass = Ext.getCmp('export_ontologyclassAndWordclass').getValue()
				    // 开始下载
				    Ext.Ajax.request({
					      url : 'ontology-object-data-trans!doExport.action',
					      params : {
						      type : 'yes' == choice ? 'xlsx' : 'xls',
						      categoryId : node ? (node.attributes.objectId ? node.parentNode.id : node.id) : '',
						      ispub : this.isPub(node),
						      dataLoaderKey : dataLoaderKey ? dataLoaderKey : '',
						      includeFaq : Ext.getCmp('export_includeFaq').getValue() ? 1 : 0,
						      fullRefValue : Ext.getCmp('export_fullRefValue').getValue(),
						      includeId : Ext.getCmp('export_includeId').getValue(),
						      includeOntologyclassWordclass : includeOntologyclassWordclass,
						      objectId : node.attributes.objectId ? node.attributes.objectId : ''
					      },
					      success : function(response) {
						      var timer = new ProgressTimer({
							        initData : Ext.decode(response.responseText),
							        progressText : 'percent',
							        progressId : 'objectExport',
							        boxConfig : {
								        title : '导出实例'
							        },
							        finish : function(p, response) {
								        if (p.success) {
									        if (!this.downloadIFrame) {
										        this.downloadIFrame = self.getEl().createChild({
											          tag : 'iframe',
											          style : 'display:none;'
										          })
									        }
									        this.downloadIFrame.dom.src = 'ontology-object-data-trans!downFile.action?_t=' + new Date().getTime();
									        if (includeOntologyclassWordclass) {
										        setTimeout(function() {
											          this.downloadIFrame.dom.src = 'ontology-object-data-trans!downFile.action?type=ontologyclass&_t=' + new Date().getTime();
										          }.dg(this), 1000)
										        setTimeout(function() {
											          this.downloadIFrame.dom.src = 'ontology-object-data-trans!downFile.action?type=wordclass&_t=' + new Date().getTime();
										          }.dg(this), 2000)
									        }
								        }
							        },
							        scope : this
						        });
						      timer.start();
					      },
					      failure : function(response) {
					      },
					      scope : this
				      });
			    },
			    scope : this,
			    icon : Ext.Msg.QUESTION,
			    minWidth : Ext.Msg.minWidth
		    });
		  if (!Ext.get('_dlg_items')) {
			  var ctn = msgbox.getDialog().body.createChild({
				    id : '_dlg_items',
				    tag : 'div'
			    })
			  ctn.setVisibilityMode(Ext.Element.DISPLAY)
			  var includeFaq = new Ext.form.Checkbox({
				    hidden : isVersionStandard(),
				    inputValue : true,
				    defaultValue : true,
				    checked : true,
				    fieldLabel : '导出扩展问',
				    id : 'export_includeFaq'
			    })
			  var fullRefValue = new Ext.form.Checkbox({
				    hidden : isVersionStandard(),
				    inputValue : true,
				    defaultValue : false,
				    checked : false,
				    fieldLabel : '导出关联答案',
				    id : 'export_fullRefValue'
			    })
			  var includeId = new Ext.form.Checkbox({
				    hidden : isVersionStandard(),
				    inputValue : true,
				    defaultValue : false,
				    checked : false,
				    fieldLabel : '导出ID列',
				    id : 'export_includeId'
			    })
			  var includeOntologyclassWordclass = new Ext.form.Checkbox({
				    inputValue : true,
				    defaultValue : false,
				    checked : false,
				    fieldLabel : '导出相关本体类及词类',
				    id : 'export_ontologyclassAndWordclass'
			    })
			  var fp = Ext.create({
				    xtype : 'panel',
				    border : false,
				    layout : 'form',
				    labelWidth : 130,
				    bodyStyle : 'background-color:#CCD9E8;margin:0 0 0 10px;padding:0;',
				    items : [includeFaq, fullRefValue, includeId, includeOntologyclassWordclass],
				    renderTo : '_dlg_items',
				    labelAlign : 'right'
			    })
			  msgbox.getDialog().on('show', function() {
				    includeFaq.setValue(includeFaq.defaultValue)
			    })
			  msgbox.getDialog().on('hide', function() {
				    Ext.get('_dlg_items').hide();
			    })
		  } else {
			  Ext.get('_dlg_items').show();
		  }
		  msgbox.getDialog().show();
	  },
	  doCategoryDetailImport : function(node) {
		  if (!this._importCategoryDetailWin) {
			  this._importCategoryDetailWin = new obj.datatrans.categoryDetailImportWin();
			  this._importCategoryDetailWin.on('importSuccess', function(cateId) {
				    var n = this.getNodeById(cateId);
				    if (n && n.parentNode && typeof n.parentNode.reload == 'function') {
					    n.parentNode.reload();
				    }
			    }, this)
		  }
		  this._importCategoryDetailWin.doImport(node ? node.id : '', this.isPub(node))
	  },
	  doCategoryDetailExport : function(node) {
		  var msgbox = Ext.Msg.show({
			    title : '导出Excel版本选择',
			    msg : '您选择要导出的excel版本',
			    buttons : {
				    yes : '07',
				    no : '03',
				    cancel : true
			    },
			    fn : function(choice) {
				    if (choice == 'cancel')
					    return
				    var self = this;
				    // 开始下载
				    Ext.Ajax.request({
					      url : 'ontology-object-data-trans!doCategoryDeatailExport.action',
					      params : {
						      type : 'yes' == choice ? 'xlsx' : 'xls',
						      categoryId : node.id,
						      ispub : this.isPub(node)
					      },
					      success : function(response) {
						      var timer = new ProgressTimer({
							        initData : Ext.decode(response.responseText),
							        progressText : 'percent',
							        progressId : 'kbCategoryExport',
							        boxConfig : {
								        title : '导出分类详情'
							        },
							        finish : function(p, response) {
								        if (p.success) {
									        if (!this.downloadIFrame) {
										        this.downloadIFrame = self.getEl().createChild({
											          tag : 'iframe',
											          style : 'display:none;'
										          })
									        }
									        this.downloadIFrame.dom.src = 'ontology-object-data-trans!downCategoryDetailFile.action?_t=' + new Date().getTime();
								        }
							        },
							        scope : this
						        });
						      timer.start();
					      },
					      failure : function(response) {
					      },
					      scope : this
				      });
			    },
			    scope : this,
			    icon : Ext.Msg.QUESTION,
			    minWidth : Ext.Msg.minWidth
		    });
		  msgbox.getDialog().show();
	  },
	  // compact mode
	  switchMode : function() {
		  this.title = this.title.replace(/\(.*\)/, '');
		  if (this.simpleMode) {
			  delete this.simpleMode
			  Ext.QuickTips.register({
				    target : this.tools.gear,
				    text : '经典模式'
			    })
			  this.setTitle(this.title + '(经典模式)')
		  } else {
			  this.simpleMode = true;
			  Ext.QuickTips.register({
				    target : this.tools.gear,
				    text : '业务模式'
			    })
			  this.setTitle(this.title + '(业务模式)')
		  }
		  this.doLayout()
	  },
	  getDimtagid : function(node) {
		  return node.id.indexOf('@') != -1 ? node.id.split('@')[1] : ''
	  },
	  getCateId : function(node) {
		  return node.id.indexOf('@') != -1 ? node.id.split('@')[0] : node.id
	  }
  });

Dashboard.registerNav(OntologyCategoryTree, 1);

ShortcutToolRegistry.register('knowledgeManager', function() {
	  Dashboard.navPanel.items.each(function(navPanel) {
		    if ('知识库管理' == navPanel.title) {
			    navPanel.expand();
			    var node = navPanel.getRootNode();
			    navPanel.expandNode(node);
			    node.expand();
			    if (node.childNodes && node.childNodes.length) {
				    node.childNodes[0].expand();
			(function() {
					    node.childNodes[0].fireEvent('click', node.childNodes[0]);
				    }).defer(100);
			    }
		    }
	    });
  }.dg(this));

var ObjectWizardWinow = Ext.extend(Ext.Window, {
	  title : '配置业务',
	  destroy : function() {
		  ObjectWizardWinow.superclass.destroy.call(this)
		  delete this.contentPanel
	  },
	  loadData : function(d) {
		  this.contentPanel.getForm().loadRecord(new Ext.data.Record(d));
	  },
	  initComponent : function() {
		  this.closeAction = 'hide'
		  this.items = [this.contentPanel = new ObjectWizardForm()]
		  this.buttons = [{
			    text : '生成业务',
			    handler : this.generate.dg(this)
		    }, {
			    text : '关闭',
			    handler : function() {
				    this.hide()
			    }.dg(this)
		    }]
		  ObjectWizardWinow.superclass.initComponent.call(this)
		  this.on('beforehide', function() {
			    this.contentPanel.getForm().reset();
		    })
	  },
	  generate : function() {
		  var values = this.contentPanel.getForm().getValues();
		  if (values.classId && !values.semanticBlock && !values.semanticBlock.trim()) {
			  this.contentPanel.semanticBlock.markInvalid('未填写语义块')
			  return;
		  }
		  this.fireEvent('generate', values)
		  this.hide()
	  },
	  layout : 'fit',
	  height : 150,
	  width : 300
  })
var ObjectWizardForm = Ext.extend(Ext.FormPanel, {
	  border : false,
	  bodyStyle : 'background-color:' + sys_bgcolor + ' padding : 3px 10px;',
	  defaults : {
		  xtype : 'textfield',
		  anchor : '100%'
	  },
	  initComponent : function() {
		  this.items = [{
			    ref : 'name',
			    name : 'name',
			    fieldLabel : '业务名称',
			    listeners : {
				    blur : function() {
					    this.semanticBlock.setValue('[' + this.name.getValue() + ']')
				    }.dg(this)
			    }
		    }, {
			    ref : 'bizTplId',
			    xtype : 'combo',
			    hiddenName : 'bizTplId',
			    fieldLabel : '业务模板',
			    triggerAction : 'all',
			    lazyRender : true,
			    mode : 'remote',
			    store : new Ext.data.JsonStore({
				      url : 'biz-template!findAllTpl.action',
				      root : 'data',
				      idProperty : 'id',
				      fields : ['name', 'id']
			      }),
			    valueField : 'id',
			    displayField : 'name'
		    }, {
			    ref : 'semanticBlock',
			    name : 'semanticBlock',
			    fieldLabel : '语义块'
		    }, {
			    ref : 'categoryId',
			    name : 'categoryId',
			    hidden : true
		    }];
		  ObjectWizardForm.superclass.initComponent.call(this)
	  }
  })
function applyIfVersionEnterprise(cfg, itemcfg) {
	return !isVersionStandard() ? Ext.apply(cfg, itemcfg) : cfg
}