Ext.namespace('PresetQCollision')
PresetQCollision.Type = {
	fields : ['id', 'q', 'qid', 'q_ai', 'qid_ai', 'qpath', 'state','no'],
	emptyRecord : function() {
		var d = {}
		Ext.each(this.fields, function(f) {
			  d[f] = ''
		  });
		return new (this.getRecordType())(d);
	},
	getRecordType : function() {
		if (!this.rt)
			this.rt = Ext.data.Record.create(this.fields);
		return this.rt;
	},
	createStore : function(cfg) {
		cfg = cfg || {}
		return new Ext.data.JsonStore(Ext.apply({
			  remoteSort : true,
			  fields : this.getRecordType()
		  }, cfg));
	}
}
PresetQCollision.ListPanel = Ext.extend(Ext.grid.GridPanel, {
	  border : false,
	  pageSize : 30,
	  loadMask : true,
	  initComponent : function() {
		  this.store = PresetQCollision.Type.createStore({
			    url : 'preset-qcollision!list.action',
			    root : 'data',
			    autoLoad : false
		    })
		  this.bbar = this._pbar = new Ext.PagingToolbar({
			    store : this.store,
			    displayInfo : true,
			    pageSize : this.pageSize,
			    prependButtons : true,
			    beforePageText : '第',
			    afterPageText : '页，共{0}页',
			    displayMsg : ' ',
			    emptyMsg : "没有记录"
		    })
		  this._pbar.on('change', function(ptbar, pageData) {
			    var btn = this.getBottomToolbar().find('name', 'btn-ok')[0]
			    this.pageData = pageData;
			    if (!btn.disable_flag) {
				    if (pageData.activePage == pageData.pages) {
					    btn.enable()
				    } else {
					    btn.disable()
				    }
			    }
			    var ri = 0;
			    this.getStore().each(function(r) {
				      if (this.linkedR[r.get('id')]) {
					      this.getSelectionModel().selectRow(ri,true)
				      }
				      ri++;
			      }, this)
		    }, this)
		  this.linkedR = {}
		  this.collectSelection = function() {
			  var rs = this.getSelectionModel().getSelections()
			  var selectrs = {}
			  Ext.each(rs, function(r) {
				    selectrs[r.get('id')] = r;
			    })
			  this.getStore().each(function(r) {
				    var id = r.get('id')
				    if (selectrs[id] && !this.linkedR[id]) {
					    this.linkedR[id] = {q_ai:selectrs[id].get('q_ai'),no:selectrs[id].get('no')};
				    } else if (!selectrs[id] && this.linkedR[id]) {
					    delete this.linkedR[id];
				    }
			    }, this)
		  }
		  this._pbar.on('beforechange', function() {
			    this.collectSelection();
		    }, this)
		  this._pbar.add('->', {
			    name : 'btn-ok',
			    text : '标准问与预置知识匹配确认',
			    iconCls : 'icon-ok',
			    handler : function(btn) {
				    btn.disable_flag = true;
				    btn.disable()
					this.collectSelection()
				    var rs = []
				    var qs = {}
				    for (var key in this.linkedR) {
				    	var d = this.linkedR[key]
				    	if (!qs[d.q_ai]){
				    		qs[d.q_ai] = []
				    	}
			    		qs[d.q_ai].push(d.no)
			    		
					    rs.push(key)
				    }
				    var errmsg = ''
				    for(var key in qs){
				    	if (qs[key].length > 1){
				    		errmsg+='预置问题:'+key+',序号列表:'+qs[key].join(',')+'<br>'
				    	}
				    }
				    if (errmsg){
				    	Ext.Msg.alert('错误','存在多个标准问匹配到同一预置知识的情况<br>'+errmsg)
				    	btn.disable_flag = false;
					    btn.enable()
				    	return;
				    }
				    
				    if (rs.length) {
					    Ext.Ajax.request({
						      url : 'preset-qcollision!doLink.action',
						      maskEl:this.getEl(),
						      params : {
							      data : rs.join(',')
						      },
						      success : function(response) {
							      Dashboard.setAlert('操作成功')
							      this.store.reload()
						      },
						      scope : this
					      })
				    } else {
					    Ext.Msg.alert('提示', '没有选中任何需要应用的记录')
				    }
			    }.dg(this)
		    })
		  this.selModel = new Ext.grid.CheckboxSelectionModel();
		  this.selModel.on('beforerowselect', function(sm, row, keepExisting, r) {
			    return !r.get('state')
		    })
		  this.autoExpandColumn = 'qpath', this.columns = [this.selModel, {
			    header : '序号',
			    dataIndex : 'no',
			    width : 50
		    }, {
			    header : '路径',
			    dataIndex : 'qpath',
			    id : 'qpath',
			    sortable : true
		    }, {
			    sortable : true,
			    header : '标准问',
			    dataIndex : 'q',
			    renderer : function(v, meta, r) {
				    if (r.get('state')) {
					    meta.attr = 'style="background:#00BFFF"';
				    }
				    return v
			    },
			    width : 200
		    }, {
			    sortable : true,
			    header : '预置知识',
			    dataIndex : 'q_ai',
			    width : 200
		    }]

		  // this.tbar = [[{}]]

		  PresetQCollision.ListPanel.superclass.initComponent.call(this)
		  this.on('render', function() {
			    this.store.load({
				      params : {
					      limit : this.pageSize
				      }
			      })

		    })
	  },
	  getSelectedR : function() {
		  return this.getSelectionModel().getSelected();
	  }
  })
PresetQCollision.showStepWin = function() {
	var win = new Ext.Window({
		  title : '请选择标准问与预置知识匹配正确项',
		  height : 600,
		  width : 900,
		  layout : 'fit',
		  items : new PresetQCollision.ListPanel(),
		  buttons : ActionRegistry.hasNext() ? [{
			    text : '下一步',
			    handler : function() {
				    win.close();
				    ActionRegistry.nextAction()
			    }
		    }] : null
	  })
	win.show()
}
ActionRegistry.register('presetqcollision', function() {
	  PresetQCollision.showStepWin();
  })
//setTimeout(function() {
//	  PresetQCollision.showStepWin()
//  }, 2000)
