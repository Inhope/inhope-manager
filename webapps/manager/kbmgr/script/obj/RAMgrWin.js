var RAMgrPanel = Ext.extend(Ext.tree.TreePanel, {
	  rootVisible : false,
	  enableDD : true,
	  lines : false,
	  border : false,
	  autoScroll : true,
	  dataUrl : 'robot-attament!load.action',
	  ctrlP : null,
	  init : function(objectId) {
		  this.loader.baseParams.objectId = objectId
		  this.getRootNode().reload();
	  },
	  initFromData : function(initData) {
		  this.getRootNode().appendChild(initData)
	  },
	  getInitData : function(initData) {
		  return this.getRootNode().childNodes
	  },
	  initComponent : function() {
		  this.root = {
			  id : '0',
			  text : 'root'
		  };
		  this.tbar = [{
			    xtype : 'button',
			    text : '查看引用',
			    iconCls : 'icon-attr-link',
			    handler : function() {
				    var r = this.getSelectionModel().getSelectedNode();
				    if (r)
					    this.ctrlP.showRelation(r.id)
			    }.dg(this)
		    }, {
			    xtype : 'button',
			    text : '解除引用',
			    iconCls : 'icon-delete',
			    handler : function() {
				    var r = this.getSelectionModel().getSelectedNode();
				    if (r) {
					    if (r.parentNode != this.getRootNode()) {
						    Ext.Msg.alert('提示', '只能解除主文件（顶级节点）的引用，请重新选择')
						    return;
					    }
					    Ext.Ajax.request({
						      url : 'attachment!relieveRelation.action',
						      params : {
							      attachmentId : r.id,
							      objectIds : this.loader.baseParams.objectId,
							      topNodeId : this.ctrlP.topNodeId
						      },
						      success : function(response) {
							      var n = this.getNodeById(r.id);
							      if (n)
								      n.parentNode.removeChild(n)
							      Dashboard.setAlert('解除成功！');
						      },
						      scope : this
					      })
				    }
			    }.dg(this)
		    }]
		  RAMgrPanel.superclass.initComponent.call(this);
		  this._buildEvents();
	  },
	  _buildEvents : function() {
		  this.on('beforeload', function(ths, node, callback) {
			    if (!this.loader.baseParams.objectId)
				    return false;
		    })
	  },
	  getAttachments : function() {
		  var files = []
		  Ext.each(this.getRootNode().childNodes, function(n) {
			    if (n.attributes.isnew) {
				    files.push({
					      id : n.id,
					      name : n.text,
					      op : n.attributes.op,
					      remark : n.attributes.remark
				      })
			    }
			    Ext.each(n.childNodes, function(nn) {
				      if (nn.attributes.isnew) {
					      files.push({
						        id : nn.attributes.id,
						        name : nn.text,
						        fileId : n.id,
						        op : nn.attributes.op,
						        physicalFileId : nn.id,
						        remark : nn.attributes.remark
					        })
				      }
			      }, this)
		    }, this)
		  return files
	  },
	  refresh : function(node) {
		  node = node || this.getRootNode();
		  if (node.isExpanded())
			  node.reload();
		  else
			  node.expand();

	  },
	  // miscs
	  getSelectedNode : function() {
		  return this.getSelectionModel().getSelectedNode();
	  }
  });
var RAMgrWin = Ext.extend(commons.FileUploadWindow, {
	  fileUploaded : function(success, uploadFileId, act, filename) {
		  if (success) {
			  var n = {
				  text : filename,
				  id : uploadFileId,
				  fileId : this.opFileId,
				  isnew : true,
				  op : this.op,
				  remark : this._uploadFm.find('name', 'remark')[0].getValue(),
				  leaf : this.opFileId ? true : false
			  }
			  var pn = this.mgrP.getRootNode();
			  if (this.opFileId) {
				  pn = this.mgrP.getNodeById(this.opFileId)
				  if (!pn) {
					  pn = this.mgrP.getRootNode().appendChild(new Ext.tree.AsyncTreeNode({
						    id : this.opR.get('id'),
						    text : this.opR.get('name'),
						    op : this.op,
						    leaf : false,
						    remark : this._uploadFm.find('name', 'remark')[0].getValue(),
						    isnew : this.op == 'ref' ? true : false
					    }));
				  }
				  if (this.op != 'ref') {
					  pn.expand(false, false, function(nn) {
						    if (this.op == 'update') {
							    if (nn.childNodes && nn.childNodes.length) {
								    pn = nn.childNodes[nn.childNodes.length - 1]
								    pn.setText(n.text)
								    Ext.apply(pn.attributes, n)
							    }
						    } else if (this.op == 'add') {
							    nn.appendChild(n)
						    }
					    }.dg(this))
				  }
			  } else {
				  pn.appendChild(n)
			  }
		  }
	  },
	  load : function(objectId) {
		  this.upload('file-upload!upload.action', this.fileUploaded)
		  this.mgrP.init(objectId)
	  },
	  upload : function(url, cb) {
		  this.uploadURL = url
		  this.cb = cb
	  },
	  loadFromData : function(nodesData) {
		  this.mgrP.initFromData(nodesData)
	  },
	  getAttachments : function() {
		  return this.mgrP.getAttachments()
	  },
	  hasAttachments : function() {
		  return this.mgrP.getRootNode().childNodes && this.mgrP.getRootNode().childNodes.length
	  },
	  showRelation : function(fileid) {
		  if (!this._RAMgrRelationWin) {
			  this._RAMgrRelationWin = new RAMgrRelationWin();
		  }
		  this._RAMgrRelationWin.load(fileid)
		  this._RAMgrRelationWin.show()
	  },
	  initComponent : function() {
		  this.layout = 'fit'
		  this.height = 400
		  this.width = 550
		  this.extraUploadParams = {
			  type : 'local'
		  }
		  RAMgrWin.superclass.initComponent.call(this);
		  var pluginOpCol = new OperationColumn({
			    operationConfig : [{
				      name : '引用',
				      handler : function(grid, row, col, e) {
					      var r = grid.getStore().getAt(row);
					      this.op = 'ref'
					      this.opFileId = r.get('id')
					      this.opR = r
					      this.fileUploaded(true, r.get('id'), '', r.get('name'))
					      Dashboard.setAlert('引用成功！');
				      }.dg(this)
			      }, {
				      name : '新增',
				      handler : function(grid, row, col, e) {
					      var r = grid.getStore().getAt(row);
					      this.op = 'add'
					      this.opFileId = r.get('id')
					      this.opR = r
					      var b = fm.getTopToolbar().find('name', 'uploadBtn')[0]
					      b.handler(b)
				      }.dg(this)
			      }, {
				      name : '覆盖',
				      handler : function(grid, row, col, e) {
					      var r = grid.getStore().getAt(row);
					      this.op = 'update'
					      this.opFileId = r.get('id')
					      this.opR = r
					      var b = fm.getTopToolbar().find('name', 'uploadBtn')[0]
					      b.handler(b)
				      }.dg(this)
			      }]
		    })
		  var g = this.candiFileP = new Ext.grid.GridPanel({
			    plugins : pluginOpCol,
			    region : 'center',
			    store : new Ext.data.JsonStore({
				      url : 'robot-attament!search.action',
				      fields : ['id', 'name', 'type'],
				      root : 'data'
			      }),
			    colModel : new Ext.grid.ColumnModel({
				      defaults : {
					      width : 120,
					      sortable : true
				      },
				      columns : [{
					        dataIndex : 'name',
					        header : '附件名称'
				        }, pluginOpCol]
			      }),
			    width : '100%',
			    height : '100%',
			    frame : true,
			    iconCls : 'icon-grid'
		    })
		  var mgrP = this.mgrP = new RAMgrPanel({
			    title : '已关联的附件',
			    region : 'west',
			    split : true,
			    collapseMode : 'mini',
			    width : 150,
			    ctrlP : this
		    })
		  this.removeAll();
		  var fm = this._buildUploadForm();
		  fm.region = 'north'
		  var filefield = fm.find('name', 'file')[0]
		  var self = this
		  filefield.on('fileselected', function(ths, fn) {
			    fn = fn.replace(/.*[/\\]/, '')
			    this.fn = fn;
			    g.getStore().load({
				      params : {
					      name : fn,
					      topNodeId : self.topNodeId
				      }
			      })
		    })
		  fm.add({
			    xtype : 'textarea',
			    name : 'remark',
			    maxLength : 100,
			    maxLengthText : '备注太长',
			    fieldLabel : '备注'
		    })
		  fm.add({
			    layout : 'column',
			    border : false,
			    bodyStyle : 'background-color:rgb(211,225,241);',
			    items : [{
				      columnWidth : .85,
				      layout : 'form',
				      border : false,
				      bodyStyle : 'background-color:rgb(211,225,241);',
				      items : {
					      xtype : 'textfield',
					      name : 'searchFileName',
					      ref : '/../searchAttachmentFileName',
					      width : 205,
					      emptyText : '选择要搜索的文件名',
					      fieldLabel : '搜索'
				      }
			      }, {
				      columnWidth : .15,
				      xtype : 'button',
				      text : '搜索',
				      handler : function() {
					      var searchName = fm.searchAttachmentFileName.getValue();
					      if (searchName)
						      g.getStore().load({
							        params : {
								        name : searchName,
								        topNodeId : self.topNodeId
							        }
						        })
					      else
						      Ext.Msg.alert('提示', '搜索名称不能为空');
				      }
			      }]
		    })
		  fm.getTopToolbar().find('name', 'uploadBtn')[0].setText('上传新文件')
		  fm.getTopToolbar().find('name', 'uploadBtn')[0].on('click', function() {
			    if (this.op)
				    delete this.op
			    if (this.opFileId)
				    delete this.opFileId
		    }.dg(this))
		  this.validateFile = function() {
			  if (this.op)
				  return;
			  if (this.candiFileP.getStore().find('name', filefield.fn) != -1 || this.mgrP.getRootNode().findByAttributes({
				    text : filefield.fn
			    })) {
				  Ext.Msg.alert('提示', '不能上传文件名相同的附件')
				  return false
			  }
		  }
		  fm.getTopToolbar().add({
			    xtype : 'button',
			    text : '查看引用',
			    iconCls : 'icon-attr-link',
			    handler : function() {
				    var r = this.candiFileP.getSelectionModel().getSelected();
				    if (r)
					    this.showRelation(r.get('id'))
			    }.dg(this)
		    })
		  this.add(new Ext.Panel({
			    layout : 'border',
			    items : [mgrP, {
				      region : 'center',
				      layout : 'border',
				      items : [fm, g]
			      }]
		    }))
	  }
  })

var RAMgrRelationWin = Ext.extend(Ext.Window, {
	  height : 300,
	  width : 300,
	  title : '附件引用关系',
	  layout : 'fit',
	  border : false,
	  closeAction : 'hide',
	  initComponent : function() {
		  this.relationP = new Ext.grid.GridPanel({
			    region : 'center',
			    store : new Ext.data.JsonStore({
				      url : 'robot-attament!relation.action',
				      fields : ['id', 'objectName', 'type'],
				      root : 'data'
			      }),
			    colModel : new Ext.grid.ColumnModel({
				      defaults : {
					      width : 120,
					      sortable : true
				      },
				      columns : [{
					        dataIndex : 'objectName',
					        header : '被关联对象名称'
				        }, {
					        dataIndex : 'type',
					        header : '附件类型',
					        renderer : function(v) {
						        if (v == 0) {
							        return '实例'
						        } else if (v == 1) {
							        return '答案'
						        }
					        }
				        }]
			      }),
			    width : '100%',
			    height : '100%',
			    frame : true,
			    iconCls : 'icon-grid'
		    })
		  this.items = this.relationP
		  RAMgrRelationWin.superclass.initComponent.call(this)
	  },
	  load : function(fileid) {
		  this.relationP.getStore().load({
			    params : {
				    'objectId' : fileid
			    }
		    })
	  }
  })
  
  var delOpCol = new OperationColumn({
			    operationConfig : [{
				      name : '删除',
				      handler : function(grid, row, col, e) {
					      var r = grid.getStore().removeAt(row);
					      Dashboard.setAlert('删除成功！');
				      }.dg(this)
			      }]
  })
  
  var RAMgrCaseRelationWin = Ext.extend(Ext.Window, {
	  height : 450,
	  width : 700,
	  title : '实例关联',
	  layout : 'border',
	  minimizable : false,// 最小化
	  maximizable : true,// 最大化
	  resizable : true,
	  draggable : true,
	  modal : true,
	  plain : true,
	  animCollapse : true,
	  closeAction : 'hide',
//	  init : function(objectRelations){
//		  this._objectRelations = objectRelations;
//	  },
	  initComponent : function() {
		  this.gridContrast =  new Ext.grid.GridPanel({
			  height : 450,
			  region : 'north',
			  viewConfig: {forceFit: true},
			  layout:'fit',
			  border:false,
			  plugins : delOpCol,
			  store : new Ext.data.JsonStore({
			      fields : ['id', 'name', 'type'],
			      root : 'relations'
		    }),
		    colModel : new Ext.grid.ColumnModel({
			      defaults : {
				      sortable : true
			      },
			      columns : [new Ext.grid.RowNumberer(),{
				        dataIndex : 'id',
				        hideable: false, 
				        hidden: true
			        },{
				        dataIndex : 'name',
				        header : '实例对比',
			        	hideable: false
			        },{
				        dataIndex : 'type',
				        hideable: false,
				        hidden: true 
			        },delOpCol]
		    }),
		    height : 200,
		    iconCls : 'icon-grid',
		    tbar : ['->',{
		    	xtype : 'button',
			    text : '添加实例对比',
			    iconCls : 'icon-connect',
			    handler : function() {
			    	var node = this.caseRelationCategoryTree.getSelectedNode();
			    	if (node.attributes.iconCls == 'icon-ontology-object') {
			    		this.caseRelationCategoryTree.doAddContrast(node);
			    	}
			    }.dg(this)
			  },{
				  xtype : 'button',
				    text : '添加实例关联',
				    iconCls : 'icon-connect',
				    handler : function() {
				    	var node = this.caseRelationCategoryTree.getSelectedNode();
				    	if (node.attributes.iconCls == 'icon-ontology-object') {
				    		this.caseRelationCategoryTree.doAddRelevance(node);
				    	}
				    }.dg(this)
			  }]
		  })
		  this.gridRelevance = new Ext.grid.GridPanel({
			    region : 'center',
			    viewConfig: {forceFit: true},
			    frame:false,
			    border:false,
			    layout:'fit',
			    plugins : delOpCol,
			    store : new Ext.data.JsonStore({
				      fields : ['id', 'name', 'type'],
				      root : 'relations'
			      }),
			    colModel : new Ext.grid.ColumnModel({
				      defaults : {
					      sortable : true
				      },
				      columns : [new Ext.grid.RowNumberer(),{
					        dataIndex : 'name',
					        header : '实例关联'
				        },delOpCol]
			      }),
			    height : 200,
			    iconCls : 'icon-grid'
			  })
		  this.caseRelationCategoryTree = new CaseRelationCategoryTree()
		  this.items = [{
				  xtype:'panel',
	              region:'west',
	              width:200,
	              layout:'fit',
	              items:[this.caseRelationCategoryTree]
			  },{
	              title:"",
	              region:"center",
	              layout:'fit',
	              border:false,
	              items:{
	            	  layout:'border',
	            	  border:false,
	            	  items:[this.gridContrast,this.gridRelevance]
	              }
			  }];
		  RAMgrCaseRelationWin.superclass.initComponent.call(this)
	  }
  })
// window.onload = function() {
// new RAFileUploadWindow().show()
// }
