var RaskValueMenuConstructor = function(_cfg) {
	this._cfg = _cfg || {};
	var cfg = {};

	var self = this;
	this.primordialEdit = false;

	this.initDefaultField = function() {
		var items = this.initGroup();
		if (items) {
			for (var i = 0; i < items.length; i++) {
				this.insert(this.items.length - 1, items[i]);
			}
		}
		this.doLayout();
	}
	this.addGroup = function() {
		var items = this.renderGroup();
		for (var i = 0; i < items.length; i++) {
			this.insert(this.items.length - 1, items[i]);
		}
		this.doLayout();
	}
	var saveP4CB = function(id) {
		this.setValue(id)
	}
	this.newParamField = function() {
		var self = this;
		var missElement = Ext.create({
					xtype : 'form',
					border : false,
					fieldLabel : '缺失要素',
					items : [{
						layout : 'column',
						border : false,
						bodyStyle : 'background-color:#f0f0f0;',
						items : [{
									columnWidth : .25,
									border : false,
									layout : 'fit',
									items : [{
												xtype : 'textfield',
												ref : '../../misElement',
												name : 'name'
											}]
								}, {
									columnWidth : .12,
									border : false,
									html : '反问句：',
									bodyStyle : 'background-color:#f0f0f0;',
									style : 'text-align:center;margin-top:4px;'
								}, {
									columnWidth : .63,
									border : false,
									layout : 'fit',
									items : [{
										xtype : 'trigger',
										ref : '../../raskContent',
										fieldType : 'text',
										name : name,
										triggerClass : 'x-form-clear-trigger',
										onTriggerClick : function() {
											self.fieldCompons.splice(
													missElement.index, 1);
											self.remove(missElement);
										}
									}]
								}]
					}]
				});
		missElement.index = this.fieldCompons.length;
		return missElement;
	}
	this.init = function() {
		this.renderUI();
		this.fireEvent('initcomplete')
	};

	this.BUTTONS = {
		OK : {
			xtype : 'button',
			text : '确定',
			colspan : 2,
			width : 50,
			handler : function() {
				var values = "";
				var isRaskEmpty = false;
				var isRaskValue = false;
				var isAnswerValueEmpty = false;
				for (var i = 0; i < self.fieldCompons.length; i++) {
					if (i == 0) {
						var answerField = self.fieldCompons[i];
						var answerValue = answerField.getValue();
						if (answerValue)
							values += answerValue + "\n******\n";
						else
							isAnswerValueEmpty = true;
						continue;
					}
					if (i == 1)
						continue;
					var misEleConpon = self.fieldCompons[i];
					var misElement = misEleConpon.misElement;
					var raskContent = misEleConpon.raskContent;
					var misEleValue = misElement.getValue();
					var raskValue = raskContent.getValue();
					if (raskValue)
					    isRaskValue=true;
					if (!misEleValue) {
						Dashboard.setAlert('提示:缺失要素不能为空！', true, 5);
						return;
					} else
						values += misEleValue;
					if (i != 2
							&& ((isRaskEmpty && raskValue) || (!isRaskEmpty && !raskValue))) {
						Dashboard.setAlert('提示:反问句状态要保持一致！', true, 5);
						return;
					}
					if (i == 2) {
						if (!raskValue)
							isRaskEmpty = true;
						else
							values += "=" + raskValue;
					} else if (raskValue) {
						values += "=" + raskValue;
					}
					values += "\n"
				}
				if (isAnswerValueEmpty && !isRaskValue) {
					Dashboard.setAlert('提示:答案或者反问句不能全为空！', true, 5);
					return;
				}
				self.raskValues = values;
				self.fireEvent('raskChanged')
				self.hide();
			}
		},
		CANCEL : {
			xtype : 'button',
			text : '取消',
			colspan : 2,
			width : 50,
			handler : function() {
				self.hide();
			}
		},
		YES : {
			xtype : 'button',
			text : '自定义',
			colspan : 3,
			width : 50,
			handler : function() {
				_cfg.parent.startEditing(_cfg.row, _cfg.col);
				self.hide();
			}
		}
	}
	this.renderUI = function() {
		var buttons = [];
		if (cfg.buttonsToAdd) {
			buttons = buttons.concat(cfg.buttonsToAdd);
		} else {
			buttons = [this.BUTTONS.OK, this.BUTTONS.CANCEL,this.BUTTONS.YES];
		}
		Ext.each(buttons, function(b) {
					if (!b.width) {
						b.width = 50
					}
				})
		var items = [];
		var buttonGroup = {
			xtype : 'buttongroup',
			layout : 'hbox',
			border : false,
			frame : false,
			defaults : {
				xtype : 'label'
			},
			items : [{
						xtype : 'spacer',
						flex : 1
					}].concat(buttons)
		}
		this.fieldCompons = [];
		items.push(buttonGroup);
		self.add(items);
		self.doLayout();
	}
	this.initGroup = function() {
		var bodyStyle = "background:transparent; border-bottom:1px solid #CCCCCC; margin-bottom:4px;";
		var paramsSet = new Ext.form.FormPanel({
					border : false,
					frame : false,
					padding : 0,
					labelWidth : 50,
					bodyStyle : bodyStyle
				});
		var firstField = new Ext.form.TextArea({
					fieldLabel : '答案',
					name : 'answer',
					height : 50,
					width : 400
				});
		var missEle = Ext.create({
			name : 'missEle',
			xtype : 'form',
			border : false,
			fieldLabel : '缺失要素',
			items : [{
						layout : 'column',
						border : false,
						bodyStyle : 'background-color:#f0f0f0;',
						items : [{
									columnWidth : .25,
									border : false,
									layout : 'fit',
									items : [{
												xtype : 'textfield',
												ref : '../../misElement',
												name : 'name'
											}]
								}, {
									columnWidth : .12,
									border : false,
									html : '反问句：',
									bodyStyle : 'background-color:#f0f0f0;',
									style : 'text-align:center;margin-top:4px;'
								}, {
									columnWidth : .63,
									border : false,
									layout : 'fit',
									items : [{
												xtype : 'trigger',
												ref : '../../raskContent',
												fieldType : 'text',
												name : name,
												triggerClass : 'x-form-add-trigger',
												onTriggerClick : function() {
													self.addGroup();
												}
											}]
								}]
					}]
		});

		var items = [firstField, paramsSet, missEle];

		if (!this.fieldCompons) {
			this.fieldCompons = [];
			firstField.index = 0;
		}
		this.fieldCompons.push(firstField);
		this.fieldCompons.push(paramsSet);
		this.fieldCompons.push(missEle);
		return items;
	}
	this.renderGroup = function() {
		var fieldCompon = self.newParamField();
		var items = [fieldCompon];
		if (!this.fieldCompons) {
			this.fieldCompons = [];
			fieldCompon.index = 0;
		} else {
			fieldCompon.index = this.fieldCompons.length;
		}
		this.fieldCompons.push(fieldCompon);
		return items;
	}

	this.addEvents('raskChanged');

	RaskValueMenu.superclass.constructor.call(this, Ext.apply({
						style : {
							overflow : 'visible',
							'background-image' : 'none'
						},
						layout : 'form',
						labelWidth : 60,
						frame : true
					}, cfg));

	this.on('show', function() {
			}, this);
	this.on('beforehide', function() {
				return !this.__noBlur;
			}, this);

}

var RaskValueMenu = Ext.extend(Ext.menu.Menu, {
			constructor : RaskValueMenuConstructor,
			fillValues : function(raskParams) {
				this.initDefaultField();
				var self = this;
				if (raskParams.length > 0) {
					if (raskParams.length == 1) {// 答案只有一条内容
						var missEle = self.fieldCompons[2];
						if (missEle) {
							var param = raskParams[0];
							var params = [];
							if (param)
								params = param.split("=");
							if (params.length >= 1) {
								var misElement = missEle.misElement;
								misElement.setValue(params[0]);
							}
							if (params.length == 2) {
								var raskContent = missEle.raskContent;
								raskContent.setValue(params[1]);
							}
						}
					} else {
						var answerField = self.fieldCompons[0];
						if (answerField)
							answerField.setValue(raskParams[0]);
						for (var i = 1; i < raskParams.length; i++) {
							if (i >= 2)
								self.addGroup();
							var missEle = self.fieldCompons[i + 1];
							if (missEle) {
								var param = raskParams[i];
								var params = [];
								if (param)
									params = param.split("=");
								if (params.length >= 1) {
									var misElement = missEle.misElement;
									misElement.setValue(params[0]);
								}
								if (params.length == 2) {
									var raskContent = missEle.raskContent;
									raskContent.setValue(params[1]);
								}
							}
						}
					}
				}
			},
			getValue : function() {
				return this.raskValues;
			},
			clear : function() {
				for (var i = 0; i < this.fieldCompons.length; i++) {
					var fieldCompon = this.fieldCompons[i];
					this.remove(fieldCompon);
				}
				this.fieldCompons = [];
			},
			setValue : function(raskVal) {
				this.primordialEdit = false;
				this.clear();
				if (raskVal == null) {
					raskVal = '';
				}
				var arr = raskVal.split(/\r\n|\r|\n/g);
				var raskParams = [];
				if (raskVal.indexOf('******') > -1) {
					for (var i = 0; i < arr.length; i++) {
						if (!arr[i].trim())
							continue;
						var args = arr[i];
						if ("******" != args)
							raskParams.push(args);
					}
				} else {
					raskParams.push('');
					for (var i = 0; i < arr.length; i++) {
						if (!arr[i].trim())
							continue;
						var args = arr[i];
						if (args)
							raskParams.push(args);
					}
				}
				this.fillValues(raskParams);
				this.raskValues = raskVal;
			}
		});
