TagEditWin = function(cfg) {

	var self = this;

	self.callTagField;
	self.callFunction;

	var selectedStore = new Ext.data.JsonStore({
		  mode : 'local',
		  root : 'data',
		  fields : ['tagId', 'tagName', 'valid']
	  });

	this.selectedStore = selectedStore;

	var selectedView = new Ext.DataView({
		  store : selectedStore,
		  tpl : new Ext.XTemplate('<tpl for=".">', '<tpl if="valid &gt; 0"><div class="thumb-wrap" style="background-color:#FBE0C0;cursor:pointer;" id="{tagId}">',
		    '<span class="x-editable">{tagName}<img border="0" src="images/cancel.png" class="deleteClickClass" /></span>', '</div></tpl>',
		    '<tpl if="valid &lt; 1"><div class="thumb-wrap" style="background-color:#A6A6A6;cursor:pointer;" id="{tagId}">',
		    '<span class="x-editable">{tagName}<img border="0" src="images/cancel.png" class="deleteClickClass" /></span>', '</div></tpl>', '</tpl>'),
		  autoHeight : true,
		  multiSelect : true,
		  overClass : 'x-view-over',
		  itemSelector : 'div.thumb-wrap',
		  emptyText : '标签为空',

		  plugins : [new Ext.DataView.DragSelector(), new Ext.DataView.LabelEditor({
			      dataIndex : 'tagId'
		      })],

		  prepareData : function(data) {
			  data.shortName = data.name;
			  return data;
		  },
		  listeners : {
			  'click' : function(view, index, node, e) {
				  var deleteBtn = e.getTarget(".deleteClickClass");
				  if (deleteBtn) {
					  Ext.each(view.store.data.items, function(item) {
						    if (item.data.tagId == node.id) {
							    view.store.remove(item);
							    view.refresh();
							    return false;
						    }
					    })
				  }
			  }
		  }
	  });
	this.selectedView = selectedView;
	var selectTagPanel = new Ext.form.FormPanel({
		  title : '选择的标签',
		  frame : false,
		  border : false,
		  region : 'north',
		  height : 200,
		  split : true,
		  collapseMode : 'mini',
		  iconCls : 'icon-ontology-cate-star',
		  bodyCssClass : 'images-view',
		  bodyStyle : 'overflow-y:scroll',
		  items : selectedView
	  });

	var typeStore = new Ext.data.Store({
          //autoLoad : true,
		  proxy : new Ext.data.HttpProxy({
			    url : 'kbase-tag!getType.action'
		    }),
		  reader : new Ext.data.JsonReader({
			    idProperty : 'typeId',
			    root : 'data',
			    fields : ['typeId', 'typeName', 'valid']
		    }),
		  writer : new Ext.data.JsonWriter()
	  });
	var typeValue = '';
	var tagTypeCombo = new Ext.form.ComboBox({
		  triggerAction : 'all',
		  allowBlank : false,
		  border : false,
		  editable : false,
		  store : typeStore,
		  valueField : 'typeId',
		  emptyText : '请选择类别',
		  displayField : 'typeName',
		  listeners : {
			  'select' : function(combo) {
				  var newTypeValue = combo.getValue();
				  if (newTypeValue != typeValue) {
					  typeValue = newTypeValue;
					  tagStore.baseParams['typeId'] = typeValue
					  tagStore.load();
				  }
			  }
		  }
	  })

	var xd = Ext.data;

	var tagStore = new Ext.data.JsonStore({
		  url : 'kbase-tag!getTags.action',
		  root : 'data',
		  fields : ['tagId', 'tagName', 'valid']
	  });

	var allTagView = new Ext.DataView({
		  store : tagStore,
		  tpl : new Ext.XTemplate('<tpl for=".">', '<div class="thumb-wrap" style="background-color:#FDEABF;cursor:pointer;" id="{tagId}">',
		    '<span class="x-editable">{tagName}</span></div>', '</tpl>', '<div class="x-clear"></div>'),
		  autoHeight : true,
		  singleSelect : true,
		  overClass : 'x-view-over',
		  itemSelector : 'div.thumb-wrap',
		  emptyText : '标签为空',

		  plugins : [new Ext.DataView.DragSelector(), new Ext.DataView.LabelEditor({
			      dataIndex : 'tagId'
		      })],
		  listeners : {
			  selectionchange : {
				  fn : function(dv, node) {
					  if (node.length > 0) {
						  var isSelected = false;
						  Ext.each(selectedView.getNodes(), function(selectedNode) {
							    if (selectedNode.id == node[0].id) {
								    isSelected = true;
								    Ext.Msg.alert('提示', '"' + node[0].textContent + '"已经被选中');
								    return false;
							    }
						    });
						  if (isSelected)
							  return;
						  Ext.each(dv.store.data.items, function(item) {
							    if (item.data.tagId == node[0].id) {
								    selectedView.store.data.add(item);
								    selectedView.refresh();
								    return false;
							    }
						    });
					  }
				  }
			  }
		  }
	  })

	var allTagPanel = new Ext.form.FormPanel({
		  title : '待选择标签',
		  border : false,
		  region : 'center',
		  iconCls : 'icon-folder',
		  layout : 'fit',
		  tbar : ['标签类别：', tagTypeCombo],
		  bodyCssClass : 'images-view',
		  bodyStyle : 'overflow-y:scroll',
		  items : allTagView
	  });

	var _cfg = {
		closeAction : 'hide',
		width : 700,
		height : 500,
		plain : true,
		border : false,
		maximizable : true,
		collapsible : true,
		title : cfg.title,
		layout : 'border',
		items : [selectTagPanel, allTagPanel],
		listeners : {
			'beforehide' : function() {
				var tagIds = [];
				Ext.each(self.selectedView.store.data.items, function(item) {
					  tagIds.push(item.data.tagId);
				  });
				if (tagIds.length == 0) {
					if (confirm('未选择标签，是否要关闭？')) {
						if (typeof(self.callFunction) == 'function')
							self.callFunction('', self.callFunction);
					} else
						return false;
				} else if (typeof(self.callFunction) == 'function')
					self.callFunction(tagIds.join(","), self.callFunction);
			}
		}
	}

	TagEditWin.superclass.constructor.call(this, _cfg);
}
Ext.extend(TagEditWin, Ext.Window, {
	  setData : function(tagIds) {
		  this.selectedStore.removeAll();
		  var self = this;
		  if (tagIds) {
			  Ext.Ajax.request({
				    url : 'kbase-tag!showTags.action',
				    params : {
					    'tagIds' : tagIds
				    },
				    success : function(resp) {
					    var result = Ext.util.JSON.decode(resp.responseText)
					    if (result.success)
						    self.selectedStore.loadData(result);
				    },
				    failure : function(resp) {
					    Ext.Msg.alert('错误', resp.responseText);
				    },
				    scope : this
			    });
		  }
	  },
	  callFunction : function(tagIds, callback) {
		  callback.call(callback, tagIds);
	  }
  })
