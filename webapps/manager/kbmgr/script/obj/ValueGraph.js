var ValueGraph = {
	styles : {
		rvalue : {
			r : [ 40, 40 ],
			color : '#FFF',
			borderColor : '#50D2F3',
			borderWidth : 5,
			fontColor : '#50D2F3',
			fontSize : 16
		},
		value : {
			r : [ 25, 25 ],
			color : '#FFF',
			borderColor : '#50D2F3',
			borderWidth : 3,
			fontColor : '#50D2F3',
			fontSize : 12
		},
		other : {
			r : [ 25, 25 ],
			color : '#FFF',
			borderColor : '#8A916F',
			borderWidth : 3,
			fontColor : '#8A916F',
			fontSize : 12
		},
		object : {
			r : [ 25, 25 ],
			color : '#FFF',
			borderColor : '#69DA69',
			borderWidth : 3,
			fontColor : '#69DA69',
			fontSize : 12
		},
		wordclass : {
			r : [ 25, 25 ],
			color : '#FFF',
			borderColor : '#9564E0',
			borderWidth : 3,
			fontColor : '#9564E0',
			fontSize : 12
		},
		clazz : {
			r : [ 25, 25 ],
			color : '#FFF',
			borderColor : '#F39150',
			borderWidth : 3,
			fontColor : '#F39150',
			fontSize : 12
		},
		'wordclass-pos' : {
			r : [ 25, 25 ],
			color : '#FFF',
			borderColor : '#C7B87C',
			borderWidth : 3,
			fontColor : '#C7B87C',
			fontSize : 12
		},
		word : {
			r : [ 25, 25 ],
			color : '#FFF',
			borderColor : '#C0BEFE',
			borderWidth : 3,
			fontColor : '#C0BEFE',
			fontSize : 12
		}
	}
};
ValueGraph.Window = Ext.extend(Ext.Window, {
	width : 1024,
	title : '小i知识图谱',
	iconCls : 'icon-graph',
	defaults : {
		border : false
	},
	modal : true,
	plain : true,
	shim : true,
	closable : true,
	closeAction : 'hide',
	collapsible : true,
	resizable : false,
	draggable : true,
	animCollapse : true,
	constrainHeader : true,
	shadow : false,
	listeners : {
		'beforehide' : function(p) {
		}
	},
	initComponent : function() {
		this.items = [ {
			items : this.tabpanel = new ValueGraph.TabPanel()
		} ];
		ValueGraph.Window.superclass.initComponent.call(this);
	}
});

ValueGraph.TabPanel = Ext.extend(Ext.TabPanel, {
	activeTab : 0,
	initComponent : function() {
		ValueGraph.tabPanel=this;
		this.gp = new ValueGraph.GraphPanel();
		this.dl = new ValueGraph.AnalysePanel();
		this.border = false;
		this.items = [ {
			items : this.gp,
			title : '语义图谱'
		}, {
			items : this.dl,
			title : '语义分析'
		} ];
		ValueGraph.TabPanel.superclass.initComponent.call(this);
	}
});

ValueGraph.GraphPanel = Ext.extend(Ext.Panel, {
	initComponent : function() {
		var self = this;
		var _getColorRef = function(color, ref) {
			return '<span style="background-color:' + color
					+ ';width:8px;height:8px;margin-left:8px;border:0px solid rgb(153,0,51);">&nbsp;&nbsp;&nbsp;</span>' + ref;
		};
		var _styles = ValueGraph.styles;
		this.tbar = new Ext.Toolbar({
			items : [
					'标准问题:',
					{
						id : 'f_',
						ref : 'f_value',
						xtype : 'textfield',
						width : 200,
						emptyText : '请输入标准问题',
						// value : '红米价格是多少',
						enableKeyEvents : true,
						listeners : {
							specialkey : function(field, e) {
								if (e.getKey() == e.ENTER) {
									self.ask(self.getTopToolbar().f_value.getValue().trim());
								}
							}
						}
					},
					{
						text : '确定',
						iconCls : 'icon-search',
						handler : function() {
							self.ask(self.getTopToolbar().f_value.getValue().trim());
						}
					},
					'-',
					'颜色说明: ' + _getColorRef(_styles.clazz.borderColor, '类型') + _getColorRef(_styles.object.borderColor, '概念')
							+ _getColorRef(_styles.value.borderColor, '知识点') + _getColorRef(_styles.wordclass.borderColor, '词类')
							+ _getColorRef(_styles.word.borderColor, '词') + _getColorRef(_styles['wordclass-pos'].borderColor, '词性')
							+ _getColorRef(_styles.other.borderColor, '实体') ]
		});
		this.width = 1200;
		this.height = 700;
		this.border = false;
		this.html = '<iframe id="vgraphFrame" src="vgraph.html" frameborder=0 scrolling=auto width="100%" height="100%"></iframe>';
		ValueGraph.GraphPanel.superclass.initComponent.call(this);
	},
	ask : function(val) {
		if (!val) {
			Dashboard.setAlert('请输入标准问题', 'error');
			return false;
		}
		this.load(val);
	},
	load : function(question) {
		document.getElementById('vgraphFrame').contentWindow.load(question);
	}
});

ValueGraph.AnalysePanel = Ext.extend(Ext.Panel, {
	initComponent : function() {
		var self = this;
		this.tbar = new Ext.Toolbar({
			items : [
					'标准问题:',
					{
						id : 'a_',
						ref : 'a_value',
						xtype : 'textfield',
						width : 200,
						emptyText : '请输入标准问题',
						// value : '红米价格是多少',
						enableKeyEvents : true,
						listeners : {
							specialkey : function(field, e) {
								if (e.getKey() == e.ENTER) {
									self.ask(self.getTopToolbar().a_value.getValue().trim());
								}
							}
						}
					},
					{
						text : '确定',
						iconCls : 'icon-search',
						handler : function() {
							self.ask(self.getTopToolbar().a_value.getValue().trim());
						}
					}] 
		 });
		this.height = 700;
		this.border = false;
		this.html = '<iframe id="analyseFrame" src="analyse.html" frameborder=0 scrolling=auto width="100%" height="100%"></iframe>';
		ValueGraph.AnalysePanel.superclass.initComponent.call(this);
	},
	ask : function(val) {
		if (!val) {
			Dashboard.setAlert('请输入标准问题', 'error');
			return false;
		}
		this.load(val);
	},
	load : function(question) {
		document.getElementById('analyseFrame').contentWindow.load(question);
	}
})



