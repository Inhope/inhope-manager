var VariableEditGrid = Ext.extend(Ext.grid.EditorGridPanel, {
	  clicksToEdit : 1,
	  initComponent : function() {
		  this.bbar = [{
			    text : '删除',
			    iconCls : 'icon-delete',
			    handler : function() {
				    var rs = this.getSelectionModel().getSelections()
				    if (rs && rs.length) {
					    Ext.each(rs, function(r) {
						      r.data = {}
					      })
					    this.getView().refresh()
				    }
			    }.dg(this)
		    }];
		  this.selModel = new Ext.grid.CheckboxSelectionModel();
		  this.autoExpandColumn = 'value'
		  this.store = new Ext.data.JsonStore({
			    fields : [{
				      name : 'id'
			      }, {
				      name : 'name'
			      }, {
				      name : 'value'
			      }, {
				      name : 'label'
			      }, {
				      name : 'type'
			      }],
			    root : 'data',
			    idProperty : 'id'
		    })
		  this.typeEditor = Ext.create({
			    xtype : 'combo',
			    editable : false,
			    triggerAction : 'all',
			    mode : 'local',
			    store : new Ext.data.ArrayStore({
				      fields : ['v', 't'],
				      data : [[0, '字符串'], [1, '整数'], [2, '浮点'], [3, '日期'], [4, '实例']]
			      }),
			    valueField : 'v',
			    displayField : 't'
		    })
		  this.columns = [this.selModel, {
			    dataIndex : 'name',
			    header : '变量名称',
			    allowBlank : false,
			    width : 150,
			    editor : {
				    xtype : 'textfield'
			    }
		    }, {
			    id : 'value',
			    dataIndex : 'value',
			    header : '变量值',
			    allowBlank : false,
			    editor : {
				    xtype : 'textfield'
			    }
		    }, {
			    dataIndex : 'label',
			    header : '变量说明',
			    editor : {
				    xtype : 'textfield'
			    }
			    // }, {
			   // dataIndex : 'description',
			   // header : '描述文字',
			   // editor : {
			   // xtype : 'textfield'
			   // }
		   }, {
			    dataIndex : 'type',
			    header : '类型',
			    editor : this.typeEditor,
			    renderer : function(v) {
				    if (v != null) {
					    var i = this.typeEditor.store.find('v', v)
					    if (i != -1) {
						    return this.typeEditor.store.getAt(i).get('t')
					    }
				    }
				    return v
			    }.dg(this)
		    }]
		  VariableEditGrid.superclass.initComponent.call(this);
		  this.on('afteredit', function(e) {
			    if (e.field == 'value' && e.record.get('type') == 4) {
					return false;
			    }
		    });
	  },
	  // private
	  margins : '0',
	  border : true,
	  bodyStyle : 'background-color:' + sys_bgcolor + ' padding : 0;'
  })
var VariableEditWin = Ext.extend(Ext.Window, {
	  /**
			 * @type {VariableEditGrid}
			 */
	  initComponent : function() {
		  this.editG = new VariableEditGrid();
		  this.items = [this.editG];
		  VariableEditWin.superclass.initComponent.call(this)
		  this.on('beforehide', function() {
			    this.fillData()
		    })
	  },
	  loadData : function(data) {
		  this._data = data
		  var st = this.editG.store
		  st.removeAll()
		  st.loadData({
			    data : data
		    });
		  if (st.getCount() < 20) {
			  this.batchAdd(20, true);
		  }
		  this.on('scrollbottomclick', function() {
			    this.batchAdd(10);
		    }, this)
	  },
	  batchAdd : function(n, includePres) {
		  var st = this.editG.store;
		  for (var i = (includePres ? st.getCount() : 0); i < n; i++) {
			  st.add(new Ext.data.Record({}));
		  }
	  },
	  fillData : function() {
		  this._data.splice(0, this._data.length)
		  this.editG.stopEditing()
		  this.editG.store.each(function(r) {
			    if (!ux.util.isEmptyData(r.data, 'id')){
				    this._data.push(r.data)
			    }
		    }, this);
	  },
	  closeAction : 'hide',
	  width : 700,
	  height : 400,
	  plain : true,
	  maximizable : true,
	  collapsible : true,
	  layout : 'fit',
	  border : false,
	  title : '变量列表'
  })
