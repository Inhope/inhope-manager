Ext.namespace('obj.datatrans')
obj.datatrans.ImportWin = Ext.extend(Ext.Window, {
	  getUploadParams : function() {
		  return {}
	  },
	  initComponent : function() {
		  var _fileForm = this.formP = this._buildForm();
		  var _importPanel = this._importPanel = new Ext.Panel({
			    autoHeight : true,
			    layout : 'fit',
			    layoutConfig : {
				    animate : true
			    },
			    items : [_fileForm],
			    buttons : [{
				      text : "开始导入",
				      handler : function() {
					      // _fileForm.find('name',
					      // 'importFile')[0].getValue()
					      if (!_fileForm.getForm().isValid()) {
						      Ext.Msg.alert("错误提示", "表单未填写完整");
					      } else {
						      // 开始导入
						      var exparams = Ext.apply(this.extraValues || {}, this.getUploadParams())
						      _fileForm.getForm().submit({
							        url : this.importURL,
							        params : Ext.apply(exparams, {
								          categoryId : this.cateId,
								          objectId : this.objectId || '',
								          ispub : this.ispub
							          }),
							        success : function(form, action) {
								        this.pgTimer = new ProgressTimer({
									          initData : action.result.data,
									          progressText : 'percent',
									          progressId : this.progressId,
									          boxConfig : {
										          title : this.title
									          },
									          poll : this.progressPoll,
									          finish : this.finishCb,
									          scope : this
								          });
								        this.pgTimer.start();
							        },
							        failure : function(form) {
								        Ext.MessageBox.hide();
								        Ext.Msg.alert('导入失败', '文件上传失败');
							        },
							        scope : this
						        });
					      }
				      }.dg(this)
			      }, {
				      text : "关闭",
				      handler : function() {
					      this.hide();
				      }.dg(this)
			      }]
		    });
		  this.items = [this._importPanel]
		  obj.datatrans.ImportWin.superclass.initComponent.call(this)
	  },
	  doImport : function(cateId, ispub, formValues, extraValues) {
		  this.cateId = cateId ? cateId : '';
		  this.ispub = ispub;
		  this.show();
		  if (formValues)
			  this.formP.getForm().setValues(formValues)
		  this.extraValues = extraValues
	  },

	  _buildForm : function() {
		  return new Ext.FormPanel({
			    frame : true,
			    border : false,
			    autoHeight : true,
			    waitMsgTarget : true,
			    defaults : {
				    bodyStyle : 'padding:10px'
			    },
			    margins : '0 0 0 0',
			    labelAlign : "left",
			    labelWidth : 50,
			    fileUpload : true,
			    items : [{
				      xtype : 'fieldset',
				      ref : 'fileFieldset',
				      title : '选择文件',
				      autoHeight : true,
				      items : [{
					        name : 'file',
					        xtype : "textfield",
					        fieldLabel : '文件',
					        inputType : 'file',
					        anchor : '96%',
					        allowBlank : false
				        }]
			      }, {
				      xtype : 'fieldset',
				      ref : 'optionsFieldset',
				      title : '选择导入模式',
				      autoHeight : true,
				      items : [{
					        name : 'cleanAction',
					        xtype : 'radio',
					        hideLabel : true,
					        boxLabel : '新增导入（只新增，不替换任何原有数据）',
					        checked : true,
					        inputValue : ""
				        }, {
					        name : 'cleanAction',
					        xtype : 'radio',
					        hideLabel : true,
					        boxLabel : '同步导入（替换对应分类下的所有数据）',
					        inputValue : "cleanRest"
				        }, {
					        name : 'cleanAction',
					        xtype : 'radio',
					        hideLabel : true,
					        disabled : true,
					        boxLabel : '清空导入（清除对应分类下所有的数据）',
					        inputValue : "cleanAll"
				        }]
			      }]
		    });
	  },
	  title : '实例导入',
	  importURL : 'ontology-object-data-trans!doImport.action',
	  progressId : 'objectImport',
	  finishCb : function(p, response) {
		  if (p.currentCount == -3) {
			  return true;
		  } else if (p.currentCount == -2) {
			  if (this.pgTimer.msgbox)
				  this.pgTimer.msgbox.hide()
			  Ext.Msg.updateProgress(0, '', p.message);
			  if (!this.downloadIFrame) {
				  this.downloadIFrame = this.getEl().createChild({
					    tag : 'iframe',
					    style : 'display:none;'
				    })
			  }
			  this.downloadIFrame.dom.src = this.importURL.replace(/\![^.]+/i, '!downFile') + '?_t=' + new Date().getTime();
		  } else if (p.currentCount == -1) {
			  // Ext.Msg.alert('导入失败', p.message);
		  } else if (p.currentCount == 100) {
			  this.fireEvent('importSuccess', [this.cateId]);
		  }
		  delete this.pgTimer;
	  },
	  width : 520,
	  autoHeight : true,
	  defaults : {
		  border : false
	  },

	  plain : true,// 方角 默认
	  modal : true,
	  plain : true,
	  shim : true,
	  closeAction : 'hide',
	  closable : true, // 关闭
	  constrainHeader : true
  })
obj.datatrans.ObjectImportWin = Ext.extend(obj.datatrans.ImportWin, {
	  doImport : function(cateId, ispub, formValues, objectId) {
		  this.objectId = objectId;
		  if (!formValues) {
			  formValues = formValues || {}
			  var initlevel = Dashboard.getModuleProp('kbmgr.import_obj_init_option');
			  if (!initlevel) {
				  initlevel = !this.isPreferred('defaultSelectSaveOnly') ? this.LEVEL_TOP + 100 : this.LEVEL_OBJ
			  }
			  Ext.applyIf(formValues, {
				    updateLevel : initlevel
			    })

		  }
		  var objectLevelImport = formValues.updateLevel == this.LEVEL_OBJ + 100;
		  this.formP.fs_type[isVersionStandard() ? 'hide' : 'show']();
		  this.formP.fs_saveonly[isVersionStandard() ? 'hide' : 'show']();
		  this.formP.fs_sync[isVersionStandard() ? 'hide' : 'show']();
		  this.formP.fs_sync['topall'][objectId ? 'disable' : 'enable']();
		  this.formP.fs_sync['category'][objectId ? 'disable' : 'enable']();

		  obj.datatrans.ObjectImportWin.superclass.doImport.call(this, cateId, ispub, formValues)
	  },
	  isPreferred : function(key) {
		  return this._pref && this._pref[key]
	  },
	  _buildForm : function() {
		  var LEVEL_TOP = this.LEVEL_TOP = 1;
		  var LEVEL_CATE = this.LEVEL_CATE = 2;
		  var LEVEL_OBJ = this.LEVEL_OBJ = 4;
		  var LEVEL_VAL = this.LEVEL_VAL = 8;
		  var LEVEL_FAQ = this.LEVEL_FAQ = 16;
		  var LEVEL_DIM_VAL = this.LEVEL_DIM_VAL = 17;
		  var LEVEL_DIM_VAL_TAG = this.LEVEL_DIM_VAL_TAG = 34;
		  var res = http_get('property!getMsgRes.action?key=kb.importObjPreferrence');
		  if (res) {
			  this._pref = {}
			  Ext.each(res.split(','), function(p) {
				    this._pref[p] = true
			    }, this)
		  }
		  var fm = new Ext.FormPanel({
			    frame : true,
			    border : false,
			    autoHeight : true,
			    waitMsgTarget : true,
			    defaults : {
				    bodyStyle : 'padding:2px'
			    },
			    margins : '0 0 0 0',
			    labelAlign : "left",
			    labelWidth : 0,
			    fileUpload : true,
			    items : [{
				      xtype : 'fieldset',
				      title : '选择文件',
				      autoHeight : true,
				      items : [{
					        name : 'file',
					        xtype : "textfield",
					        fieldLabel : '文件',
					        inputType : 'file',
					        anchor : '96%',
					        allowBlank : false
				        }]
			      }, {
				      xtype : 'fieldset',
				      ref : 'fs_type',
				      title : '导入数据类型',
				      autoHeight : true,
				      defaultType : 'radio',
				      layout : 'column',
				      defaults : {
					      style : 'margin-left:10px'
				      },
				      items : [{
					        name : 'includeFaq',
					        hideLabel : true,
					        boxLabel : '全部数据（维度值+扩展问）',
					        handler : function(t, checked) {
						        if (checked)
							        this.formP.fs_sync.findBy(function(cmp) {
								          cmp.enable();
							          }, this)
					        }.dg(this),
					        inputValue : 1,
					        checked : true
				        }, {
					        name : 'includeFaq',
					        hideLabel : true,
					        boxLabel : '基础数据（只包含维度值）',
					        handler : function(t, checked) {
						        if (!checked)
							        return;
						        var checkToReset = false;
						        this.formP.fs_sync.findBy(function(cmp) {
							          var v = parseInt(cmp.inputValue);
							          if (v > 100)
								          v -= 100;
							          if (checked && v < LEVEL_DIM_VAL) {
								          if (cmp.getValue()) {
									          checkToReset = true
								          }
								          cmp.disable();
							          } else {
								          cmp.enable();
							          }
						          }, this)
						        if (checkToReset) {
							        this.formP.fs_sync.dim_val.setValue(true)
						        }
					        }.dg(this),
					        inputValue : 0
				        }, {
					        name : 'includeFaq',
					        hideLabel : true,
					        boxLabel : '简单问答（只包含问题和答案）',
					        handler : function(t, checked) {
						        if (!checked)
							        return;
					        }.dg(this),
					        inputValue : 2
				        }, {
					        name : 'includeId',
					        xtype : 'checkbox',
					        hideLabel : true,
					        boxLabel : '包含ID列',
					        inputValue : true
				        }, {
					        name : 'disableSemanticSuggest',
					        xtype : 'checkbox',
					        hideLabel : true,
					        boxLabel : '禁用语义推荐',
					        inputValue : true
				        }]
			      }, {
				      xtype : 'fieldset',
				      ref : 'fs_saveonly',
				      title : '新增导入(只新增，不替换任何原有数据)',
				      autoHeight : true,
				      defaultType : 'radio',
				      layout : 'column',
				      defaults : {
					      style : 'margin-left:10px'
				      },
				      items : [{
					        name : 'updateLevel',
					        xtype : 'radio',
					        boxLabel : '属性',
					        inputValue : LEVEL_OBJ
				        }, {
					        name : 'updateLevel',
					        xtype : 'radio',
					        boxLabel : '维度值+扩展问',
					        inputValue : LEVEL_VAL
				        }]
			      }, {
				      xtype : 'fieldset',
				      ref : 'fs_sync',
				      title : '同步导入(替换对应级别下的所有数据)',
				      autoHeight : true,
				      defaultType : 'radio',
				      layout : 'column',
				      defaults : {
					      style : 'margin-left:5px'
				      },
				      items : [{
					        name : 'updateLevel',
					        ref : 'topall',
					        boxLabel : '全部',
					        inputValue : LEVEL_TOP + 100
				        }, {
					        name : 'updateLevel',
					        ref : 'category',
					        boxLabel : '分类',
					        inputValue : LEVEL_CATE + 100
				        }, {
					        name : 'updateLevel',
					        hideLabel : true,
					        boxLabel : '实例',
					        inputValue : LEVEL_OBJ + 100
				        }, {
					        name : 'updateLevel',
					        boxLabel : '属性',
					        inputValue : LEVEL_VAL + 100
				        }, {
					        ref : 'dim_val',
					        name : 'updateLevel',
					        boxLabel : '维度',
					        inputValue : LEVEL_DIM_VAL + 100
				        }]
			      }]
		    });
		  Ext.each(fm.find('name', 'updateLevel'), function(f) {
			    f.on('specialkey', function(f, e) {
				      if (e.ctrlKey) {
					      Ext.Msg.alert('导入级别', f.inputValue)
				      }
			      })
		    })
		  return fm
	  }
  })
obj.datatrans.ClassImportWin = Ext.extend(obj.datatrans.ImportWin, {
	  title : '本体类导入',
	  importURL : 'ontology-class-data-trans!doImport.action',
	  progressId : 'classImport',
	  progressPoll : function(p, response) {
		  if (!this.msgbox && p.data) {
			  this.pgTimer.pause();
			  var info = new Ext.XTemplate('本次导入终止，需要删除的属性{allCount}个；由于其中有部分属性被' + (p.data.target ? '本体类' : '实例') + '关联，无法进行删除操作，详情如下（' + '<tpl for="countInfo">' + '"{N}"关联{C}条;</tpl>）')
			    .apply(p.data);
			  this.msgbox = Ext.Msg.show({
				    title : '导入终止',
				    msg : info,
				    buttons : {
					    // yes : '确认',
					    no : '确定'
				    },
				    fn : function(choice) {
					    Ext.Ajax.request({
						      url : 'ontology-class-data-trans!confirmImport.action',
						      params : {
							      abort : 'no' == choice ? '1' : ''
						      },
						      success : function(response) {
							      this.msgbox.hide();
							      delete this.msgbox
							      this.pgTimer.restart(50);
						      },
						      failure : function(response) {
							      this.msgbox.hide();
							      delete this.msgbox
							      this.pgTimer.restart(50);
						      },
						      scope : this
					      });
				    },
				    scope : this,
				    icon : Ext.Msg.QUESTION,
				    minWidth : Ext.Msg.minWidth
			    });
			  this.msgbox.getDialog().getFooterToolbar().show()
			  var st = new Date().getTime();
			  var et = st + p.data.timeout * 1000;

			  var tt = setInterval(function() {
				    var left = et - new Date().getTime();
				    if (left > 1000 && this.msgbox)
					    this.msgbox.getDialog().setTitle('确认(剩余' + parseInt(left / 1000) + '秒)')
				    else {
					    clearInterval(tt);
					    if (this.msgbox) {
						    this.msgbox.hide();
						    delete this.msgbox
						    this.pgTimer.restart(50);
					    }
				    }
			    }.dg(this), 1000)
		  }
	  }
  })
obj.datatrans.LegacyImportWin = Ext.extend(obj.datatrans.ImportWin, {
	title : '遗留数据导入',
	importURL : 'legacy-faq-data-trans!doImport.action',
	progressId : 'legacyImport',
	_buildForm : function() {
		return new Ext.FormPanel({
			  frame : true,
			  border : false,
			  autoHeight : true,
			  waitMsgTarget : true,
			  defaults : {
				  bodyStyle : 'padding:10px'
			  },
			  margins : '0 0 0 0',
			  labelAlign : "left",
			  labelAlign : 'top',
			  fileUpload : true,
			  items : [{
				    xtype : 'fieldset',
				    ref : 'fileFieldset',
				    title : '选择文件',
				    autoHeight : true,
				    items : [{
					      name : 'file',
					      xtype : "textfield",
					      fieldLabel : '文件',
					      inputType : 'file',
					      anchor : '96%',
					      allowBlank : false
				      }]
			    }, {
				    xtype : 'textarea',
				    anchor : '100%',
				    height : 60,
				    name : 'defaultCategoryDims',
				    fieldLabel : '顶级分类默认维度,格式为{"旧分类名":"新维度名"}',
				    value : '{"掌厅业务":"WAP","短厅业务":"短信","全省":"WEB,FETION"}'
			    }, {
				    xtype : 'textarea',
				    anchor : '100%',
				    height : 50,
				    name : 'dimsToNew',
				    fieldLabel : '维度名字转换,格式为{"旧维度名":"新维度名"}',
				    value : '{"FETION":"飞信"}'
			    }, {
				    xtype : 'textarea',
				    anchor : '100%',
				    height : 60,
				    name : 'allowCitys',
				    fieldLabel : '允许地市(顶级分类)',
				    allowBlank : false,
				    value : ''
			    }]
		  });
	}
	 // initComponent : function() {
	 // obj.datatrans.LegacyImportWin.superclass.initComponent
	 // .call(this);
	 // this.formP.optionsFieldset.hide();
	 // }
 })

obj.datatrans.categoryDetailImportWin = Ext.extend(obj.datatrans.ImportWin, {
	  title : '分类详情数据导入',
	  importURL : 'ontology-object-data-trans!doCategoryDetailImport.action',
	  progressId : 'categoryDetailImport',
	  _buildForm : function() {
		  return new Ext.FormPanel({
			    frame : true,
			    border : false,
			    autoHeight : true,
			    waitMsgTarget : true,
			    defaults : {
				    bodyStyle : 'padding:10px'
			    },
			    margins : '0 0 0 0',
			    labelAlign : "left",
			    labelAlign : 'top',
			    fileUpload : true,
			    items : [{
				      xtype : 'fieldset',
				      ref : 'fileFieldset',
				      title : '选择文件',
				      autoHeight : true,
				      items : [{
					        name : 'file',
					        xtype : "textfield",
					        fieldLabel : '文件',
					        inputType : 'file',
					        anchor : '96%',
					        allowBlank : false
				        }]
			      }]
		    });
	  }
  })