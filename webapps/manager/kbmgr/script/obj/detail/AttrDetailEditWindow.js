/**
 * @include "/ontologybase/src/main/webapp/kbmgr/script/obj/detail/MainPanel.js"
 * @include "/ontologybase/src/main/webapp/kbmgr/script/obj/detail/AttrEditPanel.js"
 * @include "/ontologybase/src/main/webapp/kbmgr/script/LocalStore.js"
 */
Ext.namespace('obj.detail')
obj.detail.TYPE_SAMPLE_REMOVED = 100;
obj.detail.TYPE_FAQ_REMOVED = 101;
obj.detail.TYPE_FAQ_MODIFIED = 102;
obj.detail.TYPE_FAQ4ATTRSPL = 103;
obj.detail.AttrDetailEditWindow = function(cfg) {
	this.TYPE_SAMPLE_REMOVED = obj.detail.TYPE_SAMPLE_REMOVED;
	this.TYPE_FAQ_REMOVED = obj.detail.TYPE_FAQ_REMOVED;
	this.TYPE_FAQ_MODIFIED = obj.detail.TYPE_FAQ_MODIFIED;
	this.TYPE_FAQ4ATTRSPL = obj.detail.TYPE_FAQ4ATTRSPL;

	var self = this;
	this.cfg = cfg || {};
	var templateRenderer = Dashboard.utils.newTemplateRenderer();
	var faqTypeCombo = Dashboard.utils.newFaqTypeCombo();
	var faqTypeRender = Dashboard.utils.newFaqTypeRenderer();

	this.valueRecords = '';
	var classNameTextField = new Ext.form.TextField({
		  allowBlank : false,
		  blankText : "请输入本体类别名称",
		  anchor : '100%',
		  fieldLabel : '本体类别名称'
	  });
	// samples
	var SampleRecord = Ext.data.Record.create(['sampleId', 'sample', 'ruleSampleId', '_RM', '_isRule', '_old' ,'status']);
	var RuleRecord = Ext.data.Record.create(['question', 'type']);
	var statusRenderVal = ['','自定义','自学习*','自学习'];
	this.RuleRecord = RuleRecord;
	this.sampleRd = function(value, metaData, r) {
		if (!value)
			return value
		if (r.get('_isRule')) {
			metaData.attr = 'style=background-color:#eee'
		}
		if (r.get('ruleSampleId')) {
			value = this.flagCompactBlock(value, true);
		}
		if (r.get('ruleSampleId')) {
			value = value.replace(/#@(.+?)#@/ig, '<font color=red><b>$1</b></font>')
			value = value.replace(/(\{.*?\})/ig, '<font color=red><b>$1</b></font>')
		}
		return value
	}
	var sampleEditGrid = this.sampleEditGrid = new ExcelLikeGrid({
		  region : 'east',
		  autoExpandColumn : 'sample',
		  margin : '5 5 5 5',
		  width : 300,
		  split : true,
		  style : 'border-width:1px;border-right-width:0px',
		  border : false,
		  clicksToEdit : 1,
		  frame : false,
		  columnLines : true,
		  selModel : new Ext.grid.RowSelectionModel(),
		  pasteable : true,
		  columns : [new Ext.grid.RowNumberer(), {
			    id : 'sample',
			    header : '模板样例',
			    dataIndex : 'sample',
			    width : 120,
			    editor : new Ext.form.TextArea(),
			    renderer : this.sampleRd.dg(this)
		    }, new obj.detail.FaqDynDeleteButtoner({
			      isRecoverBtn : function(r) {
				      return r.get('ruleSampleId') && r.get('_old')
			      }
		      })],
		  store : new LocalStore({
			    reader : new Ext.data.JsonReader({
				      idProperty : 'sampleId',
				      root : 'samples',
				      fields : SampleRecord
			      })
		    })
	  });
	this.allFaqStore = new LocalStore({
		  reader : new Ext.data.JsonReader({
			    idProperty : 'faqId',
			    root : 'faqs',
			    fields : ['faqId', 'question', 'type', 'ruleId', 'samples', 'rule', '_isRule', '_RM', '_isRule', '_old']
		    }),
		  remoteSort : false,
		  __filters : null,
		  listeners : {
			  load : function(store) {
				  store.multiSort([{
					      field : '_isRule'
				      }, {
					      field : 'type',
					      direction : 'DESC'
				      }], 'ASC');
			  }
		  },
		  filterIt : function(__filters) {
			  if (__filters)
				  this.__filters = __filters;
			  else
				  __filters = this.__filters;

			  this.filter(__filters);
		  }
	  })
	this.presetStore = {
		baseParams : {},
		onload : Ext.emptyFn,
		load : function(cb) {
			var r0 = this.attrDtEd.store.getAt(0);
			this.presetStore.baseParams['osb'] = this._data.object.semanticBlock;
			if (r0) {
				this.presetStore.baseParams['vsb'] = r0.get('semanticBlock');
				var sbx = r0.get('sbx')
				if (sbx) {
					if (r0.get('attributeType') == 3) {
						this.presetStore.baseParams['sbx'] = Ext.encode(sbx)
					} else {
						!sbx.XXX ? 0 : this.presetStore.baseParams['osb'] = sbx.XXX
						!sbx.YYY ? 0 : this.presetStore.baseParams['vsb'] = sbx.YYY
					}
				}
			}
			Ext.Ajax.request(({
				  url : 'ontology-object!listPresetFaq.action',
				  params : this.presetStore.baseParams,
				  success : function(response) {
					  var result = Ext.decode(response.responseText);
					  this._bstrXY = result.attachment;
					  this.presetStore.onload(result.data)
					  cb();
				  },
				  failure : function() {
					  cb();
				  },
				  scope : this
			  }))
		}.dg(this)
	}
	// events
	this.flagCompactBlock = function(value, plain) {
		var xyCompactBlock = this.getBlockStringXY();
		if (!xyCompactBlock)
			return value
		var raw = value;
		var offset = plain ? 1 : 0;
		for (var j = offset; j < xyCompactBlock.length; j += 2) {
			var ii = offset + j;
			if (xyCompactBlock[ii]) {
				var i = value.indexOf(xyCompactBlock[ii]);
				if (i != -1) {
					var p = /#@/ig
					var c = 0;
					while (p.test(value.substring(0, i)))
						c++
					if (c % 2 == 0)// 非嵌套才能累加
						value = value.substring(0, i) + '#@' + xyCompactBlock[ii] + '#@' + value.substring(i + xyCompactBlock[ii].length);
				}
			}
		}
		return value
	}
	this.getBlockStringXY = function() {
		return this._bstrXY;
		// return [xString, yString, parsedBlockX.raw, parsedBlockY.raw]
	}

	this.prepareSample = function(s, blockXY) {
		s.ruleSampleId = s.sampleId;
		// s.sample = s.sample.replace('XXX', blockXY[0])
		// if (blockXY[1])
		// s.sample = s.sample.replace('YYY', blockXY[1])
	}
	this.setSampleRM = function(s) {
		var i = this.allFaqStore.find('ruleId', s.sampleId);
		if (i != -1) {
			s._RM = true
		} else {
			s._RM = false
		}
	}
	this.presetStore.onload = function(rs) {
		var _rs = []
		this.allFaqStore.clearFilter(true)
		var blockXY = this.getBlockStringXY();

		Ext.each(rs, function(_d) {
			  _d.question = _d.rule;
			  _d._isRule = true;
			  var pos = this.allFaqStore.find('ruleId', _d.ruleId);
			  var merged;// saved merged with dyn ones,and mixed
			  // '_isRule' set;
			  if (pos != -1) {
				  var lR = this.allFaqStore.getAt(pos)
				  if (lR.get('type') == this.TYPE_FAQ_REMOVED) {// disabled
					  pos = -1;
					  _d._RM = true
				  } else {// override
					  var dynSamples = _d.samples;
					  var savedSamples = lR.data.samples;
					  loop1 : for (var i = dynSamples.length - 1; i >= 0; i--) {
						  if (!merged)
							  merged = true;
						  dynSamples[i]._isRule = true;
						  for (var j = savedSamples.length - 1; j >= 0; j--) {
							  if (dynSamples[i].sampleId == savedSamples[j].ruleSampleId) {
								  savedSamples[j]._old = dynSamples[i];
								  // create _old
								  this.prepareSample(savedSamples[j]._old, blockXY)
								  dynSamples.splice(i, 1)
								  continue loop1;
							  }
						  }
						  // not saved
						  this.prepareSample(dynSamples[i], blockXY);
						  dynSamples[i]._old = Ext.apply({}, dynSamples[i]);
					  }
					  if (dynSamples.length > 0) {
						  savedSamples = savedSamples.concat(dynSamples);
					  }
					  _d.samples = lR.data.samples = savedSamples;

					  if (lR.get('type') == this.TYPE_FAQ_MODIFIED) {
						  pos = -1;// just occupy,need raw data
					  } else {
						  lR.data._old = Ext.apply({}, _d)
						  delete lR.data._old.samples// no old samples
						  _d = lR.data
					  }
				  }
			  }

			  if (pos == -1) {
				  if (_d.type == 0) {// plain
				  } else {// template
					  if (!merged)
						  Ext.each(_d.samples, function(s) {
							    s._isRule = true;
						    })
					  if (_d.samples) {
						  Ext.each(_d.samples, function(s) {
							    if (s._isRule && !s._old) {
								    // prepare old
								    this.prepareSample(s, blockXY)
								    s._old = Ext.apply({}, s);
							    }
						    }, this)
					  }
				  }
				  if (!_d._old)
					  _d._old = Ext.apply({}, _d)
				  delete _d._old.samples// no old samples
				  delete _d._old._RM// old data with no remove flag
				  _rs.push(new this.allFaqStore.recordType(_d));
			  }
			  if (_d.samples) {
				  Ext.each(_d.samples, function(s) {
					    if (s._isRule)
						    this.setSampleRM(s)
				    }, this)
			  }
		  }, this)

		var oldIsModified = this.allFaqStore.isModified
		if (_rs.length > 0) {
			Ext.each(_rs, function(_r) {
				  this.allFaqStore.addSorted(_r)
			  }, this)
		}
		this.allFaqStore.filterIt()
		if (this.allFaqStore.getCount() < 20) {
			this.ruleEditGrid.batchAdd(20)
		}
		this.allFaqStore.isModified = oldIsModified
	}.dg(this);
	// UI
	this.questionRd = function(value, metaData, r, row, col, store) {
		if (!value)
			return value
		var colored = false;
		if (r.get('_isRule')) {
			metaData.attr = 'style=background-color:#eee'
		}
		if (r.get('ruleId')) {
			value = this.flagCompactBlock(value, r.get('type') == 0);
		}
		if (r.get('type') != 0) {
			value = templateRenderer(value);
		}
		if (r.get('ruleId')) {
			value = value.replace(/#@(.+?)#@/ig, '<font color=red><b>$1</b></font>')
		}
		return value
	}
	var ruleEditGrid = new ExcelLikeGrid({
		  selectLastEditRowField : 'question',
		  border : false,
		  region : 'center',
		  margins : '0 0 0 0',
		  autoExpandColumn : 'question',
		  clicksToEdit : 1,
		  style : 'border-width:1px;border-left-width:0px',
		  selModel : new Ext.grid.RowSelectionModel(),
		  subgridLoader : {
			  load : self.loadSamples,
			  scope : self
		  },
		  pasteable : true,
		  columns : [new Ext.grid.RowNumberer(), {
			    id : 'question',
			    header : '扩展问',
			    dataIndex : 'question',
			    width : 120,
			    renderer : this.questionRd.dg(this),
			    editor : new Ext.form.TextArea({
				      allowBlank : true
			      })
		    }, {
			    id : 'type',
			    header : '模板类型',
			    dataIndex : 'type',
			    hidden : isVersionStandard(),
			    width : 120,
			    renderer : faqTypeRender,
			    editor : faqTypeCombo
		    }, new obj.detail.FaqDynDeleteButtoner({
			      isRecoverBtn : function(r) {
				      return r.get('ruleId') && r.get('_old')
			      }
		      })],
		  store : this.allFaqStore
	  });

	var menu = new Ext.menu.Menu({
		  items : [{
			    text : '复制',
			    iconCls : 'icon-attr-copy',
			    width : 150,
			    handler : function() {
				    Dashboard.temp_grid = self.id;
				    Dashboard.temp.length = 0;
				    var records = ruleEditGrid.getSelectionModel().getSelections();

				    if (records.length == 0) {
					    Dashboard.setAlert('请选择至少一条进行复制!');
					    return;
				    }
				    for (var i = 0; i < records.length; i++) {
					    if (records[i].data.question == undefined || records[i].data.type >= 100) {
						    Dashboard.setAlert('不能复制空行...')
						    Dashboard.temp.length = 0;
						    return;
					    }
					    Dashboard.temp.push(records[i]);
				    }
			    }
		    }, {
			    text : '复制全部',
			    iconCls : 'icon-attr-copy',
			    width : 150,
			    handler : function() {
				    Dashboard.temp_grid = self.id;
				    Dashboard.temp.length = 0;
				    var store = ruleEditGrid.getStore();
				    var selected = [];
				    store.each(function(record, i) {
					      if (record.get('question') != undefined
									&& record.get('type') != this.TYPE_SAMPLE_REMOVED
									&& record.get('type') != this.TYPE_FAQ_REMOVED
									&& record.get('type') != this.TYPE_FAQ_MODIFIED
									&& record.get('type') != this.TYPE_FAQ4ATTRSPL
									&& !record.get('_isRule')) {
						      Dashboard.temp.push(record);
						      selected.push(i);
					      }
				      });
				    ruleEditGrid.getSelectionModel().selectRows(selected);
			    }
		    }, {
			    text : '剪切',
			    iconCls : 'icon-attr-cut',
			    width : 150,
			    handler : function() {
				    Dashboard.temp_grid = self.id;
				    Dashboard.temp.length = 0;
				    var records = ruleEditGrid.getSelectionModel().getSelections();

				    if (records.length == 0) {
					    Dashboard.setAlert('请选择至少一条进行剪切!');
					    return;
				    }
				    for (var i = 0; i < records.length; i++) {
					    if (records[i].data.question == undefined) {
						    Dashboard.setAlert('不能剪切空行...')
						    Dashboard.temp.length = 0;
						    return;
					    }
					    Dashboard.temp.push(records[i]);
				    }
				    for (var i = 0; i < records.length; i++) {
					    ruleEditGrid.getStore().remove(records[i]);
					    sampleEditGrid.getStore().removeAll();
					    sampleEditGrid.batchAdd(20);
				    }
				    sampleEditGrid.disable();
				    if(Dashboard.temp.length > 0) {
					    	if ('true' == Dashboard.getModuleProp('kbmgr.touchValueStateWhenFaqChange')) {
							self.attrDtEd.getStore().getAt(0).set('state', 0)
					    	}
				    }
			    }
		    }, {
			    text : '粘贴',
			    iconCls : 'icon-attr-paste',
			    width : 150,
			    handler : function() {
			    		ruleEditGrid.getStore().clearFilter(false);
			    		var store = ruleEditGrid.getStore(), repeat = [], tempBlank = 1;
			    		
			    		for (var j = 0; j < Dashboard.temp.length; j++) {
						var isRepeat = false;
						store.each(function(record, k) {
							if (record.get('question') != undefined && Dashboard.temp[j].data.question == record.get('question')) {
								repeat.push(Dashboard.temp[j].data.question);
								isRepeat = true;
							}
						});
					};
					if (repeat.length > 0) {
					    Dashboard.setAlert(repeat.join(',') + '重复!');
					    ruleEditGrid.getStore().filterIt();
					    return ;
					}
					for (var j = 0; j < Dashboard.temp.length; j++) {
						var d = Ext.apply({}, Dashboard.temp[j].data);
						store.each(function(record, k) {
							if (record.get('question') == undefined) {
								store.remove(record);
								tempBlank++
							}
						});
						d.faqId = '';
						store.add(new store.recordType(d))
					}
					ruleEditGrid.batchAdd(tempBlank);
					if(tempBlank > 1) {
						if ('true' == Dashboard.getModuleProp('kbmgr.touchValueStateWhenFaqChange')) {
							self.attrDtEd.getStore().getAt(0).set('state', 0)
					    	}
					}
				    sampleEditGrid.disable();
					ruleEditGrid.getStore().filterIt();
			    }
		    }]
	  });

	ruleEditGrid.on('cellcontextmenu', function(grid, rowIndex, cellIndex, e) {
				e.preventDefault();
				var records = ruleEditGrid.getSelectionModel().getSelections();
				if (records.length == 0) {
					menu.items.itemAt(0).disable();// 复制
					menu.items.itemAt(2).disable();// 剪切
					menu.items.itemAt(3).disable();// 粘贴
				} else {
					var r = true;
					for (var i = 0; i < records.length; i++) {
						if (records[i].data.question != undefined
								&& records[i].data._isRule) {
							r = false;
						}
					}
					if (r) {
						menu.items.itemAt(0).enable();
						menu.items.itemAt(1).enable();
						menu.items.itemAt(2).enable();
					} else {
						Dashboard.setAlert('动态扩展问不能够被复制...');
						menu.items.itemAt(0).disable();
						menu.items.itemAt(1).disable();
						menu.items.itemAt(2).disable();
					}
				}
				if (Dashboard.temp.length == 0
						|| Dashboard.temp_grid == self.id) {
					menu.items.itemAt(3).disable();
				} else {
					menu.items.itemAt(3).enable();
				}
				menu.showAt(e.getXY());
				return;
			});

	var handleDelete_faq = function(grid, row, col, e) {
		var btn = e.getTarget('.deleteButton');
		if (btn) {
			var store = grid.getStore()
			var r = store.getAt(row)
			if (r && r.get('_isRule')) {// 禁用
				r.data._RM = true;

				var d = Ext.apply({}, r.data)
				delete d.rule
				delete d.samples
				d.question = ' ';
				d.type = this.TYPE_FAQ_REMOVED;
				store.add(new store.recordType(d))
				store.filterIt();
			} else {// 删除
				grid.getStore().removeAt(row);
			}
			if ('true' == Dashboard.getModuleProp('kbmgr.touchValueStateWhenFaqChange'))
				this.attrDtEd.getStore().getAt(0).set('state', 0)
		}
	}
	var handleRecover_faq = function(grid, row, col, e) {
		var btn = e.getTarget('.recoverButton');
		if (btn) {
			var st = this.allFaqStore;
			var r = st.getAt(row);
			if (r) {
				st.clearFilter(true);
				if (r.get('_isRule')) {// 启用
					var pos = st.findBy(function(_r) {
						  return _r.get('ruleId') == r.get('ruleId') && _r.get('type') == this.TYPE_FAQ_REMOVED
					  }, this)
					if (pos != -1) {// 启用
						delete r.data._RM;
						st.removeAt(pos);
					}
				} else {// 还原 , 删除
					var allIsRule = true;
					Ext.each(r.data.samples, function(s) {
						  if (!s._isRule || s._RM)
							  allIsRule = false;
					  }, this)
					if (r.data._old) {
						if (r.data._old._old != null)
							delete r.data._old._old
						Ext.apply(r.data, r.data._old)
					} else {
						grid.getStore().remove(r);
					}
					if (!allIsRule)
						this.addOccupySampleFaq();
				}
				st.filterIt();
				grid.getView().refresh()
				if ('true' == Dashboard.getModuleProp('kbmgr.touchValueStateWhenFaqChange'))
					this.attrDtEd.getStore().getAt(0).set('state', 0)
			}
		}
	}
	ruleEditGrid.on("cellclick", function() {
		  return this.isEditable()
	  }, this);
	ruleEditGrid.on("cellclick", handleDelete_faq, this);
	ruleEditGrid.on("cellclick", handleRecover_faq, this);
	this.syncSampleModifyOccupier = function() {
		var sampleModified = false;
		sampleEditGrid.getStore().each(function(r) {
			  if (r.get('sample') && !r.get('_isRule')) {
				  sampleModified = true;
				  return false;
			  }
		  }, this)
		if (!sampleModified) {
			var ruleId = ruleEditGrid.getSelectionModel().getSelected().get('ruleId');
			if (ruleId) {
				ruleEditGrid.getStore().findBy(function(r) {
					  if (r.get('ruleId') == ruleId && r.get('type') == this.TYPE_FAQ_MODIFIED) {
						  ruleEditGrid.getStore().remove(r)
						  return true;
					  }
				  }, this)
			}
		}
	}
	var handleDelete_sample = function(grid, row, col, e) {
		var btn = e.getTarget('.deleteButton');
		if (!btn)
			return
		var store = grid.getStore()
		var r = store.getAt(row);
		if (r && r.get('_isRule')) {// 禁用
			var d = {
				ruleId : r.get('ruleSampleId')
			}
			d.question = '';
			d.type = this.TYPE_SAMPLE_REMOVED;
			// tricky
			ruleEditGrid.store.add(new ruleEditGrid.store.recordType(d))
			r.data._RM = true;
			grid.getView().refresh()
			ruleEditGrid.store.filterIt()
		} else {
			grid.getStore().removeAt(row);
			this.syncSampleModifyOccupier();
		}
		if ('true' == Dashboard.getModuleProp('kbmgr.touchValueStateWhenFaqChange'))
			this.attrDtEd.getStore().getAt(0).set('state', 0)
	}
	var handleRecover_sample = function(grid, row, col, e) {
		var btn = e.getTarget('.recoverButton');
		if (btn) {
			var samplest = grid.getStore();
			var faqst = ruleEditGrid.store;
			var r = samplest.getAt(row);
			if (r) {
				this.allFaqStore.clearFilter(true);
				// ruleId == sampleId (&& question == '*'):tricky
				var pos = faqst.find('ruleId', r.get('sampleId'))
				if (pos != -1) {// 启用
					delete r.data._RM;
					faqst.removeAt(pos);
				} else {// 还原
					if (r.data._old) {
						if (r.data._old._old)
							delete r.data._old._old
						Ext.apply(r.data, r.data._old);
					}
					this.syncSampleModifyOccupier();
				}
				grid.getView().refresh()
				this.allFaqStore.filterIt();
				if ('true' == Dashboard.getModuleProp('kbmgr.touchValueStateWhenFaqChange'))
					this.attrDtEd.getStore().getAt(0).set('state', 0)
			}
		}
	}
	sampleEditGrid.on("cellclick", function() {
		  return this.isEditable()
	  }, this);
	sampleEditGrid.on("cellclick", handleDelete_sample.dg(this));
	sampleEditGrid.on("cellclick", handleRecover_sample.dg(this));

	var isEmptyRecord = this.isEmptyRecord = ux.util.isEmptyRecord;
	function faqUpdated(store, r) {
		if ('true' == Dashboard.getModuleProp('kbmgr.touchValueStateWhenFaqChange'))
			this.attrDtEd.getStore().getAt(0).set('state', 0)
		if (r.get('_isRule')) {
			store.clearFilter(true);
			for (var i = store.getCount() - 1; i >= 0; i--) {
				var _r = store.getAt(i);
				if (_r.get('ruleId') == r.get('ruleId') && r != _r) {
					// remove the '#' faq
					if (_r.get('type') == this.TYPE_FAQ_MODIFIED) {
						r.data.faqId = _r.get('faqId');
					}
					store.removeAt(i);
					// this.resetSamplesId(_r.data.samples)
					// rs.push(_r);
				}
			}
			delete r.data._isRule
			delete r.data._RM
			delete r.data.rule
			store.filterIt();
		} else {
			if (r.get('question')) {
				var plain = r.get('question').indexOf('[') == -1;
				if (isVersionStandard()) {
					r.data.type = 0
				} else {
					if (r.data.type == null) {
						r.data.type = plain ? 0 : 1
					} else if (plain && r.data.type != 0)
						r.data.type = 0;
					else if (!plain && r.data.type == 0)
						r.data.type = 1;
				}
			} else {
				delete r.data.type
			}
		}
		if (!r || !r.get('type'))
			sampleEditGrid.disable();
		else
			sampleEditGrid.enable()
	}
	this.allFaqStore.on('update', faqUpdated, this)
	this.sampleEdited = function(store, r, op) {
		if (op == 'edit' && 'true' == Dashboard.getModuleProp('kbmgr.touchValueStateWhenFaqChange')) {
			this.attrDtEd.getStore().getAt(0) ? this.attrDtEd.getStore().getAt(0).set('state', 0) : 0
		}
		if (isEmptyRecord(r))
			return
		var cleared = false;
		if (r.data._isRule) {
			delete r.data._isRule;
			delete r.data._RM;
			ruleEditGrid.store.clearFilter(true);
			cleared = true;
			ruleEditGrid.store.findBy(function(fr) {
				  if (fr.get('ruleId') == r.get('sampleId')) {
					  ruleEditGrid.store.remove(fr);
					  return true;
				  }
			  })
		}
		this.addOccupySampleFaq(!cleared);
		if (cleared)
			ruleEditGrid.store.filterIt();
	}
	this.resetSamplesId = function(spl) {
		if (spl)
			Ext.each(spl, function(s) {
				  delete s.sampleId
			  })
	}
	this.addOccupySampleFaq = function(clearFilter) {
		var faqR = ruleEditGrid.getSelectionModel().getSelected();
		if (!faqR)
			return
		if (clearFilter)
			ruleEditGrid.store.clearFilter(true);
		var occupyR;
		// TODO:the following logic ok?!
		ruleEditGrid.store.findBy(function(fr) {
			  if (fr.get('ruleId') == faqR.get('ruleId') && fr.get('type') != this.TYPE_FAQ_REMOVED && !fr.get('_isRule')) {
				  // same rule and not the delete occupy
				  occupyR = fr;
				  return true;
			  }
		  }, this)
		if (!occupyR) {
			occupyR = new ruleEditGrid.store.recordType({
				  question : '',
				  ruleId : faqR.get('ruleId'),
				  type : this.TYPE_FAQ_MODIFIED
			  })
			ruleEditGrid.store.add(occupyR)
			occupyR.data.samples = faqR.data.samples
			this.resetSamplesId(occupyR.data.samples)
		}
		if (clearFilter)
			ruleEditGrid.store.filterIt();
	}
	sampleEditGrid.store.on('add', this.sampleEdited, this);
	sampleEditGrid.store.on('update', this.sampleEdited, this);
	sampleEditGrid.store.on('remove', this.sampleEdited, this);

	ruleEditGrid.store.on('load', function(g, rs) {
		  ruleEditGrid.getSelectionModel().clearSelections()
		  var rtoselect, faq4attrspl
		  this.allFaqStore.each(function(r) {
			    if (r.get('type') == obj.detail.TYPE_FAQ4ATTRSPL) {
				    rtoselect = faq4attrspl = r;
				    return false
			    } else if (!rtoselect && !isEmptyRecord(r)) {
				    rtoselect = r
			    }
		    })
		  if (faq4attrspl) {
			  this.attrDtEd.getSelectionModel().selectRow(0)
		  } else {
			  // ruleEditGrid.getSelectionModel().selectRow(0);
		  }
		  self.loadSamples(rtoselect)
		  sampleEditGrid.enable();
	  }, this);

	this.ruleEditGrid = ruleEditGrid;
	this.attrDtEd = new obj.detail.AttrEditPanel({
		  region : 'north',
		  split : true,
		  collapseMode : 'mini',
		  height : 90,
		  mainpanel : cfg.mainP,
		  mode : obj.detail.AttrEditPanel.MODE.DETAIL,
		  listeners : {
			  cellclick : function(grid, rowIndex, columnIndex, e) {
				  this.ruleEditGrid.getSelectionModel().clearSelections()
				  this.allFaqStore.clearFilter();
				  var faq4attrspl;
				  this.allFaqStore.each(function(r) {
					    if (r.get('type') == obj.detail.TYPE_FAQ4ATTRSPL) {
						    faq4attrspl = r;
						    return false
					    }
				    })
				  if (faq4attrspl == null) {
					  var oldIsModified = this.allFaqStore.isModified
					  faq4attrspl = new this.allFaqStore.recordType({
						    type : obj.detail.TYPE_FAQ4ATTRSPL,
						    question : '',
						    samples : []
					    })
					  this.allFaqStore.add(faq4attrspl)
					  this.allFaqStore.isModified = oldIsModified
				  }
				  this.allFaqStore.filterIt()
				  this.loadSamples.defer(150, this, [faq4attrspl])// 150数值参考excellike中的defer
				  this.sampleEditGrid.enable();
			  }.dg(this)
		  }
	  });
	this.attrDtEd.getObjectData = function() {
		return this.getObjectData.apply(this, arguments)
	}.dg(this);
	// remove old rule related faq/sample
	this.attrDtEd.on('attributeChanged', function(r, attr) {
		  this.allFaqStore.clearFilter(true);
		  for (var i = this.allFaqStore.getCount() - 1; i >= 0; i--) {
			  var r = this.allFaqStore.getAt(i)
			  if (r.get('ruleId')) {
				  this.allFaqStore.removeAt(i);
			  }
		  }
		  this.allFaqStore.filterIt()
		  delete this.presetStore._loaded;
		  this.presetStore.baseParams['attributeId'] = attr?attr.attributeId:null;
	  }.dg(this));
	sampleEditGrid.batchAdd(20);
	ruleEditGrid.on('scrollbottomclick', function() {
		  ruleEditGrid.batchAdd(10);
	  })
	sampleEditGrid.on('scrollbottomclick', function() {
		  sampleEditGrid.batchAdd(10);
	  })
	ruleEditGrid.on("shiftright", function(e) {
		  var g = sampleEditGrid
		  if (g.disabled)
			  return
		  var col = g.findMostLeftEditCol()
		  g.getSelectionModel().selectRow(0)
		  this.stopEditing()
		  g.startEditing(0, col)
	  });
	sampleEditGrid.on("shiftleft", function(e) {
		  var g = ruleEditGrid
		  var col = g.findMostRightEditCol()
		  var r = g.getSelectionModel().getSelected()
		  var row = g.getStore().indexOf(r)
		  this.stopEditing()
		  g.startEditing(row, col)
	  });

	ruleEditGrid.getSelectionModel().on('selectionchange', function() {
		  var r = this.getSelected();
		  if (!r || !r.get('type'))
			  sampleEditGrid.disable()
		  else
			  sampleEditGrid.enable()
	  })
	ruleEditGrid.on('rowclick', function(g, row, e) {
		  var r = g.store.getAt(row);
		  if (!r || !r.get('type'))
			  sampleEditGrid.disable();
		  else
			  sampleEditGrid.enable()
	  })

	obj.detail.AttrDetailEditWindow.superclass.constructor.call(this, Ext.apply({
		    closeAction : 'close',
		    width : 800,
		    height : 500,
		    plain : true,
		    maximizable : true,
		    collapsible : true,
		    border : false,

		    layout : 'border',
		    region : 'center',
		    border : false,
		    items : [this.attrDtEd, ruleEditGrid, sampleEditGrid]

	    }, this.cfg));

	var checkWrongResultStore = new Ext.data.Store({
		  proxy : new Ext.data.HttpProxy({
			    url : 'ontology-object!checkWrongReulst.action'
		    }),
		  reader : new Ext.data.JsonReader({
			    idProperty : 'inputques',
			    root : 'data',
			    fields : [{
				      name : 'inputques',
				      type : 'string'
			      }, {
				      name : 'inputcate',
				      type : 'string'
			      }, {
				      name : 'sampletemp',
				      type : 'string'
			      }, {
				      name : 'hopestan',
				      type : 'string'
			      }, {
				      name : 'hopecate',
				      type : 'string'
			      }, {
				      name : 'locaques',
				      type : 'string'
			      }, {
				      name : 'locacate',
				      type : 'string'
			      }, {
				      name : 'matchques',
				      type : 'string'
			      }, {
				      name : 'similar',
				      type : 'string'
			      }, {
				      name : 'attributeName',
				      type : 'string'
			      }, 'answer']
		    }),
		  writer : new Ext.data.JsonWriter()
	  });
	var checkWrongResultGrid = new Ext.grid.GridPanel({
		  region : 'center',
		  border : false,
		  frame : false,
		  store : checkWrongResultStore,
		  loadMask : true,
		  colModel : new Ext.grid.ColumnModel({
			    defaults : {
				    sortable : true
			    },
			    columns : [new Ext.grid.RowNumberer(), {
				      header : '输入问题',
				      dataIndex : 'inputques',
				      sortable : false
			      }, {
				      header : '样例模板',
				      dataIndex : 'sampletemp',
				      sortable : false
			      }, {
				      header : '期望标准问',
				      dataIndex : 'hopestan',
				      sortable : false
			      }, {
				      header : '定位标准问',
				      dataIndex : 'locaques',
				      sortable : false
			      }, {
				      header : '匹配的问题',
				      dataIndex : 'matchques',
				      sortable : false
			      }, {
				      header : '匹配的答案',
				      dataIndex : 'answer',
				      sortable : false,
				      hidden : true
			      }, {
				      header : '相似度',
				      dataIndex : 'similar',
				      sortable : false,
				      hidden : true
			      }, {
				      header : '属性名',
				      dataIndex : 'attributeName',
				      sortable : false
			      }]
		    }),
		  viewConfig : {
			  forceFit : true
		  }
	  });
	// 打开窗口
	var checkDataWindow = new Ext.Window({
		  width : 800,
		  height : 400,
		  modal : false,
		  plain : true,
		  shim : true,
		  title : '跑错结果',
		  closeAction : 'hide',
		  collapsible : true,// 折叠
		  closable : true, // 关闭
		  resizable : false,// 改变大小
		  draggable : true,// 拖动
		  minimizable : true,// 最小化
		  maximizable : true,// 最大化
		  animCollapse : true,
		  constrainHeader : true,
		  layout : 'border',
		  bodyStyle : 'padding:5px;',
		  items : [checkWrongResultGrid]
	  });
	this.checkWrongResultStore = checkWrongResultStore;
	this.checkDataWindow = checkDataWindow;
};

Ext.extend(obj.detail.AttrDetailEditWindow, Ext.Window, {
	  /**
			 * 
			 * @type {obj.detail.AttrEditPanel}
			 */
	  attrP : null,
	  initComponent : function() {
		  this.sampleEditGrid.disable();
		  this._data = {}
		  this.tbar = this._buildTbar();
		  // relay events
		  this.attrP.on('ontologyClassesChange', function(ids) {
			    this.attrDtEd.ontologyClassesChange(ids);
		    }, this)
		  // adjust height
		  this.adjustHeight = function() {
			  // var gv = this.attrDtEd.getView();
			  // // TODO:any alternatives instead of calling private
			  // method below?!
			  // this.layout.north.onSplitMove('', Math.min(200, 50
			  // + gv.mainBody.getHeight()))
		  }
		  this.attrDtEd.getView().on('rowremoved', this.adjustHeight, this);
		  this.attrDtEd.getView().on('rowupdated', this.adjustHeight, this);
		  this.attrDtEd.getView().on('rowsinserted', this.adjustHeight, this);
		  this.attrDtEd.getView().on('refresh', this.adjustHeight, this);

		  this.attrP.ontologyClassesChange();// force fire the lazy
		  // loading value
		  obj.detail.AttrDetailEditWindow.superclass.initComponent.call(this);
		  /**
				 * @event attributeValueChange Fires when synchronizing the attribute value
				 *        in editing.
				 * @param {obj.detail.AttrDetailEditWindow}
				 *         this
				 * @param {Object}
				 *         attribute values(key-value pairs)
				 */
		  this.addEvents('attributeValueChange', this)
		  this.on('beforehide', function() {
			    var self = this
			    if (!self.__to_hide) {
				    self.attrDtEd.stopEditing();
				    self.ruleEditGrid.stopEditing();
				    self.sampleEditGrid.stopEditing();
				    this._confirmModified(function() {
					      self.__to_hide = true;
					      self[self.closeAction]();
					      self.__to_hide = false;
				      })
				    return false;
			    }
		    }, this);
	  },
	  _resetModifiedState : function() {
		  if (this.attrDtEd.getStore().modified && this.attrDtEd.getStore().modified.length)
			  this.attrDtEd.getStore().modified = []
		  this.allFaqStore.isModified = false;
	  },
	  _confirmModified : function(cb) {
		  if (this.allFaqStore.isModified || (this.attrDtEd.getStore().getModifiedRecords() && this.attrDtEd.getStore().getModifiedRecords().length)) {
			  Ext.Msg.show({
				    title : '提示',
				    msg : '数据未保存，是否要保存?',
				    buttons : Ext.Msg.YESNOCANCEL,
				    fn : function(result) {
					    if (result == 'yes') {
						    this._resetModifiedState();
						    this._saveValue(function() {
							      cb()
						      });
					    } else if (result == 'no') {
						    this._resetModifiedState();
						    cb()
					    }
				    },
				    scope : this,
				    icon : Ext.Msg.QUESTION,
				    minWidth : Ext.Msg.minWidth
			    });
		  } else {
			  cb()
		  }
	  },
	  _navAttr : function(dir) {
		  var self = this;
		  self.fireEvent('navAttr', dir, this);
	  },
	  _saveObject : function() {
		  this._saveValue()// 适配attrEditPanel的保存调用方法
	  },
	  _saveValue : function(cb) {
		  if (this.getTopToolbar().saveBtn.disabled)
			  return
		  this.attrDtEd.stopEditing()
		  this.ruleEditGrid.stopEditing()
		  this.sampleEditGrid.stopEditing()
		  this.allFaqStore.clearFilter(true);
		  var attrsd = this.attrDtEd.getAttrsData();
		  var d = attrsd[0];
		  d.dimIdDeleted = attrsd.dimIdDeleted
		  if (!d) {
			  Ext.Msg.alert('提示', '请先填写标准问和属性值');
			  return;
		  }
		  if (d.faqs) {
			   var faqs = [], attrForSampleFound = false;
			  // copy for re-editing when saving fail
			  Ext.each(d.faqs, function(f) {
				    if ((ux.util.isEmptyData(f) || !f.question || !f.question.trim()) && (f.type == null || f.type < 100))
					    return;
					if(f.type == this.TYPE_FAQ4ATTRSPL) {
						if(attrForSampleFound) {
							return ;
						}
					  	attrForSampleFound=true;
					}
				    faqs.push(f = Ext.apply({}, f))
				    if (f.samples && f.samples.length) {
					    var samples = []
					    Ext.each(f.samples, function(s) {
						      if (!ux.util.isEmptyData(s, ['sampleId', '_old', 'ruleSampleId','status']))
							      samples.push(Ext.apply({}, s))
					      })
					    f.samples = samples
				    }
			    }, this)
			  d.faqs = faqs
			  for (var i = faqs.length - 1; i >= 0; i--) {
				  var faq = faqs[i];
				  if (faq.rule) {
					  faqs.splice(i, 1);
					  continue;
				  } else {
					  for (var key in faq) {
						  if (key.toString().indexOf('_') != -1) {
							  delete faq[key];
						  }
					  }
				  }
				  if (faq.samples) {
					  for (var j = faq.samples.length - 1; j >= 0; j--) {
						  if (faq.samples[j]._isRule) {
							  faq.samples.splice(j, 1);
						  } else {
							  for (var key in faq.samples[j]) {
								  if (key.toString().indexOf('_') != -1) {
									  delete faq.samples[j][key];
								  }
							  }
						  }
					  }
				  }
				  if (faq.samples && !faq.samples.length) {
					  // to spare a record when no more sample-modify
					  // exists
					  delete faq.samples
					  if (faq.type == this.TYPE_FAQ_MODIFIED || faq.type == this.TYPE_FAQ4ATTRSPL) {
						  var tfaq = faqs.splice(i, 1)
						  delete tfaq;
					  }
				  }
			  }
		  }
		  this.getEl().mask('保存...')
		  var rid = this.attrDtEd.store.getAt(0).id
		  var origRec = this.attrP.store.getById(rid)
		  var it = this.attrP.dimIterator();
		  var attr;
		  var toRemoves = []
		  var ri = 0
		  while (attr = it.nextAttr()) {
			  ri++
			  if (attr == origRec) {
				  break;
			  }
		  }
		  d.seq = ri
		  Ext.Ajax.request({
			    url : 'ontology-object-value!save.action',
			    params : {
				    objectId : this._data.object.objectId,
				    categoryId : this._data.object.categoryId,
				    data : Ext.encode(ux.util.resetEmptyString(d), 'Y-m-d H:i'),
				    ispub : this.ispub,
				    sync : this.getTopToolbar().sync.checked,
				    disableDynValidation : this.disableDynValidation
			    },
			    success : function(response, opts) {
				    this.getEl().unmask()
				    var result = Ext.decode(response.responseText);
				    if (result.success) {
					    this._resetModifiedState();

					    var valueObj = result.data;

					    var rid = this.attrDtEd.store.getAt(0).id
					    var oldvid = this.attrDtEd.store.getAt(0).get('vid')
					    var row = this.attrP.store.indexOf(this.attrP.store.getById(rid));
					    if (row != -1) {
						    var range = this.attrP._findAttrDimRange(row);
						    for (var i = range[1]; i >= range[0]; i--)
							    this.attrP.store.removeAt(i);
					    }
					    this._savedFaqs = {
						    valueId : valueObj.valueId,
						    faqs : valueObj.faqs
					    };
					    delete valueObj.faqs
					    var attrR = new (obj.detail.AttrType.getRecordType())(valueObj)
					    var rds = this.attrP._loadDimRecordSet(attrR);
					    var parentR;
					    Ext.each(rds, function(rd, i) {
						      var _r = new obj.detail.AttrDimType.recordType(rd, rd.vid)
						      if (!parentR) {
							      parentR = _r;
						      }
						      this.attrP.store.insert(row + i, _r);
					      }, this);
					    this.attrP.getView().refresh();
					    this.attrP.editAttrDetail(this.attrP.store.indexOf(parentR), oldvid);
					    if (result.multilineMsg) {
						    Ext.Msg.show({
							      title : '警告',
							      msg : result.message,
							      buttons : Ext.MessageBox.OK,
							      width : 600
						      })
						    addMsgBoxDetail(result.multilineMsg)
					    } else
						    Dashboard.setAlert('保存成功')
					    this.attrP.resetLinkedAttrs(parentR)
				    } else {
					    Ext.Msg.alert('错误', result.message)
					    this.allFaqStore.filterIt()
				    }
				    if (cb)
					    cb(result.success)
			    },
			    failure : function(response) {
				    this.getEl().unmask()
				    this.allFaqStore.filterIt()
				    Ext.Msg.alert('保存失败', response.responseText);
				    if (cb)
					    cb(false)
			    },
			    scope : this
		    });
	  },
	  _fxBtnClick : function(cb) {
		  this.attrDtEd.stopEditing()
		  // this.ruleEditGrid.stopEditing()
		  this.sampleEditGrid.stopEditing()
		  var attrsd = this.attrDtEd.getAttrsData();
		  var d = attrsd[0];
		  if (!d) {
			  Ext.Msg.alert('提示', '请先填写标准问和属性值');
			  return;
		  }
		  this.getEl().mask('正在查询中...')
		  // this.ruleEditGrid.getStore().insert(0,{"question":"1111"});
		  // this.ruleEditGrid.getStore().add({"question":"1111"});
		  var _ruleEditGrid = this.ruleEditGrid;
		  var _ruleRecord = this.RuleRecord;
		  var self = this;
		  Ext.Ajax.request({
			    url : 'ontology-object!suggestSemantic.action',
			    params : {
				    valueId : d.valueId,
				    ispub : this.ispub
			    },
			    success : function(response, opts) {
				    this.getEl().unmask();
				    var result = Ext.decode(response.responseText);
				    if(result.success){
				    	Dashboard.setAlert('分析测试样例完成');
						self.loadData0(self.valueRecords.records, self.valueRecords.object);
				    }
//				    var data = new Array();
//				    var insertpos
//				    Ext.each(result, function(rd, i) {
//					      rd = '%' + rd;
//					      var exist = false;
//					      var insertpos0 = 0
//					      _ruleEditGrid.getStore().each(function(record) {
//						        if (this.isEmptyRecord(record))
//							        return false
//						        else
//							        insertpos0++
//						        if (record.data.question == rd)
//							        exist = true;
//					        }, this);
//					      if (insertpos == null)
//						      insertpos = insertpos0
//					      if (!exist)
//						      data.push(new _ruleRecord({
//							        'question' : rd,
//							        'type' : 1
//						        }));
//				      }, this);
//				    insertpos == null ? insertpos = 0 : 0
//				    if (data.length > 0)
//					    _ruleEditGrid.getStore().insert(insertpos, data);
			    },
			    failure : function(response) {
				    this.getEl().unmask();
			    },
			    scope : this
		    });
	  },
	  _filter_occupyRS : function() {
		  return {
			  fn : function(r) {
				  return !r.get('type') || r.get('type') < obj.detail.TYPE_SAMPLE_REMOVED
			  }
		  }
	  },
	  _filter_dynFaq : function() {
		  return {
			  fn : function(r) {
				  return !r.get('_isRule')
			  }
		  }
	  },
	  filterAttrRuleFaq : function(btn, press) {
		  if (press) {
			  btn.setText("隐藏动态扩展问")
			  this.allFaqStore.filterIt(this._filter_occupyRS());
			  for (var i = this.allFaqStore.getCount() - 1; i >= 0; i--) {
				  if (this.isEmptyRecord(this.allFaqStore.getAt(i))) {
					  this.allFaqStore.removeAt(i);
				  }
			  }
			  if (this.presetStore.baseParams['attributeId']) {
				  if (!this.presetStore._loaded) {
					  this.getEl().mask('加载动态扩展问...')
					  this.presetStore.load(function() {
						    this.getEl().unmask()
					    }.dg(this))
				  } else {
					  if (this.allFaqStore.getCount() < 20) {
						  this.ruleEditGrid.batchAdd(20)
					  }
				  }
			  } else {
				  if (this.allFaqStore.getCount() < 20) {
					  this.ruleEditGrid.batchAdd(20)
				  }
			  }
			  this.presetStore._loaded = true
		  } else {
			  btn.setText("显示动态扩展问")
			  this.allFaqStore.filterIt([this._filter_occupyRS(), this._filter_dynFaq()]);
			  if (this.allFaqStore.getCount() < 20) {
				  this.ruleEditGrid.batchAdd(20)
			  }
		  }
	  },
	  _buildTbar : function() {
		  this.filterAttrRuleFaqsBtn = new Ext.Button({
			    enableToggle : true,
			    // tooltip : '显示/隐藏动态生成的Faq',
			    text : '显示动态扩展问',
			    iconCls : 'icon-view-switcher',
			    toggleHandler : this.filterAttrRuleFaq.dg(this),
			    scope : this
		    })
		  var bar = [this.filterAttrRuleFaqsBtn, '-', {
			    xtype : 'button',
			    text : '<< 上一个标准问',
			    handler : this._navAttr.dg(this, ['pre'])
		    }, {
			    xtype : 'button',
			    text : '下一个标准问 >>',
			    handler : this._navAttr.dg(this, ['next'])
		    }, !(Dashboard.u().isSupervisor() || "true"==Dashboard.getModuleProp('kb.sugSemantic_button.enable')) ? '' : ['-', {
			      xtype : 'button',
			      text : '分析测试样例',
			      ref : 'fxBtn',
			      handler : function() {
				      this._fxBtnClick()
			      }.dg(this)
		      }], '-', {
			    xtype : 'splitbutton',
			    text : '保存',
			    ref : 'saveBtn',
			    menu : {
				    items : [{
					      text : '同步',
					      checked : true,
					      name : 'sync',
					      ref : '../../sync'
				      }]
			    },
			    iconCls : 'icon-table-save',
			    width : 50,
			    handler : function() {
				    this._saveValue()
			    }.dg(this)
		    }, !(Dashboard.u().isSupervisor() || "true"==Dashboard.getModuleProp('kb.sugSemantic_button.enable')) ? '' : {
			    xtype : 'button',
			    text : '删除推荐语义',
			    iconCls : 'icon-delete',
			    handler : function() {
				    var attrsd = this.attrDtEd.getAttrsData();
				    var d = attrsd[0];
				    var self = this;
				    Ext.Ajax.request({
					      url : 'analyse-stand-question!deleteSuggestSemantic.action',
					      params : {
						      valueId : d.valueId
					      },
					      success : function(resp) {
						      var result = Ext.decode(resp.responseText);
						      if (result.success) {
							      Dashboard.setAlert('删除推荐语义成功');
							      self.loadData0(self.valueRecords.records, self.valueRecords.object);
						      } else
							      Ext.Msg.alert('失败', result.message);
					      },
					      failure : function(resp) {
						      Ext.Msg.alert('错误', resp.responseText);
					      }
				      });
			    }.dg(this)
		    }, {
			    xtype : 'button',
			    text : '样例模板跑错',
			    iconCls : 'icon-ontology-sync',
			    hidden : isVersionStandard(),
			    handler : function() {
				    var attrsd = this.attrDtEd.getAttrsData();
				    var d = attrsd[0];
				    var self = this;
				    Ext.Ajax.request({
					      url : 'ontology-object!checkWrong.action',
					      params : {
						      ispub : this.ispub,
						      valueId : d.valueId
					      },
					      success : function(resp) {
						      var result = Ext.util.JSON.decode(resp.responseText);
						      if (result.success) {
							      var timer = new ProgressTimer({
								        initData : result.data,
								        progressId : 'singleValueCheckWrongStatus',
								        boxConfig : {
									        title : '知识点样例的模板跑错'
								        },
								        finish : function(p, response) {
									        self.checkWrongResultStore.reload();
									        self.checkDataWindow.show();
								        }
							        });
							      timer.start();
						      } else
							      Ext.Msg.alert('提示', "跑错结果不是导入数据跑错，不能进行语义分析");
					      },
					      failure : function(resp) {
						      Ext.Msg.alert('错误', resp.responseText);
					      }
				      });
			    }.dg(this)
		    }];
		  return bar
	  },
	  cancel : function() {
		  this.hide();
	  },
	  _loadDataInternal : function(rs) {
		  this.attrDtEd.init();
		  this.attrDtEd.store.removeAll()
		  this.attrDtEd.store.add(rs);
		  // init attredit model & ui ===
		  this.attrDtEd.getDimChooserMenu().init();
		  this.attrDtEd.dimIdDeleted = []
		  // ===
		  this.sampleEditGrid.store.removeAll();
		  this.sampleEditGrid.batchAdd(20);
		  this.ruleEditGrid.getStore().loadLocalData(rs[0].data);
		  this.ruleEditGrid.batchAdd(20);
		  this._resetModifiedState();
		  this.allFaqStore.filterIt(this._filter_occupyRS());

		  if (!this._syncInit) {
			  this._syncInit = true
			  // reset sync option of save button
			  var syncOption = this.getTopToolbar().sync
			  syncOption.setChecked(Dashboard.getModuleProp('kbmgr.enableAudit') ? false : true);
			  var authName = 'kb.obj.instance.S'
			  var lastIndex = authName.lastIndexOf('.');
			  var perm = authName;
			  if (lastIndex != -1) {
				  perm = authName.substring(lastIndex + 1);
			  }
			  if (lastIndex != -1 && Dashboard.u().allow(authName)
			    || Dashboard.u().allow('kb.obj.catecontent.' + this.mainP.objectInEditing.cateIdPath.substring(1).replace(/\//g, '.') + '.' + perm)) {
				  ;
			  } else {
				  syncOption.setChecked(false);
				  syncOption.disable()
			  }
		  }
	  },
	  loadData : function(valueRecords, object) {
		  this.valueRecords = {
			  'records' : valueRecords,
			  'object' : object
		  };
		  var self = this;
		  this._confirmModified(function() {
			    self.loadData0(valueRecords, object);
		    })
	  },
	  loadData0 : function(_valueRecords, object) {
		  var valueRecords = [];
		  Ext.each(_valueRecords, function(r) {
			    var rr = r.copy()
			    valueRecords.push(rr);
			    rr.set('faqs', null);
		    });
		  // faqs
		  var valueData0 = valueRecords[0].data;
		  this.attrDtEd.defaultDimTagIds = object.dimTagIds
		  this.attrDtEd._data.object = object
		  this._data.object = object;
		  var valueId = valueData0.valueId ? valueData0.valueId : valueData0.fromValueId
		  if (!valueId) {
			  valueId = valueData0.presetRefId
		  }
		  // if fromValueId, it's a copy/paste one before saving
		  this.fromValueId = valueData0.fromValueId;
		  if (valueId) {
			  if (false && this._savedFaqs && this._savedFaqs.valueId == valueData0.valueId) {
				  valueData0.faqs = this._savedFaqs.faqs;
				  this._loadDataInternal(valueRecords)
			  } else {
				  this.getEl().mask('加载详情数据...')
				  var presetValue = valueId == valueData0.presetRefId
				  Ext.Ajax.request({
					    url : 'ontology-object-value!findFaqs.action',
					    params : {
						    valueId : valueId,
						    ispub : this.ispub,
						    presetValue : presetValue
					    },
					    success : function(response, opts) {
						    this.getEl().unmask()
						    var result = Ext.decode(response.responseText);
						    var faqs = valueData0.faqs = result.data;
						    if (this.fromValueId || presetValue) {
							    Ext.each(faqs, function(f) {
								      delete f.faqId
								      if (f.samples) {
									      Ext.each(f.samples, function(s) {
										        delete s.sampleId
									        })
								      }
							      })
						    }
						    this._loadDataInternal(valueRecords)
					    },
					    failure : function(response, opts) {
						    this.getEl().unmask()
					    },
					    scope : this
				    });
			  }
		  } else {
			  this._loadDataInternal(valueRecords)
		  }
		  this.presetStore.baseParams['attributeId'] = valueData0.attributeId;
		  this.filterAttrRuleFaqsBtn.toggle(false);
		  if (isVersionStandard()) {
			  this.filterAttrRuleFaqsBtn.hide()
			  this.attrDtEd.hide()
			  this.sampleEditGrid.hide()
		  }
		  if (valueData0.attributeId) {
			  this.filterAttrRuleFaqsBtn.enable();
		  } else {
			  this.filterAttrRuleFaqsBtn.disable();
		  }
		  delete this.presetStore._loaded
	  },
	  loadSamples : function(r) {
		  if (r) {
			  this.sampleEditGrid.getStore().loadLocalData(r.data);
		  } else {
			  this.sampleEditGrid.store.removeAll();
		  }
		  this.sampleEditGrid.batchAdd(18);
	  },
	  isEditable : function() {
		  var state = this.attrDtEd.getStore().getAt(0).get('state')
		  if (state == STATE_AUD_SUBMIT || state == STATE_AUD_PASS)
			  return false
		  return true
	  },
	  destroy : function() {
		  obj.detail.AttrDetailEditWindow.superclass.destroy.call(this);
		  if (this.fromValueId)
			  delete this.fromValueId;
		  if (this.attrP)
			  delete this.attrP;
		  if (this.mainP)
			  delete this.mainP;
		  if (this.valueRecords)
			  delete this.valueRecords;
	  }
  });
/**
 * dynDiffField recoverField
 */
obj.detail.FaqDynDeleteButtoner = Ext.extend(DeleteButtoner, {
	  renderer : function(value, metaData, r, rowIndex, colIndex, store) {
		  var ret = ''
		  if (!ux.util.isEmptyRecord(r)) {
			  if (this.isEnableBtn && this.isEnableBtn(r) || r.get('_RM')) {
				  ret = "<a class='recoverButton' href='javascript:void(0);' onclick='javscript:return false;' class='alarm_check'>启用</a>";
			  } else if (this.isDisableBtn && this.isDisableBtn(r) || r.get('_isRule')) {
				  ret = "<a class='deleteButton' href='javascript:void(0);' onclick='javscript:return false;' class='alarm_check'>禁用</a>";
			  } else if (this.isRecoverBtn && this.isRecoverBtn(r)) {
				  ret = "<a class='recoverButton' href='javascript:void(0);' onclick='javscript:return false;' class='alarm_check'>还原</a>";
			  } else {
				  ret = "<a class='deleteButton' href='javascript:void(0);' onclick='javscript:return false;' class='alarm_check'>删除</a>";
			  }
		  }
		  return ret
	  }
  })