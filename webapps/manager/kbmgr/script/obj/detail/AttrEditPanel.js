var kb_edit_allow_field_list = http_get('property!get.action?key=kbmgr.obj_list_display_fields')
if (kb_edit_allow_field_list)
	kb_edit_allow_field_list = kb_edit_allow_field_list.split(',')
else
	kb_edit_allow_field_list = []
var attachmentByRobotSearch = http_get('property!get.action?key=attachment.addr');
var enable_endtime1 = http_get('property!get.action?key=kbmgr.enable_endtime1');
var __enableAudit = http_get('property!get.action?key=kbmgr.enableAudit');
function _reload_apps() {
	var __all_apps = http_get('app!list.action?type=-1');
	if (__all_apps) {
		__all_apps = Ext.decode(__all_apps);
		var _tmp = {}
		Ext.each(__all_apps, function(a) {
			  _tmp[a.id] = a
		  })
		getTopWindow().__all_apps = _tmp;
		delete _tmp;
	}
}
_reload_apps();

var replacePattern = Dashboard.getModuleProp("kbmgr.replace_pattern_renderer");
var replaceReg = [];
var replaceOutputPath = [];
var replaceKeyMap = [];
function readReplaceToMap() {
	if (replacePattern) {
		var patternArry = replacePattern.split("#");
		for (var p = 0; p < patternArry.length; p++) {
			var replacePatternBlock = patternArry[p];
			if (replacePatternBlock) {
				var patternC = replacePatternBlock.split(/\r\n|\r|\n/g);
				if (patternC.length > 1) {
					var replaceRegC = patternC[0];
					var repMap = patternC[1];
					if (replaceRegC) {
						var replaceRegData = replaceRegC.split("&");
						if (replaceRegData[0].indexOf("replaceReg=") > -1)
							replaceReg.push(replaceRegData[0].replace("replaceReg=", ""));
						if (replaceRegData.length > 1) {
							if (replaceRegData[1].indexOf("outputPath=") > -1)
								replaceOutputPath.push(replaceRegData[1].replace("outputPath=", ""));
						}
					}
					if (repMap) {
						var repStr = repMap.split(/,|，/);
						var replaceKeyObj = {};
						for (var i = 0; i < repStr.length; i++) {
							var mapC = repStr[i].split("=");
							replaceKeyObj[mapC[0]] = mapC[1];
						}
						replaceKeyMap.push(replaceKeyObj);
					}
				}
			}
		}
	}
}
readReplaceToMap();
/**
 * @include "../../index.js"
 * @include "/ontologybase/src/main/webapp/kbmgr/script/ux/ux.js"
 */
Ext.namespace('obj.detail')
var STATE_SYNC_SUC = 1;
var STATE_SYNC_FAIL = 2;
var STATE_SYNC_RANGE = [STATE_SYNC_SUC, STATE_SYNC_SUC << 1];

var STATE_AUD_SUBMIT = 8;
var STATE_AUD_PASS = 31;
var STATE_AUD_REJECT = 17;
var STATE_AUD_RANGE = [STATE_AUD_SUBMIT, STATE_AUD_PASS];
var _obj_date_format = 'Y-m-d H:i'
var OP_CREATE = 1;
var OP_MODIFY = 2;
var OP_DELETE = 3;
/**
 * 
 * @type {obj.detail.AttrType}
 */
obj.detail.AttrType = {
	fields : ['attributeId', 'valueId', 'attributeName', 'attributeStdRule', 'attributeType', 'attributeClassId', 'attributeClassBH', 'question', 'faqs', 'values', 'semanticBlock',
	  'objectName', 'objectStatus', 'objectId', 'disabled', 'startTime', 'endTime', 'endTime1', 'faqCount', 'refId', 'editor', 'createTime', 'editTime', 'editTime1', 'catePath',
	  'cateIdPath', 'state', 'op', 'flag', 'dimop', 'sbx', 'sbxSuggested', 'materialId', 'materialType', 'dynrule', 'appcall', 'presetRefId', 'dimAllow', 'variables', 'tags'],
	getRecordType : function() {
		if (!this.rt)
			this.rt = Ext.data.Record.create(this.fields);
		return this.rt;
	},
	createStore : function(cfg) {
		cfg = cfg || {}
		return new Ext.data.JsonStore(Ext.apply({
			  remoteSort : false,
			  idProperty : 'valueId',
			  fields : this.getRecordType()
		  }, cfg));
	}
}
var _vcolumncfg = http_get('ontology-object!getVirtualColumnConfig.action');
var _attrDimType_extra_fields = []
if (_vcolumncfg) {
	_vcolumncfg = Ext.decode(_vcolumncfg)
	Ext.each(_vcolumncfg.columns, function(c) {
		  if (c.type == 'v') {
			  obj.detail.AttrType.fields.push(c.fieldName)
			  _attrDimType_extra_fields.push(c.fieldName)
		  }
	  })
}
_attr_s_f = function(name, sortType) {
	return {
		name : name,
		sortType : sortType ? sortType : function(v) {
			if (!v)
				return '~'
			return v;
		}
	};
}
obj.detail.AttrDimType = {
	isEmptyRecord : function(r) {
		for (var key in r.data) {
			if (key && key != 'dimTagIds' && key != 'dimension' && key != 'state' && key != 'vid' && r.data[key])
				return false;
		}
		return true;
	},
	recordType : Ext.data.Record.create([_attr_s_f('vid'), 'id', 'valueId', 'dimTagIds', _attr_s_f('attributeId'), 'attributeName', 'attributeStdRule', 'attributeType',
	  'attributeClassId', _attr_s_f('attributeClassBH'), 'question', 'dimension', 'value', 'cmd', 'semanticBlock', 'faqs', 'objectName', 'objectStatus', _attr_s_f('objectId'),
	  'disabled', 'startTime', 'endTime', 'endTime1', 'faqCount', _attr_s_f('refId'), 'refBy', 'editor', 'createTime', 'editTime', 'editTime1', 'catePath', 'cateIdPath', 'ispub',
	  'state', 'op', 'flag', 'dimFlag', 'dimop', 'sbx', 'sbxSuggested', 'materialId', 'materialType', 'dynrule', 'appcall', 'presetRefId', 'dimAllow', 'variables', 'tags']
	  .concat(_attrDimType_extra_fields)),
	createStore : function(cfg) {
		cfg = cfg || {}
		return new Ext.data.JsonStore(Ext.apply({
			  remoteSort : false,
			  idProperty : 'vid',
			  fields : this.recordType,
			  beginEdit : function() {
				  this._dataTransCount++;
			  },
			  endEdit : function() {
				  this._dataTransCount--;
				  if (this._dataTransCount == 0) {
					  this.fireEvent('datachanged', this);
				  }
			  },
			  _dataTransCount : 0,
			  listeners : {
				  add : function() {
					  return this._dataTransCount == 0 ? true : false;
				  },
				  update : function(st, record, operation) {
					  return this._dataTransCount == 0 ? true : false;
				  },
				  remove : function() {
					  return this._dataTransCount == 0 ? true : false;
				  },
				  datachanged : function() {
					  return this._dataTransCount == 0 ? true : false;
				  }
			  }
		  }, cfg));

	}
}
/**
 * Grid holding object's VALUES
 */
obj.detail.AttrEditPanel = Ext.extend(ExcelLikeGrid, {
	  // config
	  rowHeight : 36,
	  // to implements
	  getObjectData : Ext.emptyFn,
	  defaultDimTagIds : [],
	  _data : null,
	  isModified : function() {
		  return this.myModified || this.store.modified && this.store.modified.length
	  },
	  resetModifiedState : function() {
		  if (this.store.modified && this.store.modified.length) {
			  delete this.store.modified;
			  this.store.modified = []
		  }
		  if (this.myModified)
			  delete this.myModified
	  },
	  getPrevRow : function(row, col) {
		  if (col >= this._getDimColIndex())
			  return row - 1
		  return this._findAttrDimRange(this._findAttrDimRange(row)[0] - 1)[0]
	  },
	  getNextRow : function(row, col) {
		  if (col >= this._getDimColIndex())
			  return row + 1
		  return this._findAttrDimRange(row)[1] + 1
	  },
	  navigateAttr : function(dir, fromWin) {
		  if (fromWin.vid) {
			  var pos = this.store.find('vid', fromWin.vid);
			  if (dir == 'pre') {
				  pos = this._findAttrDimRange(pos)[0] - 1
			  } else {
				  pos = this._findAttrDimRange(pos)[1] + 1
			  }
			  if (!this.store.getAt(pos)) {
				  Dashboard.setAlert('超出记录范围', 'error')
				  return;
			  }
			  this.selectAttrRow(pos)
			  this.editAttrDetail(pos, null, null, fromWin);
		  }
	  },
	  objectFieldChange : function(field, value) {
		  if (field == 'name') {
			  if (value != this._data.object.name) {
				  this.store.each(function(r) {
					    if (this._isDimParent(r)) {
						    var stdRuleOld = this._replaceByStdRule(r, this._data.object.name);
						    if (r.get('question') == stdRuleOld) {
							    var stdRuleNew = this._replaceByStdRule(r, value);
							    if (r.get('question') != stdRuleNew)
								    r.set('question', stdRuleNew)
						    }
					    }
				    }, this)
				  this._data.object.name = value
			  }
		  }
	  },
	  loadAttrsData : function(d, defaultDimTagIds, toDimValueId, object) {
		  delete this.attrVDeleted;
		  this.dimIdDeleted ? delete this.dimIdDeleted : 0, this.dimIdDeleted = []
		  this._data.object = object;
		  if (!defaultDimTagIds || defaultDimTagIds && defaultDimTagIds.length == 0) {
			  defaultDimTagIds = []
		  }
		  this.toDimValueId = this.lastQ ? null : toDimValueId
		  this.defaultDimTagIds = defaultDimTagIds;
		  this.proxyStore.removeAll();
		  this.proxyStore.loadData(d);
		  if (this.dimAttachment) {
			  for (var key in this.dimAttachment) {
				  if (this.dimAttachment[key])
					  this.dimAttachment[key].destroy();
			  }
			  delete this.dimAttachment
			  this.dimAttachment = {}
		  }
	  },
	  init : function() {
		  if (this._props && this._props[this._props_default_scope])
			  delete this._props[this._props_default_scope];
		  this._props[this._props_default_scope] = {};
	  },
	  validate : function(cb) {
		  var it = this.dimIterator();
		  var attr;
		  var invalidQuestion = '';
		  while (attr = it.nextAttr()) {
			  if (obj.detail.AttrDimType.isEmptyRecord(attr))
				  continue;
			  var dim;
			  var dimV = false;
			  while (dim = it.nextDim()) {
				  if (dim.get('value')) {
					  if (!attr.get('question')) {
						  this.selectRow(this.getStore().indexOf(dim), 0)
						  Ext.Msg.alert('提示', '标准答案 ' + dim.get('value') + ' 没有对应的标准问题')
						  return;
					  }
					  dimV = true;
				  }
			  }
			  if (!dimV) {
				  if (attr.get('attributeType')) {
					  if (attr.get('question')) {
						  var xpb = this.compileSemBlc(this._data.object.semanticBlock);
						  var stdRuleOld = this._replaceByStdRule(attr, xpb);
						  if (attr.get('semanticBlock') || attr.get('question') != stdRuleOld) {
							  invalidQuestion += attr.get('question') + '<br>'
						  }
					  }
				  } else {
					  invalidQuestion += attr.get('question') + '<br>'
				  }
			  }
		  }
		  if (invalidQuestion) {
			  Ext.Msg.confirm('保存确认', '您确定要忽略以下属性的标准问题（被修改过）并保存么？<br>' + invalidQuestion, function(result) {
				    if (result == 'yes') {
					    if (typeof cb == 'function')
						    cb();
				    }
			    })

		  } else if (typeof cb == 'function') {
			  cb();
		  }
	  },
	  getAttrsData : function() {
		  var ret;
		  this.getStore().doWithoutFilter(function() {
			    ret = this.getAttrsData0();
			    ret.dimIdDeleted = this.dimIdDeleted
		    }, this);
		  return ret;
	  },
	  getAttrsData0 : function() {
		  var rs = {};
		  var ret = []
		  var st = this.getStore()
		  st.each(function(r) {
			    var pr = this._getDimParent(r);
			    if (r != pr) {
				    // ref-ed dim v child,skip
				    if (pr.get('refId') && !pr.get('refBy'))
					    return;
			    }

			    var vo = rs[pr.id];
			    var d = Ext.apply({}, r.data);
			    var dimRID = r.id;
			    if (pr.get('refId')) {
				    if (!pr.get('refBy')) {
					    delete d.value;// ref-ed parent,deleting
					    // dimV
				    } else {
					    delete d.refId
				    }
			    }
			    if (!vo) {
				    vo = Ext.apply({}, d);
				    ret.push(vo)
				    rs[pr.id] = vo;
				    vo.values = [];
				    Ext.each(['vid', 'id', 'dimTagIds', 'dimension', 'value', 'cmd', 'objectName', 'attributeName0', 'startTime', 'endTime', 'refBy', 'endTime1', 'dimFlag', 'dimop',
				        'materialId', 'materialType', 'dynrule', 'appcall', 'dimAllow'], function(fname) {
					      delete vo[fname]
				      })
			    } else {
				    if (this._isDimParent(r)) {
					    d.values = vo.values;
					    Ext.apply(vo, d);
					    Ext.each(['vid', 'id', 'dimTagIds', 'dimension', 'value', 'cmd', 'objectName', 'attributeName0', 'startTime', 'endTime', 'refBy', 'state', 'fromValueId',
					        'deleteFromValue', 'op', 'endTime1', 'dimFlag', 'dimop', 'materialId', 'materialType', 'dynrule', 'appcall', 'dimAllow'], function(fname) {
						      delete vo[fname]
					      })
				    }
			    }
			    if (d.value) {
				    Ext.each(['attributeId', 'valueId', 'attributeName', 'attributeType', 'attributeClassId', 'attributeClassBH', 'question', 'faqs', 'vid', 'dimension', 'semanticBlock',
				        'objectName', 'attributeStdRule', 'disabled', 'attributeName0', 'faqCount', 'refBy', 'refId', 'editor', 'createTime', 'editTime', 'state', 'fromValueId',
				        'deleteFromValue', 'cleanFaq', 'op', 'visible', 'flag', 'sbx', 'sbxSuggested', 'dynrule', 'presetRefId', 'dimAllow', 'variables', 'tags'], function(fname) {
					      delete d[fname]
				      })
				    for (var key in d)
					    if (key.indexOf('field') == 0)
						    delete d[key]
				    vo.values.push(d)
				    if (this.dimAttachment) {
					    var win = this.dimAttachment[dimRID];
					    if (win) {
						    d.attachments = win.getAttachments();
						    !win.hasAttachments() && (d.dimFlag & 1) == 1 ? d.dimFlag = 0 : ''
					    }
				    }
			    }
		    }, this);
		  // remove refId's values
		  for (var i = ret.length - 1; i >= 0; i--) {
			  if (ret[i].refId) {
				  delete ret.values
			  } else if (!ret[i].values || !ret[i].values.length) {
				  ret.splice(i, 1);
				  continue;
			  } else {
				  var vo = ret[i]
				  // trans virtual to persist attr field
				  var resetCol = {}
				  for (var key in vo) {
					  if (_vcolumncfg) {
						  var vcnt = 0;
						  Ext.each(_vcolumncfg.columns, function(c) {
							    if (c.fieldName == key) {
								    if (!resetCol[c.vcolumnName]) {
									    vo[c.vcolumnName] = 0
									    resetCol[c.vcolumnName] = true;
								    }
								    if (vo[key]) {
									    if (c.vtype == 0) {
										    vo[c.vcolumnName] |= c.value
									    } else {
										    if (c.value > 10) {
											    vo[c.vcolumnName] += parseInt(vo[key]) * (c.numStart ? c.numStart : 10000)
										    } else {
											    vo[c.vcolumnName] += parseInt(vo[key]) * Math.pow(10, vcnt) * 1000
											    vcnt++;
										    }
									    }
								    }
								    delete vo[key]
								    return false;
							    }
						    }, this)
					  }
					  if (key && key.indexOf('field') == 0)
						  delete vo[key]
				  }
			  }
		  }
		  return ret
	  },
	  // init
	  /**
			 * 
			 * @return {DimensionChooserMenu}
			 */
	  getDimChooserMenu : function() {
		  return this._dimChooser;
	  },
	  getDimTabCheckBoxMenu : function() {
		  return this._dimTabCheckBox;
	  },
	  getCmdChooserMenu : function() {
		  return this.__cmdChooser;
	  },
	  mode : 0,
	  _isModeEmbed : function() {
		  with (obj.detail.AttrEditPanel.MODE) {
			  return this.mode == EMBED;
		  }
	  },
	  _isModeDetail : function() {
		  with (obj.detail.AttrEditPanel.MODE) {
			  return this.mode == DETAIL;
		  }
	  },
	  _isModeList : function() {
		  with (obj.detail.AttrEditPanel.MODE) {
			  return this.mode == LIST;
		  }
	  },
	  /**
			 * 禁止批量添加（包括加载后预置，滚动块点击2种）
			 * 
			 * @type Boolean
			 */
	  disableBatchAdd : false,
	  enableColumnMove : false,
	  initComponent : function() {
		  this._data = {}
		  this.dimAttachment = {}
		  this.id = Ext.id().replace('-');
		  if (this._isModeDetail() || this._isModeList()) {
			  this.disableBatchAdd = true;
		  }
		  if (this._isModeList()) {
			  this.editable = false;
		  }
		  this._dimChooser = new DimensionChooserMenu({
			    buttonsToAdd : [{
				      xtype : 'button',
				      text : '确定',
				      handler : this._editAttrDimension.dg(this)
			      }, {
				      xtype : 'button',
				      text : '新增',
				      handler : this._addAttrDimension.dg(this)
			      }, 'CANCEL']
		    });
		  this._dimTabCheckBox = new DimensionTabCheckBox({
			    buttonsToAdd : [{
				      xtype : 'button',
				      text : '确定',
				      handler : this._editAttrCheckBoxDimension.dg(this)
			      }, {
				      xtype : 'button',
				      text : '新增',
				      handler : this._addAttrCheckBoxDimension.dg(this)
			      }, 'CANCEL']
		    });
		  this.__cmdChooser = new CommandChooserMenu();
		  this.__cmdChooser.init();
		  this.__cmdChooser.on("commandChanged", function() {
			    var r = this.getSelectionModel().getSelected()
			    if (!this._validateRowDimPerm({
				      record : r,
				      value : this.__cmdChooser.getCommand() || 'q'
			      }))
				    return
			    r.set('cmd', this.__cmdChooser.getCommand())
		    }, this);

		  this.selModel = new obj.detail.GroupSelectionModel({
			    columnIndex : 0,
			    getGroupRange : function(rowIndex) {
				    var ret = {
					    range : this.grid._findAttrDimRange(rowIndex)
				    }
				    if (!this.columnIndex || this.columnIndex < this.grid._getDimColIndex()) {
					    ret.ranged = true;
				    }
				    return ret;
			    }
		    });
		  this.colModel = this._buildColModel(this._buildColConfig());
		  if (typeof this._buildTbar == 'function')
			  this.tbar = this._buildTbar()// tbar may depends on
		  // colmodel
		  if (!this.proxyStore)
			  this.proxyStore = obj.detail.AttrType.createStore();
		  if (!this.store)
			  this.store = obj.detail.AttrDimType.createStore();
		  this.store.on('update', function(store, r) {
			    if (r.get('state') < STATE_AUD_RANGE[0] || r.get('state') > STATE_AUD_RANGE[1] || r.get('state') == STATE_AUD_REJECT) {
				    this._getDimParent(r).set('state', 0)
			    }
		    }, this)
		  this.proxyStore.on('load', function(pstore) {
			    this.dynruleCount = 0;
			    if (this._initedBefore)
				    delete this._initedBefore
			    else
				    this.init();
			    this.getDimChooserMenu().init(this._loadDimStore.dg(this, [pstore]));
		    }, this);
		  this._buildContextMenu();

		  this.selectRow = this.selectAttrRow = function(rowIndex, columnIndex) {
			  this.getSelectionModel().columnIndex = columnIndex;
			  this.getSelectionModel().selectRow(rowIndex)
		  }
		  obj.detail.AttrEditPanel.superclass.initComponent.call(this);
		  // bind events before the extjs ones-修改让父类监听器优先加载2015-12-13
		  this.on('cellclick', function(grid, rowIndex, columnIndex, e) {
			    // path for isCellEditable
			    this._allowEdit = false;
			    if (this.editable === false)
				    return;
			    var store = this.getStore();
			    var r = store.getAt(rowIndex);
			    var rr = this._getDimParent(r);
			    var _editable = true
			    if (rr.get('state') == STATE_AUD_SUBMIT || rr.get('op') || rr.get('state') == STATE_AUD_PASS) {
				    _editable = false;
			    }
			    // onclick in cell
			    if (!this._cell_btns) {
				    this._cell_btns = {};
				    for (var key in this) {
					    if (key && key.indexOf('_cell_btn_') == 0 && typeof this[key] == 'function') {
						    this._cell_btns[key.replace('_cell_btn_', '')] = this[key]
					    }
				    }
			    }
			    for (var key in this._cell_btns) {
				    var btn = e.getTarget('.' + key);
				    if (btn) {
					    var authname = btn.getAttribute('authName')
					    if (authname) {
						    var authlist = authname.split(',')
						    Ext.each(this.mainpanel.getObjEditPrivilege(), function(p) {
							      if (authlist.indexOf(p) != -1) {
								      authname = null
								      return false;
							      }
						      }, this)
						    !authname && _editable ? this._cell_btns[key].call(this, rowIndex, btn) : 0
					    } else
						    this._cell_btns[key].call(this, rowIndex, btn)
					    return
				    }
			    }
			    if (!_editable) 
				    return false;
				if (!(this.mainpanel && this.mainpanel.getObjEditPrivilege().M)){
				    if (this._isModeDetail() && 'true' == Dashboard.getModuleProp('ibot.kb.obj.saveobjonly') && !this.ispub) {
					    if ('question value dimension startTime endTime endTime1'.split(' ').indexOf(grid.getColumnModel().getDataIndex(columnIndex)) != -1) {
						    // 问题，答案，维度，开始时间，结束时间
						    return false;
					    }
				    }else
				    	return false
				}
			    // customized selection model need colIndex
			    if (!(e.shiftKey || e.ctrlKey)) {
				    if (columnIndex > 0) {
					    this.getSelectionModel().columnIndex = columnIndex;
					    this.getSelectionModel().origSelect = true
					    this.getSelectionModel().clearSelections()
					    delete this.getSelectionModel().origSelect
					    this.getSelectionModel().selectRow(rowIndex)
					    delete this.getSelectionModel().columnIndex
				    }
			    }
			    if (columnIndex >= this._getDimColIndex() && r.get('dimAllow') === false) {
				    return false;
			    }
			    if (columnIndex < this._getDimColIndex() && !this._isAllDimAllow(r)) {
				    return false;
			    }
			    // edit part
			    var store = this.getStore();
			    if (this._getDimColIndex() == columnIndex) {
				    if (!obj.detail.AttrDimType.isEmptyRecord(r) && !this.dimFieldEditable) {
					    if (this.dimFieldCheckable) {
						    this.getDimTabCheckBoxMenu().show(e.getTarget(), 'tl');
						    this.getDimTabCheckBoxMenu().setDimensionsByTagId(r.get('dimTagIds'));
						    return false;
					    } else {
						    this.getDimChooserMenu().show(e.getTarget(), 'tl');
						    this.getDimChooserMenu().setPositionFn(null)
						    this.getDimChooserMenu().setDimensionsByTagId(r.get('dimTagIds'))
						    var t = e.getTarget();
						    this.getDimChooserMenu().setPositionFn(function() {
							      this.getDimChooserMenu().show(t, 'tl');
						      }.dg(this))
						    this.getDimChooserMenu().show(e.getTarget(), 'tl');
					    }
				    }
			    }

			    this.codeValueEditing = false;
			    if (grid.getColumnModel().getDataIndex(columnIndex) == 'value') {
				    if (r.get('cmd') && r.get('cmd') == ('rask;')) {
					    this.primordialRaskEdit = false;
					    this.__raskValueChooser = new RaskValueMenu({
						      'parent' : this,
						      'row' : rowIndex,
						      'col' : columnIndex
					      });
					    this.__raskValueChooser.init();
					    this.__raskValueChooser.on("raskChanged", function() {
						      var r = this.getSelectionModel().getSelected()
						      r.set('value', this.__raskValueChooser.getValue())
					      }, this);
					    var questionVal = r.get('value');
					    this.__raskValueChooser.setValue(questionVal);
					    if (!this.primordialRaskEdit)
						    this.__raskValueChooser.show(e.getTarget(), 'tl');
					    this.codeValueEditing = true;
					    return false;
				    } else if (r.get('cmd') && r.get('cmd').indexOf('code;') > -1) {
					    this.primordialCodeEdit = false;
					    this.__codeValueChooser = new CodeValueMenu({
						      'parent' : this,
						      'row' : rowIndex,
						      'col' : columnIndex
					      });
					    this.__codeValueChooser.init();
					    this.__codeValueChooser.on("codeChanged", function() {
						      var r = this.getSelectionModel().getSelected();
						      r.set('value', this.__codeValueChooser.getValue())
					      }, this);
					    var questionVal = r.get('value');
					    this.__codeValueChooser.setValue(questionVal)
					    if (!this.primordialCodeEdit)
						    this.__codeValueChooser.show(e.getTarget(), 'tl');
					    this.codeValueEditing = true;
					    return false;
				    }
			    }
			    if (grid.getColumnModel().getDataIndex(columnIndex) == 'cmd') {
				    var cmdVal = r.get('cmd');
				    if (cmdVal)
					    cmdVal = cmdVal.replace(/\[exprq\]/g, '');
				    this.getCmdChooserMenu().setCommand(cmdVal)
				    this.getCmdChooserMenu().show(e.getTarget(), 'tl');
			    }
		    })
		  if (!this.disableBatchAdd) {
			  this.on('scrollbottomclick', function() {
				    this._batchAdd(10);
			    }, this)
		  }
		  this.addEvents(
		    /**
						 * @event editAttributeDetail Fires when start to edit attribute in detail
						 *        view.
						 * @param {obj.detail.AttrEditPanel}
						 *         this
						 * @param {Ext.data.Record}
						 *         record which in grid to edit
						 * @param {Boolean}
						 *         triggerByUser
						 */
		    'editAttributeDetail')
		  this._props = {};
		  this.on('afterrender', function() {
			    this.getView().hmenu.items.each(function(item) {
				      if (item.itemId != 'columns')
					      item.hide()
			      })
			    if (!this._isModeEmbed())
				    return;
			    // add filters
			    this.getView().colMenu.on('beforeshow', function() {
				      this.getView().colMenu.items.each(function(item) {
					        if (item.itemId == 'col-value') {
						        Ext.each(this.getColumnModel().config, function(cfg) {
							          if (cfg.id == 'value') {
								          item.setText(cfg.headerName)
								          return false
							          }
						          })
					        }
				        }, this)
			      }, this)
			    var self = this;
			    var fi = this.filterItems = {}
			    var items = [fi.emtydv = new Ext.menu.CheckItem({
				        text : '过滤空标准答案',
				        checkHandler : function(item, checked) {
					        self.filterEmptyDimAttrValue(checked)
				        },
				        scope : this
			        }), new Ext.menu.CheckItem({
				        text : '过滤继承属性',
				        checkHandler : function(item, checked) {
					        self.filterEmptyInheritedAttr(checked)
				        },
				        scope : this
			        }), new Ext.menu.CheckItem({
				        text : '过滤已禁用属性',
				        checkHandler : function(item, checked) {
					        self.filterDisabledAttr(checked)
				        },
				        scope : this
			        }), new Ext.menu.CheckItem({
				        text : '过滤已填写详情的属性',
				        checkHandler : function(item, checked) {
					        self.filterAttrWithoutFaq(checked)
				        },
				        scope : this
			        }), new Ext.menu.CheckItem({
				        text : '排序关联属性',
				        checkHandler : function(item, checked) {
					        if (checked)
						        this.store.multiSort([{
							            field : 'refId'
						            }, {
							            field : 'attributeClassBH'
						            }, {
							            field : 'attributeType'
						            }, {
							            field : 'vid'
						            }], 'ASC');
					        else
						        this.store.multiSort([{
							            field : 'attributeClassBH'
						            }, {
							            field : 'attributeType'
						            }, {
							            field : 'refId'
						            }, {
							            field : 'vid'
						            }], 'ASC');
				        },
				        scope : this
			        })]
			    this.getView().hmenu.add(items)
		    }, this)

		  this.enableRowHeightAdjust();

		  this.proxyStore.on('beforeload', function() {
			    this.el.mask('加载中...')
		    }, this)
		  this.proxyStore.on('load', function() {
			    this.el.unmask()
		    }, this)
		  this._buildSyncLinkAttrEvents();

		  this.init();
		  this._initedBefore = true;

		  this.on('columnmove', function(oldIndex, newIndex) {
			    delete this.__dimColIndex;
		    }, this)

		  this.on('validateedit', function(e) {
			    this._editAttrDimensionStr(e)
			    if (!this._validateRowDimPerm(e)) {
				    if (e.field == 'value') {
					    this.postEditValue(e.originalValue, '', e.record, e.field)
				    } else {
					    e.record.set(e.field, e.originalValue)
				    }
				    return false;
			    }
			    if (e.field == 'value')
				    return false;
		    }, this);

		  this.setCellHeight = function(row, col, h) {
			  var cellToSet = Ext.fly(this.getView().getCell(row, col));
			  var oldh = cellToSet.getHeight();
			  var d = cellToSet.child('div');
			  d.setHeight(h)
			  if (d = d.child('.toAdjust'))
				  d.setHeight(h)
			  return oldh;
		  }
		  this.on('beforeedit', function(e) {
			    if (this._lastAdjustRow != null) {
				    this.getView().refreshRow(this._lastAdjustRow[0])
				    delete this._lastAdjustRow
			    }
		    })
		  this.on('beforeedit1', function(e) {
			    if (e.field == 'value') {
				    !this._lastAdjustRow ? this._lastAdjustRow = [] : ''
				    this._lastAdjustRow[0] = e.row
				    this._lastAdjustRow[1] = e.column
				    var actualHeight = this.adjustRowCellsHeight(null, this._lastAdjustRow[0], null, e._field.height)
				    e._field.setHeight(actualHeight)
			    }
		    }, this);
		  this.on('headerclick', function(g, columnIndex, e) {
			    if (g.getColumnModel().getDataIndex(columnIndex) == 'value') {
				    if (e.ctrlKey) {
					    var objname = null;
					    if (this.getObjectData() && (objname = this.getObjectData()['name'])) {
						    this.store.beginEdit();
						    this.store.each(function(r) {
							      if (!obj.detail.AttrDimType.isEmptyRecord(r) && !r.get('value')) {
								      var attrname = r.get('attributeName');
								      if (attrname)
									      r.set('value', "【答案】" + objname + '的' + attrname)
							      }
						      })
						    this.store.endEdit();
					    }
				    }
			    }
		    })
	  },
	  // records
	  _loadDimRecordSet : function(r, ds) {
		  ds = ds || []
		  // 修正渲染维度值list时数据源获取的问题，list页面采用r.json，因为需要传递record上的ispub给实例编辑ui;
		  // 实例编辑ui获取json和data都可以;详情编辑界面没有json，所以才用data;实例编辑部分的ispub是panel上设置的，依次传递
		  if (r.json) {
			  r.data.ispub = r.json.ispub
		  }
		  var data = r.data;
		  if (data.values && data.values.length > 0) {
			  for (var i = 0; i < data.values.length; i++) {
				  var dimV = data.values[i];
				  var dataCreated = this._createDimRecordData(data, dimV, i == 0);
				  ds.push(dataCreated);
			  }
		  } else {
			  var dataCreated = this._createDimRecordData(data);
			  ds.push(dataCreated);
		  }
		  return ds;
	  },
	  _loadDimStore : function(pstore) {
		  this.getDimChooserMenu().filterAllowed(this.defaultDimTagIds);
		  var ds = [];
		  // transient value OR the actual value
		  // - as for LIST view,virtual records are created by server to
		  // be
		  // consistent of totalcount
		  var attrsToLoad = []
		  pstore.each(function(r) {
			    if (!r.get('valueId') || r.get('valueId').indexOf('-') == -1) {
				    // set refBy for highlight
				    if (r.get('refId')) {
					    var i = pstore.find('valueId', r.get('refId'));
					    if (i != -1) {
						    // avoid modification garbage of the
						    // record
						    pstore.getAt(i).data.refBy = true;
						    var vs = []
						    Ext.each(pstore.getAt(i).data.values, function(d) {
							      d = Ext.apply({}, d)
							      vs.push(d)
						      })
						    r.data.values = vs
					    }
				    }
				    attrsToLoad.push(r);
			    }
		    }, this)
		  Ext.each(attrsToLoad, function(r) {
			    this._loadDimRecordSet(r, ds)
		    }, this)
		  delete attrsToLoad;
		  // explict gc
		  pstore.removeAll.defer(100, pstore, [])
		  if (this._data.object && this._data.object.statInfo)
			  this.fireEvent('statusInfoChange', new Ext.XTemplate('[属性]:{total}/{inherited}/{rest}' + ' [扩展问]:{allFaqCount}/{dynFaqCount}/{customFaqCount}'
			      + ' [样例]:{allSplCount}/{dynSplCount}/{customSplCount}(总数/动态/自定义)').apply(this._data.object.statInfo), false)
		  if (!this.disableBatchAdd) {
			  for (var i = ds.length; i < 20; i++)
				  ds.push(this._createDimRecordData({}));
		  }
		  this.store.beginEdit();
		  if (this.disableLocalSort)
			  delete this.store.multiSortInfo
		  this.store.loadData(ds)
		  // prepare linkid
		  var st = this.store;
		  var dimIt = this.dimIterator();
		  var r, dimr;
		  while (r = dimIt.nextAttr()) {
			  if (r.get('refBy')) {
				  this.doInDimRange(r, function(r) {
					    r.data.refId = r.data.valueId
				    })
			  } else if (r.get('refId')) {
				  var parentRange = null;
				  while (dimr = dimIt.nextDim()) {
					  if (!parentRange) {
						  var _i = st.find('valueId', r.get('refId'));
						  if (_i != -1)
							  parentRange = this._findAttrDimRange(st.getAt(_i));
					  }
					  if (parentRange)
						  for (var i = parentRange[0]; i <= parentRange[1]; i++) {
							  var rr = st.getAt(i);
							  // if copied from parent,then reset id as
							  // new one
							  if (rr.get('id') == dimr.get('id')) {
								  delete dimr.data.id
								  break;
							  }
						  }
				  }
			  }
		  }
		  if (!this.disableLocalSort && !this.store.multiSortInfo)
			  this.store.multiSort([{
				      field : 'objectName'
			      }, {
				      field : 'attributeClassBH'
			      }, {
				      field : 'attributeType'
			      }, {
				      field : 'vid'
			      }], 'ASC');
		  if (this.filterItems) {
			  if (!this._fedav_0) {
				  this.filterItems.emtydv.setChecked(true, true);
				  this._fedav_0 = true
				  this.filterEmptyDimAttrValue(true)
			  } else {
				  this._doSearch();
			  }
		  }
		  this.store.endEdit();
		  var i = 0;
		  st.each(function(r) {
			    if (window.editAttrDetail && r.get('valueId') == window.standQuesId) {
				    window.editAttrDetail = null;
				    window.navValueId = null;
				    this._cell_btn_editAttrDetail(i);
			    }
			    i++;
		    }, this);
	  },
	  createDimRecord : function(baseData, dimVData, isParent) {
		  var d = this._createDimRecordData(baseData, dimVData, isParent)
		  return new obj.detail.AttrDimType.recordType(d, d.vid);
	  },
	  _createDimRecordData : function(baseData, dimVData, isParent) {
		  if (typeof isParent == 'undefined') {
			  isParent = baseData && !dimVData;
		  }
		  dimVData = dimVData || {}
		  var d = Ext.apply({}, baseData);
		  if (isParent) {
			  if (baseData.valueId)
				  d.vid = baseData.valueId
			  else
				  d.vid = Ext.id(null, '~');
			  baseData.vid = d.vid
			  // add virtual field
			  if (_vcolumncfg) {
				  var vcnt = 0;
				  Ext.each(_vcolumncfg.columns, function(c) {
					    if (c.type == 'v') {
						    var v = 0;
						    if (baseData[c.vcolumnName]) {
							    if (c.vtype == 0) {
								    v = baseData[c.vcolumnName] & c.value
							    } else {
								    if (c.value > 10) {
									    v = parseInt(baseData[c.vcolumnName] / (c.numStart ? numStart : 10000))
								    } else {
									    var pow = Math.pow(10, vcnt);
									    var lastpow = Math.pow(10, vcnt + 1);
									    v = parseInt(baseData[c.vcolumnName] / 1000 % lastpow / pow)
									    vcnt++;
								    }
							    }
						    }
						    if (c.vtype == 0)
							    v = v != 0
						    if (typeof v != 'number' || v) {
							    d[c.fieldName] = baseData[c.fieldName] = v;
						    }
					    }
				    }, this)
			  }
		  } else {
			  delete d.attributeName
			  delete d.question
			  delete d.semanticBlock
			  var trunkId = (baseData.vid ? baseData.vid : baseData.valueId);
			  var brunchId;
			  var vid_counter = this._getprop('vid_counter', {})
			  if (!vid_counter[trunkId])
				  brunchId = vid_counter[trunkId] = 1;
			  else
				  brunchId = (vid_counter[trunkId] += 1);
			  d.vid = trunkId + this._dimIdSpliter + brunchId;
		  }
		  delete d.values
		  d.dimTagIds = dimVData.dimTagIds;
		  d.id = dimVData.id;
		  if (!d.dimTagIds) {
			  d.dimTagIds = this.defaultDimTagIds;
		  }
		  d.dimension = this.getDimChooserMenu().getNameStrById(d.dimTagIds);
		  d.value = dimVData.value;
		  if (this._isModeList()) {
			  d.objectId = baseData.objectId || dimVData.objectId;
			  d.objectName = baseData.objectName || dimVData.objectName;
		  }
		  d.cmd = dimVData.cmd;
		  d.startTime = dimVData.startTime;
		  d.endTime = dimVData.endTime;
		  d.endTime1 = dimVData.endTime1;
		  d.dimFlag = dimVData.dimFlag;
		  d.materialId = dimVData.materialId;
		  d.materialType = dimVData.materialType;
		  d.dimop = dimVData.dimop;
		  d.appcall = dimVData.appcall;
		  if (d.question || baseData.question) {
			  if (!d.dimTagIds || !d.dimTagIds.length) {
				  if (!Dashboard.u().allow('kb.obj.dim.ALLDIM')) {
					  d.dimAllow = false
				  }
			  } else {
				  Ext.each(d.dimTagIds, function(t) {
					    var dim = this.getDimChooserMenu().getDimensionIdOfTag(t, true)
					    if (!dim)
						    d.dimAllow = false
				    }, this)
			  }
		  }
		  return d;
	  },
	  // ui
	  _buildContextMenu : function() {
		  var cellCtxMenu = new Ext.menu.Menu({
			    defaults : {
				    width : 50,
				    scope : this
			    },
			    items : [{
				      ref : 'delAttr',
				      text : '删除属性',
				      iconCls : 'icon-ontology-object-del',
				      handler : this._onDeleteAttrV.dg(this, [])
			      }, {
				      ref : 'delDim',
				      text : '删除维度',
				      iconCls : 'icon-ontology-object-del',
				      handler : this._onDeleteDimV
			      }, {
				      ref : 'addDim',
				      text : '新增维度',
				      iconCls : 'icon-ontology-object-del',
				      handler : this._addAttrDimensionStr.dg(this, [])
			      }, '-', {
				      ref : 'disableAttr',
				      text : '禁用属性',
				      iconCls : 'icon-ontology-object-del',
				      handler : this._onDisableAttrV.dg(this, [false])
			      }, {
				      ref : 'enableAttr',
				      text : '启用属性',
				      iconCls : 'icon-ontology-object-del',
				      handler : this._onDisableAttrV.dg(this, [true])
			      }, '-', {
				      ref : 'linkBtn',
				      text : '关联属性',
				      iconCls : 'icon-attr-link',
				      handler : this._onLinkAttrV.dg(this, [true])
			      }, {
				      ref : 'unlinkBtn',
				      text : '取消属性关联',
				      iconCls : 'icon-attr-link-del',
				      handler : this._onLinkAttrV.dg(this, [false])
			      }, '-', {
				      ref : 'copyAttrBtn',
				      text : '复制属性',
				      iconCls : 'icon-attr-copy',
				      handler : this._onCopyAttrV.dg(this, [])
			      }, __enableAudit ? '' : {
				      ref : 'cutAttrBtn',
				      text : '剪切属性',
				      iconCls : 'icon-attr-cut',
				      handler : this._onCutAttrV.dg(this, [])
			      }, {
				      ref : 'pasteAttrBtn',
				      text : '粘贴属性',
				      iconCls : 'icon-attr-paste',
				      handler : this._onPasteAttrV.dg(this, [])
			      }, !Dashboard.u().isSupervisor() ? '' : {
				      text : '添加变量',
				      iconCls : 'icon-attr-paste',
				      handler : this._onAddAttrVariable.dg(this, [])
			      }, !Dashboard.u().isSupervisor() ? '' : {
				      text : '添加实例引用',
				      iconCls : 'icon-ontology-object',
				      handler : this._onAddAttrObjVar.dg(this, [])
			      }, !attachmentByRobotSearch ? '' : {
				      ref : 'attachmentBtn',
				      text : '上传附件',
				      iconCls : 'icon-attr-paste',
				      handler : this._onAttachment.dg(this, [])
			      }, '-', !__enableAudit ? '' : {
				      ref : 'auditCommitBtn',
				      text : '提交审核',
				      iconCls : 'icon-attr-audit-submit',
				      handler : this._onAudSubmit.dg(this, [1])
			      }, !__enableAudit ? '' : {
				      ref : 'auditRevokeBtn',
				      text : '撤销审核',
				      iconCls : 'icon-attr-audit-submit',
				      handler : this._onAudSubmit.dg(this, [0])
			      }, {
				      ref : 'tagEditBtn',
				      text : '标签设置',
				      iconCls : 'icon-star',
				      handler : this._onSetTag.dg(this, [0])
			      }, {
				      text : '新增10行',
				      ref : 'batchAddBtn',
				      iconCls : 'icon-add',
				      handler : function() {
					      this._batchAdd(10, false);
				      }.dg(this)
			      }]
		    });
		  if (!this._isModeList())
			  this.on('cellcontextmenu', function(grid, rowIndex, cellIndex, e) {
				    e.preventDefault();
				    var m = cellCtxMenu;
				    var rs = null
				    if (cellIndex < this._getDimColIndex()) {
					    grid.selectAttrRow(rowIndex, cellIndex)
					    rs = this.getSelectionModel().getSelections()
				    } else {
					    if (!grid.getSelectionModel().isSelected(rowIndex)) {
						    grid.getSelectionModel().origSelect = true
						    grid.selectRow(rowIndex)
						    delete grid.getSelectionModel().origSelect
					    }
				    }
				    if (this.mainpanel && !this.mainpanel.getObjEditPrivilege().M) {
					    if (this._isModeEmbed())
						    return
					    // 没有修改权限禁用所有菜单除了提交审核
					    for (var k in m) {
						    if (m[k] && typeof m[k].disable == 'function') {
							    m[k].disable();
						    }
					    }
					    if (m.auditRevokeBtn)
						    m.auditCommitBtn.enable();
					    if (m.auditRevokeBtn)
						    m.auditRevokeBtn.enable();
					    m.rowIndex = rowIndex;
					    m.showAt(e.getXY())
					    return
				    }
				    if (cellIndex < this._getDimColIndex()) {
					    rs = this.getSelectionModel().getSelections()
					    var disableCount = 0;
					    var allAttrCount = 0;
					    var audCommit;
					    var editCondition = true;// 预置知识需要禁用右键编辑类操作项
					    Ext.each(rs, function(r) {
						      if (this._isDimParent(r)) {
							      if (this.ispub && isVersionStandard()) {
								      editCondition = false;
							      }
							      if (r.get('state') == STATE_AUD_SUBMIT || r.get('op')) {
								      audCommit = true
								      return false;
							      }
							      disableCount += r.get('disabled') ? 1 : 2
							      allAttrCount++;
						      }
					      }, this)
					    if (audCommit) {
						    for (var k in m) {
							    if (m[k] && typeof m[k].disable == 'function') {
								    m[k].disable();
							    }
						    }
						    if (m.auditRevokeBtn)
							    m.auditCommitBtn.enable();
						    if (m.auditRevokeBtn)
							    m.auditRevokeBtn.enable();
					    } else {
						    if (disableCount == allAttrCount * 1) {
							    m.enableAttr.enable();
							    m.disableAttr.disable();
						    } else if (disableCount == allAttrCount * 2) {
							    m.disableAttr.enable();
							    m.enableAttr.disable();
						    } else {
							    m.disableAttr.enable();
							    m.enableAttr.enable();
						    }
						    // rs
						    var attrRs = []
						    Ext.each(rs, function(r) {
							      if (this._isDimParent(r)) {
								      attrRs.push(r);
							      }
						      }, this)
						    var toLink = 0;
						    if (attrRs.length > 1) {
							    var refId;
							    var refedCount = 0;
							    toLink |= 1;
							    Ext.each(attrRs, function(r) {
								      if (r.get('refId')) {
									      refedCount++;
									      toLink |= 2;
									      if (!refId)
										      refId = r.get('refId');
									      else if (refId != r.get('refId')) {
										      // 2
										      // group,refused!
										      toLink = 0
									      }
								      }
							      }, this)
							    if (refedCount == attrRs.length && toLink)
								    toLink = ~((~toLink) ^ 1);

						    } else if (attrRs[0].get('refId')) {
							    toLink = 2;
						    }
						    (toLink & 1) == 1 ? m.linkBtn.enable() : m.linkBtn.disable();
						    (toLink & 2) == 2 ? m.unlinkBtn.enable() : m.unlinkBtn.disable();

						    var allDimAllow = true
						    Ext.each(attrRs, function(attrr) {
							      if (!this._isAllDimAllow(attrr)) {
								      allDimAllow = false
							      }
						      }, this)
						    if (!allDimAllow) {
							    m.disableAttr.disable();
							    m.enableAttr.disable();
						    }
						    editCondition ? editCondition = allDimAllow : 0
						    m.delAttr[editCondition ? 'enable' : 'disable']();
						    m.delDim.disable();
						    m.addDim.disable();
						    if (m.auditCommitBtn)
							    m.auditCommitBtn.enable();
						    if (m.auditRevokeBtn)
							    m.auditRevokeBtn.enable();
						    m.copyAttrBtn[editCondition ? 'enable' : 'disable']()
						    if (m.cutAttrBtn)
							    m.cutAttrBtn[editCondition ? 'enable' : 'disable']()
						    if (m.attachmentBtn)
							    m.attachmentBtn.disable();
					    }
				    } else {// dim part
					    rs = this.getSelectionModel().getSelections()
					    var editCondition = rs[0] && rs[0].get('dimAllow') !== false
					    editCondition ? editCondition = !(this.ispub && isVersionStandard()) : 0

					    m.copyAttrBtn.disable()
					    if (m.cutAttrBtn)
						    m.cutAttrBtn.disable()
					    m.disableAttr.disable();
					    m.enableAttr.disable();
					    m.delAttr.disable();
					    m.delDim[editCondition ? 'enable' : 'disable']();
					    m.addDim.enable();
					    if (m.attachmentBtn) {
						    if (this._isModeEmbed())
							    m.attachmentBtn[editCondition ? 'enable' : 'disable']();
						    else
							    m.attachmentBtn.disable();
					    }
					    if (m.auditCommitBtn)
						    m.auditCommitBtn.disable();
					    if (m.auditRevokeBtn)
						    m.auditRevokeBtn.disable();
					    m.linkBtn.disable();
					    m.unlinkBtn.disable();
				    }
				    m.pasteAttrBtn[(window._attr_copys ? 'enable' : 'disable')]();

				    m.rowIndex = rowIndex;
				    m.showAt(e.getXY())
			    }, this)
	  },
	  _props : null,
	  _props_default_scope : '_load',
	  _props_cmp_scope : '_cmp',
	  _getprop : function(key, createV, attrScope) {
		  attrScope = attrScope || this._props_default_scope;
		  var scopeAttrs = this._props[attrScope];
		  if (!scopeAttrs) {
			  scopeAttrs = this._props[attrScope] = {};
		  }
		  if (!scopeAttrs[key] && createV)
			  return scopeAttrs[key] = createV;
		  return scopeAttrs[key]
	  },
	  _setprop : function(key, v, attrScope) {
		  attrScope = attrScope || this._props_default_scope;
		  var scopeAttrs = this._props[attrScope];
		  if (!scopeAttrs)
			  return;
		  if (v == null)
			  delete scopeAttrs[key];
		  else
			  scopeAttrs[key] = v;
	  },
	  _buildSyncLinkAttrEvents : function() {
		  this.getStore().on('update', function(st, r) {
			    if (this._getprop('syncing_link_attrs'))
				    return;
			    if (r.get('refId')) {
				    this._setprop('syncing_link_attrs', true);
				    var st = this.getStore()
				    var pr = this._getDimParent(r);
				    var offset = st.indexOf(r) - st.indexOf(pr);
				    var it = this.dimIterator();
				    var attr;
				    var toRemoves = []
				    while (attr = it.nextAttr()) {
					    if (attr != pr && attr.get('refId') == pr.get('refId')) {
						    var rr = st.getAt(st.indexOf(attr) + offset)
						    var dimr = this._createDimRecordData(attr.data, r.data, attr == rr)
						    delete dimr.vid
						    delete dimr.id
						    rr.beginEdit()
						    for (var key in dimr) {
							    rr.set(key, dimr[key])
						    }
						    rr.endEdit();
					    }
				    }
				    this._setprop('syncing_link_attrs', null)
			    }
		    }, this)
	  },
	  _syncLinkDimRemove : function(r) {
		  var st = this.getStore()
		  var pr = this._getDimParent(r);
		  if (pr.get('refId')) {
			  var offset = st.indexOf(r) - st.indexOf(pr);
			  var it = this.dimIterator();
			  var attr;
			  var toRemoves = []
			  while (attr = it.nextAttr()) {
				  if (attr == pr || attr.get('refId') == pr.get('refId'))
					  toRemoves.push(st.indexOf(attr) + offset)
			  }
			  for (var i = toRemoves.length - 1; i >= 0; i--)
				  this._deleteDimV(toRemoves[i])
			  if (toRemoves.length)
				  return true;
		  }
	  },
	  _syncLinkDimAdd : function(r) {
		  var st = this.getStore()
		  var pr = this._getDimParent(r);
		  if (pr.get('refId')) {
			  var offset = st.indexOf(r) - st.indexOf(pr);
			  var it = this.dimIterator();
			  var attr;
			  while (attr = it.nextAttr()) {
				  if (attr != pr && attr.get('refId') == pr.get('refId')) {
					  st.insert(st.indexOf(attr) + offset, this.createDimRecord(attr.data, r.data, false))
				  }
			  }
		  }
	  },
	  // row height adjustment >>>
	  enableRowHeightAdjust : function() {
		  // adjust row cells' height when row updated
		  this.getView().on('rowupdated', this.adjustRowCellsHeight, this);
		  this.getView().on('rowsinserted', this.adjustRowCellsHeight, this);
		  this.getView().on('refresh', this.batchAdjust, this);
	  },
	  getAttrRowHeight : function() {
		  return this.attrRowHeights[this.getSelectionModel().getSelected().id];
	  },
	  attrRowHeights : {},
	  adjustRowCellsHeight : function(view, rowIndex, r, hh) {
		  var range = this._findAttrDimRange(rowIndex);
		  var totalHeight = 0;
		  var actualHeight;
		  for (var i = range[0]; i <= range[1]; i++) {
			  var cell = Ext.fly(this.getView().getCell(i, 0));
			  var h = hh ? Math.max(hh, parseInt(cell.getHeight())) : cell.getHeight();
			  if (rowIndex == i) {// the row changed
				  var endIndex = this._compactView ? this.getColumnModel().getColumnCount() : this._getDimColIndex();
				  for (var col = 1; col < endIndex; col++) {
					  var cellToSet = Ext.fly(this.getView().getCell(i, col));
					  if (this._compactView) {
						  var ctDiv = cellToSet.child('div');
						  if (col >= this._getDimColIndex())
							  ctDiv.setStyle('overflow', 'hidden')
						  ctDiv.setHeight(this.rowHeight)
						  actualHeight = this.rowHeight
					  } else {
						  actualHeight = h
						  var d = cellToSet.child('div');
						  d.setHeight(h)
						  if (d = d.child('.toAdjust'))
							  d.setHeight(h)
					  }
				  }
			  }
			  totalHeight += h;
		  }
		  this.attrRowHeights[this.store.getAt(range[0]).id] = totalHeight;
		  return actualHeight
	  },
	  batchAdjust : function(view) {
		  this.store.each(function(r, i) {
			    this.adjustRowCellsHeight(view, i, r)
		    }, this)
		  // scroll the record to view
		  if (this.toDimValueId || this.lastQ) {
			  var toDimValueId = this.toDimValueId;
			  var lastQ = this.lastQ;
			  delete this.toDimValueId
			  delete this.lastQ;
			  var f = function() {
				  lastQ ? lastQ = lastQ.split('\6') : 0
				  var row = toDimValueId ? this.store.find('id', toDimValueId) : this.store.find('question', lastQ[0])
				  if (row != -1) {
					  if (lastQ && parseInt(lastQ[1])) {
						  row += parseInt(lastQ[1])
					  }
					  this.getSelectionModel().origSelect = true
					  try {
						  this.getSelectionModel().selectRow(row)
					  } finally {
						  delete this.getSelectionModel().origSelect
					  }
					  var row = Ext.fly(this.getView().getRow(row))
					  var xyScrollOffset = row.getOffsetsTo(this.getView().mainBody);
					  this.getView().mainBody.up('.x-grid3-scroller').scrollTo('top', xyScrollOffset[1] - 100, true);
					  // Ops,the scroller
					  // Ext.fly(this.getView().getRow(row)).scrollIntoView(this
					  // .getView().mainBody.dom.parentNode)
				  }
			  };
			  f.defer(100, this);
		  }
	  },
	  // row height adjust <<<
	  reset : function() {
		  // this.store.removeAll();//unnecessary
		  delete this.store.modified
		  this.store.modified = []
		  this.proxyStore.removeAll();
		  delete this.proxyStore.modified
		  this.proxyStore.modified = []
	  },
	  _isHTMLEdToolbarForShow : function() {
		  if (!this._htmlEdElId)
			  return false;
		  if (!this.__htmlEdEl) {
			  this.__htmlEdEl = Ext.getDom(this._htmlEdElId);
		  }
		  if (!this.__htmlEdEl)
			  return false
		  return this.__htmlEdEl.checked;
	  },
	  _getDisableStyle : function(r) {
		  if (!this._isDimParent(r))
			  r = this._getDimParent(r);
		  return r.get('disabled') ? 'color:grey;font-style:italic' : '';
	  },
	  _buildColConfig : function() {
		  var attrEditP = this;
		  // renderer
		  var maxBhLen = 5;
		  var baseColor0 = 0xFFFFa0;
		  var baseColor1 = 0xffdcc2;
		  var baseColor2 = 0x10BFFF;
		  var baseColor3 = 0xC0C0C0;
		  var colorIncre0 = [(0xff0000 - (baseColor0 & 0xff0000)) / maxBhLen, (0xff00 - (baseColor0 & 0xff00)) / maxBhLen, (0xff - (baseColor0 & 0xff)) / maxBhLen];
		  var colorIncre1 = [(0xff0000 - (baseColor1 & 0xff0000)) / maxBhLen, (0xff00 - (baseColor1 & 0xff00)) / maxBhLen, (0xff - (baseColor1 & 0xff)) / maxBhLen];
		  var questionRenderer = function(value, metaData, r, rowIndex, colIndex, store) {
			  if (this._isDimParent(r)) {
				  if (Ext.isEmpty(value) && !this._isModeList()) {
					  this._setQuestionByAttr(r);
					  value = r.data.question
				  }
			  }
			  metaData.attr = 'style=padding:0;background-color:white;' + this._getDisableStyle(r);
			  var s = '<div class="toAdjust x-selectable" style="position:relative;overflow:hidden;height:' + this.rowHeight + 'px;' + '">' + (value ? value : '') + '</div>';
			  var preR = store.getAt(rowIndex - 1);
			  if (preR != null && this._getDimParentId(preR) == this._getDimParentId(r)) {
				  s = '<div style="position:relative;height:0;"><div style="position:absolute;top:-2px;background-color:white' + ';height:2px;width:100%"></div></div>' + s
			  }
			  return s;
		  }
		  var defaultAttrRender = function(value, metaData, r, rowIndex, colIndex, store) {
			  if (obj.detail.AttrDimType.isEmptyRecord(r))
				  return value;
			  var theColor = 0xffffff;

			  var pR = this._getDimParent(r);
			  var s = '';
			  // header
			  var renderHeader = pR == r;
			  var metaStyle = 'padding:0;height:100%;background:#' + theColor.toString(16) + ';'
			  if (metaData.attr && metaData.attr.split('=')[0] == 'style')
				  metaStyle = metaData.attr.split('=')[1] + metaStyle
			  metaData.attr = 'style=' + metaStyle;
			  if (renderHeader) {
				  value = value || ''
				  s += '<div class="toAdjust x-selectable" style="position:relative;overflow:hidden;height:' + this.rowHeight + 'px;' + '">' + value + '</div>'
			  } else {
				  var preR = store.getAt(rowIndex - 1);
				  if (preR != null && this._getDimParentId(preR) == this._getDimParentId(pR)) {
					  s = '<div style="position:relative;height:0;"><div style="position:absolute;top:-2px;background-color:#' + theColor.toString(16) + ';height:2px;width:100%"></div></div>'
					    + s
				  }
			  }
			  return s;
		  }
		  this.dynruleCount = 0;
		  var attrNameRenderer = function(value, metaData, r, rowIndex, colIndex, store) {
			  if (obj.detail.AttrDimType.isEmptyRecord(r))
				  return '<div style="height:100%;width:100%;cursor:pointer" title="点击修改属性类型" class="editAttrType"></div>';
			  var theColor = 0xffffff;
			  var pR = this._getDimParent(r)
			  var preR = store.getAt(rowIndex - 1);
			  var bh = pR.get('attributeClassBH');
			  var baseColor = r.get('sbxSuggested') ? baseColor1 : baseColor0;
			  var colorIncre = r.get('sbxSuggested') ? colorIncre1 : colorIncre0;
			  if (bh) {
				  if (bh.split('.').length > maxBhLen) {
					  throw new Error('AttrEditPanel::questionRenderer:bh exceed the length limit')
				  }
				  var bhlen = r.get('attributeType') == 3 ? 1 : bh.split('.').length - 1
				  theColor = [(baseColor & 0xff0000) + colorIncre[0] * bhlen] | [(baseColor & 0xff00) + colorIncre[1] * bhlen] | [(baseColor & 0xff) + colorIncre[2] * bhlen]
				  if (theColor < 0) {
					  theColor = baseColor;
				  }
			  }
			  if (r.get('presetRefId')) {
				  theColor = baseColor2
			  }
			  if (r.get('disabled') == 2) {
				  theColor = baseColor3
			  }
			  var nextR = store.getAt(rowIndex + 1);
			  var s = ''
			  // header
			  var renderHeader = pR == r;
			  var renderFooter = nextR == null || this._getDimParentId(nextR) != this._getDimParentId(r)
			  if (renderHeader || renderFooter) {
				  s += '<div style="height:100%;position:relative;font-weight:bold;color:#15428B;">'
				  if (renderHeader) {
					  var style = ''
					  // if (r.get('disabled') || !r.get('value'))
					  // style += 'color:#bbb'
					  if (this._compactView)
						  style += 'overflow:hidden'
					  style += this._getDisableStyle(pR);
					  var drcount = '';
					  var dr = r.get('dynrule');
					  if (dr) {
						  var prevdr = preR ? this._getDimParent(preR).get('dynrule') : '';
						  if (!prevdr || dr.split('<-')[0] != prevdr.split('<-')[0]) {
							  this.dynruleCount++;
						  }
						  drcount = '[' + this.dynruleCount + ']';
					  }
					  s += '<div' + (style ? ' style="' + style + '"' : '') + '>'
					    + (bh ? (pR.get('attributeType') == 3 ? '<img src="images/inherited.gif">' : '<img src="images/inherited1.gif">') : '')
					    + (r.get('attributeName') ? r.get('attributeName') : '自定义') + ('<sup><font style="color:red">' + drcount + '</font></sup>') + '</div>'
					  if (isVersionStandard() && dr) {
						  s += '<div style="position:relative;height:0;"><p style="font-weight:normal;font-style:italic;position:absolute;left:8px;width:700px;">' + dr + '</p></div>'
					  }
				  }
				  if (renderFooter) {
					  var yyyField = pR.get('semanticBlock') ? pR.get('semanticBlock') : '';
					  var xxxField = pR.get('sbx') ? pR.get('sbx') : '';
					  var attrType = pR.get('attributeType');
					  var isFaqEmpty = !attrType && !pR.get('faqCount') && pR.get('value');
					  var editCondition = !(pR.get('disabled') != null && pR.get('disabled') != 0) && !(this.ispub && isVersionStandard())
					  var editAttrTypeBtnTpl = ''
					  if ((this._isModeEmbed() || this._isModeDetail()) && editCondition)
						  editAttrTypeBtnTpl = '<a style="right:40px;text-decoration:underline;" authName="M,MFAQ" title="修改属性类型" class="mylink1 editAttrType" href="javascript:void(0);">类型</a>';
					  var editXXXField = ''
					  if (attrType && (this._isModeEmbed() || this._isModeDetail())) {
						  var sx = ''
						  if (xxxField) {
							  for (var key in xxxField) {
								  if (typeof key == 'string')
									  sx += key + ":" + xxxField[key] + ','
							  }
							  if (sx.length)
								  sx = sx.substring(0, sx.length - 1)
						  }
						  editXXXField = '<a style="left:0px;text-decoration:underline;" title="修改语义块' + (sx ? '(' + sx + ')' : '')
						    + '" class="mylink1 semanticBlock" href="javascript:void(0);" authName="M,MFAQ">语义块</a>'
					  }
					  var editDetailTpl = ''
					  if (this._isModeEmbed() && editCondition)
						  editDetailTpl = '<a style="right:0;text-decoration:underline;' + (isFaqEmpty ? 'color:red' : '')
						    + '" title="修改属性详情" class="mylink1 editAttrDetail" href="javascript:void(0)">详情' + (pR.get('faqCount') ? '<SUP>' + pR.get('faqCount') + '</SUP>' : '') + '</a>'
					  s += editDetailTpl + editAttrTypeBtnTpl + editXXXField
				  }
				  s += '</div>';
			  }
			  var metaStyle = 'padding:0;height:100%;background:#' + theColor.toString(16) + ';'
			  metaData.attr = 'style=' + metaStyle;

			  if (preR != null && this._getDimParentId(preR) == this._getDimParentId(pR))
				  s = '<div style="position:relative;height:0;"><div style="position:absolute;top:-2px;background-color:#' + theColor.toString(16) + ';height:2px;width:100%"></div></div>' + s

			  return s;
		  };
		  var NameEditor = Ext.extend(Ext.form.TextArea, {
			    attrEditP : attrEditP,
			    initComponent : function() {
				    this.on('blur', function() {
					      var r = this.recordInEditing;
					      r.beginEdit();
					      var oldattrsb = r.get('semanticBlock')
					      r.set(this.dynFieldName, this.getValue());
					      this.attrEditP._setQuestionByAttr(r, oldattrsb);
					      r.endEdit();
				      })
				    NameEditor.superclass.initComponent.call(this);
			    },
			    setDynParam : function(recordInEditing, dynFieldName) {
				    this.dynFieldName = dynFieldName;
				    this.recordInEditing = recordInEditing;
			    },
			    setValue : function() {
				    if (!this.recordInEditing)
					    return true;
				    return NameEditor.superclass.setValue.call(this, this.recordInEditing.get(this.dynFieldName))
			    },
			    getValue : function() {
				    return NameEditor.superclass.getValue.call(this);
			    }
		    });
		  // editor
		  this.createAttrEditor = function(field, kbEditable) {
			  return new Ext.grid.GridEditor(Ext.create(field), {
				    isKBEditable : function() {
					    return kbEditable;
				    },
				    getCustomEditorSize : function() {
					    return ['', this.getAttrRowHeight()]
				    }.dg(this)
			    })
		  }
		  this.attrNameEd = new NameEditor({
			    dynFieldName : 'attributeName'
		    })
		  var self = this;
		  this.getCustomSortInfo = function() {
			  var si = this._getprop('sortInfo', null, this._props_cmp_scope);
			  return si ? {
				  sort : si[2],
				  dir : si[1],
				  col : si[0]
			  } : null;
		  }
		  this.removeCustomSortInfo = function() {
			  this._setprop('sortInfo', null, this._props_cmp_scope);
		  }
		  this.on('headermousedown', function(grid, columnIndex, e) {
			    var btn = e.getTarget('.x-grid3-hd-btn')
			    if (btn)// ignore click on column selector
				    return;
			    if (this.getColumnModel().config[columnIndex].sortable) {
				    var si = this._getprop('sortInfo', [], this._props_cmp_scope);
				    if (si[0] != columnIndex || si[1] == 'DESC') {
					    si[1] = 'ASC'
				    } else
					    si[1] = 'DESC'
				    si[0] = columnIndex;
				    var di = this.getColumnModel().getDataIndex(columnIndex);
				    if (di == 'attributeName0')
					    di = 'attributeName';
				    si[2] = di;
				    this.fireEvent('customSortChange', di, si[1], columnIndex)
			    }
			    return false;
		    })
		  this.defaultRenderer = function(fn) {
			  return function(v, m, r) {
				  if (typeof fn == 'function')
					  v = fn.apply(this, arguments);
				  if (v) {
					  var disableStyle = this._getDisableStyle(r)
					  if (disableStyle)
						  m.attr += 'style=' + disableStyle
				  }
				  return v
			  }.dg(this);
		  }
		  var cols = [{
			    id : 'attributeName',
			    dataIndex : 'attributeName0',
			    header : '属性名称',
			    width : 155,
			    renderer : attrNameRenderer.dg(this),
			    editor : this.createAttrEditor(this.attrNameEd, false),
			    hideable : false
		    }, {
			    id : 'question',
			    header : '标准问题',
			    dataIndex : 'question',
			    width : 150,
			    replaceable : true,
			    editor : this.createAttrEditor({
				      xtype : 'textarea'
			      }, true),
			    renderer : questionRenderer.dg(this),
			    hideable : false
		    }, {
			    id : 'editor',
			    dataIndex : 'editor',
			    header : '编辑者',
			    hidden : kb_edit_allow_field_list.indexOf('编辑者') == -1,
			    renderer : defaultAttrRender.dg(this)
		    }, {
			    id : 'createTime',
			    dataIndex : 'createTime',
			    header : '创建时间',
			    hidden : kb_edit_allow_field_list.indexOf('创建时间') == -1,
			    renderer : defaultAttrRender.dg(this)
		    }, {
			    id : 'editTime',
			    dataIndex : 'editTime',
			    header : '修改时间',
			    hidden : kb_edit_allow_field_list.indexOf('修改时间') == -1,
			    renderer : defaultAttrRender.dg(this)
		    }, {
			    dataIndex : 'state',
			    header : '状态',
			    width : 60,
			    renderer : function(v, meta, r) {
				    var a = arguments;
				    var s = ''
				    v == 0 ? (a[0] = '未同步', s = 'color:tomato;') : 0
				    v == STATE_SYNC_SUC ? (a[0] = '已同步', s = 'color:green;') : 0
				    v == STATE_SYNC_FAIL ? (a[0] = '同步失败', s = 'color:red;') : 0
				    v == STATE_AUD_SUBMIT ? (a[0] = '提交审批', s = 'color:magenta;') : 0
				    v == STATE_AUD_PASS ? (a[0] = '审批通过', s = 'color:green;') : 0
				    v == STATE_AUD_REJECT ? (a[0] = '审批驳回', s = 'color:red;') : 0
				    if (s) {
					    meta.attr = 'style=' + s + (v == 2 ? 'font-weight:bold;' : '')
					    if (r.get('op') == OP_DELETE)
						    a[0] += '(删除)'
				    }
				    return defaultAttrRender.apply(this, arguments)
			    }.dg(this)
		    }, {
			    dataIndex : 'dimension',
			    header : '维度',
			    width : 70,
			    renderer : this.defaultRenderer(function(v, meta, r) {
				      if (!obj.detail.AttrDimType.isEmptyRecord(r) || !this._isDimParent(r))
					      return v + ((r.get('dimFlag') & 1) == 1 ? '<img src="images/page_attach.png"></img>' : '') + (r.get('dimop') == 3 ? '<span style="color:red">(删除)</span>' : '');
			      }),
			    sortable : false,
			    hideable : false,
			    editor : {
				    xtype : 'textarea'
			    }
		    }, {
			    id : 'value',
			    headerName : '标准答案',
			    header : this._isModeList() ? '标准答案' : ('<div style="float:left;position:relative;top:0px;margin-left:4px;" unselectable="on">标准答案</div>'),
			    dataIndex : 'value',
			    width : 350,
			    replaceable : true,
			    editor : new Ext.grid.GridEditor({
				    field : {
					    KBEditable : true,
					    xtype : 'xhtmleditor',
					    createTxtEditor : function() {
						    var _valueEditor = new FloatHtmlEditor({
							      height : 118,
							      width : '100%',
							      onSpecialKey : function(e) {
								      if (e.getKey() == e.CTRL) {
									      if (!this.getValue()) {
										      if (attrEditP.getObjectData()) {
											      var objname = attrEditP.getObjectData()['name'];
											      var r = attrEditP.getSelectionModel().getSelected();
											      var attrname = r.get('attributeName');
											      this.setValue("【答案】" + objname + '的' + attrname)
										      }
									      }
								      } else if (e.getKey() == e.ESC) {
									      self.stopEditing()
									      self.lastEdit ? self.getView().refreshRow(self.lastEdit.row) : 0

								      }
							      }
						      })
						    _valueEditor.on('styleBtnClick', function() {
							      var old = _valueEditor.getValue();
							      var v = old;
							      v = v.replace(/&lt;HTML&gt;|&lt;\/HTML&gt;/g, '');
							      v = '&lt;HTML&gt;' + v + '&lt;/HTML&gt;';
							      if (v != old)
								      _valueEditor.setValue(v)
						      })
						    return _valueEditor
					    },
					    listeners : {
						    keyup : function(e, t, o) {
							    if (e.getKey() == e.CTRL) {
								    if (!this.getValue().data) {
									    if (attrEditP.getObjectData()) {
										    var objname = attrEditP.getObjectData()['name'];
										    var r = attrEditP.getSelectionModel().getSelected();
										    var attrname = r.get('attributeName');
										    this.setValue("【答案】" + objname + '的' + attrname)
										    this.show()
									    }
								    }
							    }
						    }
					    }
				    }
				     // revertInvalid : false
			     }),
			    renderer : this.defaultRenderer(function(v, m, r) {
				      if (!v)
					      return v
				      if (r.get('materialId')) {
					      var type = r.get('materialType')
					      var url;
					      var imgname
					      if (type == 'imgtxtmsg') {
						      url = "../matmgr/imgtxt-msg!getImgmsg.action?imgmsgId=";
					      } else if (type == 'imagemsg') {
						      url = "../matmgr/imagemsg/imageItem.jsp?imageId=";
					      } else if (type == 'audiomsg') {
						      url = "../matmgr/audiomsg/audioItem.jsp?audioId=";
					      } else if (type == 'videomsg') {
						      url = "../matmgr/videomsg/videoItem.jsp?videoId=";
					      } else if (type == 'p4msg') {
						      url = "../matmgr/p4-msg!showP4Html.action?objectId=";
					      } else if (type == 'app') {
						      var data = this.fillAppArg(r.get('materialId'), r.get('appcall') ? Ext.decode(r.get('appcall')) : null)
						      if (data) {
							      var tpl = new Ext.XTemplate("<div class=\"boxcn1-r-cn2\"><h3 class=\"boxcn1-r-title fl\">应用</h3>\r\n"//
							          + "<div class=\"fl\" style='padding-left:5px;padding-top:3px;'>\r\n" + //
							          '	<div>名称 : {name}</div>' + //
							          '	<tpl if="typeof appArgs != \'undefined\'">' + //
							          '	<tpl for="appArgs">' + //
							          '	<p>{title} = <span style="border:0;border-bottom: 1px dashed #CCCCCC;">{value}</span>' + //
							          '	<tpl if="optional == 1"><font color="red">*</font></tpl>' + //
							          '	</p>' + //
							          '	</tpl>', // use
							        // current
							        // array
							        // index to
							        '	</tpl>' //
							          + "</div><p class=\"clear\"></p></div>" //
							      );
							      return tpl.apply(data); // pass the kids
						      } else {
							      return '应用未找到,id=' + r.get('materialId')
						      }
					      } else if (type == 'refobj') {
						      return '<p>/引用实例 : ' + r.get('value') + '</p>'
					      } else {// txt
						      return 'not supported app type';
					      }

					      if (url) {
						      var title = '';
						      type == 'imagemsg' ? title = '应用' : 0
						      type == 'imgtxtmsg' ? title = '图文' : 0
						      type == 'imagemsg' ? title = '图片' : 0
						      type == 'audiomsg' ? title = '语音' : 0
						      type == 'videomsg' ? title = '视频' : 0
						      type == 'p4msg' ? title = 'P4' : 0
						      return "<div class=\"boxcn1-r-cn2\">\r\n" + "            <h3 class=\"boxcn1-r-title fl\">" + title + "</h3>\r\n"
						        + "            <div class=\"boxcn1-r-cn2p2 bgcol02 pt20 pb20 tc fr\">\r\n" + "                <span class=\"xans-" + type.replace('msg', '')
						        + "1\"><a target='_blank' href=\"" + url + r.get('materialId') + "\" class=\"link1\">" + v + "</a></span>\r\n" + "            </div>\r\n"
						        + "            <p class=\"clear\"></p>\r\n" + "        </div>"
					      }
				      } else {// txt
					      var ret = this.isHTMLTagged(v)
					      if (!ret) {// for htmleditor
						      v = Ext.util.Format.htmlEncode(v).replace(/\r\n|\r|\n/g, "<br>").replace(/ /g, "&nbsp;");
					      } else if ((ret & 1) == 1) {
						      v = v.replace(/<HTML>/i, '&lt;HTML&gt;').replace(/<\/HTML>/i, '&lt;/HTML&gt;')
					      }
					      if (v.indexOf("[expans]") != -1) {
						      v = v.replace(/\[expans\]/gm, "<font color='red'>").replace(/\[\/expans\]/gm, "</font>")
					      }
					      if (replaceReg.length > 0) {
						      for (var i = 0; i < replaceReg.length; i++) {
							      if (replaceReg[i].length > 0) {
								      var regExp = new RegExp(replaceReg[i], 'g');
								      var sb = ''
								      var lastIndex = 0;
								      var regRes
								      if (!replaceOutputPath[i])
									      replaceOutputPath[i] = "";
								      while (regRes = regExp.exec(v)) {
									      var startindex = regRes.index
									      if (startindex > lastIndex) {
										      sb += v.substring(lastIndex, startindex)
									      }
									      lastIndex = regRes.index + regRes[0].length
									      var pre = regRes[1] || ''
									      if (pre)
										      pre = pre.substring(0, Math.floor(pre.length / 2))
									      sb += "<img height='16px' src='" + replaceOutputPath[i] + replaceKeyMap[i][regRes[1]] + "'></img>";
								      }
								      if (lastIndex < v.length) {
									      sb += v.substring(lastIndex, v.length)
								      }
								      v = sb;
							      } else {
								      for (var name in replaceKeyMap[i]) {
									      v = v.replace(name, replaceKeyMap[i][name]);
								      }
							      }
						      }
					      }
					      return v;
				      }
			      })
		    }, {
			    dataIndex : 'cmd',
			    header : '指令',
			    replaceable : true,
			    width : 100,
			    renderer : this.defaultRenderer(function(v, meta, r) {
				      if (v && v.indexOf("rq\(") != -1) {
					      var content = v.substring(v.indexOf("rq(") + "rq(".length);
					      content = content.substring(0, content.indexOf(")"));
					      fromStr = "rq(" + content + ")";

					      var cmds = content.split(',');
					      var tempv = "rq(";
					      for (var i = 0; i < cmds.length; i++) {
						      var _cmd = cmds[i];
						      if (_cmd.indexOf("[exprq]") == 0) {
							      _cmd = _cmd.substring("[exprq]".length);
							      tempv += "<font color='red'>" + _cmd + "</font>";
						      } else {
							      tempv += _cmd;
						      }
						      if (i < cmds.length - 1) {
							      tempv += ",";
						      }
					      }
					      tempv += ")";
					      return v.replace(fromStr, tempv);
				      }
				      return v;
			      }).dg(this)
		    }, {
			    dataIndex : 'startTime',
			    header : '开始时间',
			    renderer : this.defaultRenderer(Ext.util.Format.dateRenderer(_obj_date_format)),
			    editor : {
				    xtype : 'datetimefield1',
				    format : _obj_date_format,
				    height : this.rowHeight + 1
			    }
		    }, {
			    dataIndex : 'endTime',
			    header : '结束时间',
			    renderer : this.defaultRenderer(Ext.util.Format.dateRenderer(_obj_date_format)),
			    editor : {
				    xtype : 'datetimefield1',
				    format : _obj_date_format,
				    height : this.rowHeight + 1
			    }
		    }].concat(enable_endtime1 ? {
			  dataIndex : 'endTime1',
			  header : '追溯时间',
			  renderer : this.defaultRenderer(Ext.util.Format.dateRenderer(_obj_date_format)),
			  editor : {
				  xtype : 'datetimefield1',
				  format : _obj_date_format,
				  height : this.rowHeight + 1
			  }
		  } : [])
		  if (_vcolumncfg) {
			  var v_extra_cfg = []
			  Ext.each(_vcolumncfg.columns, function(c) {
				    if (c.type == 'v') {
					    v_extra_cfg.push({
						      dataIndex : c.fieldName,
						      header : c.name,
						      width : 50,
						      renderer : this.defaultRenderer(c.vtype ? '' : function(v) {
							      return v ? '*' : ''
						      }),
						      editor : {
							      xtype : c.vtype ? 'textfield' : 'checkbox'
						      }
					      })
				    }
			    }, this)
			  Ext.each(cols, function(c, i) {
				    if (c.dataIndex == 'dimension') {
					    cols.splice.apply(cols, [i, 0].concat(v_extra_cfg))
					    return false;
				    }
			    })
		  }
		  var colors = ['red', 'pink', 'yellow', 'blue', 'grey', 'black']
		  var _cols = [this.selModel, new Ext.grid.RowNumberer({
			      renderer : function(v, p, r, row) {
				      if (this._isDimParent(r)) {
					      var refId = r.get('refId');
					      if (refId) {
						      var attrLinkColors = this._getprop('attrLinkColors', {});
						      var c = attrLinkColors[refId]
						      if (!c) {
							      if (attrLinkColors.colorIndex == null)
								      attrLinkColors.colorIndex = -1
							      if (attrLinkColors.colorIndex < colors.length - 1)
								      attrLinkColors.colorIndex++;
							      c = colors[attrLinkColors.colorIndex];
							      attrLinkColors[refId] = c;
						      }
						      var style = 'background:' + c + ';';
						      if (r.get('refBy'))
							      style += 'font-weight:bold'
						      return '<div style="' + style + '">' + (row + 1) + '</div>'
					      }
				      }
				      return (row + 1);
			      }.dg(this)
		      })].concat(cols)
		  return _cols;
	  },
	  _buildColModel : function(_cols) {
		  var colModel = new Ext.grid.ColumnModel({
			    attrEditP : this,
			    columns : _cols,
			    defaults : {
				    menuDisabled : this._isModeDetail(),
				    sortable : this._isModeList()
			    },
			    isCellEditable : function(col, row) {
				    if (this.attrEditP.editable === false)
					    return false;
				    var record = this.attrEditP.getStore().getAt(row);
				    var pr = this.attrEditP._getDimParent(record)
				    if (pr.get('disabled') != null && pr.get('disabled') != 0)
					    return false;
				    if (this.attrEditP.ispub && isVersionStandard() && this.getColumnAt(col).dataIndex != 'value') {
					    return false;
				    }

				    var column = this.getColumnAt(col);
				    var ret = true;
				    if (col < this.attrEditP._getDimColIndex()) {
					    var parentRow = this.attrEditP._findAttrDimRange(row)[0];
					    if (parentRow != row) {
						    if (!this.getColumnAt(col).dataIndex) {
							    this.attrEditP.attrNameEd.setDynParam(this.attrEditP.getStore().getAt(parentRow), 'attributeName');
						    }
						    this.attrEditP.startEditing(parentRow, col);
						    return false;
					    }
				    }
				    if (column.dataIndex == 'attributeName0') {
					    ret = this.attrEditP._allowEdit;
				    } else if (column.dataIndex == 'question') {
					    if (!this.attrEditP._isDimParent(record)) {
						    ret = false;
					    }
				    } else if (column.dataIndex == 'dimension' && !this.attrEditP.dimFieldEditable) {
					    ret = false;
				    }
				    if (!ret) {
					    return ret;
				    }
				    return Ext.grid.ColumnModel.prototype.isCellEditable.call(this, col, row);
			    }
		    });
		  return colModel;
	  },
	  _searchFieldDefaults : {
		  xtype : 'textfield',
		  width : 80
	  },
	  _searchFieldPrefix : '_s_f_',
	  getSearchValueMap : function() {
		  var valueMap = {}
		  Ext.each(this.compactSearchFields, function(f) {
			    if (f.name) {
				    valueMap[f.name] = f.getValue();
			    }
		    })
		  if (valueMap.typeSelector /** && valueMap.typeContent */
		  )
			  valueMap[valueMap.typeSelector] = (valueMap.typeContent != null && valueMap.typeContent != "") ? valueMap.typeContent : valueMap[valueMap.typeSelector];
		  valueMap.typeSelector != null ? delete valueMap.typeSelector : 0
		  valueMap.typeContent != null ? delete valueMap.typeContent : 0
		  if (valueMap.startAndEndTime){
		  		var now = Ext.util.Format.date(new Date(), 'Y-m-d H:i:s')
		  		if (valueMap.startAndEndTime == 1){//未过期
		  			valueMap.endStartTime = now
		  			valueMap.startEndTime = now
		  		}else if (valueMap.startAndEndTime == 2){//已过期
		  			valueMap.endEndTime = now
		  		}
		  }
	  	  valueMap.startAndEndTime != undefined ? delete valueMap.startAndEndTime : 0
		  return valueMap;
	  },
	  _doSearch : function() {
		  var valueMap = this.getSearchValueMap();
		  var filters = []
		  if (this.isFilterInheritedAttr) {
			  filters.push({
				    fn : this._createNonDimFilterFn('', 'FILTER_INHERITED_ATTR'),
				    scope : this
			    });
		  }
		  if (this.isFilterDisabledAttr) {
			  filters.push({
				    fn : this._createNonDimFilterFn('', 'FILTER_DISABLED_ATTR'),
				    scope : this
			    });
		  }
		  if (this.isFilterAttrWithoutFaq) {
			  filters.push({
				    fn : this._createNonDimFilterFn('', 'FILTER_ATTR_NO_FAQ'),
				    scope : this
			    });
		  }
		  if (this.isFilterEmptyDimAttrValue) {
			  filters.push({
				    fn : this._createNonDimFilterFn('', 'FILTER_EMPTY_DIM_ATTR'),
				    scope : this
			    });
		  }
		  this._doSearchImpl(valueMap, filters);
	  },
	  _createNonDimFilterFn : function(field, matchValue) {
		  return function(record) {
			  var rr = record
			  if (!this._isDimParent(record)) {
				  var vid = this._getDimParentId(record);
				  if (vid) {
					  var r = this.getStore().getById(vid);
					  if (r) {
						  record = r;
					  } else {
						  return false;
					  }
				  } else {
					  return false
				  }
			  }
			  if (typeof matchValue == 'function') {
				  return matchValue(rr, record);
			  } else if (matchValue == 'FILTER_INHERITED_ATTR') {
				  return !record.get('attributeType');
			  } else if (matchValue == 'FILTER_DISABLED_ATTR') {
				  return record.get('disabled') != 1;
			  } else if (matchValue == 'FILTER_ATTR_NO_FAQ') {
				  return !record.get('attributeType') && !record.get('faqCount');
			  } else if (matchValue == 'FILTER_EMPTY_DIM_ATTR') {
				  return rr.get('value');
			  } else {
				  var v = record.get(field);
				  return isNaN(v) ? v.indexOf(matchValue) != -1 : v == parseInt(matchValue);
			  }
		  }
	  },
	  _searchFaqSamples : function(t, q, filters) {
		  var objectId = this._data.object && this._data.object.objectId ? this._data.object.objectId : '';
		  if (!objectId) {
			  this._execSearch(filters);
			  return;
		  }
		  Ext.Ajax.request({
			    url : 'ontology-object!searchAttrs.action',
			    params : {
				    queryContent : q,
				    queryType : t,
				    objectId : objectId
			    },
			    success : function(response) {
				    var result = Ext.decode(response.responseText);
				    var d = result.data ? result.data : [];
				    filters.push({
					      fn : this._createNonDimFilterFn('', function(r, pr) {
						        return pr.get('valueId') && d.indexOf(pr.get('valueId')) != -1
					        }),
					      scope : this
				      })
				    this._execSearch(filters);
			    },
			    scope : this
		    })
	  },
	  _buildFiltersByField : function(valueMap, filters) {
		  filters = filters || []
		  var serverSearchQuery;
		  for (var key in valueMap) {
			  var fieldName = key, fieldValue = valueMap[key];
			  if (!fieldName || !fieldName.trim() || !fieldValue || !fieldValue.trim()) {
				  continue;
			  }
			  if (fieldName == 'faq' || fieldName == 'sample') {
				  serverSearchQuery = [fieldName, fieldValue]
			  } else if (fieldName == 'question' || fieldName == 'attributeName' || fieldName == 'semanticBlock' || fieldName == 'state') {
				  filters.push({
					    fn : this._createNonDimFilterFn(fieldName, fieldValue),
					    scope : this
				    })
			  } else {
				  filters.push({
					    fn : function(record) {
						    var ret = false;
						    this.doInDimRange(record, function(_r) {
							      var v = _r.get(fieldName);
							      !ret && v != null ? ret = (("" + v).indexOf(fieldValue) != -1) : 0
						      }, false, this)
						    return ret
					    },
					    scope : this
				    });
			  }
		  }
		  if (serverSearchQuery) {
			  this._searchFaqSamples(serverSearchQuery[0], serverSearchQuery[1], filters);
			  return false;
		  }
		  return true;
	  },
	  _doSearchImpl : function(valueMap, filters) {
		  if (this._buildFiltersByField(valueMap, filters))
			  this._execSearch(filters);
	  },
	  _execSearch : function(filters) {
		  this.benchmark();
		  // default search-impl:local store filter
		  var st = this.getStore()
		  st.beginEdit();
		  st.clearFilter(true)
		  // remove empty ones
		  for (var i = st.getCount() - 1; i >= 0; i--) {
			  var d = st.getAt(i).data;
			  if (!d.attributeId && !d.value && !d.question)
				  st.removeAt(i);
		  }

		  if (filters.length > 0)
			  this.getStore().filter(filters)
		  if (this.getStore().getCount() < 20) {
			  this._batchAdd(20, true);
		  }
		  st.endEdit();
		  this.benchmark();
	  },
	  getSearchFieldsConfig : function() {
		  return [{
			    name : 'semanticBlock',
			    label : '语义块'
		    }, {
			    name : 'attributeName',
			    width : 50,
			    label : '属性名称'
		    }, {
			    name : 'question'
		    }, ((Dashboard.isKBase() && Dashboard.isKBaseTag()) ? {
			    name : 'qtag',
			    label : '标准问标签',
			    valueRenderer : {}
		    } : {
			    name : 'qtag',
			    label : '标准问标签',
			    valueRendererData : {
				    'QTAG_NOBLANK' : '已填写',
				    'QTAG_BLANK' : '未填写'
			    }
		    }), {
			    name : 'dimension',
			    width : 50
		    }, {
			    name : 'value',
			    width : 100
		    }, {
			    name : 'cmd'
		    }, {
			    name : 'cmd-exprq',
			    label : '无效rq'
		    }, {
			    name : 'startAndEndTime',
			    label : '是否过期',
			    valueRendererData : {
				    '1 ' : '未过期',
				    '2 ' : '已过期',
				    ' ' : '---不选择---'
			    }
		    }, {
			    name : 'faq',
			    label : '扩展问'
		    }, {
			    name : 'sample',
			    label : '测试样例'
		    }, {
			    name : 'sampleStatus',
			    label : '样例状态',
			    valueRendererData : function() {
				    var ret = {};
				    ret[' '] = '全部';
				    ret['1'] = '自定义';
				    ret['2'] = '自学习*';
				    ret['3'] = '自学习';
				    return ret;
			    }()
		    }, {
			    name : 'state',
			    label : '同步状态',
			    valueRendererData : function() {
				    var ret = {
					    0 : '未同步'
				    };
				    ret[STATE_SYNC_SUC] = '已同步'
				    ret[STATE_SYNC_FAIL] = '同步失败'
				    ret[STATE_AUD_SUBMIT] = '提交审批'
				    ret[STATE_AUD_PASS] = '审批通过'
				    ret[STATE_AUD_REJECT] = '审批驳回'
				    return ret;
			    }()
		    }, {
			    name : 'disabled',
			    label : '是否禁用',
			    valueRendererData : {
				    0 : '未禁用',
				    1 : '禁用'
			    }
		    }, {
			    name : 'suggestType',
			    label : '匹配类型',
			    valueRendererData : {
				    '1 ' : '预置知识',
				    '2 ' : '推荐语义',
				    '0 ' : '其他',
				    ' ' : '---不选择---'
			    }
		    }, {
			    name : 'editor',
			    label : '编辑者'
		    }, {
			    xtype : 'button',
			    text : '搜索',
			    searchBtn : true,
			    iconCls : 'icon-search',
			    width : 20,
			    handler : this._doSearch.dg(this)
		    }];
	  },
	  getSearchFields : function(fields) {
		  if (!fields)
			  fields = this.getSearchFieldsConfig();
		  Ext.each(fields, function(f) {
			    if (!f.label) {
				    var cfg = this.getColumnModel().config
				    var i = this.getColumnModel().findColumnIndex(f.name);
				    f.label = i != -1 ? (cfg[i].headerName ? cfg[i].headerName : cfg[i].header) : '';
			    }
			    Ext.applyIf(f, this._searchFieldDefaults)
			    if (f.label) {
				    f.ref = '../' + this._searchFieldPrefix + f.name
			    }
		    }, this)
		  return fields;
	  },
	  _getEmptyInheritedAttrFilter : function() {
		  return {
			  fn : function(r) {
			  },
			  scope : this
		  }
	  },
	  isFilterAttrWithoutFaq : false,
	  filterAttrWithoutFaq : function(toFilter) {
		  this.isFilterAttrWithoutFaq = toFilter
		  this._doSearch();
	  },
	  isFilterDisabledAttr : false,
	  filterDisabledAttr : function(toFilter) {
		  this.isFilterDisabledAttr = toFilter
		  this._doSearch();
	  },
	  isFilterInheritedAttr : false,
	  filterEmptyInheritedAttr : function(toFilter) {
		  this.isFilterInheritedAttr = toFilter
		  this._doSearch();
	  },
	  isFilterEmptyDimAttrValue : false,
	  filterEmptyDimAttrValue : function(toFilter) {
		  this.isFilterEmptyDimAttrValue = toFilter
		  this._doSearch();
	  },
	  getCompactSearchFields : function(fields) {
		  this.compactSearchFields = []
		  !fields ? fields = this.getSearchFields() : 0
		  var typeSelectData = []
		  var searchBtn = null;
		  var renderers = []
		  Ext.each(fields, function(f) {
			    if (f.xtype == 'textfield') {
				    typeSelectData.push([f.name, f.label])
			    } else if (f.searchBtn) {
				    searchBtn = Ext.create(f);
			    }
			    if (f.valueRendererData) {
				    var ds = []
				    for (var key in f.valueRendererData) {
					    if (key)
						    ds.push([key.trim ? key.trim() : key, f.valueRendererData[key]])
				    }
				    renderers.push(Ext.create({
					      xtype : 'combo',
					      store : ds,
					      name : f.name,
					      triggerAction : 'all',
					      mode : 'local',
					      hidden : true
				      }))
			    } else if (f.valueRenderer && 'qtag' == f.name && Dashboard.isKBase() && Dashboard.isKBaseTag()) {
				    var typeStore = new Ext.data.Store({
					      autoLoad : true,
					      proxy : new Ext.data.HttpProxy({
						        url : 'kbase-tag!getAllTags.action'
					        }),
					      reader : new Ext.data.JsonReader({
						        idProperty : 'tagId',
						        root : 'data',
						        fields : ['tagId', 'tagName']
					        }),
					      writer : new Ext.data.JsonWriter(),
					      listeners : {
						      'load' : function(store) {
						      }
					      }
				      });

				    renderers.push(Ext.create({
					      xtype : 'combo',
					      store : typeStore,
					      name : f.name,
					      triggerAction : 'all',
					      hidden : true,
					      valueField : 'tagId',
					      displayField : 'tagName'
				      }))
			    }
		    })
		  var searchType = new Ext.form.ComboBox({
			    mode : 'local',
			    emptyText : '请选择要搜索的字段',
			    triggerAction : 'all',
			    store : typeSelectData,
			    name : 'typeSelector',
			    editable : false
		    })
		  this.compactSearchFields.push(searchType);
		  var searchText = new Ext.form.TextField({
			    name : 'typeContent',
			    listeners : {
				    specialkey : function(f, e) {
					    if (e.getKey() == e.ENTER) {
						    this._doSearch();
					    }
				    },
				    scope : this
			    }
		    })
		  this.compactSearchFields.push(searchText);
		  searchBtn.setHandler(this._doSearch.dg(this));
		  var renderersAndTypeContent = renderers.concat(searchText)
		  searchType.on('select', function(combo, r) {
			    var found;
			    Ext.each(renderersAndTypeContent, function(f) {
				      if (f.name == r.get('field1')) {
					      found = true;
					      f.setVisible ? f.setVisible(true) : 0
				      } else {
					      f.setVisible ? f.setVisible(false) : 0
					      f.setValue('')
				      }
			      }, this)
			    if (!found)
				    searchText.show()
		    }, this)
		  searchType.setValue('question')
		  return this.compactSearchFields = this.compactSearchFields.concat(renderers).concat(searchBtn);
	  },
	  _compactView : false,
	  switchView : function(compact) {
		  this._compactView = compact;
		  this.getView().refresh()
	  },
	  // ui event cb
	  _onDisableAttrV : function(enable) {
		  var rs = this.getSelectionModel().getSelections();
		  Ext.each(rs, function(r) {
			    this._disableAttrV(this.getStore().indexOf(r), enable)
		    }, this)
	  },
	  _disableAttrV : function(rowIndex, enable) {
		  if (rowIndex == -1)
			  return;
		  var r = this.getStore().getAt(this._findAttrDimRange(rowIndex)[0])
		  if (r) {
			  r.set('disabled', enable ? 0 : 1)
		  }
		  this.doInDimRange(r, function(_r) {
			    if (_r != r) {
				    this.getView().refreshRow(_r)
			    }
		    }, false, this)
	  },
	  _onDeleteDimV : function() {
		  Ext.Msg.confirm('提示', '您确定要删除选中的维度记录吗？', function(result) {
			    if (result == 'yes') {
				    var rs = this.getSelectionModel().getSelections();
				    if (rs.length) {
					    for (var i = rs.length - 1; i >= 0; i--) {
						    var r = rs[i];
						    if (!this._syncLinkDimRemove(r))
							    this._deleteDimV(this.getStore().indexOf(r))
					    }
				    }
			    }
		    }, this);
	  },
	  _onDeleteAttrV : function(cb) {
		  this.attrVDeleted = true
		  var rs = this.getSelectionModel().getSelections();
		  Ext.Msg.confirm('提示', '您确定要删除选中的' + rs.length + '条属性记录吗？', function(result) {
			    if (result == 'yes') {
				    if (rs.length) {
					    Ext.each(rs, function(r) {
						      this._deleteAttrV(this.getStore().indexOf(r))
					      }, this)
				    }
				    if (typeof cb == 'function')
					    cb()
			    }
		    }, this);
	  },
	  _deleteAttrV : function(rowIndex) {
		  if (rowIndex == -1)
			  return;
		  this.myModified = true;
		  this._unlinkAttrs([this.getStore().getAt(rowIndex)])
		  this.doInDimRange(rowIndex, function(r, row) {
			    if (Dashboard.getModuleProp('kbmgr.enableAudit')) {
				    this.getStore().getAt(row).set('op', OP_DELETE);
				    this.getStore().getAt(row).set('state', 0);
			    } else {
				    if (r.get('vid') && r.get('vid').indexOf('~') == -1) {
					    !this.dimIdDeleted ? this.dimIdDeleted = [] : 0, this.dimIdDeleted.push(r.get('id'))
				    }
				    this.getStore().removeAt(row);
			    }
		    }, true, this)
	  },
	  _deleteDimV : function(rowIndex) {
		  if (rowIndex == -1)
			  return;
		  this.myModified = true;
		  var prow = this._findAttrDimRange(rowIndex);
		  if (Dashboard.getModuleProp('kbmgr.enableAudit')) {
			  this.getStore().getAt(rowIndex).set('dimop', OP_DELETE);
			  this.getStore().getAt(prow[0]).set('state', OP_MODIFY);
		  } else {
			  if (prow[0] == rowIndex) {
				  if (prow[0] == prow[1]) {
					  this._deleteAttrV(rowIndex);
					  delete rowIndex
				  } else {
					  var pr = this.getStore().getAt(rowIndex);
					  var nr = this.getStore().getAt(rowIndex + 1);
					  pr.beginEdit();
					  pr.set('value', nr.get('value'))
					  pr.set('cmd', nr.get('cmd'))
					  pr.set('startTime', nr.get('startTime'))
					  pr.set('endTime', nr.get('endTime'))
					  pr.set('endTime1', nr.get('endTime1'))
					  pr.set('dimTagIds', nr.get('dimTagIds'))
					  pr.set('dimension', this.getDimChooserMenu().getNameStrById(pr.get('dimTagIds')))
					  pr.set('faqCount', nr.get('faqCount'))
					  var pr_dimid = pr.get('id')
					  var nr_id = nr.id
					  pr.set('dimFlag', nr.get('dimFlag'))
					  pr.set('id', nr.get('id'))
					  pr.endEdit()
					  pr.commit();
					  this.getStore().removeAt(rowIndex + 1);
					  // patch dim attachment>>
					  if (this.dimAttachment[nr_id]) {
						  this.dimAttachment[pr.id] = this.dimAttachment[nr_id]
						  delete this.dimAttachment[nr_id]
					  }
					  // <<
					  if (pr.get('vid') && pr.get('vid').indexOf('~') == -1)
						  !this.dimIdDeleted ? this.dimIdDeleted = [] : 0, this.dimIdDeleted.push(pr_dimid)
				  }
			  } else {
				  this.getStore().removeAt(rowIndex);
			  }
		  }
		  if (rowIndex)// refresh to render footer correctly
			  this.doInDimRange(prow[0], function(r) {
				    this.getView().refreshRow(r)
			    }, false, this)
	  },
	  _isAttrDimValueNotEmpty : function(r) {
		  var range = this._findAttrDimRange(r);
		  for (var i = range[0]; i <= range[1]; i++) {
			  if (this.getStore().getAt(i).get('value')) {
				  return true;
			  }
		  }
		  return false;;
	  },
	  _onLinkAttrV : function(link) {
		  var rs = this.getSelectionModel().getSelections();
		  if (!rs.length)
			  return;
		  var attrRs = [];
		  var linkParent = [];
		  var linkedItem;
		  var linkToItem;
		  Ext.each(rs, function(r) {
			    if (this._isDimParent(r)) {
				    attrRs.push(r);
				    if (this._isAttrDimValueNotEmpty(r)) {
					    linkParent.push(r);
				    }
				    if (r.get('refId')) {
					    linkedItem = r;
				    }
				    if (r.get('refBy')) {
					    if (linkToItem)
						    throw new Error('linkToItem count > 1');
					    linkToItem = r;
				    }
			    }
		    }, this)
		  var st = this.getStore();
		  if (link) {
			  if (linkParent.length == 0) {
				  Ext.Msg.alert('提示', '选中属性中没有可被关联的主干项（所有标准答案都为空）');
				  return;
			  } else if (linkToItem) {
				  this._linkAttrs(linkToItem.get('valueId'), attrRs)
			  } else if (linkedItem) {
				  this._linkAttrs(linkedItem.get('refId'), attrRs)
			  } else if (linkParent.length == 1) {
				  this._linkAttrs(linkParent[0].get('valueId'), attrRs)
			  } else {
				  if (!this._parentSelectWin) {
					  this._parentSelectWin = new obj.detail.LinkAttrSelectWin();
					  this._parentSelectWin.on('linkParentSelect', function(valueId, _attrRs) {
						    this._linkAttrs(valueId, _attrRs)
						    this._parentSelectWin.hide();
					    }, this)
				  }
				  this._parentSelectWin.show();
				  this._parentSelectWin.loadItems(attrRs);
			  }
			  this.getView().refresh()
		  } else {
			  this._unlinkAttrs(attrRs)
			  this.getView().refresh()
		  }
	  },
	  _getSelectedAttrRsCopy : function(cut) {
		  var rs = this.getSelectionModel().getSelections();
		  if (!rs.length)
			  return;
		  var attrRId = {}
		  var attrRs = [];
		  Ext.each(rs, function(r) {
			    if (this._isDimParent(r) && !r.get('valueId')) {
				    var title = r.get('attributeName') ? r.get('attributeName') : r.get('question')
				    title = title || ''
				    Ext.Msg.alert('提示', String.format('属性 {0} 未保存，不能被复制/剪切', title));
				    attrRs = null;
				    return false;
			    }
			    var pr = this._getDimParent(r);
			    if (!attrRId[pr.id]) {
				    attrRId[pr.id] = pr.id;
				    this.doInDimRange(pr, function(r, row, offset) {
					      var nr = r.copy()
					      nr.data.fromValueId = nr.data.valueId
					      if (cut)
						      nr.data.deleteFromValue = true
					      delete nr.data.valueId;
					      if (!cut)
						      delete nr.data.id;
					      delete nr.data.refId;
					      delete nr.data.refBy;
					      attrRs.push(nr);
				      })
			    }
		    }, this)
		  delete attrRId
		  return attrRs;
	  },
	  _onCopyAttrV : function() {
		  var rs = this._getSelectedAttrRsCopy(false);
		  if (rs)
			  window._attr_copys = rs;
	  },
	  _onCutAttrV : function() {
		  var rs = this._getSelectedAttrRsCopy(true);
		  if (rs)
			  this._onDeleteAttrV(function() {
				    window._attr_copys = rs;
			    })
	  },
	  _onPasteAttrV : function() {
		  var valid = true;
		  if (window._attr_copys) {
			  var rs = window._attr_copys;
			  this.store.beginEdit()
			  try {
				  // Ext.each(rs, function(r, i) {
				  // var attrId;
				  // if (attrId = r.get('attributeId')) {
				  // var attrType = r.get('attributeType');
				  // if (this.store.findBy(function(_r) {
				  // return _r.get('attributeId') == attrId && (attrType == 1 || attrType ==
				  // 2 && _r.get('semanticBlock') == r.get('semanticBlock'))
				  // }) != -1) {
				  // Ext.Msg.alert('提示', '粘贴失败：被粘贴的属性已存在')
				  // valid = false;
				  // return false;
				  // }
				  // }
				  // }, this)
				  if (valid)
					  Ext.each(rs, function(r, i) {
						    this.store.insert(i, r);
					    }, this)
			  } finally {
				  if (valid)
					  try {
						  delete window._attr_copys;
					  } catch (e) {// IE patch
						  window._attr_copys = null;
					  }
				  this.store.endEdit()
			  }
		  }
	  },
	  _onAddAttrVariable : function() {
		  var r = this.getSelectionModel().getSelected()
		  if (r) {
			  r = this._getDimParent(r)
			  var variables = r.get('variables')
			  !variables ? r.set('variables', variables = []) : 0
			  var varedwin = new VariableEditWin({
				    closeAction : 'close'
			    });
			  varedwin.loadData(variables);
			  varedwin.show()
		  }
	  },
	  _onAddAttrObjVar : function() {
		  var r = this.getSelectionModel().getSelected()
		  if (r) {
			  r = this._getDimParent(r)
			  var variables = r.get('variables')
			  !variables ? r.set('variables', variables = []) : 0
			  var objchswin = new ObjChooserWin({
				    closeAction : 'close'
			    });
			  objchswin.loadData(variables);
			  objchswin.show()
		  }
	  },
	  dimAttachment : null,
	  _onAttachment : function() {
		  var r = this.getSelectionModel().getSelections();
		  if (!r.length)
			  return;
		  var dimRID = r[0].id
		  var win = this.dimAttachment[dimRID]
		  if (!win) {
			  win = this.dimAttachment[dimRID] = new RAMgrWin({
				    topNodeId : '3'
			    })
			  win.load(r[0].get('id') ? r[0].get('id') : '_a_mock_dvid_')
		  }
		  win.show()
	  },
	  _onAudSubmit : function(type) {
		  var rs = this.getSelectionModel().getSelections();
		  Ext.each(rs, function(r) {
			    if (obj.detail.AttrDimType.isEmptyRecord(r))
				    return;
			    if (this._isDimParent(r)) {
				    var state = r.get('state')
				    if (type) {// to submit
					    if (!state) {
						    r.set('state', STATE_AUD_SUBMIT)
					    } else {
						    type = null
						    Ext.Msg.alert('提示', '当前状态不能提交审核')
					    }
				    } else {// reset
					    if (state >= STATE_AUD_RANGE[0] && state <= STATE_AUD_RANGE[1]) {
						    r.set('state', 0)
						    r.set('op', 0)
					    }
				    }
			    }
		    }, this)
		  if (type)
			  this.ownerCt._saveObject();
	  },
	  _onSetTag : function() {
		  var r = this.getSelectionModel().getSelections();
		  if (!r.length)
			  return;
		  var tags = r[0].get('tags');
		  if (!(Dashboard.isKBase() && Dashboard.isKBaseTag())) {
			  Ext.Msg.prompt('提示', '填写标签名称:', function(btn, text) {
				    if (btn == 'ok') {
					    r[0].set('tags', text);
				    }
			    }, this, false, tags);
		  } else {
			  var cfg = {};
			  cfg.title = "标准问标签";
			  var tagIds = tags;
			  if (!this.tagedwin)
				  this.tagedwin = new TagEditWin(cfg);
			  this.tagedwin.setData(tagIds)
			  this.tagedwin.callFunction = function(tagIds) {
				  r[0].set('tags', tagIds)
			  };
			  this.tagedwin.show();
		  }
	  },
	  resetLinkedAttrs : function(attrUpdated) {
		  var st = this.getStore();
		  if (attrUpdated.get('refId')) {
			  this.doInDimRange(attrUpdated, function(r) {
				    r.data.refId = attrUpdated.get('refId')
			    })
		  } else {
			  var i = st.find('refId', attrUpdated.get('valueId'));
			  if (i != -1) {
				  this.doInDimRange(attrUpdated, function(r) {
					    r.data.refId = attrUpdated.get('valueId')
				    })
			  }
		  }
		  this._resetLinkedAttrs();
	  },
	  _resetLinkedAttrs : function() {
		  // reset refId & refBy
		  var st = this.getStore();
		  var it = this.dimIterator();
		  var attr;
		  while (attr = it.nextAttr()) {
			  if (attr.get('refId')) {
				  var attr1 = null;
				  var it1 = this.dimIterator();
				  var refBy = attr.get('refBy');
				  var count = 0;
				  while (attr1 = it1.nextAttr()) {
					  if (attr1 != attr && attr1.get('refId') == attr.get('refId')) {
						  refBy = refBy || attr1.get('refBy')
						  count++;
					  }
				  }
				  if (count == 0) {
					  this.doInDimRange(attr, function(r) {
						    delete r.data.refBy, r.set('refId', null)
					    });
				  } else if (!refBy) {
					  var oldRefId = attr.get('refId');
					  attr.set('refBy', true)
					  if (!oldRefId)
						  this.doInDimRange(attr, function(r) {
							    r.data.refId = r.data.valueId
						    })
					  st.each(function(r) {
						    if (r.get('refId') == oldRefId) {
							    r.set('refId', attr.get('valueId'))
						    }
					    });
				  }
			  }
		  }
	  },
	  _unlinkAttrs : function(attrRs) {
		  // delete link
		  var st = this.getStore()
		  Ext.each(attrRs, function(r) {
			    if (r.get('refId')) {
				    this.doInDimRange(r, function(r) {
					      delete r.data.refId
					      delete r.data.refBy
				      })
			    }
		    }, this)
		  this._resetLinkedAttrs();
	  },
	  _linkAttrs : function(parentValueId, _attrRs) {
		  if (!parentValueId) {
			  Ext.Msg.alert('提示', '被关联到的记录尚未被保存，请先保存后再进行关联')
			  return
		  }
		  var st = this.getStore();
		  Ext.each(_attrRs, function(r) {
			    if (r.get('valueId') != parentValueId) {
				    if (r.get('refId') != parentValueId) {
					    var baseData = Ext.apply({}, r.data);
					    baseData.refId = parentValueId
					    var rowPos = st.indexOf(r);
					    this.doInDimRange(r, function(r, row) {
						      st.removeAt(row);
					      }, true)
					    var rs = [];
					    this.doInDimRange(st.find('valueId', parentValueId), function(rr, row, i) {
						      var dimr = this.createDimRecord(baseData, rr.data, i == 0)
						      delete dimr.data.id
						      rs.push(dimr)
					      }, false, this)
					    Ext.each(rs, function(r, i) {
						      st.insert(rowPos + i, r);
					      })
				    }
			    } else {
				    r.set('refBy', true)
				    this.doInDimRange(r, function(rr, row, i) {
					      r.set('refId', parentValueId)
				      })
			    }
		    }, this)
	  },
	  // event cb
	  ontologyClassesChange : function(ids, clsIdChanged, checked) {
		  if ((!ids || ids.length == 0) && this._otClsChange_cache_ids) {
			  ids = this._otClsChange_cache_ids;
		  }
		  if (this._attrTpEd) {
			  delete this._otClsChange_cache_ids
		  } else {
			  this._otClsChange_cache_ids = ids;
		  }
		  // load inherited attrs dynamically
		  if (clsIdChanged && checked) {
			  Ext.Ajax.request({
				    url : 'ontology-object!loadInheritedAttr.action?classId=' + clsIdChanged,
				    success : function(response) {
					    var result = Ext.decode(response.responseText);
					    if (result.data) {
						    result.data.reverse()
						    Ext.each(result.data, function(valueObj) {
							      if (this.getStore().find('attributeId', valueObj.attributeId) != -1)
								      return;
							      var attrR = new (obj.detail.AttrType.getRecordType())(valueObj)
							      var rds = this._loadDimRecordSet(attrR);
							      Ext.each(rds, function(rd, index) {
								        var _r = new obj.detail.AttrDimType.recordType(rd, rd.vid)
								        this.getStore().insert(index, _r);
							        }, this);
						      }, this);
						    this.getView().refresh();
					    }
				    },
				    scope : this
			    })
		  }
		  this.fireEvent('ontologyClassesChange', ids)
	  },
	  // dyn rule related
	  _replaceByStdRule : function(r, XXXs) {
		  var stdRule = r.get('attributeStdRule')
		  if (stdRule) {
			  var objdata = this.getObjectData();
			  if (!XXXs)
				  XXXs = objdata ? objdata.name : null;
			  stdRule = stdRule.replace(/XXX/g, XXXs);
			  if (r.get('attributeType') == 3) {
				  var sbx = Ext.apply({}, r.get('sbx'))
				  if (sbx && !ux.util.isEmptyData(sbx)) {
					  if (!sbx.concept)
						  sbx.concept = XXXs
					  for (var key in sbx) {
						  var sblock = this.compileSemBlc(sbx[key]);
						  var firstYsInRow = [];
						  Ext.each(sblock.rows, function(row) {
							    if (row[0])
								    firstYsInRow.push(row[0])
						    })
						  if (firstYsInRow.length)
							  stdRule = stdRule.replace(new RegExp('\\[' + key + '\\]', 'ig'), firstYsInRow.join(''))
					  }

					  return stdRule
				  }
			  } else if (r.get('attributeType') == 2) {
				  var YYYs = this.compileSemBlc(r.get('semanticBlock'));
				  var firstYsInRow = [];
				  Ext.each(YYYs.rows, function(row) {
					    if (row[0])
						    firstYsInRow.push(row[0])
				    })
				  if (firstYsInRow.length)
					  stdRule = stdRule.replace(/YYY/g, firstYsInRow.join(''))
				  // if (firstXsInRow.length && firstYsInRow.length) {
				  if (XXXs && firstYsInRow.length) {
					  return stdRule
				  }
				  // } else if (firstXsInRow.length) {
			  } else if (XXXs) {
				  return stdRule
			  }
		  }
		  return '';
	  },
	  _setQuestionByAttr : function(r, oldattrsb) {
		  if (!r.get('attributeType'))
			  return
		  var objdata = this.getObjectData();
		  if (!objdata)
			  return;
		  var stdRuleOld;
		  if (oldattrsb) {
			  var newattrsb = r.data.semanticBlock
			  r.data.semanticBlock = oldattrsb
			  stdRuleOld = this._replaceByStdRule(r)
			  r.data.semanticBlock = newattrsb
		  }
		  if (!stdRuleOld || !r.get('question') || stdRuleOld == r.get('question')) {
			  var stdRuleNew = this._replaceByStdRule(r);
			  r.data.question = stdRuleNew
		  }
	  },
	  _editAttrType0 : function(targetR, attr, cleanFaq) {
		  var fields = ['attributeId', 'attributeName', 'attributeType', 'attributeClassId', 'attributeClassBH', 'attributeStdRule', 'sbx', 'sbxSuggested', 'presetRefId']
		  var oldq = targetR.get('question')
		  if (cleanFaq) {
			  targetR.data.cleanFaq = true
		  } else {
			  delete targetR.data.cleanFaq
		  }
		  this.doInDimRange(targetR, function(r) {
			    for (var i = 0; i < fields.length; i++) {
				    if (r.data[fields[i]] != null)
					    delete r.data[fields[i]]
			    }
		    })
		  if (!attr) {
			  // to plain attrvalue
			  targetR.set('state', 0);
		  } else {
			  // to inherited attrvalue
			  targetR.beginEdit()
			  for (var i = 0; i < fields.length; i++) {
				  targetR.set(fields[i], attr[fields[i]]);
			  }
			  if (!oldq)
				  this._setQuestionByAttr(targetR);
			  targetR.endEdit();
		  }
		  if (targetR.get('attributeType') != 2) {
			  delete targetR.data.semanticBlock
		  }
		  this.doInDimRange(targetR, function(r) {
			    this.getView().refreshRow(r)
		    }, false, this)
	  },
	  _editAttrType : function(r, attr) {
		  var faqKeepOrNot = r.get('faqCount') && this._isModeEmbed();
		  var sbKeepOrNot = (r.get('semanticBlock') || r.get('sbx')) && (r.get('attributeId') && (!attr || r.get('attributeId') != attr.attributeId));
		  if (faqKeepOrNot || sbKeepOrNot) {
			  var msg = []
			  if (sbKeepOrNot) {
				  msg.push('语义块将被清除')
			  }
			  if (faqKeepOrNot) {
				  msg.push('请选择扩展问将被删除还是保留')
			  }
			  var buttons;
			  if (faqKeepOrNot) {
				  buttons = {
					  ok : '删除',
					  no : '保留',
					  cancel : '取消'
				  }
			  } else if (sbKeepOrNot) {
				  buttons = Ext.MessageBox.OKCANCEL
			  }
			  if (buttons) {
				  Ext.Msg.show({
					    title : '提示',
					    msg : msg.join('，'),
					    buttons : buttons,
					    width : 300,
					    fn : function(result) {
						    if (result == 'ok') {
							    if (faqKeepOrNot) {
								    this._editAttrType0(r, attr, true)
							    } else {
								    this._editAttrType0(r, attr, false)
							    }
						    } else if (result == 'no') {
							    if (faqKeepOrNot) {
								    this._editAttrType0(r, attr, false)
							    }
						    }
					    },
					    scope : this
				    });
				  return;
			  }
		  }
		  this._editAttrType0(r, attr, false)
	  },
	  // dimension
	  DimAttrIterator : function(attrEditP) {
		  this.row = 0;
		  this.parentvid;
		  this.firstRow;
		  var st = attrEditP.getStore();
		  this.nextAttr = function() {
			  while (this.row < st.getCount()) {
				  var r = st.getAt(this.row++);
				  if (attrEditP._isDimParent(r)) {
					  this.parentvid = r.get('vid')
					  this.firstRow = r;
					  return r;
				  }
			  }
			  delete this.parentvid
		  }
		  this.nextDim = function() {
			  var _st = st;
			  if (this.parentvid) {
				  if (this.firstRow) {
					  var r = this.firstRow;
					  delete this.firstRow
					  return r;
				  }
				  if (this.row < st.getCount()) {
					  var r = st.getAt(this.row);
					  if (!attrEditP._isDimParent(r)) {
						  this.row++;
						  return r;
					  }
				  }
			  }
		  }

	  },
	  /**
			 * 
			 * @return {this.DimAttrIterator}
			 */
	  dimIterator : function() {
		  return new this.DimAttrIterator(this);
	  },
	  _getDimColIndex : function() {
		  if (!this.__dimColIndex) {
			  /**
					 * @type {Ext.grid.ColumnModel}
					 */
			  var cm = this.getColumnModel();
			  for (var i = 0; i < cm.getColumnCount(); i++) {
				  var c = cm.getColumnAt(i);
				  if (c.dataIndex == 'dimension') {
					  this.__dimColIndex = i;
					  break;
				  }
			  }
		  }
		  return this.__dimColIndex;
	  },
	  /**
			 * cb(r,row,offsetToParentR)
			 */
	  doInDimRange : function(r, cb, reverse, scope) {
		  var range = this._findAttrDimRange(r);
		  if (reverse)
			  for (var i = range[1]; i >= range[0]; i--) {
				  cb.call(scope, this.getStore().getAt(i), i, range[1] - i)
			  }
		  else
			  for (var i = range[0]; i <= range[1]; i++) {
				  cb.call(scope, this.getStore().getAt(i), i, i - range[0])
			  }
	  },
	  _findAttrDimRange : function(rowIndex) {
		  if (rowIndex == null) {
			  rowIndex = this.getStore().indexOf(this.getSelectionModel().getSelected())
		  }
		  var r;
		  var attrBlockStartIndex = rowIndex;
		  if (rowIndex instanceof Ext.data.Record) {
			  r = rowIndex;
			  attrBlockStartIndex = this.getStore().indexOf(r);
		  } else
			  r = this.getStore().getAt(rowIndex)
		  var vid = r.get('vid');
		  if (!this._isDimParent(r)) {
			  attrBlockStartIndex = this.getStore().find('vid', this._getDimParentId(r));
			  vid = this._getDimParentId(r);
		  }

		  var rs = this.getStore().getRange(attrBlockStartIndex);
		  var attrBlockEndIndex = attrBlockStartIndex;
		  for (var i = 1; i < rs.length; i++) {
			  if (rs[i].get('vid').indexOf(vid + this._dimIdSpliter) == -1) {
				  break;
			  } else {
				  attrBlockEndIndex = i + attrBlockStartIndex
			  }
		  }
		  return [attrBlockStartIndex, attrBlockEndIndex];
	  },
	  _validateDimensionInRange : function(tagIds, r, edit) {
		  if (!tagIds.length && !Dashboard.u().allow('kb.obj.dim.ALLDIM')) {
			  Dashboard.setAlert('您没有权限将维度值设置为"所有维度"，请重新选择', 'error');
			  return
		  }
		  var rp = this.store.indexOf(r)
		  var range = this._findAttrDimRange(rp);
		  var tempMap = {};
		  for (var i = 0; i < tagIds.length; i++) {
			   if(tagIds[i] != 'searchAll')
				  continue;
			  if (tempMap[tagIds[i]]) {
				  Dashboard.setAlert('维度值重复！同一维度下不允许重复的维度值，请重新选择', 'error');
				  return;
			  }
			  tempMap[tagIds[i]] = true
		  }
		  delete tempMap
		  var newdim = tagIds.sort().join(',')
		  for (var i = range[0]; i <= range[1]; i++) {
			  var dimTagIds = this.store.getAt(i).get('dimTagIds');
			  if (newdim == dimTagIds.sort().join(',') && (!edit || rp != i)) {
				  Dashboard.setAlert('维度值组合重复！当前属性中已存在该维度值组合，请重新选择', 'error');
				  return false;
			  }
		  }
		  return true;
	  },
	  _validateRowDimPerm : function(e) {
		  var q = e.value
		  var r = e.record
		  if (!r)
			  return
		  if (q) {
			  var dimtagids = r.get('dimTagIds');
			  if (!dimtagids || !dimtagids.length && !Dashboard.u().allow('kb.obj.dim.ALLDIM')) {
				  var dt = this.getDimChooserMenu().getFirstAllowDimTagId()
				  if (dt) {
					  r.set('dimTagIds', [dt.id])
					  r.set('dimension', this.getDimChooserMenu().getNameStrById([dt.id]));
				  } else {
					  Dashboard.setAlert('没有授权任何可用维度值，请管理员重新授权', 'error')
					  return false
				  }
			  }
		  }
		  var pr = this._getDimParent(r)
		  var offset = 0;
		  this.doInDimRange(pr, function(r, ri, i, cnt) {
			    if (e.record == r)
				    offset = i;
		    })
		  pr ? this.lastQ = pr.get('question') + '\6' + offset : 0
		  return true
	  },
	  _editAttrDimensionStr : function(e) {
		  if (e.field == 'question') {
			  var r = e.record;
			  var q = e.value;
			  if (q && q.trim() && !r.get('attributeId') && !r.get('valueId') && !r.get('presetRefId') && !r.get('sbxSuggested')) {
				  Ext.Ajax.request({
					    url : 'ontology-object!getSemanticSuggestion.action',
					    params : {
						    q : q.trim()
					    },
					    // maskEl : maskel,
					    getMaskEl : function() {
						    var maskel = Ext.fly(this.getView().getRow(e.row));
						    maskel.maskMsg = '正在努力分析中...';
						    return maskel;
					    }.dg(this),
					    success : function(response) {
						    if (response.responseText) {
							    var semantic = Ext.decode(response.responseText);
							    if (semantic) {
								    // attributeName', 'attributeStdRule',
								    // 'attributeType',
								    // 'attributeClassId',
								    // 'attributeClassBH',
								    if (semantic.presetRefId) {
									    e.record.set('presetRefId', semantic.presetRefId);
									    e.record.set('semanticBlock', semantic.semanticBlock);
								    } else {
									    e.record.set('sbxSuggested', 1);
								    }
								    e.record.set('attributeId', semantic.attributeId);
								    e.record.set('attributeName', semantic.attributeName);
								    e.record.set('attributeStdRule', semantic.attributeStdRule);
								    e.record.set('attributeType', semantic.attributeType);
								    e.record.set('attributeClassId', semantic.attributeClassId);
								    e.record.set('attributeClassBH', semantic.attributeClassBH);
								    e.record.set('sbx', semantic.semanticBlocks);
							    }
						    }
					    },
					    scope : this
				    })
			  }
			  return;
		  }
		  // if (e.field == 'value') {
		  // e.record.set('materialId', '');
		  // e.record.set('materialType', '');
		  // return;
		  // }

		  if (e.field != 'dimension')
			  return;
		  var dimstr = e.value
		  var r = e.record
		  if (!r)
			  return
		  var tagIds = []
		  if (dimstr && ALL_DIM_STRING != dimstr.trim()) {
			  var dimtagnames = dimstr.split(/[,，;；]/);
			  var name_ids = this.getDimChooserMenu().findTagIds(dimtagnames);
			  var notFound = []
			  Ext.each(dimtagnames, function(n) {
				    if (!name_ids[n])
					    notFound.push(n)
				    else
					    tagIds.push(name_ids[n])
			    })
			  if (notFound.length) {
				  Ext.Msg.alert('错误', '维度标签不存在:' + notFound.join(','))
				  r.set('dimension', e.originalValue)
				  return false;
			  }
		  }
		  if (!this._validateDimensionInRange(tagIds, r, true)) {
			  r.set('dimension', e.originalValue)
			  return;
		  }
		  r.set('dimTagIds', tagIds);
		  r.set('dimension', this.getDimChooserMenu().getNameStrById(tagIds));
	  },
	  _addAttrDimensionStr : function() {
		  var parentR = this.getSelectionModel().getSelected();
		  if (!parentR)
			  return
		  Ext.Msg.prompt('维度设置', '请填写需要添加的维度标签，逗号或者分号间隔（不填写直接确定表示添加"所有维度"）', function(result, v) {
			    if (result != 'ok')
				    return
			    v = v || ''
			    var tagIds = []
			    if (v && ALL_DIM_STRING != v.trim()) {
				    var dimtagnames = v.split(/[,，;；]/);
				    var name_ids = this.getDimChooserMenu().findTagIds(dimtagnames);
				    var notFound = []
				    Ext.each(dimtagnames, function(n) {
					      if (!name_ids[n])
						      notFound.push(n)
					      else
						      tagIds.push(name_ids[n])
				      })
				    if (notFound.length) {
					    Ext.Msg.alert('错误', '维度标签不存在:' + notFound.join(','))
					    return false;
				    }
			    }
			    // new add
			    if (!this._validateDimensionInRange(tagIds, parentR, false))
				    return;
			    var dimr = this.createDimRecord(parentR.data, {
				      dimTagIds : tagIds
			      })
			    this.getStore().insert(this.getStore().indexOf(parentR) + 1, dimr)
			    this.getDimChooserMenu().hide();
			    this._syncLinkDimAdd(dimr)
			    this.getView().refresh();
		    }, this)
	  },
	  _editAttrDimension : function(dim) {
		  dim = this.getDimChooserMenu().getSelectedDimensions();
		  var tagIds = this._getTagIds(dim)
		  var r = this.getSelectionModel().getSelected();
		  if (!this._validateDimensionInRange(tagIds, r, true))
			  return;
		  if (r) {
			  r.set('dimTagIds', tagIds);
			  r.set('dimension', this.getDimChooserMenu().getNameStrById(tagIds));
		  }
		  this.getDimChooserMenu().hide();
	  },
	  _editAttrCheckBoxDimension : function(dim) {
		  this.getDimTabCheckBoxMenu().submitValue();
		  var tagIds = this.getDimTabCheckBoxMenu().getValue();
		  var r = this.getSelectionModel().getSelected();
		  if (!this._validateDimensionInRange(tagIds, r, true))
			  return;
		  if (r) {
			  r.set('dimTagIds', tagIds);
			  r.set('dimension', this.getDimTabCheckBoxMenu().getNameById(tagIds));
		  }
		  this.getDimTabCheckBoxMenu().hide();
	  },
	  _addAttrDimension : function(dim) {
		  dim = this.getDimChooserMenu().getSelectedDimensions();
		  var tagIds = this._getTagIds(dim)
		  // new add
		  var parentR = this.getSelectionModel().getSelected();
		  if (!this._validateDimensionInRange(tagIds, parentR, false))
			  return;
		  if (parentR) {
			  var dimr = this.createDimRecord(parentR.data, {
				    dimTagIds : this._getTagIds(dim)
			    })
			  this.getStore().insert(this.getStore().indexOf(parentR) + 1, dimr)
		  }
		  this.getDimChooserMenu().hide();
		  this._syncLinkDimAdd(dimr)
		  this.getView().refresh();
	  },
	  _addAttrCheckBoxDimension : function(dim) {
		  this.getDimTabCheckBoxMenu().submitValue();
		  var tagIds = this.getDimTabCheckBoxMenu().getValue();
		  // new add
		  var parentR = this.getSelectionModel().getSelected();
		  if (!this._validateDimensionInRange(tagIds, parentR, false))
			  return;
		  if (parentR) {
			  var dimr = this.createDimRecord(parentR.data, {
				    dimTagIds : tagIds
			    })
			  this.getStore().insert(this.getStore().indexOf(parentR) + 1, dimr)
		  }
		  this.getDimTabCheckBoxMenu().hide();
		  this._syncLinkDimAdd(dimr);
		  this.getView().refresh();
	  },
	  _dimIdSpliter : '|',
	  _isDimParent : function(r) {
		  return r.get('vid').indexOf(this._dimIdSpliter) == -1;
	  },
	  _getDimParentId : function(r) {
		  if (this._isDimParent(r)) {
			  return r.get('vid');
		  }
		  return r.get('vid').substring(0, r.get('vid').indexOf(this._dimIdSpliter));
	  },
	  _getDimParent : function(r) {
		  var vid = r.get('vid');
		  var i = vid.indexOf(this._dimIdSpliter);
		  if (i == -1) {
			  return r;
		  }
		  return this.getStore().getById(vid.substring(0, i))
	  },
	  _getTagIds : function(tags) {
		  var tagIds = []
		  for (var i = 0; i < tags.length; i++) {
			  tagIds.push(tags[i].id);
		  }
		  return tagIds;
	  },
	  _isAllDimAllow : function(attrr) {
		  var ret = true
		  this.doInDimRange(attrr, function(rr) {
			    if (rr.get('dimAllow') === false) {
				    ret = false
			    }
		    })
		  return ret
	  },
	  // cell button
	  _cell_btn_editAttrType : function(row, target) {
		  if (!this._attrTpEd) {
			  this._attrTpEd = new ClassAttributeChooserMenu();
			  this._attrTpEd.on('attributeChanged', function(attr) {
				    this.fireEvent('attributeChanged', this._attrTpEd._r, attr)
				    this._editAttrType(this._attrTpEd._r, attr);
			    }, this);
		  }
		  this._attrTpEd._r = this._getDimParent(this.getStore().getAt(row));
		  this._attrTpEd.show(target, 't');
	  },
	  _cell_btn_semanticBlock : function(row, btn) {
		  // this._allowEdit = true;
		  var r = this._getDimParent(this.getStore().getAt(row));
		  if (r.get('attributeId')) {
			  Ext.Ajax.request({
				    url : 'ontology-class!getAttributePriorityRule.action',
				    params : {
					    attrId : r.get('attributeId')
				    },
				    success : function(response) {
					    var rule = response.responseText;
					    if (rule) {
						    var data = {
							    contextVar : r.get('sbx') || {},
							    rule : rule
						    }// type==3
						    if (r.get('attributeType') < 3) {
							    data.contextVar.XXX ? 0 : data.contextVar.XXX = this._data.object.semanticBlock
							    data.contextVar.YYY ? 0 : data.contextVar.YYY = r.get('semanticBlock')
						    } else {
							    data.contextVar.concept ? 0 : data.contextVar.concept = this._data.object.semanticBlock
						    }
						    var w = this.semanticEditor
						    if (!w) {
							    w = this.semanticEditor = new KBSemanticEditor();
						    }
						    w.show(btn)

						    w.loadData(data, function(contextVar) {
							      var c = 0;
							      for (var key in contextVar) {
								      var v = contextVar[key] ? contextVar[key].trim() : null
								      if (!v) {
									      Dashboard.setAlert('存在未填写的语义块，请重新填写', 'error')
									      return false;
								      }
								      if ((key == 'concept' || key == 'XXX') && v == this._data.object.semanticBlock) {
									      delete contextVar[key]
								      } else if (key == 'YYY') {
									      r.set('semanticBlock', contextVar[key])
									      delete contextVar[key]
								      } else
									      c++;
							      }
							      if (c == 0) {
								      contextVar = null
							      }
							      r.set('sbx', contextVar)
							      r.set('sbxSuggested', null)
						      }.dg(this));
					    }
				    },
				    scope : this
			    })
		  }
	  },
	  _cell_btn_editAttrName : function(row, btn) {
		  var r = this.getStore().getAt(row);
		  if (r.get('attributeType') != 1) {
			  this.attrNameEd.setDynParam(r, 'attributeName');
		  }
	  },
	  editAttrDetail : function(row, oldvid, btn, fromWin) {
		  if (!this._data.object || !this._data.object.objectId) {
			  Ext.Msg.alert('提示', '实体未保存，请先保存后再编辑详情')
			  return
		  }
		  if (!this.store.getAt(row).get('value')) {
			  Ext.Msg.alert('提示', '未填写答案，请先填写答案后再编辑详情')
			  return
		  }
		  this.selectAttrRow(row, 0)
		  var range = this._findAttrDimRange(row);
		  var rs = this.store.getRange(range[0], range[1]);
		  if (rs) {
			  if (!rs[0].data['faqs']) {
				  rs[0].data['faqs'] = [];
			  }
			  this.fireEvent('editAttributeDetail', this, rs, btn != null, oldvid, btn, fromWin);
		  }
	  },
	  _cell_btn_editAttrDetail : function(row, btn) {
		  this.editAttrDetail(row, null, btn)
	  },
	  isHTMLTagged : function(value, typemask) {
		  var ret = 0;
		  if (value) {
			  if ((!typemask || ((typemask & 1) == 1)) && /^<HTML>/i.test(value))
				  ret |= 1
			  if ((!typemask || ((typemask & 2) == 2)) && /^&lt;HTML&gt;/i.test(value))
				  ret |= 2
		  }
		  return ret
	  },
	  onEditComplete : function(ed, value, startValue) {
		  this.editing = false;
		  this.lastActiveEditor = this.activeEditor;
		  this.activeEditor = null;

		  var r = ed.record, field = this.colModel.getDataIndex(ed.col);
		  value = this.postEditValue(value, startValue, r, field);
		  var notEqualsV = String(value) !== String(startValue)
		  if (typeof value == 'object') {// 修改对象比较方法
			  notEqualsV = Ext.encode(value) !== Ext.encode(startValue)
		  }
		  if (this.forceValidation === true || notEqualsV) {
			  var e = {
				  grid : this,
				  record : r,
				  field : field,
				  originalValue : startValue,
				  value : value,
				  row : ed.row,
				  column : ed.col,
				  cancel : false
			  };
			  if (this.fireEvent("validateedit", e) !== false && !e.cancel && notEqualsV) {
				  r.set(field, e.value);
				  delete e.cancel;
				  this.fireEvent("afteredit", e);
			  }
		  }
		  // this.view.focusCell(ed.row, ed.col);
	  },
	  // processing dim compound value
	  postEditValue : function(value, startValue, r, field) {
		  if (field == 'value' && value) {
			  r.beginEdit()
			  if (value.type == 'app') {
				  r.set('materialId', value.id)
				  r.set('materialType', 'app')
				  r.set('appcall', Ext.encode(value.data))
				  r.set('value', '-')
			  } else if (value.type == 'txt') {
				  r.set('materialId', '')
				  r.set('materialType', '')
				  r.set('appcall', '')

				  var ret = this.isHTMLTagged(value.data);
				  var v = value.data
				  if (!ret) {
					  v = v ? v.replace(/<br\/?>/ig, '\n').replace(/&nbsp;/g, " ").replace(/&gt;/g, ">").replace(/&lt;/g, "<").replace(/&quot;/g, '"').replace(/&amp;/g, "&") : v;
				  } else if ((ret & 2) == 2) {
					  v = v.replace(/&lt;HTML&gt;/i, '<HTML>').replace(/&lt;\/HTML&gt;/i, '</HTML>').replace(/<a/g, '<a target="_blank"');
				  }
				  r.set('value', v)
			  } else {
				  r.set('value', value.title ? value.title : '-')
				  r.set('materialId', value.id)
				  r.set('materialType', value.type)
			  }
			  r.endEdit();
		  }
		  return obj.detail.AttrEditPanel.superclass.postEditValue.apply(this, arguments);
	  },
	  // private
	  preEditValue : function(r, field) {
		  var value = r.data[field];
		  if (field == 'value') {
			  var o = {}
			  if (r.get('materialId')) {
				  o.type = r.get('materialType')
				  o.id = r.get('materialId')
				  o.title = r.get('value')
				  if (r.get('appcall')) {
					  o.data = this.fillAppArg(r.get('materialId'), Ext.decode(r.get('appcall')))
				  }
			  } else {
				  if (!value)
					  value = ''
				  var ret = this.isHTMLTagged(value)
				  if (!ret) {// for htmleditor
					  value = Ext.util.Format.htmlEncode(value).replace(/\r\n|\r|\n/g, "<br>").replace(/ /g, "&nbsp;")
				  } else {
					  if ((ret & 1) == 1) {
						  value = value.replace(/<HTML>/i, '&lt;HTML&gt;').replace(/<\/HTML>/i, '&lt;/HTML&gt;')
					  }
					  // value = Ext.util.Format.htmlDecode(value)// txtarea-tive
				  }
				  o.type = 'txt'
				  value = value.replace(/\[\/?expans\]/gm, '');
				  o.data = value
			  }
			  return o;
		  }
		  return obj.detail.AttrEditPanel.superclass.preEditValue.apply(this, arguments);
	  },
	  fillAppArg : function(appid, appcall) {
		  if (!getTopWindow().__all_apps)
			  return null;
		  var a = getTopWindow().__all_apps[appid]
		  if (a) {
			  appcall = appcall || {}
			  a = Ext.apply({}, a)
			  var newargs = []
			  Ext.each(a.appArgs, function(arg) {
				    newargs.push(Ext.apply({
					      value : appcall[arg.name]
				      }, arg))
			    })
			  a.appArgs = newargs;
		  }
		  return a;
	  },
	  // utils
	  _batchAdd : function(n, includePres) {
		  var rs = [];
		  for (var i = (includePres ? this.getStore().getCount() : 0); i < n; i++) {
			  rs.push(this.createDimRecord({}));
		  }
		  this.getStore().beginEdit();
		  this.getStore().add(rs);
		  this.getStore().endEdit();
	  },
	  showAnsEditor : function() {
		  var r = this.getSelectionModel().getSelected();
		  if (r) {
			  var w = this.anseditorWin
			  if (w == null) {
				  w = this.anseditorWin = new AnsEditorWin();
				  w.on('materialSelected', function(obj) {
					    r = this.getSelectionModel().getSelected()
					    if (!r.get('value'))
						    r.set('value', '-')
					    r.set('materialId', obj.answer)
					    r.set('materialType', obj.type)
				    }.dg(this))
			  }
			  w.show()
		  }
		  return;
	  },
	  compileSemBlc : function(str) {
		  if (!str)
			  return {
				  rows : [],
				  allowBlank : {},
				  raw : str
			  }
		  // first OR wc combination
		  var inWC = false;
		  var lastBound = 0;
		  for (var i = 0; i < str.length; i++) {
			  var c = str.charAt(i);
			  if (c == '[') {
				  inWC = true;
			  } else if (c == ']') {
				  inWC = false;
			  } else if (c == '|' && !inWC) {
				  if (i > lastBound) {
					  str = str.substring(lastBound, i);
					  break;
				  }
			  }
		  }
		  var p = /\[([^\]]+)\]/gi
		  var result;
		  var ret = []
		  var allowBlank = {};
		  var lastIndex = 0;
		  while (result = p.exec(str)) {
			  var content = result[1];
			  if (result.index > lastIndex) {
				  ret.push([str.substring(lastIndex, result.index)]);
			  }
			  if (content.indexOf('?') != -1) {
				  content = content.replace(/\?/ig, '');
				  allowBlank[ret.length] = true;
			  }
			  ret.push(content.split('|'))
			  lastIndex = result.index + result[0].length
		  }
		  if (lastIndex < str.length) {
			  ret.push([str.substring(lastIndex, str.length)])
		  }
		  return {
			  rows : ret,
			  allowBlank : allowBlank,
			  raw : str
		  };
	  },
	  benchmark : function(phaseText, title) {
		  if (!window.console)
			  return;
		  if (!title)
			  title = ''
		  if (phaseText) {
			  console.log(title, phaseText, new Date().getTime() - this._lst)
		  } else if (this._bst) {
			  console.log(title, 'ALL', new Date().getTime() - this._bst)
			  delete this._bst, delete this._lst;
		  } else {
			  this._bst = new Date().getTime();
		  }
		  this._lst = new Date().getTime()
	  },
	  destroy : function() {
		  try {
			  if (this._valueEditor && this._valueEditor.field.toolbarWin) {
				  this._valueEditor.field.toolbarWin.destroy()
				  delete this._valueEditor.field.toolbarWin
				  this._valueEditor.destroy();
				  delete this._valueEditor
			  }
			  this.proxyStore.destroy();
			  delete this.proxyStore;
			  delete this._data
			  delete this._props
			  if (this.dimAttachment) {
				  for (var key in this.dimAttachment) {
					  if (this.dimAttachment[key])
						  this.dimAttachment[key].destroy();
				  }
				  delete this.dimAttachment
				  this.dimAttachment = {}
			  }
			  if (this.anseditorWin) {
				  this.anseditorWin.destroy();
				  delete this.anseditorWin
			  }
			  if (this.semanticEditor) {
				  this.semanticEditor.destroy();
				  delete this.semanticEditor
			  }
			  this.mainpanel ? delete mainpanel : 0
		  } catch (e) {
			  window.console ? console.log('attrEditP:destroy', e) : 0
		  }
		  return obj.detail.AttrEditPanel.superclass.destroy.call(this);
	  },
	  region : 'center',
	  border : false,
	  columnLines : true,
	  clicksToEdit : 1
  })
obj.detail.AttrEditPanel.MODE = {
	/**
	 * for object editing view
	 * 
	 * @type Number
	 */
	EMBED : 0,
	/**
	 * for single attribute detail editing view
	 * 
	 * @type Number
	 */
	DETAIL : 1,
	/**
	 * for category browsing view
	 * 
	 * @type Number
	 */
	LIST : 2
}
obj.detail.GroupSelectionModel = Ext.extend(Ext.grid.CheckboxSelectionModel, {
	  handleMouseDown : function(g, rowIndex, e) {
		  if (e.button !== 0 || this.isLocked()) {
			  return;
		  }
		  this.origSelect == null ? this.origSelect = e.shiftKey || e.ctrlKey : 0

		  var view = this.grid.getView();
		  if (e.shiftKey && !this.singleSelect && this.last !== false) {
			  var last = this.last;
			  // reset last
			  var rangeobj = this.getGroupRange(last)
			  if (rangeobj && rowIndex > last) {
				  last = rangeobj.range[0]
			  }
			  this.selectRange(last, rowIndex, e.ctrlKey);
			  this.last = last; // reset the last
			  view.focusRow(rowIndex);
		  } else {
			  var isSelected = this.isSelected(rowIndex);
			  if (e.ctrlKey && isSelected) {
				  this.deselectRow(rowIndex);
			  } else if (!isSelected || this.getCount() > 1) {
				  this.selectRow(rowIndex, e.ctrlKey || e.shiftKey);
				  view.focusRow(rowIndex);
			  }
		  }
		  delete this.origSelect
	  },
	  /**
			 * @return int[] boundary both inclusive
			 */
	  getGroupRange : function(rowIndex) {
	  },
	  isGroupSelected : function() {
		  return this.origSelect
	  },
	  selectAll : function() {
		  this.origSelect = true
		  obj.detail.GroupSelectionModel.superclass.selectAll.apply(this, arguments);
		  delete this.origSelect
	  },
	  selectRow : function(index, keepExisting, preventViewNotify) {
		  if (this.origSelect) {
			  obj.detail.GroupSelectionModel.superclass.selectRow.apply(this, arguments);
		  } else {
			  var rangeobj = this.getGroupRange(index);
			  if (rangeobj.ranged) {
				  // group-part
				  var range = rangeobj.range
				  var allSelected = true;
				  for (var i = range[0]; i < range[1] + 1; i++) {
					  if (!this.isSelected(i)) {
						  allSelected = false;
						  break;
					  }
				  }
				  if (!allSelected) {
					  this.origSelect = true;
					  if (range[0] == range[1])
						  obj.detail.GroupSelectionModel.superclass.selectRow.call(this, range[0], keepExisting, preventViewNotify);
					  else
						  this.selectRange(range[0], range[1], keepExisting);
					  delete this.origSelect
				  }
			  } else {// dim-part
				  this.origSelect = true;
				  obj.detail.GroupSelectionModel.superclass.selectRow.apply(this, arguments);
				  delete this.origSelect
			  }
		  }
	  },
	  deselectRow : function(index, preventViewNotify) {
		  if (this.origSelect) {
			  obj.detail.GroupSelectionModel.superclass.deselectRow.apply(this, arguments);
			  return
		  }
		  if (this.isLocked()) {
			  return;
		  }
		  if (this.last == index) {
			  this.last = false;
		  }
		  if (this.lastActive == index) {
			  this.lastActive = false;
		  }
		  var rangeobj = this.getGroupRange(index);;
		  if (rangeobj.ranged) {
			  var range = rangeobj.range
			  for (var i = range[0]; i <= range[1]; i++) {
				  var r = this.grid.store.getAt(i);
				  if (r)
					  this.selections.remove(r);
				  if (!preventViewNotify) {
					  this.grid.getView().onRowDeselect(i);
					  this.fireEvent('rowdeselect', this, i, r);
				  }
			  }
			  this.fireEvent('selectionchange', this);
		  } else {
			  var r = this.grid.store.getAt(index);
			  if (r)
				  this.selections.remove(r);
		  }
	  }
  })
obj.detail.LinkAttrSelectWin = Ext.extend(Ext.Window, {
	  initComponent : function() {
		  obj.detail.LinkAttrSelectWin.superclass.initComponent.call(this);
	  },
	  loadItems : function(itemsData) {
		  this.removeAll();
		  Ext.each(itemsData, function(r) {
			    this.add(new Ext.form.Radio({
				      inputValue : r.get('valueId'),
				      fieldLabel : r.get('question'),
				      name : 'selectValue',
				      handler : function(btn) {
					      this.fireEvent('linkParentSelect', btn.inputValue, itemsData)
				      }.dg(this)
			      }))
		    }, this)
		  this.doLayout()
	  },
	  title : '请选择需要被关联的主属性',
	  autoHeight : true,
	  autoWidth : true,
	  layout : 'form',
	  closeAction : 'hide',
	  listeners : {
		  beforehide : function() {
			  // this.removeAll();
		  }
	  }
  });
var KBSemanticEditor = Ext.extend(Ext.menu.Menu, {
	style : {
		overflow : 'visible', // For the Combo popup
		'background-image' : 'none'
	},
	initComponent : function() {
		KBSemanticEditor.superclass.initComponent.call(this)
	},
	layout : 'table',
	defaults : {
		margins : '0 2 0 0'
	},
	// width : 1000,
	loadData : function(data, cb) {
		this.cb = cb
		var ctxvars = data.contextVar || {}
		var rule = data.rule
		rule = rule.replace(/\<([^|>?]+)[^>]*>/g, '$1')
		var p = /\[([^\[]+)\]|XXX|YYY/g
		var lasti = 0;
		var items = []
		var result;
		while (result = p.exec(rule)) {
			var XXXYYY = result[0].indexOf('[') == -1
			if (XXXYYY
			  || 'XXX,YYY,concept,concept1,concept2,concept3,attribute,attribute1,attribute2,attribute3,adjective,action,action1,action2,action3,feename,neg-action,!action,subject,object,unit,agent,patient,duration,number,datename,place,titlename'
			    .split(',').indexOf(result[1]) != -1) {
				if (lasti < result.index) {
					items.push({
						  xtype : 'label',
						  html : '&nbsp;' + rule.substring(lasti, result.index) + '&nbsp;'
					  })
				}
				items.push({
					  xtype : 'textfield',
					  width : 150,
					  value : ctxvars[XXXYYY ? result[0] : result[1]],
					  name : XXXYYY ? result[0] : result[1],
					  emptyText : XXXYYY ? result[0] : result[1],
					  listeners : {
						  afterrender : function() {
							  var me = this;
							  me.el.swallowEvent(['keypress', 'keydown']);
						  }
					  }
				  })
				lasti = result[0].length + result.index
			}
		}
		if (lasti < rule.length) {
			items.push({
				  xtype : 'label',
				  html : '&nbsp;' + rule.substring(lasti) + '&nbsp;'
			  })
		}

		this.removeAll();
		var buttons = {
			OK : {
				xtype : 'button',
				iconCls : 'icon-ok',
				tooltip : '确定修改',
				colspan : 2,
				width : 25,
				handler : function() {
					var contextVar = {}
					this.items.each(function(item) {
						  if (item.name) {
							  contextVar[item.name] = item.getValue();
						  }
					  }, this)
					if (this.cb(contextVar) !== false) {
						this.hide()
					}
				}.dg(this)
			}
		}
		this.layoutConfig = {
			padding : 1,
			columns : items.length + 2
		}
		this.add(buttons.OK);
		Ext.each(items, function(item) {
			  this.add(item)
		  }, this)
		this.doLayout();
	}
})