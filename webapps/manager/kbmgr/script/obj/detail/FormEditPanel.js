Ext.namespace('obj.detail')
/**
 * Form holding object's attribute except for VALUES
 * 
 * @class obj.detail.FormEditPanel
 * @extends Ext.form.FormPanel
 */
obj.detail.FormEditPanel = Ext.extend(Ext.form.FormPanel, {
	  initComponent : function() {
		  this.addEvents(
		    /**
						 * @event ontologyClassesChange Fires when classes of object edit form
						 *        changed.
						 * @param {obj.detail.FormEditPanel}
						 *         panel
						 * @param {Array}
						 *         ontology-class id array
						 */
		    'ontologyClassesChange')
		  this._buildItems();
		  obj.detail.FormEditPanel.superclass.initComponent.call(this);
	  },
	  loadData : function(object) {
		  var dataRecord = new obj.detail.ObjectType(object, object.id);
		  this.getForm().loadRecord(dataRecord);
		  this._encvalue = Ext.encode(this.getForm().getFieldValues())
	  },
	  resetModifiedState : function() {
		  delete this._encvalue
	  },
	  isModified : function() {
		  return this._encvalue && Ext.encode(this.getForm().getFieldValues()) != this._encvalue
	  },
	  _buildItems : function() {
		  var self = this;
		  var fmEdP = this;
		  var classSelected = new TreeComboCheck({
			    ref : 'classesField',
			    dataUrl : 'ontology-class!list.action?listClassOnly=true&classtype=0',
			    name : 'classes',
			    anchor : '100%',
			    fieldLabel : '本体分类',
			    listWidth : 250,
			    treeConfig : {
				    tbar : [{
					      text : '清空',
					      iconCls : 'icon-refresh',
					      handler : function() {
						      classSelected.setValueEx();
					      }
				      }, {
					      text : '刷新',
					      iconCls : 'icon-refresh',
					      handler : function() {
						      classSelected.reload();
					      }
				      }]
			    },
			    onValueChange : function(values, v, checked) {
				    var ids = [];
				    Ext.each(values, function(cls) {
					      ids.push(cls.id);
				      })
				    if (v)
					    fmEdP.fireEvent('ontologyClassesChange', fmEdP, ids, v.id, checked, values);
				    else
					    fmEdP.fireEvent('ontologyClassesChange', fmEdP, ids, null, null, values);
			    }
		    });
		  this.items = [{
			    xtype : 'panel',
			    border : false,
			    bodyStyle : 'background-color:' + sys_bgcolor,
			    layout : 'column',
			    defaults : {
				    bodyStyle : 'background-color:' + sys_bgcolor,
				    border : false,
				    labelWidth : 55
			    },
			    items : [{
				      layout : 'form',
				      width : 200,
				      items : {
					      name : 'name',
					      fieldLabel : '实例名称',
					      xtype : 'textfield',
					      anchor : '100%',
					      allowBlank : false,
					      blankText : '名称是必填项，请填写后再保存'
				      },
				      style : 'margin-right:5px'
			      }, {
				      layout : 'form',
				      anchor : '100%',
				      items : classSelected,
				      style : 'margin-right:5px'
			      }, {
				      layout : 'form',
				      width : 200,
				      items : {
					      name : 'semanticBlock',
					      fieldLabel : 'XXX模板',
					      xtype : 'textfield'
				      },
				      style : 'margin-right:5px'
			      }, {
				      layout : 'form',
				      width : 200,
				      items : {
					      name : 'orderNo',
					      fieldLabel : '实例顺序',
					      emptyText : '请填数字',
					      width : 50,
					      xtype : 'textfield'
				      }
			      }, {
				      xtype : 'textfield',
				      name : 'tag',
				      ref : '../tag'
			      }, {
				      xtype : 'textfield',
				      name : 'bizTplId',
				      hidden : true
			      }]
		    }, {
			    name : 'bh',
			    hidden : true
		    }, {
			    name : 'categoryId',
			    hidden : true
		    }, {
			    name : 'objectId',
			    hidden : true
		    }, {
			    name : 'dimTagIds',
			    hidden : true
		    }, {
			    name : 'editComment',
			    xtype : 'textarea',
			    emptyText : '请填写备注'
		    }]
	  },
	  region : 'north',
	  split : true,
	  collapseMode : 'mini',
	  padding : '8px 8px',
	  height : 40,
	  minSize : 50,
	  maxSize : 200,
	  margins : '0 0 0 0',
	  border : false,
	  style : 'border-bottom-width:1px',
	  bodyStyle : 'background-color:' + sys_bgcolor,
	  labelWidth : 55,
	  defaults : {
		  xtype : 'textfield'
	  }
  })