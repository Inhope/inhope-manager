/**
 * @include "../../ux.js";
 * @include "FormEditPanel.js"
 * @include "AttrEditPanel.js"
 * @include "AttrDetailEditWindow.js"
 */
Ext.namespace('obj.detail')
obj.detail.ObjectType = Ext.data.Record.create(['objectId', 'categoryId', 'semanticBlock', 'classes', 'name', 'bh', 'pathString', 'status', 'reverseAsk', 'bizTplId']);

var isKbase = http_get('property!get.action?key=sys.kbase');
/**
 * @class obj.detail.MainPanel
 * @extends Ext.Panel
 */
obj.detail.MainPanel = Ext.extend(Ext.Panel, {
	// callback
	/**
	 * 
	 * @param {obj.detail.ObjectType}
	 *         obj
	 */
	objectChanged : function(obj, cb, toDimValueId, ispub, options) {
		if (this.lockTimeout) {
			Dashboard.setAlert('当前实例编辑锁定超时，如需加载最新数据请关闭后重新打开', 'error');
			return
		}
		this.editObject(obj.data, cb, toDimValueId, ispub, options)
	},
	categoryChanged : function(n) {
	},

	// view >>>
	/**
	 * @type {obj.detail.AttrEditPanel}
	 * @private
	 */
	_attrP : null,
	/**
	 * @type {obj.detail.FormEditPanel}
	 * @private
	 */
	_fmP : null,
	/**
	 * @return {obj.detail.FormEditPanel}
	 */
	getFormEditPanel : function() {
		return this._fmP;
	},
	/**
	 * @return {obj.detail.AttrEditPanel}
	 */
	getAttrEditPanel : function() {
		return this._attrP;
	},
	// overrides
	disable : function() {
		obj.detail.MainPanel.superclass.disable.apply(this, arguments);
		this.reset();
	},
	// view <<<
	// model >>>
	getPostData : function(validate) {
		var dataObject = this._fmP.getForm().getFieldValues();
		if (this._RAMgrWin) {
			dataObject.attachments = this._RAMgrWin.getAttachments()
		}
		if (_vcolumncfg) {
			var items = this.getTopToolbar().find('vcolumn', 1)
			var resetCol = {}
			Ext.each(items, function(item) {
				  if (item.c.vtype == 0) {
					  if (!resetCol[item.c.vcolumnName]) {
						  resetCol[item.c.vcolumnName] = true;
						  dataObject[item.c.vcolumnName] = 0;
					  }
					  if (item.getValue()) {
						  dataObject[item.c.vcolumnName] |= item.c.value
					  }
				  }
			  }, this)
		}
		if (this.objectInEditing) {
			// 附件
			if ((this.objectInEditing.status & 256) == 256 && (!this._RAMgrWin || this._RAMgrWin.hasAttachments()))
				dataObject.status |= 256;
			if ((this.objectInEditing.status & 128) == 128)
				dataObject.status |= 128;
			// 实例变量
			dataObject.variables = this.objectInEditing.variables
		}
		// var orfields = this.getTopToolbar().saveBtn.menu.find('objrelation', 1)
		// var orvalues = []
		// Ext.each(orfields, function(orfield) {
		// var objnames = orfield.getValue().split('\n')
		// Ext.each(objnames, function(n) {
		// if (n && n.trim) {
		// orvalues.push({
		// name : n.trim(),
		// type : orfield.or.type
		// })
		// }
		// }, this)
		// }, this)
		// if (orvalues.length)
		// dataObject.relations = orvalues;

		dataObject.attributes = [];
		var attrs = this._attrP.getAttrsData()
		Ext.each(attrs, function(d) {
			  dataObject.attributes.push(d);
		  }, this);
		dataObject.dimIdDeleted = attrs.dimIdDeleted;
		return dataObject;
	},
	/**
	 * if object modified in detail panel
	 * 
	 * @return {Boolean}
	 */
	isModified : function() {
		var modified = false;
		this._attrP.getStore().each(function(r) {
			  for (var key in r.getChanges()) {
				  modified = true;
				  return false;
			  }
		  });
		return modified;
	},

	objectInEditing : null,
	/**
	 * reset panels in detail panel(FormEditPanel & AttrEditPanel)
	 */
	reset : function() {
		this._fmP.getForm().reset();
		this._attrP.reset();
	},
	_resetModifiedState : function() {
		this._attrP.resetModifiedState();
		this._fmP.resetModifiedState();
	},
	_confirmModified : function(cb) {
		this._attrP.stopEditing();
		if (this._attrP.isModified() || this._fmP.isModified()) {
			Ext.Msg.show({
				  title : '提示',
				  msg : '数据未保存，是否要保存?',
				  buttons : Ext.Msg.YESNOCANCEL,
				  fn : function(result) {
					  if (result == 'yes') {
						  this._saveObject(function() {
							    cb()
						    }.dg(this));
					  } else if (result == 'no') {
						  this._resetModifiedState();
						  cb()
					  }
				  },
				  scope : this,
				  icon : Ext.Msg.QUESTION,
				  minWidth : Ext.Msg.minWidth
			  });
		} else {
			cb()
		}
	},
	_loadObject : function(object, toDimValueId) {// fill local edit panel
		this.reset();

		if (object) {
			this.objectInEditing = object;
			var status = object.status;
			if (_vcolumncfg && status) {
				var items = this.getTopToolbar().find('vcolumn', 1)
				Ext.each(items, function(item) {
					  if (item.c.vtype == 0) {
						  item.setValue(item.c.value == (item.c.value & status))
					  }
				  }, this)
			}
			if (this.getTopToolbar().attachmentBtn) {
				if ((status & 256) == 256) {
					this.getTopToolbar().attachmentBtn.getEl().setStyle('background', '#5a5')
				} else {
					this.getTopToolbar().attachmentBtn.getEl().setStyle('background', 'inherit')
				}
			}

			// var orfields = this.getTopToolbar().saveBtn.menu.find('objrelation', 1)
			// var orvalues = []
			// Ext.each(orfields, function(orfield) {
			// orfield.setValue('');
			// }, this)
			// Ext.each(orfields, function(orfield) {
			// Ext.each(object.relations, function(r) {
			// if (r.type == orfield.or.type) {
			// orfield.setValue(((orfield.getValue() ? orfield.getValue() + '\n' : '') +
			// r.name).trim())
			// }
			// }, this)
			// }, this)

			this._fmP.loadData(object);
			this._attrP.loadAttrsData(object.attributes, object.dimTagIds, toDimValueId, object);
			this._attrP.ispub = this._editObjectParam[3]
			this.pathText.setText('所属分类:' + object.catePath)

			if (this._RAMgrWin) {
				this._RAMgrWin.load(this.objectInEditing ? this.objectInEditing.objectId : null, true)
			}
			if (!this._syncInit) {
				this._syncInit = true
				// reset sync option of save button
				var syncOption = '';
				if (isKbase)
					syncOption = this.getTopToolbar().sync;
				else
					syncOption = this.getTopToolbar().saveBtn.sync;
				syncOption.checked = (Dashboard.getModuleProp('kbmgr.enableAudit') ? false : true);
				var authName = 'kb.obj.instance.S'
				var lastIndex = authName.lastIndexOf('.');
				var perm = authName;
				if (lastIndex != -1) {
					perm = authName.substring(lastIndex + 1);
				}
				if (lastIndex != -1 && Dashboard.u().allow(authName) || Dashboard.u().allow('kb.obj.catecontent.' + object.cateIdPath.substring(1).replace(/\//g, '.') + '.' + perm)) {
					;
				} else {
					syncOption.checked = false;
					syncOption.disable()
				}
			}
		} else {
			throw new Error('fail to load object instance');
		}

		this.viewSwitcher.toggle(false);
	},
	/**
	 * Add(no obj parameter) or Edit object instance
	 * 
	 * @param {Object}
	 *         ontology-object
	 */
	editObject : function() {// load remote
		this._confirmModified(this._editObject0.dg(this, arguments))
	},
	getObjEditPrivilege : function() {
		var p = this._editObjectParam[4] && this._editObjectParam[4].privilege ? this._editObjectParam[4].privilege : {};
		var arr = []
		for (var key in p) {
			if (p[key]) {
				arr[key] = true
				arr.push(key)
			}
		}
		return arr
	},
	_editObject0 : function(obj, cb, toDimValueId, ispub, options) {// load remote
		_reload_apps()
		var as = []
		Ext.each(arguments, function(a) {
			  as.push(a)
		  })
		this._editObjectParam = as
		if (!this.getObjEditPrivilege().M && 'true' != Dashboard.getModuleProp('ibot.kb.obj.saveobjonly') && !this._editObjectParam[3]) {
			this.getTopToolbar().saveBtn.disableByTarget(1)
		}
		if (this.el)
			this.el.mask('加载中...')
		Ext.Ajax.request({
			  url : 'ontology-object!edit.action',
			  params : {
				  data : Ext.encode(ux.util.resetEmptyString(obj)),
				  ispub : ispub,
				  classId : 'cb59dfbd192c4c389b7c31ad3cbbeaeb'
			  },
			  success : function(response) {
				  var result = Ext.util.JSON.decode(response.responseText);
				  if (!result.success) {
					  Ext.Msg.alert("提示", result.message);
					  return;
				  }
				  if (cb)
					  cb();
				  this._loadObject(result.data, toDimValueId);
				  this.lockEditingObject()
			  },
			  failure : function(response) {
				  if (this.el)
					  this.el.unmask()
				  Ext.Msg.alert("错误", response.responseText);
				  this.close()
			  },
			  scope : this
		  });
	},
	lockEditingEnabled : function() {
		return Dashboard.getModuleProp('kbmgr.obj_edit_lock_duration')
	},
	clearLockTimer : function() {
		if (this.locktimer) {
			clearInterval(this.locktimer), delete this.locktimer
			this.edlockInfo.setText('', false);
		}
	},
	resetLockState : function(locked) {
		this.lockTimeout = !locked
		this.edlockInfo.setText('')
		this.edlockInfo.setIcon(locked ? 'images/lock.png' : 'images/lock_open.png')
		this.getTopToolbar().saveBtn[locked ? 'enableByTarget' : 'disableByTarget'](2)
		this.getTopToolbar().refreshBtn[locked ? 'enable' : 'disable']()

		if (this._attrDtEdWin) {
			for (var key in this._attrDtEdWin) {
				if (this._attrDtEdWin[key].isVisible())
					this._attrDtEdWin[key].getTopToolbar().saveBtn[!this.lockTimeout ? 'enableByTarget' : 'disableByTarget'](2)
			}
		}
	},
	unlockEditingObject : function() {
		if (!this.lockEditingEnabled())
			return
		var objectid = this._editObjectParam[0].objectId
		var ispub = this._editObjectParam[3]
		if (!objectid)
			return;
		this.clearLockTimer()
		Ext.Ajax.request({
			  url : 'ontology-object!unlockEditingObject.action',
			  params : {
				  objectId : objectid,
				  ispub : ispub
			  },
			  success : function(response) {
			  },
			  failure : function(response) {
				  Ext.Msg.alert("错误", '实例"' + (this.objectInEditing && this.objectInEditing.name ? this.objectInEditing.name : '') + '"编辑解除锁定失败');
			  },
			  scope : this
		  });
	},
	tryResetEditingTime : function() {
		if (!this.lockEditingEnabled())
			return
		if (this.lockduration < 120 && (!this.lastmovetime || new Date().getTime() - this.lastmovetime > 15000)) {
			this.lockEditingObject()
			this.lastmovetime = new Date().getTime()
		}
	},
	lockEditingObject : function(ts) {
		if (!this.lockEditingEnabled())
			return
		if (this.lockTimeout && !ts)
			return
		var objectid = this._editObjectParam[0].objectId
		var ispub = this._editObjectParam[3]
		if (!objectid)
			return;
		this.clearLockTimer()
		Ext.Ajax.request({
			  url : 'ontology-object!lockEditingObject.action',
			  params : {
				  objectId : objectid,
				  ispub : ispub,
				  ts : ts ? ts : ''
			  },
			  success : function(response) {
				  var objname = this.objectInEditing.name
				  this.edlockInfo.show()
				  var result = response.responseText;// [ts,duration]
				  if (!result || result.split(',').length != 2) {
					  var errmsg = '实例"' + objname + '"编辑锁定失败，请重新打开实例';
					  if (this.el)
						  this.el.mask(errmsg)
					  if (ts)
						  Ext.Msg.alert('提示', errmsg)
					  return;
				  } else {
					  if (ts)
						  Dashboard.setAlert('实例"' + objname + '"编辑锁定成功');
					  this.resetLockState(true)
					  result = result.split(',')
					  this.lockts = result[0]
					  this.lockduration = result[1]
					  var interval = 5000
					  var timealert = false;
					  var fn = function() {
						  this.lockduration -= interval / 1000
						  this.edlockInfo.setText('编辑锁定剩余时间:' + parseInt(this.lockduration / 60) + '分钟' + this.lockduration % 60 + '秒');
						  if (this.lockduration <= 0) {
							  Ext.Msg.confirm('提示', '实例"' + objname + '"编辑锁定超时，点"是"立即尝试继续编辑；您也可以点击右下角锁定超时信息按钮继续编辑。', function(result) {
								    if (result == 'yes') {
									    this.lockEditingObject(this.lockts)
								    }
							    }, this)
							  this.lockduration = 0
							  clearInterval(this.locktimer)
							  delete this.locktimer
							  this.resetLockState(false)
							  this.edlockInfo.setText('锁定超时时间:' + Ext.util.Format.date(new Date(), "Y-m-d H:i:s") + "，点击尝试重新锁定编辑");
						  } else {
							  if (this.lockduration <= 60 && !timealert) {
								  Dashboard.setAlert('实例"' + objname + '"编辑锁定剩余1分钟，请尽快保存', 'error');
								  timealert = true
							  }
						  }
					  }.dg(this);
					  interval = 0
					  fn()
					  interval = 5000
					  this.locktimer = setInterval(fn, interval);
				  }
			  },
			  failure : function(response) {
				  if (this.el)
					  this.el.unmask()
				  Ext.Msg.alert("错误", response.responseText);
			  },
			  scope : this
		  });
	},
	_addAttr : function(attr) {
		var r;
		var attrStore = this._attrP.getStore();
		if (attr) {
			r = this._attrP.createDimRecord({
				  vid : Ext.id(),
				  attributeName : attr.name,
				  attributeId : attr.attributeId,
				  attributeClassId : attr.ontologyClass.classId,
				  attributeClassBH : attr.ontologyClass.bh
			  });
		} else {
			r = this._attrP.createDimRecord({
				  vid : Ext.id(),
				  attributeName : '新建属性'
			  });
		}
		attrStore.insert(0, r);
	},
	// model <<<
	// layout
	initComponent : function() {
		this._fmP = new obj.detail.FormEditPanel({});
		this._attrP = new obj.detail.AttrEditPanel({
			  mainpanel : this
		  });
		this._attrP.getObjectData = function(excludeAttrs) {
			return this._fmP.getForm().getFieldValues();
		}.dg(this);
		this.items = [this._attrP];
		// object edit tbars
		this.tbar = {
			enableOverflow : true,
			items : this._buildTBar()
		};
		this.statusText = new Ext.form.Label({
			  text : '暂无'
		  });
		this.pathText = new Ext.form.Label({
			  text : '暂无'
		  });
		this.edlockInfo = new Ext.create({
			  text : '',
			  xtype : 'button',
			  hidden : true,
			  style : {
				  color : 'red'
			  },
			  text : 'hello',
			  hidden : !this.lockEditingEnabled(),
			  handler : function() {
				  this.lockEditingObject(this.lockts)
			  }.dg(this),
			  icon : 'images/lock.png'
		  });
		this.bbar = this.bbar || [this.statusText, '->', this.pathText, '-', this.edlockInfo]
		// bind events
		this._bindEvents();
		obj.detail.MainPanel.superclass.initComponent.call(this);
		this._attrP.getView().on('refresh', function() {
			  this.el.unmask()
		  }, this)
		this._attrP.on('cellclick', this.tryResetEditingTime, this)
	},
	_buildTbarEx : function() {
		var self = this;
		return new Ext.Toolbar({
			  items : [{
				    xtype : 'label',
				    html : '&nbsp;维度编辑:'
			    }, new Ext.form.ComboBox({
				      anchor : '98%',
				      width : 85,
				      triggerAction : 'all',
				      editable : false,
				      mode : 'local',
				      value : 'default',
				      store : new Ext.data.ArrayStore({
					        fields : ['name', 'value'],
					        data : [['列表方式', 'default'], ['文本方式', 'editAble'], ['勾选方式', 'checkAble']]
				        }),
				      valueField : 'value',
				      displayField : 'name',
				      listeners : {
					      select : function(own, record, idx) {
						      if (1 == idx) {
							      self._attrP.dimFieldEditable = true;
							      self._attrP.dimFieldCheckable = false;
							      return true;
						      } else if (2 == idx) {
							      // Ext.util.Cookies.clear('kb.detail.dimedit');
							      self._attrP.dimFieldEditable = false;
							      self._attrP.dimFieldCheckable = true;
							      return true;
						      } else if (0 == idx) {
							      // Ext.util.Cookies.clear('kb.detail.dimedit');
							      self._attrP.dimFieldEditable = false;
							      self._attrP.dimFieldCheckable = false;
						      }
					      }.dg(this)
				      },
				      scope : self
			      })]
		  });
	},
	_buildTBar : function() {
		var semanticBlock = this._fmP.find('name', 'semanticBlock')[0];
		var nameField = this._fmP.find('name', 'name')[0];
		nameField.on('blur', function(f) {
			  this._attrP.objectFieldChange('name', f.getValue())
		  }, this)
		var formItems = [{
			  xtype : 'label',
			  html : '&nbsp;名称:'
		  }, this._fmP.find('name', 'name')[0], {
			  xtype : 'label',
			  html : '&nbsp;继承类:'
		  }, this._fmP.find('name', 'classes')[0], {
			  xtype : 'label',
			  html : '&nbsp;语义块:'
		  }, semanticBlock];
		Ext.each(formItems, function(f) {
			  if (f.xtype == 'label')
				  return;
			  if (f.name == 'classes')
				  f.width = 120
			  else if (f.name == 'semanticBlock')
				  f.width = 120
			  else
				  f.width = 80
		  })
		var searchFields = this._attrP.getCompactSearchFields();
		Ext.each(searchFields, function(f) {
			  if (f.name)
				  f.width = 90
		  })
		var tagField = this._fmP.find('name', 'tag')[0];
		var osetv = tagField.setValue;
		// for init
		tagField.setValue = function(v) {
			this.getTopToolbar().tagBtn.setTooltip(v);
			osetv.call(tagField, v);
		}.dg(this)
		// add virtual column
		if (_vcolumncfg) {
			formItems.push('-')
			Ext.each(_vcolumncfg.columns, function(c) {
				  if (c.type == 'o') {
					  if (c.vtype == 0) {
						  formItems.push(c.name, {
							    xtype : 'checkbox',
							    vcolumn : 1,
							    c : c
						    })
					  }
				  }
			  }, this)
			formItems.push('-')
		}
		var attachmentByRobotSearch = http_get('property!get.action?key=attachment.addr');
		var saveBtn;
		if (Dashboard.getModuleProp('kbmgr.enable_obj_orderno') == 'true') {
			formItems.push({
				  xtype : 'label',
				  html : '&nbsp;实例顺序:'
			  }, this._fmP.find('name', 'orderNo')[0])
		}
		if (Dashboard.u().isSupervisor())
			formItems.push({
				  text : '变量',
				  handler : function() {
					  if (!this.varedwin)
						  this.varedwin = new VariableEditWin();
					  !this.objectInEditing.variables ? this.objectInEditing.variables = [] : 0
					  this.varedwin.loadData(this.objectInEditing.variables)
					  this.varedwin.show()
				  }.dg(this),
				  width : 40
			  });
		formItems.push(((Dashboard.isKBase() && Dashboard.isKBaseTag()) ? [{
			    text : '标签',
			    iconCls : 'icon-star',
			    ref : 'tagBtn',
			    handler : function() {
				    var cfg = {};
				    cfg.title = "属性标签";
				    if (!this.tagedwin)
					    this.tagedwin = new TagEditWin(cfg);
				    this.tagedwin.setData(tagField.getValue())
				    this.tagedwin.callFunction = function(tagIds) {
					    tagField.setValue(tagIds)
				    };
				    this.tagedwin.show()
			    }.dg(this),
			    width : 40
		    }] : {
			  text : '标签',
			  ref : 'tagBtn',
			  menu : {
				  listeners : {
					  hide : function() {
						  this.getTopToolbar().tagBtn.setTooltip(tagField.getValue())
					  }.dg(this)
				  },
				  items : [tagField]
			  },
			  width : 40
		  }), '-', Dashboard.isKBase() ? [{
			    text : '实例关联',
			    ref : 'tagBtn',
			    width : 40,
			    handler : function() {
				    if (!this._RAMgrCaseRelationWin) {
					    this._RAMgrCaseRelationWin = new RAMgrCaseRelationWin();
					    if (this.objectInEditing.relations) {
						    var gridContrastStore = this._RAMgrCaseRelationWin.gridContrast.getStore();
						    var gridRelevanceStore = this._RAMgrCaseRelationWin.gridRelevance.getStore();
						    Ext.each(this.objectInEditing.relations, function(item) {
							      if (item.type == '1') {
								      gridContrastStore.add(new Ext.data.Record({
									        id : item.id,
									        name : item.name,
									        type : item.type
								        }));
							      } else if (item.type == '2') {
								      gridRelevanceStore.add(new Ext.data.Record({
									        id : item.id,
									        name : item.name,
									        type : item.type
								        }));
							      }
						      });
					    }
				    }
				    // this._RAMgrCaseRelationWin.load("112233");
				    // this._RAMgrCaseRelationWin.load(this.objectInEditing.relations);
				    this._RAMgrCaseRelationWin.show();
			    }.dg(this),
			    scope : this
		    }, {
			    xtype : 'checkbox',
			    boxLabel : '同步',
			    labelStyle : 'vertical-align: inherit;top: -1px',
			    checked : Dashboard.getModuleProp('kbmgr.enableAudit') ? false : true,
			    name : 'sync',
			    ref : 'sync'
		    }, {
			    xtype : 'checkbox',
			    boxLabel : '版本',
			    checked : false,
			    name : 'version',
			    ref : 'version'
		    }, {
			    xtype : 'button',
			    text : '保存',
			    ref : 'saveBtn',
			    iconCls : 'icon-table-save',
			    handler : function() {
				    if (isKbase && this.getTopToolbar().version.checked) {
					    Ext.MessageBox.prompt("提示", "你勾选了版本，请填写版本备注信息方可保存", function(e, text) {
						      if ('ok' == e) {
							      if (!text) {
								      Ext.Msg.alert("提示", "保存无效，勾选版本的备注信息不能为空！");
								      return;
							      }
							      this._saveObject(function() {
							        }, null, text);
						      } else
							      return false;
					      }, this, true);
				    } else {
					    this._saveObject();
				    }
			    },
			    scope : this
		    }] : saveBtn = {
			  xtype : 'splitbutton',
			  text : '保存',
			  ref : 'saveBtn',
			  menu : {
				  plain : true,
				  items : [{
					    text : '同步',
					    checked : Dashboard.getModuleProp('kbmgr.enableAudit') ? false : true,
					    name : 'sync',
					    ref : '../sync'
				    }, this._fmP.find('name', 'editComment')[0]],
				  listeners : {
					  render : function() {
						  this.keyNav.disable()
					  }
				  }
			  },
			  iconCls : 'icon-table-save',
			  width : 50,
			  handler : function() {
				  this.clearLockTimer()
				  this._saveObject()
			  }.dg(this)
		  }, attachmentByRobotSearch ? {
			  ref : 'attachmentBtn',
			  tooltip : '添加附件',
			  iconCls : 'icon-attachment',
			  handler : function() {
				  if (!this._RAMgrWin) {
					  this._RAMgrWin = new RAMgrWin({
						    topNodeId : '1'
					    })
					  this._RAMgrWin.load(this.objectInEditing ? this.objectInEditing.objectId : null)
				  }
				  this._RAMgrWin.show()
			  }.dg(this)
		  } : '');
		// if (_objrelations) {
		// // [name,type]
		// Ext.each(_objrelations, function(or) {
		// var c = {
		// xtype : 'textarea',
		// objrelation : 1,
		// or : or
		// }
		// saveBtn.menu.items.push(or.name)
		// saveBtn.menu.items.push(c)
		// }, this)
		// }
		formItems.push('-')
		this.viewSwitcher = new Ext.Button({
			  enableToggle : true,
			  tooltip : '切换紧凑视图',
			  iconCls : 'icon-view-switcher',
			  toggleHandler : function(btn, press) {
				  this._attrP.switchView(press)
			  },
			  scope : this
		  })
		this.on('render', function() {
			var field = this.getTopToolbar().find('name', 'typeContent')[0]
			var searchBtn = this.getTopToolbar().find('iconCls', 'icon-search')[0]
			field.on('specialkey', function(f, e) {
				  if (e.getKey() == e.ENTER) {
					  searchBtn.handler();
				  }
			  });
			 // if (!Dashboard.u().allow('kb.obj.catecontent.'
			 // + this.navPanel.getAuthPath() + '.S')) {
			 // this.getTopToolbar().saveBtn.sync.checked = false
			 // this.getTopToolbar().saveBtn.sync.disable()
			 // }
		 }, this)
		return formItems.concat(searchFields).concat('-', this.viewSwitcher, {
			  xtype : 'button',
			  tooltip : '替换...',
			  iconCls : 'icon-text-replace',
			  handler : function() {
				  var win = new ReplaceWindow({
					    grid : this.getAttrEditPanel()
				    });
				  win.show();
			  },
			  scope : this
		  }, (isVersionStandard() ? '' : {
			  tooltip : '显示类继承关系',
			  iconCls : 'icon-ontology-cate',
			  handler : function() {
				  var value = this.getTopToolbar().find('name', 'classes')[0].getValue()
				  if (value) {
					  var objectParentIds = []
					  Ext.each(value, function(v) {
						    var part = v.path.replace(/^\/|\/$/, '').split('/');
						    objectParentIds.push(v.id)
					    });
					  Ext.Ajax.request({
						    url : 'ontology-class!findAttrs.action?data=' + objectParentIds.join(','),
						    scope : this,
						    success : function(response) {
							    if (!response || !response.responseText)
								    return;
							    var drawCfg = {
								    'picType' : 'clz',
								    data : []
							    }
							    var data = Ext.decode(response.responseText).data;
							    Ext.each(data, function(d) {
								      d.text = d.name, delete d.name
								      drawCfg.data.push(d)
							      }, this)
							    var objectname = this.objectInEditing.name
							    drawCfg.data.push({
								      id : encodeURIComponent(objectname),
								      text : objectname,
								      lineType : '2',
								      parentId : objectParentIds
							      })
							    if (!this._canvas) {
								    this._canvas = new DrawDiagramWin({
									      closeAction : 'close',
									      listeners : {
										      close : function() {
											      delete this._canvas
										      }.dg(this)
									      }
								      })
							    }
							    this._canvas.show();
							    this._canvas.draw(drawCfg)
						    }
					    });
				  }
			  },
			  scope : this
		  }), {
			  tooltip : '刷新',
			  iconCls : 'icon-refresh',
			  ref : 'refreshBtn',
			  handler : function() {
				  this.editObject.apply(this, this._editObjectParam);
			  },
			  scope : this
		  });
		/*
		 * , '-', new ClassAttributeChooserBtn({ ref : '../classAttrChooserBtn', text :
		 * '新增属性', iconCls : 'icon-table-add', width : 50, handler : function() { //
		 * here,sucks:reverse referrence,suggestion?!
		 * this._addAttr(this.classAttrChooserBtn .getSelectedAttribute()) }, scope :
		 * this, listeners : { addAttribute : this._addAttr.dg(this) } } , { ref :
		 * '../deleteBtn', disabled : true, text : '删除属性', iconCls :
		 * 'icon-table-delete', width : 50, handler : this._deleteAttr.dg(this) }
		 */
	},
	_bindEvents : function() {
		// edit attribute detail
		this._attrP.on('statusInfoChange', function(t) {
			  this.statusText.setText(t)
		  }, this)
		this._attrP.on('editAttributeDetail', function(attrEditP, rs, triggerByUser, oldvid, btn, fromWin) {
			  !this._attrDtEdWin ? this._attrDtEdWin = {} : 0;
			  var vid = rs[0].get('vid');
			  var win = this._attrDtEdWin[vid]
			  var oldwin = win
			  if (oldvid && oldvid != vid) {
				  win = this._attrDtEdWin[oldvid], delete this._attrDtEdWin[oldvid]
				  win ? this._attrDtEdWin[vid] = win : 0
			  } else if (fromWin && fromWin.isVisible() && fromWin.vid != vid && !win) {
				  win = this._attrDtEdWin[vid] = fromWin
				  this._attrDtEdWin[fromWin.vid] ? delete this._attrDtEdWin[fromWin.vid] : 0
			  }
			  if (!win) {
				  win ? delete this._attrDtEdWin[vid] : 0
				  this._attrDtEdWin[vid] = win = new obj.detail.AttrDetailEditWindow({
					    title : '属性编辑界面',
					    objectId : this._fmP.getForm().getFieldValues()['objectId'],
					    attrP : this._attrP,
					    mainP : this
				    });
				  win.getObjectData = function(excludeAttrs) {
					  return this._fmP.getForm().getFieldValues();
				  }.dg(this);
				  win.on('attributeValueChange', this._syncAttributeData, this)
				  win.on('navAttr', function(dir, fromWin) {
					    this._attrP.navigateAttr(dir, fromWin)
				    }, this)
				  var p = this.getObjEditPrivilege()
				  var b = win.getTopToolbar().saveBtn
				  b[p.MFAQ || p.M ? 'enableByTarget' : 'disableByTarget'](1)
				  b[!this.lockTimeout ? 'enableByTarget' : 'disableByTarget'](2)

				  win.attrDtEd.on('cellclick', this.tryResetEditingTime, this)
				  win.ruleEditGrid.on('cellclick', this.tryResetEditingTime, this)
				  win.sampleEditGrid.on('cellclick', this.tryResetEditingTime, this)
				  win.on('close', function() {
					    this._attrDtEdWin[win.vid] ? delete this._attrDtEdWin[win.vid] : 0
				    }, this)
			  }
			  win.vid = vid;
			  var object = this._fmP.getForm().getFieldValues();;
			  var subtitle = rs[0].get('question') ? rs[0].get('question') : '';
			  if (subtitle.length > 20)
				  subtitle = subtitle.substring(0, 17) + '...';
			  var title = '详情编辑 - ' + object.name + '::' + subtitle;
			  win.show()
			  win.toFront()
			  if (triggerByUser && !oldwin) {
				  win.alignTo(btn, 'tr?', [50, 0])
			  }
			  win.setTitle(title);
			  win.ispub = this._editObjectParam[3]
			  win.loadData(rs, object);
			  win.disableDynValidation = this._editObjectParam[4] ? this._editObjectParam[4].disableDynValidation : false
		  }, this);
		// enable(disable) delete button against the selected attribute record
		// this._attrP.on('rowclick', function(grid, rowIndex, event) {
		// var r = grid.getStore().getAt(rowIndex);
		// if (r.get('attributeType') == 1) {
		// // cannot delete XXX
		// this.deleteBtn.disable();
		// } else {
		// this.deleteBtn.enable();
		// }
		// }, this);
		// sync the ontology-class selected's info to class attribute chooser
		this._fmP.on('ontologyClassesChange', function(formEdP, ids, v, checked, value) {
			  this._attrP.ontologyClassesChange(ids, v, checked)
		  }, this)
		this.on('beforeclose', function() {
			  if (this.forceClose)
				  return;
			  this._confirmModified(function() {
				    this.forceClose = true;
				    this.close();
				    delete this.forceClose
			    }.dg(this))
			  return false;
		  }, this);
		this.on('tabinactive', function() {
			  this._attrP.stopEditing();
			  Ext.each([this._canvas, this.varedwin, this._RAMgrWin], function(c) {
				    c && c.isVisible() ? c.hide(c._visible = 1) : 0
			    })
		  })
		this.on('tabactive', function() {
			  Ext.each([this._canvas, this.varedwin, this._RAMgrWin], function(c) {
				    c && c._visible ? c.show(delete c._visible) : 0
			    })
		  })
		this.addListener('render', function() {
			  this._buildTbarEx().render(this.tbar);
		  }, this)
	},
	destroy : function() {
		this.unlockEditingObject()
		if (this._attrDtEdWin) {
			for (var key in this._attrDtEdWin) {
				this._attrDtEdWin[key].destroy()
				delete this._attrDtEdWin[key]
			}
		}
		Ext.each('_fmP _attrP _fmP _attrDtEdWin _canvas varedwin _RAMgrWin'.split(' '), function(c) {
			  if (this[c]) {
				  this[c].destroy()
				  delete this[c]
			  }
		  })
		obj.detail.MainPanel.superclass.destroy.call(this)
	},
	// handlers
	_deleteAttr : function() {
		var r = this._attrP.getSelectionModel().getSelected();
		if (r != null) {
			this._attrP.getStore().remove(r);
		}
	},
	_saveObject : function(cb, options, comment) {
		if (this.getTopToolbar().saveBtn.disabled)
			return
		this._attrP.stopEditing();
		if (!this._fmP.getForm().isValid()) {
			Ext.Msg.alert('验证不通过', '对象实例表单有未填字段，请先填写后再保存')
			return;
		}
		if (!comment && confirmBlankComment == 'true') {
			var ec = this._fmP.getForm().getFieldValues().editComment;
			if (!ec || !ec.trim()) {
				Ext.Msg.confirm('提示', '您没有填写备注，确定要提交吗（本次修改将不加入版本记录）？', function(result) {
					  if (result == 'yes') {
						  this._attrP.validate(this._saveObject0.dg(this, [cb, options]))
					  }
				  }, this)
				return;
			}
		}
		if (comment)
			this._fmP.getForm().findField("editComment").setValue(comment);
		this._attrP.validate(this._saveObject0.dg(this, [cb, options]))
	},
	_saveObject0 : function(cb, options) {
		options = options || {}
		var dataObject = this.getPostData();
		// 获取实例管关联数据
		if (this._RAMgrCaseRelationWin) {
			var gridContrastStore = this._RAMgrCaseRelationWin.gridContrast.getStore();
			var gridRelevanceStore = this._RAMgrCaseRelationWin.gridRelevance.getStore();
			dataObject.relations = [];
			Ext.each(gridContrastStore.getRange(), function(r) {
				  dataObject.relations.push({
					    'name' : r.data.name,
					    'type' : r.data.type
				    });
			  });
			Ext.each(gridRelevanceStore.getRange(), function(r) {
				  dataObject.relations.push({
					    'name' : r.data.name,
					    'type' : r.data.type
				    });
			  });
		} else {
			dataObject.relations = this.objectInEditing.relations;
		}
		ux.util.resetEmptyString(dataObject);
		this.getEl().mask('保存...')
		Ext.Ajax.request({
			  url : 'ontology-object!save.action',
			  params : {
				  data : Ext.encode(dataObject, 'Y-m-d H:i'),
				  ispub : this._editObjectParam[3],
				  sync : isKbase ? this.getTopToolbar().sync.checked : this.getTopToolbar().saveBtn.sync.checked,
				  attrVDeleted : this._attrP.attrVDeleted ? true : false,
				  disableDynValidation : this._editObjectParam[4] ? this._editObjectParam[4].disableDynValidation : false,
				  lockEditing : options.lockEditing === false ? false : true
			  },
			  success : function(response) {
				  if (options.lockEditing !== false) {
					  this.lockEditingObject()
				  }
				  this.getEl().unmask()
				  var result = Ext.util.JSON.decode(response.responseText);
				  if (!result.success) {
					  Ext.Msg.alert("提示", result.message);
					  return;
				  }
				  var d = result.data;
				  this.navPanel.updateAttr({
					    id : d.objectId,
					    text : d.name,
					    iconCls : 'icon-ontology-object',
					    objectId : d.objectId
				    }, d.categoryId, this.tabId);
				  this._attrP.getStore().clearData();
				  this._loadObject(d);

				  if (cb)
					  cb(true)

				  if (this._editObjectParam) {
					  // obj
					  var obj = this._editObjectParam[0];
					  if (obj && !obj.objectId) {
						  obj.objectId = d.objectId
					  }
				  }
				  if (result.multilineMsg) {
					  Ext.Msg.show({
						    title : '警告',
						    msg : result.message,
						    buttons : Ext.MessageBox.OK,
						    width : 600
					    })
					  addMsgBoxDetail(result.multilineMsg)
				  } else
					  Dashboard.setAlert("保存成功!")
				  this._resetModifiedState()
			  },
			  failure : function(response) {
				  this.getEl().unmask()
				  Ext.Msg.alert("错误", '错误信息:<br>' + response.responseText.replace(/\r\n/ig, '<br>'));
				  if (cb)
					  cb(false)
			  },
			  scope : this
		  });
	},
	// event
	_syncAttributeData : function(attribute) {
		var attr = this._attrP.getStore().getById(attribute._recordId);
		if (attr != null) {
			attr.beginEdit();
			for (var key in attribute) {
				if (attr.get(key) == attribute[key]) {
					continue;
				}
				if (key == 'value') {
					var setEmptyAllowed = this._attrP.updateDimensionValue(attr.data, dimensionChooser.getMenu().getSelectedDimensions(), attribute[key]);
					if (!setEmptyAllowed) {
						Ext.Msg.alert('提示', '该属性值的其他维度均为空，请先清除“问题”字段！');
						this._attrP.editAttrDetail(attribute._recordId);
						attr.cancelEdit();
						return false;
					}
				}
				attr.set(key, attribute[key])
			}
			attr.endEdit();
			return true;
		}
		return false;
	},
	// default config
	region : 'center',
	layout : 'border',
	split : true,
	width : 200,
	minSize : 175,
	maxSize : 400,
	margins : '0 0 0 0',
	border : false
	 // style : 'border-left-width:1px'
 })
var confirmBlankComment = http_get('property!get.action?key=kbmgr.obj.confirmBlankComment');
// var _objrelations = http_get('property!get.action?key=kbmgr.obj_relations');
// if (_objrelations) {
// _objrelations = Ext.decode(_objrelations)
// }
