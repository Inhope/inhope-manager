XAnsEditor = Ext.extend(Ext.form.Field, {
	  // private properties
	  actionMode : 'wrap',
	  // deferHeight : true,
	  height : 150,
	  // private
	  initComponent : function() {
		  this.id = Ext.id()
		  this.renderers = {}
		  XAnsEditor.superclass.initComponent.call(this);
	  },
	  activateEditor : function(obj) {
		  this.v = obj
		  var type = obj.type;
		  for (var key in this.editors) {
				  Ext.fly(this.editors[key]).enableDisplayMode()[key == type ? 'show' : 'hide']()
		  }
		  var ed = this.lastEd = this.editors[type]
		  if (type == 'txt') {
			  ed.field.setValue(obj.data || '')
		  } else if (type == 'app') {
			  if (obj.data && obj.data.args && !obj.data.appArgs) {
				  obj.data.appArgs = Ext.decode(obj.data.args)
			  }
			  var tpl = new Ext.XTemplate('' + '<p appid={id}>/调用 : {name}</p>' + //
			      '<tpl if="typeof appArgs != \'undefined\'">' + //
			      '<tpl for="appArgs">' + //
			      '<p>{title} = <input style="border:0;border-bottom: 1px dashed #CCCCCC;" name="{name}" title="{title}" optional="{optional}" value="{value}"/>' + //
			      '	<tpl if="optional == 1"><font color="red">*</font></tpl>' + //
			      '</p>' + //
			      '</tpl>', // use current array index to
			    '</tpl>');
			  tpl.overwrite(ed, obj.data); // pass the kids
		  } else if (type == 'refobj') {
		  	ed.innerHTML = '<p>/引用实例 : '+obj.title+'</p>'
		  } else {
			  var rd = this.renderers[type];
			  if (!rd) {
				  this.renderers[type] = rd = new IFramePanel()
				  ed.innerHTML = rd.html
			  }
			  if (obj.id) {
				  var url;
				  if (type == 'imgtxtmsg') {
					  url = "../matmgr/imgtxt-msg!getImgmsg.action?imgmsgId=";
				  } else if (type == 'imagemsg') {
					  url = "../matmgr/imagemsg/imageItem.jsp?imageId=";
				  } else if (type == 'audiomsg') {
					  url = "../matmgr/audiomsg/audioItem.jsp?audioId=";
				  } else if (type == 'videomsg') {
					  url = "../matmgr/videomsg/videoItem.jsp?videoId=";
				  } else if (type == 'p4msg') {
					  url = "../matmgr/p4-msg!showP4Html.action?objectId=";
				  }
				  url ? rd.location(url + obj.id) : 0
			  }
		  }
		  this.findBtn(type) ? this.findBtn(type).toggle(true) : 0
		  ed.field ? ed.field.focus() : ed.focus()
	  },
	  findBtn : function(type) {
		  return this.tb.find('itemId', type)[0]
	  },
	  btn : function(id, toggle, handler) {
		  var tipsEnabled = Ext.QuickTips && Ext.QuickTips.isEnabled();
		  return {
			  itemId : id,
			  cls : 'x-btn-icon',
			  icon : 'images/xans-' + id.replace('msg', '') + '.png',
			  toggleGroup : this.id,
			  enableToggle : true,
			  handler : function(b, state) {
				  if (b.itemId.indexOf('msg') != -1 || b.itemId == 'app' || b.itemId == 'refobj') {// material
					  if (this.v && this.v.type && this.v.type != this.findBtn(b.itemId)) {
						  this.findBtn(this.v.type).toggle(true)
					  }
					  var win = this.win
					  if (true) {
						  win = this.win = b.itemId == 'refobj'?new ObjChooserWin():new AnsEditorWin();
						  win.on('materialSelected', function(obj) {
							    // obj = {
							    // type : 'app',
							    // id : 'a5f97e2399f2446eabbee0e7c8424d7e',
							    // data : {
							    // id : 'a5f97e2399f2446eabbee0e7c8424d7e',
							    // title : 'test app title',
							    // }
							    // }
							    this.activateEditor(obj)
						    }.dg(this))
					  }
					  win.show()
					  var obj = {
						  type : b.itemId
					  }
					  if (this.v && b.itemId == this.v.type)
						  obj = this.v
					  win.loadData(obj.type, obj.id,obj.title)
				  } else {
					  if (!(this.v && b.itemId == this.v.type)) {
						  this.activateEditor({
							    type : b.itemId
						    })
					  }
				  }
			  }.dg(this),
			  tooltip : tipsEnabled ? this.buttonTips[id] || undefined : undefined,
			  overflowText : this.buttonTips[id].title || undefined,
			  tabIndex : -1
		  };
	  },
	  createToolbar : function(editor) {
		  var items = [];
		  for (var key in this.buttonTips) {
			if ((key == 'app' || key =='refobj') && !Dashboard.u().isSupervisor() && !sys_labs){
				delete this.buttonTips[key]
		  	  	continue;
		  	  }
			  var bcfg = this.buttonTips[key]
			  items.push(this.btn(key));
		  }

		  // build the toolbar
		  var tb = new Ext.Toolbar({
			    renderTo : this.wrap.dom.firstChild,
			    items : items
		    });
		  // stop form submits
		  this.mon(tb.el, 'click', function(e) {
			    e.preventDefault();
		    });

		  this.tb = tb;
		  this.tb.doLayout();

		  var editors = this.editors = {}
		  var contentRegion = document.createElement('div')
		  contentRegion.className = 'xhtmleditor'
		  this.wrap.dom.appendChild(contentRegion)
		  Ext.fly(contentRegion).applyStyles('position:relative')
		  // app
		  // {title:'',args:[{"name":"a","optional":0,"title":"b1"},{"name":"bb","optional":0,"title":"啊"}]}
		  var editorwrap;
		  var editorwrapdiv;
		  for (var key in this.buttonTips) {
			  if (key == 'txt') {
				  var id = Ext.id()
				  editorwrap = document.createElement('div');
				  document.body.appendChild(editorwrap)
				  var field = this.createTxtEditor ? this.createTxtEditor() : Ext.create({
					    xtype : 'htmleditor',
					    width : '100%'
				    })
				  field.render(editorwrap)
				  editorwrap.field = field
				  // editorwrap = document.createElement('textarea');
				  // Ext.fly(editorwrap).on('keyup', function(e, t, o) {
				  // this.fireEvent('keyup', e, t, o,key)
				  // }, this)
			  } else {
				  editorwrap = document.createElement('div');
				  editorwrap.innerHTML = key
			  }
			  editors[key] = editorwrap;
			  Ext.fly(editorwrap).applyStyles('border:0;width:98%;height:98%;position:relative;display:none')
			  contentRegion.appendChild(editorwrap)
		  }
	  },
	  onDisable : function() {
		  this.wrap.mask();
		  XAnsEditor.superclass.onDisable.call(this);
	  },

	  onEnable : function() {
		  this.wrap.unmask();
		  XAnsEditor.superclass.onEnable.call(this);
	  },

	  setReadOnly : function(readOnly) {
		  XAnsEditor.superclass.setReadOnly.call(this, readOnly);
	  },

	  onRender : function(ct, position) {
		  XAnsEditor.superclass.onRender.call(this, ct, position);
		  this.el.dom.style.border = '0 none';
		  this.el.dom.setAttribute('tabIndex', -1);
		  this.el.addClass('x-hidden');
		  if (Ext.isIE) { // fix IE 1px bogus margin
			  this.el.applyStyles('margin-top:-1px;margin-bottom:-1px;');
		  }
		  this.wrap = this.el.wrap({
			    cls : 'x-html-editor-wrap',
			    cn : {
				    cls : 'x-html-editor-tb'
			    }
		    });

		  this.createToolbar(this);

		  this.tb.doLayout();

		  if (!this.width) {
			  var sz = this.el.getSize();
			  this.setSize(sz.width, this.height || sz.height);
		  }
		  this.resizeEl = this.positionEl = this.wrap;
		  this.activateEditor(this.v ? this.v : {
			  type : 'txt'
		  })
	  },

	  // private
	  onResize : function(w, h) {
		  XAnsEditor.superclass.onResize.apply(this, arguments);
		  if (this.lastEd && this.lastEd.field) {
			  this.lastEd.field.setHeight(h - 30)
		  }
	  },

	  markInvalid : Ext.emptyFn,

	  clearInvalid : Ext.emptyFn,

	  setValue : function(v) {
		  this.v = typeof v == 'string' ? {
			  type : 'txt',
			  data : v
		  } : v
	  },

	  show : function() {
		  if (this.rendered) {
			  this.activateEditor(this.v ? this.v : {
				  type : 'txt'
			  })
		  }
		  return XAnsEditor.superclass.show.call(this)
	  },

	  isValid : function() {
		  if (this.v.type == 'app') {
			  var ed = this.editors[this.v.type]
			  var inputs = Ext.fly(ed).query('input');
			  var ret = true;
			  Ext.each(inputs, function(input) {
				    if (input.name && input.getAttribute('optional') == 1 && (!input.value || !input.value.trim())) {
					    Dashboard.setAlert(input.getAttribute('title') + '为必填项', 'error');
					    ret = false;
					    return false;
				    }
			    }, this)
			  return ret
		  }
		  return true
	  },
	  getValue : function() {
		  if (!this.v)
			  return this.v;
		  var ret = {
			  type : this.v.type
		  }
		  var ed = this.editors[this.v.type]
		  if (this.v.type == 'txt') {
			  ret.data = ed.field.getValue()
		  } else if (this.v.type == 'app') {
			  var inputs = Ext.fly(ed).query('input');
			  var appid = Ext.fly(ed).query('[appid]')[0];
			  ret.id = appid.getAttribute('appid')
			  Ext.each(inputs, function(input) {
				    if (input.name && input.value) {
					    !ret.data ? ret.data = {} : 0
					    ret.data[input.name] = input.value
				    }
			    }, this)
		  } else {// material,already set
			  return this.v
		  }
		  return ret;
	  },

	  // docs inherit from Field
	  focus : function() {
	  },

	  // private
	  beforeDestroy : function() {
		  if (this.monitorTask) {
			  Ext.TaskMgr.stop(this.monitorTask);
		  }
		  if (this.rendered) {
			  Ext.destroy(this.tb);
			  Ext.destroy(this.win);
			  if (this.wrap) {
				  this.wrap.dom.innerHTML = '';
				  this.wrap.remove();
			  }
		  }
		  XAnsEditor.superclass.beforeDestroy.call(this);
	  },

	  getToolbar : function() {
		  return this.tb;
	  },

	  buttonTips : {
		  txt : {
			  title : '文本',
			  text : '填写文本内容'
		  },
		  app : {
			  title : '应用',
			  text : '选择应用，填写调用参数'
		  },
		  imgtxtmsg : {
			  title : '图文',
			  text : '选择图文'
		  },
		  imagemsg : {
			  title : '图片',
			  text : '选择图片'
		  },
		  audiomsg : {
			  title : '语音',
			  text : '选择声音'
		  },
		  videomsg : {
			  title : '视频',
			  text : '选择视频'
		  },
		  p4msg : {
			  title : 'P4',
			  text : '选择P4'
		  },
		  refobj : {
			  title : '引用实例',
			  text : '选择实例'
		  }
	  }
  });
Ext.reg('xhtmleditor', XAnsEditor);
