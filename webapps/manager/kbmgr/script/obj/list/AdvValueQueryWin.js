Ext.namespace('obj.list')
obj.list.AdvValueQueryForm = Ext.extend(Ext.FormPanel, {
	  frame : true,
	  border : false,
	  style : 'border:none',
	  destroy : function() {
		  delete this.listP
		  obj.list.AdvValueQueryForm.superclass.destroy.call(this)
	  },
	  listP : null,
	  initComponent : function() {
		  this.searchAllSchema = true;
		  this.buttons = [{
			    text : '重置',
			    handler : function() {
				    this.getForm().reset();
			    }.dg(this)
		    }, {
			    xtype : 'splitbutton',
			    text : '搜索',
			    menu : {
				    items : [{
					      text : '精确匹配',
					      checked : false,
					      checkHandler : function(btn, checked) {
						      this.matchExactly = checked
					      }.dg(this)
				      }, {
					      text : '搜索全部',
					      checked : true,
					      checkHandler : function(btn, checked) {
						      this.searchAllSchema = checked
					      }.dg(this)
				      }]
			    },
			    handler : function() {
				    var vs = this.getSearchValueMap();
				    this.listP.listValue(vs, true, true)
			    }.dg(this)
		    }]
		  obj.list.AdvValueQueryForm.superclass.initComponent.call(this);
	  },
	  getSearchValueMap : function() {
		  var vs = this.getForm().getFieldValues();
		  var extravs = {}
		  for (var k in vs) {
			  if (k && k.toLowerCase().indexOf('time') != -1) {
				  var start = vs[k][0];
				  var end = vs[k][1];
				  if (start || end) {
					  if (start && end) {
						  if (start.toString() == end.toString())
							  end.setHours(23), end.setMinutes(59), end.setSeconds(59)
					  } else if (start) {
						  end = 0
					  } else {
						  start = 0
					  }
					  start ? start = Ext.util.Format.date(start, 'Y-m-d H:i') : 0
					  end ? end = Ext.util.Format.date(end, 'Y-m-d H:i') : 0
				  }
				  var camelk = k.substring(0, 1).toUpperCase() + k.substring(1);
				  start ? extravs['start' + camelk] = start : 0;
				  end ? extravs['end' + camelk] = end : 0;
				  delete vs[k]
			  }
		  }
		  Ext.apply(vs, extravs)
		  if (this.matchExactly)
			  vs.queryMatchMode = 'eq'
		  if (this.searchAllSchema)
			  vs.searchSchema = 'all'
		  return vs;
	  },
	  labelWidth : 55,
	  buildForm : function(itemConfigs) {
		  if (!this.formBuilt) {
			  this.formBuilt = true
			  itemConfigs.push({
				    name : 'editTime',
				    label : '修改时间'
			    }, {
				    name : 'startTime',
				    label : '开始时间'
			    }, {
				    name : 'endTime',
				    label : '结束时间'
			    })
			  if ('true' == enable_endtime1) {
				  itemConfigs.push({
					    name : 'endTime1',
					    label : '追溯时间'
				    })
			  }
			  Ext.each(itemConfigs, function(cfg) {
				    cfg.fieldLabel = cfg.label
				    delete cfg.label
				    delete cfg.width
				    cfg.anchor = '100%'
				    if (cfg.valueRendererData) {
					    var vrd = cfg.valueRendererData
					    var ds = []
					    for (var key in vrd) {
						    if (key)
							    ds.push([key, vrd[key]])
					    }
					    Ext.apply(cfg, {
						      xtype : 'combo',
						      store : ds,
						      triggerAction : 'all',
						      mode : 'local'
					      })
					    delete cfg.valueRendererData
				    }
			    }, this)
			  var timeItems = []
			  for (var i = itemConfigs.length - 1; i >= 0; i--) {
				  var cfg = itemConfigs[i];
				  if (cfg.xtype == 'button') {
					  itemConfigs.splice(i, 1)
					  itemConfigs.push(cfg)
					  return false;
				  } else if (cfg.name.toLowerCase().indexOf('time') != -1) {
					  itemConfigs.splice(i, 1)
					  timeItems.push(cfg)
					  cfg.xtype = 'datetimefield';
					  cfg.toText = ''
					  cfg.hideAllDay = true
				  }
			  }
			  timeItems.reverse()
			  var col_times = []
			  for (var i = 0; i < timeItems.length; i++) {
				  var cfg = timeItems[i]
				  col_times.push({
					    xtype : 'panel',
					    layout : 'form',
					    labelAlign : 'top',
					    anchor : '100%',
					    items : cfg
				    })
			  }
			  this.add({
				    layout : 'column',
				    items : [{
					      columnWidth : 1,
					      style:'padding-right:8px',
					      layout : 'form',
					      items : itemConfigs
				      }, {
					      width : 172,
					      layout : 'form',
					      items : col_times
				      }]
			    })
		  }
		  this.doLayout();
	  }
  })
obj.list.AdvValueQueryWin = Ext.extend(Ext.Window, {
	  initComponent : function() {
		  this._fmP = new obj.list.AdvValueQueryForm({
		    });
		  this.items = this._fmP
		  obj.list.AdvValueQueryWin.superclass.initComponent.call(this);
	  },
	  getSearchValueMap : function() {
		  return this._fmP.getSearchValueMap()
	  },
	  open : function(itemConfigs, listP) {
		  this._fmP.listP = listP;
		  this._fmP.buildForm(itemConfigs);
		  this.doLayout()
	  },
	  title : '高级查询',
	  minHeight: 440,
	  height: 440,
	  width : 400,
	  minWidth : 400,
	  // width : 200,
	  closeAction : 'hide',
	  layout : 'fit',
	  border : false,
	  iconCls : 'icon-ontology-cate'
  })