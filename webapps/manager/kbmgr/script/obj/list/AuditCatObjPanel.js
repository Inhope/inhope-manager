AuditCatObjPanel = function(_cfg) {
	var self = this;
	this.data = {};
	this.extraAttrs = {};
	var _pageSize = 20;
	var dictType;
	this.readMode = _cfg.readMode;
	this.state = _cfg.auditState;
	this.mySubmit = _cfg.mySubmit;

	this.queryMode = _cfg.isObject;

	var _store = new Ext.data.Store({
		  proxy : new Ext.data.HttpProxy({
			    url : 'ontology-object!auditList.action',
			    params : {
				    type : self.queryMode,
				    state : self.state,
				    mySubmit : self.mySubmit
			    }
		    }),
		  reader : new Ext.data.JsonReader({
			    idProperty : 'id',
			    root : 'data',
			    fields : [{
				      name : 'id',
				      type : 'string'
			      }, {
				      name : 'pathname',
				      type : 'string'
			      }, {
				      name : 'categoryname',
				      type : 'string'
			      }, {
				      name : 'objectname',
				      type : 'string'
			      }, {
				      name : 'state',
				      type : 'string'
			      }, {
				      name : 'op',
				      type : 'string'
			      }, {
				      name : 'submitauth',
				      type : 'string'
			      }, {
				      name : 'submittime',
				      type : 'string'
			      }, {
				      name : 'ispub',
				      type : 'string'
			      }]
		    }),
		  writer : new Ext.data.JsonWriter()
	  });

	var pagingBar = new Ext.PagingToolbar({
		  store : _store,
		  displayInfo : true,
		  pageSize : _pageSize,
		  prependButtons : true,
		  beforePageText : '第',
		  afterPageText : '页，共{0}页',
		  displayMsg : '第{0}到{1}条记录，共{2}条',
		  emptyMsg : "没有记录",
		  prependButtons : false
	  })
	var _sm = new Ext.grid.CheckboxSelectionModel({});
	// var isObject = new Ext.form.Radio({
	// xtype : 'radio',
	// name : 'isType',
	// boxLabel : '实例审核',
	// inputValue : 1,
	// checked : true,
	// listeners : {
	// 'check' : function(own, checked) {
	// if (checked) {
	// self.queryMode = 1;
	// self.loadData();
	// }
	// }
	// }
	// })
	//
	// var isCategory = new Ext.form.Radio({
	// xtype : 'radio',
	// name : 'isType',
	// boxLabel : '目录审核',
	// inputValue : 2,
	// listeners : {
	// 'check' : function(own, checked) {
	// if (checked) {
	// self.queryMode = 0;
	// self.loadData();
	// }
	// }
	// }
	// })

	var queryData = [[0, '全部'], [1, self.queryMode == 0 ? '目录名称' : '实例名称']];
	var queryCombo = new Ext.form.ComboBox({
		  typeAhead : true,
		  triggerAction : 'all',
		  lazyRender : true,
		  mode : 'local',
		  store : new Ext.data.ArrayStore({
			    id : 0,
			    fields : ['type', 'text'],
			    data : !self.mySubmit ? queryData.concat([[2, '提交人']]) : queryData
		    }),
		  valueField : 'type',
		  displayField : 'text'
	  });
	var queryValue = new Ext.form.TextField({
		  emptyText : '请填写你要查询的内容'
	  });
	this.queryCombo = queryCombo;
	this.queryValue = queryValue;

	var actionTbarArray = ['查询条件:', queryCombo, queryValue, {
		  xtype : 'button',
		  text : '搜索',
		  iconCls : 'icon-search',
		  handler : function() {
			  self.loadData(true);
		  }
	  }, '-', {
		  xtype : 'button',
		  text : '审核通过',
		  iconCls : 'icon-table-edit',
		  handler : this._doAudit.dg(this, [1]),
		  ref : 'auditBtn'
	  }, {
		  text : '审核驳回',
		  iconCls : 'icon-table-edit',
		  handler : this._doAudit.dg(this, [0])
	  }]
	var auditpart = (this.readMode ? ['查询条件:', queryCombo, queryValue, {
		  xtype : 'button',
		  text : '搜索',
		  iconCls : 'icon-search',
		  handler : function() {
			  self.loadData(true);
		  }
	  }, '-'] : actionTbarArray).concat({
		  text : '刷新',
		  iconCls : 'icon-refresh',
		  handler : function() {
			  this.getStore().reload();
		  },
		  scope : this
	  });
	var actionTbar = new Ext.Toolbar({
		  items : auditpart
	  });

	var cfg = {
		region : 'center',
		border : false,
		frame : false,
		store : _store,
		loadMask : true,
		colModel : new Ext.grid.ColumnModel({
			  defaults : {
				  sortable : true
			  },
			  columns : [_sm, new Ext.grid.RowNumberer(), {
				    header : 'id',
				    dataIndex : 'id',
				    sortable : false,
				    hidden : true
			    }, {
				    header : '目录名称',
				    dataIndex : 'categoryname',
				    sortable : false,
				    hidden : self.queryMode == 1 ? true : false
			    }, {
				    header : '实例名称',
				    dataIndex : 'objectname',
				    sortable : false,
				    hidden : self.queryMode == 1 ? false : true
			    }, {
				    header : '路径名称',
				    dataIndex : 'pathname',
				    sortable : false,
				    width : 450,
				    fixed : true
			    }, {
				    header : '状态',
				    dataIndex : 'state',
				    sortable : false,
				    renderer : function(v, meta, r) {
					    var a = arguments;
					    var s = ''
					    v == 0 ? (a[0] = '未同步', s = 'color:tomato;') : 0
					    v == STATE_SYNC_SUC ? (a[0] = '已同步', s = 'color:green;') : 0
					    v == STATE_SYNC_FAIL ? (a[0] = '同步失败', s = 'color:red;') : 0
					    v == STATE_AUD_SUBMIT ? (a[0] = '提交审批', s = 'color:magenta;') : 0
					    v == STATE_AUD_PASS ? (a[0] = '审批通过', s = 'color:green;') : 0
					    v == STATE_AUD_REJECT ? (a[0] = '审批驳回', s = 'color:red;') : 0
					    if (s) {
						    meta.attr = 'style=' + s + (v == 2 ? 'font-weight:bold;' : '')
					    }
					    return v;
				    }
			    }, {
				    header : '操作',
				    dataIndex : 'op',
				    sortable : false,
				    renderer : function(v, meta, r) {
					    v = (v == 1 ? '新建' : (v == 2 ? '修改' : '删除'));
					    return v;
				    }
			    }, {
				    header : '提交人',
				    dataIndex : 'submitauth',
				    sortable : false
			    }, {
				    header : '提交时间',
				    dataIndex : 'submittime',
				    sortable : false
			    }, {
				    header : 'ispub',
				    dataIndex : 'ispub',
				    sortable : false,
				    hidden : true
			    }]
		  }),
		tbar : actionTbar,
		sm : _sm,
		bbar : pagingBar,
		viewConfig : {
			forceFit : true
		}

	}
	AuditCatObjPanel.superclass.constructor.call(this, Ext.applyIf(cfg));
	this.cfg = cfg;
	this.getPageSize = function() {
		return _pageSize;
	}
	// self.loadData();
}
Ext.extend(AuditCatObjPanel, Ext.grid.GridPanel, {
	  loadData : function(queryMode) {
		  var self = this;
		  var store = this.getStore();
		  for (var key in store.baseParams) {
			  if (key && key.indexOf('queryParam.') != -1)
				  delete store.baseParams[key];
		  }
		  if (queryMode) {
			  if (self.queryCombo.getValue()) {
				  if (self.queryCombo.getValue() > 0 && !self.queryValue.getValue()) {
					  Ext.Msg.alert('提示', "请输入查询内容");
					  return;
				  }
				  store.setBaseParam('queryParam.type', self.queryCombo.getValue());
				  store.setBaseParam('queryParam.value', self.queryValue.getValue());
			  }
		  }
		  store.load({
			    params : {
				    start : 0,
				    limit : this.getPageSize(),
				    type : self.queryMode,
				    state : self.state,
				    mySubmit : self.mySubmit
			    },
			    add : false,
			    callback : function(r, op, success) {
				    // if (success) {
				    // if (self.queryMode == 0) {
				    // this.cfg.colModel.setHidden(4, true);
				    // this.cfg.colModel.setHidden(3, false);
				    // } else {
				    // this.cfg.colModel.setHidden(3, true);
				    // this.cfg.colModel.setHidden(4, false);
				    // }
				    // }
			    },
			    scope : this
		    });
	  },
	  _doAudit0 : function(vids, type) {
		  var self = this;
		  Ext.Ajax.request({
			    url : 'ontology-object!audit.action',
			    params : {
				    isObject : this.queryMode,
				    auditType : type,
				    // sync : type == 0 ? false :
				    // self.getTopToolbar().auditBtn.sync.checked,
				    data : Ext.encode(vids)
			    },
			    success : function(resp) {
				    var obj = Ext.decode(resp.responseText);
				    if (obj.success) {
					    Dashboard.setAlert((type ? '审核' : '驳回') + '成功')
					    self.getStore().reload();
				    } else {
					    Ext.Msg.alert('异常', obj.message);
				    }
			    },
			    failure : function(response) {
				    Ext.Msg.alert("错误", response.responseText);
			    },
			    scope : this
		    });
	  },
	  _doAudit : function(type) {
		  var self = this;
		  var rs = this.getSelectionModel().getSelections();
		  if (rs.length == 0) {
			  Ext.Msg.alert('提示', '请先选择要操作的记录');
			  return;
		  }
		  var vids = []
		  Ext.each(rs, function(r) {
			    var cateObjName = self.queryMode == 0 ? r.get('categoryname') : r.get('objectname');
			    vids.push({
				      pendingAuditId : r.get('id'),
				      ispub : r.get('ispub'),
				      pathname : r.get('pathname'),
				      name : cateObjName,
				      submitauth : r.get('submitauth'),
				      submittime : r.get('submittime'),
				      op : r.get('op'),
				      ispub : r.get('ispub')
			      })
		    }, this)
		  this._doAudit0(vids, type)
	  }
  })
