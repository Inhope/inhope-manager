
/**
 * 
 * @include "../detail/AttrEditPanel.js"
 */
Ext.namespace('obj.list')
obj.list.AuditPanel = Ext.extend(obj.list.BasePanel, {
	  // init
	  disableLocalSort : true,
	  readMode : false,
	  mode : obj.detail.AttrEditPanel.MODE.LIST,
	  CMPConfig : {
		  pageSize : 20
	  },
	  initComponent : function() {
		  obj.list.AuditPanel.superclass.initComponent.call(this);
		  this.searchAllSchema = true
	  },
	  // ui
	  _getSearchConfigs : function(excludeNonName) {
		  var sharedField = this.getSearchFieldsConfig();
		  for (var i = sharedField.length - 1; i >= 0; i--) {
			  var f = sharedField[i];
			  if (excludeNonName && !f.name || this.onlyMyOwn && f.name == 'editor' || f.name == 'dimension') {
				  sharedField.splice(i, 1);
			  }
		  }
		  return this.getSearchFields([{
			    width : 140,
			    name : 'dimension',
			    xtype : 'dimensionbox',
			    clearBtn : true,
			    menuConfig : {
				    labelAlign : 'top'
			    },
			    valueTransformer : function(value) {
				    if (value == 'ALLDIM')
					    return {
						    ALLDIM : []
					    }
				    var tags = this.tags;
				    var groupTags = {};
				    if (tags)
					    Ext.each(tags, function(t) {
						      var groupTag = groupTags[t.dimId];
						      if (!groupTag) {
							      groupTag = [];
							      groupTags[t.dimId] = groupTag;
						      }
						      groupTag.push(t.id)
					      });
				    return groupTags;
			    }
		    }, {
			    name : 'objectName',
			    label : '实例名称'
		    }].concat(sharedField));
	  },
	  _buildTbar : function() {
		  var filterNames = '实例名称、属性名称、标准问题、标准答案、指令';
		  var cfg = this.getSearchFields(this._getSearchConfigs());
		  this.dimChooser = Ext.create(cfg[0]);
		  Ext.each(cfg, function(f) {
			    if (f.name)
				    f.width = 110
			    if (f.name == 'typeSelector') {
				    f.getStore().each(function(r) {
					      if (r.get('field1') == 'dimension') {
						      f.getStore().remove(r)
						      return false;
					      }
				      })
			    }
		    }, this);
		  this.searchAllSchema = true;
		  var filterFields = [];
		  for (var i = 0; i < cfg.length; i++) {
			  var f = cfg[i];
			  if (f && f.label && filterNames.indexOf(f.label) == -1)
				  filterFields.push(f);
		  }

		  Ext.each(filterFields, function(ff) {
			    cfg.remove(ff);
		    })
		  var searchFields = this.getCompactSearchFields(cfg);
		  Ext.each(searchFields, function(f) {
			    if (f.name)
				    f.width = 100
			    if (f.name == 'typeSelector') {
				    f.width = 90
			    } else if (f.name == 'typeContent') {
				    f.width = 150
			    }
		    })

		  var checkIllegalWords = http_get('property!get.action?key=kbmgr.audit.check_illegalwords')
		  var auditpart = [{
			    xtype : 'splitbutton',
			    text : '审核通过',
			    iconCls : 'icon-table-edit',
			    handler : this._doAudit.dg(this, [1]),
			    ref : 'auditBtn',
			    menu : {
				    items : [{
					      text : '同步',
					      checked : true,
					      name : 'sync',
					      ref : '../sync'
				      }, {
					      text : '校验敏感词',
					      checked : 'true' == checkIllegalWords,
					      name : 'checkIlegalWords',
					      ref : '../checkIlegalWords'
				      }]
			    }
		    }, {
			    text : '审核驳回',
			    iconCls : 'icon-table-edit',
			    handler : this._doAudit.dg(this, [0])
		    }]
		  return [this.dimChooser.label + ':', this.dimChooser].concat(searchFields).concat((this.readMode ? [] : auditpart).concat({
			    text : '刷新',
			    iconCls : 'icon-refresh',
			    handler : function() {
				    this.refresh()
			    },
			    scope : this
		    }));
	  },
	  _doAudit0 : function(vids, type) {
		  Ext.Ajax.request({
			    url : 'ontology-object-value!audit.action',
			    params : {
				    type : type,
				    sync : type == 0 ? false : this.getTopToolbar().auditBtn.sync.checked,
				    data : Ext.encode(vids, 'Y-m-d H:i:s')
			    },
			    success : function(resp) {
				    Dashboard.setAlert((type ? '审核' : '驳回') + '成功')
				    this.refresh()
			    },
			    failure : function(response) {
				    Ext.Msg.alert("错误", response.responseText);
			    },
			    scope : this
		    });
	  },
	  _doAudit : function(type) {
		  var rs = this.getSelectionModel().getSelections();
		  if (rs.length == 0) {
			  Ext.Msg.alert('提示', '请先选择要操作的记录');
			  return;
		  }
		  var vids = []
		  Ext.each(rs, function(r) {
			    if (this._isDimParent(r))
				    vids.push({
					      objectId : r.get('objectId'),
					      valueId : r.get('valueId'),
					      disabled : r.get('disabled'),
					      question : r.get('question'),
					      objectName : r.get('objectName'),
					      editor : r.get('editor'),
					      op : r.get('op'),
					      editTime : r.get('editTime'),
					      createTime : r.get('createTime'),
					      state : r.get('state'),
					      ispub : r.get('ispub')
				      })
		    }, this)
		  if (type && this.getTopToolbar().auditBtn.checkIlegalWords.checked) {// 审核通过才需要检查关键字
			  Ext.Ajax.request({
				    url : 'ontology-object-value!checkIlegalWords.action',
				    params : {
					    data : Ext.encode(vids, 'Y-m-d H:i:s')
				    },
				    success : function(resp) {
					    if (!resp.responseText) {
						    this._doAudit0(vids, type)
					    } else {
						    var win = new AuditIllegalResultWin();
						    win.show()
						    win.loadData(Ext.decode(resp.responseText))
					    }
				    },
				    failure : function(response) {
					    Ext.Msg.alert("错误", response.responseText);
				    },
				    scope : this
			    });
		  } else {
			  this._doAudit0(vids, type)
		  }
	  },
	  auditState : STATE_AUD_SUBMIT,
	  myView : false,
	  _prepareQueryParams : function(queryMap) {
		  if (this.myView)
			  queryMap = this._addParamToQueryMap(queryMap, 'editor', Dashboard.u().getUsername())
		  queryMap = this._addParamToQueryMap(queryMap, 'state', this.auditState)
		  queryMap = obj.list.AuditPanel.superclass._prepareQueryParams.call(this, queryMap)
		  return queryMap;
	  },
	  destroy : function() {
		  obj.list.AuditPanel.superclass.destroy.call(this)
		  this.dimChooser ? delete this.dimChooser : 0
		  this._fmP ? delete this._fmP : 0
		  if (this._advwin) {
			  this._advwin.destroy();
			  delete this._advwin
		  }
	  },
	  // search
	  reset : function() {
		  obj.list.AuditPanel.superclass.reset.call(this)
		  this._fmP.getForm().reset();
	  },
	  onTabShow : function() {
		  this._doSearchImpl({});
	  },
	  margins : '0 0 0 0',
	  border : false,
	  style : 'border-right-width:1px',
	  columnLines : true,
	  loadMask : '加载中...'
  });
var AuditIllegalResultWin = Ext.extend(Ext.Window, {
	  width : 500,
	  height : 600,
	  layout : 'fit',
	  border : false,
	  title : '敏感词校验结果:: 以下为标准问或者答案包含不合法的关键词的知识点',
	  initComponent : function() {
		  this.items = this.datapanel = new Ext.grid.GridPanel({
			    store : new Ext.data.JsonStore({
				      autoDestroy : true,
				      root : 'data',
				      fields : ['question', 'content', 'type']
			      }),
			    autoExpandColumn : 'content',
			    colModel : new Ext.grid.ColumnModel({
				      defaults : {
					      width : 120
				      },
				      columns : [{
					        id : 'content',
					        header : '敏感词涉及内容',
					        dataIndex : 'content'
				        }, {
					        header : '标准问题',
					        dataIndex : 'question'
				        }, {
					        header : '类型',
					        dataIndex : 'type',
					        renderer : function(v) {
						        return v == 1 ? '标准问' : '标准答案'
					        }
				        }]
			      })
		    })
		  AuditIllegalResultWin.superclass.initComponent.call(this);
	  },
	  loadData : function(d) {
		  this.datapanel.store.loadData(d)
	  }
  })