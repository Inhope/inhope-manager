var actionGroup = ['', '新增', '修改', '删除'];
var stateGroup = ['未审核', '审核通过', '已驳回'];
var typeGroup = {
	1 : '目录',
	2 : '词类',
	4 : '修改父级',
	8 : '词'
};
AuditedWordclassPanel = function(_cfg) {
	var self = this;
	this.data = {};
	var _pageSize = 30;
	this.getPageSize = function() {
		return _pageSize;
	};

	var _store = new Ext.data.Store({
		  proxy : new Ext.data.HttpProxy({
			    url : 'wordclass!listAuditData.action'
		    }),
		  reader : new Ext.data.JsonReader({
			    idProperty : 'id',
			    root : 'data',
			    fields : [{
				      name : 'opUser',
				      type : 'string'
			      }, {
				      name : 'opAction',
				      type : 'string'
			      }, {
				      name : 'parentPath',
				      type : 'string'
			      }, {
				      name : 'newContent',
				      type : 'string'
			      }, {
				      name : 'target',
				      type : 'string'
			      }, {
				      name : 'state',
				      type : 'string'
			      }, {
				      name : 'opTimeStr',
				      type : 'string'
			      }, {
				      name : 'opType',
				      type : 'string'
			      }, {
				      name : 'auditUser',
				      type : 'string'
			      }, {
				      name : 'auditTimeStr',
				      type : 'string'
			      }, {
				      name : 'ex',
				      type : 'string'
			      }, {
				      name : 'orderName',
				      type : 'string'
			      }]
		    }),
		  writer : new Ext.data.JsonWriter(),
		  listeners : {
			  'beforeload' : function(th, n, cb) {
				  th.baseParams['isAudited'] = true;
			  }
		  }
	  });

	var pagingBar = new Ext.PagingToolbar({
		  store : _store,
		  displayInfo : true,
		  pageSize : _pageSize,
		  prependButtons : true,
		  beforePageText : '第',
		  afterPageText : '页，共{0}页',
		  displayMsg : '第{0}到{1}条记录，共{2}条',
		  emptyMsg : "没有记录"
	  })

	var tbarEx = new Ext.Toolbar({
		  items : [{
			    width : 48
		    }, '-', '目标值:', {
			    ref : 'auditedSearchWords',
			    xtype : 'textfield',
			    emptyText : '输入词搜索...',
			    listeners : {
				    specialkey : function(field, e) {
					    if (e.getKey() == Ext.EventObject.ENTER) {
						    self.search();
					    }
				    }
			    }
		    }, '新内容:', {
			    ref : 'auditedSearchOldwords',
			    xtype : 'textfield',
			    emptyText : '输入词搜索...',
			    listeners : {
				    specialkey : function(field, e) {
					    if (e.getKey() == Ext.EventObject.ENTER) {
						    self.search();
					    }
				    }
			    }
		    }, '词长度:', {
			    ref : 'auditedSearchWordSymbol',
			    xtype : 'combo',
			    triggerAction : 'all',
			    anchor : '100%',
			    allowBlank : false,
			    mode : 'local',
			    editable : false,
			    value : "1",
			    width : 75,
			    store : new Ext.data.ArrayStore({
				      fields : ['id', 'name'],
				      data : [["1", '大于'], ["2", '小于'], ["3", '大于等于'], ["4", '小于等于'], ["5", '等于']]
			      }),
			    valueField : 'id',
			    displayField : 'name'
		    }, {
			    ref : 'auditedSearchWordLength',
			    xtype : 'numberfield',
			    hideTrigger : true,
			    allowNegative : false,
			    nanText : '请输入有效的整数',
			    emptyText : '词长度',
			    allowDecimals : false,
			    width : 45,
			    listeners : {
				    specialkey : function(field, e) {
					    if (e.getKey() == Ext.EventObject.ENTER) {
						    self.search();
					    }
				    }
			    }
		    }, '词类类别:', {
			    ref : 'auditedSearchWordType',
			    xtype : 'combo',
			    triggerAction : 'all',
			    anchor : '100%',
			    mode : 'local',
			    editable : false,
			    value : "",
			    width : 75,
			    store : new Ext.data.ArrayStore({
				      fields : ['id', 'name'],
				      data : [["", "全部"], ["1", '基础词类'], ["2", '抽象语义词类'], ["3", '公用词类'], ["4", '专有词类']]
			      }),
			    valueField : 'id',
			    displayField : 'name'
		    }, '操作动作:', {
			    ref : 'auditedSearchWordAction',
			    xtype : 'combo',
			    triggerAction : 'all',
			    fieldLabel : '操作动作',
			    anchor : '100%',
			    mode : 'local',
			    editable : false,
			    value : "0",
			    width : 75,
			    store : new Ext.data.ArrayStore({
				      fields : ['id', 'name'],
				      data : [["0", '全部'], ["1", '新增'], ["2", '修改'], ["3", '删除']]
			      }),
			    valueField : 'id',
			    displayField : 'name'
		    }]
	  });
	this.tbarEx = tbarEx;

	var cfg = {
		region : 'center',
		frame : false,
		border : false,
		store : _store,
		stripeRows : true,
		columnLines : true,
		loadMask : true,
		columns : [new Ext.grid.RowNumberer(), {
			  header : '提交人',
			  dataIndex : 'opUser',
			  width : 60
		  }, {
			  header : '操作动作',
			  dataIndex : 'opAction',
			  renderer : function(v, meta, r) {
				  return actionGroup[v];
			  },
			  width : 50
		  }, {
			  header : '操作类别',
			  dataIndex : 'opType',
			  renderer : function(v, meta, r) {
				  return typeGroup[v];
			  },
			  width : 50
		  }, {
			  header : '父级目录',
			  dataIndex : 'parentPath',
			  width : 250
		  }, {
			  header : '新内容',
			  dataIndex : 'newContent'
		  }, {
			  header : '目标值',
			  dataIndex : 'target'
		  }, {
			  header : '状态',
			  dataIndex : 'state',
			  renderer : function(v, meta, r) {
				  if (1 == v) {
					  meta.style = 'font-weight:bold;color:green;width:60';
				  } else {
					  meta.style = 'font-weight:bold;color:red;width:60';
				  }
				  return stateGroup[v];
			  },
			  width : 60
		  }, {
			  header : '提交时间',
			  dataIndex : 'opTimeStr'
		  }, {
			  header : '审核人',
			  dataIndex : 'auditUser',
			  width : 60
		  }, {
			  header : '备注',
			  dataIndex : 'ex'
		  }, {
			  header : '组别',
			  dataIndex : 'orderName',
			  width : 60
		  }, {
			  header : '审核时间',
			  dataIndex : 'auditTimeStr'
		  }],
		tbar : [{
			  text : '搜索',
			  xtype : 'button',
			  iconCls : 'icon-search',
			  handler : function() {
				  self.search();
			  }
		  }, '-', '提交人:', {
			  ref : '../searchAuditedOperateUser',
			  xtype : 'textfield',
			  emptyText : '输入提交人搜索...',
			  listeners : {
				  specialkey : function(field, e) {
					  if (e.getKey() == Ext.EventObject.ENTER) {
						  self.search();
					  }
				  }
			  }
		  }, '审核人:', {
			  ref : '../searchAuditedAuditUser',
			  xtype : 'textfield',
			  emptyText : '输入审核人搜索...',
			  listeners : {
				  specialkey : function(field, e) {
					  if (e.getKey() == Ext.EventObject.ENTER) {
						  self.search();
					  }
				  }
			  }
		  }, '审核状态:', {
			  ref : '../searchAuditedState',
			  xtype : 'combo',
			  editable : false,
			  triggerAction : 'all',
			  valueField : 'id',
			  displayField : 'name',
			  value : '0',
			  width : 75,
			  mode : 'local',
			  store : new Ext.data.ArrayStore({
				    fields : ['id', 'name'],
				    data : [['0', '全部'], ['1', '已通过'], ['2', '已驳回']]
			    })
		  }, '审核时间:从', {
			  ref : '../searchAuditedTimeStart',
			  xtype : 'datefield',
			  emptyText : '开始时间',
			  width : 150,
			  format : 'Y-m-d',
			  editable : false,
			  listeners : {
				  specialkey : function(field, e) {
					  if (e.getKey() == Ext.EventObject.ENTER) {
						  self.search();
					  }
				  }
			  }
		  }, '到', {
			  ref : '../searchAuditedTimeEnd',
			  xtype : 'datefield',
			  width : 150,
			  emptyText : '结束时间',
			  format : 'Y-m-d',
			  editable : false,
			  listeners : {
				  specialkey : function(field, e) {
					  if (e.getKey() == Ext.EventObject.ENTER) {
						  self.search();
					  }
				  }
			  }
		  }],
		bbar : pagingBar,
		listeners : {
			'render' : function() {
				tbarEx.render(this.tbar);
			},
			'afterrender' : function(th) {

			}
		},
		viewConfig : {
			forceFit : true
		}
	}
	AuditedWordclassPanel.superclass.constructor.call(this, cfg);
}

Ext.extend(AuditedWordclassPanel, Ext.grid.GridPanel, {
	  loadData : function() {
		  var self = this;
		  var store = this.getStore();
		  store.currentPage = 1;
		  for (var key in store.baseParams) {
			  if (key && key.indexOf('queryParam.') != -1)
				  delete store.baseParams[key];
		  }
		  store.load({
			    params : {
				    start : 0,
				    limit : self.getPageSize()
			    }
		    });
	  },
	  search : function() {
		  var self = this;
		  var operateUser = self.searchAuditedOperateUser.getValue();
		  var auditUser = self.searchAuditedAuditUser.getValue();
		  var auditState = self.searchAuditedState.getValue();
		  var auditTimeStart = self.searchAuditedTimeStart.getRawValue()
		  var auditTimeEnd = self.searchAuditedTimeEnd.getRawValue();
		  var searchWord = self.tbarEx.auditedSearchWords.getValue();
		  var searchOldword = self.tbarEx.auditedSearchOldwords.getValue();
		  var wordSymbol = self.tbarEx.auditedSearchWordSymbol.getValue();
		  var wordLength = self.tbarEx.auditedSearchWordLength.getValue();
		  var wordType = self.tbarEx.auditedSearchWordType.getRawValue();
		  var wordAction = self.tbarEx.auditedSearchWordAction.getValue();
		  var store = this.getStore();
		  store.currentPage = 1;
		  for (var key in store.baseParams) {
			  if (key && key.indexOf('queryParam.') != -1)
				  delete store.baseParams[key];
		  }
		  if (operateUser)
			  store.setBaseParam('queryParam.operateUser', operateUser);
		  if (auditUser)
			  store.setBaseParam('queryParam.auditUser', auditUser);
		  if (auditState)
			  store.setBaseParam('queryParam.auditState', auditState);
		  if (auditTimeStart)
			  store.setBaseParam('queryParam.auditTimeStart', auditTimeStart);
		  if (auditTimeEnd)
			  store.setBaseParam('queryParam.auditTimeEnd', auditTimeEnd);
		  if (searchWord)
			  store.setBaseParam('queryParam.searchWord', searchWord);
		  if (searchOldword)
			  store.setBaseParam('queryParam.searchOldword', searchOldword);
		  if (wordSymbol)
			  store.setBaseParam('queryParam.wordSymbol', wordSymbol);
		  if (wordLength)
			  store.setBaseParam('queryParam.wordLength', wordLength);
		  if (wordType)
			  store.setBaseParam('queryParam.wordType', wordType);
		  if (wordAction)
			  store.setBaseParam('queryParam.wordAction', wordAction);
		  store.load();
	  }
  });