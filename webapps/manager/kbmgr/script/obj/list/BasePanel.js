/**
 * 
 * @include "/ontologybase/src/main/webapp/kbmgr/script/obj/detail/AttrEditPanel.js"
 */
Ext.namespace('obj.list')
obj.list.BasePanel = Ext.extend(obj.detail.AttrEditPanel, {
	  // init
	  disableLocalSort : true,
	  readOnly : true,
	  mode : obj.detail.AttrEditPanel.MODE.LIST,
	  CMPConfig : {
		  pageSize : 20
	  },
	  initComponent : function() {
		if (!this.proxyStore)
		  this.proxyStore = obj.detail.AttrType.createStore({
			    url : 'ontology-object-value!list.action',
			    root : 'data'
		    })
		  this.proxyStore.on('beforeload', function() {
			    var si = this.getCustomSortInfo();
			    if (si) {
				    // this.disableLocalSort = true
				    this.proxyStore.baseParams.sort = si.sort
				    this.proxyStore.baseParams.dir = si.dir
			    } else {
				    // delete this.disableLocalSort
			    }
		    }, this)
		  this.proxyStore.reader.readRecords = function(o) {
			  if (o.attachment) {
				  this.getBottomToolbar().statLabel.setText(new Ext.XTemplate('[实例]:{objCount}[属性]:{total}/{inherited}[{inherited1}|{inherited2}|{inherited3}]/{rest}' + ' [扩展问]:{allFaqCount}/{dynFaqCount}/{customFaqCount}'
				      + ' [样例]:{allSplCount}/{dynSplCount}/{customSplCount}(总数/动态/自定义)').apply(o.attachment), false)
			  }
			  return Ext.data.JsonReader.prototype.readRecords.call(this.proxyStore.reader, o);
		  }.dg(this);
		  this.getView().on('refresh', function() {
			  var si = this.getCustomSortInfo();
			  if (si)
				  this.getView().updateSortIcon(si.col, si.dir)
			   // this.getView().updateSortIcon.defer(100,this.getView(),[si.col,
			   // si.dir])
		   }, this)
		  this.bbar = this._pbar = new Ext.PagingToolbar({
			    store : this.proxyStore,
			    displayInfo : true,
			    pageSize : this.CMPConfig.pageSize,
			    prependButtons : true,
			    beforePageText : '第',
			    afterPageText : '页，共{0}页',
			    displayMsg : '第{0}到{1}条，共{2}条',
			    emptyMsg : "没有记录"
		    })
		  this._pbar.add('-', {
			    xtype : 'label',
			    ref : 'statLabel',
			    text : ''
		    })
		  obj.list.BasePanel.superclass.initComponent.call(this);

		  this.on('rowdblclick', function(grid, rowIndex, columnIndex, e) {
			    this.editObjectDetail(rowIndex);
		    })
		  this.on('customSortChange', function(sortField, sortDir, col) {
			    this.proxyStore.load();
		    }, this);
		  this.getView().updateHeaderSortState = function() {
			  // disable update sort state,we hold the control
		  }
	  },
	  editObjectDetail : function(row) {
		  if (Ext.getCmp('objectCategoryNav')) {
			  var r = this.store.getAt(row);
			  Ext.getCmp('objectCategoryNav').showObjectTab({
				    id : r.get('objectId'),
				    text : r.get('objectName'),
				    ispub : r.data.ispub,
				    cateIdPath : r.data.cateIdPath
			    }, r.get('id'))
		  }
	  },

	  // ui
	  _buildTbar : function() {
	  },
	  _buildColConfig : function() {
		  this.defaultRender = function(color, fn) {
			  return function(value, metaData, record, rowIndex, colIndex, store) {
				  var s = '';
				  metaData.attr = 'style=padding:0;height:100%;background-color:' + color + ';font-weight:bold;color:#15428B;'
				  var preR = store.getAt(rowIndex - 1);
				  if (preR != null && preR.get('objectId') == record.get('objectId'))
					  s = '<div style="position:relative;height:0;">' + '<div style="position:absolute;top:-2px;background-color:' + color + ';height:2px;width:100%"></div>' + '</div>'
				  else {
					  var st = record.get('objectStatus');
					  s = value + (st && (st & 256) == 256 ? '<img src="images/page_attach.png"></img>' : '')
				  }
				  return s;
			  }.dg(this)
		  }
		  var cols = obj.list.BasePanel.superclass._buildColConfig.call(this);
		  var objectNameCol = {
			  header : '实例名称',
			  dataIndex : 'objectName',
			  width : 70,
			  renderer : this.defaultRender('#eee'),
			  hideable : false
		  };
		  for (var i = 0; i < cols.length; i++) {
			  if (cols[i] && cols[i] instanceof Ext.grid.RowNumberer) {
				  cols = cols.slice(0, i + 1).concat([objectNameCol, {
					    header : '路径',
					    dataIndex : 'catePath',
					    renderer : this.defaultRender('white'),
					    hidden : kb_edit_allow_field_list.indexOf('路径') == -1
				    }]).concat(cols.slice(i + 1));
				  break;
			  }
		  }
		  return cols;
	  },
	  // model
	  _addParam : function(key, v) {
		  if (v != null)
			  this.proxyStore.baseParams[key] = v;
		  else
			  delete this.proxyStore.baseParams[key];
	  },
	  _addParamToQueryMap : function(queryMap, key, value) {
		  if (!queryMap)
			  queryMap = {};
		  queryMap[key] = value;
		  return queryMap;
	  },
	  _doSearchImpl : function(queryMap, filters) {
		  this.listValue(queryMap)
	  },
	  _prepareQueryParams : function(queryMap) {
		  if (this.matchExactly) {
			  queryMap.queryMatchMode = 'eq';
		  }
		  if (this.searchAllSchema) {
			  queryMap.searchSchema = 'all';
		  }
		  for (var k in queryMap) {
			  if (k.indexOf('cmd-') == 0) {
				  queryMap.cmd = k.substring(4);
				  delete queryMap[k];
			  }
		  }
		  return queryMap
	  },
	  listValue : function(queryMap, realQuery) {
		  queryMap = this._prepareQueryParams(queryMap)
		  this._addParam('data', queryMap ? Ext.encode(ux.util.resetEmptyString(queryMap)) : null);
		  if (realQuery === false)
			  return;
		  this.removeCustomSortInfo();
		  this.proxyStore.load({
			    params : {
				    start : 0,
				    limit : this.CMPConfig.pageSize
			    }
		    });
	  },
	  refresh : function() {
		  this.proxyStore.reload()
	  },
	  destroy : function() {
		  obj.list.BasePanel.superclass.destroy.call(this)
		  this.defaultRender ? delete this.defaultRender : 0
	  },
	  // search
	  reset : function() {
		  this._attrP.reset();
	  },
	  margins : '0 0 0 0',
	  border : false,
	  style : 'border-right-width:1px',
	  columnLines : true,
	  loadMask : '加载中...'
  });