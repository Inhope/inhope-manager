/**
 * 
 * @include "/ontologybase/src/main/webapp/kbmgr/script/obj/detail/AttrEditPanel.js"
 */
Ext.namespace('obj.list')
obj.list.MainPanel = Ext.extend(obj.list.BasePanel, {
	  // init
	  disableLocalSort : true,
	  readOnly : true,
	  mode : obj.detail.AttrEditPanel.MODE.LIST,
	  CMPConfig : {
		  pageSize : 20
	  },
	  initComponent : function() {
		  try {
			  this.onlyMyOwn = Dashboard.getModuleFeature('kb.obj.list.onlymyown') == 'true' ? true : false;
			  this.baseQuery = Ext.decode(Dashboard.getModuleFeature('kb.obj.list.defaultquery'));
		  } catch (e) {
		  }

		  this._fmP = new obj.detail.FormEditPanel({});
		  obj.list.MainPanel.superclass.initComponent.call(this);
	  },

	  // ui
	  _getSearchConfigs : function(excludeNonName) {
		  var sharedField = this.getSearchFieldsConfig();
		  for (var i = sharedField.length - 1; i >= 0; i--) {
			  var f = sharedField[i];
			  if (excludeNonName && !f.name || this.onlyMyOwn && f.name == 'editor' || f.name == 'dimension') {
				  sharedField.splice(i, 1);
			  }
		  }
		  return this.getSearchFields([{
			    width : 140,
			    name : 'dimension',
			    xtype : 'dimensionbox',
			    clearBtn : true,
			    menuConfig : {
				    labelAlign : 'top'
			    },
			    valueTransformer : function(value) {
				    if (value == 'ALLDIM')
					    return {
						    ALLDIM : []
					    }
				    var tags = this.tags;
				    var groupTags = {};
				    if (tags)
					    Ext.each(tags, function(t) {
						      var groupTag = groupTags[t.dimId];
						      if (!groupTag) {
							      groupTag = [];
							      groupTags[t.dimId] = groupTag;
						      }
						      groupTag.push(t.id)
					      });
				    return groupTags;
			    }
		    }, {
			    name : 'objectName',
			    label : '实例名称'
		    }, {
			    name : 'objectTag',
			    label : '实例标签'
		    }].concat(sharedField).concat([{
			    name : 'ontologyclass',
			    label : '继承类名'
		    }]));
	  },
	  _buildTbar : function() {
		  ShortcutToolRegistry.register('analyseStandQuestion', function() {
			    if (!this.anaWin)
				    this.anaWin = new AnalyseStandQuestionWin();
			    this.anaWin.loadData();
			    this.anaWin.show();
		    }.dg(this));
		  var cfg = this.getSearchFields(this._getSearchConfigs());
		  this.dimChooser = Ext.create(cfg[0]);
		  Ext.each(cfg, function(f) {
			    if (f.name)
				    f.width = 110
			    if (f.name == 'typeSelector') {
				    f.getStore().each(function(r) {
					      if (r.get('field1') == 'dimension') {
						      f.getStore().remove(r)
						      return false;
					      }
				      })
			    } else if (f.xtype == 'button') {
				    f.xtype = 'splitbutton'
				    f.menu = {
					    defaults : {
						    hideOnClick : false
					    },
					    items : [{
						      text : '只看我编辑的',
						      checked : this.onlyMyOwn,
						      disabled : this.onlyMyOwn,
						      checkHandler : function(btn, checked) {
							      this.listOfMyOwn = checked
						      }.dg(this)
					      }, {
						      text : '精确匹配',
						      checked : false,
						      checkHandler : function(btn, checked) {
							      this.matchExactly = checked
						      }.dg(this)
					      }, {
						      text : '搜索全部',
						      checked : true,
						      checkHandler : function(btn, checked) {
							      this.searchAllSchema = checked
						      }.dg(this)
					      }, {
						      text : '维度语义匹配',
						      checked : false,
						      checkHandler : function(btn, checked) {
							      this.searchSemanticDim = checked
						      }.dg(this)
					      }]
				    }
			    }
		    }, this);
		  this.searchAllSchema = true;
		  var searchFields = this.getCompactSearchFields(cfg);
		  Ext.each(searchFields, function(f) {
			    if (f.name)
				    f.width = 100
			    if (f.name == 'typeSelector') {
				    f.width = 90
			    } else if (f.name == 'typeContent') {
				    f.width = 150
			    }
		    })
		  return [this.dimChooser.label + ':', this.dimChooser].concat(searchFields).concat('-', {
			    text : isVersionStandard() ? '高级' : '高级查询',
			    iconCls : 'icon-search',
			    handler : function() {
				    if (!this._advwin) {
					    this._advwin = new obj.list.AdvValueQueryWin()
					    this._advwin.open(this._getSearchConfigs(true), this)
				    }
				    this._advwin.show();
				    // this._advwin.alignTo(this.navPanel.el, 'tl');
			    }.dg(this)
		    }, '-', {
			    text : '编辑',
			    iconCls : 'icon-table-edit',
			    handler : function() {
				    var r = this.getSelectionModel().getSelected()
				    if (r) {
					    this.editObjectDetail(this.store.indexOf(r))
				    }
			    }.dg(this)
		    }, !Dashboard.u().allow('kb.obj.catecontent.0.EXP') ? '' : {
			    text : '导出',
			    iconCls : 'icon-ontology-export',
			    ref : 'exportBtn',
			    handler : function() {
				    var queryMap = this.advSearch && this._advwin ? this._advwin.getSearchValueMap() : this.getSearchValueMap();
				    queryMap = this._prepareQueryParams(queryMap)
				    this.listValue(queryMap, false);
				    Ext.Ajax.request({
					      url : 'ontology-object-value!exportList.action',
					      params : Ext.apply({}, this.proxyStore.baseParams),
					      success : function(response, opts) {
						      if (response.responseText) {
							      var dataLoaderKey = response.responseText
							      this.navPanel.doExport(this.navPanel.getSelectedNode(), dataLoaderKey);
						      }
					      },
					      scope : this
				      });
				    // this.navPanl.doExport(this.navPanl
				    // .getSelectedNode());
			    }.dg(this)
		    }, {
			    text : '分析标准问',
			    iconCls : 'icon-participle',
			    handler : function() {
				    if (!this.anaWin)
					    this.anaWin = new AnalyseStandQuestionWin();
				    this.anaWin.loadData();
				    this.anaWin.show();
			    }
		    }, !Dashboard.u().isSupervisor() ? '' : {
			    xtype : 'splitbutton',
			    text : '推荐语义',
			    iconCls : 'icon-participle',
			    menu : {
				    width : 100,
				    plain : true,
				    items : [{
					      xtype : 'radio',
					      ref : '/deleteAllSuggest',
					      boxLabel : '删除所有',
					      name : 'suggestSemantic',
					      checked : false,
					      value : -1
				      }, {
					      xtype : 'radio',
					      ref : '/autoSuggest',
					      name : 'suggestSemantic',
					      boxLabel : '自动推荐',
					      checked : true,
					      value : 1
				      }]
			    },
			    handler : function() {
				    var option = this.deleteAllSuggest.checked ? -1 : 1;
				    Ext.Ajax.request({
					      url : 'analyse-stand-question!suggestSemantic.action',
					      params : {
						      optionValue : option
						      // -1:delet all，1:auto suggest semantic
					      },
					      success : function(resp) {
						      var timer = new ProgressTimer({
							        initData : Ext.decode(resp.responseText),
							        progressId : 'manualExecuteSuggestSemantic',
							        boxConfig : {
								        title : option == -1 ? '删除推荐语义' : '自动生成语义'
							        }
						        });
						      timer.start();
					      },
					      failure : function(resp) {
						      Ext.Msg.alert('错误', resp.responseText);
					      }
				      });
			    }
		    });
	  },
	  listValue : function(queryMap, realQuery, advSearch) {
		  if (realQuery !== false)
			  this.advSearch = advSearch;
		  return obj.list.MainPanel.superclass.listValue.apply(this, arguments)
	  },
	  _doSearch : function() {
		  if (this.getTopToolbar().exportBtn)
			  this.getTopToolbar().exportBtn.enable()
		  this.poptions ? delete this.poptions : 0
		  return obj.list.MainPanel.superclass._doSearch.apply(this, arguments)
	  },
	  categoryChanged : function(categoryId, categoryBH, ispub, options) {
		  this._addParam('categoryId', categoryId);
		  this._addParam('categoryBH', categoryBH);
		  this._addParam('ispub', ispub);
		  this.poptions = options;// panel options
		  this.listValue()
		  this.getTopToolbar().exportBtn ? this.getTopToolbar().exportBtn.disable() : 0
	  },
	  _prepareQueryParams : function(queryMap) {
		  if (!this.advSearch && queryMap) {
			  queryMap = obj.list.MainPanel.superclass._prepareQueryParams.call(this, queryMap)
			  var dimV = this.dimChooser.getValue();
			  if (dimV)
				  queryMap = this._addParamToQueryMap(queryMap, 'dimension', dimV)
			  if (this.listOfMyOwn) {
				  queryMap = this._addParamToQueryMap(queryMap, 'editor', Dashboard.u().getUsername())
			  }
			  if (this.baseQuery)
				  queryMap = Ext.applyIf(queryMap || {}, this.baseQuery)
			  if (queryMap) {
				  if (queryMap.startEditTime && queryMap.startEditTime == queryMap.endEditTime && queryMap.startEditTime.indexOf(' ') == -1) {
					  queryMap.startEditTime += ' 00:00'
					  queryMap.endEditTime += ' 23:59'
				  }
				  for (var k in queryMap) {
					  if (k && k.toLowerCase().indexOf('time') != -1 && queryMap[k].indexOf(' ') == -1) {
						  queryMap[k] += ' 00:00'
					  }
				  }
			  }
		  }
		  if (this.searchSemanticDim) {
			  queryMap = queryMap || {}
			  queryMap.searchSemanticDim = !!this.searchSemanticDim;
		  }
		  if (this.poptions) {
			  if (this.poptions.dimtagid) {
				  queryMap = this._addParamToQueryMap(queryMap, 'dimension', {
					    im_a_dimension : [this.poptions.dimtagid]
				    })
				  queryMap = this._addParamToQueryMap(queryMap, 'searchSemanticDim', 'true')
			  }
		  }
		  return queryMap;
	  },
	  destroy : function() {
		  obj.list.MainPanel.superclass.destroy.call(this)
		  this.dimChooser ? delete this.dimChooser : 0
		  this._fmP ? delete this._fmP : 0
		  if (this._advwin) {
			  this._advwin.destroy();
			  delete this._advwin
		  }
	  },
	  // search
	  reset : function() {
		  obj.list.MainPanel.superclass.reset.call(this)
		  this._fmP.getForm().reset();
	  },
	  margins : '0 0 0 0',
	  border : false,
	  style : 'border-right-width:1px',
	  columnLines : true,
	  loadMask : '加载中...'
  });