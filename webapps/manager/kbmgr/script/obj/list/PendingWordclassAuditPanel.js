Ext.namespace("PendingWordclassAuditPanel");
PendingWordclassAuditPanel = function(_cfg) {
	this.mask;
	var self = this;
	this.auditState;
	this.data = {};
	var _pageSize = 30;
	this.getPageSize = function() {
		return _pageSize;
	};

	var _store = new Ext.data.Store({
		  proxy : new Ext.data.HttpProxy({
			    url : 'wordclass!listAuditData.action'
		    }),
		  reader : new Ext.data.JsonReader({
			    idProperty : 'id',
			    root : 'data',
			    fields : [{
				      name : 'id',
				      type : 'string'
			      }, {
				      name : 'relateId',
				      type : 'string'
			      }, {
				      name : 'parentId',
				      type : 'string'
			      }, {
				      name : 'ispub',
				      type : 'string'
			      }, {
				      name : 'toCategoryId',
				      type : 'string'
			      }, {
				      name : 'domainId',
				      type : 'string'
			      }, {
				      name : 'opUser',
				      type : 'string'
			      }, {
				      name : 'opAction',
				      type : 'string'
			      }, {
				      name : 'parentPath',
				      type : 'string'
			      }, {
				      name : 'newContent',
				      type : 'string'
			      }, {
				      name : 'target',
				      type : 'string'
			      }, {
				      name : 'state',
				      type : 'string'
			      }, {
				      name : 'opTimeStr',
				      type : 'string'
			      }, {
				      name : 'opType',
				      type : 'string'
			      }, {
				      name : 'ex',
				      type : 'string'
			      }, {
				      name : 'orderName',
				      type : 'string'
			      }]
		    }),
		  writer : new Ext.data.JsonWriter()
	  });

	var pagingBar = new Ext.PagingToolbar({
		  store : _store,
		  displayInfo : true,
		  pageSize : _pageSize,
		  prependButtons : true,
		  beforePageText : '第',
		  afterPageText : '页，共{0}页',
		  displayMsg : '未审核共{2}个',
		  emptyMsg : "没有记录"
	  });

	var _syncCheckbox = new Ext.form.Checkbox({
		  boxLabel : '同步',
		  checked : Dashboard.u().allow('kb.word.AUD') ? true : false,
		  disabled : Dashboard.u().allow('kb.word.AUD') ? false : true,
		  name : 'sync',
		  flex : 5,
		  anchor : '100% 100% '
	  });
	this.syncCheckbox = _syncCheckbox;
	var tbarEx = new Ext.Toolbar({
		  items : ['词类类别:', {
			    ref : 'auditedSearchWordType',
			    xtype : 'combo',
			    triggerAction : 'all',
			    anchor : '100%',
			    mode : 'local',
			    editable : false,
			    value : "",
			    width : 75,
			    store : new Ext.data.ArrayStore({
				      fields : ['id', 'name'],
				      data : [["", "全部"], ["1", '基础词类'], ["2", '抽象语义词类'], ["3", '公用词类'], ["4", '专有词类']]
			      }),
			    valueField : 'id',
			    displayField : 'name'
		    }, '操作动作:', {
			    ref : 'auditedSearchWordAction',
			    xtype : 'combo',
			    triggerAction : 'all',
			    fieldLabel : '操作动作',
			    anchor : '100%',
			    mode : 'local',
			    editable : false,
			    value : "0",
			    width : 75,
			    store : new Ext.data.ArrayStore({
				      fields : ['id', 'name'],
				      data : [["0", '全部'], ["1", '新增'], ["2", '修改'], ["3", '删除']]
			      }),
			    valueField : 'id',
			    displayField : 'name'
		    }, {
			    text : '审核通过',
			    xtype : 'button',
			    iconCls : 'icon-save',
			    disabled : Dashboard.u().allow('kb.word.AUD') ? false : true,
			    handler : function() {
				    self.auditPass();
			    }
		    }, {
			    text : '审核驳回',
			    xtype : 'button',
			    iconCls : 'icon-delete',
			    handler : function() {
				    self.auditDismiss();
			    }
		    }, {
			    text : '一键全局通过',
			    xtype : 'button',
			    disabled : Dashboard.u().allow('kb.word.AUD') ? false : true,
			    iconCls : 'icon-save',
			    handler : function() {
				    self.auditAllPass();
			    }
		    }, {
			    text : '一键全局驳回',
			    xtype : 'button',
			    disabled : Dashboard.u().allow('kb.word.AUD') ? false : true,
			    iconCls : 'icon-delete',
			    handler : function() {
				    self.auditAllDismiss();
			    }

		    }, _syncCheckbox]
	  });

	this.tbarEx = tbarEx;

	var _sm = new Ext.grid.CheckboxSelectionModel({});
	var config = {
		region : 'center',
		style : 'border-right: 1px solid ' + sys_bdcolor,
		stripeRows : true,
		columnLines : true,
		border : false,
		store : _store,
		sm : _sm,
		loadMask : true,
		colModel : new Ext.grid.ColumnModel({
			  defaults : {
				  sortable : true
			  },
			  columns : [_sm, new Ext.grid.RowNumberer(), {
				    header : 'id',
				    dataIndex : 'id',
				    hidden : true
			    }, {
				    header : 'domainId',
				    dataIndex : 'domainId',
				    hidden : true
			    }, {
				    header : 'ispub',
				    dataIndex : 'ispub',
				    hidden : true
			    }, {
				    header : 'toCategoryId',
				    dataIndex : 'toCategoryId',
				    hidden : true
			    }, {
				    header : 'parentId',
				    dataIndex : 'parentId',
				    hidden : true
			    }, {
				    header : '提交人',
				    dataIndex : 'opUser',
				    width : 60
			    }, {
				    header : '操作动作',
				    dataIndex : 'opAction',
				    width : 60,
				    renderer : function(v, meta, r) {
					    return actionGroup[v];
				    }
			    }, {
				    header : '操作类别',
				    dataIndex : 'opType',
				    renderer : function(v, meta, r) {
					    return typeGroup[v];
				    }
			    }, {
				    header : '父级目录',
				    dataIndex : 'parentPath',
				    width : 250
			    }, {
				    header : '目标值',
				    dataIndex : 'target'
			    }, {
				    header : '新内容',
				    dataIndex : 'newContent'
			    }, {
				    header : '状态',
				    dataIndex : 'state',
				    renderer : function(v, meta, r) {
					    if (1 == v) {
						    meta.style = 'font-weight:bold;color:green';
					    } else {
						    meta.style = 'font-weight:bold;color:red';
					    }
					    return stateGroup[v];
				    },
				    width : 60
			    }, {
				    header : '组别',
				    dataIndex : 'orderName',
				    width : 60
			    }, {
				    header : '备注',
				    dataIndex : 'ex'
			    }, {
				    header : '提交时间',
				    dataIndex : 'opTimeStr'
			    }]
		  }),
		tbar : [{
			  text : '搜索',
			  xtype : 'button',
			  iconCls : 'icon-search',
			  handler : function() {
				  self.search();
			  }
		  }, '-', '提交人:', {
			  ref : '../pendingAuditSearchOperateUser',
			  xtype : 'textfield',
			  width : 80,
			  emptyText : '输入提交人搜索...',
			  disabled : Dashboard.u().allow('kb.word.AUD') ? false : true,
			  value : Dashboard.u().allow('kb.word.AUD') ? '' : Dashboard.u().getUsername(),
			  listeners : {
				  specialkey : function(field, e) {
					  if (e.getKey() == Ext.EventObject.ENTER) {
						  self.search();
					  }
				  }
			  }
		  }, '提交时间:从', {
			  ref : '../pendingAuditSearchTimeStart',
			  xtype : 'datefield',
			  emptyText : '开始时间',
			  width : 150,
			  format : 'Y-m-d',
			  editable : false,
			  listeners : {
				  specialkey : function(field, e) {
					  if (e.getKey() == Ext.EventObject.ENTER) {
						  self.search();
					  }
				  }
			  }
		  }, '到', {
			  ref : '../pendingAuditSearchTimeEnd',
			  xtype : 'datefield',
			  width : 150,
			  emptyText : '结束时间',
			  format : 'Y-m-d',
			  editable : false,
			  listeners : {
				  specialkey : function(field, e) {
					  if (e.getKey() == Ext.EventObject.ENTER) {
						  self.search();
					  }
				  }
			  }
		  }, '目标值:', {
			  ref : '../pendingAuditSearchWords',
			  xtype : 'textfield',
			  emptyText : '输入词搜索...',
			  width : 100,
			  listeners : {
				  specialkey : function(field, e) {
					  if (e.getKey() == Ext.EventObject.ENTER) {
						  self.search();
					  }
				  }
			  }
		  }, '新内容:', {
			  ref : '../pendingAuditSearchOldwords',
			  xtype : 'textfield',
			  emptyText : '输入词搜索...',
			  width : 100,
			  listeners : {
				  specialkey : function(field, e) {
					  if (e.getKey() == Ext.EventObject.ENTER) {
						  self.search();
					  }
				  }
			  }
		  }, '词长度:', {
			  ref : '../pendingAuditSearchWordSymbol',
			  xtype : 'combo',
			  editable : false,
			  triggerAction : 'all',
			  valueField : 'id',
			  displayField : 'name',
			  value : '1',
			  width : 75,
			  store : new Ext.data.ArrayStore({
				    fields : ['id', 'name'],
				    data : [["1", '大于'], ["2", '小于'], ["3", '大于等于'], ["4", '小于等于'], ["5", '等于']]
			    }),
			  mode : 'local'
		  }, {
			  ref : '../pendingAuditSearchWordLength',
			  xtype : 'numberfield',
			  hideTrigger : true,
			  allowNegative : false,
			  nanText : '请输入有效的整数',
			  emptyText : '词长度',
			  allowDecimals : false,
			  width : 45,
			  listeners : {
				  specialkey : function(field, e) {
					  if (e.getKey() == Ext.EventObject.ENTER) {
						  self.search();
					  }
				  }
			  }
		  }],
		bbar : pagingBar,
		viewConfig : {
			forceFit : true
		},
		listeners : {
			'render' : function() {
				tbarEx.render(this.tbar);
			}
		}
	}

	PendingWordclassAuditPanel.superclass.constructor.call(this, config);
	this.loadData();
};

Ext.extend(PendingWordclassAuditPanel, Ext.grid.GridPanel, {
	  loadData : function() {
		  var self = this;
		  var store = this.getStore();
		  store.currentPage = 1;
		  for (var key in store.baseParams) {
			  if (key && key.indexOf('queryParam.') != -1)
				  delete store.baseParams[key];
		  }
		  var operateUser = self.pendingAuditSearchOperateUser.getValue();
		  if (operateUser)
			  store.setBaseParam('queryParam.operateUser', operateUser);
		  store.load({
			    params : {
				    start : 0,
				    limit : self.getPageSize()
			    }
		    });
	  },
	  search : function() {
		  var self = this;
		  var operateUser = self.pendingAuditSearchOperateUser.getValue();
		  var submitTimeStart = self.pendingAuditSearchTimeStart.getRawValue()
		  var submitTimeEnd = self.pendingAuditSearchTimeEnd.getRawValue();
		  var searchWord = self.pendingAuditSearchWords.getValue();
		  var searchOldword = self.pendingAuditSearchOldwords.getValue();
		  var wordSymbol = self.pendingAuditSearchWordSymbol.getValue();
		  var wordLength = self.pendingAuditSearchWordLength.getValue();
		  var wordType = self.tbarEx.auditedSearchWordType.getRawValue();
		  var wordAction = self.tbarEx.auditedSearchWordAction.getValue();
		  var store = this.getStore();
		  for (var key in store.baseParams) {
			  if (key && key.indexOf('queryParam.') != -1)
				  delete store.baseParams[key];
		  }
		  store.currentPage = 1;
		  if (operateUser)
			  store.setBaseParam('queryParam.operateUser', operateUser);
		  if (submitTimeStart)
			  store.setBaseParam('queryParam.submitTimeStart', submitTimeStart);
		  if (submitTimeEnd)
			  store.setBaseParam('queryParam.submitTimeEnd', submitTimeEnd);
		  if (searchWord)
			  store.setBaseParam('queryParam.searchWord', searchWord);
		  if (searchOldword)
			  store.setBaseParam('queryParam.searchOldword', searchOldword);
		  if (wordSymbol)
			  store.setBaseParam('queryParam.wordSymbol', wordSymbol);
		  if (wordLength)
			  store.setBaseParam('queryParam.wordLength', wordLength);
		  if (wordType)
			  store.setBaseParam('queryParam.wordType', wordType);
		  if (wordAction)
			  store.setBaseParam('queryParam.wordAction', wordAction);
		  store.load();
	  },
	  auditPass : function() {
		  this.mask = false;
		  var rows = this.getSelectionModel().getSelections();
		  if (rows.length == 0) {
			  Ext.Msg.alert('提示', '请先选择要审核的记录！');
			  return false;
		  }
		  var self = this;
		  var isSync = this.syncCheckbox.getValue();
		  Ext.Msg.confirm('确定框', '确定要通过' + rows.length + '条记录吗？', function(sel) {
			    if (sel == 'yes') {
				    var paraData = [];
				    Ext.each(rows, function(_item) {
					      paraData.push(_item.data);
				      });
				    paraData = Ext.encode(paraData);
				    Ext.Ajax.request({
					      url : 'wordclass!auditPass.action',
					      params : {
						      data : paraData,
						      isSync : isSync
					      },
					      maskEl : self.getEl(),
					      success : function(resp) {
						      Ext.Msg.alert('提示：', '审核通过成功！');
						      self.getStore().reload();
					      },
					      failure : function(resp) {
						      Ext.Msg.alert('错误', resp.responseText);
					      }
				      });
			    }
		    });
	  },
	  auditAllPass : function() {
		  var self = this;
		  var isSync = this.syncCheckbox.getValue();
		  Ext.Msg.confirm('确定框', '确定要全部审核通过吗？', function(sel) {
			    if (sel == 'yes') {
				    Ext.Ajax.request({
					      url : 'wordclass!auditAllPass.action',
					      params : {
						      isSync : isSync
					      },
					      maskEl : self.getEl(),
					      success : function(resp) {
						      var timer = new ProgressTimer({
							        initData : Ext.decode(resp.responseText).data,
							        progressId : 'auditAllWordclassOperate',
							        boxConfig : {
								        title : '一键全局通过'
							        },
							        finish : function(p, response) {
								        self.loadData(true);
								        self.getStore().reload();
							        }
						        });
						      timer.start();
					      },
					      failure : function(resp) {
						      Ext.Msg.alert('错误', resp.responseText);
					      }
				      });
			    }
		    });
	  },
	  auditDismiss : function() {
		  this.mask = false;
		  var rows = this.getSelectionModel().getSelections();
		  if (rows.length == 0) {
			  Ext.Msg.alert('提示', '请先选择要审核的记录！');
			  return false;
		  }
		  var self = this;
		  var isSync = this.syncCheckbox.getValue();
		  Ext.Msg.confirm('确定框', '确定要驳回' + rows.length + '条记录吗？', function(sel) {
			    if (sel == 'yes') {
				    var paraData = [];
				    Ext.each(rows, function(_item) {
					      paraData.push(_item.data);
				      });
				    paraData = Ext.encode(paraData);
				    Ext.Ajax.request({
					      url : 'wordclass!auditDismiss.action',
					      params : {
						      data : paraData,
						      isSync : isSync
					      },
					      maskEl : self.getEl(),
					      success : function(resp) {
						      Ext.Msg.alert('提示：', '审核驳回成功！');
						      self.getStore().reload();
					      },
					      failure : function(resp) {
						      Ext.Msg.alert('错误', resp.responseText);
					      }
				      });
			    }
		    });
	  },
	  auditAllDismiss : function() {
		  var self = this;
		  var isSync = this.syncCheckbox.getValue();
		  Ext.Msg.confirm('确定框', '确定要全部审核驳回吗？', function(sel) {
			    if (sel == 'yes') {
				    Ext.Ajax.request({
					      url : 'wordclass!auditAllDismiss.action',
					      params : {
						      isSync : isSync
					      },
					      maskEl : self.getEl(),
					      success : function(resp) {
						      var timer = new ProgressTimer({
							        initData : Ext.decode(resp.responseText).data,
							        progressId : 'auditAllWordclassOperate',
							        boxConfig : {
								        title : '一键全局驳回'
							        },
							        finish : function(p, response) {
								        self.loadData(true);
								        self.getStore().reload();
							        }
						        });
						      timer.start();
					      },
					      failure : function(resp) {
						      Ext.Msg.alert('错误', resp.responseText);
					      }
				      });
			    }
		    });
	  }

  });