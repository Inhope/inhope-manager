var KBCommonDataStatPane = Ext.extend(Ext.grid.GridPanel, {
	  extversion : 3,
	  initComponent : function() {
		  this.tbar = [{
			    text : '刷新',
			    iconCls : 'icon-refresh',
			    handler : function() {
				    this.store.reload()
			    }.dg(this)
		    }, {
			    text : '导出',
			    iconCls : 'icon-export',
			    handler : function() {
				    if (!this.downloadIFrame) {
					    this.downloadIFrame = this.getEl().createChild({
						      tag : 'iframe',
						      style : 'display:none;'
					      })
				    }
				    this.downloadIFrame.dom.src = 'kb-statistics!exportKB'+this.statname+'.action?_t=' + new Date().getTime();
			    }.dg(this)
		    }];
		  this.store = new Ext.data.JsonStore({
			    url : 'kb-statistics!statKB'+this.statname+'.action',
			    autoLoad : true,
			    fields : ['domain_group', 'domain', //
			      'allAttrCount', 'x_classCount', 'x_attrCount', 'x_objCount', 'x_subclassCount', 'x_valueOf', //
			      'wordclassCount', 'x_faqCount0', 'x_faqCount1', 'dynFaqCountAbs', 'faqExpandCount', 'allSampleCount'],
			    root : 'data'
		    })
		  this.store.on('beforeload', function() {
			    this.getEl().mask();
		    }, this)
		  this.store.on('load', function() {
			    this.getEl().unmask();
		    }, this)
		  this.columns = [{
			    dataIndex : 'domain_group',
			    header : '分类1'
		    },{
			    dataIndex : 'domain',
			    header : '分类2'
		    }, {
			    dataIndex : 'allAttrCount',
			    header : '知识点'
		    }, {
			    dataIndex : 'x_classCount',
			    header : '本体类'
		    }, {
			    dataIndex : 'x_attrCount',
			    header : '属性'
		    }, {
			    dataIndex : 'x_objCount',
			    header : '本体实例'
		    }, {
			    dataIndex : 'x_objCount',
			    header : 'instanceof'
		    }, {
			    dataIndex : 'x_subclassCount',
			    header : 'subclassof'
		    }, {
			    dataIndex : 'x_valueOf',
			    header : 'valueof'
		    }, {
			    dataIndex : 'wordclassCount',
			    header : '词类'
		    }, {
			    dataIndex : 'x_faqCount0',
			    header : '普通扩展问'
		    }, {
			    dataIndex : 'x_faqCount1',
			    header : '语义表达式'
		    }, {
			    dataIndex : 'dynFaqCountAbs',
			    header : '抽象语义规则'
		    }, {
			    dataIndex : 'faqExpandCount',
			    header : '引擎扩展'
		    }, {
			    dataIndex : 'allSampleCount',
			    header : '测试样例'
		    }]
		  KBCommonDataStatPane.superclass.initComponent.call(this);
	  },
	  // private
	  margins : '0',
	  border : true,
	  bodyStyle : 'background-color:' + sys_bgcolor + ' padding : 0;'
  })