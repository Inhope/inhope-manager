var KBDataStatPanel = Ext.extend(Ext.grid.GridPanel, {
	  extversion : 3,
	  initComponent : function() {
		  this.tbar = [{
			    text : '刷新',
			    iconCls : 'icon-refresh',
			    handler : function() {
				    this.store.reload()
			    }.dg(this)
		    }, {
			    text : '导出',
			    iconCls : 'icon-export',
			    handler : function() {
				    if (!this.downloadIFrame) {
					    this.downloadIFrame = this.getEl().createChild({
						      tag : 'iframe',
						      style : 'display:none;'
					      })
				    }
				    this.downloadIFrame.dom.src = 'kb-statistics!exportKBDomain.action?_t=' + new Date().getTime();
			    }.dg(this)
		    }];
		  this.store = new Ext.data.JsonStore({
			    url : 'kb-statistics!statKBDomain.action',
			    autoLoad : true,
			    fields : ['domain_group', 'domain',
			      // 词类
			      'wordclassCount', 'wordCount', 'wordclassCount1', 'wordCount1',
			      // 本体类
			      'classCount', 'attrCount', 'absAttrCount', 'customClsAttrCount', 'ruleCount0', 'ruleCount1', 'ruleCountAbs', 'ruleExpandCount', 'sampleCount',
			      // 知识点
			      'objCount', 'allAttrCount', 'customAttrCount', 'inheritedAttrCount', 'inheritedAbsAttrCount', 'customFaqCount0', 'customFaqCount1', 'customFaqExpandCount1', 'dynFaqCount0',
			      'dynFaqCount1', 'dynFaqExpandCount1', 'dynFaqCountAbs', 'dynFaqExpandCountAbs', 'allSampleCount'],
			    root : 'data'
		    })
		  this.store.on('beforeload', function() {
			    this.getEl().mask();
		    }, this)
		  this.store.on('load', function() {
			    this.getEl().unmask();
		    }, this)
		  this.columns = [{
			    dataIndex : 'domain_group',
			    header : '行业'
		    }, {
			    dataIndex : 'domain',
			    header : '领域'
		    }, {
			    dataIndex : 'wordclassCount',
			    header : '同义词类'
		    }, {
			    dataIndex : 'wordCount',
			    header : '同义词'
		    }, {
			    dataIndex : 'wordclassCount1',
			    header : '同类词类'
		    }, {
			    dataIndex : 'wordCount1',
			    header : '同类词'
		    }, // 本体类
		    {
			    dataIndex : 'classCount',
			    header : '本体类'
		    }, {
			    dataIndex : 'attrCount',
			    header : '属性'
		    }, {
			    dataIndex : 'absAttrCount',
			    header : '引用抽象语义的属性'
		    }, {
			    dataIndex : 'customClsAttrCount',
			    header : '用户编写的属性'
		    }, {
			    dataIndex : 'ruleCount0',
			    header : '平均普通模板'
		    }, {
			    dataIndex : 'ruleCount1',
			    header : '平均语义表达式'
		    }, {
			    dataIndex : 'ruleCountAbs',
			    header : '平均抽象语义规则'
		    }, {
			    dataIndex : 'ruleExpandCount',
			    header : '平均模板展开数'
		    }, {
			    dataIndex : 'sampleCount',
			    header : '平均测试样例'
		    }, // 知识点
		    {
			    dataIndex : 'objCount',
			    header : '实例'
		    }, {
			    dataIndex : 'allAttrCount',
			    header : '知识点'
		    }, {
			    dataIndex : 'inheritedAttrCount',
			    header : '属性知识点'
		    }, {
			    dataIndex : 'inheritedAbsAttrCount',
			    header : '抽象语义知识点'
		    }, {
			    dataIndex : 'customAttrCount',
			    header : '自定义知识点'
		    }, {
			    dataIndex : 'customFaqCount0',
			    header : '平均自定义普通模板'
		    }, {
			    dataIndex : 'customFaqCount1',
			    header : '平均自定义肯定模板'
		    }, {
			    dataIndex : 'customFaqExpandCount1',
			    header : '平均自定义肯定模板展开数'
		    }, {
			    dataIndex : 'dynFaqCount0',
			    header : '平均动态普通模板'
		    }, {
			    dataIndex : 'dynFaqCount1',
			    header : '平均动态肯定模板'
		    }, {
			    dataIndex : 'dynFaqExpandCount1',
			    header : '平均动态肯定模板展开数'
		    }, {
			    dataIndex : 'dynFaqCountAbs',
			    header : '平均动态抽象语义规则'
		    }, {
			    dataIndex : 'dynFaqExpandCountAbs',
			    header : '平均动态抽象语义规则展开数'
		    }, {
			    dataIndex : 'faqExpandCount',
			    header : '平均模板展开数'
		    }, {
			    dataIndex : 'allSampleCount',
			    header : '平均测试样例'
		    }]
		  KBDataStatPanel.superclass.initComponent.call(this);
	  },
	  // private
	  margins : '0',
	  border : true,
	  bodyStyle : 'background-color:' + sys_bgcolor + ' padding : 0;'
  })