Ext.namespace('stat.dim')
stat.dim.MainPanel = Ext4.extend(Ext4.Panel, {
	height : 450,
	initComponent : function() {
		var self = this;
		// build UI
		this.dimChartStore = new Ext4.data.JsonStore({
					proxy : {
						type : 'ajax',
						url : 'statistics!dim.action',
						reader : {
							type : 'json',
							root : 'data'
							// idProperty : 'name'
						},
						extraParams : {}
					},
					fields : ['name', 'count'],
					autoLoad : true
				});
		var donut = false;
		this.dimChart = new Ext4.chart.Chart({
			animate : true,
			store : self.dimChartStore,
			shadow : true,
			legend : {
				position : 'right'
			},
			insetPadding : 60,
			theme : 'Base:gradients',
			series : [{
				type : 'pie',
				field : 'count',
				showInLegend : true,
				donut : donut,
				tips : {
					trackMouse : true,
					width : 240,
					height : 28,
					renderer : function(storeItem, item) {
						var total = 0;
						self.dimChartStore.each(function(rec) {
									total += rec.get('count');
								});
						var percent = '' + storeItem.get('count') / total * 100;
						var idx = percent.indexOf('.');
						if (idx != -1)
							percent = percent.substring(0, idx + 2);
						this.setTitle(storeItem.get('name') + ': ' + percent
								+ '%');
					}
				},
				highlight : {
					segment : {
						margin : 20
					}
				},
				label : {
					field : 'name',
					display : 'rotate',
					contrast : true,
					font : '18px Arial',
					renderer : function(value, label, storeItem, item, i,
							display, animate, index) {
						var total = 0;
						self.dimChartStore.each(function(rec) {
									total += rec.get('count');
								});
						var a = Math
								.round(storeItem.get('count') / total * 100);
						if (a < 5)
							return "";
						else
							return value;
					}
				}
			}]
		});
		this.items = self.dimChart;
		this.tbar = this._buildTbar();
		// event
		stat.dim.MainPanel.superclass.initComponent.call(this);
	},
	reloadChartStore : function(dimId) {
		this.dimChartStore.getProxy().setExtraParam('dimId', dimId);
		this.dimChartStore.reload();
	},
	_buildTbar : function() {
		this.combo = new Ext4.form.ComboBox({
					displayField : 'name',
					valueField : 'id',
					triggerAction : 'all',
					store : new Ext4.data.JsonStore({
								proxy : {
									type : 'ajax',
									url : 'ontology-dimension!list.action',
									reader : {
										type : 'json',
										root : 'data',
										idProperty : 'id'
									}
								},
								fields : ['id', 'name'],
								autoLoad : true
							}),
					listeners : {
						select : function(combo, r, i) {
							this.reloadChartStore(r[0].data.id);
						}.dg(this)
					}
				});
		this.combo.getStore().on('load', function() {
			if (this.combo.getStore().getCount() > 0)
		(function() {
					this.combo.setValue(this.combo.getStore().getAt(0)
							.get('id'))
					this.reloadChartStore(this.combo.getValue())
				}).defer(100, this)
		}, this)
		var top_tbar = [{
					xtype : 'label',
					text : '请选择需要统计的维度 :'
				}, this.combo, {
					enableToggle : true,
					pressed : false,
					text : '圆环图',
					toggleHandler : function(btn, pressed) {
						this.dimChart.series.first().donut = pressed
								? 35
								: false;
						this.dimChart.refresh();
					},
					scope : this
				}];
		return top_tbar;
	},
	layout : 'fit',
	border : false
});
