Ext.namespace('stat.valuetype')
stat.valuetype.MainPanel = Ext4.extend(Ext4.panel.Panel, {
	height : 450,
	initComponent : function() {
		var self = this;
		// build UI
		this.chartStore = new Ext4.data.JsonStore({
					proxy : {
						type : 'ajax',
						url : 'statistics!valueType.action',
						reader : {
							type : 'json',
							root : 'data'
							// idProperty : 'name'
						}
					},
					fields : ['name', 'count'],
					autoLoad : true
				});
		var donut = false;
		this.typeChart = new Ext4.chart.Chart({
			animate : true,
			store : self.chartStore,
			shadow : true,
			legend : {
				position : 'right'
			},
			insetPadding : 60,
			theme : 'Base:gradients',
			series : [{
				type : 'pie',
				field : 'count',
				showInLegend : true,
				donut : donut,
				tips : {
					trackMouse : true,
					width : 240,
					height : 28,
					renderer : function(storeItem, item) {
						var total = 0;
						self.chartStore.each(function(rec) {
									total += rec.get('count');
								});
						var percent = '' + storeItem.get('count') / total * 100;
						var idx = percent.indexOf('.');
						if (idx != -1)
							percent = percent.substring(0, idx + 2);
						this.setTitle(storeItem.get('name') + ': ' + percent
								+ '%');
					}
				},
				highlight : {
					segment : {
						margin : 20
					}
				},
				label : {
					field : 'name',
					display : 'rotate',
					contrast : true,
					font : '18px Arial',
					renderer : function(value, label, storeItem, item, i,
							display, animate, index) {
						var total = 0;
						self.chartStore.each(function(rec) {
									total += rec.get('count');
								});
						var a = Math
								.round(storeItem.get('count') / total * 100);
						if (a < 5)
							return "";
						else
							return value;
					}
				}
			}]
		});

		this.items = self.typeChart;
		this.tbar = [{
					text : '刷新',
					handler : function() {
						self.chartStore.reload();
					}
				}, {
					enableToggle : true,
					pressed : false,
					text : '圆环图',
					toggleHandler : function(btn, pressed) {
						self.typeChart.series.first().donut = pressed
								? 35
								: false;
						self.typeChart.refresh();
					}
				}];
		stat.valuetype.MainPanel.superclass.initComponent.call(this);
	},
	reloadChartStore : function(dimId) {
		this.chartStore.reload();
	},
	// ui
	_buildTbar : function() {
	},
	layout : 'fit',
	border : false
});
