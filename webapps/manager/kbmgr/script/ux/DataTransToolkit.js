Ext.namespace('datatrans')
/**
 * config - { baseActionUrl:'${url}', dataName:'$dataName',
 * baseProgressId:'$progressId',
 * 'import':{buildForm:function(){},getParams:function()},
 * 'export':{getParams:function()} }
 * 
 * @class
 * @extends Ext.emptyFn
 */
datatrans.DataTransToolkit = Ext.extend(Ext.emptyFn, {
	onImportSuccess: function() {
	},
	constructor : function(config) {
		this.config = config;
		if (!config.baseActionUrl || !config.dataName || !config.baseProgressId)
			throw new Error('fail to instantiate [datatrans.DataTransToolkit]: missing required properties of config(baseActionUrl,dataName,baseProgressId)')
		datatrans.DataTransToolkit.superclass.constructor.call(this)
	},
	getUrl : function(action) {
		return this.config.baseActionUrl.replace('.action', '!' + action
						+ '.action')
	},
	destroy : function() {
		if (this.importwin) {
			this.importwin.destroy()
			delete this.importwin
		}
	},
	doImport : function() {
		if (this.importwin == null) {
			var cfg = Ext.apply({
						title : this.config.dataName,
						importURL : this.getUrl('doImport'),
						progressId : this.config.baseProgressId + 'Import'
					}, this.config['import'])
			this.importwin = new datatrans.DefaultImportWin(cfg);
			this.importwin.on('importSuccess',this.onImportSuccess)
		}
		this.importwin.show();
	},
	doExport : function() {
		var cfg = this['export'] || {};
		var msgbox = Ext.Msg.show({
			title : 'Excel版本选择',
			msg : '您选择要导出的excel版本',
			buttons : {
				yes : '07',
				no : '03',
				cancel : true
			},
			fn : function(choice) {
				if (choice == 'cancel')
					return
				var params = cfg.getParams ? cfg.getParams() : {};
				// 开始下载
				Ext.Ajax.request({
					url : this.getUrl('doExport'),
					params : Ext.apply(params, {
								type : 'yes' == choice ? 'xlsx' : 'xls'
							}),
					success : function(response) {
						var timer = new ProgressTimer({
							initData : Ext.decode(response.responseText),
							progressText : 'percent',
							progressId : this.config.baseProgressId + 'Export',
							boxConfig : {
								title : '导出' + this.config.dataName
							},
							finish : function(p, response) {
								if (p.success) {
									if (!this.downloadIFrame) {
										this.downloadIFrame = Ext.getBody()
												.createChild({
															tag : 'iframe',
															style : 'display:none;'
														})
									}
									// this.downloadIFrame.dom.src =
									// 'ontology-object-data-trans!downFile.action?_t='
									this.downloadIFrame.dom.src = this
											.getUrl('downFile')
											+ '?_t=' + new Date().getTime();
								}
							},
							scope : this
						});
						timer.start();
					},
					failure : function(response) {
					},
					scope : this
				});
			},
			scope : this,
			icon : Ext.Msg.QUESTION,
			minWidth : Ext.Msg.minWidth
		});
		msgbox.getDialog().show();
	}
});
datatrans.DefaultImportWin = Ext.extend(Ext.Window, {
	getParams : function() {
		return {}
	},
	initComponent : function() {
		var _fileForm = this.formP = this._buildForm();
		var _importPanel = this._importPanel = new Ext.Panel({
			autoHeight : true,
			layout : 'fit',
			layoutConfig : {
				animate : true
			},
			items : [_fileForm],
			buttons : [{
				text : "开始导入",
				handler : function() {
					// _fileForm.find('name', 'importFile')[0].getValue()
					if (!_fileForm.getForm().isValid()) {
						Ext.Msg.alert("错误提示", "表单未填写完整");
					} else {
						// 开始导入
						_fileForm.getForm().submit({
									url : this.importURL,
									params : this.getParams(),
									success : function(form, action) {
										this.pgTimer = new ProgressTimer({
													initData : action.result.data,
													progressText : 'percent',
													progressId : this.progressId,
													boxConfig : {
														title : this.title
													},
													poll : this.progressPoll,
													finish : this.finishCb,
													scope : this
												});
										this.pgTimer.start();
									},
									failure : function(form, action) {
										Ext.MessageBox.hide();
										Ext.Msg.alert('导入失败', '文件上传失败');
									},
									scope : this
								});
					}
				}.dg(this)
			}, {
				text : "关闭",
				handler : function() {
					this.hide();
				}.dg(this)
			}]
		});
		this.items = [this._importPanel]
		datatrans.DefaultImportWin.superclass.initComponent.call(this)
	},
	doImport : function(cateId, ispub) {
		this.cateId = cateId ? cateId : '';
		this.ispub = ispub;
		this.show();
	},

	_buildForm : function() {
		return new Ext.FormPanel({
					frame : true,
					border : false,
					autoHeight : true,
					waitMsgTarget : true,
					defaults : {
						bodyStyle : 'padding:10px'
					},
					margins : '0 0 0 0',
					labelAlign : "left",
					labelWidth : 50,
					fileUpload : true,
					items : [{
								xtype : 'fieldset',
								ref : 'fileFieldset',
								title : '选择文件',
								autoHeight : true,
								items : [{
											name : 'file',
											xtype : "textfield",
											fieldLabel : '文件',
											inputType : 'file',
											anchor : '96%',
											allowBlank : false
										}]
							}]
				});
	},
	importURL : 'ontology-object-data-trans!doImport.action',
	progressId : 'objectImport',
	finishCb : function(p, response) {
		if (p.currentCount == -2) {
			if (this.pgTimer.msgbox)
				this.pgTimer.msgbox.hide()
			Ext.Msg.updateProgress(0, '', p.message);
			if (!this.downloadIFrame) {
				this.downloadIFrame = this.getEl().createChild({
							tag : 'iframe',
							style : 'display:none;'
						})
			}
			this.downloadIFrame.dom.src = this.importURL.replace(/\![^.]+/i,
					'!downFile')
					+ '?_t=' + new Date().getTime();
		} else if (p.currentCount == -1) {
			// Ext.Msg.alert('导入失败', p.message);
		} else if (p.currentCount == 100) {
			this.fireEvent('importSuccess');
		}
		delete this.pgTimer;
	},
	width : 520,
	autoHeight : true,
	defaults : {
		border : false
	},

	plain : true,// 方角 默认
	modal : true,
	plain : true,
	shim : true,
	closeAction : 'hide',
	closable : true, // 关闭
	constrainHeader : true
})