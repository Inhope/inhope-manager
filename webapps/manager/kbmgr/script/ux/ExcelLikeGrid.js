ExcelLikeGrid = Ext.extend(Ext.grid.EditorGridPanel, {

	postEditValue : function(value, startValue, r, field) {
		if (this.readOnly) {
			return ExcelLikeGrid.superclass.startEditing.apply(this, arguments);
		}
		if (this.onPostEditValue) {
			this.onPostEditValue(value, startValue, r, field)
		}
		return this.autoEncode && Ext.isString(value) ? Ext.util.Format.htmlEncode(value) : value;
	},
	
	// private
    preEditValue : function(r, field){
        var value = r.data[field];
        return this.autoEncode && Ext.isString(value) ? Ext.util.Format.htmlDecode(value) : value;
    },

	batchAdd : function(n) {
		var s = this.getStore();
		var c = s.getTotalCount();
		for (var i = 0; i < n; i++) {
			s.add(new s.recordType({}, Ext.id()));
		}
	},

	findMostLeftEditCol : function() {
		var cm = this.getColumnModel()
		var cnt = cm.getColumnCount()
		for (var i = 0; i < cnt; i++) {
			var ed = cm.getCellEditor(i, 0)
			if (ed && ed.isKBEditable())
				return i;
		}
		return -1;
	},
	findMostRightEditCol : function() {
		var cm = this.getColumnModel()
		var cnt = cm.getColumnCount()
		for (var i = cnt - 1; i >= 0; i--) {
			var ed = cm.getCellEditor(i, 0)
			if (ed && ed.isKBEditable())
				return i;
		}
		return -1;
	},

	handlePaste : function(grid, startRow, startCol) {
		var store = grid.getStore()
		var fcb = this.fakeClipboard
		fcb.listen(function(value) {
					var cm = grid.getColumnModel().config
					var idarray = []
					for (var i = startCol; i < cm.length; i++) {
						if (cm[i].editor)
							idarray.push(cm[i].dataIndex);
					}
					var data = fcb.decodeExcel(value)
					for (var i = 0; i < data.length; i++) {
						var row = data[i]
						var record = store.getAt(startRow + i)
						if (!record) {
							record = new store.recordType({}, Ext.id())
							store.add(record);
						}
						var nEdit = 0;
						var end = Math.min(row.length, idarray.length)
						for (var j = 0; j < end; j++) {
							var v = row[j]
							if (v)
								record.set(idarray[j], v);
						}
					}
				});
	},
	initEvents : function(){
        Ext.grid.EditorGridPanel.superclass.initEvents.call(this);

        //this.getGridEl().on('mousewheel', this.stopEditing.createDelegate(this, [true]), this);
        this.on('columnresize', this.stopEditing, this, [true]);

        if(this.clicksToEdit == 1){
            this.on("cellclick", this.onCellDblClick, this);
        }else {
            var view = this.getView();
            if(this.clicksToEdit == 'auto' && view.mainBody){
                view.mainBody.on('mousedown', this.onAutoEditClick, this);
            }
            this.on('celldblclick', this.onCellDblClick, this);
        }
    },
	stopEditing : function(cancel){
        if(this.editing){
        	this.editing = false;
            // Store the lastActiveEditor to check if it is changing
            var ae = this.lastActiveEditor = this.activeEditor;
            if(ae){
                ae[cancel === true ? 'cancelEdit' : 'completeEdit']();
//                this.view.focusCell(ae.row, ae.col);
            }
            this.activeEditor = null;
        }
    },
	startEditing : function(row, col) {
		if (this.readOnly) {
			return ExcelLikeGrid.superclass.startEditing.apply(this, arguments);
		}
		this.stopEditing();
		if (this.colModel.isCellEditable(col, row)) {
			this.view.ensureVisible(row, col, true);
			var r = this.store.getAt(row), field = this.colModel
					.getDataIndex(col), e = {
				grid : this,
				record : r,
				field : field,
				value : r.data[field],
				row : row,
				column : col,
				cancel : false
			};
			if (this.fireEvent("beforeedit", e) !== false && !e.cancel) {
				var ed = this.colModel.getCellEditor(col, row);
				if (!ed) {
					return;
				}
				if (!ed.rendered) {
					ed.parentEl = this.view.getEditorParent(ed);
					ed.on({
								scope : this,
								render : {
									fn : function(c) {
										c.field.focus(false, true);
									},
									single : true,
									scope : this
								},
								specialkey : function(field, e) {
									this.getSelectionModel().onEditorKey(field,
											e);
								},
								complete : this.onEditComplete,
								canceledit : this.stopEditing.createDelegate(
										this, [true])
							});
				}
				Ext.apply(ed, {
							row : row,
							col : col,
							record : r
						});
				ed.beforeedit_event = e;
				this.lastEdit = {
					row : row,
					col : col
				};
				this.activeEditor = ed;
				// Set the selectSameEditor flag if we are reusing the same
				// editor again and
				// need to prevent the editor from firing onBlur on itself.
				ed.selectSameEditor = (this.activeEditor == this.lastActiveEditor);
				var v = this.preEditValue(r, field);
				ed.startEdit(this.view.getCell(row, col).firstChild, Ext
								.isDefined(v) ? v : '');

				// Clear the selectSameEditor flag
				if (!ed._task)
					ed._task = new Ext.util.DelayedTask();
				ed._task.cancel();
				ed._task.delay(500, function() {
							delete ed.selectSameEditor;
						})
			}
		}
	},

	_delayLoadSubGrid : function(row) {
		if (this.subgridLoader) {
			this.defaultSelected = row;
			if (!this._task)
				this._task = new Ext.util.DelayedTask();
			this._task.cancel();
			var r = this.getStore().getAt(row);
			this._task.delay(150, this.subgridLoader.load,
					this.subgridLoader.scope, [r])
		}
	},

	initComponent : function() {

		if (this.readOnly) {
			return ExcelLikeGrid.superclass.initComponent
					.apply(this, arguments);
		}

		var self = this;

		this._completeEdits = [];

		Ext.grid.EditorGridPanel.superclass.initComponent.call(this);
		if (this.initFunc){
			this.initFunc.call(this,this)
		}
		this.getSelectionModel().on("rowselect", function(grid, row, e) {
					this._delayLoadSubGrid(row)
				}, this);

		this.on("cellclick", function(grid, row, col, e) {

					var btn = e.getTarget('._deleteButton');
					if (btn) {
						this.getStore().removeAt(row);
						if (col > 0) {
							this.getSelectionModel().selectRow(row)
							this.defaultSelected = null
							col = 0;
						} else {
							e.stopEvent();
							return false;
						}
					}
					var c = this.getColumnModel().getColumnAt(col);
					var ed = typeof c.getCellEditor == 'function' ? c
							.getCellEditor(row) : null;
					if (ed && ed.isKBEditable()) {
						if (this.pasteable)
							this.handlePaste(this, row, col);
						else
							this.inputDetector.focus();
					}

				});

		this.addEvents('scrolltopclick', 'scrollbottomclick',
				'scrollleftclick', 'scrollrightclick', 'shiftleft',
				'shiftright');

		this.on("afterrender", function(e) {
			var c = this.getEl().select("div[class=x-grid3-scroller]").first();
			if (c)
				this.scroller = c

			var cm = this.getColumnModel()
			var ccnt = cm.getColumnCount()
			for (var i = 0; i < ccnt; i++) {
				var ed = null
				try {
					ed = cm.getCellEditor(i, 0)
				} catch (e) {
				}
				if (ed) {

					if (window.XAnsEditor && ed.field instanceof XAnsEditor) {
		(function		(i) {
							var tmp = null
							if (this._startEdit) {
								tmp = this.startEdit
								this.startEdit = this._startEdit
							}

							try {
								self.startEditing(0, i)
							} catch (ex) {
							}
							try {
								self.stopEditing(true)
								self.getView().refreshRow(0)
							} catch (ex) {
							}

							if (tmp) {
								this.startEdit = tmp;
							}
						}).defer(600, ed, [i])
					}

					if (!ed.isKBEditable)
						ed.isKBEditable = function() {
							return this.field.getSelection || this.field.KBEditable
						};
					if (!ed.isKBEditable())
						continue;

					ed.columnNum = i;
					if (ed.field instanceof Ext.form.TriggerField) {
						ed.isTriggerField = true
						ed.isDisableEdit = ed.field.editable ? false : true
					}

					ed.completeOnEnter = false
					ed.cancelOnEsc = false
					if (ed.field instanceof Ext.form.TextArea) {
						ed.field.enterIsSpecial = true
						ed.field.insertAtCursor = function(html, text) {
							if (Ext.isIE) {
								document.selection.createRange().text = text;
							} else {
								var d = this.getEl().dom
								var c = d.selectionStart
								this
										.setValue(d.value.substr(0,
												d.selectionStart)
												+ text
												+ d.value
														.substr(d.selectionEnd));
								this.selectText(c + 1, c + 1)
							}
						}
					}

					ed.on('startedit', function(boundEl, value) {
								if (value && Ext.isString(value)
										&& value.length > 0) {
									if (this.field.selectText)
										this.field.selectText(value.length)
									else if (this.field.moveCursorToEnd)
										this.field.moveCursorToEnd()
								}
							})
					ed.startEdit0=ed.startEdit
					ed._startEdit = function(){
						var e = this.beforeedit_event
						if(e)
						e._field = this.field
						if (e && (self.fireEvent("beforeedit1", e) === false || e.cancel)) {
							return
						}
						self.editing = true;
						this.startEdit0.apply(this,arguments)
					}
					ed.startEdit = function(el, value) {
						if(self.codeValueEditing){
						  this._startEdit(el,value);
						  return;
						}
						this._el = el
						this._value = value
						this.boundEl = Ext.get(el);
						self.realEditor = this
						var fakeEditor
						if (!self.fakeEditor) {
							fakeEditor = self.fakeEditor = self.scroller
									.createChild({
										tag : 'div',
										style : "position:absolute;border:2px solid black;background-image:url(images/transparent.png);_background-image:url(images/transparent.gif);"
									})
							fakeEditor.unselectable()
							fakeEditor.on('click', function(event) {
										if (self.inputDetector)
											self.inputDetector.focus();
									})
							fakeEditor.on('dblclick', function(event) {
										this.hide()
										self.realEditor._startEdit(
												self.realEditor._el,
												self.realEditor._value)
										event.stopEvent();
										return false;
									})
							fakeEditor.on('contextmenu', function(event, dom, options ) {
										self.fireEvent('cellcontextmenu',self,self.lastEdit.row,self.lastEdit.col,event)
										event.stopEvent();
										return false;
									})
						} else {
							fakeEditor = self.fakeEditor
						}

						if (window._activeFakeEditor)
							window._activeFakeEditor.hide()

						window._activeFakeEditor = fakeEditor;

						var bel = this.boundEl.findParent('td', 5, true)
						var sz = bel.getSize()
						fakeEditor.setSize(sz.width, sz.height);
						fakeEditor.alignTo(bel, this.alignment, this.offsets);
						fakeEditor.show()
						if (self.pasteable)
							self.handlePaste.defer(10, self, [self, this.row,
											this.columnNum]);
						else
							self.inputDetector.focus.defer(10,
									self.inputDetector)
					}
					ed.__completeEdit = ed.completeEdit
					ed.completeEdit = function(remainVisible) {
						if (this.editing)
							this.__completeEdit(remainVisible)
					}
					ed.__cancelEdit = ed.cancelEdit
					ed.cancelEdit = function(remainVisible) {
						if (this.editing)
							this.__cancelEdit(remainVisible)
					}

					ed.onnavkey = function(fd, e) {

						var col = this.columnNum
						var row = this.row;

						var sel = null;

						if (fd && this.isKBEditable())
							sel = fd.getSelection();
						if (e.getKey() == e.LEFT) {
							if (!fd || this.isDisableEdit
									|| (sel && sel.selectionStart == 0)) {
								while ((--col) >= 0) {
									var _ed = cm.getCellEditor(col, row)
									if (_ed && _ed.isKBEditable())
										break;
								}
								if (col != -1) {
									self.startEditing(row, col)
									e.stopEvent();
									return false;
								} else {
									self.fireEvent('shiftleft')
									e.stopEvent();
									return false;
								}
							}
						} else if (e.getKey() == e.RIGHT) {
							if (!fd
									|| this.isDisableEdit
									|| (sel && sel.selectionStart == sel.wholeText.length)) {
								while ((++col) < ccnt) {
									var _ed = cm.getCellEditor(col, row)
									if (_ed && _ed.isKBEditable())
										break;
								}
								if (col != ccnt) {
									self.startEditing(row, col)
									e.stopEvent();
									return false;
								} else {
									self.fireEvent('shiftright')
									e.stopEvent();
									return false;
								}
							}
						} else if (e.getKey() == e.UP) {
							if (!fd
									|| this.isDisableEdit
									|| (sel && sel.beforeText.indexOf('\n') == -1)) {
								if (row > 0) {
									var targetRow = (self.getPrevRow) ? self
											.getPrevRow(row, col) : row - 1
									if (self.selectRow)
										self.selectRow(targetRow)
									else
										self.getSelectionModel()
												.selectRow(targetRow)
									self.startEditing(targetRow, col);
									e.stopEvent();
									return false;
								}
							}
						} else if (e.getKey() == e.DOWN) {
							if (fd && this.isTriggerField)
								return
							if (!fd || sel && sel.afterText.indexOf('\n') == -1) {
								if (row + 1 < self.getStore().getCount()) {
									var targetRow = (self.getNextRow) ? self
											.getNextRow(row, col) : row + 1
									if (self.selectRow)
										self.selectRow(targetRow)
									else
										self.getSelectionModel()
												.selectRow(targetRow)
									self.startEditing(targetRow, col);
									e.stopEvent();
									return false;
								}
							}
						} else if (e.getKey() == e.ESC) {
							if (fd) {
								this.cancelEdit()
								this.startEdit(this._el, this._value)
							}
						} else if (e.getKey() == e.ENTER) {
							if (e.ctrlKey) {
								if (fd)
									self.startEditing.defer(10, self,
											[row, col])
							} else if (!fd && e.shiftKey) {
								if (row > 0) {
									var targetRow = (self.getPrevRow) ? self
											.getPrevRow(row, col) : row - 1
									if (self.selectRow)
										self.selectRow(targetRow)
									else
										self.getSelectionModel()
												.selectRow(targetRow)
									self.startEditing(targetRow, col);
								}
							} else if (!self.fakeEditor
									|| !self.fakeEditor.isVisible()) {
								return true;
								// if (fd && fd.insertAtCursor)
								// fd.insertAtCursor('<br>', '\n')
							} else {
								if (row + 1 < self.getStore().getCount()) {
									var targetRow = (self.getNextRow) ? self
											.getNextRow(row, col) : row + 1
									if (self.selectRow)
										self.selectRow(targetRow)
									else
										self.getSelectionModel()
												.selectRow(targetRow)
									self.startEditing(targetRow, col);
								}
							}
							e.stopEvent();
							return false;
						}
					}

					if (ed.field.events['_specialkey'])
						ed.field.on('_specialkey', ed.onnavkey, ed);
					else
						ed.field.on('specialkey', ed.onnavkey, ed);
				}

			}

			this.inputDetector = this.getGridEl().createChild({
				tag : 'textarea',
				style : 'position:absolute;width:0px;height:0px;left:0px;top:0px;border:0;margin:0;'
			})

			var inputFn = function(event) {
				if (event.ctrlKey)
					return;
				(function() {
					if (!Ext.isEmpty(this.dom.value)
							&& this.dom.value.indexOf('\n') == -1
							&& self.fakeEditor && self.fakeEditor.isVisible()) {
						var ed = self.realEditor
						ed.hide()
						ed._startEdit(ed._el, this.dom.value)
						ed.startValue = ed._value
					}
					if (!Ext.isEmpty(this.dom.value))
						this.dom.value = ''
				}).defer(10, this)
			}

			this.inputDetector.on("input", inputFn)
			this.inputDetector.on("propertychange", inputFn)

			this.fakeClipboard = new FakeClipboard(this.inputDetector)

			var sm = this.getSelectionModel();
			if (sm.rowNav) {
				sm.rowNav._doRelay = sm.rowNav.doRelay
				sm.rowNav.doRelay = function(e, h, hname) {
					if (!self.fakeEditor || !self.fakeEditor.isVisible())
						this._doRelay(e, h, hname)
					return true;
				}
			}
			this.getGridEl().on('keydown', function(e) {
						var ed = self.realEditor
						var edf = self.fakeEditor
						if (ed && edf && edf.isVisible()) {
							if (e.getKey() == 113) {// F2 KEY
								edf.hide()
								ed._startEdit(ed._el, ed._value)
							} else {
								ed.onnavkey(null, e)
							}
						}
					})

			this.getView().on('rowupdated', function() {
						if (self.fakeEditor)
							self.fakeEditor.hide()
					})
			this.getView().on('refresh', function() {
						if (self.fakeEditor)
							self.fakeEditor.hide()
					})

		})

		this.on("mousedown", function(e) {

			var c = this.scroller
			if (!c)
				return

			var last = this._lastFire
			var curr = new Date().getTime()
			if (last && curr - last < 100)
				return;
			this._lastFire = curr

			var x = c.getX() + c.dom.clientWidth;
			var y = c.getY() + c.dom.clientHeight;
			var xe = e.getPageX();
			var ye = e.getPageY();
			if (xe > x && xe < x + Ext.getScrollBarWidth()
					&& ye > y - Ext.getScrollBarWidth() && ye < y) {
				if (c.dom.scrollTop + c.dom.clientHeight >= c.dom.scrollHeight) {
					this.fireEvent('scrollbottomclick');
				}
			}
		});
		if (this.selectLastEditRowField){
			this.on('afteredit',function(e){
				this._lastEditRowValue = e.record.get(this.selectLastEditRowField)
			})
			this.getView().on('refresh', function() {
				if (this._lastEditRowValue){
					var rowindex = this.getStore().find(this.selectLastEditRowField,this._lastEditRowValue);
					if (rowindex != -1){
						this.getSelectionModel().selectRow(rowindex);
						var row = Ext.fly(this.getView().getRow(rowindex))
						var xyScrollOffset = row.getOffsetsTo(this.getView().mainBody);
						this.getView().mainBody.up('.x-grid3-scroller').scrollTo('top', xyScrollOffset[1]-100, true);
					}
					delete this._lastEditRowValue
				}
			},this)
		}
	}

});

DeleteButtoner = Ext.extend(Object, {
	header : '操作',
	sortable : false,
	dataIndex : '',
	width : 40,
	renderer : function(value, metaData, record, rowIndex, colIndex, store) {
		var fds = store.fields
		var ret = ''
		fds.each(function(fd) {
			var v = null
			if (Ext.isString(fd))
				v = record.get(fd)
			else
				v = record.get(fd.name)
			if (v) {
				ret = this.isDeleteable && this.isDeleteable(value, metaData, record, rowIndex, colIndex, store) === false?"":"<a class='_deleteButton' href='javascript:void(0);' onclick='javscript:return false;'>删除</a>";
				return false;
			}
		},this);
		return ret;
	},
	constructor : function(config) {
		Ext.apply(this, config);
	}
});
