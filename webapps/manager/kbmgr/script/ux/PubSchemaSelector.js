var PubSchemaSelector = Ext.extend(Ext.form.ComboBox, {
			getSchemas : function() {
				return this.schemas;
			},
			setFirstSchema : function() {
				this.setValue(this.schemas[0]);
			},
			setValue : function(v) {
				return PubSchemaSelector.superclass.setValue.call(this, v)
			},
			initComponent : function() {
				this.schemas = []
				PubSchemaSelector.superclass.initComponent.call(this);
				Ext.Ajax.request({
							url : 'property!get.action?key=kbmgr.pubtable.schema',
							success : function(response) {
								var d = response && response.responseText
										? response.responseText
										: '';
								if (!d)
									d = 'PUB';
								d = d.split(',')
								this.schemas = [].concat(d);
								var data = []
								for (var i = 0; i < d.length; i++) {
									data.push([d[i]]);
								}
								this.store.loadData(data);
							},
							failure : function(response) {
								Ext.Msg.alert("错误", '加载公共库配置失败');
							},
							scope : this
						});
			},
			store : new Ext.data.ArrayStore({
						idIndex : 0,
						fields : ['id']
					}),
			hiddenName : 'ispub',
			fieldLabel : '数据库',
			anchor : '100%',
			typeAhead : true,
			mode : 'local',
			triggerAction : 'all',
			emptyText : '请选择...',
			selectOnFocus : true,
			valueField : 'id',
			displayField : 'id',
			editable : false
		})