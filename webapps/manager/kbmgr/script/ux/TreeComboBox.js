TreeComboBox = function(cfg) {
	var self = this;
	this.NodeRecord = Ext.data.Record.create([{
				name : 'id'
			}, {
				name : 'path'
			}]);
	cfg = cfg || {}
	Ext.apply(cfg, {
				triggerAction : 'all',
				mode : 'local',
				valueField : 'id',
				displayField : 'path',
				store : new Ext.data.SimpleStore({
							fields : ['id', 'path'],
							data : [[]]
						})
			});
	TreeComboBox.superclass.constructor.call(this, cfg);
}
TreeComboBox.FIELD_ID = 'id'
TreeComboBox.FIELD_PATH = 'path'
Ext.extend(TreeComboBox, Ext.form.ComboBox, {
	getNodePath : function(node) {
		return node.getPath('text').replace('/root', '') + '/';
	},
	lastSelectedNode : null,
	initList : function() {
		if (this.list) {
			return;
		}
		this.list = new Ext.Layer({
					cls : 'x-combo-list',
					constrain : false
				});
		var combo = this;
		var treeTbar = new Ext.Toolbar({
					items : [{
								text : '清空',
								iconCls : 'icon-refresh',
								handler : function() {
									combo.setValueEx();
								}
							}, {
								text : '刷新',
								iconCls : 'icon-refresh',
								handler : function() {
									tree.root.reload();
								}
							}, /*
								 * '-', { text : '设为词典', iconCls :
								 * 'icon-wordclass-root', handler : function() {
								 * combo.setValueEx(tree.getRootNode());
								 * combo.collapse(); } },
								 */'-', {
								text : '确定',
								iconCls : 'icon-status-ok',
								handler : function() {
									var node = tree.getSelectionModel()
											.getSelectedNode();
									if (!node) {
										alert("请先选择节点!");
										return;
									}
									combo.lastSelectedNode = node;
									combo.setValueEx(node);
									combo.collapse();
								}
							}, {
								text : '关闭',
								iconCls : 'icon-status-cancel',
								handler : function() {
									combo.collapse();
								}
							}]
				});
		this.treeConfig = Ext.applyIf(this.treeConfig || {}, {
					border : false,
					root : this.root,
					rootVisible : this.rootVisible,
					loader : this.loader,
					renderTo : this.list,
					animate : true,
					lines : false,
					bodyCfg : {
						style : 'overflow:auto;background-color:#ffffff; height:'
								+ this.minHeight
								+ 'px; height:'
								+ this.minHeight + 'px;'
					},
					tbar : treeTbar,
					listeners : {
						'dblclick' : function(node) {
							combo.lastSelectedNode = node;
							combo.setValueEx(node);
							combo.collapse();
						},
						'beforeload' : function(node) {
							var _beforeload = combo.beforeload;
							if (_beforeload && typeof _beforeload == 'function')
								_beforeload(node);
						}
					}
				})
		var tree = new Ext.tree.TreePanel(this.treeConfig);
		this.tree = tree;
		this.innerList = this.list.createChild();
		if (this.listWidth)
			this.list.setWidth(this.listWidth);
//			this.innerList.setWidth(this.listWidth);
	},
	reload : function() {
		if (this.tree)
			this.tree.root.reload()
	},
	setValueEx : function(node) {
		// var _rec = new this.NodeRecord({
		// 'id' : node.id,
		// 'path' : this.getNodePath(node)
		// });
		// this.getStore().add(_rec);
		// TreeComboBox.superclass.setValue.call(this, node.id);
		if (node) {
			var path = '';
			if (node.path)
				path = node.path;
			else if (node.getPath) {
				path = this.getNodePath(node);
			}
			node = {
					id : node.id,
					path : path
				}
			this.setValue(node);
		}else{
			this.reset()
			if (this.tree) this.tree.root.reload();
		}
		this.fireEvent('valueChanged', node, this);
	},
	_presetValue : null,
	/**
	 * 
	 * @param {id,path}
	 *            node
	 */
	reset : function() {
		this.value = null;
		TreeComboBox.superclass.setRawValue.call(this, '');
		this.dataNode = null;
	},
	setValue : function(node) {
		if (!node) {
			TreeComboBox.superclass.setRawValue.call(this, node);
			this.value = null;
			for (var key in this.dataNode) {
				delete this.dataNode[key];
			}
		} else if (typeof node == 'object') {
			if (this.rendered) {
				TreeComboBox.superclass.setRawValue.call(this, node.path);
				if (this.dataNode) {
					this.dataNode.id = node.id
					this.dataNode.path = node.path
				} else {
					this.dataNode = node;
				}
				this.value = node.id;
			} else {
				var v = this._presetValue;
				this._presetValue = node;
				if (!v) {
					this.on('afterrender', function() {
								this.setValue(this._presetValue)
							}, this)
				}

			}
		}
	},
	getValue : function() {
		return this.value == '0' ? null : this.value;
	},
	doQuery : function() {
		this.expand();
	},
	collapse : function() {
		if (this.autoCollapse) {
			tree.collapseAll();
		}
		TreeComboBox.superclass.collapse.call(this);
	}
});