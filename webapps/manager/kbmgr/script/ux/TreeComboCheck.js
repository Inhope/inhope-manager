var CheckTreeLoader = Ext.extend(Ext.tree.TreeLoader, {
			strictCheckable:false,
			createNode : function(attr) {
				this.checkIds && this.checkIds[attr.id]
						? attr.checked = true
						: (!this.strictCheckable?attr.checked = false:0);
				return CheckTreeLoader.superclass.createNode.apply(this,
						arguments);
			}
		})
var TreeComboCheck = Ext.extend(TreeComboBox, {
			emptyText : '请选择...',
			editable : false,
			allowBlank : true,
			minHeight : 250,
			expandCheckedAfterRender : true,
			expand : function() {
				var tree = this.tree;
				TreeComboCheck.superclass.expand.call(this);
				if (tree) {
					this.expandCheckedNode(tree);
				}
			},
			initComponent : function() {
				this.root = {
					id : '0',
					text : 'root'
				}
				var self = this;
				var treeListeners = {
					afterrender : function(tree) {
						self.expandCheckedNode(tree)
					},
					checkchange : function(node, checked) {
						if (self.reseting)
							return;
						var v = self.valueTransformer(node);
						if (v) {
							if (checked) {
								self.getValue().push(v);
							} else {
								Ext.each(self.getValue(), function(_v, index) {
											if (_v.id == node.id) {
												self.getValue()
														.splice(index, 1);
												return false;
											}
										})
							}
							self.updateUIText(v, checked);
						}
					}
				};
				if (!this.expandCheckedAfterRender) {
					delete treeListeners.afterrender
				}
				this.treeConfig = Ext.applyIf(this.treeConfig || {}, {
							listeners : treeListeners
						})
				if (this.treeConfig.dataUrl)
					this.dataUrl = this.treeConfig.dataUrl;
				if (this.dataUrl)
					this.loader = new CheckTreeLoader({
								dataUrl : this.dataUrl
							});
				delete this.treeConfig.dataUrl
				delete this.dataUrl
				if (!this.valueTransformer)
					this.valueTransformer = function(node) {
						return {
							id : node.id,
							name : node.text,
							path : node.getPath('text').replace('/root', '')
						}
					}
				TreeComboCheck.superclass.initComponent.call(this);
			},
			getValue : function() {
				if (!this.value) {
					this.value = [];
				}
				return this.value;
			},
			updateUIText : function(v, checked) {
				this.onValueChange(this.value, v, checked);
				var s = '';
				Ext.each(this.getValue(), function(cls) {
							s += cls.path + ' , ';
						})
				if (s) {
					s = s.substring(0, s.length - 2);
				}
				TreeComboCheck.superclass.setRawValue.call(this, s);
				this.getEl() && this.getEl().dom ? this.getEl().dom.setAttribute('ext:qtip',s) : 0
			},
			onValueChange : function() {
			},// function(values){}
			setValue : function(vs) {
				if (typeof vs == 'string')
					return

				this.value = vs;
				if (vs) {
					this.updateUIText();
					this.loader.checkIds = {};
					this.value = vs;
				} else {
					this.onValueChange();
				}
			},
			reset : function() {
				try {
					this.reseting = true;
					if (this.value && this.tree) {
						Ext.each(this.tree.getChecked(), function(n) {
									if (n.getUI().isChecked())
										n.getUI().toggleCheck(false);
								}, this)
						this.setValue([]);
					}
				} finally {
					this.reseting = false;
				}
				this.value = [];
			},
			// expand & check
			doQuery : function() {
				this.expand();
			},
			expandNonExistNode : function(v) {
				var expandPath = '/0';
				if (v.idPath) {
					this.loader.checkIds[v.id] = true
					var arr = v.idPath.split('/');
					for (var i = arr.length - 1; i >= 0; i--) {
						if (!arr[i])
							arr.splice(i, 1);
					}
					if (arr.length > 1) {
						arr.length = arr.length - 1
						expandPath = '/0/' + arr.join('/')
					}
				}
				return expandPath;
			},
			expandCheckedNode : function(tree) {
				try {
					if (this.reseting)
						return
					!tree ? tree = this.tree : 0;
					if (tree && this.getValue()) {
						tree.collapseAll()
					}
					this.reseting = true;
					Ext.each(this.getValue(), function(v) {
								var n;
								if (tree) {
									n = tree.getNodeById(v.id)
								}
								var expandPath;
								if (n) {
									n.getUI().toggleCheck(true);
									expandPath = n.getPath()
									var arr = expandPath.split('/');
									arr.length = arr.length - 1;
									expandPath = arr.join('/')
								} else {
									expandPath = this.expandNonExistNode(v)
								}
								if (expandPath)
									tree.expandPath(expandPath);
							}, this)
				} finally {
					this.reseting = false;
				}
			}
		})