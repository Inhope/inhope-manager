Ext.namespace('ux')
Ext.form.TextField.prototype.getSelection = function(element) {
	var domElement = element ? element : this.getEl().dom;
	if (Ext.isIE) {
		var sel = document.selection;
		var range = sel.createRange();
		if (range.parentElement() != domElement)
			return null;
		var bookmark = range.getBookmark();
		var selection = domElement.createTextRange();
		selection.moveToBookmark(bookmark);
		var before = domElement.createTextRange();
		before.collapse(true);
		before.setEndPoint("EndToStart", selection);
		var after = domElement.createTextRange();
		after.setEndPoint("StartToEnd", selection);
		return {
			selectionStart : before.text.length,
			selectionEnd : before.text.length + selection.text.length,
			beforeText : before.text,
			text : selection.text,
			afterText : after.text,
			wholeText : before.text + selection.text + after.text
		}
	} else {
		if (domElement.selectionEnd >= 0 && domElement.selectionStart >= 0) {
			if (domElement.selectionEnd >= domElement.selectionStart) {
				return {
					selectionStart : domElement.selectionStart,
					selectionEnd : domElement.selectionEnd,
					beforeText : domElement.value.substr(0, domElement.selectionStart),
					text : domElement.value.substr(domElement.selectionStart, domElement.selectionEnd - domElement.selectionStart),
					afterText : domElement.value.substr(domElement.selectionEnd),
					wholeText : domElement.value
				};
			}
		}
	}
	return null;
}
Ext.form.HtmlEditor.prototype.getSelection = function() {
	if (Ext.isIE) {
		var range = this.getDoc().selection.createRange();
		domElement = this.getDoc().body
		var bookmark = range.getBookmark();
		var selection = domElement.createTextRange();
		selection.moveToBookmark(bookmark);
		var before = domElement.createTextRange();
		before.collapse(true);
		before.setEndPoint("EndToStart", selection);
		var after = domElement.createTextRange();
		after.setEndPoint("StartToEnd", selection);

		function tripHTML(html) {
			var str = html
			str = str.replace(/<br>/gi, "\n");
			str = str.replace(/<[^>]+>/g, "");
			str = str.replace('&nbsp;', ' ');
			str = str.replace('&lt;', '<');
			str = str.replace('&gt;', '>');
			return str;
		}

		var beforeText = tripHTML(before.htmlText)
		var afterText = tripHTML(after.htmlText)
		var selText = tripHTML(selection.htmlText)
		return {
			selectionStart : beforeText.length,
			selectionEnd : beforeText.length + selText.length,
			beforeText : beforeText,
			text : selText,
			afterText : afterText,
			wholeText : beforeText + selText + afterText
		}
	} else {
		function getNodeText(node) {
			var text = ''
			var node = node.firstChild
			while (node) {
				if (node.nodeType == 3) {
					text = text + node.nodeValue
				} else if (node.nodeType == 1) {
					if (node.nodeName == 'BR') {
						text = text + '\n'
					} else {
						text = text + getNodeText(node)
					}
				}
				node = node.nextSibling;
			}
			return text;
		}
		function getBeforeNodeText(top, node) {
			var text = ''
			while (node != top) {
				while (node.previousSibling != null) {
					node = node.previousSibling;
					if (node.nodeType == 3) {
						text = node.nodeValue + text
					} else if (node.nodeType == 1) {
						if (node.nodeName == 'BR') {
							text = '\n' + text
						} else {
							text = getNodeText(node) + text
						}
					}
				}
				node = node.parentNode
			}
			return text
		}
		function getAfterNodeText(top, node) {
			var text = ''
			while (node != top) {
				while (node.nextSibling != null) {
					node = node.nextSibling;
					if (node.nodeType == 3) {
						text = text + node.nodeValue
					} else if (node.nodeType == 1) {
						if (node.nodeName == 'BR') {
							text = text + '\n'
						} else {
							text = text + getNodeText(node)
						}
					}
				}
				node = node.parentNode
			}
			return text
		}

		var body = this.getDoc().getElementsByTagName('BODY')[0]
		var sel = this.getWin().getSelection();
		var start = sel.anchorOffset
		var end = sel.focusOffset
		var value, selNode = sel.anchorNode
		if (selNode.nodeType == 3) {
			value = selNode.nodeValue
		} else if (selNode.nodeType == 1) {
			selNode = selNode.childNodes[start]
			if (!selNode)
				return;
			start = end = 0
			if (selNode.nodeName == 'BR') {
				value = '\n'
			}
		}
		var beforeNodeText = getBeforeNodeText(body, selNode)
		var afterNodeText = getAfterNodeText(body, selNode)
		if (!value || value == '\n')
			value = ''
		return {
			selectionStart : beforeNodeText.length + start,
			selectionEnd : beforeNodeText.length + end,
			beforeText : beforeNodeText + value.substr(0, start),
			text : value.substr(start, end),
			afterText : value.substr(end) + afterNodeText,
			wholeText : beforeNodeText + value + afterNodeText
		}
	}
}

var _ext_panel_init = Ext.Panel.prototype.initComponent;
var _ext_panel_const = Ext.Panel;
Ext.override(Ext.data.Record, {
			_set_method : Ext.data.Record.prototype.set,
			set : function(key, value) {
				if (!key) {
					return;
				}
				return this._set_method.apply(this, arguments)
			}
		})
Ext.override(Ext.Editor, {
			_realign : Ext.Editor.prototype.realign,
			_initComponent : Ext.Editor.prototype.initComponent,
			_setSize : Ext.Editor.prototype.setSize,
			initComponent : function() {
				this.autoSize = true;
				if (this.field && this.field.getCustomEditorSize) {
					this.getCustomEditorSize = this.field.getCustomEditorSize
				}
				this._initComponent.call(this);
			},
			setSize : function(w, h) {
				if (this.getCustomEditorSize) {
					var sz = this.getCustomEditorSize(this);
					if (sz[0])
						w = sz[0];
					if (sz[1])
						h = sz[1]
				}
				this._setSize.apply(this, [w, h]);
			},
			realign : function(autoSize) {
				if (!this.field.getResizeEl())
					autoSize = false;
				var old = this.boundEl
				if (old) {
					var el = old.findParent('td', 5, true)
					if (el) {
						this.boundEl = el
						this._realign.apply(this, [autoSize])
						this.boundEl = old;
						return;
					}
				}
				this._realign.apply(this, [autoSize])
			}
		});
Ext.override(Ext.Panel, {
// split : true,
		// collapsible : true
		// initComponent : function() {
		// if (this.region && this.region != 'center') {
		// if (this.title == '控制台') {
		// Ext.apply(this.initialConfig, {
		//
		// })
		// }
		// }
		// return _ext_panel_init.call(this);
		// }
		})
Ext.override(Ext.tree.TreePanel, {
			// private
			_originalInitCmp : Ext.tree.TreePanel.prototype.initComponent,
			initComponent : function() {
				this._originalInitCmp.call(this)
				if (this.root && this.root.attributes && this.root.attributes.lazyLoad) {
					this.on('activate', function() {
								this._activated = true;
							})
					this.on('afterrender', function() {
								if (this._activated) {
									this.renderRootNode();
								} else {
									this.on('expand', function() {
												this.renderRootNode();
											})
								}
							})
				}
			},
			renderRoot : function() {
				if (this.root && this.root.attributes && !this.root.attributes.lazyLoad) {
					this.renderRootInternal();
				}
			},
			renderRootInternal : function() {
				if (this.__rootRendered)
					return false;
				this.__rootRendered = true;
				this.root.render();
				if (!this.rootVisible) {
					this.root.renderChildren();
				}
			},
			renderRootNode : function() {
				this.renderRootInternal();
			}
		})
Ext.form.ComboBox.prototype._doQuery = Ext.form.ComboBox.prototype.doQuery
Ext.override(Ext.form.ComboBox, {
			doQuery : function() {
				if (this.getStore().autoLoad === false && !this.getStore().__autoLoaded) {
					this.getStore().__autoLoaded = true;
					this.getStore().load();
				}
				return Ext.form.ComboBox.prototype._doQuery.apply(this, arguments)
			}
		})
Ext.override(Ext.form.Field, {
			hideField : function() {
				var field = this;
				if (field) {
					field.disable();// for validation
					field.hide();
					var labelEl = field.getEl();
					if (labelEl) {
						labelEl.up('.x-form-item').setDisplayed(false); // hide
						// label
					}
				}
			},
			showField : function() {
				var field = this;
				if (field) {
					field.enable();
					field.show();
					var labelEl = field.getEl();
					if (labelEl) {
						field.getEl().up('.x-form-item').setDisplayed(true); // hide
						// label
					}
				}
			}
		})
Function.prototype.dg = Function.prototype.createDelegate

ux.ObjectTextField = Ext.extend(Ext.form.Field, {
			fieldPath : '',
			initComponent : function() {
				if (!this.fieldPath) {
					throw new Error('ObjectTextField.fieldPath required')
				}
			},
			reset : function() {
				this.value = null;
				this.setRawValue(null);
			},
			setValue : function(v) {
				if (v) {
					if (typeof v == 'string') {
						if (!this.value)
							this.value = {};
						this.value[this.fieldPath] = v;
					} else {
						this.value = v;
					}
					this.setRawValue(v[this.fieldPath]);
				} else {
					this.value = v;
				}
			},
			getValue : function() {
				var v = this.getRawValue();
				if (v) {
					if (!this.value)
						this.value = {};
					this.value[this.fieldPath] = v;
				} else {
					this.value = null;
				}
				return this.value;
			}
		})

ux.PostEditableEditorGridPanel = Ext.extend(Ext.grid.EditorGridPanel, {
			postEditValue : function(value, startValue, r, field) {
				if (this.onPostEditValue) {
					this.onPostEditValue(value, startValue, r, field)
				}
				return ux.PostEditableEditorGridPanel.superclass.postEditValue.apply(this, arguments);
			}
		})
ux.util = {
	resetEmptyString : function(obj) {
		for (var key in obj) {
			var v = obj[key];
			if (!v) {
				delete obj[key];
			} else if (v.push && v.length) {
				for (var i = 0; i < v.length; i++) {
					this.resetEmptyString(v[i]);
				}
			} else if (key.indexOf('id') != -1 && key.indexOf('-') == 0) {
				delete obj[key]// reset negative id as null
			}
		}
		return obj;
	},
	isEmptyRecord : function(r) {
		return ux.util.isEmptyData(r.data)
	},
	isEmptyData : function(data,excludeFields) {
		for (var key in data) {
			if (key && data[key]) {
				if (!excludeFields || excludeFields.indexOf(key) == -1){
					if (data[key] instanceof Array) {
						if (data[key].length)
							return false
					} else {
						return false
					}
				}
			}
		}
		return true
	}
}
Ext.override(Ext.tree.TreeNode, {
			traverseNode : function(node, cb) {
				if (node && cb(node) === false)
					return
				if (node.childNodes && node.childNodes.length)
					for (var i = 0; i < node.childNodes.length; i++) {
						this.traverseNode(node.childNodes[i], cb)
					}
			},
			/**
			 * 
			 * @param {Function}
			 *            cb - return false to stop the traverse. params - node
			 */
			traverse : function(cb) {
				this.traverseNode(this, cb);
			},
			findByAttributes : function(attrsmap) {
				var foundNode;
				this.traverse(function(node) {
							for (var key in attrsmap) {
								if (node.attributes[key] != attrsmap[key]) {
									return;
								}
							}
							foundNode = node
							return false;
						});
				return foundNode;
			}
		})
Ext.util.Format._dateRenderer = Ext.util.Format.dateRenderer;
Ext.util.Format.dateRenderer = function(format) {
	var oldrender = this._dateRenderer.apply(this, arguments);
	return function(v) {
		if (typeof v == 'string')
			return v;
		return oldrender.apply(this, arguments);
	}
};
/**
 * 
 * @param {}
 *            cb()
 */
Ext.data.Store.prototype.doWithoutFilter = function(cb, scope) {
	var cur = this.data;
	try {
		if (this.snapshot && this.snapshot != this.data) {
			this.data = this.snapshot;
		}
		cb.call(scope);
	} finally {
		if (cur != this.data)
			this.data = cur;
	}
}


Ext.override(Ext.form.RadioGroup, {   
    setValue: function(v){   
        if (this.rendered)    
            this.items.each(function(item){   
                item.setValue(item.inputValue == v); 
            });   
        else {   
            for (var k in this.items) {   
                this.items[k].checked = this.items[k].inputValue == v;   
            }   
        }   
    }   
}); 

// fix editor lay up of Window
Ext.Editor.prototype.zIndex = 1000;
// enable grid cell to be selected in chrome/IE
Ext.grid.GridView.prototype.cellTpl = new Ext.Template(Ext.grid.GridView.prototype.cellTpl.html.replace('unselectable="on"', '').replace('class="', 'class="x-selectable '))

Ext.chart.Chart.CHART_URL = '../ext/resources/charts.swf'
var OperationColumn = Ext.extend(Object, {
	header : '操作',
	sortable : false,
	dataIndex : '',
	renderer : function(value, metaData, record, rowIndex, colIndex, store) {
		var fds = store.fields
		var ret = ''
		fds.each(function(fd) {
			var v = null
			if (Ext.isString(fd))
				v = record.get(fd)
			else
				v = record.get(fd.name)
			if (v) {
				Ext.each(this.operationConfig,function(c,i){
					var name = typeof c.getName == 'function'? c.getName(value, metaData, record, rowIndex, colIndex, store):c.name;
					if (name)
						ret += "<a class='"+this._id+"_"+i+"' href='javascript:void(0);' onclick='javscript:return false;'>"+name+"</a>&nbsp;";
				},this)
				return false;
			}
		},this);
		return ret;
	},
	init:function(grid){
		grid.on('cellclick', function(grid, rowIndex, columnIndex, e) {
				Ext.each(this.operationConfig,function(c,i){
					if (e.getTarget('.'+this._id+'_'+i) && c.handler){
						c.handler.call(this,grid, rowIndex, columnIndex, e)
					}
				},this)
			},this)
	},
	constructor : function(config) {
		this._id = Ext.id()
		Ext.apply(this, config);
	}
});
Ext.override(Ext.Button, {
			_disabletarget : 0,
			enableByTarget : function(target) {
				if ((this._disabletarget & target) == target)
					this._disabletarget=~(~this._disabletarget | target)
				if (!this._disabletarget)
					this.enable();
			},
			disableByTarget : function(target) {
				this._disabletarget|=target
				this.disable();
			}
		})