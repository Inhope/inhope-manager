<%@page import="org.springframework.context.ApplicationContext"%>
<%@page import="org.codehaus.jackson.map.ObjectMapper"%>
<%@page import="com.incesoft.xmas.commons.LicenceHolder"%>
<%@page import="com.incesoft.xmas.commons.LabsUtil"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="res" uri="/WEB-INF/tld/import-res.tld"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="usr" uri="/WEB-INF/tld/login-user.tld"%>
<%@taglib prefix="mdl" uri="/WEB-INF/tld/mdl-ft.tld"%>
<%
	ApplicationContext ctx = WebApplicationContextUtils
			.getWebApplicationContext(application);
	LicenceHolder licenceholder = ctx.getBean(LicenceHolder.class);
	if (licenceholder != null && licenceholder.getLicence() != null) {
		request.setAttribute("supportPlatforms", licenceholder
				.getLicence().getSupportedPlatforms());
	}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>语义任务</title>
<usr:checkPerm jsExportVar="kb_check_perm_result" permissionNames="kb" dump="true" />
<usr:name jsExportVar="kb_username" />
<mdl:dump name="kb" jsExportVar="kb_mdl_ft" />
<mdl:dump name="kb" jsExportVar="kb_mdl_props" type="props" />
<%@include file="/commons/extjsvable.jsp"%>
<script type="text/javascript">
	var isAllow = http_get('../app/g/kbmgr/abstract-semantic-task!isAllow.action');
	var sys_labs = <%=LabsUtil.isLabs()%>
</script>
<link rel="stylesheet" type="text/css" href="<c:url value="/kbmgr/custom.css"/>" />
<link rel="stylesheet" type="text/css" href="<c:url value="/kbmgr/multitools.css"/>" />


<!-- 加载extjs4.2.1 -->
<script src="<c:url value="/ext4/ext-all-sandbox.js"/>"
	type="text/javascript"></script>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/ext4/ext-sandbox.css"/>" />
<!-- 加载extjs4.2.1结束 -->

<script src="<c:url value="/kbmgr/script/ux/ddtabpanel.js"/>" type="text/javascript"></script>
<script src="<c:url value="/kbmgr/script/ux/TreeComboBox.js"/>" type="text/javascript"></script>
<script src="<c:url value="/kbmgr/script/index.js"/>" type="text/javascript"></script>
<script src="<c:url value="/kbmgr/script/NavTaskPanel.js"/>" type="text/javascript"></script>
<script src="<c:url value="/kbmgr/script/AbstractSemanticTaskAssignmentPanel.js"/>" type="text/javascript"></script>
<script src="<c:url value="/kbmgr/script/AbstractSemanticTaskDetailPanel.js"/>" type="text/javascript"></script>
<script src="<c:url value="/kbmgr/script/AbstractSemanticTaskCountPanel.js"/>" type="text/javascript"></script>
<script src="<c:url value="/kbmgr/script/AbstractSemanticTaskDetailSearchPanel.js"/>" type="text/javascript"></script>
</head>

<body>
</body>
</html>