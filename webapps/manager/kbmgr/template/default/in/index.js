var STATE_SYNC_SUC = 1;
var STATE_SYNC_FAIL = 2;
var STATE_SYNC_RANGE = [STATE_SYNC_SUC, STATE_SYNC_SUC << 1];

var STATE_AUD_SUBMIT = 8;
var STATE_AUD_PASS = 31;
var STATE_AUD_REJECT = 17;
var STATE_AUD_RANGE = [STATE_AUD_SUBMIT, STATE_AUD_PASS];
var _obj_date_format = 'Y-m-d H:i'
var OP_CREATE = 1;
var OP_MODIFY = 2;
var OP_DELETE = 3;

var contextData;
var Dashboard = {
	u : function() {
		return this._usr ? this._usr : this._usr = new LoginUser('kb')
	},
	getModuleFeature : function(key) {
		return window.kb_mdl_ft ? window.kb_mdl_ft[key] : null
	},
	getModuleProp : function(key) {
		return window.kb_mdl_props ? window.kb_mdl_props[key] : null
	},
	setAlert : function(msg, status, delay) {
		if (!status)
			status = 'ok'
		if (!delay)
			delay = 3
		if (!this.msgCt) {
			this.msgCt = Ext.DomHelper.insertFirst(document.body, {
				  id : 'msg-div'
			  }, true);
			this.msgCt.setStyle('position', 'absolute');
			this.msgCt.setStyle('z-index', 9999);
			this.msgCt.setWidth(300);
		}
		this.msgCt.alignTo(document, 't-t');
		var html = ['<div class="app-msg">', '<div class="x-box-tl"><div class="x-box-tr"><div class="x-box-tc"></div></div></div>',
		  '<div class="x-box-ml"><div class="x-box-mr"><div class="x-box-mc"><h3 class="x-icon-text icon-status-' + status + '">', msg, '</h3>', '</div></div></div>',
		  '<div class="x-box-bl"><div class="x-box-br"><div class="x-box-bc"></div></div></div>', '</div>'].join('');
		Ext.DomHelper.append(this.msgCt, {
			  'html' : html
		  }, true).slideIn('t').pause(delay).ghost("t", {
			  remove : true
		  });
	}
};
var MainPanel = Ext.extend(Ext.FormPanel, {
	  createAttrRow : function(a) {
		  var cfg;
		  if (!a.attributeName) {
			  cfg = {
				  xtype : 'panel',
				  aid : a.valueId,
				  bodyStyle : schemaColor,
				  layout : 'column',
				  border : false,
				  defaults : {
					  layout : 'form',
					  anchor : '100%',
					  border : false,
					  labelWidth : 30
				  },
				  items : [{
					    ref : 'qCol',
					    columnWidth : .40,
					    items : []
				    }, {
					    ref : 'aCol',
					    columnWidth : .60,
					    items : []
				    }, {
					    ref : 'opCol1', // 
					    width : 200,
					    labelWidth : 40,
					    items : []
				    }, {
					    ref : 'opCol2',// time
					    width : 150,
					    labelWidth : 55,
					    items : []

				    }, {
					    ref : 'opCol3', // op
					    // columnWidth : .05,
					    width : 30,
					    items : []
				    }]
			  }
		  } else {
			  cfg = {
				  xtype : 'panel',
				  border : false,
				  layout : 'column',
				  aid : a.valueId,
				  defaults : {
					  layout : 'form',
					  anchor : '100%',
					  border : false,
					  style : 'padding:0;margin:0',
					  bodyStyle : schemaColor + ';padding:0;margin:0;',
					  labelWidth : 55
				  },
				  items : [{
					    ref : 'qCol',
					    width : 0,
					    hidden : true,
					    items : []
				    }, {
					    ref : 'aCol',
					    columnWidth : 1,
					    items : []
				    }, {
					    ref : 'opCol1', // 
					    width : 200,
					    labelWidth : 40,
					    items : []
				    }, {
					    ref : 'opCol2',// time
					    width : 150,
					    labelWidth : 55,
					    items : []

				    }, {
					    ref : 'opCol3', // op
					    // columnWidth : .05,
					    width : 30,
					    items : []
				    }]
			  }
		  }
		  return Ext.create(cfg)
	  },
	  addAttrValue : function(a, insertPos) {
		  if (!a.valueId)
			  a.valueId = '-' + Ext.id()
		  if (!a.values || !a.values.length) {
			  a.values = [{
				    id : '-' + Ext.id()
			    }]
		  }
		  var fs = a.attributeName ? this[a.attributeGroup + 'qFieldSet'] : this['补充问答qFieldSet']
		  Ext.each(a.values, function(dimv, i) {
			    var aid = a.valueId
			    var attrRowP = this.createAttrRow(a)
			    if (i == 0 && insertPos == null) {
				    var qCfg;
				    if (a.attributeName) {// 继承属性
					    qCfg = {
						    aid : aid,
						    attachment : a,
						    value : a.question,
						    xtype : 'textfield',
						    hidden : true
					    }
				    } else {// 自定义
					    qCfg = {
						    aid : aid,
						    attachment : a,
						    fieldLabel : '问题',
						    emptyText : '请填写问题',
						    value : a.question,
						    xtype : 'textarea',
						    height : 140,
						    anchor : '98%'
					    }
					    if (!a.valueId) {
						    Ext.apply(a, {
							      valueId : '-' + Ext.id(),
							      attributeGroup : '补充问答'
						      })
					    }
				    }
				    attrRowP.qCol.add(qCfg)
			    } else {
				    attrRowP.qCol.add({
					      xtype : 'textfield',
					      style : 'display:none'
				      })
			    }
			    var dimpart = this.createDimPart(dimv, a, i == 0 && insertPos == null);
			    this.dimValueOtherPart[dimv.id] = dimv
			    attrRowP.aCol.add(dimpart.ans)
			    attrRowP.opCol1.add(dimpart.dim)
			    attrRowP.opCol1.add(dimpart.cmd)
			    attrRowP.opCol2.add(dimpart.startTime)
			    attrRowP.opCol2.add(dimpart.endTime)
			    attrRowP.opCol2.add(dimpart.endTime1)
			    attrRowP.opCol3.add(dimpart.op)
			    attrRowP.opCol3.add(dimpart.opD)
			    if (i == 0 && insertPos == null) {
				    attrRowP.opCol1.add(dimpart.e_l)
				    attrRowP.opCol1.add(dimpart.bb)
				    attrRowP.opCol2.add(dimpart.pageanchor)
				    attrRowP.opCol2.add(dimpart.pageorder)
				    attrRowP.opCol2.add(dimpart.state)
			    }

			    attrRowP.dimid = dimv.id
			    attrRowP.attachment = a
			    if (insertPos != null) {
				    fs.items.insert(insertPos, attrRowP);
			    } else {
				    fs.add(attrRowP);
			    }
		    }, this)
		  fs.doLayout()
	  },
	  addDimValue : function(a, dimv) {
		  var fs = this[a.attributeGroup + 'qFieldSet'];
		  fs.items.each(function(p, i) {
			    if (p.dimid == dimv.id) {
				    var aa = {
					    valueId : a.valueId,
					    attributeGroup : a.attributeGroup,
					    attributeName : a.attributeName
				    }
				    this.addAttrValue(aa, i + 1);
				    return false;
			    }
		    }, this)
	  },
	  delDimValue : function(a, dimv) {
		  var fs = this[a.attributeGroup + 'qFieldSet'];
		  var parentRow;
		  var deleteRow;
		  var nextRow;
		  fs.items.each(function(p, i) {
			    if (a.valueId == p.aid) {
				    if (!parentRow)
					    parentRow = p
				    if (deleteRow && !nextRow)
					    nextRow = p
				    if (p.dimid == dimv.id) {
					    if (!deleteRow)
						    deleteRow = p
				    }
			    }
		    }, this)
		  if (deleteRow == parentRow && nextRow) {
			  // copy nextRow
			  Ext.each('aCol opCol1 opCol2 opCol3'.split(' '), function(colname) {
				    try {
					    deleteRow[colname].items.each(function(item, i) {
						      if (nextRow[colname].items.get(i))
							      item.setValue(nextRow[colname].items.get(i).getValue())
					      })
				    } catch (e) {
					    console.log(e)
				    }
			    })
			  deleteRow.dimid = nextRow.dimid
			  deleteRow.doLayout()
			  deleteRow = nextRow;
		  }
		  fs.remove(deleteRow)
	  },
	  translateAttrState : function(attr) {
		  var s = ''
		  var a = []
		  var v = attr.state;
		  v == 0 || !v ? (a[0] = '未同步', s = 'color:tomato;') : 0
		  v == STATE_SYNC_SUC ? (a[0] = '已同步', s = 'color:green;') : 0
		  v == STATE_SYNC_FAIL ? (a[0] = '同步失败', s = 'color:red;') : 0
		  v == STATE_AUD_SUBMIT ? (a[0] = '提交审批', s = 'color:magenta;') : 0
		  v == STATE_AUD_PASS ? (a[0] = '审批通过', s = 'color:green;') : 0
		  v == STATE_AUD_REJECT ? (a[0] = '审批驳回', s = 'color:red;') : 0
		  if (s) {
			  s = 'style=' + s + (v == 2 ? 'font-weight:bold;' : '')
			  if (attr.op == OP_DELETE)
				  a[0] += '(删除)'
		  }
		  return '<span ' + s + '>' + a[0] + '</span>'
	  },
	  createDimPart : function(dimv, a, first) {
		  var items = [{
			    itemId : 'ans',
			    aid : a.valueId,
			    xtype : 'htmleditor',
			    height : 140 + 27,
			    anchor : '98%',
			    fieldLabel : first ? (a.attributeName ? a.attributeName : '答案') : '',
			    emptyText : '请填写答案',
			    value : dimv.value,
			    listeners : {
				    initialize : function(ed) {
					    this.getToolbar().hide();
					    Ext.EventManager.on(ed.getWin(), 'blur', function() {
						      this.setHeight(140 + 27)
						      this.getToolbar().hide()
					      }, this);
					    Ext.EventManager.on(ed.getWin(), 'focus', function() {
						      this.getToolbar().show()
						      this.setHeight(140)
					      }, this);
				    }
			    }
		    }, {
			    itemId : 'dim',
			    xtype : 'dimensionbox',
			    value : dimv.dimTagIds,
			    anchor : '95%',
			    fieldLabel : '维度'
		    }, {
			    itemId : 'cmd',
			    xtype : 'cmdchoosercombo',
			    value : dimv.cmd,
			    anchor : '95%',
			    fieldLabel : '指令'
		    }, {
			    itemId : 'e_l',
			    xtype : 'checkbox',
			    checked : (a.flag & 1) == 1,
			    anchor : '95%',
			    fieldLabel : 'E-L'
		    }, {// broadcast bar
			    itemId : 'bb',
			    xtype : 'checkbox',
			    checked : (a.flag & 2) == 2,
			    anchor : '95%',
			    fieldLabel : '公告栏'
		    }, {// 页签
			    itemId : 'pageanchor',
			    xtype : 'textfield',
			    value : a.flag == null ? 0 : parseInt(a.flag % 10000 / 1000),
			    anchor : '95%',
			    fieldLabel : '页签'
		    }, {// 属性排序
			    itemId : 'pageorder',
			    xtype : 'textfield',
			    value : a.flag == null ? 0 : parseInt(a.flag / 10000),
			    anchor : '95%',
			    fieldLabel : '优先级'
		    }, {// 属性状态
			    itemId : 'state',
			    xtype : 'label',
			    style : 'position:relative;top:3px',
			    html : this.translateAttrState(a),
			    anchor : '95%',
			    fieldLabel : '状态'
		    }, {
			    itemId : 'startTime',
			    xtype : 'datefield',
			    format : 'Y-m-d',
			    value : dimv.startTime,
			    anchor : '95%',
			    fieldLabel : '生效时间',
			    labelWidth : 200
		    }, {
			    itemId : 'endTime',
			    xtype : 'datefield',
			    format : 'Y-m-d',
			    value : dimv.endTime,
			    anchor : '95%',
			    fieldLabel : '失效时间'
		    }, {
			    itemId : 'endTime1',
			    xtype : 'datefield',
			    format : 'Y-m-d',
			    value : dimv.endTime1,
			    anchor : '95%',
			    fieldLabel : '追溯时间'
		    }]
		  var ret = {}
		  Ext.each(items, function(item) {
			    ret[item.itemId] = item
		    })
		  ret['op'] = {
			  xtype : 'button',
			  tooltip : '新增答案',
			  iconCls : 'icon-add',
			  handler : function() {
				  this.addDimValue(a, dimv)
			  },
			  scope : this
		  }
		  ret['opD'] = {
			  xtype : 'button',
			  tooltip : '删除答案',
			  iconCls : 'icon-delete',
			  handler : function() {
				  this.delDimValue(a, dimv)
			  },
			  scope : this
		  }
		  return ret
	  },
	  saveData : function(sync) {
		  var d = {}
		  var values = this.getForm().getValues();
		  // object fields
		  for (var key in values) {
			  if (key.indexOf('obj_') == 0) {
				  var v = values[key]
				  key = key.substring(4)
				  if (key != 'classId') {
					  d[key] = v;
				  }
			  }
		  }
		  d.bizTplId = this.getTopToolbar().bizTplId.getValue()
		  d.tag = this.getTopToolbar().tag.getValue()
		  d.status = 0;
		  Ext.each("newest hotest recommend".split(' '), function(n) {
			    if (this.getTopToolbar()[n].getValue())
				    d.status |= parseInt(this.getTopToolbar()[n].inputValue)
		    }, this)
		  // attributes
		  d.attributes = []
		  for (var key in this) {
			  var fs = this[key];
			  if (key.indexOf('qFieldSet') == -1 || !(fs instanceof Ext.form.FieldSet))
				  continue;
			  var aid = null;
			  var curAttr = null;
			  fs.items.each(function(f) {
				    if (f.aid != aid) {
					    if (curAttr && (!curAttr.values || !curAttr.values.length)) {
						    d.attributes.length = d.attributes.length - 1;
					    }
					    aid = f.aid
					    curAttr = Ext.apply({}, f.attachment);
					    delete curAttr.attributeGroup
					    d.attributes.push(curAttr);
					    curAttr.question = f.qCol.items.get(0).getValue()
					    curAttr.values = []
					    if (curAttr.valueId && curAttr.valueId.indexOf('-') == 0) {
						    delete curAttr.valueId
					    }
					    if (this.auditState != null)
						    curAttr.state = this.auditState
					    else if (this.orig_state < 3) {
						    curAttr.state = 0
					    }
					    var flag = 0
					    if (getItem('pageorder', f).getValue())
						    flag += (parseInt(getItem('pageorder', f).getValue())) * 10000
					    if (getItem('pageanchor', f).getValue())
						    flag += parseInt(getItem('pageanchor', f).getValue()) * 1000
					    if (getItem('e_l', f).checked)
						    flag |= 1
					    if (getItem('bb', f).checked)
						    flag |= 2
					    curAttr.flag = flag
				    }
				    var dimv = {};
				    Ext.each("ans dim cmd startTime endTime endTime1".split(' '), function(itemid) {
					      dimv[itemid] = getItem(itemid, f).getValue()
				      })
				    dimv.value = dimv.ans
				    dimv.dimTagIds = dimv.dim
				    if (dimv.ans)
					    delete dimv.ans
				    if (dimv.dim)
					    delete dimv.dim
				    if (dimv.value && dimv.value.trim() || dimv.cmd && dimv.cmd.trim()) {
					    if (f.dimid && f.dimid.indexOf('-') != 0)
						    dimv.id = f.dimid
					    var db_dimv = this.dimValueOtherPart[dimv.id]
					    if (db_dimv) {
						    Ext.applyIf(dimv, db_dimv)
					    }
					    curAttr.values.push(dimv)
				    }
			    }, this)
		  }
		  if (curAttr && (!curAttr.values || !curAttr.values.length)) {
			  d.attributes.length = d.attributes.length - 1;
		  }
		  if (this.invisibleAttrs && this.auditState != null) {
			  Ext.each(this.invisibleAttrs, function(a) {
				    a.state = this.auditState
			    }, this)
		  }
		  d.attributes = d.attributes.concat(this.invisibleAttrs)
		  Ext.Ajax.request({
			    url : 'ontology-object!save.action',
			    params : {
				    data : Ext.encode(d),
				    ispub : '',
				    sync : sync != null ? sync : this.getTopToolbar().saveBtn.sync.checked,
				    attrVDeleted : false
			    },
			    success : function(response) {
				    this.getEl().unmask()
				    var result = Ext.util.JSON.decode(response.responseText);
				    if (!result.success) {
					    Ext.Msg.alert("提示", result.message);
					    return;
				    }
				    var d = result.data;
				    if (parent.Dashboard)
					    parent.Dashboard.setAlert("保存成功!");
				    window.location = window.location.toString().replace(/\?.*/ig, "") + '?data={"objectId":"' + d.objectId + '"}';
			    },
			    failure : function(response) {
				    this.getEl().unmask()
				    Ext.Msg.alert("错误", '错误信息:<br>' + response.responseText.replace(/\r\n/ig, '<br>'));
				    if (cb)
					    cb(false)
			    },
			    scope : this
		    });
	  },
	  doAudit : function(type) {
		  if (type) {// to submit
			  this.auditState = STATE_AUD_SUBMIT
		  } else {// reset
			  this.auditState = 0
		  }
		  this.saveData(false)
	  },
	  loadData : function(objData) {
		  this.objData = objData;
		  this.getForm().reset();
		  for (var key in objData) {
			  var f = this.find('name', 'obj_' + key)[0]
			  if (f)
				  f.setValue(objData[key])
		  }
		  Ext.each("newest hotest recommend".split(' '), function(n) {
			    var v = parseInt(this.getTopToolbar()[n].inputValue)
			    if ((v & objData.status) == v)
				    this.getTopToolbar()[n].setValue(true);
		    }, this)

		  var groupFieldSet = {}
		  this.invisibleAttrs = []
		  this.dimValueOtherPart = {}
		  var attrs = objData.attributes;
		  var classId;
		  if (objData.classes && objData.classes.length)
			  classId = objData.classes[0].id
		  Ext.each(attrs, function(a, i) {
			    if (!a.visible) {
				    this.invisibleAttrs.push(a)
				    return
			    }
			    if (!a.valueId) {
				    a.valueId = '-' + Ext.id()
			    }
			    var aid = a.valueId;
			    var grp = a.attributeGroup;
			    if (!grp) {
				    grp = a.attributeName ? '属性知识' : '补充问答'
				    a.attributeGroup = grp;
			    }
			    if (!groupFieldSet[grp]) {
				    groupFieldSet[grp] = {}
			    }
		    }, this)
		  var stateinfosstr = []
		  var a = attrs[0] || {};
		  Ext.each(attrs, function(att) {
			    if (att.values && att.values.length && att.values[0].value) {
				    a = att
				    var str = null;
				    if (a.state == STATE_AUD_SUBMIT)
					    str = ('已提交审核')
				    else if (a.state == STATE_AUD_REJECT)
					    str = ('审核被驳回')
				    else if (a.state == STATE_AUD_PASS)
					    str = ('审核通过')
				    else if (a.state == 0)
					    str = ('未提交审核')
				    else if (a.state == 1)
					    str = ('发布成功')
				    else if (a.state == 2)
					    str = ('发布失败')
				    if (stateinfosstr.indexOf(str) == -1)
					    stateinfosstr.push(str)
			    }
		    })
		  // this.orig_state = a.state
		  Ext.getDom('statinfo').innerHTML = stateinfosstr.join(',')

		  this.getTopToolbar().bizTplId.setValue(objData.bizTplId)
		  this.getTopToolbar().tag.setValue(objData.tag)

		  if (!groupFieldSet['补充问答']) {
			  groupFieldSet['补充问答'] = {}
		  }
		  var itemtoremove = []
		  this.items.each(function(item) {
			    if (item.name && item.name.indexOf('qFieldSet') != -1) {
				    itemtoremove.push(item)
			    }
		    })
		  Ext.each(itemtoremove, function(item) {
			    this.remove(item)
		    })
		  for (var key in groupFieldSet) {
			  var gfs = groupFieldSet[key];
			  var _cfg;
			  if (key == '补充问答') {
				  _cfg = {
					  xtype : 'fieldset',
					  name : key + 'qFieldSet',
					  ref : '../' + key + 'qFieldSet',
					  bodyStyle : schemaColor,
					  title : key,
					  layout : 'form',
					  style : 'margin:5px;border-width:0;border-top-width:1px;' + schemaColor,
					  border : true,
					  defaults : {
						  border : false,
						  bodyStyle : schemaColor
					  },
					  items : []
				  }
			  } else {
				  _cfg = {
					  xtype : 'fieldset',
					  name : key + 'qFieldSet',
					  ref : '../' + key + 'qFieldSet',
					  title : key,
					  border : true,
					  layout : 'form',
					  style : 'margin:5px;border-width:0;border-top-width:1px;' + schemaColor,
					  bodyStyle : schemaColor,
					  defaults : {
						  border : false,
						  bodyStyle : schemaColor
					  },
					  items : []
				  }
			  }
			  this.attrContainer.add(_cfg)
		  }
		  this.doLayout()
		  Ext.each(attrs, function(att) {
			    this.addAttrValue(att)
		    }, this)
		  this.doLayout()
	  },
	  initComponent : function() {
		  this.tbar = {
			  autoScroll : true
		  }
		  this.tbar.items = [{
			    xtype : 'splitbutton',
			    text : '保存',
			    ref : 'saveBtn',
			    menu : {
				    items : [{
					      text : '同步',
					      checked : false,
					      name : 'sync',
					      ref : '../sync'
				      }]
			    },
			    iconCls : 'icon-table-save',
			    width : 50,
			    handler : this.saveData.dg(this, [])
		    }, {
			    xtype : 'button',
			    text : '新增补充问答',
			    iconCls : 'icon-add',
			    handler : function() {
				    this.addAttrValue({});
			    },
			    scope : this
		    }, {
			    ref : 'submitAuditBtn',
			    xtype : 'button',
			    text : '提交审核',
			    iconCls : 'icon-attr-audit-submit',
			    handler : function() {
				    this.doAudit(1)
			    },
			    scope : this
		    }, {
			    ref : 'revokeAuditBtn',
			    xtype : 'button',
			    text : '撤销审核',
			    iconCls : 'icon-attr-audit-submit',
			    handler : function() {
				    this.doAudit(0)
			    },
			    scope : this
		    }, {
			    ref : 'syncBtn',
			    xtype : 'button',
			    text : '发布知识',
			    iconCls : 'icon-attr-audit-submit',
			    handler : function() {
				    this.saveData(true)
			    },
			    scope : this
		    }, {
			    text : '刷新',
			    iconCls : 'icon-refresh',
			    handler : function() {
				    window.location.reload()
			    }
		    }, '-', '业务模板:', {
			    ref : 'bizTplId',
			    xtype : 'combo',
			    triggerAction : 'all',
			    lazyRender : true,
			    mode : 'remote',
			    store : new Ext.data.JsonStore({
				      url : 'biz-template!findAllTpl.action',
				      root : 'data',
				      idProperty : 'id',
				      autoLoad : true,
				      fields : ['name', 'id'],
				      listeners : {
					      load : function() {
						      this.getTopToolbar().bizTplId.setValue(this.getTopToolbar().bizTplId.getValue())
					      },
					      scope : this
				      }
			      }),
			    valueField : 'id',
			    displayField : 'name'
		    }, '标签:', {
			    ref : 'tag',
			    xtype : 'textfield'
		    }, '-', '最新:', {
			    ref : 'newest',
			    inputValue : 1,
			    xtype : 'checkbox'
		    }, '最热:', {
			    ref : 'hotest',
			    inputValue : 2,
			    xtype : 'checkbox'
		    }, '推荐:', {
			    ref : 'recommend',
			    inputValue : 4,
			    xtype : 'checkbox'
		    }, '->', {
			    xtype : 'panel',
			    border : false,
			    bodyStyle : 'background:transparent',
			    html : '<span style="color:red;font-weight:bold;" id="statinfo"></span>'
		    }]

		  this.items = [{
			  xtype : 'panel',
			  html : '<div id="obj_name_ui" style="height:100%;padding-left:15px;padding-top:10px;text-align:left;font:bold 25px Georgia, serif;' + schemaColor
			    + '"><span id="classinfo"></span></div>'
		  }, {
			  xtype : 'panel',
			  itemId : 'objset',
			  style : 'padding:0;margin:0;' + schemaColor,
			  defaults : {
				  xtype : 'textfield',
				  hidden : true
			  },
			  items : [{
				    name : 'obj_objectId'
			    }, {
				    name : 'obj_name'
			    }, {
				    name : 'obj_categoryId'
			    }, {
				    name : 'obj_semanticBlock'
			    }]
		  }, {
			  xtype : 'panel',
			  ref : 'attrContainer',
			  autoHeight : true,
			  items : []
		  }, {
			  xtype : 'fieldset',
			  items : {
				  xtype : 'textarea',
				  fieldLabel : '修改备注',
				  name : 'obj_editComment',
				  width : 400
			  }
		  }]

		  MainPanel.superclass.initComponent.call(this);
		  this.on('afterrender', function() {// schemaColor patch
			    // this.getForm().getEl().dom.style['backgroundColor']
			    // =
			    // 'rgb(240,240,240)';
		    })
	  },
	  height : '100%',
	  width : '100%',
	  region : 'center',
	  border : false,
	  style : schemaColor,
	  bodyStyle : schemaColor,
	  autoScroll : true,
	  defaults : {
		  border : false,
		  anchor : '100%'
	  }
  })
var schemaColor = '';// 'background-color:rgb(240,240,240);'
Ext.onReady(function() {
	  Ext.QuickTips.init();
	  var m = new MainPanel();
	  var viewport = new Ext.Viewport({
		    layout : 'border',
		    items : [m],
		    listeners : {
			    afterrender : function() {
				    m.loadData(contextData)
			    }
		    }
	    });
  });
function getItem(itemid, f) {
	return f.find('itemId', itemid)[0]
}