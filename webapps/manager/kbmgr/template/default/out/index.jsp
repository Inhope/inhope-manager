<%@page import="org.codehaus.jackson.map.ObjectMapper"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>知识库管理</title>
<style type="text/css">
body{font:"宋体";font-size:12px;}
a:link,a:visited{font-size:12px;color:#666;text-decoration:none;}
a:hover{color:#ff0000;text-decoration:underline;}
#Tab{margin:0 auto;width:auto;border-top:none;}
.Menubox{height:28px;}
.Menubox ul{list-style:none;margin:7px 0px;padding:0;position:absolute;}
.Menubox ul li{float:left;background:#CCCCCC;line-height:25px;height:25px;display:block;cursor:pointer;width:110px;text-align:center;color:#fff;font-weight:bold;font-size:15px;border-top:1px solid #D0D0D0;border-left:1px solid #D0D0D0;border-right:1px solid #D0D0D0;}
.Menubox ul li.hover{background:#fff;border-bottom:1px solid #fff;color:#000;font-size:15px;font-weight:bold;}
.Contentbox{clear:both;margin-top:0px;border-top:none;height:181px;padding-top:5px;height:100%;border:none;}
#main_table td,th {border: solid 1px #D0D0D0;}
#qa_table td {border: none;}
.font15 {font-size: 13px;background-color: #f2f2f2;}
.font13 {font-size: 12px;}
</style>
<script>
	var createXHR = function() {
		var _xhr = false;
		try {
			_xhr = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				_xhr = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e2) {
				_xhr = false;
			}
		}
		if (!_xhr && window.XMLHttpRequest)
			_xhr = new XMLHttpRequest();
		return _xhr;
	};
	var contextData = <%=new ObjectMapper().writeValueAsString(request.getAttribute("data"))%>;
	onload = function() {
		var attributes = contextData.attributes;
		var getCmdLink = function(cmdstr) {
			if (!cmdstr)
				return '';
			var cmdMap = {};
			var b = '', args = [];
			var cmd = null;
			for ( var i = 0; i < cmdstr.length; i++) {
				var c = cmdstr.charAt(i);
				if (c == '\\') {
					if (i < cmdstr.length() - 1) {
						b += cmdstr.charAt(i++ + 1);
					} else {
						b += c;
					}
				} else if (c == '(') {// start
					cmd = b.toString();
					b = '';
					args = [];
				} else if (c == ')') {// end
					if (b.length > 0) {
						args.push(b);
						b = '';
					}
					cmdMap[cmd] = args;
					args = [];
				} else if (c == ';') {// new
					if (b.length > 0) {
						cmdMap[b] = null;
						b = '';
					}
					args = [];
				} else if (c == ',') {// arg
					args.push(b);
					b = '';
				} else {// cmd name or arg
					b += c;
				}
			}
			if (cmdMap.ansrefobj) {
				var url = '<%=System.getProperty("robot.context_path")%>'.replace('robot/', '') + 'ahdemo/object-compare!compare.action?names=' + encodeURIComponent(cmdMap.ansrefobj.join(','));
				//url = 'http://192.168.1.43/' + 'ahdemo/object-compare!compare.action?ts=' + new Date().getTime() + '&names=' + encodeURIComponent(cmdMap.ansrefobj.join(','));
				var xhr = createXHR();
				xhr.open('GET', url, false);
				xhr.send();
				eval('var vo = ' + xhr.responseText);
				var clientWidth = document.documentElement.clientWidth;
				if (!clientWidth)
					clientWidth = document.body.clientWidth;
				var divWidth = clientWidth - 150 + 'px';
				var cols = 0;
				for ( var key in vo.headers) {
					cols++;
				}
				var tableWidth = 200 * cols;
				var html = '<div style="width:' + divWidth + ';overflow-x:scroll;"><table width="' + tableWidth + '" bordercolor="#000000" border="1" cellpadding="2" cellspacing="0" style="border-collapse:collapse;margin:5px;"><tr style="background-color:#CCCCCC;">';
				var keys = {};
				var idx = 0;
				html += '<td width="200" style="font-size:12pt;font-weight:bold;text-align:center;">名称</td>';
				for ( var key in vo.headers) {
					var _hideCol = true;
					for ( var i = 0; i < vo.objects.length; i++) {
						if (vo.objects[i].attrAnswers[key]
								&& vo.objects[i].attrAnswers[key].length) {
							_hideCol = false;
							break;
						}
					}
					if (_hideCol)
						vo.headers[key] = null;
				}
				for ( var key in vo.headers) {
					if (vo.headers[key] == null)
						continue;
					html += '<td width="200" style="font-size:12pt;font-weight:bold;text-align:center;">'
							+ vo.headers[key] + '</td>';
					keys[idx++] = key;
				}
				html += '</tr>';
				for ( var i = 0; i < vo.objects.length; i++) {
					html += '<tr><td class="font13_c">'
							+ vo.objects[i].name
							+ '</td>';
					for ( var j = 0; j < idx; j++) {
						var key = keys[j];
						var answers = vo.objects[i].attrAnswers[key];
						var v = '-';
						if (answers
								&& answers.length > 0)
							v = answers[0];
						if (v.length > 180)
							v = v.substring(0, 180)
									+ '...';
						html += '<td class="font13_c">'
								+ v + '</td>';
					}
					html += '</tr>';
				}
				html += '</table></div>';
				return html;
			} else if (cmdMap.p4 && cmdMap.p4[0]) {
				if (cmdMap.p4[0].indexOf('http') == -1)
					cmdMap.p4[0] = '<%=System.getProperty("robot.context_path")%>p4data/' + cmdMap.p4[0] + "/index.html";
				return '<a style="margin-left:10px;" href="#" onclick="window.showModalDialog(\'' + cmdMap.p4[0] + '\', null, \'dialogWidth=600px;dialogHeight=650px;\');return false;">点击查看附件</a>';
			} else if (cmdMap.attachment)
				return '<a style="margin-left:10px;" href="<%=System.getProperty("robot.context_path")%>attachment.action?id=' + cmdMap.attachment[0] +'&name=' + cmdMap.attachment[1] + '">点击查看附件</a>';
			return '';
		};
		
		var attrData = [], qaData = [], attrGroup = {};
		for ( var i = 0; i < attributes.length; i++) {
			var val = '-', cmd = '';
			if (attributes[i].values && attributes[i].values.length){
				val = attributes[i].values[0].value;
				cmd = attributes[i].values[0].cmd;
			}
			if (attributes[i].attributeGroup) {
				var grp = attrGroup[attributes[i].attributeGroup];
				if (!grp) {
					grp = [];
					attrGroup[attributes[i].attributeGroup] = grp;
				}
				if (!attributes[i].attributeName)
					continue;
				grp.push([ attributes[i].attributeName, val, cmd ]);
			} else {
				if (!attributes[i].attributeName)
					qaData.push([ attributes[i].question, val, cmd ]);
				else
					attrData.push([ attributes[i].attributeName, val, cmd ]);
			}
		}
		var renderCell = function(attrData, i ){
			var cmd = attrData[i][2];
			if (cmd && cmd.indexOf('ansrefobj') != -1)
				return getCmdLink(attrData[i][2]);
			else
				return attrData[i][1] + getCmdLink(attrData[i][2]);
		}
		var tabIdx = 0;
		//attrGroup['fred'] = [[1,2,0]];
		var ul = document.getElementById('tabTitles');
		var div = document.getElementById('tabContents');
		var thead = '<thead><th style="font-size:16pt;text-align:center;background-color:#f2f2f2;" id="obj_name" colspan="2">' + contextData.name + '</th></thead>';
		for (var key in attrGroup) {
			ul.innerHTML += '<li id="menu' + (tabIdx + 1) + '" onmouseover="setTab(\'menu\',' + (tabIdx + 1) + ')" ' + (tabIdx == 0 ? 'class="hover"' : '') + '>'+ key + '</li>';
			var tab = document.createElement('div');
			tab.id = 'con_menu_' + (tabIdx + 1);
			if (tabIdx == 0)
				tab.className = 'hover';
			else
				tab.style.display = 'none';
			var _innerHTML = '<table bordercolor="#D0D0D0" border="1" cellpadding="10" cellspacing="0" style="border-collapse:collapse;width:100%;height:auto;"><tbody style="font-size:13px;">';
			var _attrData = attrGroup[key];
			for ( var i = 0; i < _attrData.length; i++) {
				_innerHTML += '<tr><td width="90" align="center" class="font15">'
						+ _attrData[i][0]
						+ '</td><td style="color:#8B4000;line-height:16px;" class="font13">'
						+ renderCell(_attrData, i)
						+ '</td></tr>';
			}
			_innerHTML += '</tbody></table>';
			tab.innerHTML = _innerHTML;
			div.appendChild(tab);
			tabIdx++;
		}
		if (tabIdx > 0)
			document.getElementById('grpHeader').innerHTML = '<table bordercolor="#D0D0D0" border="1" cellpadding="10" cellspacing="0" style="border-collapse:collapse;width:100%;height:auto;">' + thead + '</table>';

		//tabIdx = 0;
		if (tabIdx == 0)  {
			bodyHtml = '<table bordercolor="#D0D0D0" id="main_table" border="1" cellpadding="10" cellspacing="0" style="border-collapse:collapse;width:100%;height:auto;">' + thead + '<tbody style="font-size:13px;">';
			var attrHtml = '';
			for ( var i = 0; i < attrData.length; i++) {
				attrHtml += '<tr><td width="90" align="center" class="font15">'
						+ attrData[i][0]
						+ '</td><td style="color:#8B4000;line-height:16px;" class="font13">'
						+ renderCell(attrData, i)
						+ '</td></tr>';
			}
			var qaHtml = '';
			if (qaData.length > 0) {
				qaHtml = '<tr"><td colspan="2" style="padding:0;">'
						+ '<table style="border-collapse:collapse;width:100%;height:auto;" cellpadding="15" cellspacing="0" id="qa_table"><tr><td class="font15" style="padding:10px;text-align:center;">常见问题</td></tr>';
				for ( var i = 0; i < qaData.length; i++) {
					qaHtml += '<tr style="border-top:solid 1px #D0D0D0;"><td style="line-height:16px;" class="font13"><b>Q</b>: '
							+ qaData[i][0]
							+ '<br/><br/><span style="color:#FF6600;"><b>A</b>: '
							+ qaData[i][1] + '</span><br/>' + getCmdLink(qaData[i][2]) +'</td></tr></tr>';
				}
				qaHtml += '</table></td></tr>';
			}
			bodyHtml += attrHtml + qaHtml + '</tbody></table>';
			document.getElementsByTagName('body')[0].innerHTML = bodyHtml;
		}
	};

	function setTab(name, cursel) {
		var ul = document.getElementById('tabTitles');
		var n = ul.getElementsByTagName('li').length;
		for (var i = 1; i <= n; i++) {
			var menu = document.getElementById(name + i);
			var con = document.getElementById("con_" + name + "_" + i);
			menu.className = i == cursel ? "hover" : "";
			con.style.display = i == cursel ? "block" : "none";
		}
	}
</script>
</head>

<body style="overflow:scroll;">
<div id="Tab">
  <div id="grpHeader"></div>
  <div class="Menubox">
    <ul id="tabTitles">
    </ul>
  </div>
  <div id="tabContents" class="Contentbox"> 
  </div>
</div>
</body>
</html>