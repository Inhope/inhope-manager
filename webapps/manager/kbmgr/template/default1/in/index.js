var STATE_AUD_SUBMIT = 8;
var STATE_AUD_PASS = 31;
var STATE_AUD_REJECT = 17;
var STATE_AUD_RANGE = [STATE_AUD_SUBMIT, STATE_AUD_PASS];

var contextData;
var Dashboard = {
	u : function() {
		return this._usr ? this._usr : this._usr = new LoginUser('kb')
	},
	getModuleFeature : function(key) {
		return window.kb_mdl_ft ? window.kb_mdl_ft[key] : null
	},
	getModuleProp : function(key) {
		return window.kb_mdl_props ? window.kb_mdl_props[key] : null
	},
	setAlert : function(msg, status, delay) {
		if (!status)
			status = 'ok'
		if (!delay)
			delay = 3
		if (!this.msgCt) {
			this.msgCt = Ext.DomHelper.insertFirst(document.body, {
						id : 'msg-div'
					}, true);
			this.msgCt.setStyle('position', 'absolute');
			this.msgCt.setStyle('z-index', 9999);
			this.msgCt.setWidth(300);
		}
		this.msgCt.alignTo(document, 't-t');
		var html = ['<div class="app-msg">', '<div class="x-box-tl"><div class="x-box-tr"><div class="x-box-tc"></div></div></div>',
				'<div class="x-box-ml"><div class="x-box-mr"><div class="x-box-mc"><h3 class="x-icon-text icon-status-' + status + '">', msg, '</h3>', '</div></div></div>',
				'<div class="x-box-bl"><div class="x-box-br"><div class="x-box-bc"></div></div></div>', '</div>'].join('');
		Ext.DomHelper.append(this.msgCt, {
					'html' : html
				}, true).slideIn('t').pause(delay).ghost("t", {
					remove : true
				});
	}
};
var MainPanel = Ext.extend(Ext.FormPanel, {
			addAttrValue : function() {
				var a = {
					valueId : '-' + Ext.id(),
					attributeGroup : '补充问答'
				}
				var fs = this['补充问答qFieldSet']
				var qColPanel = fs.items.get(0)
				var aColPanel = fs.items.get(1)
				var dColPanel = fs.items.get(2)
				var cColPanel = fs.items.get(3)
				var oColPanel = fs.items.get(4)
				var dimpart = this.createDimPart({}, a);
				var createPos = qColPanel.items.getCount()
				qColPanel.items.insert(createPos, Ext.create({
									aid : a.valueId,
									attachment : a,
									fieldLabel : '问题',
									emptyText : '请填写问题',
									value : a.question,
									style : a.values && a.values.length > 1 ? 'margin-bottom:' + ((a.values.length - 1) * 145 + 5) + 'px' : '',// 73*n+5
									xtype : 'textarea',
									height : 140,
									anchor : '98%'
								}))
				aColPanel.items.insert(createPos, Ext.create(dimpart.ans))
				dColPanel.items.insert(createPos, Ext.create(dimpart.dim))
				cColPanel.items.insert(createPos, Ext.create(dimpart.cmd))
				oColPanel.items.insert(createPos, Ext.create(dimpart.op))
				this.doLayout()
			},
			addDimValue : function(attrGroup, aid) {
				var fs = this[attrGroup + 'qFieldSet'];
				var qColPanel = fs.items.get(0)
				var aColPanel = fs.items.get(1)
				var dColPanel = fs.items.get(2)
				var cColPanel = fs.items.get(3)
				var oColPanel = fs.items.get(4)
				var count = 0;
				var createPos;
				var createFromAttr;
				aColPanel.items.each(function(f, i, total) {
							if (f.aid == aid) {
								if (count == 0) {
									createFromAttr = qColPanel.items.get(i)
									count = 1
								}
								if (i == total - 1) {
									createPos = total
									return false;
								}
							} else if (count == 1) {
								createPos = i
								return false;
							}
						})
				if (createPos != null) {
					var dimpart = this.createDimPart({}, createFromAttr.attachment);
					qColPanel.items.insert(createPos, Ext.create({
										xtype : 'label'
									}))
					aColPanel.items.insert(createPos, Ext.create(dimpart.ans))
					dColPanel.items.insert(createPos, Ext.create(dimpart.dim))
					cColPanel.items.insert(createPos, Ext.create(dimpart.cmd))
					oColPanel.items.insert(createPos, Ext.create(dimpart.op))
					this.adjustAttrRowSpan(qColPanel, createPos)
					this.doLayout()
				}
			},
			createDimPart : function(dimv, a, first) {
				var ret = {
					ans : {
						aid : a.valueId,
						dimid : dimv.id,
						xtype : 'htmleditor',
						height : 140 + 27,
						anchor : '98%',
						fieldLabel : first ? (a.attributeName ? a.attributeName : '答案') : '',
						emptyText : '请填写答案',
						value : dimv.value,
						listeners : {
							initialize : function(ed) {
								this.getToolbar().hide();
								Ext.EventManager.on(ed.getWin(), 'blur', function() {
											this.setHeight(140 + 27)
											this.getToolbar().hide()
										}, this);
								Ext.EventManager.on(ed.getWin(), 'focus', function() {
											this.getToolbar().show()
											this.setHeight(140)
										}, this);
							}
						}
					},
					dim : {
						aid : a.valueId,
						dimid : dimv.id,
						xtype : 'dimensionbox',
						value : dimv.dimTagIds,
						anchor : '95%',
						style : 'margin-bottom:120px',
						fieldLabel : '维度'
					},
					cmd : {
						aid : a.valueId,
						dimid : dimv.id,
						xtype : 'cmdchoosercombo',
						value : dimv.cmd,
						anchor : '95%',
						style : 'margin-bottom:120px',
						fieldLabel : '指令'
					}
				}
				ret['op'] = {
					style : 'margin-bottom:124px',
					xtype : 'button',
					tooltip : '新增答案',
					iconCls : 'icon-add',
					handler : function() {
						this.addDimValue(a.attributeGroup, a.valueId)
					},
					scope : this
				}
				return ret
			},
			saveData : function(sync) {
				var d = {}
				var values = this.getForm().getValues();
				// object fields
				for (var key in values) {
					if (key.indexOf('obj_') == 0) {
						var v = values[key]
						key = key.substring(4)
						if (key != 'classId') {
							d[key] = v;
						}
					}
				}
				d.bizTplId = this.getTopToolbar().bizTplId.getValue()
				d.tag = this.getTopToolbar().tag.getValue()
				// attributes
				d.attributes = []
				for (var key in this) {
					var fs = this[key];
					if (key.indexOf('qFieldSet') == -1 || !(fs instanceof Ext.form.FieldSet))
						continue;
					var aid = null;
					var qColPanel = fs.items.get(0)
					var aColPanel = fs.items.get(1)
					var dColPanel = fs.items.get(2)
					var cColPanel = fs.items.get(3)
					var curAttr = null;
					qColPanel.items.each(function(f, i, t) {
								if (f.attachment && f.attachment.valueId != aid) {
									if (curAttr && (!curAttr.values || !curAttr.values.length)) {
										d.attributes.length = d.attributes.length - 1;
									}
									aid = f.attachment.valueId
									curAttr = Ext.apply({}, f.attachment);
									delete curAttr.attributeGroup
									d.attributes.push(curAttr);
									curAttr.question = f.getValue()
									curAttr.values = []
									if (curAttr.valueId && curAttr.valueId.indexOf('-') == 0) {
										delete curAttr.valueId
									}
									if (this.auditState != null)
										curAttr.state = this.auditState
									else if (this.orig_state < 3) {
										curAttr.state = 0
									}
								}
								var dimField = aColPanel.items.get(i);
								if (dimField.getValue().trim()) {
									var dimv = {
										value : aColPanel.items.get(i).getValue(),
										cmd : cColPanel.items.get(i).getValue(),
										dimTagIds : dColPanel.items.get(i).getValue()
									};
									if (dimField.dimid && dimField.dimid.indexOf('-') != 0)
										dimv.id = dimField.dimid
									var db_dimv = this.dimValueOtherPart[dimv.id]
									if (db_dimv) {
										Ext.applyIf(dimv, db_dimv)
									}
									curAttr.values.push(dimv)
								}
							}, this)
				}
				if (curAttr && (!curAttr.values || !curAttr.values.length)) {
					d.attributes.length = d.attributes.length - 1;
				}
				if (this.invisibleAttrs && this.auditState != null) {
					Ext.each(this.invisibleAttrs, function(a) {
								a.state = this.auditState
							}, this)
				}
				d.attributes = d.attributes.concat(this.invisibleAttrs)
				Ext.Ajax.request({
							url : 'ontology-object!save.action',
							params : {
								data : Ext.encode(d),
								ispub : '',
								sync : sync != null ? sync : this.getTopToolbar().saveBtn.sync.checked,
								attrVDeleted : false,
								autoCreateWC : true
							},
							success : function(response) {
								this.getEl().unmask()
								var result = Ext.util.JSON.decode(response.responseText);
								if (!result.success) {
									Ext.Msg.alert("提示", result.message);
									return;
								}
								var d = result.data;
								if (parent.Dashboard)
									parent.Dashboard.setAlert("保存成功!");
								window.location = window.location.toString().replace(/\?.*/ig, "") + '?data={"objectId":"' + d.objectId + '"}';
							},
							failure : function(response) {
								this.getEl().unmask()
								Ext.Msg.alert("错误", '错误信息:<br>' + response.responseText.replace(/\r\n/ig, '<br>'));
								if (cb)
									cb(false)
							},
							scope : this
						});
			},
			doAudit : function(type) {
				if (type) {// to submit
					this.auditState = STATE_AUD_SUBMIT
				} else {// reset
					this.auditState = 0
				}
				this.saveData(false)
			},
			initComponent : function() {
				var attrs = contextData.attributes;
				var classId;
				if (contextData.classes && contextData.classes.length)
					classId = contextData.classes[0].id
				var objFieldCol = [{
							name : 'obj_objectId',
							xtype : 'textfield',
							hidden : true,
							value : contextData.objectId
						}, {
							name : 'obj_name',
							fieldLabel : '业务名称',
							xtype : 'textfield',
							hidden : true,
							value : contextData.name
						}, {
							name : 'obj_categoryId',
							xtype : 'textfield',
							hidden : true,
							value : contextData.categoryId
						}, {
							xtype : 'textfield',
							fieldLabel : '语义块',
							hidden : true,
							name : 'obj_semanticBlock',
							value : contextData.semanticBlock
						}];
				var attrQFieldCol = [];
				var attrAFieldCol = [];
				var attrDimFieldCol = [];
				var attrCmdCol = [];
				var attrOpCol = [];

				var attrQFieldCol_c = [];
				var attrAFieldCol_c = [];
				var attrDimFieldCol_c = [];
				var attrCmdCol_c = [];
				var attrOpCol_c = [];
				var groupFieldSet = {}
				this.invisibleAttrs = []
				this.dimValueOtherPart = {}
				Ext.each(attrs, function(a, i) {
							if (!a.visible) {
								this.invisibleAttrs.push(a)
								return
							}
							if (!a.valueId) {
								a.valueId = '-' + Ext.id()
							}
							var aid = a.valueId;
							var grp = a.attributeGroup;
							if (!grp) {
								grp = a.attributeName ? '属性知识' : '补充问答'
								// if (contextData.name.indexOf('天翼领航_A8_4M') !=
								// -1 && a.attributeName) {
								// // demo debug code>>>
								// if ('套餐介绍 推荐理由 优惠信息
								// 适用地域/客户'.indexOf(a.attributeName) != -1) {
								// grp = '业务介绍'
								// } else if ('套餐基础包 套餐可选包
								// 套餐说明'.indexOf(a.attributeName) != -1) {
								// grp = '资费说明'
								// }
								// // <<<
								// }
								a.attributeGroup = grp;
							}
							if (!groupFieldSet[grp]) {
								groupFieldSet[grp] = {}
								var cols = 'attrQFieldCol attrAFieldCol attrDimFieldCol attrCmdCol attrOpCol'.split(' ');
								for (var j = 0; j < cols.length; j++) {
									groupFieldSet[grp][cols[j]] = [];
								}
							}
							var grpCols = groupFieldSet[grp];
							if (a.attributeName) {
								grpCols.attrQFieldCol.push({
											aid : aid,
											attachment : a,
											value : a.question,
											xtype : 'textfield',
											hidden : true
										})
							} else {
								grpCols.attrQFieldCol.push({
											aid : aid,
											attachment : a,
											fieldLabel : '问题',
											emptyText : '请填写问题',
											style : a.values && a.values.length > 1 ? 'margin-bottom:' + ((a.values.length - 1) * 145 + 5) + 'px' : '',// 73*n+5
											value : a.question,
											xtype : 'textarea',
											height : 140,
											anchor : '98%'
										})
							}
							if (!a.values || !a.values.length) {
								a.values = [{
											id : '-' + Ext.id()
										}]
							}
							Ext.each(a.values, function(dimv, i) {
										var dimpart = this.createDimPart(dimv, a, i == 0);
										if (i > 0) {// occupier
											grpCols.attrQFieldCol.push({
														xtype : 'label'
													})
										}
										this.dimValueOtherPart[dimv.id] = dimv
										grpCols.attrAFieldCol.push(dimpart.ans)
										if (i > 0)
											dimpart.ans.fieldLabel = ''
										grpCols.attrDimFieldCol.push(dimpart.dim)
										grpCols.attrCmdCol.push(dimpart.cmd)
										grpCols.attrOpCol.push(dimpart.op)
									}, this)
						}, this)
				var stateinfosstr = ''
				var a = attrs[0] || {};
				Ext.each(attrs, function(att) {
							if (att.values && att.values.length && att.values[0].value) {
								a = att
								return false;
							}
						})
				if (a.state == STATE_AUD_SUBMIT)
					stateinfosstr = '已提交审核'
				else if (a.state == STATE_AUD_REJECT)
					stateinfosstr = '审核被驳回'
				else if (a.state == STATE_AUD_PASS)
					stateinfosstr = '审核通过'
				else if (a.state == 0)
					stateinfosstr = '未提交审核'
				else if (a.state == 1)
					stateinfosstr = '发布成功'
				else if (a.state == 2)
					stateinfosstr = '发布失败'
				this.orig_state = a.state
				this.tbar = [{
							xtype : 'splitbutton',
							text : '保存',
							ref : 'saveBtn',
							menu : {
								items : [{
											text : '同步',
											checked : false,
											name : 'sync',
											ref : '../sync'
										}]
							},
							iconCls : 'icon-table-save',
							width : 50,
							handler : this.saveData,
							scope : this
						}, {
							xtype : 'button',
							text : '新增补充问答',
							iconCls : 'icon-add',
							handler : function() {
								this.addAttrValue();
							},
							scope : this
						}, {
							ref : 'submitAuditBtn',
							xtype : 'button',
							text : '提交审核',
							iconCls : 'icon-attr-audit-submit',
							handler : function() {
								this.doAudit(1)
							},
							scope : this
						}, {
							ref : 'revokeAuditBtn',
							xtype : 'button',
							text : '撤销审核',
							iconCls : 'icon-attr-audit-submit',
							handler : function() {
								this.doAudit(0)
							},
							scope : this
						}, {
							ref : 'syncBtn',
							xtype : 'button',
							text : '发布知识',
							iconCls : 'icon-attr-audit-submit',
							handler : function() {
								this.saveData(true)
							},
							scope : this
						}, {
							text : '刷新',
							iconCls : 'icon-refresh',
							handler : function() {
								window.location.reload()
							}
						}, '-', '业务模板:', {
							ref : 'bizTplId',
							xtype : 'combo',
							triggerAction : 'all',
							lazyRender : true,
							mode : 'remote',
							value : contextData.bizTplId,
							store : new Ext.data.JsonStore({
										url : 'biz-template!findAllTpl.action',
										root : 'data',
										idProperty : 'id',
										autoLoad : true,
										fields : ['name', 'id'],
										listeners : {
											load : function() {
												this.getTopToolbar().bizTplId.setValue(this.getTopToolbar().bizTplId.getValue())
											},
											scope : this
										}
									}),
							valueField : 'id',
							displayField : 'name'
						}, '标签:', {
							ref : 'tag',
							xtype : 'textfield',
							value : contextData.tag
						}, '->', {
							xtype : 'panel',
							border : false,
							bodyStyle : 'background:transparent',
							html : '<span style="color:red;font-weight:bold;" id="statinfo">' + stateinfosstr + '</span>'
						}]
				this.items = [{
					xtype : 'panel',
					html : '<div style="height:100%;padding-left:15px;padding-top:10px;text-align:left;font:bold 25px Georgia, serif;' + schemaColor + '">' + contextData.name
							+ '<span id="classinfo"></span></div>'
				}, {
					xtype : 'fieldset',
					ref : 'objset',
					style : 'padding:0;margin:0;' + schemaColor,
					bodyStyle : 'padding:0;margin:0;' + schemaColor,
					layout : 'form',
					defaults : {
						anchor : '50%',
						border : false,
						labelWidth : 55
					},
					items : objFieldCol
				}]
				if (!groupFieldSet['补充问答']) {
					groupFieldSet['补充问答'] = {}
					var cols = 'attrQFieldCol attrAFieldCol attrDimFieldCol attrCmdCol attrOpCol'.split(' ');
					for (var j = 0; j < cols.length; j++) {
						groupFieldSet['补充问答'][cols[j]] = [];
					}
				}
				for (var key in groupFieldSet) {
					var gfs = groupFieldSet[key];
					var _cfg;
					if (key == '补充问答') {
						_cfg = {
							xtype : 'fieldset',
							ref : key + 'qFieldSet',
							bodyStyle : schemaColor,
							title : key,
							layout : 'column',
							style : 'margin:5px;border-width:0;border-top-width:1px;' + schemaColor,
							border : true,
							defaults : {
								layout : 'form',
								anchor : '100%',
								border : false,
								bodyStyle : schemaColor,
								labelWidth : 30
							},
							items : [{
										columnWidth : .30,
										items : gfs.attrQFieldCol
									}, {
										columnWidth : .35,
										items : gfs.attrAFieldCol
									}, {
										columnWidth : .15,
										labelWidth : 30,
										items : gfs.attrDimFieldCol
									}, {
										columnWidth : .15,
										labelWidth : 30,
										items : gfs.attrCmdCol
									}, {
										columnWidth : .05,
										items : gfs.attrOpCol
									}]
						}
					} else {
						_cfg = {
							xtype : 'fieldset',
							ref : key + 'qFieldSet',
							title : key,
							border : true,
							layout : 'column',
							style : 'margin:5px;border-width:0;border-top-width:1px;' + schemaColor,
							bodyStyle : schemaColor,
							defaults : {
								layout : 'form',
								anchor : '100%',
								border : false,
								bodyStyle : schemaColor,
								labelWidth : 60
							},
							items : [{
										width : 0,
										hidden : true,
										items : gfs.attrQFieldCol
									}, {
										columnWidth : .55,
										items : gfs.attrAFieldCol
									}, {
										columnWidth : .2,
										labelWidth : 30,
										items : gfs.attrDimFieldCol
									}, {
										columnWidth : .2,
										labelWidth : 30,
										items : gfs.attrCmdCol
									}, {
										columnWidth : .05,
										items : gfs.attrOpCol
									}]
						}
					}
					this.items.push(_cfg)
				}
				MainPanel.superclass.initComponent.call(this);
				this.on('afterrender', function() {// schemaColor patch
							// this.getForm().getEl().dom.style['backgroundColor']
							// =
							// 'rgb(240,240,240)';
						})
			},
			height : '100%',
			width : '100%',
			region : 'center',
			border : false,
			style : schemaColor,
			bodyStyle : schemaColor,
			autoScroll : true,
			defaults : {
				border : false,
				anchor : '100%'
			}
		})
var schemaColor = '';// 'background-color:rgb(240,240,240);'
Ext.onReady(function() {
			Ext.QuickTips.init();
			var viewport = new Ext.Viewport({
						layout : 'border',
						items : [new MainPanel()]
					});
		});