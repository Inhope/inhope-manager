<%@page import="org.codehaus.jackson.map.ObjectMapper"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="res" uri="/WEB-INF/tld/import-res.tld" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="usr" uri="/WEB-INF/tld/login-user.tld"%>
<%@taglib prefix="mdl" uri="/WEB-INF/tld/mdl-ft.tld"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>知识库管理</title>
	<script>
	var contextData = <%=((ObjectMapper)request.getAttribute("jackson")).writeValueAsString(request.getAttribute("data")) %>
	var _dimChooserInitData = eval('(<%=new ObjectMapper().writeValueAsString(request.getAttribute("dimChooserInitData")) %>)');
	</script>
	<usr:checkPerm jsExportVar="kb_check_perm_result" permissionNames="kb"
			dump="true" />
	<usr:name jsExportVar="kb_username"/>
	<mdl:dump name="kb" jsExportVar="kb_mdl_ft"/>
	<mdl:dump name="kb" jsExportVar="kb_mdl_props" type="props"/>
	<%@include file="/commons/extjsvable.jsp" %>
	<link rel="stylesheet" type="text/css" href="<c:url value="/kbmgr/custom.css"/>" />
	<script type="text/javascript" src='<c:url value="/kbmgr/script/ux/TwinTriggerCombo.js"/>'></script>
	<script type="text/javascript" src='<c:url value="/kbmgr/script/obj/DimensionChooser.js"/>'></script>
	<script type="text/javascript" src='<c:url value="/kbmgr/script/obj/CommandChooser.js"/>'></script>
	<script type="text/javascript" src='<c:url value="/kbmgr/script/obj/CommandChooserCombo.js"/>'></script>
	<script type="text/javascript" src='<c:url value="/kbmgr/script/obj/P4Win.js"/>'></script>
	<script type="text/javascript" src='<c:url value="/kbmgr/script/obj/FileUploadCMP.js"/>'></script>
	<script type="text/javascript" src='<c:url value="/kbmgr/template/default/in/index.js"/>'></script>
</head>

<body>
</body>
</html>