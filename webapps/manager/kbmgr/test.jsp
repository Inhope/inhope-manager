<%@page import="com.incesoft.xmas.commons.PropertyPlaceholderRecorder"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@page
	import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="res" uri="/WEB-INF/tld/import-res.tld"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="usr" uri="/WEB-INF/tld/login-user.tld"%>
<%@taglib prefix="mdl" uri="/WEB-INF/tld/mdl-ft.tld"%>
<%
	ApplicationContext ctx = WebApplicationContextUtils
			.getWebApplicationContext(application);
	PropertyPlaceholderRecorder propertyPlaceholder = ctx.getBean(PropertyPlaceholderRecorder.class);
	out.println("propertyPlaceholder.getProperties:"+propertyPlaceholder.getProperties().get("file.transfer.addr1"));
	out.print("<br />");
	out.println("System.getProperty:"+System.getProperty("file.transfer.addr1"));
	out.print("<br />");
	out.println("System.getProperty:"+System.getProperty("file.transfer.addr2"));
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>知识库管理</title>
<usr:checkPerm jsExportVar="kb_check_perm_result" permissionNames="kb"
	dump="true" />
<usr:name jsExportVar="kb_username" />
<mdl:dump name="kb" jsExportVar="kb_mdl_ft" />
<mdl:dump name="kb" jsExportVar="kb_mdl_props" type="props" />
<%@include file="/commons/extjsvable.jsp"%>
</head>

<body>
</body>
</html>