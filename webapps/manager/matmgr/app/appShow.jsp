<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/appselected.css" type="text/css" />
<script type="text/javascript" src="js/adjust.js"></script>
<title>图片–iBotCloud小i机器人智能云服务平台</title>
<script>
	var appMap = [];
	selectClick = function(appId, isClick) {
	    document.getElementById(appId).checked = true;
		document.getElementById('selectedVal').value = appId;
		if (isClick){
			var app = appMap[appId];
			parent.answerRendered({
				id : appId,
				type : 'app',
				data : app
			});
		}
	};
</script>
</head>
<body>
	<div id="mainDiv" class="dialog">
		<div class="dialog_content">
			<div class="dialog_list">
				<input type="hidden" id="selectedVal" /> <input type="hidden"
					id="type" value="p4msg" />
<!-- 				<div class="page_nav"> -->
<!-- 					<%@include file="../nav-templatePage.jsp"%> -->
<!-- 				</div> -->
				<!----------------------循环列表内容------------------------->
				<div class="list_ul">
					<ul class="app-ul">
						<c:forEach items="${pg.result}" var="appDomain" varStatus="s">
							<script>
								var appObj = {
									id : '${appDomain.id}',
									name : '${appDomain.name}',
									description : '${appDomain.description}',
									url : '${appDomain.url}',
									type : '${appDomain.type}',
									args : '${appDomain.args}'
								};
								appMap['${appDomain.id}'] = appObj;
							</script>
							<li class="float-p">
								<div class="left checkbox">
									<c:choose>
										<c:when test="${appDomain.id==param.materialId}">
											<input type="radio" value="${appDomain.id}" name="file" id="${appDomain.id}"
												checked="checked" onclick="selectClick(this.value,true)">
												<script>
													selectClick('${appDomain.id}', false);
												</script>
										</c:when>
										<c:otherwise>
											<input type="radio" value="${appDomain.id}" name="file" id="${appDomain.id}"
												onclick="selectClick(this.value,true)">
										</c:otherwise>
									</c:choose>
								</div>
								<div class="left file_infor">
									<div class="left" style="margin-top: -18px;">
										<div class="app_icon f_l">
											<a href="#"> <c:choose>
													<c:when test="${appDomain.type==0}">
														<img src="images/app/${appDomain.icon}" width="60"
															height="60" onclick="selectClick('${appDomain.id}',true)">
													</c:when>
													<c:otherwise>
														<img src="images/appicon/app60/${appDomain.icon}"
															width="60" height="60" onclick="selectClick('${appDomain.id}',true)">
													</c:otherwise>
												</c:choose> </a>
										</div>
										<div class="app_intro f_l">
											<h2>
											    <c:choose>
													<c:when test="${appDomain.type==0}">
												<a href="#">${appDomain.name}</a>
												</c:when>
													<c:otherwise>
													
													<a href="#">${appDomain.name}</a><span style="font-size:10px;color:green;">(自定义)</span>
													</c:otherwise>
												</c:choose>
											</h2>
											<p>${appDomain.description}</p>
										</div>
									</div>
								</div></li>
						</c:forEach>
					</ul>
				</div>
				<div class="page_nav"><%@include
						file="../nav-templatePage.jsp"%></div>
			</div>
		</div>
	</div>
</body>
</html>