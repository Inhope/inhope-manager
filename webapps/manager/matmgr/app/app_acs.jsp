<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link type="text/css" rel="stylesheet" href="../css/apiUser.css" />
<link type="text/css" rel="stylesheet" href="../css/account.css" />
<link rel="stylesheet" type="text/css" href="../css/app.css">
<link rel="stylesheet" type="text/css" href="../css/css.css">
<link rel="stylesheet" type="text/css" href="../css/acs.css">
<link type="text/css" rel="stylesheet" href="../css/jquery-ui.css" />
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery-ui.js"></script>
<script type="text/javascript" src="../js/tab.js"></script>
<script type="text/javascript">
	onload = function() {
		addTab('content', '管理', null, true);
	}
</script>
<title>应用详情-人工坐席</title>
</head>
<body>
	<div style="padding : 15px 0;" class="con">
		<div style="width:776px; float:left; margin-left:10px;">
			<div style="width:776px; margin:0px 0 5px 0;">
				<div style="height:0;">
					<div id="app_id" style="display:none;">${app.id}</div>
					<div id="app_name" class="title_mid left"
						style="display:none;width:759px;">${app.name}</div>
				</div>
				<div class="right_con" style="padding:0;">
					<div class="Contentboxtwo ov_fl" style="width:774px;">
						<div class="Apply-one p-t20">
							<div class="Apply-Icon f_l">
								<img src="../images/app/${app.icon}">
							</div>
							<div class="Apply-Iconcn1 f_l" style="padding-top:10px;">
								<h2>${app.name}</h2>
								<div class="Apply-Iconcn2 f_l"></div>
							</div>
							<div class="Apply-Iconcn4 f_r" style="margin-right:20px;">
							</div>
						</div>
						<div id="detail_content" style="padding:10px 0;">
							<div class="Menuboxtwo">
								<ul id="tabs">
								</ul>
							</div>
							<div id="intro__tab" class="apptab" style="height:1400px;">
								<%@include file="app_intro_part.jsp"%>
								<div class="Apply-Intro"></div>
							</div>
							<div id="content__tab" class="Contentboxdiv1 apptab"
								style="display:block">
								<div class="Apply-twocn ov_fl">
									<div class="Apply-twoform p-b20 CM-dsp">
										<h3>人工客服项目设置</h3>
										<div class="Apply-twoformcn1 CM-dsp">
											<div class="Apply-twoformp2 f_l" id="acsVistorLink">
												<p class="f_l" style="padding:3px 10px;margin-left:200px;">
													<a style="font-size:12px;" class="App-button01"
														href="../../acsmgr/" target="_blank"><b>点击进入客服页面</b> </a>
												</p>
											</div>
										</div>
									</div>
								</div>
								<div class="Apply-twocn ov_fl">
									<div class="Apply-twoform p-b20 CM-dsp">
										<h3>状态回复设置</h3>
										<div class="Apply-twoformcn1 CM-dsp">
											<p class="Apply-twoformp1 f_l">
												<span class="c5">状态</span>
											</p>
											<div class="Apply-twoformp2 f_l">编辑回复内容（不能为空）</div>
										</div>
										<div class="Apply-twoformcn1 CM-dsp">
											<p class="Apply-twoformp1 f_l">连接中</p>
											<div class="Apply-twoformp2 f_l">
												<input type="text" maxlength="30" style="width:300px;"
													class="Apply-input1" name="acsConnectingPrompt" value="" />
											</div>
										</div>
										<div class="Apply-twoformcn1 CM-dsp">
											<p class="Apply-twoformp1 f_l">连接成功</p>
											<div class="Apply-twoformp2 f_l">
												<input type="text" maxlength="30" style="width:300px;"
													class="Apply-input1" name="acsConnSuccPrompt" value="" />
											</div>
										</div>
										<div class="Apply-twoformcn1 CM-dsp">
											<p class="Apply-twoformp1 f_l">连接失败</p>
											<div class="Apply-twoformp2 f_l">
												<input type="text" maxlength="30" style="width:300px;"
													class="Apply-input1" name="acsConnFailPrompt" value="" />
											</div>
										</div>
										<div class="Apply-twoformcn1 CM-dsp">
											<p class="Apply-twoformp1 f_l">用户退出</p>
											<div class="Apply-twoformp2 f_l">
												<input type="text" maxlength="30" style="width:300px;"
													class="Apply-input1" name="acsConnClosedPrompt" value="" />
											</div>
										</div>
										<div class="Apply-twoformcn1 CM-dsp">
											<p class="Apply-twoformp1 f_l">客服离开</p>
											<div class="Apply-twoformp2 f_l">
												<input type="text" maxlength="30" style="width:300px;"
													class="Apply-input1" name="acsLeavePrompt" value="">
											</div>
										</div>
										<div class="Apply-twoformcn1 CM-dsp">
											<p class="Apply-twoformp1 f_l">客服关闭(拒绝)</p>
											<div class="Apply-twoformp2 f_l">
												<input type="text" maxlength="30" style="width:300px;"
													class="Apply-input1" name="acsConnBeClosedPrompt" value="" />
											</div>
										</div>
										<div class="Apply-twoformcn1 CM-dsp">
											<p class="Apply-twoformp1 f_l">客服不在线</p>
											<div class="Apply-twoformp2 f_l">
												<p class="A-pr5 f_l A-Lh28 t-l A-pr10">
													<select
														style="width:312px;height:28px;border: 1px solid #cccccc;"
														name="acsNotInSelect" onchange="selectChangeState()">
														<option selected="selected" value="1">工作日(周一至周五,国定假期外)</option>
														<option value="2">工作日(周一至周六,国定假期外)</option>
														<option value="3">节假日(周六,周天,国定假期)</option>
														<option value="4">全年无休</option>
														<option value="5">客服不在线，请和机器人进行交流！</option>
														<option value="6">客服正在从火星赶来的路上，千万不要生气哟！</option>
													</select>
												</p>
												<textarea name="acsNotAvailablePrompt" cols="3" rows="3"
													style="width:306px;color: #555;font-size: 12px;border: 1px solid #cccccc;">
					</textarea>
											</div>
										</div>

										<div class="Apply-twoformcn1 CM-dsp">
											<p class="Apply-twoformp1 f_l"></p>
											<div class="Apply-twoformp2 f_l">
												<a style="font-size:12px;" class="wx-button02"
													href="javascript:csReplyStateSave()"><b>保 存</b> </a>&nbsp;&nbsp;&nbsp;&nbsp;
												<a style="font-size:12px;" class="wx-button02"
													href="javascript:saveDefaultMessageResource()"><b>恢复默认</b>
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<%
	String isStartUpAcsServer = (String)request.getAttribute("isStartUp");
	if("true".equals(isStartUpAcsServer)){
	   out.print("<script type='text/javascript' src='../js/acs.js'>document.getElementById('acsVistorLink').style.display='block';</script>");
	}else{
	   out.print("<script type='text/javascript'>document.getElementById('acsVistorLink').style.display='none';alert('人工坐席服务未启动!');</script>");
	}
%>

</html>