<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link type="text/css"  rel="stylesheet" href="../css/apiUser.css"/>
<link rel="stylesheet" type="text/css" href="../css/app_edit.css">
<link type="text/css"  rel="stylesheet" href="../css/jquery-ui.css"/>
<style>
.doc-body {
    line-height: 20px;
    width: 660px;
}
.hd {
    margin-bottom: 10px;
    position: relative;
}
.hd h2 {
    font-size: 15px;
    padding: 0 0 10px 3px;
}
.hd h2, .hd h3 {
    border-bottom: 1px solid #E2E2E2;
    font-weight: bold;
}
.colmain {
    display: table-cell;
    padding: 25px 35px;
	float:left;
}

.item-list {
    margin-left: 50px;
    padding-bottom: 20px;
}

.item-list ul {
    padding-bottom: 10px;
}

.item-list li {
    line-height: 300%;
}

.doc-part {
    padding-bottom: 20px;
}

.colmain .bd {
    padding: 15px 0 15px 15px;
}

.doc-part .bd li {
    padding-bottom: 5px;
}
.main-wrap li {
    line-height: 180%;
}
.btn-txt, .medi-btn {
    background: url("../images/apibtn.png") no-repeat scroll 0 0 transparent;
    display: inline-block;
}
.medi-btn {
    background-position: right -276px;
    padding-right: 1px;
}

.doc-part .bd li {
    padding-bottom: 5px;
}
.doc-part th {
    border: 1px solid #CCCCCC;
    font-weight: bolder;
    line-height: 20px;
    text-align: center;
}

.doc-part td {
    border: 1px solid #CCCCCC;
    line-height: 25px;
    padding-left: 5px;
    text-align: left;
}

.doc-part pre {
    background-color: #F9F9F9;
    border: 1px dashed #2F6FAB;
    color: black;
    line-height: 1.1em;
    overflow-x: auto;
    overflow-y: hidden;
    padding: 1em;
    width: 95%;
}
</style>
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery-ui.js"></script>
<script type="text/javascript" src="../js/jquery.dotdotdot.min.js"></script>
<script type="text/javascript" src="../js/jsrender.min.js"></script>
<script type="text/javascript" src="../js/tab.js"></script>
<script>
$(document).ready(function() {
	//addTab
	$('div[id$=__tab]').hide().appendTo($('#tabcontent_container'))
	var id = '${id}';
	var saveorupdate = !'${name}';
	addTab('appeditor',(saveorupdate?'创建':'修改')+'应用');
	addTab('doc','开发文档');
	
	//activateTab(id||location.toString().indexOf('saveCustom')!=-1?'appeditor':'func')
	activateTab('appeditor')
	//edit custom
	if (location.toString().indexOf('f=1') != -1){
		alert('保存成功')
	}
	var args = $.parseJSON($('textarea[name=args]').val())
	if (!args) args=[{},{}]
	$('#argsContainer').append($($.templates("#argTpl").render(args)))
	var iconid = $('input[name=icon]').val()
	if (iconid)
		$('#iconImg').attr('src','../images/appicon/app34/'+iconid.replace('imageId=','')).show()
		
	$('textarea[name=description]').bind('keyup blur paste',function(){
		var v = $(this).val();
		var maxlength = parseInt($(this).attr('maxlength'))
		var remain=maxlength- v.length
		if (remain <= 0) {
			remain = 0;
			$(this).val(v.substring(0,maxlength))
		}
		$('#words_left').html(remain)
	}).blur()
	//validation
	var target = $('#validationError').attr('target')
	var msg= $('#validationError').attr('msg')
	if (target){
		markInvalid($('[name='+target+']'),msg)
		alert(msg)
	}
	if (saveorupdate) 
		randomToken();
});
function markInvalid(target,msg){
	if (msg != null){
		target.addClass('validation_error')
		target.attr('title',msg)
	}else{
		target.removeClass('validation_error')
		target.attr('title','')
	}
}
function dosubmit(){
	var err = []
	//common fields
	$.each('id name icon url token'.split(' '),function(i,n){
		var f=$('[name='+n+']')
		var v = f.val()?$.trim(f.val()):'';
		var errlen = err.length
		if (!v) {
			err.push(f.attr('vname')+'不能为空!')
		}else {
			if (n == 'url'){
				if (v.indexOf('http://') != 0 && v.indexOf('https://') != 0){
					err.push(f.attr('vname')+'格式不对（必须以http或者https开始）')
				}			
			}
		}
		if (errlen != err.length){
			markInvalid(f,err[err.length-1])
		}else{
			markInvalid(f)
		}
	})
	
	//arg fields
	var argsArray = []
	$('.argGroup').each(function(){
		var arg_name_field = $('input[name=arg_name]',$(this));
		var name = arg_name_field.val()
		var optional = $('input:checked',$(this)).length
		var title = $('input[name=arg_title]',$(this)).val()
		if (name){
			if (!/^[a-zA-Z][a-zA-Z0-9_]*$/.test(name)){
				err.push('参数名称 '+name+' 不合法（必须以字母开始，包含字母、数字或下划线）')
				markInvalid(arg_name_field,err[err.length-1])
			}else{
				argsArray.push({name:name,optional:optional,title:title?title:''})
			}
		}else{
			if (title){
				err.push('参数描述 '+ title+' 的 参数名称 不能为空!')
				markInvalid(arg_name_field)
			}
		}
	})
	if (argsArray.length)
		$('textarea[name=args]').val(JSON.stringify(argsArray))
	else
		$('textarea[name=args]').val('')
	
	if (err.length){
		alert(err.join('\n'))
		return;
	}
	document.getElementById('form1').submit()
}
function add_arg(){
	if ($('.argGroup').length < 10)
		$('#argsContainer').append($($.templates("#argTpl").render([{}])))
}
function op_del_arg(link){
	$(link).parents('.argGroup').remove()
}
function selecticon(link){
	var iconid = $(link).children('img').attr('src').replace(/.*\//,'')
	$('input[name=icon]').val(iconid)
	$('#iconImg').attr('src','../images/appicon/app34/'+iconid).show()
	$('#iconselector').hide()
}
function randomToken(){
	var v = ''
	for(var i=0;i<8;i++){
		var c = parseInt(Math.random()*(26+10))
		if (c < 10){
			v += c 
		}else{
			v += String.fromCharCode(c-10+97)
		}
	}
	$('[name=token]').val(v)
}
</script>
<title></title>
</head>
<body style="background-color:#fff;">
<div>
<%-- <%@include file="header.jsp" %> --%>

<div class="con">
<%-- <div style="width:213px; float:left;">
	<%@include file="menu.jsp" %>
</div> --%>
 <div style="width:776px; float:left; margin-left:10px;">
	<div style="width:776px; margin:10px 0 5px 0;">
		<div style="height:29px;">
			<div class="left"><img src="../images/title_left.gif"  style="width:9px;"/></div>
			<div class="title_mid left" style="width:759px;">自定义应用</div>
			<div class="left"><img src="../images/title_right.gif" style="width:8px;"/></div>
   		</div>
		<div class="right_con" style="padding:0;">
			<div style="width:772px;padding:0px 1px 0px 1px;position:relative;">
				<div class="Menuboxtwo" style="width:763px">
					<ul id="tabs">
					</ul>
				</div>
				<div id="tabcontent_container" style="clear:both"></div>
			</div>
		</div>
</div>
<p class="clearbh"></p>
</div>
</div>
</div>
<%-- <%@include file="../footer.jsp" %> --%>
<div style="display:none" class="dialog-repo">

<div id="dialog-confirm" title="确认" style="overflow:hidden">
  <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span><span id="dialog-confirm-content"></span></p>
</div>

<div id="dialog-alert" title="提示">
  <p><span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 7px 20px 0;"></span><span id="dialog-alert-content"></span></p>
</div>

</div>
<!-- 说明文档开始 -->
<!-- 
<div id="func__tab" class="Contentboxdiv1" style="height:450px;padding:0;<c:if test="${not empty accList}">display:none;</c:if>">
							<div class="channel-logo1 f_l"><img src="../images/customapp_big.png"></div>
							<div class="ASK-content">
	                        	<div class="ASK-cn1" style="width:500px;text-align:left;padding-left:20px;position:relative;margin-right:10px;">
	                        		<p style="font-size:14px;padding-top:25px;">
	                        		自定义应用是iBot Cloud为开发者提供的拓展答案表现形式，达成开发者的自身业务快速对接的应用形式。通过循序应用开发规范，开发者的应用可以被iBot Cloud问答引擎调用，并通过自定义知识将用户问题快速指向开发者提供的业务接口，帮助用户达成业务目标。
	                        		</p>
	                        	</div>
                     		</div>
						</div>
						 -->
<div id="appeditor__tab">
<!--======内容开始======-->
<input type="hidden" id="validationError" target="${validationError.target}" msg="${validationError.message}">
<form id="form1" action="app!saveCustom.action" method="POST">
<input type="hidden" name="icon" value="${icon}" vname="图标">
<textarea style="display:none" name="args">${args}</textarea>
<p style="width:100%;height:10px;"></p>

<div class="Apply-content01" style="position:relative;">
	<h2 class="Apply-title01">基本设置</h2>
	<div class="Apply-formcn01">
    	<div class="Apply-form-pp1 p-t10">
        	<p class="Apply-formp1 f_l">应用ID：</p>
            <p class="Apply-formp2 f_l"><input ${empty id?'':'readonly="true"' } type="text" maxlength="30" style="width:300px;" class="logincn-input1" name="id" value="${id}" vname="应用ID"></p>
        </div>
    	<div class="Apply-form-pp1 p-t10">
        	<p class="Apply-formp1 f_l">应用名称：</p>
            <p class="Apply-formp2 f_l"><input type="text" maxlength="10" style="width:300px;" class="logincn-input1" name="name" value="${name}" vname="应用名称"></p>
        </div>
        <div class="Apply-form-pp1 p-t10">
        	<p class="Apply-formp1 f_l">应用图标：</p>
            <div class="Apply-formicon f_l">
            	<p class="Apply-formp2 p-b10 p-t5">
            	<a style="font-size:12px;" class="wx-button02" href="javascript:void(0)" onclick="$('#iconselector').show()"><b>选择图标</b></a>
            	<div style="position:relative">
            		<ul class="Apply-formicon02" style="display:none;" id="iconselector">
				    	<li><a href="javascript:void(0)" onclick="selecticon(this)"><img src="../images/appicon/app34/1.png"></a></li>
				    	<li><a href="javascript:void(0)" onclick="selecticon(this)"><img src="../images/appicon/app34/2.png"></a></li>
				    	<li><a href="javascript:void(0)" onclick="selecticon(this)"><img src="../images/appicon/app34/3.png"></a></li>
				    	<li><a href="javascript:void(0)" onclick="selecticon(this)"><img src="../images/appicon/app34/4.png"></a></li>
				    	<li><a href="javascript:void(0)" onclick="selecticon(this)"><img src="../images/appicon/app34/5.png"></a></li>
				    	<li><a href="javascript:void(0)" onclick="selecticon(this)"><img src="../images/appicon/app34/6.png"></a></li>
				    	<li><a href="javascript:void(0)" onclick="selecticon(this)"><img src="../images/appicon/app34/7.png"></a></li>
				    	<li><a href="javascript:void(0)" onclick="selecticon(this)"><img src="../images/appicon/app34/8.png"></a></li>
				    	<li><a href="javascript:void(0)" onclick="selecticon(this)"><img src="../images/appicon/app34/9.png"></a></li>
				    	<li><a href="javascript:void(0)" onclick="selecticon(this)"><img src="../images/appicon/app34/10.png"></a></li>
				    	<div style="clear:both;height: 20px"><a href="javascript:void(0)" onclick="$('#iconselector').hide()" style="float:right;margin-right: 6px">[关闭]</a></div>
				    </ul>
            	</div>
            	<p class="Apply-formp2" ><img id="iconImg" style="display:none"></p>
            	</p>
            </div>
        </div>
        
        <div class="Apply-form-pp1 p-t10">
        	<p class="Apply-formp1 f_l">应用介绍：</p>
            <div class="Apply-formicon f_l">
            	<p class="Apply-formp2 f_l"><textarea name="description" cols="" rows="" class="Apply-input01" style="resize:none;width:315px;" value="" maxlength="120" vname="应用介绍">${description}</textarea></p>
                <p class="Apply-formp3 f_l">还能输入 <span id="words_left"></span> 个字</p>
            </div>    
        </div>
    </div>
    <p class="clearbh"></p>
    
    <h2 class="Apply-title01">应用设置</h2>
	<div class="Apply-formcn01">
    	<div class="Apply-form-pp1 p-t10">
        	<p class="Apply-formp1 f_l">URL：</p>
            <p class="Apply-formp2 f_l" style="padding-bottom:10px;"><input type="text" maxlength="100" style="width:300px;" class="logincn-input1" name="url" value="${url}" vname="URL"><img src="../images/help.png" title="${testURL}"/></p>
        </div>
        <div class="Apply-form-pp1 p-t10" style="display:none">
        	<p class="Apply-formp1 f_l">Token：</p>
            <div class="Apply-formicon f_l">
            	<p class="Apply-formp4 p-b10 p-t5 f_l"><input name="token" type="text" value="${token}" vname="Token"></p>
            	<p class="Apply-formp5 f_l"><a href="javascript:void(0)" onclick="randomToken()" target="_blank" class="link3">生成Token</a></p>
            </div>
            <p class="Apply-formp2 f_l p-t10"><a href="#" target="_blank" class="link3">URL以及Token有什么用？</a></p>
        </div>
    </div>
    <p class="clearbh"></p>
    
    <h2 class="Apply-title01">参数设置 [<a href="javascript:void(0)" onclick="add_arg()">添加参数</a>]</h2>
	<div class="Apply-formcn01" id="argsContainer">
		
    </div>
    <p class="clearbh"></p>
    
    <div class="Apply-form-pp1 p-t10">
       	<p class="Apply-formp1 f_l">&nbsp;</p>
           <p class="Apply-formp2 f_l"><a href="javascript:void(0)" onclick="dosubmit()" class="wx-button02" style="font-size:12px;"><b>提 交</b></a></p>
       </div>
</div>


<!--======内容结束======-->
</form>
</div>						
<div class="colmain" id="doc__tab">
						   		<div class="doc-body">
					<div class="doc-part">
						<div class="bd">
							<ul>
								<li>
									描述：
									<span> 自定义应用开发帮助，说明应用开发规则以及交互模式。</span>
								</li>
							</ul>
						</div>
					</div>
					
					<div class="doc-part">
						<div class="hd">
							<h2>请求参数</h2>
						</div>
						<div class="bd">
							<ul>
								<li>
									<table border="1" cellspacing="0" cellpadding="0" width="100%">
										<tbody>
											<tr>
												<th width="15%">
													元素名
												</th>
												<th width="40%">
													说明
												</th>
												<th width="45%">
													示例
												</th>
											</tr>
											<tr>
												<td>
													ask_app
												</td>
												<td>
													应用ID
												</td>
												<td>
													例如：“joke”
												</td>
											</tr>
											<tr>
												<td>
													ask_user
												</td>
												<td>
													用户ID
												</td>
												<td>
													例如：“tom”
												</td>
											</tr>
											<tr>
												<td>
													ask_platform
												</td>
												<td>
													访问平台
												</td>
												<td>
													例如：“android”
												</td>
											</tr>
											<tr>
												<td>
													ask_input
												</td>
												<td>
													请求问题
												</td>
												<td>
													例如：“你叫什么名字”
												</td>
											</tr>
											<tr>
												<td>
													ask_segments
												</td>
												<td>
													分词结果
												</td>
												<td>
													例如：“你|叫|什么|名字”
												</td>
											</tr>
											<tr>
												<td>
													param1
												</td>
												<td>
													应用自定义的参数
												</td>
												<td>
												</td>
											</tr>
											<tr>
												<td>
													param2
												</td>
												<td>
													应用自定义的参数
												</td>
												<td>
												</td>
											</tr>
											<tr>
												<td>
													...
												</td>
												<td>
													应用自定义的参数
												</td>
												<td>
												</td>
											</tr>
										</tbody>
									</table>
								</li>
							</ul>
						</div>
					</div>
					
					<!-- 未完成响应说明 -->
					<div class="doc-part">
						<div class="hd">
							<h2>响应说明</h2>
						</div>
						<div class="bd">
							<ul>
								<li>
									<table border="1" cellspacing="0" cellpadding="0" width="100%">
										<tbody>
											<tr>
												<th width="15%">
													元素名
												</th>
												<th width="40%">
													说明
												</th>
												<th width="45%">
													示例
												</th>
											</tr>
											<tr>
												<td>
													Response
												</td>
												<td>
													应用响应
												</td>
												<td>
												</td>
											</tr>
											<tr>
												<td>
													FlowPriority
												</td>
												<td>
													请求处理方式
												</td>
												<td>
													0：正常流程；1：下次请求优先处理
												</td>
											</tr>
											<tr>
												<td>
													ProcessResult
												</td>
												<td>
													请求处理结果
												</td>
												<td>
													0：未处理；1：处理
												</td>
											</tr>
											<tr>
												<td>
													Message
												</td>
												<td>
													消息内容（‘text’：文本、‘imgtxt’：图文、‘img’：图片、‘music’：音频、‘video’：视频、‘app’:应用）
												</td>
												<td>
													
												</td>
											</tr>
										</tbody>
									</table>
								</li>
							</ul>
						</div>
					</div>
					

					
					<div class="doc-part">
						<div class="hd">
							<h2>调用示例(请求)</h2>
						</div>
						<div class="bd">
							<pre>
POST /robot/ask.do HTTP/1.1
Cache-Control: no-cache
Pragma: no-cache
Host: aaa.bbb.ccc
Connection: Keep-Alive
Content-Length: xxx
Content-Type:application/x-www-form-urlencoded; charset=UTF-8

ask_app=joke&amp;ask_user=floyd&amp;ask_platform=android&amp;ask_input=你叫什么名字&amp;ask_segments=你|叫|什么|名字&amp;param1=value1&amp;param2=value2&...
							</pre>
						</div>
					</div>
					
					<div class="doc-part">
						<div class="hd">
							<h2>调用示例(响应)</h2>
						</div>
						<div class="bd">
							<pre>

HTTP/1.1 200 OK
Cache-Control: no-cache
Pragma: no-cache
Content-Type: application/xml; charset=utf-8
Content-Length: xxx

&lt;?xml version="1.0" encoding="utf-8"?&gt;
&lt;Response&gt;
	&lt;FlowPriority&gt;0&lt;/FlowPriority&gt;
	&lt;ProcessResult&gt;1&lt;/ProcessResult&gt;
	&lt;Message type="text"&gt;
		xxxxxx
	&lt;/Message&gt;
&lt;/Response&gt;

							</pre>
						</div>
						<div class="bd">
							<pre>

HTTP/1.1 200 OK
Cache-Control: no-cache
Pragma: no-cache
Content-Type: application/xml; charset=utf-8
Content-Length: xxx

&lt;?xml version="1.0" encoding="utf-8"?&gt;
&lt;Response&gt;
	&lt;FlowPriority&gt;0&lt;/FlowPriority&gt;
	&lt;ProcessResult&gt;1&lt;/ProcessResult&gt;
	&lt;Message type="imgtxt"&gt;
	&lt;Articles&gt;
	 &lt;item&gt;
	 &lt;Title&gt;&lt;![CDATA[title1]]&gt;&lt;/Title&gt; 
	 &lt;Description&gt;&lt;![CDATA[description1]]&gt;&lt;/Description&gt;
	 &lt;PicUrl&gt;&lt;![CDATA[picurl]]&gt;&lt;/PicUrl&gt;
	 &lt;Url&gt;&lt;![CDATA[url]]&gt;&lt;/Url&gt;
	 &lt;/item&gt;
	 &lt;item&gt;
	 &lt;Title&gt;&lt;![CDATA[title]]&gt;&lt;/Title&gt;
	 &lt;Description&gt;&lt;![CDATA[description]]&gt;&lt;/Description&gt;
	 &lt;PicUrl&gt;&lt;![CDATA[picurl]]&gt;&lt;/PicUrl&gt;
	 &lt;Url&gt;&lt;![CDATA[url]]&gt;&lt;/Url&gt;
	 &lt;/item&gt;
	&lt;/Articles&gt;
	&lt;/Message&gt;
&lt;/Response&gt;

							</pre>
						</div>
						<div class="bd">
							<pre>

HTTP/1.1 200 OK
Cache-Control: no-cache
Pragma: no-cache
Content-Type: application/xml; charset=utf-8
Content-Length: xxx

&lt;?xml version="1.0" encoding="utf-8"?&gt;
&lt;Response&gt;
	&lt;FlowPriority&gt;0&lt;/FlowPriority&gt;
	&lt;ProcessResult&gt;1&lt;/ProcessResult&gt;
	&lt;Message type="img"&gt;
	&lt;Title&gt;&lt;![CDATA[TITLE]]&gt;&lt;/Title&gt;
	&lt;Description&gt;&lt;![CDATA[DESCRIPTION]]&gt;&lt;/Description&gt;
	&lt;Url&gt;&lt;![CDATA[url]]&gt;&lt;/Url&gt;
	&lt;/Message&gt;
&lt;/Response&gt;

							</pre>
						</div>
						<div class="bd">
							<pre>

HTTP/1.1 200 OK
Cache-Control: no-cache
Pragma: no-cache
Content-Type: application/xml; charset=utf-8
Content-Length: xxx

&lt;?xml version="1.0" encoding="utf-8"?&gt;
&lt;Response&gt;
	&lt;FlowPriority&gt;0&lt;/FlowPriority&gt;
	&lt;ProcessResult&gt;1&lt;/ProcessResult&gt;
	&lt;Message type="music"&gt;
	&lt;Title&gt;&lt;![CDATA[TITLE]]&gt;&lt;/Title&gt;
	&lt;Description&gt;&lt;![CDATA[DESCRIPTION]]&gt;&lt;/Description&gt;
	&lt;Url&gt;&lt;![CDATA[MUSIC_Url]]&gt;&lt;/MusicUrl&gt;
	&lt;HQUrl&gt;&lt;![CDATA[HQ_MUSIC_Url]]&gt;&lt;/HQMusicUrl&gt;
	&lt;Thumbnail&gt;&lt;![CDATA[Thumbnail_Url]]&gt;&lt;/Thumbnail&gt;
	&lt;/Message&gt;
&lt;/Response&gt;

							</pre>
						</div>
						<div class="bd">
							<pre>

HTTP/1.1 200 OK
Cache-Control: no-cache
Pragma: no-cache
Content-Type: application/xml; charset=utf-8
Content-Length: xxx

&lt;?xml version="1.0" encoding="utf-8"?&gt;
&lt;Response&gt;
	&lt;FlowPriority&gt;0&lt;/FlowPriority&gt;
	&lt;ProcessResult&gt;1&lt;/ProcessResult&gt;
	&lt;Message type="video"&gt;
	&lt;Title&gt;&lt;![CDATA[TITLE]]&gt;&lt;/Title&gt;
	&lt;Description&gt;&lt;![CDATA[DESCRIPTION]]&gt;&lt;/Description&gt;
	&lt;Url&gt;&lt;![CDATA[VIDEO_Url]]&gt;&lt;/Url&gt;
	&lt;Thumbnail&gt;&lt;![CDATA[Thumbnail_Url]]&gt;&lt;/Thumbnail&gt;
	&lt;/Message&gt;
&lt;/Response&gt;

							</pre>
						</div>
						<div class="bd">
							<pre>

HTTP/1.1 200 OK
Cache-Control: no-cache
Pragma: no-cache
Content-Type: application/xml; charset=utf-8
Content-Length: xxx

&lt;?xml version="1.0" encoding="utf-8"?&gt;
&lt;Response&gt;
	&lt;FlowPriority&gt;0&lt;/FlowPriority&gt;
	&lt;ProcessResult&gt;1&lt;/ProcessResult&gt;
	&lt;Message type="app"&gt;
		&lt;Service&gt;joke(sth)&lt;/Service&gt;
		&lt;Name&gt;menu&lt;/Name&gt;
		&lt;EXURL&gt;&lt;/EXURL&gt;
		&lt;Args&gt;
			&lt;Arg name=""&gt;&lt;/Arg&gt;
		&lt;/Args&gt;
	&lt;/Message&gt;
&lt;/Response&gt;

							</pre>
						</div>
					</div>
					
				</div>		   		
						   		
						   </div>
						   		
						   		<!-- 自定义应用doc  end -->
</body>
<script id="argTpl" type="text/x-jsrender">
<div class="Apply-form-pp1 p-t10 argGroup">
<p class="Apply-formp1 f_l">参数名称：</p>
<p class="Apply-formp2 f_l" style="width:600px">
<input type="text" maxlength="30" style="width:107px;" class="logincn-input1" name="arg_name" value="{{:name}}"/>
参数描述：<input type="text" maxlength="30" style="width:107px;" class="logincn-input1" name="arg_title" value="{{:title}}"/>
<input type="checkbox" name="arg_optional" style="position:relative;top:5px" {{if optional==1}}checked="true"{{/if}}/> 
<span>必填 </span> [<a href="javascript:void(0)" onclick="op_del_arg(this)">删除</a>]</p>
</div>
</script>
</html>