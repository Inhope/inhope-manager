<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.incesoft.xmas.matmgr.domain.App"%>
<%@page import="org.apache.commons.io.IOUtils"%>
<%@page import="java.io.ByteArrayOutputStream"%>
<%@page import="java.io.InputStream"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<link type="text/css" rel="stylesheet" href="../css/apiUser.css" />
<link type="text/css" rel="stylesheet" href="../css/account.css" />
<link rel="stylesheet" type="text/css" href="../css/app.css">
<link rel="stylesheet" type="text/css" href="../css/css.css">
<link rel="stylesheet" type="text/css" href="../css/store.css">
<link rel="stylesheet" type="text/css" href="../css/${app.id}.css">
<link type="text/css" rel="stylesheet" href="../css/jquery-ui.css" />
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery-ui.js"></script>
<script type="text/javascript" src="../js/tab.js"></script>
<title>应用详情-${app.name}</title>
</head>
<body style="background-color:#fff;">
	<div style="padding : 15px 0;" class="con">
		<div style="width:776px; float:left; margin-left:10px;">
			<div style="width:776px; margin:0px 0 5px 0;">
				<div style="height:0;">
					<div id="app_id" style="display:none;">${app.id}</div>
					<div id="app_name" class="title_mid left"
						style="display:none;width:759px;">${app.name}</div>
				</div>
				<div class="right_con" style="padding:0;">
					<div class="Contentboxtwo ov_fl" style="width:774px;">
						<div class="Apply-one p-t20">
							<div class="Apply-Icon f_l">
								<img src="../images/app/${app.icon}">
							</div>
							<div class="Apply-Iconcn1 f_l" style="padding-top:10px;">
								<h2>${app.name}</h2>
								<div class="Apply-Iconcn2 f_l"></div>
							</div>
							<div class="Apply-Iconcn4 f_r" style="margin-right:20px;">
							</div>
						</div>
						<div id="detail_content" style="padding:10px 0;">
							<div class="Menuboxtwo">
								<ul id="tabs">
								</ul>
							</div>
							<div id="intro__tab" class="apptab" style="height:1400px;">
								<%@include file="app_intro_part.jsp"%>
								<div class="Apply-Intro">
									<p style='text-align:center;padding:10px 0;'>
										<img src='../images/app/menu01.png' title='自定义菜单' />
									</p>
									<p style='text-align:center;padding:10px 0;'>
										<img src='../images/app/menu02.png' title='定义菜单触发键' />
									</p>
									<p style='text-align:center;padding:10px 0;'>
										<img src='../images/app/menu03.png' title='配置自定义知识' />
									</p>
								</div>
							</div>

							<div id="content__tab" class="Contentboxdiv1 apptab"
								style="height:530px;">
								<div class="App-menucn01 CM-dsp"
									style="width:770px;padding-left:0;">
									<div class="Apply-menuleft f_l" style="width:380px;">
										<div class="Apply-formcn1 t-l">
											<a id="addRootBtn" href="#">新增</a> <a id="editRootBtn"
												href="#">编辑</a> <a id="delRootBtn" href="#">删除</a> <a
												style="display:;" id="applyBtn" href="#">应用</a>
										</div>
										<div class="Apply-formcn2">
											<table width="100%" border="0" cellspacing="0"
												cellpadding="0" class="Ap-line1">
												<tbody id="root_cont">
													<tr>
														<td width="15%" height="30" align="center" valign="middle"
															bgcolor="#f9f9f9" class="Ap-line2 CM-cl01 CM-fw">序号</td>
														<td width="60%" height="30" align="center" valign="middle"
															bgcolor="#f9f9f9" class="Ap-line2 CM-cl01 CM-fw">名称</td>
														<td width="25%" height="30" align="center" valign="middle"
															bgcolor="#f9f9f9" class="Ap-line4 CM-cl01 CM-fw">触发键</td>
													</tr>
													<c:forEach var="root" items="${rootMenus}"
														varStatus="status">
														<tr id="${root.id}" header="${root.header}"
															footer="${root.footer}" name="${root.name}"
															key="${root.key}" status="1" bh="${root.bh}"
															name0="rootmenu">
															<td height="30" align="center" valign="middle"
																class="Ap-line2 CM-cl01">${status.count}</td>
															<td height="30" align="center" valign="middle"
																class="Ap-line2 CM-cl01">${root.name}</td>
															<td height="30" align="center" valign="middle"
																class="Ap-line4 CM-cl01">${root.key }</td>
														</tr>
													</c:forEach>
													<!--  <tr id="1" name="rootmenu">
	                    <td height="30" align="center" valign="middle" class="Ap-line2 CM-cl01">1</td>
	                    <td height="30" align="center" valign="middle" class="Ap-line2 CM-cl01">笑话</td>
	                    <td height="30" align="center" valign="middle" class="Ap-line4 CM-cl01">Joke</td>
	                  </tr>
	                  <tr id="2" name="rootmenu">
	                    <td height="30" align="center" valign="middle" class="Ap-line3">4</td>
	                    <td height="30" align="center" valign="middle" class="Ap-line3">新闻</td>
	                    <td height="30" align="center" valign="middle">Joke</td>
	                  </tr> -->
													<script>
														var targetRoot = $('tr[name0=rootmenu]:first', $('#root_cont'));
													</script>
												</tbody>
											</table>
										</div>
										<div style="padding:10px 3px;display:none;">
											菜单指令:&nbsp;&nbsp; <font style="color:blue;">B</font> - 返回上一级
											&nbsp;&nbsp; <font style="color:blue;">R</font> - 返回根目录
											&nbsp;&nbsp; <font style="color:blue;">Q</font> - 退出菜单
										</div>
									</div>
									<div class="Apply-menuright f_l">
										<h2>菜单详情</h2>
										<div class="Apply-menuright-cn ov_fl"
											style="overflow:auto;height:500px;">
											<iframe id="tree_frame" style="height:100%;width:100%;"
												frameborder="0" src="app_menu_tree.jsp"></iframe>
										</div>
									</div>
								</div>
							</div>

							<div id="menu_form" style="display:none;position:relative;" title="菜单编辑">
								<form>
									<fieldset>
										<input type="hidden" id="f_id" /> <input type="hidden"
											id="f_parentId" />
										<p id="name_part">
											<label for="f_name">菜单名称</label> <input type="text"
												id="f_name" class="text ui-widget-content ui-corner-all"
												style="width:97%" />
										</p>
										<p id="key_part">
											<label for="f_key">触发键</label> <input type="text" id="f_key"
												value="" class="text ui-widget-content ui-corner-all"
												style="width:97%" />
										</p>
										<p id="header_part">
											<label for="f_header">菜单页眉</label>
											<textarea id="f_header" value=""
												class="ui-widget-content ui-corner-all" rows="3"
												style="width:97%;"></textarea>
										</p>
										<p id="footer_part" style="padding:3px 0;">
											<label for="f_footer">菜单页脚</label>
											<textarea id="f_footer" value=""
												class="ui-widget-content ui-corner-all" rows="3"
												style="width:97%;"></textarea>
										</p>
										<p id="content_part" style="padding:3px 0;">
											<label for="f_content">菜单内容</label>
										<div style="width:480px;height:420px;" id="f_content">
										<iframe id="_content"  style="height:300px;width:480px;" frameborder='no' border='0' marginwidth='0' marginheight='0' scrolling='no' allowtransparency='yes' src="../../kbmgr/anseditor/page/index.jsp?showTextEditor=true"></iframe>
										</div>
										</p>
										<p id="status_part" style="padding:5px 0;display:none;">
											<label for="f_status">是否启用</label> <select id="f_status"
												value="" style="width:50%;"
												class="ui-widget-content ui-corner-all"><option
													selected="selected" value="1">启用</option>
												<option value="0">禁用</option>
											</select>
										</p>
									</fieldset>
								</form>
								<p style="position:absolute;bottom:3px;" class="validateTips"></p>
							</div>

							<div style="display:none" id="message-dialog-menu" title="提示">
								<p>
									<span class="" style="float: left; margin: 0 7px 20px 0;"></span>
								</p>
							</div>
							<script type="text/javascript" src="../js/jquery.blockUI.js"></script>
							<script type="text/javascript" src="../js/dialog.js"></script>
							<script type="text/javascript" src="../js/jquery.json.js"></script>
							<script type="text/javascript" src="../js/custommenu.js"></script>
						</div>
					</div>
				</div>
			</div>
			<p class="clearbh"></p>
		</div>
	</div>
</body>
</html>