<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <link type="text/css" rel="stylesheet" href="../js/jquery-easyui/themes/bootstrap/easyui.css" />
<link type="text/css" rel="stylesheet" href="../js/jquery-easyui/themes/icon.css" />
<script type="text/javascript" src="../js/jquery-easyui/jquery.min.js"></script>
<script type="text/javascript" src="../js/jquery-easyui/jquery.easyui.min.js"></script> 
<script type="text/javascript" src="../js/app/custommenu_tree.js"></script> -->
<c:if test="${empty requestScope.extjsvable_excludeCSS}">
<link rel="stylesheet" type="text/css" href='<c:url value="/ext/resources/css/ext-all.css"/>'/>
<link rel="stylesheet" type="text/css" href='<c:url value="/ext/resources/css/patch.css"/>'/>
<link rel="stylesheet" type="text/css" href='<c:url value="/ext/ux/css/ux-all.css"/>'/>
</c:if>
<c:if test="${param.debug==null}">
<script type="text/javascript" src='<c:url value="/ext/adapter/ext/ext-base.js"/>'></script>
<script type="text/javascript" src='<c:url value="/ext/ext-all.js"/>'></script>
<script type="text/javascript" src='<c:url value="/ext/ux/ux-all.js"/>'></script>
</c:if>
<c:if test="${param.debug!=null}">
<script type="text/javascript" src='<c:url value="/ext/adapter/ext/ext-base-debug.js"/>'></script>
<script type="text/javascript" src='<c:url value="/ext/ext-all-debug.js"/>'></script>
<script type="text/javascript" src='<c:url value="/ext/ux/ux-all-debug.js"/>'></script>
</c:if>
<script type="text/javascript">
Ext.BLANK_IMAGE_URL = '<c:url value="/ext/resources/images/default/s.gif"/>';
</script>
<link rel="stylesheet" type="text/css" href="../css/menu.css"/>
<script type="text/javascript" src="../js/custommenu_tree.js"></script>
<title></title>
</head>
<body style="padding:none;magrin:none;">
<div id="tree-div"></div>
</body>
</html>

<!-- 
<ul id="tt" class="easyui-tree"></ul>
<div id="mm1" class="easyui-menu" style="width:120px;">
	<div id="add_item" onclick="addItem()" data-options="iconCls:'icon-add'">新建菜单项</div>
	<div onclick="editItem()" data-options="iconCls:'icon-edit'">编辑菜单项</div>
	<div class="menu-sep"></div>
	<div onclick="addContent()" data-options="iconCls:'icon-add'">新建内容项</div>
	<div onclick="editContent()" data-options="iconCls:'icon-edit'">编辑内容项</div>
	<div class="menu-sep"></div>
	<div onclick="reload();" data-options="iconCls:'icon-reload'">刷新</div>
	<div onclick="delete();" data-options="iconCls:'icon-remove'">删除</div>
	<div onclick="expand();" data-options="iconCls:'icon-redo'">展开</div>
	<div onclick="collapse();" data-options="iconCls:'icon-undo'">收起</div>
</div>
 -->