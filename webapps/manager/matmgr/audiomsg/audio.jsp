﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link type="text/css" rel="stylesheet" href="css/apiUser.css" />
<link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />
<link type="text/css" rel="stylesheet" href="css/material.css" />
<style type="text/css">
.progressBar {
	width: 150px;
	font-size: 12px;
	font-family: Arial, sans-serif;
	border: 0;
	background: transparent url(images/loading.gif) no-repeat 10px 10px;
	top: 50%;
	left: 50%;
	margin-left: -74px;
	margin-top: 200px;
	padding: 10px 10px 10px 50px;
	text-align: right;
	line-height: 65px;
	font-weight: bold;
	position: absolute;
	z-index: 10000;
	color: white;
}

.mask {
	display: none;
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background: #000;
	z-index: 9999;
	-moz-opacity: 0.6;
	opacity: .60;
	filter: alpha(opacity =               60);
	height: 1000px;
}
</style>
<title>语音–iBotCloud小i机器人智能云服务平台</title>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery.jplayer.min.js"></script>
<script language="javascript">
	function submitUpload() {
	 if ($("#materialAudio")[0].value) {
		 var fileName = $("#materialAudio")[0].value;
		 var vaildSuffix = [ 'mp3', 'fla', 'm4a' ];
		 var v = 0;
		 for ( var i = 0; i < vaildSuffix.length; i++) {
			 if (fileName.substring(fileName.length - vaildSuffix[i].length).toLowerCase() == vaildSuffix[i].toLowerCase()) {
				 v++;
			 }
		 }
		 if (v == 0) {
			 alert('非法文件格式');
			 return;
		 }
		 $("#materialAudioUpload").attr('action', '<c:url value="audio-msg!audioUpload.action?module=matCallback"/>');
		 $(".progressBar").show();
		 $(".mask").css({
		  display : "block",
		  height : $(document).height()
		 });//获取高度，兼容IE6。如果不考虑IE6直接可以换成show()
		 $("#materialAudioUpload").submit();
	 }
 }
 function submitCallback(msg) {
	 $(".mask").hide();
	 $(".progressBar").hide();
	 if ('true' == msg) {
		 window.location = window.location;
	 } else if ('toobig' == msg) {
		 alert("语音文件太大");
	 } else if ('error' == msg) {
		 alert("上传失败");
	 } else {
		 alert(msg);
	 }
 }
 function update(objectId) {
	 var aNode = document.getElementById('updateName' + objectId);
	 aNode.style.display = "block";
	 var divNode = document.getElementById('filename' + objectId);
	 divNode.style.border = "1px solid #ddd";
	 var input = document.getElementById(objectId);
	 input.readOnly = false;
	 input.focus();
	 input.select();
 }
 function updateShow(objectId) {
	 var input = document.getElementById(objectId);
	 input.readOnly = true;
	 var aNode = document.getElementById('updateName' + objectId);
	 aNode.style.display = "none";
	 var divNode = document.getElementById('filename' + objectId);
	 divNode.style.border = "0px";
 }
 function updateName(objectId) {
	 var input = document.getElementById(objectId);
	 input.readOnly = true;
	 var fileName = input.value;
	 if (!fileName) {
		 alert('文件名不能为空!');
		 return;
	 }
	 var aj = $.ajax({
	  url : '<c:url value="audio-msg!update.action"/>',// 跳转到 action    
	  data : {
	   objectId : objectId,
	   fileName : fileName
	  },
	  type : 'post',
	  cache : false,
	  dataType : 'json',
	  success : function(data) {
		  if (data.success == "true") {
			  alert("修改成功！");
		  } else {
			  alert(data.msg);
		  }
		  var aNode = document.getElementById('updateName' + objectId);
		  aNode.style.display = "none";
		  var divNode = document.getElementById('filename' + objectId);
		  divNode.style.border = "0px";
	  },
	  error : function() {
		  alert("异常！");
		  var aNode = document.getElementById('updateName' + objectId);
		  aNode.style.display = "none";
		  var divNode = document.getElementById('filename' + objectId);
		  divNode.style.border = "0px";
	  }
	 });
 }

 function playAndStop(objectId, obj) {
	 if (obj.playing) {
		 obj.playing = false;
		 $(".audioTxt", $(obj)).css("display", "block");
		 $(".audioIco", $(obj)).css("display", "none");
		 $("#jquery_jplayer").jPlayer("stop");
	 } else {
		 obj.playing = true;
		 if ($("#jquery_jplayer").attr("ready")) {
			 $("#jquery_jplayer").jPlayer("setMedia", {
				 mp3 : "audio-msg!playingAudio.action?objectId=" + objectId
			 }).jPlayer("play");
		 } else {
			 $("#jquery_jplayer").jPlayer({
			  ready : function() {
				  $("#jquery_jplayer").attr("ready", "ok");
				  $(this).jPlayer("setMedia", {
					  mp3 : "audio-msg!playingAudio.action?objectId=" + objectId
				  }).jPlayer("play");
			  },
			  ended : function() {
				  obj.playing = false;
				  $(".audioTxt", $(obj)).css("display", "block");
				  $(".audioIco", $(obj)).css("display", "none");
			  },
			  swfPath : "js",
			  wmode : "window",
			  supplied : "mp3",
			  solution : "flash,html"
			 });
		 }
		 $(".audioTxt").css("display", "block");
		 $(".audioIco").css("display", "none");
		 $(".audioTxt", $(obj)).css("display", "none");
		 $(".audioIco", $(obj)).css("display", "block");
	 }
 }

 function remove(objectId) {
	 //      $
	 // 				.ajax({
	 // 					url : '<c:url value="customKnowledge!queryAnswerByAnswerId.do"/>',// 跳转到 action    
	 // 					data : {
	 // 						answerId : objectId
	 // 					},
	 // 					type : 'post',
	 // 					cache : false,
	 // 					dataType : 'json',
	 // 					success : function(data) {
	 // 						if (data.exist == "true") {
	 // 							alert('当前素材有知识点关联，禁止删除！');
	 // 						} else if (data.exist == "false") {
	 if (window.confirm('你确定要删除此素材？')) {
		 var aj = $.ajax({
		  url : '<c:url value="audio-msg!remove.action"/>',// 跳转到 action    
		  data : {
			  objectId : objectId
		  },
		  type : 'post',
		  cache : false,
		  dataType : 'json',
		  success : function(data) {
			  if (data.success == "true") {
				  document.getElementById('divCenterAudio').style.display = 'block';
				  var tmid = window.setTimeout(function() {
					  document.getElementById('divCenterAudio').style.display = 'none';
					  window.location.reload();
				  }, 1000);
				  // reloadData();
			  } else {
				  alert(data.msg);
			  }
		  },
		  error : function() {
			  alert("异常！");
		  }
		 });
	 }
 }
 // 					},
 // 					error : function() {
 // 						alert("查询是否关联知识点发生异常！");
 // 					}
 // 				});
 // }
 
function searchName() {
      var nameContent = $("#searchInputName").val();
      if(nameContent){
         if('请输入文件名查询'==nameContent){
            alert('提示：请输入你要查询的名称');
         }else
         window.location.href = 'audio-msg!list.action?type=edit&name=' + nameContent;
      }else{
         alert('提示：查询名称不能为空');
         return;
      }
}
</script>
</head>
<body style="">
	<div class="mask"></div>
	<div class="progressBar" style="display: none; ">文件上传中，请稍等...</div>
	<div id="jquery_jplayer"></div>
	<div style="color: red">
		<s:fielderror />
	</div>
	<div>
		<div class="con">
			<div style="width: 100%; float: left; margin-left: 10px;">
				<div style="width: 100%; margin: 10px 0 5px 0;">
					<div style="height: 29px;">
						<div class="left">
							<img src="images/title_left.gif" style="width: 9px;" />
						</div>
						<div class="title_mid left" style="width: 98%;">语音</div>
						<div class="left">
							<img src="images/title_right.gif" style="width: 8px;" />
						</div>
					</div>
					<iframe style="display: none" src="about:blank"
						name="audioFileupload"></iframe>
					<div class="right_con" style="width:99.4%;">
<div class="scgl_title_search">
					<div style="padding-left:10px;">
							<div class="search">
							  <div class="search_suo">
							    <input name="text" type="text" id="searchInputName" style="color: rgb(102, 102, 102);" onfocus="if(value =='请输入文件名查询'){value ='';}" onblur="if (value ==''){value='请输入文件名查询';}" onkeypress="if(event.keyCode==13) {searchName();}" value="请输入文件名查询" />
						      <span onclick="searchName();"><a href="javascript:void(0)" id="" onclick="searchName();"></a> </span> </div>
							</div>
						</div>	
							<div class="scgl_title_con">
									
							</div>
						</div>
						<div id="divCenterAudio"
							style="position: absolute; z-index: 100; display: none; background-color: #A7D3F1; width: 100px;height:52px;text-align: center; left: 35%; top: 110px;">
							<p
								style="font-weight: bold; font-size: 15px; color: #fff;margin-top:15px">
								删除成功</p>
						</div>
						<div style="padding-left:10px">
							
								<form id="materialAudioUpload" style="float:left"
									encType="multipart/form-data" method="post"
									target="audioFileupload" name="audioForm" action="">
									<div id="materialAudioFile">
										<input type="button" value=""
											style="cursor:pointer;background:url(images/uploadSubmit.jpg) no-repeat;width:70px;height:26px;line-height:26px;color:white;border:0" />
										<input id="materialAudio" style="cursor:pointer;"
											onchange="submitUpload();" type="file" name="audioFile" />
										&nbsp;&nbsp;<span
											style="font-size:2;position:absolute;left:85px;top:5px;width:500px;">注：语音大小限制：256k，长度限制：60s
											格式限制：mp3, fla, m4a</span>
									</div>
								</form>
								<%@include file="../nav-templatePage.jsp"%>

								<div style="clear:both;"></div>
								<div class="material_content" id="palyAudioShow" style="padding-left:10px">
									<div class="list_title">
										<div style="margin:0 auto;width:80%;">
											<div class="left title msg">文件名</div>
											<div class="second title msg">文件</div>
											<div class="right title opt">操作</div>
											<div class="right title opt">大小</div>
										</div>
									</div>
									<div>
										<ul class="list_content">
											<c:forEach items="${pg.result}" var="audioDomain"
												varStatus="s">
												<li class="list_item">
													<div style="margin:0 auto;width:80%;">
														<div class="left file">
															<div class="file_name_area"
																id="filename${audioDomain.id}">
																<input type="text" id="${audioDomain.id}"
																	readonly="true" class="file_name"
																	value="${audioDomain.name}" maxlength="20"
																	style="float:left;border:0"
																	onkeypress="if(event.keyCode==13) {updateName('${audioDomain.id}');}">
																</input> <a href="javascript:updateName('${audioDomain.id}');"
																	style="float:right;display:none"
																	id="updateName${audioDomain.id}"><imgsrc="images/submitTip.jpg" width="29" height="30">
																</a>
															</div>
															<div class="mediaBox audioBox"
																onclick="playAndStop('${audioDomain.id}',this)">
																<div class="mediaContent">
																	<span class="audioTxt">点击播放</span> <span
																		class="audioIco"></span>
																	<!-- &nbsp;&nbsp;&nbsp; <b>${audioDomain.timeLength}"</b>   -->
																</div>
																<span class="iconArrow"></span>
															</div>
														</div>
														<div class="opt oper right">
															<a style="margin:0 6px;" title="下载"
																href="audio-msg!download.action?objectId=${audioDomain.id}">下载</a>
															<a style="margin:0 6px;" title="重命名"
																href="javascript:update('${audioDomain.id}')">重命名</a> <a
																style="margin:0 6px;" title="删除"
																href="javascript:remove('${audioDomain.id}')">删除</a>
														</div>
														<div class="right size">${audioDomain.size}</div>
														<div class="clr"></div>
													</div>
												</li>
											</c:forEach>
										</ul>
									</div>
								</div>
							</div>
							<%@include file="../nav-templatePage.jsp"%>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>