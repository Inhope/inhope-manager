<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link type="text/css" rel="stylesheet" href="../css/material.css" />
<title>语音–iBotCloud小i机器人智能云服务平台</title>
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery-ui.js"></script>
<script type="text/javascript" src="../js/jquery.jplayer.min.js"></script>
<%
	String audioId = request.getParameter("audioId");
%>
<script language="javascript">
	function playAndStop(objectId, obj) {
		if (obj.playing) {
			obj.playing = false;
			$(".audioTxt", $(obj)).css("display", "block");
			$(".audioIco", $(obj)).css("display", "none");
			$("#jquery_jplayer").jPlayer("stop");
		} else {
			obj.playing = true;
			if ($("#jquery_jplayer").attr("ready")) {
				$("#jquery_jplayer").jPlayer("setMedia", {
					mp3 : "audio-msg!playingAudio.action?objectId=" + objectId
				}).jPlayer("play");
			} else {
				$("#jquery_jplayer")
						.jPlayer(
								{
									ready : function() {
										$("#jquery_jplayer")
												.attr("ready", "ok");
										$(this)
												.jPlayer(
														"setMedia",
														{
															mp3 : "audio-msg!playingAudio.action?objectId="
																	+ objectId
														}).jPlayer("play");
									},
									ended : function() {
										obj.playing = false;
										$(".audioTxt", $(obj)).css("display",
												"block");
										$(".audioIco", $(obj)).css("display",
												"none");
									},
									swfPath : "../js",
									wmode : "window",
									supplied : "mp3"
								});
			}
			$(".audioTxt").css("display", "block");
			$(".audioIco").css("display", "none");
			$(".audioTxt", $(obj)).css("display", "none");
			$(".audioIco", $(obj)).css("display", "block");
		}
	}
</script>
</head>
<body style="border:0;padding:0;margin-top:5px;margin-left:10px;">
	<div id="jquery_jplayer"></div>
		<div class="mediaBox audioBox"
			onclick="playAndStop('<%=audioId%>',this)">
			<div class="mediaContent">
				<span class="audioTxt">点击播放</span> <span class="audioIco"></span>
			</div>
			<span class="iconArrow"></span>
		</div>
</body>
</html>