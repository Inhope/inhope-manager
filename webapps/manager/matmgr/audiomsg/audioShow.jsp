<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/select.css" type="text/css" />
<link type="text/css" rel="stylesheet" href="css/material.css" />
<title>语音–iBotCloud小i机器人智能云服务平台</title>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery.jplayer.min.js"></script>
<script type="text/javascript" src="js/adjust.js"></script>
<script>
	selectClick = function(audioId, name, isClick) {
		document.getElementById('selectedVal').value = audioId;
		if (isClick)
			parent.answerRendered({
				id : audioId,
				type : 'audiomsg',
				title : name
			});
	};
	playAndStop = function(objectId, obj) {
		if (obj.playing) {
			obj.playing = false;
			$(".audioTxt", $(obj)).css("display", "block");
			$(".audioIco", $(obj)).css("display", "none");
			$("#jquery_jplayer").jPlayer("stop");
		} else {
			obj.playing = true;
			if ($("#jquery_jplayer").attr("ready")) {
				setTimeout(function() {
					$("#jquery_jplayer").jPlayer("setMedia", {
						mp3 : "audio!playingAudio.action?objectId=" + objectId
					}).jPlayer("play");
				}, 500);
			} else {
				$("#jquery_jplayer").jPlayer({
					ready : function() {
						$("#jquery_jplayer").attr("ready", "ok");
						$(this).jPlayer("setMedia", {
							mp3 : "audio!playingAudio.action?objectId=" + objectId
						}).jPlayer("play");
					},
					ended : function() {
						obj.playing = false;
						$(".audioTxt", $(obj)).css("display", "block");
						$(".audioIco", $(obj)).css("display", "none");
					},
					swfPath : "../js",
					wmode : "window",
					supplied : "mp3"
				});
			}
			$(".audioTxt").css("display", "block");
			$(".audioIco").css("display", "none");
			$(".audioTxt", $(obj)).css("display", "none");
			$(".audioIco", $(obj)).css("display", "block");
		}
	}
</script>
</head>
<body>
	<div id="jquery_jplayer"></div>
	<div id="mainDiv" class="dialog">
		<div class="dialog_content">
			<div class="dialog_list">
				<input type="hidden" id="selectedVal" /> <input type="hidden"
					id="type" value="audiomsg" />
				<!-- 				<div class="page_nav"> -->
				<!-- 					<%@include file="../nav-templatePage.jsp"%> -->
				<!-- 				</div> -->
				<!----------------------循环列表内容------------------------->
				<div class="list_ul">
					<c:choose>
						<c:when test="${pg.totalCount>0}">
							<ul>
								<c:forEach items="${pg.result}" var="audioDomain" varStatus="s">
									<li class="float-p">
										<div class="left checkbox">
											<c:choose>
												<c:when test="${audioDomain.id==param.materialId}">
													<input type="radio" value="${audioDomain.id}" name="file"
														checked="checked" onclick="selectClick(this.value,'${audioDomain.name}',true)">
														<script>
															selectClick('${audioDomain.id}','${audioDomain.name}', false);
														</script>
												</c:when>
												<c:otherwise>
													<input type="radio" value="${audioDomain.id}" name="file"
														onclick="selectClick(this.value,'${audioDomain.name}',true)">
												</c:otherwise>
											</c:choose>
										</div>
										<div class="left file_infor">
											<div class="left">
												<div class="fileName">${audioDomain.name}</div>
												<div class="mediaBox audioBox"
													onclick="playAndStop('${audioDomain.id}',this)">
													<div class="mediaContent">
														<span class="audioTxt">点击播放</span> <span class="audioIco"></span>
													</div>
													<span class="iconArrow"></span>
												</div>
												<div class="clr"></div>
											</div>
											<div class="right tr">
												<div>${audioDomain.size}</div>
											</div>
											<div class="clr"></div>
											<div></div>
										</div>
									</li>
								</c:forEach>
							</ul>
						</c:when>
						<c:otherwise>
							  没有数据
							</c:otherwise>
					</c:choose>
				</div>
				<div class="page_nav"><%@include
						file="../nav-templatePage.jsp"%></div>
			</div>
		</div>
	</div>
</body>
</html>