﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link type="text/css" rel="stylesheet" href="css/apiUser.css" />
<link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />
<link type="text/css" rel="stylesheet" href="css/material.css" />
<link type="text/css" rel="stylesheet" href="css/image-ex.css" />
<style type="text/css">
.progressBar {
	width: 150px;
	font-size: 12px;
	font-family: Arial, sans-serif;
	border: 0;
	background: transparent url(images/loading.gif) no-repeat 10px 10px;
	top: 50%;
	left: 50%;
	margin-left: -74px;
	margin-top: 200px;
	padding: 10px 10px 10px 50px;
	text-align: right;
	line-height: 65px;
	font-weight: bold;
	position: absolute;
	z-index: 10000;
	color: white;
}

.mask {
	display: none;
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background: #000;
	z-index: 9999;
	-moz-opacity: 0.6;
	opacity: .60;
	filter: alpha(opacity = 60);
	height: 1000px;
}

input.button {
	cursor: pointer;
	cursor: hand;
}
</style>
<title>图片–iBotCloud小i机器人智能云服务平台</title>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery.blockUI.js"></script>
<script language="javascript">
	function submitUpload() {
	 if ($("#materialImage")[0].value) {
		 var fileName = $("#materialImage")[0].value;
		 var vaildSuffix = [ 'png', 'jpeg', 'jpg', 'gif' ];
		 var v = 0;
		 for ( var i = 0; i < vaildSuffix.length; i++) {
			 if (fileName.substring(fileName.length - vaildSuffix[i].length).toLowerCase() == vaildSuffix[i].toLowerCase()) {
				 v++;
			 }
		 }
		 if (v == 0) {
			 alert('非法文件格式');
			 return;
		 }

		 $("#materialImgUpload").attr("action", "image-msg!imgUpload.action?module=matCallback");
		 $(".progressBar").show();
		 $(".mask").css({
		  display : "block",
		  height : $(document).height()
		 });//获取高度，兼容IE6。如果不考虑IE6直接可以换成show()
		 $("#materialImgUpload").submit();
	 }
 }
 function submitCallback(msg) {
	 $(".mask").hide();
	 $(".progressBar").hide();
	 if ('true' == msg) {
		 window.location = window.location;
	 } else if ('toobig' == msg) {
		 alert("图片太大");
	 } else if ('error' == msg) {
		 alert("上传失败");
	 } else {
		 alert(msg);
	 }
 }
 function update(objectId) {
	 var aNode = document.getElementById('updateName' + objectId);
	 aNode.style.display = "block";
	 var divNode = document.getElementById('filename' + objectId);
	 divNode.style.border = "1px solid #ddd";
	 var input = document.getElementById(objectId);
	 input.readOnly = false;
	 input.focus();
	 input.select();
 }
 function updateShow(objectId) {
	 var input = document.getElementById(objectId);
	 input.readOnly = true;
	 var aNode = document.getElementById('updateName' + objectId);
	 aNode.style.display = "none";
	 var divNode = document.getElementById('filename' + objectId);
	 divNode.style.border = "0px";
	 updateName(objectId);
 }
 function updateName(objectId) {
	 var input = document.getElementById(objectId);
	 input.readOnly = true;
	 var fileName = input.value;
	 if (!fileName) {
		 alert('文件名不能为空!');
		 return;
	 }
	 var aj = $.ajax({
	  url : '<c:url value="image-msg!update.action"/>',// 跳转到 action    
	  data : {
	   objectId : objectId,
	   fileName : encodeURI(fileName)
	  },
	  type : 'post',
	  cache : false,
	  dataType : 'json',
	  success : function(data) {
		  if (data.success == "true") {
			  alert("修改成功！");
		  } else {
			  alert(data.msg);
		  }
		  var aNode = document.getElementById('updateName' + objectId);
		  aNode.style.display = "none";
		  var divNode = document.getElementById('filename' + objectId);
		  divNode.style.border = "0px";
	  },
	  error : function() {
		  var aNode = document.getElementById('updateName' + objectId);
		  aNode.style.display = "none";
		  var divNode = document.getElementById('filename' + objectId);
		  divNode.style.border = "0px";
		  alert("异常！");
	  }
	 });
 }
 function remove(objectId) {
	 // 		$.ajax({
	 // 			url : '<c:url value="customKnowledge!queryAnswerByAnswerId.do"/>',// 跳转到 action    
	 // 			data : {
	 // 				answerId : objectId
	 // 			},
	 // 			type : 'post',
	 // 			cache : false,
	 // 			dataType : 'json',
	 // 			success : function(data) {
	 // 				if (data.exist == "true") {
	 // 					alert('当前素材有知识点关联，禁止删除！');
	 // 				} else if (data.exist == "false") {
	 if (window.confirm('你确定要删除此素材？')) {
		 var aj = $.ajax({
		  url : '<c:url value="image-msg!remove.action"/>',// 跳转到 action    
		  data : {
			  objectId : objectId
		  },
		  type : 'post',
		  cache : false,
		  dataType : 'json',
		  success : function(data) {
			  if (data.success == "true") {
				  document.getElementById('divCenterImage').style.display = 'block';
				  var tmid = window.setTimeout(function() {
					  document.getElementById('divCenterImage').style.display = 'none';
					  window.location.reload();
				  }, 1000);
				  // reloadData();
			  } else {
				  alert(data.msg);
			  }
		  },
		  error : function() {
			  alert("异常！");
		  }
		 });
	 }
	 // 				}
	 // 			},
	 // 			error : function() {
	 // 				alert("查询是否关联知识点发生异常！");
	 // 			}
	 // 		});
 }
 function showImageMark() {
	 $.ajax({
	  url : '<c:url value="image-msg!getImageMark.action"/>',// 跳转到 action    
	  type : 'post',
	  cache : false,
	  dataType : 'json',
	  success : function(data) {
		  if (data.success == "true") {
			  $("#imageMarkText").val(data.mark);
			  var ena = ("1" == data.enabled ? true : false);
			  if (ena)
				  document.getElementById('imgMarkTextContent').innerHTML = data.mark;
			  $("#imageMarkEnabled").prop('checked', ena);
		  } else if ('false' != data.exist) {
			  alert("加载图片水印内容失败");
		  }
	  },
	  error : function() {
		  alert("加载图片水印内容异常！");
	  }
	 });
	 showMessageConfirm();
	 //$(".imageMarkDiv").show();
 }
 function hideImageMark() {
	 finishMessageConfirm();
	 //$(".imageMarkDiv").hide();
 }
 function saveImageMark() {
	 var textmark = $("#imageMarkText")[0].value;
	 var enabled = $("#imageMarkEnabled").is(':checked');
	 if (textmark) {
		 if (textmark.length > 12) {
			 alert('水印内容长度不能超过12个字符');
			 return;
		 } else {
			 if (!enabled) {
				 if (!window.confirm('你确定要保存未启用的水印内容吗？')) {
					 return;
				 }
			 }
			 var enable = (enabled ? 1 : 0)
			 $.ajax({
			  url : '<c:url value="image-msg!saveImageMark.action"/>',// 跳转到 action    
			  data : {
			   mark : textmark,
			   enabled : enable
			  },
			  type : 'post',
			  cache : false,
			  dataType : 'json',
			  success : function(data) {
				  if (data.success == "true") {
					  document.getElementById('imgMarkTextContent').innerHTML = textmark;
					  alert("保存成功");
					  hideImageMark();
				  } else {
					  alert("保存失败");
				  }
			  },
			  error : function() {
				  alert("异常！");
			  }
			 });
		 }
	 } else {
		 alert('水印内容不能为空');
		 return;
	 }
 }
 function showMessageConfirm(title, msg) {
	 $.blockUI({
	  message : $(".imageMarkDiv"),
	  css : {
	   border : 0,
	   background : "none",
	   width : "700px",
	   top : '20%',
	   left : '25%'
	  }
	 });
 }

 function finishMessageConfirm() {
	 $.unblockUI();
 }
 function searchName() {
             var nameContent = $("#searchInputName").val();
             if(nameContent){
                if('请输入文件名查询'==nameContent){
                   alert('提示：请输入你要查询的名称');
                }else
                window.location.href = 'image-msg!list.action?type=edit&name=' + nameContent;
             }else{
                alert('提示：查询名称不能为空');
                return;
             }
}
</script>
</head>
<body style="position: relative;">
	<!--==="素材管理——图片水印设置"内容开始===-->
	<div class="imageMarkDiv" style="display:none">
		<div style="width:700px;height:auto;display:;z-index:999;">
			<div class="TC-contenttop f_l">
				<p></p>
				<div></div>
				<font></font>			</div>
			<!--顶部内容结束-->
			<div class="TC-contentone f_l">
				<div class="TC-contenttwo f_l">
					<div class="TC-content ov_fl">
						<div class="TC-title TC-titline">
							<h2>图片水印设置</h2>
							<p>
								<a href="javascript:hideImageMark();"></a>
							</p>
						</div>
						<!--===title contemt end===-->

						<!--==="选择图片"内容开始-->
						<div class="TC-cn01">
							<div class="TC-imgcn1">
								<div class="sucai-imgcn f_l">
									<p id="imgMarkTextContent">水印文字</p>
									<img src="images/imageDefault.jpg">
										<h2>预览图</h2>
								</div>
								<div class="sucai-cn01 f_r">
									<p>设置图文消息和图片消息的微信水印</p>
									<input name="" type="text" id="imageMarkText"
										class="Form-input01" style="width:200px;">
										<p style="line-height:20px;font-size:12px;color:red">注：水印内容不能超过12个字符</p>
										<p style="height:40px;line-height:40px;">
											<input id="imageMarkEnabled"
												style="margin:12px 5px 0 0;float:left;" name=""
												type="checkbox" checked="true">是否启用 
										</p>
								</div>

							</div>
							<p class="h10 TC-titline"></p>
							<div class="TC-imgcn1 t-c">
								<a href="javascript:saveImageMark()" class="button_two2">保 存</a>&nbsp;&nbsp;<a
									href="javascript:hideImageMark()" class="button_two1">取 消</a>
							</div>
						</div>
						<!--==="选择图片"内容结束-->
					</div>
				</div>
			</div>
			<div class="TC-contentbottom f_l">
				<p></p>
				<div></div>
				<font></font>			</div>
			<!--顶部内容结束-->

		</div>


	</div>
	<!--==="素材管理——图片水印设置"内容结束===-->
	<div class="mask"></div>
	<div class="progressBar" style="display: none; ">文件上传中，请稍等...</div>
	<div style="color:red">
		<s:fielderror />
	</div>
	<div>
		<div class="con">
			<div style="width: 100%; float: left; margin-left: 10px;">
				<div style="width: 100%; margin: 10px 0 5px 0;">
					<div style="height: 29px;">
						<div class="left">
							<img src="images/title_left.gif" style="width: 9px;" />
						</div>
						<div class="title_mid left" style="width: 98%;">图片</div>
						<div class="left">
							<img src="images/title_right.gif" style="width: 8px;" />
						</div>
					</div>
					<iframe style="display: none" src="about:blank"
						name="imgFileupload"></iframe>
					<div class="right_con" style="width:99.4%;">
                    
					<div class="scgl_title_search">
					<div style="padding-left:10px;">
							<div class="search">
							  <div class="search_suo">
							    <input name="text" type="text" id="searchInputName" style="color: rgb(102, 102, 102);" onfocus="if(value =='请输入文件名查询'){value ='';}" onblur="if (value ==''){value='请输入文件名查询';}" onkeypress="if(event.keyCode==13) {searchName();}" value="请输入文件名查询" />
						      <span onclick="searchName();"><a href="javascript:void(0)" id="" onclick="searchName();"></a> </span> </div>
							</div>
						</div>	
							<div class="scgl_title_con">
									
							</div>
						</div>
					
					
						<div id="divCenterImage"
							style="position: absolute; z-index: 100; display: none; background-color: #A7D3F1; width: 100px;height:52px; text-align: center; left: 35%; top: 110px;">
							<p
								style="font-weight: bold; font-size: 15px; color: #fff;margin-top:15px">
								删除成功</p>
						</div>
						<!-- 图片水印start -->
						<!-- 图片水印end -->
						<div style="padding-left:10px;">
							<div>
								<form id="materialImgUpload" style="float:left;"
									encType="multipart/form-data" method="post"
									target="imgFileupload" name="imgForm" action="">
									<div id="materialImgFile">
										<input type="button" class="button"
											style="cursor:pointer;background:url(images/uploadSubmit.jpg) no-repeat;width:70px;height:26px;line-height:26px;color:white;border:0;" />
										<input id="materialImage" onchange="submitUpload();"
											type="file" name="imageFile" style="cursor:pointer;" />
										&nbsp;&nbsp;<span
											style="font-size:2;position:absolute;left:85px;top:5px;width:500px;">大小：不超过128k，
											格式：png, jpeg, jpg, gif <a href="javascript:showImageMark();"
											style="font-size:12px;">设置图片水印</a> </span>									</div>
								</form>
								<%@include file="../nav-templatePage.jsp"%>
								<div style="clear:both;"></div>
								<div class="material_content" style="padding-left:10px;">
									<div class="list_title">
										<div style="margin:0 auto;width:80%;">
											<div class="left title msg">文件名</div>
											<div class="second title msg">文件</div>
											<div class="right title opt">操作</div>
											<div class="right title opt">大小</div>
										</div>
									</div>
									<div>
										<ul class="list_content">
											<c:forEach items="${pg.result}" var="imageDomain"
												varStatus="s">
												<li class="list_item">
													<div style="margin:0 auto;width:80%;">
														<div class="left file" style="width:35%">
															<div class="file_name_area"
																id="filename${imageDomain.id}">
																<input type="text" id="${imageDomain.id}"
																	readonly="true" class="file_name"
																	value="${imageDomain.name}" maxlength="20"
																	style="float:left;border:0"
																	onkeypress="if(event.keyCode==13) {updateName('${imageDomain.id}');}">
																</input> <a href="javascript:updateName('${imageDomain.id}');"
																	style="float:right;display:none"
																	id="updateName${imageDomain.id}"><img
																	src="images/submitTip.jpg" width="29" height="30">
																</a>
															</div>
															<img class="xi_img image_load"
																onclick="window.open('image-msg!showImg.action?objectId=${imageDomain.id}')"
																src="image-msg!showImg.action?objectId=${imageDomain.id}" />
															<div class="clr"></div>
														</div>
														<div class="opt oper right">
															<a style="margin:0 6px;" title="下载"
																href="image-msg!download.action?objectId=${imageDomain.id}">下载</a>
															<a style="margin:0 6px" title="重命名"
																href="javascript:update('${imageDomain.id}')">重命名</a><a
																style="margin:0 6px" title="删除"
																href="javascript:remove('${imageDomain.id}')">删除</a>
														</div>
														<div class="right size">${imageDomain.size}</div>
														<div class="clr"></div>
													</div></li>
											</c:forEach>
										</ul>
									</div>
								</div>
							</div>
							<%@include file="../nav-templatePage.jsp"%>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>