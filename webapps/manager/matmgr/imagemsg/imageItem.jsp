<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>图片–iBotCloud小i机器人智能云服务平台</title>
<%
	String imageId = request.getParameter("imageId");
%>
<style type="text/css">
.image_load {
padding: 5px;
background: #f0f0f0;
border: 1px solid #e2e2e2;
width: 100px;
}
</style>
</head>
<body style="border:0;padding:0;margin:0;">
	<div>
		<span class="img_loading"></span><img class="image_load" src="image-msg!showImg.action?objectId=<%=imageId%>" />
	</div>
</body>
</html>