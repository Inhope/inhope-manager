<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/select.css" type="text/css" />
<script type="text/javascript" src="js/adjust.js"></script>
<title>图片–iBotCloud小i机器人智能云服务平台</title>
<script>
	function selectClick(imageId,name, isClick) {
	    document.getElementById(imageId).checked = true;
		document.getElementById('selectedVal').value = imageId;
		if (isClick)
			parent.answerRendered({
				id : imageId,
				type : 'imagemsg',
				title : name,
			});
	}
</script>


</head>
<body>
	<div id="mainDiv" class="dialog">
		<div class="dialog_content">
			<div class="dialog_list">
				<input type="hidden" id="selectedVal" /> <input type="hidden"
					id="type" value="imagemsg" />
				<!-- 				<div class="page_nav"> -->
				<!-- 					<%@include file="../nav-templatePage.jsp"%> -->
				<!-- 				</div> -->
				<!----------------------循环列表内容------------------------->
				<div class="list_ul">
					<c:choose>
						<c:when test="${pg.totalCount>0}">
							<ul>
								<c:forEach items="${pg.result}" var="imageDomain" varStatus="s">
									<li class="float-p">
										<div class="left checkbox">
											<c:choose>
												<c:when test="${imageDomain.id==param.materialId}">
													<input type="radio" value="${imageDomain.id}" name="file"
														id="${imageDomain.id}" checked="checked"
														onclick="selectClick(this.value,'${imageDomain.name}',true)"> <script>
															selectClick('${imageDomain.id}','${imageDomain.name}', false);
														</script>
												</c:when>
												<c:otherwise>
													<input type="radio" value="${imageDomain.id}"
														id="${imageDomain.id}" name="file"
														onclick="selectClick(this.value,'${imageDomain.name}',true)">
												</c:otherwise>
											</c:choose>
										</div>
										<div class="left file_infor">
											<div class="left">
												<div class="fileName">${imageDomain.name}</div>
												<div class="xi_img">
													<span class="Imgloading"></span> <img class="xi_img"
														src="image-msg!showImg.action?objectId=${imageDomain.id}"
														onclick="selectClick('${imageDomain.id}','${imageDomain.name}',true)"
														style="width:100px;">
												</div>
											</div>
											<div class="right tr">
												<div>${imageDomain.size}</div>
											</div>
											<div class="clr"></div>
											<div></div>
										</div>
									</li>
								</c:forEach>
							</ul>
						</c:when>
						<c:otherwise>
							  没有数据
						</c:otherwise>
					</c:choose>
				</div>
				<div class="page_nav"><%@include
						file="../nav-templatePage.jsp"%></div>
			</div>
		</div>
	</div>
</body>
</html>