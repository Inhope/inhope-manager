<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.incesoft.xmas.matmgr.domain.ImgtxtMsgObject"%>
<%@page import="com.incesoft.xmas.matmgr.web.base.PageEx"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.incesoft.xmas.matmgr.service.ImgtxtMsgService"%>
<%@page import="java.util.List"%>
<%@page
	import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%
	ImgtxtMsgService imgMsgService = WebApplicationContextUtils.getWebApplicationContext(application)
	.getBeansOfType(ImgtxtMsgService.class).values().iterator().next();
	  PageEx pg = (PageEx)request.getAttribute("pg");
      int dataSize = 0;
      List<List<ImgtxtMsgObject>> listLeft=null;
      List<List<ImgtxtMsgObject>> listRight=null;
      if(pg!=null){
      List<String> list = pg.getResult();
         if(list!=null && list.size()>0){
		     listLeft = new ArrayList<List<ImgtxtMsgObject>>();  //左侧div 内容
		     listRight = new ArrayList<List<ImgtxtMsgObject>>(); //右侧div 内容
             for(int i=0;i<list.size();i++){
	             String imgmsgId = list.get(i);
	             if(StringUtils.isNotBlank(imgmsgId)){
	                 dataSize++;
		             List<ImgtxtMsgObject> imgtxtMsgList =  imgMsgService.getImgmsgById(imgmsgId,false);
		             if(imgtxtMsgList.size()>0){
						if (i % 2 == 0) {
						  listLeft.add(imgtxtMsgList);
						} else {
					      listRight.add(imgtxtMsgList);
						}
		           }
		       }
	      }
		}
	}
	List<String[]> topTags= imgMsgService.getTopTags();
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link type="text/css" rel="stylesheet" href="css/apiUser.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="css/imgmsg.css">
<link rel="stylesheet" type="text/css" href="css/imgmsgex.css">
<title>图文消息–iBotCloud小i机器人智能云服务平台</title>
<script type="text/javascript">
function searchTag() {
             var tagContent = $("#searchInputTag").val();
             if(tagContent){
                if('请输入标签查询'==tagContent){
                   alert('提示：请输入你要查询的标签内容');
                }else
                window.location.href = 'imgtxt-msg!listTag.action?tag=' + tagContent;
             }else{
                alert('提示：查询标签内容不能为空');
                return;
             }
}
function update(objectId) {
	window.location.href = 'imgtxtmsg/imgtxtMsgEdit.jsp?imgmsgId=' + objectId;
}
function removeImgMsg(objectId) {
// 	$.ajax({
// 		url : '<c:url value="customKnowledge!queryAnswerByAnswerId.do"/>',// 跳转到 action    
// 		data : {
// 			answerId : objectId
// 		},
// 		type : 'post',
// 		cache : false,
// 		dataType : 'json',
// 		success : function(data) {
// 			if (data.exist == "true") {
// 				alert('当前素材有知识点关联，禁止删除！');
// 			} else if (data.exist == "false") {
				if (window.confirm('你确定要删除该图文消息？')) {
					var aj = $.ajax({
						url : '<c:url value="imgtxt-msg!remove.action"/>',// 跳转到 action    
						data : {
							objectId : objectId
						},
						type : 'post',
						cache : false,
						dataType : 'json',
						success : function(data) {
							if (data.success == "true") {
								document.getElementById('divCenterImgmsg').style.display = 'block';
								var tmid = window.setTimeout(function() {
									document.getElementById('divCenterImgmsg').style.display = 'none';
									window.location.reload();
								}, 1000);
							} else {
								alert(data.msg);
							}
						},
						error : function() {
							alert("异常！");
						}
					});
				}
// 			}
// 		},
// 		error : function() {
// 			alert("查询是否关联知识点发生异常！");
// 		}
// 	});
}
</script>
</head>
<body style="">
	<div>
		<div class="con">

			<div style="width: 776px; float: left; margin-left: 10px;">
				<div style="width: 776px; margin: 10px 0 5px 0;">
					<div class="right_con" style="width:774px;padding-top:2px">
						<div id="divCenterImgmsg"
							style="position: absolute; z-index: 100; display: none; background-color: #A7D3F1; width: 100px;height:52px;text-align: center; left: 35%; top: 110px;">
							<p
								style="font-weight: bold; font-size: 15px; color: #fff;margin-top:15px">
								删除成功</p>
						</div>
						<div class="scgl_title">

							<div class="sou">
								<div class="sousuo">
									<input style="color: rgb(102, 102, 102);" id="searchInputTag"
										value="请输入标签查询" onfocus="if(value =='请输入标签查询'){value ='';}"
										onblur="if (value ==''){value='请输入标签查询';}"
										onkeypress="if(event.keyCode==13) {searchTag();}" type="text"><span onclick="searchTag();"><a
										href="javascript:void(0)" id="" onclick="searchTag();"></a> </span>
								</div>
							</div>
							<div class="scgl_title_con">
									<%
										if (topTags !=null && topTags.size() > 0) {
										    out.print("<ul>");
									        out.print("<li><span>热门标签：</span></li>");
											for (String[] tagArr : topTags) {
												out.print("<li><a href=\"imgtxt-msg!listTagId.action?tagId=" + tagArr[0] + "\">" + tagArr[1]
														+ "</a></li>");
											}
											out.print("</ul>");
										}
									%>
							</div>
						</div>
						<div
							style="padding:0;margin: 5px 5px 0 5px; border: 0px solid #DDDDDD; display: inline-block; width: 752px;">
							<div id="showImgmsgLeft"
								style="margin: 10px 0 0 15px; float: left; width: 360px;">
								<span class="create_access"> <i
									class="icon42_common add_gray"></i> <a
									href="javascript:void(0);"
									onclick="window.location.href='imgtxtmsg/imgtxtMsgEdit.jsp?isSample=true'" style="margin:45px 0 45px 0;height:60px;width:30%;background: url('images/addSampleImgmsgButton.jpg') top center no-repeat"><i
										class="icon_appmsg_create" ></i><strong>单图文消息</strong> </a> <a
									href="javascript:void(0)"
									onclick="window.location.href='imgtxtmsg/imgtxtMsgEdit.jsp'" style="38px 0 39px 0;height:60px;width:30%;background: url('images/addMultiImgmsgButton.jpg') top center no-repeat"><i
										class="icon_appmsg_create multi"></i><strong>多图文消息</strong> </a> </span>
								<%
									if (dataSize > 0) {
										for (List<ImgtxtMsgObject> listImgMsg : listLeft) {
											out.print("<div class=\"show-img-msg-left\"><div class=\"msg-item-wrapper\">");
											out.print("<div class=\"msg-item multi-msg\">");
											ImgtxtMsgObject titleImgmsg = listImgMsg.get(0);
											String url = StringUtils.isNotBlank(titleImgmsg.getSource()) ? titleImgmsg.getSource()
													: "";
											if (1 == listImgMsg.size()) {
												out.print("<div class=\"appmsgItem\" onclick=\"window.open('"+url+"');\"><h4 class=\"appmsg_title\" style=\"font-weight:400;font-style: normal;font-size:16px;line-height:24px;"
														+ "/white-space:pre-wrap;/word-wrap:normal;word-break:normal;word-break:break-all;margin:0 10px;\">"
														+ "<a href=\"javascript:void(0);\" style=\"display: block;color:#666;\">"
														+ titleImgmsg.getTitle()
														+ "</a>"
														+ "</h4><div class=\"appmsg_info\" style=\"padding:5px 10px;\"><em class=\"appmsg_date\" style=\"font-weight:400;"
														+ "font-style: normal;color:#999;font-size:14px;\">"
														+ (StringUtils.isNotBlank(titleImgmsg.getDate()) ? titleImgmsg.getDate() : "")
														+ "</em></div><div class=\"cover\">"
														+ "<p class=\"default-tip\" style=\"display: none\">封面图片</p>"
														+ "<img class=\"i-img\" src=\""
														+ titleImgmsg.getImg()
														+ "\" /> </div><div class=\"rel sub-msg-item appmsgItem sub-msg-opr-show\" style=\"border-top:0;padding:5px 10px;max-height:none;\">"
														+ "<h4 class=\"msg-t\" style=\"padding-top:0;max-height:none;line-height:22px;\"><span class=\"i-title\">"
														+ (StringUtils.isNotBlank(titleImgmsg.getDescription()) ? titleImgmsg
																.getDescription() : "") + "</span></h4></div></div>");
											} else {
												out.print("<div class=\"appmsgItem\" onclick=\"window.open('"+url+"');\"><div class=\"appmsg_info\" style=\"padding:5px 10px;\"><em class=\"appmsg_date\" style=\"font-weight:400;"
														+ "font-style: normal;color:#999;font-size:14px;\">"
														+ (StringUtils.isNotBlank(titleImgmsg.getDate()) ? titleImgmsg.getDate() : "")
														+ "</em></div><div class=\"cover\"><p class=\"default-tip\" style=\"display: none\">封面图片</p>"
														+ "<h4 class=\"msg-t\"><a href=\""
														+ url
														+ "\" target=\"#\">"
														+ "<span class=\"i-title\" style=\"color: #fff;\" id=\"appmsgItem1_title\">"
														+ titleImgmsg.getTitle()
														+ "</span> </a></h4>"
														+ "<img class=\"i-img\" style=\"\" src=\""
														+ titleImgmsg.getImg()
														+ "\" /></div></div>");
												for (int s = 1; s < listImgMsg.size(); s++) {
													ImgtxtMsgObject imgmsgItem = listImgMsg.get(s);
													String urlItem = StringUtils.isNotBlank(imgmsgItem.getSource()) ? imgmsgItem
															.getSource() : "";
													if(StringUtils.isNotBlank(imgmsgItem.getImg())){
													  out.print("<div class=\"rel sub-msg-item appmsgItem sub-msg-opr-show\" onclick=\"window.open('"+urlItem+"');\">"
															+ "<span class=\"thumb\"> <span class=\"default-tip\" style=\"display: none\">缩略图</span>"
															+ "<img class=\"i-img\" src=\"" + imgmsgItem.getImg()
															+ "\" style=\"\"></span><h4 class=\"msg-t\">" + "<a href=\"javascript:void(0);\">"
															+"<span class=\"i-title\">" + imgmsgItem.getTitle()+ "</span></a>" + "</h4></div>");
													}else{
													  out.print("<div class=\"rel sub-msg-item appmsgItem sub-msg-opr-show\" onclick=\"window.open('"+urlItem+"');\">"
															+ "<span class=\"thumb\"> <span class=\"default-tip\" style=\"display: block\">缩略图</span>"
															+ "<img class=\"i-img\" src=\"\" style=\"display:none\"></span><h4 class=\"msg-t\">" + "<a href=\"javascript:void(0);\">"
															+"<span class=\"i-title\">" + imgmsgItem.getTitle()+ "</span></a>" + "</h4></div>");
													}
												}
											}
											out.print("<div style=\"height:30px;background-color: #F4F4E8;cursor: pointer;\"><img style=\"margin-top:5px;margin-left:100px;\""
													+ " src=\"images/edit.png\" onclick=\"javascript:update('"
													+ titleImgmsg.getImgmsgId()
													+ "');\" title=\"修改\" />"
													+ "<img style=\"margin-top:5px;margin-left:50px;\" src=\"images/remove.png\" "
													+ "onclick=\"javascript:removeImgMsg('"
													+ titleImgmsg.getImgmsgId()
													+ "');\""
													+ " title=\"删除\" /></div>");
											out.print("</div></div></div>");
										}
									}
								%>
							</div>
							<div id="showImgmsgRight"
								style="margin:10px 0 0 0;float:right;width:360px;">
								<%
									if (dataSize > 1) {
										for (List<ImgtxtMsgObject> listImgMsg : listRight) {
											out.print("<div class=\"show-img-msg-left\"><div class=\"msg-item-wrapper\">");
											out.print("<div class=\"msg-item multi-msg\">");
											ImgtxtMsgObject titleImgmsg = listImgMsg.get(0);
											String url = StringUtils.isNotBlank(titleImgmsg.getSource()) ? titleImgmsg.getSource()
													: "";
											if (1 == listImgMsg.size()) {
												out.print("<div class=\"appmsgItem\" onclick=\"window.open('"+url+"');\"><h4 class=\"appmsg_title\" style=\"font-weight:400;font-style: normal;font-size:16px;line-height:24px;"
														+ "/white-space:pre-wrap;/word-wrap:normal;word-break:normal;word-break:break-all;margin:0 10px;\">"
														+ "<a href=\"javascript:void(0);\" style=\"display: block;color:#666;\">"
														+ titleImgmsg.getTitle()
														+ "</a>"
														+ "</h4><div class=\"appmsg_info\" style=\"padding:5px 10px;\"><em class=\"appmsg_date\" style=\"font-weight:400;"
														+ "font-style: normal;color:#999;font-size:14px;\">"
														+ (StringUtils.isNotBlank(titleImgmsg.getDate()) ? titleImgmsg.getDate() : "")
														+ "</em></div><div class=\"cover\">"
														+ "<p class=\"default-tip\" style=\"display: none\">封面图片</p>"
														+ "<img class=\"i-img\" src=\""
														+ titleImgmsg.getImg()
														+ "\" /> </div><div class=\"rel sub-msg-item appmsgItem sub-msg-opr-show\" style=\"border-top:0;padding:5px 10px;max-height:none;\">"
														+ "<h4 class=\"msg-t\" style=\"padding-top:0;max-height:none;line-height:22px;\"><span class=\"i-title\">"
														+ (StringUtils.isNotBlank(titleImgmsg.getDescription()) ? titleImgmsg
																.getDescription() : "") + "</span></h4></div></div>");
											} else {
												out.print("<div class=\"appmsgItem\" onclick=\"window.open('"+url+"');\"><div class=\"appmsg_info\" style=\"padding:5px 10px;\"><em class=\"appmsg_date\" style=\"font-weight:400;"
														+ "font-style: normal;color:#999;font-size:14px;\">"
														+ (StringUtils.isNotBlank(titleImgmsg.getDate()) ? titleImgmsg.getDate() : "")
														+ "</em></div><div class=\"cover\"><p class=\"default-tip\" style=\"display: none\">封面图片</p>"
														+ "<h4 class=\"msg-t\"><a href=\""
														+ url
														+ "\" target=\"#\">"
														+ "<span class=\"i-title\" style=\"color: #fff;\" id=\"appmsgItem1_title\">"
														+ titleImgmsg.getTitle()
														+ "</span> </a></h4>"
														+ "<img class=\"i-img\" style=\"\" src=\""
														+ titleImgmsg.getImg()
														+ "\" /></div></div>");
	
												for (int s = 1; s < listImgMsg.size(); s++) {
													ImgtxtMsgObject imgmsgItem = listImgMsg.get(s);
													String urlItem = StringUtils.isNotBlank(imgmsgItem.getSource()) ? imgmsgItem
															.getSource() : "";
													if(StringUtils.isNotBlank(imgmsgItem.getImg())){
													out.print("<div class=\"rel sub-msg-item appmsgItem sub-msg-opr-show\" onclick=\"window.open('"+urlItem+"');\">"
															+ "<span class=\"thumb\"> <span class=\"default-tip\" style=\"display: none\">缩略图</span>"
															+ "<img class=\"i-img\" src=\"" + imgmsgItem.getImg()
															+ "\" style=\"\"></span><h4 class=\"msg-t\">" + "<a href=\"javascript:void(0);\">"
															+"<span class=\"i-title\">" + imgmsgItem.getTitle()+ "</span></a>" + "</h4></div>");
													}else{
													out.print("<div class=\"rel sub-msg-item appmsgItem sub-msg-opr-show\" onclick=\"window.open('"+url+"');\">"
															+ "<span class=\"thumb\"> <span class=\"default-tip\" style=\"display: block\">缩略图</span>"
															+ "<img class=\"i-img\" src=\"\" style=\"display:none\"></span><h4 class=\"msg-t\">" + "<a href=\"javascript:void(0);\">"
															+"<span class=\"i-title\">" + imgmsgItem.getTitle()+ "</span></a>" + "</h4></div>");
													}
												}
											}
											out.print("<div style=\"height:30px;background-color: #F4F4E8;cursor: pointer;\"><img style=\"margin-top:5px;margin-left:100px;\""
													+ " src=\"images/edit.png\" onclick=\"javascript:update('"
													+ titleImgmsg.getImgmsgId()
													+ "');\" title=\"修改\" />"
													+ "<img style=\"margin-top:5px;margin-left:50px;\" src=\"images/remove.png\" "
													+ "onclick=\"javascript:removeImgMsg('"
													+ titleImgmsg.getImgmsgId()
													+ "');\" title=\"删除\" /></div>");
											out.print("</div></div></div>");
										}
									}
								%>
							</div>
						</div>
						<%@include file="../nav-templatePage.jsp"%> 
					</div>
				</div>
			</div>
		</div>
	</div>
	<%
		String tagQueryParam = (String)request.getParameter("tag");
		tagQueryParam =  StringUtils.isNotBlank(tagQueryParam)?tagQueryParam:"";
	%>
	<script type="text/javascript">
	   var tagName ='<%=tagQueryParam%>';
		if (tagName) {
			$("#searchInputTag").val(tagName);
		}
	</script>
</body>
</html>