<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.eastrobot.commonsapi.ibotcluster.ClusterConfigServiceHelper"%>
<%@page import="com.incesoft.xmas.matmgr.domain.ImgtxtMsgObject"%>
<%@page import="com.incesoft.xmas.matmgr.service.ImgtxtMsgService"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	buffer="none" pageEncoding="UTF-8"%>
<%@page
	import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1    
	response.setHeader("Pragma", "no-cache"); //HTTP 1.0    
	response.setDateHeader("Expires", 0); //prevents caching at the proxy server

	ImgtxtMsgService imgMsgService = WebApplicationContextUtils.getWebApplicationContext(application)
	.getBeansOfType(ImgtxtMsgService.class).values().iterator().next();

	String imgmsgId = request.getParameter("imgmsgId");
	String isSample = request.getParameter("isSample");
	int i = 0;
	List<ImgtxtMsgObject> listImgMsg = new ArrayList<ImgtxtMsgObject>();
	if (StringUtils.isNotBlank(imgmsgId)) {
		listImgMsg = imgMsgService.getImgmsgById(imgmsgId,true);
	    i= listImgMsg.size();
		if(i<=0)
	      out.print("<script>alert(\"内容加载失败\");</script>");
	} else
		imgmsgId = "";
	
	String isSampleImgmsg = "false";
	if(i == 1 || (StringUtils.isNotBlank(isSample) && "true".equals(isSample)))
	  isSampleImgmsg = "true";
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="pragma" content="no-cache" />
<link type="text/css" rel="stylesheet" href="../css/apiUser.css" />
<link rel="stylesheet" type="text/css" href="../css/imgmsg.css">
	<link rel="stylesheet" type="text/css" href="../css/imgmsgex.css">
		<script type="text/javascript" src="../js/jquery.js"></script>
		<script type="text/javascript" src="../js/jquery.json.js"></script>
		<script type="text/javascript" src="../../commons/ckeditor4/ckeditor.js"></script>
		<script type="text/javascript" src="imgtxtMsgApp.js"></script>
		<title>编辑图文消息</title>
</head>
<script type="text/javascript">
	mouseOver = function(div) {
		div.getElementsByTagName("ul")[0].style.display = "block";
	}
	mouseOut = function(div) {
		div.getElementsByTagName("ul")[0].style.display = "none";
	}
	function submitUpload() {
		if ($("#imgmsgImgImage")[0].value) {
			var fileName = $("#imgmsgImgImage")[0].value;
			validateImg(fileName);
			$("#imgmsgImgUpload").attr("action", "../imgtxt-msg!upload.action");
			uploadSubmit('<%=imgmsgId%>','<%=isSampleImgmsg%>');
			$("#imgmsgImgImage")[0].value = '';
		}
	}
	function goBack(){
       if(confirm("确定要离开图文编辑页面吗？"))
          location.replace("../imgtxt-msg!list.action?type=edit");
          else
             return false;
   }
</script>
<body style="">
    <div style="height:20px;margin:5px 0 0 10px;"><a href="javascript:goBack();" style="font-size:14px;font-weight: bold;"><--返回列表</a></div>
	<div>
		<div class="con">
			<div
				style="width: 776px; float: left; margin-left: 10px;display:inline-block;">
				<div style="margin: 10px 0 5px 0;">
					<div class="right_con">
						<div id="main" class="container">
							<div class="containerBox">
								<div class="content">
									<div class="z oh msg-edit">
										<div class="edit-img-msg-left">
											<div class="msg-item-wrapper" id="appmsg" data-appid="">
												<div class="msg-item multi-msg" id="img_msg_item">
													<%
														if(StringUtils.isNotBlank(isSampleImgmsg)&& "true".equals(isSampleImgmsg)){
														    out.print("<div id=\"imgmsgItem1\" class=\"appmsgItem\">");
														}else{
														    out.print("<div id=\"imgmsgItem1\" class=\"appmsgItem\" onclick=\"updateEditImgMsg('imgmsgItem1','"+isSampleImgmsg+"');\">");
														}
														if (i > 0) {
													        ImgtxtMsgObject imgMsg1 = listImgMsg.get(0);
													        out.print("<input type=\"hidden\" id=\"imgmsg_title_tag\" value=\"" + (StringUtils.isNotBlank(imgMsg1.getTag())?imgMsg1.getTag():"")
															   + "\" />");
													        String imgAddr = imgMsg1.getImg();
															String urlContent = imgMsg1.getUrlContent();
															urlContent =StringUtils.isNotBlank(urlContent)?urlContent:"";
											            	if (i == 1) {
											            	   out.print("<h4 class=\"appmsg_title\" style=\"font-weight:400;font-style: normal;font-size:16px;line-height:24px;"
				                                               +"/white-space:pre-wrap;/word-wrap:normal;word-break:normal;word-break:break-all;margin:0 10px;\">"
				                                               +"<span class=\"i-title\" id=\"sampleImgmsg_title\">" + imgMsg1.getTitle()+ "</span>"
				                                               +"</h4><div class=\"appmsg_info\" style=\"padding:5px 10px;\"><em class=\"appmsg_date\" style=\"font-weight:400;"
				                                               +"font-style: normal;color:#999;font-size:14px;\" id=\"sampleImgmsg_date\">"+(StringUtils.isNotBlank(imgMsg1.getDate())?imgMsg1.getDate():"")+"</em></div><div class=\"cover\">"
				                                               +"<p class=\"default-tip\" style=\"display: none\" id=\"sampleImgmsg_imgMask\">封面图片</p>"
				                                               +"<img class=\"i-img\" id=\"sampleImgmsg_img\" src=\""+imgAddr
				                                               +"\" /> </div><div class=\"rel sub-msg-item appmsgItem sub-msg-opr-show\" style=\"border-top:0;padding:5px 10px;max-height:none;\">"
				                                               +"<h4 class=\"msg-t\" style=\"padding-top:0;max-height:none;line-height:22px;\"><span class=\"i-title\" id=\"sampleImgmsg_description\" name=\"description\">"
				                                               +(StringUtils.isNotBlank(imgMsg1.getDescription())?imgMsg1.getDescription():"")
				                                               +"</span></h4><input type=\"hidden\" id=\"sampleImgmsg_source\" name=\"source\" value=\"" + imgMsg1.getSource()
															   + "\" /><input type=\"hidden\" id=\"sampleImgmsg_titleImgHtml\" value=\"" + imgMsg1.getIsTitleImgHtml()
															   + "\" /><input type=\"hidden\" id=\"sampleImgmsg_wifiAutoLoad\" value=\"" + imgMsg1.getIsWifiAutoLoad()
															   + "\" /><input type=\"hidden\" id=\"sampleImgmsg_htmlAuth\" value=\"" + imgMsg1.getHtmlAuth()
															   + "\" /><textarea style=\"display:none\" id=\"sampleImgmsg_urlContent\" name=\"urlContent\" >"+urlContent + "</textarea></div>");
												            }else{
																out.print("<div class=\"cover\"><p class=\"default-tip\" style=\"display:none\">封面图片</p>");
																out.print("<h4 class=\"msg-t\">");
																out.print("<span class=\"i-title\" id=\"imgmsgItem1_title\">" + imgMsg1.getTitle()
																		+ "</span>");
																out.print("</h4>");
																out.print("<img class=\"i-img\" style=\"\" src=\"" + imgAddr + "\" />"
																+"<input type=\"hidden\" name=\"source\" value=\"" + imgMsg1.getSource()
																+ "\" /><input type=\"hidden\" id=\"sampleImgmsg_titleImgHtml\" value=\"" + imgMsg1.getIsTitleImgHtml()
																+ "\" /><input type=\"hidden\" id=\"sampleImgmsg_wifiAutoLoad\" value=\"" + imgMsg1.getIsWifiAutoLoad()
																+ "\" /><input type=\"hidden\" id=\"sampleImgmsg_htmlAuth\" value=\"" + imgMsg1.getHtmlAuth()
															   + "\" /><textarea style=\"display:none\" name=\"urlContent\" >"
																		+urlContent + "</textarea></div>");
															}
										                 } else {
									                        out.print("<input type='hidden' id='imgmsg_title_tag' value='' />");
														    if(StringUtils.isNotBlank(isSample) && "true".equals(isSample)){
														         out.print("<h4 class=\"appmsg_title\" style=\"font-weight:400;font-style: normal;font-size:16px;line-height:24px;"
				                                                 +"/white-space:pre-wrap;/word-wrap:normal;word-break:normal;word-break:break-all;margin:0 10px;\">"
				                                                 +"<span class=\"i-title\" id=\"sampleImgmsg_title\">标题</span>"
				                                                 +"</h4><div class=\"appmsg_info\" style=\"padding:5px 10px;\"><em class=\"appmsg_date\" style=\"font-weight:400;"
				                                                 +"font-style: normal;color:#999;font-size:14px;\" id=\"sampleImgmsg_date\"></em></div><div class=\"cover\">"
				                                                 +"<p class=\"default-tip\" style=\"display: block\" id=\"sampleImgmsg_imgMask\">封面图片</p>"
				                                                 +"<img class=\"i-img\" id=\"sampleImgmsg_img\" src=\""
				                                                 +"\" /> </div><div class=\"rel sub-msg-item appmsgItem sub-msg-opr-show\" style=\"border-top:0;padding:5px 10px;max-height:none;\">"
				                                                 +"<h4 class=\"msg-t\" style=\"padding-top:0;max-height:none;line-height:22px;\"><span class=\"i-title\" id=\"sampleImgmsg_description\" name=\"description\">"
			                                                     +"</span></h4><input type=\"hidden\" id=\"sampleImgmsg_source\" name=\"source\" value=\""
																 + "\" /><input type=\"hidden\" id=\"sampleImgmsg_titleImgHtml\" value=\"true\" />"
																 +"<input type=\"hidden\" id=\"sampleImgmsg_wifiAutoLoad\" value=\"\" />"
																 +"<input type=\"hidden\" id=\"sampleImgmsg_htmlAuth\" value=\"\" />"
																 +"<textarea style=\"display:none\" id=\"sampleImgmsg_urlContent\" name=\"urlContent\" >"
																 + "</textarea></div>");
														    }else{
																out.print("<div class=\"cover\"><p class=\"default-tip\" style=\"\">封面图片</p>");
																out.print("<h4 class=\"msg-t\">");
																out.print("<span class=\"i-title\" id=\"imgmsgItem1_title\">标题</span>");
																out.print("</h4>");
																out.print("<img class=\"i-img\" style=\"\"><input type=\"hidden\" name=\"source\""
																		+ "value=\"\" /><input type=\"hidden\" id=\"sampleImgmsg_titleImgHtml\" value=\"true\" />"
																		+"<input type=\"hidden\" id=\"sampleImgmsg_wifiAutoLoad\" value=\"\" />"
																		+"<input type=\"hidden\" id=\"sampleImgmsg_htmlAuth\" value=\"\" />"
																		+"<textarea style=\"display:none\" name=\"urlContent\" ></textarea></div>");
															}
														 }
														%>
												</div>
												<%
														if (i > 1) {
															for (int j = 1; j < listImgMsg.size(); j++) {
																ImgtxtMsgObject imgMsgNext = listImgMsg.get(j);
																String itemId = "appmsgItem" + (j + 1);
																String img = imgMsgNext.getImg();
																img = StringUtils.isNotBlank(img) ? img  : "";
																String urlContentNext = imgMsgNext.getUrlContent();
																urlContentNext = StringUtils.isNotBlank(urlContentNext)?urlContentNext:"";
																out.print("<div class=\"rel sub-msg-item appmsgItem sub-msg-opr-show edit-msg-opr-show\""
																+"onmouseover=\"mouseOver(this);\" onmouseout=\"mouseOut(this);\""
																+" id=\"" + itemId+ "\">");
																out.print("<span class=\"thumb\" id=\"appmsgSpanShow\"> <span class=\"default-tip\""
																		+ " style=\"" + (StringUtils.isNotBlank(img) ? "display:none" : "")
																		+ "\">缩略图</span>" + "<img class=\"i-img\""
																		+ (StringUtils.isNotBlank(img) ? (" src=\"" + img + "\"") : "") + " style=\""
																		+ (StringUtils.isBlank(img) ? "display:none" : "") + "\">"
																		+ "</span><h4 class=\"msg-t\"><span class=\"i-title\">" + imgMsgNext.getTitle()
																		+ "</span></h4><input type=\"hidden\" name=\"source\" value=\""
																		+ imgMsgNext.getSource()
																		+ "\"/><input type=\"hidden\" id=\"sampleImgmsg_htmlAuth"+(j+1)+"\" value=\""+imgMsgNext.getHtmlAuth()+"\" />"
																		+"<input type=\"hidden\" id=\"sampleImgmsg_wifiAutoLoad"+(j+1)+"\" value=\""+imgMsgNext.getIsWifiAutoLoad()+"\" />"
																		+"<textarea style=\"display:none\" name=\"urlContent\" >"
																		+ urlContentNext + "</textarea>");
															  out.print("<ul class=\"abs tc sub-msg-opr\" style=\"display: none;\">"
															            +"<li class=\"b-dib sub-msg-opr-item\">"
																		+ "<a href=\"javascript:updateEditImgMsg('" + itemId + "');\""
																		+ " class=\"th icon18 iconEdit\" data-rid=\"2\">编辑</a></li>"
																		+ "<li class=\"b-dib sub-msg-opr-item\"><a href=\"javascript:removeItem('"
																		+ itemId + "');\"" + "class=\"th icon18 iconDel\" data-rid=\"2\">删除</a>"
																		+ "</li></ul>");
																out.print("</div>");
															}
														}
														if ("false".equals(isSampleImgmsg)) {
														   out.print("<div class=\"sub-add\" id=\"addImgMsg\"><a href=\"javascript:addImgMsg();\" class=\"block tc sub-add-btn\"><span class=\"vm dib sub-add-icon\"></span>增加一条</a></div>");
														}
													%>
											</div>
										</div>
									</div>
									<div class="msg-edit-area" id="msgEditArea">
										<div class="rel msg-editer-wrapper">
											<div id="imgmsg-editer" class="msg-editer">
												<div style="padding: 3px;padding-top:10px;">
													<label>标题</label><br />
													<%
															if (i > 0) {
																	out.print("<input class=\"msg-input\" id=\"imgmsg_edit_title\" style=\"width:380px;height:25px;line-height:25px;margin-bottom:10px;\" value=\""
																					+ listImgMsg.get(0).getTitle()
																					+ "\" type=\"text\" onchange=\"updateTitle(this.value,'"+isSampleImgmsg+"')\">");
																} else {
																	out.print("<input class=\"msg-input\" id=\"imgmsg_edit_title\" style=\"width:380px;height:25px;line-height:25px;margin-bottom:10px;\""
																			+ "type=\"text\" onchange=\"updateTitle(this.value,'"+isSampleImgmsg+"')\">");
																}
														%>
													<br /> <label> 作者(选填)</label><br />
													<%
															if (i > 0) {
																	out.print("<input class=\"msg-input\" id=\"imgmsg_edit_auth\" style=\"width:380px;height:25px;line-height:25px;margin-bottom:10px;\" value=\""
																					+ listImgMsg.get(0).getHtmlAuth()
																					+ "\" type=\"text\" onchange=\"updateAuth(this.value,'"+isSampleImgmsg+"')\">");
																} else {
																	out.print("<input class=\"msg-input\" id=\"imgmsg_edit_auth\" style=\"width:380px;height:25px;line-height:25px;margin-bottom:10px;\""
																			+ "type=\"text\" onchange=\"updateAuth(this.value,'"+isSampleImgmsg+"')\">");
																}
														%>
													<div class="cover-area" style="margin-top: 2px;">
														<div class="oh cover-hd" style="margin-bottom:5px;">
															<label> 图片</label><br />
															<iframe id="upload" style="display: none"
																src="about:blank" name="upload"></iframe>
															<form id="imgmsgImgUpload" encType="multipart/form-data"
																method="post" target="upload" name="imgForm" action="">
																<div id="imgmsgImgFile">
																	<a><input type="button" value="上传图片"
																		onclick="submitUpload();" /> <input
																		id="imgmsgImgImage" onchange="submitUpload();"
																		type="file" name="uploadFile" /> </a>
																</div>
																<p id="imgmsg-upload-tip" class="upload-tip"
																	style="font-size: 12px; margin-left: 200px; margin-top:-28px;float:left;color: red">
																	大图片建议尺寸:360像素*200像素</p>
															</form>
															<div class="imgaddr" id="img_addr" style="display: none">
																<%
																 if (i > 0) {
																		out.print("<input id=\"imgmsg_edit_imgurl\" style=\"width:380px;height:25px;line-height:25px;margin-bottom:10px;\""
																				+ "type=\"text\" value=\"" + listImgMsg.get(0).getImg()
																				+ "\" onchange=\"updateIMG(this.value,'"+isSampleImgmsg+"')\">");
																	} else {
																		out.print("<input class=\"msg-input\" id=\"imgmsg_edit_imgurl\" style=\"width:380px;height:25px;line-height:25px;margin-bottom:10px;\""
																				+ "type=\"text\" onchange=\"updateIMG(this.value,'"+isSampleImgmsg+"')\">");
																	}
																%>
															</div>
														</div>
														<!-- <p id="imgmsg-upload-tip" class="upload-tip"
																style="font-size: 11px; margin-left: 35px; color: red">
																大图片建议尺寸:360像素*200像素</p> -->
														<label style="color: #a3a3a3;"
															id="titleImgIsShowBodyLable"><input
															id="titleImgIsShowBody" style="float:left"
															type="checkbox" value="" />封面图片显示在正文中 </label>
													</div>
													<a name="submit" href="javascript:changeIMG();"
																style="display:block;font-size:14px;margin-left:0px;width:100%;float:left;">切换方式</a>
													<p style="width:100%;height:10px;line-height:10px;"></p>
													<% 
														  if (i == 1 || (StringUtils.isNotBlank(isSample) && "true".equals(isSample))) {
														         out.print("<a name=\"submit\" id='addButtonDescriptionImgmsg' href=\"javascript:addDescription();\" style=\"font-size:14px;margin-left: 0px\">添加摘要</a>") ;
														         ImgtxtMsgObject imgMsgTitle=null;
														         String descriptionTitle=null;
															     if(i==1)
															       imgMsgTitle = listImgMsg.get(0);
															      out.print("<div id=\"imgmsg_div_description_div\" style=\"width: 100%;display:none\"><label> 摘要</label><br /><textarea id=\"imgmsg_edit_description\" rows=\"4\""
																 				+ " style=\"overflow-x:hidden;width:380px;margin-bottom:10px;\" onchange=\"updateDescription(this.value,'"+isSampleImgmsg+"')\">");
																 if(i==1 && StringUtils.isNotBlank(imgMsgTitle.getDescription())){
																   descriptionTitle= imgMsgTitle.getDescription();
																   out.print(imgMsgTitle.getDescription());
																  }
															      out.print("</textarea></div><p style=\"width:100%;height:5px;line-height:5px;\"></p>");
															     if(StringUtils.isNotBlank(descriptionTitle))
															      out.print("<script type=\"text/javascript\">addDescription();</script>");
															}
														%>
													<label> 正文 </label>
													<%
															out.print("<textarea id=\"imgmsg_edit_urlContent\" name=\"imgmsg_edit_urlContent\" rows=\"0\""
																	+ " style=\"overflow-x:hidden;overflow-y:hidden;\" >");
															if (i > 0) {
															    if(StringUtils.isNotBlank(listImgMsg.get(0).getUrlContent()))
																   out.print(listImgMsg.get(0).getUrlContent());
															}
															out.print("</textarea>");
														%>
													<script type="text/javascript">
						CKEDITOR.replace('imgmsg_edit_urlContent',
						{
							toolbar:
							[
							    '/',
							    { name: 'document',    items : [ 'Maximize','Source','Preview'] },
							    { name: 'basicstyles', items : [ 'Bold','Italic','Underline'] },
							    { name: 'paragraph',   items : [ 'NumberedList','BulletedList','JustifyLeft','JustifyCenter','JustifyRight'] },
							    { name: 'insert',      items : [ 'Image','Link','Unlink','Table'] },
							    { name: 'colors',      items : [ 'RemoveFormat','TextColor','BGColor' ] }
							],
				        	filebrowserBrowseUrl : '<%=request.getContextPath()+(ClusterConfigServiceHelper.getAppURI(request)!=null?"/app/"+ClusterConfigServiceHelper.getAppURI(request):"")%>/matmgr/imgtxtmsg/jsp/file_imgmsg_json.jsp',
						    filebrowserUploadUrl : '<%=request.getContextPath()+(ClusterConfigServiceHelper.getAppURI(request)!=null?"/app/"+ClusterConfigServiceHelper.getAppURI(request):"")%>/matmgr/imgtxtmsg/jsp/upload_json.jsp'
																			});
															CKEDITOR.on('instanceReady', function(ev) {
																var editor = ev.editor;
																var editorContainer = document.getElementById('imgmsg_edit_urlContent').parentNode;
																var resizeFn = function() {
																	editor.resize('100%', 245);
																	editor.resize('60%', editorContainer.offsetHeight/2);
																};
																if (editorContainer.addEventListener)
																	window.addEventListener('resize', resizeFn, false)
																else
																	window.attachEvent('onresize', resizeFn)
																resizeFn();
																// 							editor.focus()
															});
															CKEDITOR.config.resize_enabled = false;
															CKEDITOR.instances['imgmsg_edit_urlContent'].on('blur', function(e) {//光标改变时，自动进行更新
																if (e.editor.checkDirty()) {
																	var content = CKEDITOR.instances.imgmsg_edit_urlContent.getData();
																	var reg = /<p>\s*<meta (?:.+?)>\s*<\/p>/i;
																	content = content.replace(reg, '');
																	updateContent(content,'<%=isSampleImgmsg%>');
															}
														});
													</script>
												</div>
												<div id="addSourceUrlImgmsgDiv"
													style="width: 100%;display:none;margin-top:5px;">
													<label> 原文链接</label><br />
													<%
																if (i > 0) {
																	    String sourceUrl = listImgMsg.get(0).getSource();
																		out.print("<input class=\"msg-input\" id=\"imgmsg_edit_source\" style=\"width:380px;height:25px;line-height:25px;margin-bottom:10px;\""
																				+ "type=\"text\" value=\"" + (StringUtils.isNotBlank(sourceUrl)?sourceUrl:"http://")
																				+ "\" onchange=\"updateSource(this.value,'"+isSampleImgmsg+"')\">");
																	} else {
																		out.print("<input class=\"msg-input\" id=\"imgmsg_edit_source\" style=\"width:380px;height:25px;line-height:25px;margin-bottom:10px;\""
																				+ "type=\"text\" value=\"http://\" onchange=\"updateSource(this.value,'"+isSampleImgmsg+"')\">");
																	}
															%>
												</div>
												<%  
														   String autoWifiLoadChecked = "";
												           if(i>0){
												             String sourceUrl = listImgMsg.get(0).getSource();
												             String isWifiAutoLoad = listImgMsg.get(0).getIsWifiAutoLoad();
												             autoWifiLoadChecked = (StringUtils.isNotBlank(isWifiAutoLoad) && "true".equals(isWifiAutoLoad))? "true":"";
													           if(StringUtils.isNotBlank(sourceUrl)){
													              out.print("<script type=\"text/javascript\">addSourceUrl();</script>");
													           }else{
													             out.print("<a name=\"submit\" id='addButtonSourceUrlImgmsg' href=\"javascript:addSourceUrl();\" style=\"font-size:14px;margin-left: 0px\">添加原文链接</a>");
													           }
												           }else
												            out.print("<a name=\"submit\" id='addButtonSourceUrlImgmsg' href=\"javascript:addSourceUrl();\" style=\"font-size:14px;margin-left: 0px\">添加原文链接</a>");
														    ImgtxtMsgObject imgMsgTitle=null;
														    if(i>0)
														      imgMsgTitle = listImgMsg.get(0);
														    String tag = "多个标签用逗号分隔";
														    if(imgMsgTitle!=null)
														       tag = StringUtils.isNotBlank(imgMsgTitle.getTag())?imgMsgTitle.getTag():tag;
														    out.print("<div id=\"imgmsgTagShowDiv\" style=\"width: 100%;margin-top:5px;\"><label> 分类标签</label><br /><input class=\"msg-input\" id=\"imgmsg_edit_tag\" "
														    +"onfocus=\"if(value =='多个标签用逗号分隔'){value ='';this.style.color='#000'}\" onblur=\"if (value ==''){value='多个标签用逗号分隔';this.style.color='#999'}\" "
														    +"style=\"width:380px;height:25px;line-height:25px;margin-bottom:10px;color:#999999\""
																		+ "type=\"text\" value=\""+tag+"\" onchange=\"updateTag(this.value,'"+isSampleImgmsg+"')\"></div>");
														%>
												<label
													style="float:left;margin-left: 0px;color: #a3a3a3;margin-bottom:5px;"
													id="wifiNetWordAutoLoadImg"><input
													id="imgIsHtmlAutoLoad" type="checkbox" value=""
													onchange="updateCheckWifiAutoLoad('<%=isSampleImgmsg%>',this.checked)"
													checked="<%=autoWifiLoadChecked%>" style="float:left" />wifi网络时自动加载正文图片（微信平台下）
												</label>
											</div>
											<span id="msg-arrow-out" class="abs msg-arrow a-out"
												style="margin-top: 0px;"></span> <span id="msg-arrow-in"
												class="abs msg-arrow a-in" style="margin-top: 0px;"></span>
										</div>
									</div>
								</div>
								<p class="tc msg-btn" style="text-align: center;">
									<div class="btn_b" style="margin-left:350px;margin-top:10px;">
										<a
											href="javascript:saveImgMsg('<%=imgmsgId%>','<%=isSampleImgmsg%>');"
											style="color:#fff; font-weight:bold;line-height:30px;text-decoration: none">保&nbsp;&nbsp;存</a>
									</div>
								</p>
							</div>
						</div>
					</div>
					<div
						class="rel sub-msg-item appmsgItem sub-msg-opr-show edit-msg-opr-show"
						style="display:none" id="appmsgItemShow"
						onmouseover="mouseOver(this);" onmouseout="mouseOut(this);"></div>
				</div>
			</div>
		</div>
	</div>
	<%
	  if("false" == isSampleImgmsg && i<2)
	    out.print("<script type=\"text/javascript\">addImgMsg();</script>");
	%>
	<script type="text/javascript">
		selectedCheckbox();
	</script>
</body>
</html>