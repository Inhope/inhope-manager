<%@page import="com.incesoft.xmas.matmgr.domain.ImgtxtMsgObject"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%
	int dataSize = 0;
	List<ImgtxtMsgObject> listImgMsg = (List<ImgtxtMsgObject>) request.getAttribute("imgmsg");
	dataSize = listImgMsg.size();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/imgmsg.css">
<title>图文消息</title>
</head>
<body style="border:0;padding:0;margin:0">
	<%
		// 		 System.out.println("长度："+dataSize);
		if (dataSize > 0) {
			ImgtxtMsgObject titleImgmsg = listImgMsg.get(0);
			out.print("<div class=\"show-img-msg-left\"><div class=\"msg-item-wrapper\" >");
			out.print("<div class=\"msg-item multi-msg\">");
			if (1 == listImgMsg.size()) {
				out.print("<div class=\"appmsgItem\"><h4 class=\"appmsg_title\" style=\"font-weight:400;font-style: normal;font-size:16px;line-height:24px;"
						+ "/white-space:pre-wrap;/word-wrap:normal;word-break:normal;word-break:break-all;margin:0 10px;\">"
						+ "<span style=\"display: block;color:#666;\">"
						+ titleImgmsg.getTitle()
						+ "</span>"
						+ "</h4><div class=\"appmsg_info\" style=\"padding:5px 10px;\"><em class=\"appmsg_date\" style=\"font-weight:400;"
						+ "font-style: normal;color:#999;font-size:14px;\">"
						+ (StringUtils.isNotBlank(titleImgmsg.getDate()) ? titleImgmsg.getDate() : "")
						+ "</em></div><div class=\"cover\">"
						+ "<p class=\"default-tip\" style=\"display: none\">封面图片</p>"
						+ "<img class=\"i-img\" src=\""
						+ titleImgmsg.getImg()
						+ "\" /> </div><div class=\"rel sub-msg-item appmsgItem sub-msg-opr-show\" style=\"border-top:0;padding:5px 10px;max-height:none;\">"
						+ "<h4 class=\"msg-t\" style=\"padding-top:0;max-height:none;line-height:22px;\"><span class=\"i-title\">"
						+ (StringUtils.isNotBlank(titleImgmsg.getDescription()) ? titleImgmsg
								.getDescription() : "") + "</span></h4></div></div>");
			} else {
				out.print("<div class=\"appmsgItem\"><div class=\"appmsg_info\" style=\"padding:5px 10px;\"><em class=\"appmsg_date\" style=\"font-weight:400;"
						+ "font-style: normal;color:#999;font-size:14px;\">"
						+ (StringUtils.isNotBlank(titleImgmsg.getDate()) ? titleImgmsg.getDate() : "")
						+ "</em></div><div class=\"cover\"><p class=\"default-tip\" style=\"display: none\">封面图片</p>"
						+ "<h4 class=\"msg-t\">"
						+ "<span class=\"i-title\" style=\"color: #fff;\" id=\"appmsgItem1_title\">"
						+ titleImgmsg.getTitle()
						+ "</span></h4>"
						+ "<img class=\"i-img\" style=\"\" src=\""
						+ titleImgmsg.getImg()
						+ "\" /></div></div>");

				for (int s = 1; s < listImgMsg.size(); s++) {
					ImgtxtMsgObject imgmsgItem = listImgMsg.get(s);
					out.print("<div class=\"rel sub-msg-item appmsgItem sub-msg-opr-show\">"
							+ "<span class=\"thumb\"> <span class=\"default-tip\" style=\"display: none\">缩略图</span>"
							+ "<img class=\"i-img\" src=\"" + imgmsgItem.getImg()
							+ "\" style=\"\"></span><h4 class=\"msg-t\">" + "<span class=\"i-title\">"
							+ imgmsgItem.getTitle() + "</span></h4></div>");
				}
				out.print("</div></div></div>");
			}
		}
	%>
</body>
</html>