<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.incesoft.xmas.matmgr.service.ImgtxtMsgService"%>
<%@page import="com.incesoft.xmas.matmgr.web.base.PageEx"%>
<%@page import="com.incesoft.xmas.matmgr.domain.ImgtxtMsgObject"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page
	import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%
	ImgtxtMsgService imgMsgService = WebApplicationContextUtils.getWebApplicationContext(application)
	.getBeansOfType(ImgtxtMsgService.class).values().iterator().next();
	PageEx pg = (PageEx) request.getAttribute("pg");
	int dataSize = 0;
	List<List<ImgtxtMsgObject>> listLeft = null;
	List<List<ImgtxtMsgObject>> listRight = null;
	if (pg != null) {
		List<String> list = pg.getResult();
		if (list != null && list.size() > 0) {
	listLeft = new ArrayList<List<ImgtxtMsgObject>>(); //左侧div 内容
	listRight = new ArrayList<List<ImgtxtMsgObject>>(); //右侧div 内容
	for (int i = 0; i < list.size(); i++) {
		String imgmsgId = list.get(i);
		if (StringUtils.isNotBlank(imgmsgId)) {
		    dataSize++;
		    List<ImgtxtMsgObject> imgtxtMsgList =  imgMsgService.getImgmsgById(imgmsgId,false);
	             	 if(imgtxtMsgList.size()>0){
		if (i % 2 == 0) {
		  listLeft.add(imgtxtMsgList);
		} else {
	      listRight.add(imgtxtMsgList);
		}
		     }
		}
	}
		}
	}
	String materialId = request.getParameter("materialId");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/api.css" />
<link rel="stylesheet" type="text/css" href="css/imgmsg.css">
<title>图文消息</title>
<script>
	var selectDiv;
	function mouseOver(div) {
		div.style.display = "block";
	}
	function mouseOut(div) {
		div.style.display = "none";
	}
	function selectClick(sdiv, imgmsgId, title, isClick) {
		if (selectDiv && selectDiv != sdiv) {
			selectDiv.style.display = "none";
		}
		selectDiv = sdiv;
		sdiv.style.display = "block";
		document.getElementById('selectedVal').value = imgmsgId;
		if (isClick)
			parent.answerRendered({
				id : imgmsgId,
				type : 'imgtxtmsg',
				title : title
			});
		// alert(imgmsgId);
	}
</script>
</head>
<body style="background-color:#F3F3F3;">
	<div style="width: 735px;">
		<input type="hidden" id="selectedVal" /> <input type="hidden"
			id="type" value="imgtxtmsg" />
		<!-- 		<%@include file="../nav-templatePage.jsp"%> -->
		<div id="borderDiv"
			style="margin: 5px 0px 0 10px; border: 1px solid #DDDDDD;display: inline-block; width: 720px;">
			<%
				if (dataSize <= 0) {
					out.print("<div style='margin-top:25px;color:#666666;text-align: center;font-family:\"Microsoft YaHei\",Helvetica,Verdana,Arial,Tahoma;font-size:14px'>没有数据</div><script>document.getElementById('borderDiv').style.border='none'</script>");
				}
			%>
			<div id="showImgmsgLeft"
				style="margin: 10px 0 0 20px; float: left; width: 350px;">
				<%
					if (dataSize > 0) {
						int currentSize = 0;
						for (List<ImgtxtMsgObject> listImgMsg : listLeft) {
							currentSize++;
							ImgtxtMsgObject titleImgmsg = listImgMsg.get(0);
							out.print("<div class=\"show-img-msg-left\"><div class=\"msg-item-wrapper\" onmouseover=\"mouseOver(imgmsg_left_"
									+ currentSize
									+ ");\" onmouseout=\"mouseOut(imgmsg_left_"
									+ currentSize
									+ ");\" onclick=\"selectClick(imgmsg_left_click_"
									+ currentSize
									+ ",'"
									+ titleImgmsg.getImgmsgId() + "','" + titleImgmsg.getTitle() + "',true);\">");
							out.print("<div class=\"msg-item multi-msg\" id=\"img_msg_item\">");
							out.print("<div class=\"imgmsg-item-click\" id=\"imgmsg_left_click_" + currentSize
									+ "\"></div>");
							out.print("<div class=\"imgmsg-item-mask\" id=\"imgmsg_left_" + currentSize + "\"></div>");
							if (1 == listImgMsg.size()) {
								out.print("<div class=\"appmsgItem\"><h4 class=\"appmsg_title\" style=\"font-weight:400;font-style: normal;font-size:16px;line-height:24px;"
										+ "/white-space:pre-wrap;/word-wrap:normal;word-break:normal;word-break:break-all;margin:0 10px;\">"
										+ "<span style=\"display: block;color:#666;\">"
										+ titleImgmsg.getTitle()
										+ "</span>"
										+ "</h4><div class=\"appmsg_info\" style=\"padding:5px 10px;\"><em class=\"appmsg_date\" style=\"font-weight:400;"
										+ "font-style: normal;color:#999;font-size:14px;\">"
										+ (StringUtils.isNotBlank(titleImgmsg.getDate()) ? titleImgmsg.getDate() : "")
										+ "</em></div><div class=\"cover\">"
										+ "<p class=\"default-tip\" style=\"display: none\">封面图片</p>"
										+ "<img class=\"i-img\" src=\""
										+ titleImgmsg.getImg()
										+ "\" /> </div><div class=\"rel sub-msg-item appmsgItem sub-msg-opr-show\" style=\"border-top:0;padding:5px 10px;max-height:none;\">"
										+ "<h4 class=\"msg-t\" style=\"padding-top:0;max-height:none;line-height:22px;\"><span class=\"i-title\">"
										+ (StringUtils.isNotBlank(titleImgmsg.getDescription()) ? titleImgmsg
												.getDescription() : "") + "</span></h4></div></div>");
							} else {
								out.print("<div class=\"appmsgItem\"><div class=\"appmsg_info\" style=\"padding:5px 10px;\"><em class=\"appmsg_date\" style=\"font-weight:400;"
										+ "font-style: normal;color:#999;font-size:14px;\">"
										+ (StringUtils.isNotBlank(titleImgmsg.getDate()) ? titleImgmsg.getDate() : "")
										+ "</em></div><div class=\"cover\"><p class=\"default-tip\" style=\"display: none\">封面图片</p>"
										+ "<h4 class=\"msg-t\">"
										+ "<span class=\"i-title\" style=\"color: #fff;\" id=\"appmsgItem1_title\">"
										+ titleImgmsg.getTitle()
										+ "</span></h4>"
										+ "<img class=\"i-img\" style=\"\" src=\""
										+ titleImgmsg.getImg()
										+ "\" /></div></div>");

								for (int s = 1; s < listImgMsg.size(); s++) {
									ImgtxtMsgObject imgmsgItem = listImgMsg.get(s);
									out.print("<div class=\"rel sub-msg-item appmsgItem sub-msg-opr-show\">"
											+ "<span class=\"thumb\"> <span class=\"default-tip\" style=\"display: none\">缩略图</span>"
											+ "<img class=\"i-img\" src=\"" + imgmsgItem.getImg()
											+ "\" style=\"\" /></span><h4 class=\"msg-t\">"
											+ "<span class=\"i-title\">" + imgmsgItem.getTitle()
											+ "</span></h4></div>");
								}
							}
							out.print("</div></div></div>");
							if (StringUtils.isNotBlank(materialId)
									&& listImgMsg.get(currentSize - 1).getImgmsgId().equals(materialId)) {
								out.print("<script>selectClick(imgmsg_left_click_" + currentSize + ",'" + materialId
										+ "','" + listImgMsg.get(currentSize - 1).getTitle() + "',false)</script>");
							}
						}
					}
				%>
			</div>
			<div id="showImgmsgRight"
				style="margin-top: 10px;float:right;width:350px;">
				<%
					if (dataSize > 1) {
						int currentSize = 0;
						for (List<ImgtxtMsgObject> listImgMsg : listRight) {
							currentSize++;
							ImgtxtMsgObject titleImgmsg = listImgMsg.get(0);
							out.print("<div class=\"show-img-msg-left\"><div class=\"msg-item-wrapper\" onmouseover=\"mouseOver(imgmsg_right_"
									+ currentSize
									+ ");\" onmouseout=\"mouseOut(imgmsg_right_"
									+ currentSize
									+ ");\" onclick=\"selectClick(imgmsg_right_click_"
									+ currentSize
									+ ",'"
									+ titleImgmsg.getImgmsgId() + "','" + titleImgmsg.getTitle() + "',true);\">");
							out.print("<div class=\"msg-item multi-msg\" id=\"img_msg_item\">");
							out.print("<div class=\"imgmsg-item-click\" id=\"imgmsg_right_click_" + currentSize
									+ "\"></div>");
							out.print("<div class=\"imgmsg-item-mask\" id=\"imgmsg_right_" + currentSize
									+ "\"></div>");
							if (1 == listImgMsg.size()) {
								out.print("<div class=\"appmsgItem\"><h4 class=\"appmsg_title\" style=\"font-weight:400;font-style: normal;font-size:16px;line-height:24px;"
										+ "/white-space:pre-wrap;/word-wrap:normal;word-break:normal;word-break:break-all;margin:0 10px;\">"
										+ "<span style=\"display: block;color:#666;\">"
										+ titleImgmsg.getTitle()
										+ "</span>"
										+ "</h4><div class=\"appmsg_info\" style=\"padding:5px 10px;\"><em class=\"appmsg_date\" style=\"font-weight:400;"
										+ "font-style: normal;color:#999;font-size:14px;\">"
										+ (StringUtils.isNotBlank(titleImgmsg.getDate()) ? titleImgmsg.getDate() : "")
										+ "</em></div><div class=\"cover\">"
										+ "<p class=\"default-tip\" style=\"display: none\">封面图片</p>"
										+ "<img class=\"i-img\" src=\""
										+ titleImgmsg.getImg()
										+ "\" /> </div><div class=\"rel sub-msg-item appmsgItem sub-msg-opr-show\" style=\"border-top:0;padding:5px 10px;max-height:none;\">"
										+ "<h4 class=\"msg-t\" style=\"padding-top:0;max-height:none;line-height:22px;\"><span class=\"i-title\">"
										+ (StringUtils.isNotBlank(titleImgmsg.getDescription()) ? titleImgmsg
												.getDescription() : "") + "</span></h4></div></div>");
							} else {
								out.print("<div class=\"appmsgItem\"><div class=\"cover\"><div class=\"appmsg_info\" style=\"padding:5px 10px;\"><em class=\"appmsg_date\" style=\"font-weight:400;"
										+ "font-style: normal;color:#999;font-size:14px;\">"
										+ (StringUtils.isNotBlank(titleImgmsg.getDate()) ? titleImgmsg.getDate() : "")
										+ "</em></div><p class=\"default-tip\" style=\"display: none\">封面图片</p>"
										+ "<h4 class=\"msg-t\">"
										+ "<span class=\"i-title\" style=\"color: #fff;\" id=\"appmsgItem1_title\">"
										+ titleImgmsg.getTitle()
										+ "</span></h4>"
										+ "<img class=\"i-img\" style=\"\" src=\""
										+ titleImgmsg.getImg()
										+ "\" /></div></div>");
								for (int s = 1; s < listImgMsg.size(); s++) {
									ImgtxtMsgObject imgmsgItem = listImgMsg.get(s);
									String urlItem = StringUtils.isNotBlank(imgmsgItem.getDescription()) ? imgmsgItem
											.getDescription() : imgmsgItem.getSource();
									out.print("<div class=\"rel sub-msg-item appmsgItem sub-msg-opr-show\">"
											+ "<span class=\"thumb\"> <span class=\"default-tip\" style=\"display: none\">缩略图</span>"
											+ "<img class=\"i-img\" src=\"" + imgmsgItem.getImg()
											+ "\" style=\"\" /></span><h4 class=\"msg-t\">"
											+ "<span class=\"i-title\">" + imgmsgItem.getTitle()
											+ "</span></h4></div>");
								}
							}
							out.print("</div></div></div>");
							if (StringUtils.isNotBlank(materialId)
									&& listImgMsg.get(currentSize - 1).getImgmsgId().equals(materialId)) {
								out.print("<script>selectClick(imgmsg_right_click_" + currentSize + ",'" + materialId
										+ "','" + listImgMsg.get(currentSize - 1).getTitle() + "',false)</script>");
							}
						}
					}
				%>
			</div>
		</div>
		<%@include file="../nav-templatePage.jsp"%>
	</div>
</body>
</html>