<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="res" uri="/WEB-INF/tld/import-res.tld"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="usr" uri="/WEB-INF/tld/login-user.tld"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>素材管理</title>
	<%@include file="/commons/extjsvable.jsp"%>
	<link rel="stylesheet" type="text/css" href="custom.css" />
	<link rel="stylesheet" type="text/css" href="dataview.css" />
	<res:import dir="script" recursive="true" />
	<usr:checkPerm jsExportVar="mat_check_perm_result" permissionNames="mat" dump="true" />
	<usr:name jsExportVar="mat_username"/>
</head>
<body>
</body>
</html>