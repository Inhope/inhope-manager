function selectChangeState() {
	var selected = $("select[name='acsNotInSelect']");
	var optionState = selected[0].value;
	var notInPrompt = "";
	if (4 >= optionState) {
		notInPrompt = "客服工作时间为:" + selected[0][optionState - 1].innerHTML
				+ "9:00—18:00，请在客服工作时间内进行咨询";
	} else {
		notInPrompt = selected[0][optionState - 1].innerHTML;
	}
	$("textarea[name='acsNotAvailablePrompt']").val(notInPrompt);
}

function csReplyStateSave() {
	var acsConnectingPrompt = $("input[name='acsConnectingPrompt']").val();
	var acsConnSuccPrompt = $("input[name='acsConnSuccPrompt']").val();
	var acsConnFailPrompt = $("input[name='acsConnFailPrompt']").val();
	var acsConnClosedPrompt = $("input[name='acsConnClosedPrompt']").val();
	var acsLeavePrompt = $("input[name='acsLeavePrompt']").val();
	var acsConnBeClosedPrompt = $("input[name='acsConnBeClosedPrompt']").val();
	var acsNotAvailablePrompt = $("textarea[name='acsNotAvailablePrompt']")
			.val();
	if (acsConnectingPrompt && acsConnSuccPrompt && acsConnFailPrompt
			&& acsConnClosedPrompt && acsLeavePrompt && acsConnBeClosedPrompt
			&& acsNotAvailablePrompt) {
		$.ajax({
			type : "POST",
			url : "acs!saveReplyState.action",
			data : {
				acsConnectingPrompt : acsConnectingPrompt,
				acsConnSuccPrompt : acsConnSuccPrompt,
				acsConnFailPrompt : acsConnFailPrompt,
				acsConnClosedPrompt : acsConnClosedPrompt,
				acsLeavePrompt : acsLeavePrompt,
				acsConnBeClosedPrompt : acsConnBeClosedPrompt,
				acsNotAvailablePrompt : acsNotAvailablePrompt
			},
			dataType : 'json',
			success : function(data) {
				if ("true" == data.success) {
					alert(data.msg);
					loadCsStateReplyPro();
				} else {
					alert(data.msg);
				}
			},
			error : function(data) {
				alert("保存回复内容异常！" + data.msg);
			}
		});
	} else {
		alert("状态回复不能为空！");
	}
}

function loadCsStateReplyPro() {
	$.ajax({
		type : "POST",
		url : "acs!loadStateReplyPro.action",
		dataType : 'json',
		success : function(data) {
			if (data) {
				if ("true" == data.success) {
					$("input[name='acsConnectingPrompt']").val(
							data.acsConnectingPrompt);
					$("input[name='acsConnSuccPrompt']").val(
							data.acsConnSuccPrompt);
					$("input[name='acsConnFailPrompt']").val(
							data.acsConnFailPrompt);
					$("input[name='acsConnClosedPrompt']").val(
							data.acsConnClosedPrompt);
					$("input[name='acsLeavePrompt']").val(data.acsLeavePrompt);
					$("input[name='acsConnBeClosedPrompt']").val(
							data.acsConnBeClosedPrompt);
					$("textarea[name='acsNotAvailablePrompt']").val(
							data.acsNotAvailablePrompt);
				}else{
                  initCsPro();				
				}
			}
		},
		error : function(data) {
			alert("加载回复内容异常！" + data.msg);
		}
	});
}

function saveDefaultMessageResource() {
	$.ajax({
		type : "POST",
		url : "acs!saveDefaultMessageResource.action",
		dataType : 'json',
		success : function(data) {
			if (data) {
				if ("true" == data.success) {
					loadCsStateReplyPro();
				} else {
					alert("默认回复失败！");
				}
			}
		},
		error : function(data) {
			alert("保存默认回复异常！" + data.msg);
		}
	});
}
function initCsPro(){
	$.ajax({
		type : "POST",
		url : "acs!initDefaultMR.action",
		dataType : 'json',
		success : function(data) {
			if (data) {
				if ("true" == data.success) {
					$("input[name='acsConnectingPrompt']").val(
							data.acsConnectingPrompt);
					$("input[name='acsConnSuccPrompt']").val(
							data.acsConnSuccPrompt);
					$("input[name='acsConnFailPrompt']").val(
							data.acsConnFailPrompt);
					$("input[name='acsConnClosedPrompt']").val(
							data.acsConnClosedPrompt);
					$("input[name='acsLeavePrompt']").val(data.acsLeavePrompt);
					$("input[name='acsConnBeClosedPrompt']").val(
							data.acsConnBeClosedPrompt);
					$("textarea[name='acsNotAvailablePrompt']").val(
							data.acsNotAvailablePrompt);
					$("input[name='csProjectName']").val(data.projectName);
				} else {
					alert("初始化信息失败！");
				}
			}
		},
		error : function(data) {
			alert("初始化默认信息异常！" + data.msg);
		}
	});
}
window.appInstalled = function() {
	$.ajax({
		type : "POST",
		url : "acs!saveDefaultMR.action",
		dataType : 'json',
		success : function(data) {
			if (data) {
				if ("true" == data.success) {
                     initCsPro();
				} else {
					alert("签名校验失败！");
				}
			}
		},
		error : function(data) {
			alert("初始化保存默认信息异常！" + data.msg);
		}
	});
};
$(document).ready(function() {
	loadCsStateReplyPro();
});