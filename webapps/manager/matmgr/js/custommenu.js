window.excludeApp = [ 'menu' ];

$(document).ready(function() {

	// window.integerationRendered = function() {
	// $('.channels', $('#f_content')).css('width', '355px').css('height',
	// '110px');
	// $('.tooltitle3', $('#f_content')).css('width', '357px');
	// $('.tooltitle2', $('#f_content')).css('width', '357px');
	// $('.tooltitle1', $('#f_content')).css('width', '357px');
	// $('.kb-cnrighttwo', $('#f_content')).css('width', '365px');
	// };
	// var contentField = addTemplate_integeration('f_content');

	var _contentFrame = $('#_content').get(0);

	// addTab('intro', '简介', null, true);
	addTab('content', '管理', null, true);
	var treePanel = document.getElementById('tree_frame').contentWindow;
	var record = [ 'id', 'key', 'name', 'header', 'footer', 'content', 'status' ];

	var lastRootClick = null;
	var rootClick = function() {
		$('tr[name0=rootmenu]').css('backgroundColor', '').attr('selected', '');
		$(this).css('backgroundColor', '#fff7e1').attr('selected', 'true');
		var currentId = this.id;
		if (lastRootClick != currentId) {
			targetRoot = $(this);
			treePanel.location.reload();
			lastRootClick = currentId;
		}
	};
	$('tr[name0=rootmenu]').each(function(idx, el) {
		$(el).css('cursor', 'pointer');
		$(el).click(function() {
			rootClick.call(this);
		});
		if (idx == 0)
			$(el).css('backgroundColor', '#fff7e1').attr('selected', 'true');
	});

	var name = $('#f_name');
	var key = $('#f_key');
	var header = $('#f_header');
	var footer = $('#f_footer');
	var allFields = $([]).add(name).add(name).add(key);
	var tips = $('.validateTips');
	var updateTips = function(t, isError) {
		if (isError)
			tips.css('color', 'red');
		else
			tips.css('color', '');
		tips.text(t).addClass("ui-state-highlight");
		setTimeout(function() {
			tips.removeClass("ui-state-highlight", 1000);
		}, 500);
	};
	var checkLength = function(o, n, max, allowBlank) {
		if (o.css('display') == 'none')
			return true;
		var val = $.trim(o.val());
		if (!val && allowBlank)
			return true;
		if (val.length == 0 || o.val().length > max) {
			o.addClass("ui-state-error");
			if (allowBlank)
				updateTips(n + '不能超过' + max + '字符', true);
			else
				updateTips(n + '不能为空且不能超过' + max + '字符', true);
			return false;
		} else {
			return true;
		}
	};

	var _getContent = function() {
		var content = _contentFrame.contentWindow.selectSubmit(true);
		if (console)
			console.log(content);
		return content;
	};

	window.editNode0 = function(cfg) {
		_contentFrame.contentWindow.location.reload();
		$('#menu_form').dialog({
			height : 500,
			width : 500,
			modal : true,
			buttons : {
				'保存' : function() {
					var self = this;
					var bValid = true;
					var dataObj = {};
					allFields.removeClass('ui-state-error');
					if (cfg && cfg.isLeaf) {
						var contentVal = _getContent();
						if (!contentVal) {
							updateTips('菜单内容不能为空', true);
							bValid = false;
						} else if (contentVal.anwser && contentVal.answer.length > 600) {
							updateTips('菜单内容不能超过600个字符，当前长度为' + contentVal.answer.length + "。", true);
							bValid = false;
						} else {
							if (contentVal && contentVal.answer)
								dataObj.content = $.toJSON(contentVal);
							else
								dataObj.content = contentVal;
						}
					} else if (cfg && cfg.isNode) {
						bValid = bValid && checkLength(name, '菜单名称', 100);
						bValid = bValid && checkLength(header, '菜单标题', 600, true);
						bValid = bValid && checkLength(footer, '菜单脚注', 600, true);
					} else {
						bValid = bValid && checkLength(name, '菜单名称', 100);
						bValid = bValid && checkLength(key, '触发键', 20);
						bValid = bValid && checkLength(header, '菜单标题', 600, true);
						bValid = bValid && checkLength(footer, '菜单脚注', 600, true);
					}
					if (bValid) {
						$('*[id^=f_]').each(function() {
							var el = $(this);
							var key = el.attr('id').substring(2);
							var val = el.val();
							if (key != 'content' && val && $.trim(val))
								dataObj[key] = val;
						});
						delete dataObj.status;
						var dataJson = $.toJSON(dataObj);
						var isAdd = !dataObj.id, isRoot = !dataObj.parentId;
						$.ajax({
							url : 'menu!save.action',
							type : 'POST',
							data : {
								data : dataJson
							},
							success : function(respTxt) {
								if ('key_duplicated' == respTxt) {
									key.addClass("ui-state-error");
									updateTips('已存在触发键为' + key.val() + '的菜单。');
									return false;
								}
								var ret = eval('ret = ' + respTxt);
								if (isRoot) {
									dataObj.id = ret.id;
									renderRoot(isAdd, dataObj);
								} else if (cfg && cfg.cb && typeof cfg.cb == 'function')
									cfg.cb.call(cfg.cbthis ? cfg.cbthis : this, isAdd, ret);
								updateTips('保存成功');
								$(self).dialog('close');
							}
						});
					}
				},
				'取消' : function() {
					$(this).dialog('close');
				}
			},
			close : function() {
				// closeDimSelectDiv();
				allFields.val('').removeClass('ui-state-error');
			}
		});
	};
	var resetForm = function() {
		$('*[id^=f_]', $('#menu_form')).each(function() {
			var el = $(this);
			if (el.get(0).tagName.toLowerCase == 'select')
				el.val('1');
			else
				el.val('');
			tips.html('');
		});
	};
	var loadRecord = function(data) {
		$('*[id^=f_]', $('#menu_form')).each(function() {
			var el = $(this);
			el.val(data[el.attr('id').substring(2)]);
		});
	};

	var renderRoot = function(isAdd, data) {
		if (isAdd) {
			var tr = $('<tr></tr>').attr('name0', 'rootmenu').css('cursor', 'pointer');
			for ( var i = 0; i < record.length; i++) {
				if (data[record[i]])
					tr.attr(record[i], data[record[i]]);
			}
			tr.click(function() {
				rootClick.call(this);
			});
			$('#root_cont').append(tr);
			for ( var i = 0; i < 3; i++) {
				var td = $('<td></td>');
				td.attr('height', '30').attr('align', 'center').attr('valign', 'middle');
				td.addClass('Ap-line2').addClass('CM-cl01');
				if (i == 0)
					td.html($('tr[name0=rootmenu]').length);
				else if (i == 1)
					td.html(data.name);
				else
					td.html(data.key);
				td.appendTo(tr);
			}
			if ($('tr[name0=rootmenu]', $('#root_cont')).length == 1)
				tr.trigger('click');
		} else {
			var tr = $('#' + data.id);
			for ( var i = 0; i < record.length; i++) {
				if (data[record[i]])
					tr.attr(record[i], data[record[i]]);
			}
			$('td', tr).each(function(idx) {
				if (idx == 1)
					$(this).html(data.name);
				else if (idx == 2)
					$(this).html(data.key);
			});
			treePanel.tree.getRootNode().firstChild.setText(data.name);
		}
	};
	var getSelectedRoot = function() {
		return $('tr[selected=true]');
	};
	$('#addRootBtn').click(function() {
		var fieldList0 = [ 'name', 'header', 'footer', 'key' ];
		for ( var i = 0; i < fieldList0.length; i++) {
			$('#' + fieldList0[i] + '_part').show();
		}
		$('#content_part').hide();
		$('#f_content').hide();
		resetForm();
		// $('#f_footer').val('B-返回上一级 R-返回根目录 Q-退出菜单');
		editNode0();
	});
	// .trigger('click')
	$('#editRootBtn').click(function() {
		var row = getSelectedRoot();
		var rowData = {};
		for ( var i = 0; i < record.length; i++) {
			var val = row.attr(record[i]);
			if (val)
				rowData[record[i]] = val;
		}
		var fieldList0 = [ 'name', 'header', 'footer', 'key' ];
		for ( var i = 0; i < fieldList0.length; i++) {
			$('#' + fieldList0[i] + '_part').show();
		}
		$('#content_part').hide();
		$('#f_content').hide();
		resetForm();
		loadRecord(rowData);
		editNode0();
	});
	$('#delRootBtn').click(function() {
		var row = getSelectedRoot();
		var ret = window.confirm('你确定要删除菜单"' + row.attr('name') + '"吗？');
		if (ret)
			$.ajax({
				url : 'menu!del.action',
				type : 'POST',
				data : {
					id : row.attr('id')
				},
				success : function(respTxt) {
					var next = row.next();
					row.remove();
					if (next && next.length)
						next.trigger('click');
					else {
						targetRoot = null;
						treePanel.location.reload();
					}
				}
			});
	});
	$('#applyBtn').click(function() {
		var ret = window.confirm('你确定要应用菜单设置吗？');
		if (ret)
			$.ajax({
				url : 'menu!apply.action',
				type : 'POST',
				data : {},
				success : function(respTxt) {
					alert('应用成功');
				}
			});
	});
	window.addNode = function(node, isLeaf, cb, cbthis) {
		resetForm();
		var hideFields = null, showFields = null, dialogCfg = {};
		$('#key_part').hide();
		var fieldList0 = [ 'name', 'header', 'footer' ];
		var fieldList1 = [ 'content' ];
		if (isLeaf) {
			dialogCfg.isLeaf = true;
			dialogCfg.dialogHeight = 350;
			hideFields = fieldList0;
			showFields = fieldList1;
			$('#f_content').show();
			// contentField = reset_integeration('f_content', contentField);
		} else {
			dialogCfg.isNode = true;
			dialogCfg.dialogHeight = 400;
			hideFields = fieldList1;
			showFields = fieldList0;
			$('#f_content').hide();
			$('#f_footer').val('B-返回上一级  R-返回根目录  Q-退出菜单');
		}
		for ( var i = 0; i < hideFields.length; i++) {
			$('#' + hideFields[i] + '_part').hide();
		}
		for ( var i = 0; i < showFields.length; i++) {
			$('#' + showFields[i] + '_part').show();
		}
		$('#f_parentId').val(node.id);
		dialogCfg.cb = cb;
		dialogCfg.cbthis = cbthis;
		editNode0(dialogCfg);
	};
	window.editNode = function(node, isLeaf, cb, cbthis) {
		if (node.parentNode.id == '0') {
			$('#editRootBtn').trigger('click');
		} else {
			var hideFields = null, showFields = null, dialogCfg = {};
			var fieldList0 = [ 'name', 'header', 'footer' ];
			var fieldList1 = [ 'content' ];
			$('#key_part').hide();
			if (isLeaf) {
				dialogCfg.isLeaf = true;
				dialogCfg.dialogHeight = 350;
				hideFields = fieldList0;
				showFields = fieldList1;
				$('#f_content').show();
				// contentField = load_integeration(contentField,
				// node.attributes.attachment.content, 'f_content');
			} else {
				dialogCfg.isNode = true;
				dialogCfg.dialogHeight = 400;
				hideFields = fieldList1;
				showFields = fieldList0;
				$('#f_content').hide();
			}
			for ( var i = 0; i < hideFields.length; i++) {
				$('#' + hideFields[i] + '_part').hide();
			}
			for ( var i = 0; i < showFields.length; i++) {
				$('#' + showFields[i] + '_part').show();
			}
			var data = {};
			data.id = node.id;
			data.parentId = node.parentNode.id;
			var attrs = node.attributes.attachment;
			data.key = attrs.key;
			data.header = attrs.header;
			data.footer = attrs.footer;
			data.content = attrs.content;
			if (!isLeaf)
				data.name = node.text;
			resetForm();
			loadRecord(data);
			dialogCfg.cb = cb;
			dialogCfg.cbthis = cbthis;
			editNode0(dialogCfg);
		}
	};
	window.delNode = function(node, isLeaf) {
		$('span', $("#message-dialog-menu")).html('你确定要删除此节点吗？' + (!isLeaf ? '' : '所有子项将被一并删除！'));
		$("#message-dialog-menu").dialog({
			resizable : false,
			height : 130,
			modal : true,
			buttons : {
				"继续" : function() {
					var self = this;
					$.ajax({
						url : 'menu!del.action',
						type : 'POST',
						data : {
							id : node.id
						},
						success : function(respTxt) {
							$(self).dialog("close");
							node.parentNode.removeChild(node);
						}
					});
				},
				"取消" : function() {
					$(this).dialog("close");
				}
			}
		});
	};
});