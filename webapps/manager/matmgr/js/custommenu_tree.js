DynamicMenuPanel = function() {
	var self = this;

	this.MenuRecord = Ext.data.Record.create([ 'id', 'key', 'parentId', 'name', 'header', 'content', 'footer', 'command', 'priority', 'enabled',
			'createTime', 'bh' ]);

	var detailMenu = new Ext.menu.Menu({
		items : [ {
			text : '刷新',
			iconCls : 'icon-refresh',
			width : 50,
			handler : function() {
				detailMenu.currentNode.reload();
			}
		}, '-', {
			ref : 'newItem',
			text : '新建菜单项',
			iconCls : 'icon-add',
			width : 50,
			handler : function() {
				self.addItem(detailMenu.currentNode);
			}
		}, {
			text : '编辑菜单项',
			iconCls : 'icon-edit',
			width : 50,
			handler : function() {
				self.modifyItem(detailMenu.currentNode);
			}
		}, {
			ref : 'delFunc',
			text : '删除菜单项',
			iconCls : 'icon-delete',
			width : 50,
			handler : function() {
				self.deleteItem(detailMenu.currentNode, true);
			}
		}, '-', {
			ref : 'newContent',
			text : '新建内容项',
			iconCls : 'icon-add',
			width : 50,
			handler : function() {
				self.addItem(detailMenu.currentNode, true);
			}
		}, {
			text : '全部展开',
			iconCls : 'icon-add',
			width : 50,
			handler : function() {
				detailMenu.currentNode.expandChildNodes(true);
			}
		}, {
			text : '全部收起',
			iconCls : 'icon-delete',
			width : 50,
			handler : function() {
				detailMenu.currentNode.collapseChildNodes(true);
			}
		} ]
	});
	var detailLeafMenu = new Ext.menu.Menu({
		items : [ {
			text : '编辑内容项',
			iconCls : 'icon-edit',
			width : 50,
			handler : function() {
				self.modifyItem(detailLeafMenu.currentNode, true);
			}
		}, {
			ref : 'delFunc',
			text : '删除内容项',
			iconCls : 'icon-delete',
			width : 50,
			handler : function() {
				self.deleteItem(detailLeafMenu.currentNode);
			}
		} ]
	});

	var config = {
		region : 'center',
		lines : true,
		dataUrl : 'menu!list.action',
		border : false,
		height : 495,
		root : new Ext.tree.TreeNode({
			id : '0',
			text : 'root'
		}),
		rootVisible : false,
		autoScroll : true,
		containerScroll : true
	};
	DynamicMenuPanel.superclass.constructor.call(this, config);

	this.on('contextmenu', function(node, event) {
		var menu = node.isLeaf() ? detailLeafMenu : detailMenu;
		event.preventDefault();
		menu.currentNode = node;
		menu.delFunc.setDisabled(node.parentNode.id == '0');
		node.select();
		if (!node.isLeaf()) {
			menu.newItem.setDisabled(false);
			menu.newContent.setDisabled(false);
			node.expand(false, true, function() {
				if (node.childNodes.length) {
					if (node.childNodes.length == 1 && node.childNodes[0].isLeaf()) {
						menu.newItem.setDisabled(true);
						menu.newContent.setDisabled(true);
					} else {
						menu.newContent.setDisabled(true);
					}
				}
				menu.showAt(event.getXY());
			}, node);

		} else {
			menu.showAt(event.getXY());
		}

	});

	this.detailMenu = detailMenu;
	this.detailLeafMenu = detailLeafMenu;
};

Ext.extend(DynamicMenuPanel, Ext.tree.TreePanel, {
	init : function(rootNode) {
		if (rootNode && rootNode.length) {
			var menuRoot = new Ext.tree.AsyncTreeNode({
				text : rootNode.attr('name'),
				id : rootNode.attr('id'),
				attachment : {
					enabled : true
				},
				leaf : false
			});
			var _root = this.getRootNode();
			_root.removeAll(true);
			_root.appendChild(menuRoot);
			_root.expand();
			menuRoot.expand(false, true);
		}
	},
	destroy : function() {
		this.getRootNode().removeAll(true);
	},
	deleteItem : function(cnode, isLeaf) {
		parent.delNode(cnode, isLeaf);
	},
	addItem : function(node, isLeaf) {
		parent.addNode(node, isLeaf, this.updateNode, this);
	},
	modifyItem : function(cnode, isLeaf) {
		parent.editNode(cnode, isLeaf, this.updateNode, this);
	},
	updateNode : function(isAdd, node) {
		if (isAdd) {
			var newItem = new Ext.tree.TreeNode(node);
			var pn = this.detailMenu.currentNode;
			if (!pn.isExpanded())
				pn.expand();
			pn.appendChild(newItem);
			if (!node.leaf) {
				var ui = newItem.getUI();
				ui.removeClass('x-tree-node-leaf');
				ui.addClass('x-tree-node-collapsed');
			}
		} else {
			var n = null;
			if (node.attachment.content)
				n = this.detailLeafMenu.currentNode;
			else
				n = this.detailMenu.currentNode;
			n.attributes = node;
			n.setText(node.text);
		}
	}
});
var tree = null;
Ext.onReady(function() {
	tree = new DynamicMenuPanel();
	tree.render('tree-div');
	tree.getRootNode().expand();
	tree.init(parent.targetRoot);
});