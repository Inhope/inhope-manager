﻿/** ====================**JQuery Functions====================* */
var container;
// 对话框
WSC_ShowDialog = function(url, title) {
	if (!title) {
		title = "素材管理";
	}

	// 容器
	var id = "material_Dialog";
	container = $("#" + id);
	if (container.size() == 0) { // 不存在则创建
		container = $("<div id='material_JQueryDialogAutoGenDiv' class='ui-helper-hidden' style='z-index:99999;'></div>")
				.attr("id", id)
				.append("<iframe id='material_iframe' name='material_iframe' frameborder='0' style='width: 100%; height: 460px;'></iframe>")
				// 内容容器
				.append("<div class=\"enter_btn\"><button class=\"btn btn_blue\" onclick=\"selectSubmit();\">确定</button><button class=\"btn btn_gray\" onclick=\"closeDialog();\">取消</button></div>")
				.appendTo($(document.body)).dialog({
							autoOpen : false,
							modal : true,
							// minWidth: 320,
							// minHeight: 220,
							show : "fade",
							hide : "blind",
							draggable : false,
							resizable : false,
							close : function(event, ui) {
								$(this).children("iframe:first")
										.removeAttr("src"); // 清除内容
							}
						});
	}

	// 标题
	container.dialog("option", "title", title);
	// 尺寸
	container.dialog("option", "width", 770).dialog("option", "height", 580);
	// 内容
	container.children("iframe:first").attr("src", url);
	// 呈现
	container.dialog("open");
	container.parent().css('z-index', '999999');
};

selectSubmit = function(self) {
	var t = $(window.frames["material_iframe"].document)
			.find("input[name='file']:checked").val();
	var selectedValJObj = $("#selectedVal",
			window.frames["material_iframe"].document);
	var type = $("#type", window.frames["material_iframe"].document).val();
	if (selectedValJObj && selectedValJObj.val()) {
		if (type == 'imgtxtmsg') {
			tLp1ClickCb(selectedValJObj.val());
		} else if (type == 'imgmsg') {
			tLp4ClickCb(selectedValJObj.val());
		} else if (type == 'musicmsg') {
			tLp3ClickCb(selectedValJObj.val());
		} else if (type == 'videomsg') {
			tLp2ClickCb(selectedValJObj.val());
		}
		container.dialog("close");
	} else {
		alert("请至少选择一项");
	}
}
closeDialog = function() {
	container.dialog("close");
}

var containerPlayer;
// 对话框
window.Player_ShowDialog = function(url) {
	var title = "播放窗口";

	// 容器
	var id = "player_video_Dialog";
	containerPlayer = $("#" + id);
	if (containerPlayer.size() == 0) { // 不存在则创建
		containerPlayer = $("<div id='player_video_JQueryDialogAutoGenDiv' class='ui-helper-hidden' style='z-index:100000;padding:0'></div>")
				.attr("id", id)
				.append("<iframe id='player_video_iframe' name='player_video_iframe' frameborder='0' style='width: 100%; height: 100%;margin:0;barder:0;padding:0'></iframe>")
				.appendTo($(document.body)).dialog({
							autoOpen : false,
							modal : true,
							show : "fade",
							hide : "blind",
							draggable : false,
							resizable : false,
							close : function(event, ui) {
								$(this).children("iframe:first")
										.removeAttr("src"); // 清除内容
							}
						});
	}

	// 标题
	containerPlayer.dialog("option", "title", title);
	// 尺寸
	containerPlayer.dialog("option", "width", 482).dialog("option", "height", 377);
	// 内容
	containerPlayer.children("iframe:first").attr("src", url);
	// 呈现
	containerPlayer.dialog("open");
	containerPlayer.parent().css('z-index', '100000');
};