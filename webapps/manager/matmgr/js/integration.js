var inited = false ;
var selectDimDivJObj ;
var userAppSelDivJObj ;

function init(){
	if(!inited){
		selectDimDivJObj = $(Template.html_dim());
		$('body').append(selectDimDivJObj);
		userAppSelDivJObj = $(Template.html_app());
		$('body').append(userAppSelDivJObj);
		inited = true;
		$('#tab0').bind('click',function (){tab0_click();return false;});
		$('#tab1').bind('click',function (){tab1_click();return false;});
		$('#tab2').bind('click',function (){tab2_click();return false;});
		$('#tab3').bind('click',function (){tab3_click();return false;});
		$('#tab4').bind('click',function (){tab4_click();return false;});
		$('#tab5').bind('click',function (){tab5_click();return false;});
	}
	classification_load();
}

/**
 * @return null<br> object<br> object Array
 * */
function getDatas_integeration(containDiv){
	var isValid = true;
	var errorMsg ;
	var datas = new Array();
	var containJObj = $('#'+containDiv);
	var contentJObjs = containJObj.children('div[class="kb-cnrighttwo ov_fl"]');
	for(var ci = 0;ci<contentJObjs.length;ci++){
		var cJObj = $(contentJObjs[ci]);
		//验证
		var typeInputJObj = cJObj.find('input[type="hidden"]');
		var cType = typeInputJObj.val();
		var answer = cJObj.find('textarea').val();
		var tags = new Array();
		cJObj.find('img').each(function (){
			tags.push($(this).attr('iv'));
		});
		
		if(!answer){
			isValid = false;
			errorMsg = '内容不能为空';
			break;
		}
		var dm ;
		if(!cType || cType == 'text'){
			dm = {'answer':answer,'tags':tags};
		}else if(cType == 'imgtxtmsg'){
			dm = {'answer':answer,'tags':tags,'type':cType};
		}else if(cType == 'imgmsg'){
			dm = {'answer':answer,'tags':tags,'type':cType};
		}else if(cType == 'musicmsg'){
			dm = {'answer':answer,'tags':tags,'type':cType};
		}else if(cType == 'videomsg'){
			dm = {'answer':answer,'tags':tags,'type':cType};
		}else if(cType == 'service'){
			var serviceContainDivJObj =	cJObj.find('textarea').next('div');
			var argsJObjs = serviceContainDivJObj.children('div[name="args"]');
			var appArgsFlag = true;
			var appArgsArray = new Array();
			for(var ai = 0; ai<argsJObjs.length ;ai++){
				var argsJObj = $(argsJObjs[ai]);
				var argsName = argsJObj.attr('argsName');
				var argsOptional = argsJObj.attr('optional');
				var inputJObj = argsJObj.children('input');
				var argsVal = inputJObj.val();
				if(argsOptional=='1'){
					if(!argsVal || argsVal.length == 0){
						appArgsFlag = false;
						break;
					}
				}else{
					argsOptional = '0';
					if(!argsVal || argsVal.length == 0){
						argsVal="";
					}
				}
				appArgsArray.push({"name":argsName,"value":argsVal,"optional":argsOptional});
			}
			if(!appArgsFlag){
				isValid = false;
				errorMsg = '必填的调用参数不能为空！';
			}
			dm = {'answer':answer,'tags':tags,'type':cType,'appName':serviceContainDivJObj.attr('appName'),'args':appArgsArray};
			
			var app = appMap[answer];
			if(app){
				var appService = app['service'];
				if(appService && appService!=''){
					dm['service']=appService;			
				}
				var appType = app['type'];
				if(appType && appType-0 > 0){
					dm['exCall']=app['url'];		
				}
			}
			
			// 应用的支持平台验证
			var result = validServicePlatform(answer,tags);
			if(!result){
				isValid = false;
				errorMsg = '选择了应用不支持的平台！';
			}
			
		}
		datas.push(dm);
	}
	//
	if(!isValid){
		if(window.integrationError){
			window.integrationError(errorMsg);
		}else{
			alert(errorMsg);
		}
		return false;
	}
	if(datas.length == 1){
		return datas[0];
	}else{
		return datas;
	}
}

function validServicePlatform(appId,tagSels){
	var tags = '';
	if(tagSels.length > 0 ){
		tags = tagSels.join(',');
	}
	var result = false;
	$.ajax({
		url : 'customKnowledge!isValidAppPlatform.do',
		type : "POST",
		async:false,
		data : {
			'appId':appId,
			'tags' : tags
		},
		success : function(msg) {
        	var vstatus = msg.status;
        	if(vstatus=="1"){
        		result = true;
        	}
		},
		error : function(xhr, status, e) {
			
		}
	});
	return result;
}

/**
 * @argument containDiv : 容器id <br> obj : 加载的内容object
 * */
function addTemplate_integeration(containDiv,obj){
	init();
	var containJObj = $('#'+containDiv);
	var contentJObj = $(Template.html());
	containJObj.append(contentJObj);
	
	if(obj){
		//load
		loadContent(contentJObj,obj['type'],obj['answer'],obj['tags'],obj['appName'],obj['args']);
	}
	if(window.integerationRendered){
		window.integerationRendered(contentJObj);
	}
	return contentJObj;
}

function reset_integeration(containDiv,contentJObj){
	contentJObj.remove();
	return addTemplate_integeration(containDiv);
}

function load_integeration(contentJObj,jsonStr,containDiv){
	if(contentJObj)
		contentJObj.remove();
	contentJObj = addTemplate_integeration(containDiv);
	var jsonObj = eval('jsonObj = ' + jsonStr);
	loadContent(contentJObj,jsonObj['type'],jsonObj['answer'],jsonObj['tags'],jsonObj['appName'],jsonObj['args']);
	return contentJObj;
}

function removeContain(type,btnDomObj){
	var containJObj = $(btnDomObj).parent();
	containJObj.remove();
}

function loadContent(contentJObj,type,answer,tags,appName,appArgs){
	var chlgp1f_lP = contentJObj.find('p[class="chlg-p1 f_l"]');
	var textIndent = 0;
	var count = 0;
	if(tags){
		tags = tags.join(',');
		if(tags.indexOf('weixin')>-1){
			var weixinImg = $('<img/>').attr('title','微信').attr('alt','微信').attr('iv','weixin').attr('src','../images/channels/ch-logo02.jpg');
			chlgp1f_lP.append(weixinImg);
			count++;
		}
		if(tags.indexOf('tqq')>-1){
			var tqqImg = $('<img/>').attr('title','腾讯微博').attr('alt','腾讯微博').attr('iv','tqq').attr('src','../images/channels/ch-logo03.jpg');
			chlgp1f_lP.append(tqqImg);
			count++;
		}
		if(tags.indexOf('yixin')>-1){
			var yixinImg = $('<img/>').attr('title','易信').attr('alt','易信').attr('iv','yixin').attr('src','../images/channels/ch-logo05.png');
			chlgp1f_lP.append(yixinImg);
			count++;
		}
		if(tags.indexOf('ios')>-1){
			var iosImg = $('<img/>').attr('title','ios').attr('alt','ios').attr('iv','ios').attr('src','../images/channels/ios_img.png');
			chlgp1f_lP.append(iosImg);
			count++;
		}
		if(tags.indexOf('android')>-1){
			var androidImg = $('<img/>').attr('title','安卓').attr('alt','安卓').attr('iv','android').attr('src','../images/channels/android_img.png');
			chlgp1f_lP.append(androidImg);
			count++;
		}
		if(tags.indexOf('custom')>-1){
			var customImg = $('<img/>').attr('title','自定义').attr('alt','自定义').attr('iv','custom').attr('src','../images/channels/ch-logo04.jpg');
			chlgp1f_lP.append(customImg);
			count++;
		}//全选则全部不显示
		if(count==6){
			chlgp1f_lP.empty();
		}else{
			textIndent = 12+count*32
		}
	}
	var barJObj = contentJObj.find('div[class="tooltitlecn"]');
	var hiddenInputJObj = contentJObj.find('input[type="hidden"]');
	var textareaJObj = contentJObj.find('textarea');
	textareaJObj.css('text-indent',textIndent+'px');
	if(!type || type == 'text'){
		tLp5Click();
		textareaJObj.val(answer);
	}else if(type=='imgtxtmsg'){
		textareaJObj.val(answer);
		toggleBar(barJObj,1);
		hiddenInputJObj.val('imgtxtmsg');
		loadAnswerByType(answer,type,textareaJObj)
	}else if(type=='imgmsg'){
		textareaJObj.val(answer);
		toggleBar(barJObj,2);
		hiddenInputJObj.val('imgmsg');
		loadAnswerByType(answer,type,textareaJObj)
	}else if(type=='musicmsg'){
		textareaJObj.val(answer);
		toggleBar(barJObj,4);
		hiddenInputJObj.val('musicmsg');
		loadAnswerByType(answer,type,textareaJObj)
	}else if(type=='videomsg'){
		textareaJObj.val(answer);
		toggleBar(barJObj,3);
		hiddenInputJObj.val('videomsg');
		loadAnswerByType(answer,type,textareaJObj)
	}else if(type=='service'){
		textareaJObj.val(answer);
		toggleBar(barJObj,5);
		hiddenInputJObj.val('service');
		loadAnswerByAppType(answer,appName,appArgs,textareaJObj);
	}
}

function toggleBar(barJObj,index){
	barJObj.find('a[class="TLp5Click"]').attr('class','TLp5');
	var barAJObjs = barJObj.find('a');
	for(var i=0;i<barAJObjs.length;i++){
		if(index == i){
			$(barAJObjs[i]).attr('class',$(barAJObjs[i]).attr('class')+'Click');
			break;
		}
	}
}

Template = {
	html : function() {
	    var linesFun = function(){
			/*
			<div style="position:relative;margin-top: 10px;" class="kb-cnrighttwo ov_fl">
				<div style="display:none;" class="tooltitle1 f_l ov_fl">
					<div class="tooltitle2">
						<div class="tooltitle3">
							<div class="tooltitlecn">
								<p><a alt="文本" title="文本" onclick="tLp5Click();return false;" class="TLp5Click" href="#"></a></p>
								<p><a alt="图文" title="图文" onclick="tLp1Click();return false;" class="TLp1" href="#"></a></p>
								<p><a alt="视频" title="视频" onclick="tLp2Click();return false;" class="TLp2" href="#"></a></p>
								<p><a alt="音乐" title="音乐" onclick="tLp3Click();return false;" class="TLp3" href="#"></a></p>
								<p><a alt="图片" title="图片" onclick="tLp4Click();return false;" class="TLp4" href="#"></a></p>
								<p><a alt="APP" title="APP" onclick="tLp6Click();return false;" class="TLp6" href="#"></a></p>
							</div>
						</div>
					</div>
				</div>
				<div class="channels f_l">
					<input type="hidden" value="">
					<div class="channel-logo ov_fl">
						<p class="chlg-p1 f_l"></p>
						<p class="chlg-p2 f_l"><a href="#" onclick="showDimSelectDiv(this);return false;"></a></p>
					</div>
					<textarea onclick="answerDivClick(this);" style="position: absolute; overflow-y: scroll; outline: 0px none; line-height: 22px;" class="answer-textarea-class"></textarea>
				</div>
				<!--<a style='cursor:pointer;' onclick='removeContain("default",this);'> 删除</a>-->
			</div>
			*/
		}
		var lines = new String(linesFun);
 		return lines.substring(lines.indexOf("/*") + 2,
                lines.lastIndexOf("*/"));
	} , 
	html_dim : function() {
	    var linesFun = function(){
			/*
			<div id="selectDimDiv" class="channel-fc" style="display:none;">
				<p class="channel-fctitp1 f_l"></p>
				<div class="channel-fccn1 f_l">
					<div class="channel-fctitle">
						<p class="channel-fcp1 f_l"><input id="platformSelectAll" name="" type="checkbox" value="" onClick="toggleAllCheckBox(this)"></p>
						<p class="channel-fcp2 f_l">全选</p>
						<p class="channel-fcp4 f_r"><a href="#" onclick="closeDimSelectDiv();return false;"></a></p>
						<p class="channel-fcp3 f_r"><a href="#" onclick="finishDimSelectDiv();return false;"></a></p>
					</div>
					<ul class="channel-fcul">
						
						<li>
							<p class="channel-fcp1 f_l"><input id="weixin" type="checkbox"></p>
							<p class="channel-iconp1 f_l"><img src="../images/channels/ch-logo02.jpg" style="cursor:pointer;" onclick="dim_img_click(this);"></p>
							<p class="channel-iconp2 f_l" style="cursor:pointer;" onclick="dim_title_click(this);">微信</p>
						</li>
						<li>
							<p class="channel-fcp1 f_l"><input id="tqq" type="checkbox"></p>
							<p class="channel-iconp1 f_l"><img src="../images/channels/ch-logo03.jpg" style="cursor:pointer;" onclick="dim_img_click(this);"></p>
							<p class="channel-iconp2 f_l" style="cursor:pointer;" onclick="dim_title_click(this);">腾讯微博</p>
						</li>
						
						<li>
							<p class="channel-fcp1 f_l"><input id="yixin" type="checkbox"></p>
							<p class="channel-iconp1 f_l"><img src="../images/channels/ch-logo05.png" style="cursor:pointer;" onclick="dim_img_click(this);"></p>
							<p class="channel-iconp2 f_l" style="cursor:pointer;" onclick="dim_title_click(this);">易信</p>
						</li>
						
						<li>
							<p class="channel-fcp1 f_l"><input id="ios" type="checkbox"></p>
							<p class="channel-iconp1 f_l"><img src="../images/channels/ios_img.png" style="cursor:pointer;" onclick="dim_img_click(this);"></p>
							<p class="channel-iconp2 f_l" style="cursor:pointer;" onclick="dim_title_click(this);">iOS</p>
						</li>
						<li>
							<p class="channel-fcp1 f_l"><input id="android" type="checkbox"></p>
							<p class="channel-iconp1 f_l"><img src="../images/channels/android_img.png" style="cursor:pointer;" onclick="dim_img_click(this);"></p>
							<p class="channel-iconp2 f_l" style="cursor:pointer;" onclick="dim_title_click(this);">安卓</p>
						</li>
						
						<li>
							<p class="channel-fcp1 f_l"><input id="custom" type="checkbox"></p>
							<p class="channel-iconp1 f_l"><img src="../images/channels/ch-logo04.jpg" style="cursor:pointer;" onclick="dim_img_click(this);"></p>
							<p class="channel-iconp2 f_l" style="cursor:pointer;" onclick="dim_title_click(this);">自定义</p>
						</li>
					</ul>
				</div>
			</div>
			*/
		}
		var lines = new String(linesFun);
 		return lines.substring(lines.indexOf("/*") + 2,
                lines.lastIndexOf("*/"));
	} ,
	html_app : function() {
	    var linesFun = function(){
			/*
			<div class="tanceng-content1 CM-dsp ov_fl" id="userAppSelDiv" style="display:none;">
				<div class="tanceng-title ov_fl f_l">
			    	<ul>
			        	<li><a id="tab0" >全部</a></li>
			            <li><a id="tab1" href="#" style="color:#1280D9;"></a></li>
			            <li><a id="tab2" href="#" style="color:#1280D9;"></a></li>
			            <li><a id="tab3" href="#" style="color:#1280D9;"></a></li>
			            <li><a id="tab4" href="#" style="color:#1280D9;"></a></li>
			            <li><a id="tab5" href="#" style="color:#1280D9;"></a></li>
			        </ul>
			        <p><a href="#" onclick="closeUserAppSelDiv();return false;"></a></p>
			    </div>
			    
			     <div class="tanceng-box f_l ov_fl" id="content0">
			    	<ul class="Apply-ul01" style="width:699px;">
			        </ul>
			        <div style="float: right;">
			        	<div class="flip_over t-r" id="pgDiv_0">
							<a id="prePg_0" href="#">上一页</a>
							　<font id="pgc_0"></font>　
							<a id="nextPg_0" href="#">下一页</a>
						</div>
			        </div>
			    </div>
			    <div class="tanceng-box f_l ov_fl" id="content1" style="display:none;">
			    	<ul class="Apply-ul01" style="width:699px;">
			        </ul>
			        <div style="float: right;">
			        	<div class="flip_over t-r" id="pgDiv_1">
							<a id="prePg_1" href="#">上一页</a>
							　<font id="pgc_1"></font>　
							<a id="nextPg_1" href="#">下一页</a>
						</div>
			        </div>
			    </div>
			    <div class="tanceng-box f_l ov_fl" id="content2" style="display:none;">
			    	<ul class="Apply-ul01" style="width:699px;">
			        </ul>
			        <div style="float: right;">
			        	<div class="flip_over t-r" id="pgDiv_2">
							<a id="prePg_2" href="#">上一页</a>
							　<font id="pgc_2"></font>　
							<a id="nextPg_2" href="#">下一页</a>
						</div>
			        </div>
			    </div>
			    <div class="tanceng-box f_l ov_fl" id="content3" style="display:none;">
			    	<ul class="Apply-ul01" style="width:699px;">
			        </ul>
			        <div style="float: right;">
			        	<div class="flip_over t-r" id="pgDiv_3">
							<a id="prePg_3" href="#">上一页</a>
							　<font id="pgc_3"></font>　
							<a id="nextPg_3" href="#">下一页</a>
						</div>
			        </div>
			    </div>
			    <div class="tanceng-box f_l ov_fl" id="content4" style="display:none;">
			    	<ul class="Apply-ul01" style="width:699px;">
			        </ul>
			        <div style="float: right;">
			        	<div class="flip_over t-r" id="pgDiv_4">
							<a id="prePg_4" href="#">上一页</a>
							　<font id="pgc_4"></font>　
							<a id="nextPg_4" href="#">下一页</a>
						</div>
			        </div>
			    </div>
			    <div class="tanceng-box f_l ov_fl" id="content5" style="display:none;">
			    	<ul class="Apply-ul01" style="width:699px;">
			        </ul>
			        <div style="float: right;">
			        	<div class="flip_over t-r" id="pgDiv_5">
							<a id="prePg_5" href="#">上一页</a>
							　<font id="pgc_5"></font>　
							<a id="nextPg_5" href="#">下一页</a>
						</div>
			        </div>
			    </div>
			    <div class="tanceng-bottom f_l"></div>
			</div>
			*/
		}
		var lines = new String(linesFun);
 		return lines.substring(lines.indexOf("/*") + 2,
                lines.lastIndexOf("*/"));
	}
}

//显示选择维度浮层
function showDimSelectDiv(selectBtn){
	//117
	//service,text,img...
	var selTypeJObj = $(selectBtn).parent().parent().parent().children('input[type="hidden"]');
	if(selTypeJObj.val() == 'service'){
		var appId = $(selectBtn).parent().parent().parent().children('textarea').val();
//		console.log('appId',appId);
		var app = appMap[appId];
		var platforms_supportArray = app['platforms'];
//		console.log('platforms_supportArray',platforms_supportArray);
		resetDimSelectDiv_disabled(platforms_supportArray);
	}else{
		resetDimSelectDiv_disabled();
	}
	//117
	
	var offsetObj = $(selectBtn).offset();
	selectDimDivJObj[0].style.top = offsetObj.top+"px";
	selectDimDivJObj[0].style.left = offsetObj.left+"px";
	selectDimDivJObj.find('input[type=checkbox]').attr("checked", false);
	getSelectDim($(selectBtn),selectDimDivJObj);
	selectDimDivJObj.show();
	currentShowDimSelectBtnDomObj = selectBtn;
}

//117
function resetDimSelectDiv_disabled(platforms_supportArray){
	if(platforms_supportArray){
		$('#selectDimDiv').find('input[type=checkbox]').attr("disabled", "disabled");
		for(var i=0;i<platforms_supportArray.length;i++){
			$('#'+platforms_supportArray[i]).removeAttr("disabled");
		}
		$('#platformSelectAll').removeAttr("disabled");
	}else{
		$('#selectDimDiv').find('input[type=checkbox]').removeAttr("disabled");
	}
}
//117

function getSelectDim(aBtnJObj,selectDimDivJObj){
	var imgJObjs_sel = aBtnJObj.parent().prev().children('img');
	if(imgJObjs_sel.length == 0 ){
		//全选
		selectDimDivJObj.find('input[type=checkbox]').attr("checked", true);
	}else{
		imgJObjs_sel.each(function(){
			var title = $(this).attr('title');
			if( title == '微信'){
				selectDimDivJObj.find('#weixin').attr("checked", true);
			}else if( title == '腾讯微博'){
				selectDimDivJObj.find('#tqq').attr("checked", true);
			}else if( title == '自定义'){
				selectDimDivJObj.find('#custom').attr("checked", true);
			}else if( title == '易信'){
				selectDimDivJObj.find('#yixin').attr("checked", true);
			}else if( title == 'ios'){
				selectDimDivJObj.find('#ios').attr("checked", true);
			}else if( title == '安卓'){
				selectDimDivJObj.find('#android').attr("checked", true);
			}
		});
	}
}

function dim_img_click(imgDomObj){
	var checkBoxJObj = $(imgDomObj).parent().parent().find('input');
	checkBoxJObj.attr("checked", !checkBoxJObj.attr("checked"));
}

function dim_title_click(pDomObj){
	var checkBoxJObj = $(pDomObj).parent().find('input');
	checkBoxJObj.attr("checked", !checkBoxJObj.attr("checked"));
}

//取消选择维度浮层
function closeDimSelectDiv(){
	selectDimDivJObj.hide();
}

//确认选择维度浮层
function finishDimSelectDiv(){
	var currentShowDimSelectBtnJObj = $(currentShowDimSelectBtnDomObj);
	var currentchlgp1f_lJObj = currentShowDimSelectBtnJObj.parent().parent().children('p[class="chlg-p1 f_l"]');
	currentchlgp1f_lJObj.empty();
	var count = 0;
	var textIndent = 12;
	if(($('#weixin').attr('checked')==true || $('#weixin').attr('checked')=='checked') && !$('#weixin').attr('disabled')){
		var weixinImg = $('<img/>').attr('title','微信').attr('alt','微信').attr('iv','weixin').attr('src','../images/channels/ch-logo02.jpg');
		currentchlgp1f_lJObj.append(weixinImg);
		count++;
	}
	if(($('#tqq').attr('checked')==true || $('#tqq').attr('checked')=='checked') && !$('#tqq').attr('disabled')){
		var tqqImg = $('<img/>').attr('title','腾讯微博').attr('alt','腾讯微博').attr('iv','tqq').attr('src','../images/channels/ch-logo03.jpg');
		currentchlgp1f_lJObj.append(tqqImg);
		count++;
	}
	if(($('#yixin').attr('checked')==true || $('#yixin').attr('checked')=='checked')&& !$('#yixin').attr('disabled')==true){
		var yixinImg = $('<img/>').attr('title','易信').attr('alt','易信').attr('iv','yixin').attr('src','../images/channels/ch-logo05.png');
		currentchlgp1f_lJObj.append(yixinImg);
		count++;
	}
	if(($('#ios').attr('checked')==true || $('#ios').attr('checked')=='checked')&& !$('#ios').attr('disabled')==true){
		var iosImg = $('<img/>').attr('title','ios').attr('alt','ios').attr('iv','ios').attr('src','../images/channels/ios_img.png');
		currentchlgp1f_lJObj.append(iosImg);
		count++;
	}
	if(($('#android').attr('checked')==true || $('#android').attr('checked')=='checked')&& !$('#android').attr('disabled')==true){
		var androidImg = $('<img/>').attr('title','安卓').attr('alt','安卓').attr('iv','android').attr('src','../images/channels/android_img.png');
		currentchlgp1f_lJObj.append(androidImg);
		count++;
	}
	if(($('#custom').attr('checked')==true || $('#custom').attr('checked')=='checked')&& !$('#custom').attr('disabled')==true){
		var customImg = $('<img/>').attr('title','自定义').attr('alt','自定义').attr('iv','custom').attr('src','../images/channels/ch-logo04.jpg');
		currentchlgp1f_lJObj.append(customImg);
		count++;
	}
	//全选则全部不显示
	if(count==6){
		currentchlgp1f_lJObj.empty();
	}else{
		textIndent = 12+count*32
	}
	
	var ansTextareaJObj = currentShowDimSelectBtnJObj.parent().parent().parent().children('textarea');
	if(ansTextareaJObj.css('display') && ansTextareaJObj.css('display').indexOf('none')>-1){
		ansTextareaJObj.css('text-indent',textIndent+'px');
		if(ansTextareaJObj.next('div').length>0){
			$(ansTextareaJObj.next('div').children('div')[0]).css('padding-left',textIndent+'px');
		}
	}else{
		ansTextareaJObj.css('text-indent',textIndent+'px')
		//>>>IE9 PATCH,to refresh the layout
		var ov = ansTextareaJObj.val()
		ansTextareaJObj.val(ov+' ')
		ansTextareaJObj.val(ov)
	}
	selectDimDivJObj.hide();
}

function toggleAllCheckBox(allCheckBoxDomObj){
	if($(allCheckBoxDomObj).attr('checked')==true || $(allCheckBoxDomObj).attr('checked')=='checked'){
		selectDimDivJObj.find('input[type=checkbox]').attr("checked", true);
	}else{
		selectDimDivJObj.find('input[type=checkbox]').attr("checked", false);	
	}
}

var currentFoucsTextareaJObj ;

function tLp1Click(){
	WSC_ShowDialog("img-msg!list.action", "图文消息");
}

function tLp6Click(){
	showUserAppSelDiv();
}

function tLp6ClickCb(btnJObj){
	var appId = $(btnJObj).attr('appId');
	var appObj = appMap[appId];
	$(currentFoucsTextareaJObj).val(appId);
	$(currentFoucsTextareaJObj).parent().children('input[type=hidden]').val('service');
	loadAnswerByAppType(appId,appObj['name'],appObj['appArgs'],$(currentFoucsTextareaJObj));
	
	//117
	servicePlatform_check(appObj,$(currentFoucsTextareaJObj));
	//117
}

//117
function servicePlatform_check(appObj,currentFoucsTextareaJObj){
//	console.log('app_platforms'+appObj['platforms']);
	if(appObj['platforms']){
		var app_platforms = appObj['platforms'].join(',');
		var ansJObj = currentFoucsTextareaJObj.parent();
		var user_platforms = new Array();
		ansJObj.find('img').each(function (){
			user_platforms.push($(this).attr('iv'));
		});
		if(user_platforms.length==0){
			user_platforms.push('weixin');
			user_platforms.push('yixin');
			user_platforms.push('tqq');
			user_platforms.push('android');
			user_platforms.push('ios');
			user_platforms.push('custom');
		}
//		console.log('user_platforms.length',user_platforms.length);
		for(var pi=0;pi<user_platforms.length;pi++){
			if(app_platforms.indexOf(user_platforms[pi]) < 0){
				alert('请选择应该对应的平台');
				showDimSelectDiv(currentFoucsTextareaJObj.parent().find('p[class="chlg-p2 f_l"]').find('a')[0]);
				break;
			}
//			console.log('userSelectedPlt',user_platforms[pi]);
			
		}
			
	}
	
}
//117

function loadAnswerByAppType(appId,appName,appArgs,currentFoucsTextareaJObj){
	if(currentFoucsTextareaJObj.next('iframe').length > 0){
		currentFoucsTextareaJObj.next('iframe').remove();
	}
	if(currentFoucsTextareaJObj.next('div').length > 0 ){
		currentFoucsTextareaJObj.next('div').remove();
	}
	var divContentJObj =  $('<div/>').attr('style','overflow-y:scroll;border: 0 none;font-family: tahoma,Arial,Helvetica,sans-serif;font-size: 12px;height: 58px;')
		.attr('appName',appName);
	currentFoucsTextareaJObj.after(divContentJObj);
	var divJObj = $('<div/>').attr('style','border: 0 none;font-family: tahoma,Arial,Helvetica,sans-serif;font-size: 12px;height: 22px;')
		.css('padding-left',$(currentFoucsTextareaJObj).css('text-indent'));
	divJObj.html('/调用 '+appName);
	divContentJObj.append(divJObj);
	if(appArgs){
		for(var i=0;i<appArgs.length;i++){
			var appArg = appArgs[i];
			var divJObj = $('<div/>').attr('name','args').attr('argsName',appArg['name']);
			var inputJObj = $('<input/>').attr('type','text').css('width','180px')
				.attr('style','border-left: 0px;border-right: 0px;border-top: 0ex;border-bottom: 1px dashed #CCCCCC;width: 230px;').attr('maxLength','20');
			if(appArg['value'] && appArg['value'].length > 0){
				inputJObj.val(appArg['value']);
			}else if(appArg['defaultVal'] && appArg['defaultVal'].length > 0){
				inputJObj.val(appArg['defaultVal']);
			}
			var argTitle = appArg['name'];
			var read_app = appMap[appId];
			if(read_app && read_app['appArgs']){
				argTitle = read_app['appArgs'][i]['title']?read_app['appArgs'][i]['title']:appArg['name'];
			}
			divJObj.append(argTitle+"=").append(inputJObj);
			if(appArg['optional'] && appArg['optional'] == '1'){
				divJObj.append('<font color="red">*</font>');
				divJObj.attr('optional','1');
			}
			divContentJObj.append(divJObj);
		}
	}
	currentFoucsTextareaJObj.hide();
//	answerDivClick(currentFoucsTextareaJObj);
	renderBar(currentFoucsTextareaJObj,'service');
	divContentJObj.bind('click',function (){
		answerDivClick(currentFoucsTextareaJObj);
		renderBar(currentFoucsTextareaJObj,'service');
	});
}

function tLp1ClickCb(imgId){
//	alert(imgId)
	$(currentFoucsTextareaJObj).val(imgId);
	$(currentFoucsTextareaJObj).parent().children('input[type=hidden]').val('imgtxtmsg');
	loadAnswerByType(imgId,'imgtxtmsg',$(currentFoucsTextareaJObj));
}

function loadAnswerByType(id,type,currentFoucsTextareaJObj){
	var url ;
	if(type=='imgtxtmsg'){
		url = "img-msg!getImgmsg.action?imgmsgId=";
	}else if(type=='imgmsg'){
		url = "imageItem.jsp?imageId=";
	}else if(type =='musicmsg'){
		url = "audioItem.jsp?audioId=";
	}else if(type =='videomsg'){
		url = "videoItem.jsp?videoId=";
	}
	if(currentFoucsTextareaJObj.next('iframe').length > 0){
		currentFoucsTextareaJObj.next('iframe').remove();
	}
	if(currentFoucsTextareaJObj.next('div').length > 0 ){
		currentFoucsTextareaJObj.next('div').remove();
	}
	var iframeJObj = $('<iframe/>').attr('src',url+id).attr('class','answer-textarea-class').attr('onclick','answerDivClick(this)');
	currentFoucsTextareaJObj.after(iframeJObj);
	currentFoucsTextareaJObj.hide();
	renderBar(currentFoucsTextareaJObj,type);
	iframeJObj.load(function(){ 
		iframeJObj[0].contentWindow.document.onclick=function(){
			answerDivClick(currentFoucsTextareaJObj);
			renderBar(currentFoucsTextareaJObj,type);
		}
    }); 
}
function tLp2Click(){
	WSC_ShowDialog("video!list.action", "视频");
}
function tLp2ClickCb(imgId){
//	alert(imgId)
	$(currentFoucsTextareaJObj).val(imgId);
	$(currentFoucsTextareaJObj).parent().children('input[type=hidden]').val('videomsg');
	loadAnswerByType(imgId,'videomsg',$(currentFoucsTextareaJObj));
}
function tLp3Click(){
	WSC_ShowDialog("audio!list.action", "语音");
}
function tLp3ClickCb(imgId){
//	alert(imgId)
	$(currentFoucsTextareaJObj).val(imgId);
	$(currentFoucsTextareaJObj).parent().children('input[type=hidden]').val('musicmsg');
	loadAnswerByType(imgId,'musicmsg',$(currentFoucsTextareaJObj));
}
function tLp4Click(){
	WSC_ShowDialog("image!list.action", "图片");
}
function tLp4ClickCb(imgId){
//	alert(imgId)
	$(currentFoucsTextareaJObj).val(imgId);
	$(currentFoucsTextareaJObj).parent().children('input[type=hidden]').val('imgmsg');
	loadAnswerByType(imgId,'imgmsg',$(currentFoucsTextareaJObj));
}
function tLp5Click(){
	$(currentFoucsTextareaJObj).parent().children('input[type=hidden]').val('');
	$(currentFoucsTextareaJObj).val('');
	$(currentFoucsTextareaJObj).next('iframe').remove();
	if($(currentFoucsTextareaJObj).next('div').length > 0 ){
		$(currentFoucsTextareaJObj).next('div').remove();
	}
	$(currentFoucsTextareaJObj).show();
	renderBar(currentFoucsTextareaJObj,'text');
}

function renderBar(textareaDomObj,type){
	var textareaJObj = $(textareaDomObj);
	if(type){
		var typeBar = textareaJObj.parent().parent().children('div[class="tooltitle1 f_l ov_fl"]');
		if(type=='imgtxtmsg'){
			typeBar.find('a[class="TLp1"]').attr('class','TLp1Click');
			typeBar.find('a[class="TLp2Click"]').attr('class','TLp2');
			typeBar.find('a[class="TLp3Click"]').attr('class','TLp3');
			typeBar.find('a[class="TLp4Click"]').attr('class','TLp4');
			typeBar.find('a[class="TLp5Click"]').attr('class','TLp5');
			typeBar.find('a[class="TLp6Click"]').attr('class','TLp6');
		}else if(type=='imgmsg'){
			typeBar.find('a[class="TLp1Click"]').attr('class','TLp1');
			typeBar.find('a[class="TLp2Click"]').attr('class','TLp2');
			typeBar.find('a[class="TLp3Click"]').attr('class','TLp3');
			typeBar.find('a[class="TLp4"]').attr('class','TLp4Click');
			typeBar.find('a[class="TLp5Click"]').attr('class','TLp5');
			typeBar.find('a[class="TLp6Click"]').attr('class','TLp6');
		}else if(type =='musicmsg'){
			typeBar.find('a[class="TLp1Click"]').attr('class','TLp1');
			typeBar.find('a[class="TLp2Click"]').attr('class','TLp2');
			typeBar.find('a[class="TLp3"]').attr('class','TLp3Click');
			typeBar.find('a[class="TLp4Click"]').attr('class','TLp4');
			typeBar.find('a[class="TLp5Click"]').attr('class','TLp5');
			typeBar.find('a[class="TLp6Click"]').attr('class','TLp6');
		}else if(type =='videomsg'){
			typeBar.find('a[class="TLp1Click"]').attr('class','TLp1');
			typeBar.find('a[class="TLp2"]').attr('class','TLp2Click');
			typeBar.find('a[class="TLp3Click"]').attr('class','TLp3');
			typeBar.find('a[class="TLp4Click"]').attr('class','TLp4');
			typeBar.find('a[class="TLp5Click"]').attr('class','TLp5');
			typeBar.find('a[class="TLp6Click"]').attr('class','TLp6');
		}else if(type =='text'){
			typeBar.find('a[class="TLp1Click"]').attr('class','TLp1');
			typeBar.find('a[class="TLp2Click"]').attr('class','TLp2');
			typeBar.find('a[class="TLp3Click"]').attr('class','TLp3');
			typeBar.find('a[class="TLp4Click"]').attr('class','TLp4');
			typeBar.find('a[class="TLp5"]').attr('class','TLp5Click');
			typeBar.find('a[class="TLp6Click"]').attr('class','TLp6');
		}else if(type =='service'){
			typeBar.find('a[class="TLp1Click"]').attr('class','TLp1');
			typeBar.find('a[class="TLp2Click"]').attr('class','TLp2');
			typeBar.find('a[class="TLp3Click"]').attr('class','TLp3');
			typeBar.find('a[class="TLp4Click"]').attr('class','TLp4');
			typeBar.find('a[class="TLp5Click"]').attr('class','TLp5');
			typeBar.find('a[class="TLp6"]').attr('class','TLp6Click');
		}
	}
}

function answerDivClick(textareaDomObj){
	var textareaJObj = $(textareaDomObj);
	if (currentFoucsTextareaJObj != null && currentFoucsTextareaJObj == textareaDomObj)
		return;
	//隐藏
	if(currentFoucsTextareaJObj){
//		$(currentFoucsTextareaJObj).parent().css('border','1px solid #CCCCCC');
		$(currentFoucsTextareaJObj).parent().parent().children('div[class="tooltitle1 f_l ov_fl"]').hide();
	}
	//显示
//	textareaJObj.parent().css('border','1px solid #1D9AFF');
	textareaJObj.parent().parent().children('div[class="tooltitle1 f_l ov_fl"]').show();
	currentFoucsTextareaJObj = textareaDomObj;
}

//app
var classification_loaded_flag = false;

function classification_load(){
	if(!classification_loaded_flag){
		classification_loaded_flag = true;
		categorys_load();
		for(var i=0;i<6;i++){
			appByCategory_load(i);
		}
	}
}

function categorys_load(){
//	$.ajax({
//		url : 'store!getAllCategorysAndCustom.do',
//		type : "POST",
//		async : false,
//		success : function(msg) {
//			var categorys = eval("("+msg+")");
//			for(var i=0;i<categorys.length;i++){
//				$('#tab'+(i+1)).html(categorys[i]["name"]);
//				$('#tab'+(i+1)).attr("cid",categorys[i]["id"]);
//			}
//		},
//		error : function(xhr, status, e) {
//			
//		}
//	});
}

function showUserAppSelDiv(){
	classification_load();
	$.blockUI({message:$(userAppSelDivJObj),css:{width:0,border:0,background:"none",left:'20%',top:'20%'}});
}

function closeUserAppSelDiv(){
	$.unblockUI();
}

var appMap = {};

function appByCategory_load(type,pn){
	var tabSelected = $('#tab'+type);
	var categoryId = tabSelected.attr('cid');
	var params = {};
	if(categoryId){
		params["categoryId"] = categoryId;
	}
	if(pn){
		params["pg.pn"] = pn;
	}
//	$.ajax({
//		url : 'store!getAppsByCategoryIdAndUid.do',
//		type : "POST",
//		async : true,
//		data : params,
//		success : function(msg) {
//			var pg = eval("("+msg+")");
////			console.log(type,pg);
//			var apps = pg.data;
//			if(apps && apps.length > 0){
//				var ulJObj = $('#content'+type).find('ul');
//				ulJObj.empty();
//				for(var i = 0;i<apps.length;i++){
//					var app = apps[i];
//					if(window.excludeApp && window.excludeApp.indexOf(app['id'])>-1){
//						continue;
//					}
//					var liJObj = $('<li/>').attr('class','liline01');
//					var iconDivJObj = $('<div/>').attr('class','Apply_icon01 f_l');
//					var aInAppDivJObj = $('<a/>').attr('href','#');
//					var imgInAInAppDivJObj = $('<img/>').attr('src',"../apppic?"+app['icon']).attr('width','60').attr('height','60');
//					aInAppDivJObj.append(imgInAInAppDivJObj);
//					iconDivJObj.append(aInAppDivJObj);
//					liJObj.append(iconDivJObj);
//					
//					var introDivJObj = $('<div/>').attr('class','Apply_intro f_l');
//					var hInIntroDivJObj = $('<h2/>');
//					var aInHInIntroDivJObj = $('<a/>').attr('href','#').html(app['name']);
//					hInIntroDivJObj.append(aInHInIntroDivJObj);
//					introDivJObj.append(hInIntroDivJObj);
//					var pInIntroDivJObj = $('<p/>').html(app['description']);
//					introDivJObj.append(pInIntroDivJObj);
//					//<p class="CM-cl04">有效期：2013年10月20日</p>
//					var p2InIntroDivJObj = $('<p/>').html('有效期：待定').attr('class','CM-cl04');
//					introDivJObj.append(p2InIntroDivJObj);
//					
//					liJObj.append(introDivJObj);
//					
//					var rightDivJObj = $('<div/>').attr('class','Apply_right f_r');
//					var pInRightDivJObj = $('<p/>');
//					var aInPInRightDivJObj = $('<a/>').css('font-size','12px').attr('class','wx-button02').attr('href','#').html('确　定')
//						.attr('appId',app['id']);
//					appMap[app['id']] = app;
//					aInPInRightDivJObj.bind('click',function(){
//						tLp6ClickCb(this);
//						$.unblockUI();
//						return false;
//					});
//					pInRightDivJObj.append(aInPInRightDivJObj);
//					rightDivJObj.append(pInRightDivJObj);
//					liJObj.append(rightDivJObj);
//					
//					ulJObj.append(liJObj);
//				}
//			}
//			//分页
//			pgControl(type,pg);
//		},
//		error : function(xhr, status, e) {
//			
//		}
//	});
}

function oneTabClick(type){
	for(var i=0;i<6;i++){
		if(i==type){
			$('#tab'+i).removeAttr('href').css('color','');
			$('#content'+i).show();
		}else{
			$('#tab'+i).attr('href','#').css('color','#1280D9');
			$('#content'+i).hide();
		}
	}
}

function tab0_click(){
	oneTabClick(0);
}

function tab1_click(){
	oneTabClick(1);
}

function tab2_click(){
	oneTabClick(2);
}

function tab3_click(){
	oneTabClick(3);
}

function tab4_click(){
	oneTabClick(4);
}

function tab5_click(){
	oneTabClick(5);
}
