var tab_cbs = {};

var addTab = function(tabId, tabName, cb, activate) {
	var tab = $('<li></li>');
	tab.attr('id', tabId);
	tab.html(tabName);
	$('#tabs').append(tab);
	tab.click(function() {
		if (!$(this).hasClass('hover'))
			activateTab($(this), cb);
	});
	if (activate)
		activateTab(tab, cb);
	tab_cbs[tabId] = cb;
};

var activateTab = function(tab, cb) {
	if (typeof tab == 'string')
		tab = $('#' + tab);
	var tabs = $('li', tab.parent());
	tabs.each(function(idx, t) {
		$(t).removeClass('hover');
	});
	tab.addClass('hover');
	$('div[id$=__tab]').hide();
	$('#' + tab.attr('id') + '__tab').show();
	if (cb && typeof cb == 'function')
		cb(tab.attr('id'));
	if (window.onContentAdjust)
		onContentAdjust($('#' + tab.attr('id') + '__tab')[0]);
};

var rebindTabs = function() {
	var tabs = $('li', $('#tabs'));
	tabs.each(function(idx, t) {
		$(t).show().unbind('click').click(function() {
			if (!$(this).hasClass('hover'))
				activateTab($(this), tab_cbs[$(this).attr('id')]);
		});
	});
};

var enableFirstTabOnly = function() {
	var tabs = $('li', $('#tabs'));
	tabs.each(function(idx, t) {
		if (idx == 0)
			activateTab($(t), tab_cbs[$(t).attr('id')]);
		else
			$(t).unbind('click').click(function() {
				showTabClickHint('您需要点击免费试用或开启应用才能查看其他内容。');
			}).hide();
	});
};

var showTabClickHint = function(hint) {
	$('span', $("#message-dialog")).html(hint);
	$("#message-dialog").dialog({
		resizable : false,
		height : 180,
		modal : true,
		buttons : {
			"确定" : function() {
				$(this).dialog("close");
			}
		}
	});
};