<%@page language="java" pageEncoding="utf-8"%>
<jsp:directive.page import="java.util.Date" />
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div style="text-align: right; padding: 10px 30px;font:12px/1.5 Helvetica,Arial,sans-serif;color:black;">
	<c:if test="${pg.totalCount > 0 }">
		<c:set value="${pg.pageNo}" var="pageNo"></c:set>
		<c:set value="${pg.pageSize}" var="pageSize"></c:set>
		<c:choose>
			<c:when test="${pageNo>1}">
				<a href="${pg.url}&pg.pn=1">首页</a>
				<a href="${pg.url}&pg.pn=${pageNo-1}">上页</a>
			</c:when>
			<c:otherwise>
				首页 上页
		</c:otherwise>
		</c:choose>

		<%-- <c:forEach var="np" items="${pg.navigatorPages}">
			<c:choose>
				<c:when test="${np==pn}">
				[${np}]
			</c:when>
				<c:otherwise>
					<a href="${pg.url}&pg.pn=${np}">${np}</a>
				</c:otherwise>
			</c:choose>
		</c:forEach> --%>

		<c:choose>
			<c:when test="${pageNo < pg.totalPageCount && pg.totalPageCount>1}">
				<a href="${pg.url}&pg.pn=${pageNo+1}">下页</a>
				<a href="${pg.url}&pg.pn=${pg.totalPageCount}">尾页</a>
			</c:when>
			<c:otherwise>
				下页 尾页
		</c:otherwise>
		</c:choose>
	共${pg.totalPageCount}页 总条数:${pg.totalCount}
</c:if>
</div>