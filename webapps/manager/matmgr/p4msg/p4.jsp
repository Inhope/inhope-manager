﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link type="text/css" rel="stylesheet" href="css/apiUser.css" />
<link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />
<link type="text/css" rel="stylesheet" href="css/material.css" />
<link type="text/css" rel="stylesheet" href="css/image-ex.css" />
<title>P4消息</title>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery.json.js"></script>
<script language="javascript">
	function viewp4(p4id) {
	 var p4url = 'p4-msg!showP4Html.action?objectId=' + p4id;
	 window.open(p4url);
 }
 function editP4(p4MsgId, p4Title) {
	 var input = document.getElementById(p4MsgId);
	 var fileName = input.value;
	 if (!fileName)
		 fileName = p4Title;
	 fileName = encodeURIComponent(fileName);
	 window.location.href = "p4msg/p4editor/p4edit.jsp?p4id=" + p4MsgId + "&p4title=" + fileName;
 }
 function update(objectId) {
	 var aNode = document.getElementById('updateName' + objectId);
	 aNode.style.display = "block";
	 var divNode = document.getElementById('filename' + objectId);
	 divNode.style.border = "1px solid #ddd";
	 var input = document.getElementById(objectId);
	 input.readOnly = false;
	 input.focus();
	 input.select();
 }
 function updateShow(objectId) {
	 var input = document.getElementById(objectId);
	 input.readOnly = true;
	 var aNode = document.getElementById('updateName' + objectId);
	 aNode.style.display = "none";
	 var divNode = document.getElementById('filename' + objectId);
	 divNode.style.border = "0px";
	 updateName(objectId);
 }
 function updateName(objectId) {
	 var input = document.getElementById(objectId);
	 input.readOnly = true;
	 var fileName = input.value;
	 if (!fileName) {
		 alert('文件名不能为空!');
		 return;
	 }
	 var aj = $.ajax({
	  url : '<c:url value="p4-msg!update.action"/>',// 跳转到 action    
	  data : {
	   objectId : objectId,
	   fileName : fileName
	  },
	  type : 'post',
	  cache : false,
	  dataType : 'json',
	  success : function(data) {
		  if (data.success == "true") {
			  alert("修改成功！");
		  } else {
			  alert(data.msg);
		  }
		  var aNode = document.getElementById('updateName' + objectId);
		  aNode.style.display = "none";
		  var divNode = document.getElementById('filename' + objectId);
		  divNode.style.border = "0px";
	  },
	  error : function() {
		  var aNode = document.getElementById('updateName' + objectId);
		  aNode.style.display = "none";
		  var divNode = document.getElementById('filename' + objectId);
		  divNode.style.border = "0px";
		  alert("异常！");
	  }
	 });
 }
 function remove(objectId) {
	 // 		$.ajax({
	 // 			url : '<c:url value="customKnowledge!queryAnswerByAnswerId.do"/>',// 跳转到 action    
	 // 			data : {
	 // 				answerId : objectId
	 // 			},
	 // 			type : 'post',
	 // 			cache : false,
	 // 			dataType : 'json',
	 // 			success : function(data) {
	 // 				if (data.exist == "true") {
	 // 					alert('当前素材有知识点关联，禁止删除！');
	 // 				} else if (data.exist == "false") {
	 if (window.confirm('你确定要删除此素材？')) {
		 var aj = $.ajax({
		  url : '<c:url value="p4-msg!remove.action"/>',// 跳转到 action    
		  data : {
			  objectId : objectId
		  },
		  type : 'post',
		  cache : false,
		  dataType : 'json',
		  success : function(data) {
			  if (data.success == "true") {
				  document.getElementById('divCenterP4').style.display = 'block';
				  var tmid = window.setTimeout(function() {
					  document.getElementById('divCenterP4').style.display = 'none';
					  window.location.reload();
				  }, 1000);
				  // reloadData();
			  } else {
				  alert(data.msg);
			  }
		  },
		  error : function() {
			  alert("异常！");
		  }
		 });
	 }
	 // 				}
	 // 			},
	 // 			error : function() {
	 // 				alert("查询是否关联知识点发生异常！");
	 // 			}
	 // 		});
 }
function searchName() {
      var nameContent = $("#searchInputName").val();
      if(nameContent){
         if('请输入文件名查询'==nameContent){
            alert('提示：请输入你要查询的名称');
         }else
         window.location.href = 'p4-msg!list.action?type=edit&name=' + nameContent;
      }else{
         alert('提示：查询名称不能为空');
         return;
      }
}
</script>
</head>
<body style="position: relative;">
	<div>
		<div class="con">
			<div style="width: 100%; float: left; margin-left: 10px;">
				<div style="width: 100%; margin: 10px 0 5px 0;">
					<div style="height: 29px;">
						<div class="left">
							<img src="images/title_left.gif" style="width: 9px;" />
						</div>
						<div class="title_mid left" style="width: 98%;">P4</div>
						<div class="left">
							<img src="images/title_right.gif" style="width: 8px;" />
						</div>
					</div>
					<iframe style="display: none" src="about:blank"
						name="imgFileupload"></iframe>
					<div class="right_con" style="width:99.4%;">
<div class="scgl_title_search">
					<div style="padding-left:10px;">
							<div class="search">
							  <div class="search_suo">
							    <input name="text" type="text" id="searchInputName" style="color: rgb(102, 102, 102);" onfocus="if(value =='请输入文件名查询'){value ='';}" onblur="if (value ==''){value='请输入文件名查询';}" onkeypress="if(event.keyCode==13) {searchName();}" value="请输入文件名查询">
						      <span onclick="searchName();"><a href="javascript:void(0)" id="" onclick="searchName();"></a> </span> </div>
							</div>
						</div>	
							<div class="scgl_title_con">
									
							</div>
						</div>
						<div id="divCenterP4"
							style="position: absolute; z-index: 100; display: none; background-color: #A7D3F1; width: 100px;height:52px; text-align: center; left: 35%; top: 110px;">
							<p
								style="font-weight: bold; font-size: 15px; color: #fff;margin-top:15px">
								删除成功</p>
						</div>
						<!-- 图片水印start -->
						<!-- 图片水印end -->
						<div>


							<div style="padding-left: 10px;">
							
<div id="materialImgFile" style="float:left;cursor:pointer;">
									<input type="button" value="" onclick="window.location.href='p4msg/p4editor/p4edit.jsp'" style="cursor:pointer;background:url(images/addSubmit.jpg) no-repeat;width:70px;height:26px;line-height:26px;color:white;border:0">
								</div>



								<%@include file="../nav-templatePage.jsp"%>
								<div style="clear:both;"></div>
								<div class="material_content" style="padding-left:10px">
									<div class="list_title">
										<div style="margin:0 auto;width:80%;">
											<div class="left title msg">P4名称</div>
                                            <div class="right title opt" style="width:200px;">操作</div>
										</div>
									</div>
									<div>
										<ul class="list_content">
											<c:forEach items="${pg.result}" var="p4Domain" varStatus="s">
												<li class="list_item">
													<div style="margin:0 auto;width:80%;">
														<div class="left file">
															<div class="file_name_area" id="filename${p4Domain.id}">
																<input type="text" id="${p4Domain.id}" readonly="true"
																	class="file_name" value="${p4Domain.name}"
																	maxlength="20" style="float:left;border:0"
																	onkeypress="if(event.keyCode==13) {updateName('${p4Domain.id}');}">
																</input> <a href="javascript:updateName('${p4Domain.id}');"
																	style="float:right;display:none"
																	id="updateName${p4Domain.id}"><img
																	src="images/submitTip.jpg" width="29" height="30">
																</a>
															</div>
														</div>
														<div class="opt oper right" style="width:200px;">
															<a style="margin:0 6px" title="预览"
																href="javascript:viewp4('${p4Domain.id}')">预览</a> <a
																style="margin:0 6px;" title="编辑"
																href="javascript:editP4('${p4Domain.id}','${p4Domain.name}')">编辑</a>
															<a style="margin:0 6px" title="重命名"
																href="javascript:update('${p4Domain.id}')">重命名</a><a
																style="margin:0 6px" title="删除"
																href="javascript:remove('${p4Domain.id}')">删除</a>

														</div>
														<div class="clr"></div>
													</div></li>
											</c:forEach>
										</ul>
									</div>
								</div>
							</div>
							<%@include file="../nav-templatePage.jsp"%>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>