<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/select.css" type="text/css" />
<script type="text/javascript" src="js/adjust.js"></script>
<title>图片–iBotCloud小i机器人智能云服务平台</title>
<script>
	selectClick = function(p4Id, name, isClick) {
		document.getElementById('selectedVal').value = p4Id;
		if (isClick)
			parent.answerRendered({
				id : p4Id,
				type : 'p4msg',
				title : name
			});
	};
</script>
</head>
<body>
	<div id="mainDiv" class="dialog">
		<div class="dialog_content">
			<div class="dialog_list">
				<input type="hidden" id="selectedVal" /> <input type="hidden"
					id="type" value="p4msg" />
				<!-- 				<div class="page_nav"> -->
				<!-- 					<%@include file="../nav-templatePage.jsp"%> -->
				<!-- 				</div> -->
				<!----------------------循环列表内容------------------------->
				<div class="list_ul">
					<ul>
						<c:forEach items="${pg.result}" var="p4Domain" varStatus="s">
							<li class="float-p">
								<div class="left checkbox">
									<c:choose>
										<c:when test="${p4Domain.id==param.materialId}">
											<input type="radio" value="${p4Domain.id}" id="${p4Domain.id}" name="file"
												checked="checked" onclick="selectClick(this.value,'${p4Domain.name}',true)">
												<script>
													selectClick('${p4Domain.id}','${p4Domain.name}', false);
												</script>
										</c:when>
										<c:otherwise>
											<input type="radio" value="${p4Domain.id}" name="file"
												onclick="selectClick(this.value,'${p4Domain.name}',true)">
										</c:otherwise>
									</c:choose>
								</div>
								<div class="left file_infor">
									<div class="left">
										<div class="fileName">${p4Domain.name}</div>
									</div>
								</div>
							</li>
						</c:forEach>
					</ul>
				</div>
				<div class="page_nav"><%@include
						file="../nav-templatePage.jsp"%></div>
			</div>
		</div>
	</div>
</body>
</html>