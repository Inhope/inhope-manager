
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="com.eastrobot.commonsapi.domain.FileDescription"%>
<%@page import="com.eastrobot.commonsapi.RemoteFileService"%>
<%@page import="com.incesoft.xmas.kbmgr.util.FileServiceImpl"%><%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*,java.io.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<script>
function funCallback(funcNum,fileUrl){  

    var parentWindow = ( window.parent == window ) ? window.opener : window.parent;  

    parentWindow.CKEDITOR.tools.callFunction(funcNum, fileUrl);  

    window.close();  

}  

</script>
<%

/**
 * KindEditor JSP
 *
 * 本JSP程序是演示程序，建议不要直接在实际项目中使用。
 * 如果您确定直接使用本程序，使用之前请仔细确认相关安全设置。
 *
 */
  String callback = request.getParameter("CKEditorFuncNum");  
//根目录URL，可以指定绝对路径，比如 http://www.yoursite.com/attached/
String rootUrl  = request.getContextPath() + "/matmgr/p4-msg!loadImage.action?imageId=";
//图片扩展名
String[] fileTypes = new String[]{"gif", "jpg", "jpeg", "png", "bmp"};

//根据path参数，设置各路径和URL
String path = request.getParameter("path") != null ? request.getParameter("path") : "";

//排序形式，name or size or type
String order = request.getParameter("order") != null ? request.getParameter("order").toLowerCase() : "name";

//不允许使用..移动到上一级目录
if (path.indexOf("..") >= 0) {
	out.println("Access is not allowed.");
	return;
}
//最后一个字符不是/
if (!"".equals(path) && !path.endsWith("/")) {
	out.println("Parameter is not valid.");
	return;
}
//遍历目录取的文件信息
RemoteFileService remoteFileService = WebApplicationContextUtils.getWebApplicationContext(application).getBeansOfType(RemoteFileService.class).values().iterator().next();
FileDescription[] files =  remoteFileService.listFileDescriptions("/materialData/p4Msg/images");
List<Hashtable> fileList = new ArrayList<Hashtable>();
if(files != null) {
	for (FileDescription file : files) {
		Hashtable<String, Object> hash = new Hashtable<String, Object>();
		String fileName = file.getPath().replaceAll(".*/", "");
		if(!file.isDirectory()){
			String fileExt = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
			hash.put("is_dir", false);
			hash.put("has_file", false);
			hash.put("filesize", file.getLength());
			hash.put("is_photo", Arrays.<String>asList(fileTypes).contains(fileExt));
			hash.put("filetype", fileExt);
			String src = rootUrl+ fileName;  
            out.println("<img width='110px' height='70px' src='" + src + "' alt='" + fileName + "' onclick=\"funCallback("+callback+",'"+ src +"')\">");  
		}
		hash.put("filename", fileName);
		//hash.put("datetime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(file.lastModified()));
		fileList.add(hash);
	}
}

if ("size".equals(order)) {
	Collections.sort(fileList, new SizeComparator());
} else if ("type".equals(order)) {
	Collections.sort(fileList, new TypeComparator());
} else {
	Collections.sort(fileList, new NameComparator());
}
%>
<%!
public class NameComparator implements Comparator {
	public int compare(Object a, Object b) {
		Hashtable hashA = (Hashtable)a;
		Hashtable hashB = (Hashtable)b;
		if (((Boolean)hashA.get("is_dir")) && !((Boolean)hashB.get("is_dir"))) {
			return -1;
		} else if (!((Boolean)hashA.get("is_dir")) && ((Boolean)hashB.get("is_dir"))) {
			return 1;
		} else {
			return ((String)hashA.get("filename")).compareTo((String)hashB.get("filename"));
		}
	}
}
public class SizeComparator implements Comparator {
	public int compare(Object a, Object b) {
		Hashtable hashA = (Hashtable)a;
		Hashtable hashB = (Hashtable)b;
		if (((Boolean)hashA.get("is_dir")) && !((Boolean)hashB.get("is_dir"))) {
			return -1;
		} else if (!((Boolean)hashA.get("is_dir")) && ((Boolean)hashB.get("is_dir"))) {
			return 1;
		} else {
			if (((Long)hashA.get("filesize")) > ((Long)hashB.get("filesize"))) {
				return 1;
			} else if (((Long)hashA.get("filesize")) < ((Long)hashB.get("filesize"))) {
				return -1;
			} else {
				return 0;
			}
		}
	}
}
public class TypeComparator implements Comparator {
	public int compare(Object a, Object b) {
		Hashtable hashA = (Hashtable)a;
		Hashtable hashB = (Hashtable)b;
		if (((Boolean)hashA.get("is_dir")) && !((Boolean)hashB.get("is_dir"))) {
			return -1;
		} else if (!((Boolean)hashA.get("is_dir")) && ((Boolean)hashB.get("is_dir"))) {
			return 1;
		} else {
			return ((String)hashA.get("filetype")).compareTo((String)hashB.get("filetype"));
		}
	}
}
%>