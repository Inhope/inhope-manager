<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page
	import="com.eastrobot.commonsapi.ibotcluster.ClusterConfigServiceHelper"%>
<%@page
	import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="com.eastrobot.commonsapi.domain.FileDescription"%>
<%@page import="com.eastrobot.commonsapi.domain.RemoteFile"%>
<%@page import="com.eastrobot.commonsapi.RemoteFileService"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
request.setCharacterEncoding("UTF-8");
//当前P4设置信息
String p4id = request.getParameter("p4id") != null ? request.getParameter("p4id") : "";
String p4content = "";
String p4Title = request.getParameter("p4title") != null ? request.getParameter("p4title") : "";
if(StringUtils.isNotBlank(p4id)){
	RemoteFileService remoteFileService = WebApplicationContextUtils.getWebApplicationContext(application).getBeansOfType(RemoteFileService.class).values().iterator().next();
	//查询对应的P4设置信息
	RemoteFile rf = remoteFileService.getFile("materialData/p4Msg/"+p4id+"/index.html");
	if (rf != null){
		p4content = new String(rf.getData(),"utf-8");
	}
	if (p4content!=null)
		p4content = p4content.replaceAll("(?s)<html>(?:.+?)<body(?:.*?)>(.+)</body></html>","$1").replaceAll("(?si)<p>\\s*<meta (?:.+?)>\\s*</p>","");
	p4content = p4content.replaceAll("\\.\\./images/", "/manager/matmgr/p4-msg!loadImage.action?imageId=");
}
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!--用户自定义CSS引入-->
<style type="text/css">
* {
	font-family: Tahoma, Verdana, Georgia;
	font-size: 12px;
}
</style>
<script type="text/javascript" src="../../../commons/ckeditor4/ckeditor.js"></script>
<script type="text/javascript">
function check() {
	CKEDITOR.instances.p4content.updateElement();
	var p4title = document.getElementById("p4title").value;
	if(!p4title) {
	    alert("p4标题不能为空！");
		document.getElementById("p4title").focus();
		return false;
	}
	var pcontent = document.getElementById("p4content").value;
	if(!pcontent) {
		alert("p4设计内容不能为空！");
		document.getElementById("p4content").focus();
		return false;
	}
	return true;
}
function goBack(){
      if(confirm("确定要离开P4编辑页面吗？"))
         location.replace("../../p4-msg!list.action?type=edit");
      else
         return false;
  }
</script>
</head>
<body style="overflow:hidden;margin: 0">
	<table width="100%" height="100%" cellpadding="0" cellspacing="0">
		<form name="designP4" method="post" action="../../p4-msg!saveP4.action"
			onsubmit="return check();" style="margin-bottom:0;">
			<input type="hidden" name="p4id" value="<%=p4id%>" />
			<tr>
				<td align="center">
					<div style="width:100%;height:100%">
						<table cellpadding="0" cellspacing="0" width="100%" height="100%"
							border="0" align="center">
							<colgroup>
								<col style="width: 50px;">
								<col style="">
							</colgroup>
							<tr>
								<td style="height:25px;"><a href="javascript:goBack();" style="font-size:14px;font-weight: bold;"><--返回列表</a></td>
							</tr>
							<tr>
								<td><div style="height:2px;border-bottom:1px #ccc solid;margin-bottom:2px;"></div>
								</td>
							</tr>
							<tr>
								<td><span style="font-size: 14px;font-weight:bold;">P4标题：</span><input
									type="text"
									style="height:30px;width:250px;border:1px solid #E7CBA0;font-size: 14px;"
									id="p4title" name="p4title" value=""></td>
							</tr>
							<tr>
								<td><div
										style="height:2px;"></div>
								</td>
							</tr>
							<tr>
								<td colspan="2" width="100%" height="100%"><textarea
										id="p4content" name="p4content"
										style="width: 100%; height:100%;visibility:hidden;"><%=htmlspecialchars(p4content)%></textarea>
									<script>
						CKEDITOR.replace('p4content',
						{
							height:0,width:'100%',
							toolbar:
							[
							    { name: 'document',    items : [ 'Source','-','NewPage','DocProps','-','Templates' ] },
							    { name: 'clipboard',   items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
							    { name: 'editing',     items : [ 'Find','Replace','-','SelectAll'] },
							    '/',
							    { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
							    { name: 'paragraph',   items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
							    { name: 'links',       items : [ 'Link','Unlink','Anchor' ] },
							    { name: 'insert',      items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar'] },
							    '/',
							    { name: 'styles',      items : [ 'Format','Font','FontSize' ] },
							    { name: 'colors',      items : [ 'TextColor','BGColor' ] }   
							],
							filebrowserBrowseUrl : '<%=request.getContextPath()+(ClusterConfigServiceHelper.getAppURI(request)!=null?"/app/"+ClusterConfigServiceHelper.getAppURI(request):"")%>/matmgr/p4msg/p4editor/jsp/file_p4data_json.jsp?p4id=<%=p4id%>',
					        filebrowserUploadUrl : '<%=request.getContextPath()+(ClusterConfigServiceHelper.getAppURI(request)!=null?"/app/"+ClusterConfigServiceHelper.getAppURI(request):"")%>/matmgr/p4msg/p4editor/jsp/upload_p4data_json.jsp?p4id=<%=p4id%>'
						});
						CKEDITOR.on( 'instanceReady', function( ev )
						{
							var editor = ev.editor;
							var editorContainer = document.getElementById('p4content').parentNode;
							var resizeFn = function(){
								editor.resize('100%',0);
								editor.resize('100%',editorContainer.offsetHeight);
							};
							if (editorContainer.addEventListener)
							 window.addEventListener('resize',resizeFn,false);
							else
							 window.attachEvent('onresize',resizeFn);
							resizeFn();
							editor.focus();
						});
						</script>
								</td>
							</tr>
							<tr>
						      <td align="center" colspan="2">
						       <input style="width:80px;height:30px;font-size:13px;font-weight:bold;" type="submit" name="button" value="保存设置" />&nbsp;&nbsp;&nbsp;&nbsp; 
						<%
						if (p4id != null && !p4id.trim().equals("")) {
						%> <script type="text/javascript">
						function viewp4() {
							var p4url = "../p4-msg!showP4Html.action?objectId=<%=p4id%>";
							window.open(p4url);
						}
						</script> <input style="width:80px;height:30px;font-size:13px;font-weight:bold;" type="button" value="预览效果" onclick="viewp4();" /> <%
						}
						%>
								</td>
							</tr>
						</table>
		</form>
		</div>
		</td>
		</tr>
	</table>
	<!-- 	<iframe id="callbackIframe" width="0" height="0" style="display: none;"></iframe> -->
</body>
<script type="text/javascript">
var titleValue = '<%=p4Title%>';
document.getElementById("p4title").value= titleValue;
</script>
</html>
<%!
private String htmlspecialchars(String str) {
	str = str.replaceAll("&", "&amp;");
	str = str.replaceAll("<", "&lt;");
	str = str.replaceAll(">", "&gt;");
	str = str.replaceAll("\"", "&quot;");
	return str;
}
%>
