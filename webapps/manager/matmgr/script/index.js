Dashboard = {
	u : function() {
		return this._usr ? this._usr : this._usr = new LoginUser('mat')
	},
	setAlert : function(msg, status, delay) {
		if (!status)
			status = 'ok';
		if (!delay)
			delay = 3;
		if (!this.msgCt) {
			this.msgCt = Ext.DomHelper.insertFirst(document.body, {
				  id : 'msg-div'
			  }, true);
			this.msgCt.setStyle('position', 'absolute');
			this.msgCt.setStyle('z-index', 9999);
			this.msgCt.setWidth(300);
		}
		this.msgCt.alignTo(document, 't-t');
		var html = ['<div class="app-msg">', '<div class="x-box-tl"><div class="x-box-tr"><div class="x-box-tc"></div></div></div>',
		  '<div class="x-box-ml"><div class="x-box-mr"><div class="x-box-mc"><h3 class="x-icon-text icon-status-' + status + '">', msg, '</h3>', '</div></div></div>',
		  '<div class="x-box-bl"><div class="x-box-br"><div class="x-box-bc"></div></div></div>', '</div>'].join('');
		Ext.DomHelper.append(this.msgCt, {
			  'html' : html
		  }, true).slideIn('t').pause(delay).ghost("t", {
			  remove : true
		  });
	},
	utils : {
		showFormItems : function(itemsArr) {
			for (var i = 0; i < itemsArr.length; i++) {
				itemsArr[i].setVisible(true);
				itemsArr[i].container.up('div.x-form-item').dom.style.display = '';
			}
		},
		hideFormItems : function(itemsArr) {
			for (var i = 0; i < itemsArr.length; i++) {
				itemsArr[i].setVisible(false);
				itemsArr[i].container.up('div.x-form-item').dom.style.display = 'none';
			}
		},
		arrayDataToMap : function(arr) {
			var _all = [], ret = {};
			for (var i = 0; i < arr.length; i++) {
				_all = _all.concat(arr[i]);
			}
			for (var i = 0; i < _all.length; i++) {
				ret[_all[i][0]] = _all[i][1];
			}
			return ret;
		}
	},
	nav : function() {
		var panelId = arguments[0];
		var node = Dashboard.navPanel.getRootNode().findChild('id', panelId, true);
		node.external = true;
		Dashboard.navPanel.fireEvent('click', node);
		var tab = Dashboard.mainPanel.get(panelId + 'Tab');
		Dashboard.mainPanel.activate(tab);
		tab.items.items[0].externalLoad.apply(tab.items.items[0], Array.prototype.slice.call(arguments, 1));
		node.external = false;
	}
};

getTopWindow().matmgr_Dashboard = Dashboard;

Ext.onReady(function() {
	  Ext.QuickTips.init();

	  var mainPanel = new Ext.TabPanel({
		    region : 'center',
		    activeTab : 0,
		    border : false,
		    style : 'border-left: 1px solid ' + sys_bdcolor,
		    resizeTabs : true,
		    tabWidth : 150,
		    minTabWidth : 120,
		    enableTabScroll : true,
		    layoutOnTabChange : true
	    });
	  var navs = Dashboard.u().filterAllowed([{
		    id : 'imgtxtmsg',
		    authName : 'mat.imgmsg',
		    leaf : true,
		    text : '图文消息',
		    iconCls : 'icon-imgmsg',
		    panelClass : ImgtxtMsgPanel
	    }, {
		    id : 'imagemsg',
		    authName : 'mat.image',
		    leaf : true,
		    text : '图片消息',
		    iconCls : 'icon-image',
		    panelClass : ImageMsgPanel
	    }, {
		    id : 'audiomsg',
		    authName : 'mat.audio',
		    leaf : true,
		    text : '语音消息',
		    iconCls : 'icon-audio',
		    panelClass : AudioMsgPanel
	    }, {
		    id : 'videomsg',
		    authName : 'mat.video',
		    leaf : true,
		    text : '视频消息',
		    iconCls : 'icon-video',
		    panelClass : VideoMsgPanel
	    }, {
		    id : 'p4msg',
		    authName : 'mat.p4',
		    leaf : true,
		    text : 'P4消息',
		    iconCls : 'icon-p4',
		    panelClass : P4MsgPanel
	    }]);

	  if (isVersionStandard()) {
		  var navs0 = Dashboard.u().filterAllowed([{
			    id : 'app',
			    authName : 'mat.app',
			    leaf : true,
			    text : '应用仓库',
			    iconCls : 'icon-app',
			    panelClass : AppView
		    }, {
			    id : 'app-custom',
			    authName : 'mat.appcustom',
			    leaf : true,
			    text : '自定义应用',
			    iconCls : 'icon-app',
			    panelClass : AppCustomView
		    }]);
		  if (!navs || !navs.length)
			  navs = navs0;
		  else if (navs0 && navs0.length)
			  navs = navs.concat(navs0);
	  }

	  var self = this;

	  var _importFileForm = new Ext.FormPanel({
		    layout : "fit",
		    frame : true,
		    border : false,
		    autoHeight : true,
		    waitMsgTarget : true,
		    defaults : {
			    bodyStyle : 'padding:10px'
		    },
		    margins : '0 0 0 0',
		    labelAlign : "left",
		    labelWidth : 50,
		    fileUpload : true,
		    items : [{
			      xtype : 'fieldset',
			      title : '选择文件',
			      autoHeight : true,
			      items : [{
				        ref : '../importFile',
				        name : 'importFile',
				        xtype : "textfield",
				        fieldLabel : '文件',
				        inputType : 'file',
				        anchor : '96%'
			        }]
		      }],
		    buttons : [{
			      text : "开始导入",
			      handler : function() {
				      var btn = this;
				      if (_importFileForm.importFile.getValue()) {
					      _importFileForm.form.submit({
						        params : {
							        'importTarget' : self.importNode.id
						        },
						        url : 'file-import-export!importZip.action',
						        success : function(form, action) {
							        btn.enable();
							        var result = action.result.data;
							        var timer = new ProgressTimer({
								          initData : result,
								          progressId : 'importMaterialStatus',
								          boxConfig : {
									          title : '正在导入素材...'
								          },
								          finish : function(p, response) {
									          // if (p.currentCount == 100) {
									          // Dashboard.navPanel.fireEvent('click', node);
									          // }
								          }
							          });
							        timer.start();
						        },
						        failure : function(form, action) {
							        btn.enable();
							        Ext.MessageBox.hide();
							        Ext.Msg.alert('错误', '导入初始化失败.' + (action.result.message ? '详细错误:' + action.result.message : ''));
						        }
					        });
				      } else
					      Ext.Msg.alert('错误', '导入文件不能为空');
			      }
		      }, {
			      text : "关闭",
			      handler : function() {
				      self.importWin.hide();
			      }
		      }]
	    });
	  this._importFileForm = _importFileForm;

	  var importWin = new Ext.Window({
		    width : 520,
		    title : '素材导入',
		    defaults : {// 表示该窗口中所有子元素的特性
			    border : false
			    // 表示所有子元素都不要边框
		    },
		    modal : true,
		    plain : true,
		    shim : true,
		    closeAction : 'hide',
		    closable : true, // 关闭
		    resizable : false,// 改变大小
		    draggable : true,// 拖动
		    animCollapse : true,
		    constrainHeader : true,
		    autoHeight : false,
		    items : [_importFileForm]
	    });
	  this.importWin = importWin;
	  this.importNode;

	  var menu = new Ext.menu.Menu({
		    items : [{
			      text : '导入数据',
			      iconCls : 'icon-import',
			      width : 50,
			      handler : function() {
				      self._importFileForm.form.reset();
				      self.importNode = menu.currentNode;
				      importWin.show();
			      }
		      }, {
			      text : '导出数据',
			      iconCls : 'icon-export',
			      width : 50,
			      handler : function() {
				      var node = menu.currentNode;
				      var self = this;
				      Ext.Ajax.request({
					        url : 'file-import-export!exportZip.action',
					        params : {
						        'target' : node.id
					        },
					        success : function(response) {
						        var result = Ext.util.JSON.decode(response.responseText);
						        if (result.success) {
							        var timer = new ProgressTimer({
								          initData : result.data,
								          progressId : 'exportMaterialStatus',
								          boxConfig : {
									          title : '正在导出数据...'
								          },
								          finish : function(p) {
									          if (p.currentCount != -1) {
										          if (!self.downloadIFrame) {
											          self.downloadIFrame = self.getEl().createChild({
												            tag : 'iframe',
												            style : 'display:none;'
											            })
										          }
										          self.downloadIFrame.dom.src = 'file-import-export!downExportFile.action?_t=' + new Date().getTime();
									          }
								          }
							          });
							        timer.start();
						        } else {
							        Ext.Msg.alert('错误', '导出失败:' + result.message)
						        }
					        },
					        failure : function(response) {
						        Ext.Msg.alert('错误', '导出失败')
					        },
					        scope : this
				        });
			      }
		      }]
	    });

	  var navPanel = new Ext.tree.TreePanel({
		    region : 'west',
		    width : 200,
		    minSize : 175,
		    maxSize : 400,
		    split : true,
		    collapsible : true,
		    collapseMode : 'mini',
		    border : false,
		    style : 'border-right: 1px solid ' + sys_bdcolor,
		    title : '控制台',
		    autoScroll : true,
		    rootVisible : false,
		    lines : false,
		    root : {
			    id : '0',
			    text : 'root',
			    children : navs
		    }
	    });

	  navPanel.on('contextmenu', function(node, event) {
		    if (node.id.indexOf('app') == -1) {
			    event.preventDefault();
			    menu.currentNode = node;
			    node.select();
			    menu.showAt(event.getXY());
		    }
	    });

	  Dashboard.navPanel = navPanel;
	  Dashboard.mainPanel = mainPanel;

	  navPanel.showTab = function(panelClass, tabId, title, iconCls, closable, cfg) {
		  if (!panelClass)
			  return false;
		  var tab = mainPanel.get(tabId);
		  if (!tab) {
			  var tabPanel = new panelClass(cfg);
			  tabPanel.navPanel = this;
			  tabPanel.tabId = tabId;
			  tab = mainPanel.add({
				    id : tabId,
				    layout : 'fit',
				    title : title,
				    items : tabPanel,
				    iconCls : iconCls,
				    closable : closable
			    });
			  tab.contentPanel = tabPanel;
			  tab.show();
		  } else {
			  mainPanel.activate(tabId);
			  if (title)
				  tab.setTitle(title)
			  if (iconCls)
				  tab.setIconClass(iconCls)
		  }
		  return tab.get(0);
	  };
	  navPanel.closeTab = function(tabId) {
		  mainPanel.remove(tabId)
	  };
	  navPanel.on('click', function(n) {
		    if (!n)
			    return false;
		    var panelClass = n.attributes.panelClass;
		    if (panelClass) {
			    var p = this.showTab(panelClass, n.id + 'Tab', n.text, n.attributes.iconCls, true);
			    if (p && p.init && !n.external)
				    p.init();
		    }
	    });
	  navPanel.on('expandnode', function(node) {
		    if (node.childNodes && node.childNodes.length) {
			    this.fireEvent('click', node.firstChild);
		    }
	    }, navPanel);
	  mainPanel.on('tabchange', function(tabPanel, tab) {
		    if (tab)
			    tab.contentPanel.fireEvent('onTabActive');
	    })

	  var viewport = new Ext.Viewport({
		    layout : 'border',
		    items : [navPanel, mainPanel]
	    });
  });