AppCustomView = function() {

	var store = new Ext.data.JsonStore({
				proxy : new Ext.data.MemoryProxy(),
				fields : ['id', 'name', 'icon'],
				sortInfo : {
					field : 'name',
					direction : 'ASC'
				}
			});
	var data = Ext.decode(http_get('app!list.action?type=1'));
	store.loadData(data);

	var dataview = new Ext.DataView({
		store : store,
		tpl : new Ext.XTemplate(
				'<ul>',
				'<tpl for=".">',
				'<li class="phone"><div tid="{id}" name="{name}" class="wrapper">',
				'<img width="60" height="60" src="images/appicon/app60/{icon}"/>',
				'<strong>{name}</strong><div class="links">',
				'</div></div></li>', '</tpl>', '</ul>'),
		plugins : [new Ext.ux.DataViewTransition({
					duration : 500,
					idProperty : 'id'
				})],
		itemSelector : 'li.phone',
		overClass : 'x-view-selected',
		singleSelect : true,
		multiSelect : false,
		autoScroll : true,
		listeners : {
			click : function(view, index, node, event) {
				var appId = node.childNodes[0].getAttribute('tid');
				var appName = node.childNodes[0].getAttribute('name');
				Dashboard.navPanel.showTab(FramePanel, appId, '自定义应用:'
								+ appName, 'icon-app', true, {
							url : 'app/app!editCustom.action?id=' + appId,
							id : appId
						});
			}
		}
	});
	dataview.addClass('phones')
	var viewPanel = new Ext.Panel({
				layout : 'fit',
				items : dataview,
				border : false,
				region : 'center',
				tbar : [{
					text : '新增应用',
					iconCls : 'icon-add',
					handler : function() {
						var appId = Ext.id()
						var appName = '新增应用'
						Dashboard.navPanel.showTab(FramePanel, appId, appName,
								'icon-app', true, {
									url : 'app/app!editCustom.action',
									id : appId
								});
					}.dg(this)
				}, {
					text : '刷新',
					iconCls : 'icon-refresh',
					handler : function() {
						var data = Ext
								.decode(http_get('app!list.action?type=1'));
						store.loadData(data);
					}.dg(this)
				}]
			});

	AppCustomView.superclass.constructor.call(this, {
				border : false,
				layout : 'border',
				items : [viewPanel]
			});

	dataview.on('afterrender', function() {
				filterData();
			});

	function filterData() {
		store.sort('name', 'DESC');
	}

};

Ext.extend(AppCustomView, Ext.Panel, {});

ShortcutToolRegistry.register('appcustom', function() {
			var items = Dashboard.navPanel.root.childNodes;
			if (items != null && items.length > 0)
				Ext.each(items, function(item) {
							if ('app-custom' == item.id) {
				                (function() {
									item.fireEvent('click', item);
								}).defer(100);
							}
						})
		}.dg(this));