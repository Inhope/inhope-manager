AppView = function() {

	var store = new Ext.data.JsonStore({
				proxy : new Ext.data.MemoryProxy(),
				fields : ['id', 'name', 'icon'],
				sortInfo : {
					field : 'name',
					direction : 'ASC'
				}
			});
	var data = Ext.decode(http_get('app!list.action'));
	store.loadData(data);

	var dataview = new Ext.DataView({
		store : store,
		tpl : new Ext.XTemplate(
				'<ul>',
				'<tpl for=".">',
				'<li class="phone"><div id="{id}" name="{name}" class="wrapper">',
				'<img width="72" height="72" src="images/app/{icon}"/>',
				'<strong>{name}</strong><div class="links">',
				'</div></div></li>', '</tpl>', '</ul>'),
		plugins : [new Ext.ux.DataViewTransition({
					duration : 500,
					idProperty : 'id'
				})],
		itemSelector : 'li.phone',
		overClass : 'x-view-selected',
		singleSelect : true,
		multiSelect : false,
		autoScroll : true,
		listeners : {
			click : function(view, index, node, event) {
				var appId = node.childNodes[0].id;
				var appName = node.childNodes[0].getAttribute('name');
				Dashboard.navPanel.showTab(FramePanel, appId, appName, 'icon-'
								+ appId, true, {
							url : 'app/' + appId + '.action',
							id : appId
						});
			}
		}
	});
	dataview.addClass('phones')

	var viewPanel = new Ext.Panel({
				title : '应用列表',
				layout : 'fit',
				items : dataview,
				border : false,
				region : 'center'
			});

	AppView.superclass.constructor.call(this, {
				border : false,
				layout : 'border',
				items : [viewPanel]
			});

	dataview.on('afterrender', function() {
				filterData();
			});

	function filterData() {
		store.sort('name', 'DESC');
	}

};

Ext.extend(AppView, Ext.Panel, {});

ShortcutToolRegistry.register('app', function() {
			var items = Dashboard.navPanel.root.childNodes;
			if (items != null && items.length > 0)
				Ext.each(items, function(item) {
							if ('app' == item.id) {
				                (function() {
									item.fireEvent('click', item);
								}).defer(100);
							}
						})
		}.dg(this));