FramePanel = function(cfg) {
	var config = {
		id : cfg.id + '-frame',
		border : false,
		html : "<iframe src='"
				+ cfg.url
				+ "' height='100%' width='100%' frameborder='no' border='0' marginwidth='0' marginheight='0' scrolling='auto' allowtransparency='yes'></iframe>"
	};
	FramePanel.superclass.constructor.call(this, config);
};

Ext.extend(FramePanel, Ext.Panel, {
	init : function() {
	}
});