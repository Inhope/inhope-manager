AudioMsgPanel = function(cfg) {
	var self = this;
	var config = {
		id : 'audioMsgPanel',
		border : false,
		html : "<iframe src='audio-msg!list.action?type=edit' height='100%' width='100%' frameborder='no' border='0' marginwidth='0' marginheight='0' scrolling='auto' allowtransparency='yes'></iframe>"
	}
	AudioMsgPanel.superclass.constructor.call(this, config);
}

Ext.extend(AudioMsgPanel, Ext.Panel, {
			init : function() {
			}
		});
ShortcutToolRegistry.register('audiomsg', function() {
			var items = Dashboard.navPanel.root.childNodes;
			if (items != null && items.length > 0)
				Ext.each(items, function(item) {
							if ('audiomsg' == item.id) {
				                (function() {
									item.fireEvent('click', item);
								}).defer(100);
							}
						})
		}.dg(this));