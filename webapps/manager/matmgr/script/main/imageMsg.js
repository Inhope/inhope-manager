ImageMsgPanel = function(cfg) {
	var self = this;
	var config = {
		id : 'imageMsgPanel',
		border : false,
		html : "<iframe src='image-msg!list.action?type=edit' height='100%' width='100%' frameborder='no' border='0' marginwidth='0' marginheight='0' scrolling='auto' allowtransparency='yes'></iframe>"
	}
	ImageMsgPanel.superclass.constructor.call(this, config);
}

Ext.extend(ImageMsgPanel, Ext.Panel, {
			init : function() {
			}
		});
ShortcutToolRegistry.register('imagemsg', function() {
			var items = Dashboard.navPanel.root.childNodes;
			if (items != null && items.length > 0)
				Ext.each(items, function(item) {
							if ('imagemsg' == item.id) {
				                (function() {
									item.fireEvent('click', item);
								}).defer(100);
							}
						})
		}.dg(this));