ImgtxtMsgPanel = function(cfg) {
	var self = this;
	var config = {
		id : 'imgtxtMsgPanel',
		border : false,
		html : "<iframe src='imgtxt-msg!list.action?type=edit' height='100%' width='100%' frameborder='no' border='0' marginwidth='0' marginheight='0' scrolling='auto' allowtransparency='yes'></iframe>"
	}
	ImgtxtMsgPanel.superclass.constructor.call(this, config);
}

Ext.extend(ImgtxtMsgPanel, Ext.Panel, {
			init : function() {
			}
		});
ShortcutToolRegistry.register('imgtxtmsg', function() {
			var items = Dashboard.navPanel.root.childNodes;
			if (items != null && items.length > 0)
				Ext.each(items, function(item) {
							if ('imgtxtmsg' == item.id) {
				                (function() {
									item.fireEvent('click', item);
								}).defer(100);
							}
						})
		}.dg(this));