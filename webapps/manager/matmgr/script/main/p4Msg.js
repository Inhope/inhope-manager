P4MsgPanel = function(cfg) {
	var self = this;
	var config = {
		id : 'p4MsgPanel',
		border : false,
		html : "<iframe src='p4-msg!list.action?type=edit' height='100%' width='100%' frameborder='no' border='0' marginwidth='0' marginheight='0' scrolling='auto' allowtransparency='yes'></iframe>"
	}
	P4MsgPanel.superclass.constructor.call(this, config);
}

Ext.extend(P4MsgPanel, Ext.Panel, {
			init : function() {
			}
		});
ShortcutToolRegistry.register('p4msg', function() {
			var items = Dashboard.navPanel.root.childNodes;
			if (items != null && items.length > 0)
				Ext.each(items, function(item) {
							if ('p4msg' == item.id) {
				                (function() {
									item.fireEvent('click', item);
								}).defer(100);
							}
						})
		}.dg(this));