VideoMsgPanel = function(cfg) {
	var self = this;
	var config = {
		id : 'videoMsgPanel',
		border : false,
		html : "<iframe src='video-msg!list.action?type=edit' height='100%' width='100%' frameborder='no' border='0' marginwidth='0' marginheight='0' scrolling='auto' allowtransparency='yes'></iframe>"
	}
	VideoMsgPanel.superclass.constructor.call(this, config);
}

Ext.extend(VideoMsgPanel, Ext.Panel, {
			init : function() {
			}
		});

ShortcutToolRegistry.register('videomsg', function() {
			var items = Dashboard.navPanel.root.childNodes;
			if (items != null && items.length > 0)
				Ext.each(items, function(item) {
							if ('videomsg' == item.id) {
				                (function() {
									item.fireEvent('click', item);
								}).defer(100);
							}
						})
		}.dg(this));