﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link type="text/css" rel="stylesheet" href="css/apiUser.css" />
<link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />
<link type="text/css" rel="stylesheet" href="css/material.css" />
<link type="text/css" href="css/jplayer/jplayer.blue.monday.css"
	rel="stylesheet" />
<style type="text/css">
.progressBar {
	width: 220px;
	font-size: 12px;
	font-family: Arial, sans-serif;
	border: 0;
	background: transparent url(images/loading.gif) no-repeat 10px 10px;
	top: 50%;
	left: 50%;
	margin-left: -74px;
	margin-top: 30px;
	padding: 10px 10px 10px 70px;
	text-align: right;
	line-height: 65px;
	font-weight: bold;
	position: absolute;
	z-index: 10000;
	color: white;
}

.mask {
	display: none;
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background: #000;
	z-index: 9999;
	-moz-opacity: 0.6;
	opacity: .60;
	filter: alpha(opacity =       60);
	height: 1000px;
}
</style>
<title>视频–iBotCloud小i机器人智能云服务平台</title>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery.jplayer.min.js"></script>
<script language="javascript">
function submitUpload(){ 
  if($("#materialVideo")[0].value){
    var fileName = $("#materialVideo")[0].value;
    var vaildSuffix = ['mp4','flv','m4v','avi','wmv','mov'];
    var v=0;
    for(var i=0;i<vaildSuffix.length;i++){
      if(fileName.substring(fileName.length - vaildSuffix[i].length).toLowerCase()== vaildSuffix[i].toLowerCase()){
         v++;
      }
    }
    if(v==0){
       alert('非法文件格式');
       return;
    }
    $("#materialVideoUpload").attr('action','<c:url value="video-msg!videoUpload.action?module=matCallback"/>');
    $(".progressBar").show();
	$(".mask").css({ display: "block", height: $(document).height() });//获取高度，兼容IE6。如果不考虑IE6直接可以换成show()
    $("#materialVideoUpload").submit();
	}
}
function submitCallback(msg){
      $(".mask").hide();
      $(".progressBar").hide();
      if('true'== msg){
       window.location=window.location;
      }else if('toobig'== msg){
        alert("视频文件太大");
      }else if('error'== msg){
         alert("上传失败");
      }else{
         alert(msg);
      }
}

function update(objectId){
var aNode = document.getElementById('updateName' + objectId);
		aNode.style.display = "block";
		var divNode = document.getElementById('filename' + objectId);
		divNode.style.border = "1px solid #ddd";
  var input = document.getElementById(objectId);
  input.readOnly = false;
  input.focus();
  input.select();
}
function updateShow(objectId){
  var input = document.getElementById(objectId);
  input.readOnly = true;
  var aNode = document.getElementById('updateName' + objectId);
		aNode.style.display = "none";
		var divNode = document.getElementById('filename' + objectId);
		divNode.style.border = "0px";
}
function updateName(objectId){
  var input = document.getElementById(objectId);
  input.readOnly = true;
  var fileName = input.value;
  if(!fileName){
     alert('文件名不能为空!');
     return;
  }
  var aj = $.ajax({
    url:'<c:url value="video-msg!update.action"/>',// 跳转到 action    
    data:{    
             objectId : objectId,    
             fileName : fileName
    },  
    type:'post',    
    cache:false,    
    dataType:'json',    
    success:function(data) {    
        if(data.success =="true" ){
            alert("修改成功！");    
        }else{    
            alert(data.msg);    
        }
        var aNode = document.getElementById('updateName' + objectId);
				aNode.style.display = "none";
				var divNode = document.getElementById('filename' + objectId);
				divNode.style.border = "0px";
     },    
     error : function() {   
     var aNode = document.getElementById('updateName' + objectId);
				aNode.style.display = "none";
				var divNode = document.getElementById('filename' + objectId);
				divNode.style.border = "0px"; 
          alert("异常！");    
     } 
  }); 
}
function remove(objectId){
//      $
// 				.ajax({
// 					url : '<c:url value="customKnowledge!queryAnswerByAnswerId.do"/>',// 跳转到 action    
// 					data : {
// 						answerId : objectId
// 					},
// 					type : 'post',
// 					cache : false,
// 					dataType : 'json',
// 					success : function(data) {
// 						if (data.exist == "true") {
// 							alert('当前素材有知识点关联，禁止删除！');
// 						} else if (data.exist == "false") {
							 if(window.confirm('你确定要删除此素材？')){
						           var aj = $.ajax({
						                      url:'<c:url value="video-msg!remove.action"/>',// 跳转到 action    
						            data:{    
						             objectId : objectId
						             },  
						            type:'post',    
						    		cache:false,    
						    		dataType:'json',
								    success:function(data) {    
								        if(data.success =="true" ){
								          document.getElementById('divCenterVideo').style.display = 'block';
								          var tmid = window.setTimeout(function(){
								              document.getElementById('divCenterVideo').style.display = 'none';
								              window.location.reload();
								          }, 1000);
								         // reloadData();
								        }else{  
								            alert(data.msg);    
								        }   
								     },    
								    error : function() {    
								          alert("异常！");    
								     } 
						           });
     						}
// 						}
// 					},
// 					error : function() {
// 						alert("查询是否关联知识点发生异常！");
// 					}
// 				});
}

function showPlayer(objectId,obj) {
$("#jp_container_1").css("display","block");
$("#jp_container_1").offset({"left":$(obj).offset().left,"top":$(obj).offset().top});
	if($("#jquery_jplayer").attr("ready")) {
		setTimeout(function(){
		$("#jquery_jplayer").jPlayer("setMedia", {
	            m4v: "video-msg!showVideo.action?objectId="+objectId
	          }).jPlayer( "play");
	    },500);
	}
	else {
		$("#jquery_jplayer").jPlayer({
	        ready: function () {
				$("#jquery_jplayer").attr("ready","ok");
				$(this).jPlayer("setMedia", {
					m4v: "video-msg!showVideo.action?objectId="+objectId
				}).jPlayer( "play");
	        },
	        ended: function() {
			 },
	        swfPath: "js",
	        wmode:"window",
	        supplied: "m4v",
	        solution:"flash,html"
	      });
	}
}
function hidePlayer() {
	$("#jquery_jplayer").jPlayer( "stop");
	$("#jp_container_1").css("display","none");
}
function searchName() {
      var nameContent = $("#searchInputName").val();
      if(nameContent){
         if('请输入文件名查询'==nameContent){
            alert('提示：请输入你要查询的名称');
         }else
         window.location.href = 'video-msg!list.action?type=edit&name=' + nameContent;
      }else{
         alert('提示：查询名称不能为空');
         return;
      }
}
</script>
</head>
<body style="">
	<div class="mask"></div>
	<div id="videoProgressBar" class="progressBar" style="display: none; ">视频文件上传(转换)中，请耐心等待...</div>
	<div id="jp_container_1" class="jp-video"
		style="display:none;position:absolute">
		<iframe src="videomsg/black.html"
			style="position:absolute;top:3px;right:3px;width:28px;height:28px;z-index:9989;border:0;padding:0;margin:0"></iframe>
		<div
			style="position:absolute;top:3px;right:3px;z-index:9990;cursor:pointer"
			onclick="hidePlayer()">
			<img src="images/video_close.png" />
		</div>

		<div class="jp-type-single">
			<div id="jquery_jplayer" class="jp-jplayer"></div>
			<div class="jp-gui">
				<div class="jp-video-play">
					<a href="javascript:;" class="jp-video-play-icon" tabindex="1">play</a>
				</div>
				<div class="jp-interface">
					<div class="jp-progress">
						<div class="jp-seek-bar">
							<div class="jp-play-bar"></div>
						</div>
					</div>
					<div class="jp-current-time"></div>
					<div class="jp-duration"></div>
					<div class="jp-controls-holder">
						<ul class="jp-controls">
							<li><a href="javascript:;" class="jp-play" tabindex="1">play</a>
							</li>
							<li><a href="javascript:;" class="jp-pause" tabindex="1">pause</a>
							</li>
							<li><a href="javascript:;" class="jp-stop" tabindex="1">stop</a>
							</li>
							<li><a href="javascript:;" class="jp-mute" tabindex="1"
								title="mute">mute</a></li>
							<li><a href="javascript:;" class="jp-unmute" tabindex="1"
								title="unmute">unmute</a></li>
							<li><a href="javascript:;" class="jp-volume-max"
								tabindex="1" title="max volume">max volume</a></li>
						</ul>
						<div class="jp-volume-bar">
							<div class="jp-volume-bar-value"></div>
						</div>
						<ul class="jp-toggles">
							<li><a href="javascript:;" class="jp-full-screen"
								tabindex="1" title="full screen">full screen</a></li>
							<li><a href="javascript:;" class="jp-restore-screen"
								tabindex="1" title="restore screen">restore screen</a></li>
							<li><a href="javascript:;" class="jp-repeat" tabindex="1"
								title="repeat">repeat</a></li>
							<li><a href="javascript:;" class="jp-repeat-off"
								tabindex="1" title="repeat off">repeat off</a></li>
						</ul>
					</div>
					<!-- <div class="jp-title">
            <ul>
              <li>Big Buck Bunny Trailer</li>
            </ul>
          </div>
           -->
				</div>
			</div>
			<div class="jp-no-solution">
				<span>Update Required</span> To play the media you will need to
				either update your browser to a recent version or update your <a
					href="http://get.adobe.com/flashplayer/" target="_blank">Flash
					plugin</a>
			</div>
		</div>
	</div>


	<div>
		<div class="con">
			<div style="width: 100%; float: left; margin-left: 10px;">
				<div style="width: 100%; margin: 10px 0 5px 0;">
					<div style="height: 29px;">
						<div class="left">
							<img src="images/title_left.gif" style="width: 9px;" />
						</div>
						<div class="title_mid left" style="width: 98%;">视频</div>
						<div class="left">
							<img src="images/title_right.gif" style="width: 8px;" />
						</div>
					</div>
					<iframe style="display: none" src="about:blank"
						name="videoFileupload"></iframe>
					<div class="right_con" style="width:99.4%;">
<div class="scgl_title_search">
					<div style="padding-left:10px;">
							<div class="search">
							  <div class="search_suo">
							    <input name="text" type="text" id="searchInputName" style="color: rgb(102, 102, 102);" onfocus="if(value =='请输入文件名查询'){value ='';}" onblur="if (value ==''){value='请输入文件名查询';}" onkeypress="if(event.keyCode==13) {searchName();}" value="请输入文件名查询">
						      <span onclick="searchName();"><a href="javascript:void(0)" id="" onclick="searchName();"></a> </span> </div>
							</div>
						</div>	
							<div class="scgl_title_con">
									
							</div>
						</div>
						<div id="divCenterVideo"
							style="position: absolute; z-index: 100; display: none; background-color: #A7D3F1; width: 100px;height:52px; text-align: center; left: 35%; top: 110px;">
							<p
								style="font-weight: bold; font-size: 15px; color: #fff;margin-top:15px"">
								删除成功</p>
						</div>
						<div style="padding-left:10px;">
							<div>
								<form id="materialVideoUpload" style="float:left"
									encType="multipart/form-data" method="post"
									target="videoFileupload" name="videoForm" action="">
									<div id="materialVideoFile">
										<input type="button" value=""
											style="cursor:pointer;background:url(images/uploadSubmit.jpg) no-repeat;width:70px;height:26px;line-height:26px;color:white;border:0" />
										<input id="materialVideo" onchange="submitUpload();"
											style="cursor:pointer;" type="file" name="videoFile" />
										&nbsp;&nbsp;<span
											style="font-size:2;position:absolute;left:85px;top:5px">注：视频大小限制：10M，
											格式限制：mp4,flv,m4v,avi,wmv,mov</span>
									</div>
								</form>
								<%@include file="../nav-templatePage.jsp"%>

								<div style="clear:both;"></div>
								<div class="material_content" style="padding-left:10px">
									<div class="list_title">
										<div style="margin:0 auto;width:80%;">
											<div class="left title msg">文件名</div>
											<div class="second title msg">文件</div>
											<div class="right title opt">操作</div>
											<div class="right title opt">大小</div>
										</div>
									</div>
									<div>
										<ul class="list_content">
											<c:forEach items="${pg.result}" var="videoDomain"
												varStatus="s">
												<li class="list_item">
													<div style="margin:0 auto;width:80%;">
														<div class="left file">
															<div class="file_name_area"
																id="filename${videoDomain.id}">
																<input type="text" id="${videoDomain.id}"
																	readonly="true" class="file_name"
																	value="${videoDomain.name}" maxlength="20"
																	style="float:left;border:0;"
																	onkeypress="if(event.keyCode==13) {updateName('${videoDomain.id}');}">
																</input> <a href="javascript:updateName('${videoDomain.id}');"
																	style="float:right;display:none"
																	id="updateName${videoDomain.id}"><img
																	src="images/submitTip.jpg" width="29" height="30">
																</a>
															</div>
															<c:choose>
																<c:when test="${not empty videoDomain.imageId}">
																	<img src="images/playerSubmit.png"
																		style="cursor:pointer;width:124px;height:79px;border:6px solid #ccc;
																	background: url(video-msg!showVideoImage.action?objectId=${videoDomain.imageId});
																	background-size:124px 79px;
                                                                    background-repeat:no-repeat;"
																		onclick="showPlayer('${videoDomain.id}',this)">
																	</img>
																</c:when>
																<c:otherwise>
																	<img src="images/video_preview.png"
																		style="cursor:pointer"
																		onclick="showPlayer('${videoDomain.id}',this)">
																	</img>
																</c:otherwise>
															</c:choose>
														</div>
														<div class="opt oper right">
															<a style="margin:0 6px;" title="下载"
																href="video-msg!download.action?objectId=${videoDomain.id}">下载</a>
															<a style="margin:0 6px;" title="重命名"
																href="javascript:update('${videoDomain.id}')">重命名</a> <a
																style="margin:0 6px;" title="删除"
																href="javascript:remove('${videoDomain.id}')">删除</a>
														</div>
														<div class="right size">${videoDomain.size}</div>
														<div class="clr"></div>
													</div></li>
											</c:forEach>
										</ul>
									</div>
								</div>
							</div>
							<%@include file="../nav-templatePage.jsp"%>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>