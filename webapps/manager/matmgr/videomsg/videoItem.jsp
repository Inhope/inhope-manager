<%@page import="com.incesoft.xmas.matmgr.service.VideoMsgService"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page
	import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link type="text/css" rel="stylesheet" href="../css/jquery-ui.css" />
<link type="text/css" rel="stylesheet" href="../css/material.css" />
<link type="text/css" href="../css/jplayer/jplayer.blue.monday.css"
	rel="stylesheet" />
<title>视频–iBotCloud小i机器人智能云服务平台</title>
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery-ui.js"></script>
<script type="text/javascript" src="../js/jquery.jplayer.min.js"></script>
<%
	String videoId = request.getParameter("videoId");
	VideoMsgService videoService = WebApplicationContextUtils.getWebApplicationContext(application).getBeansOfType(VideoMsgService.class).values().iterator().next();
	boolean isHasImage = videoService.isCoverImage(videoId);
	String videoSrc="";
	if(isHasImage)
	   videoSrc = "video-msg!showVideoImage.action?objectId="+videoId;
%>
<script language="javascript">
	function showPlayer(objectId) {
	    parent.open('videoPlayer.jsp?videoId=' + objectId,'newwindow', 'height=349, width=490,top=100,left=450, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=n o, status=no');
	}
</script>
</head>
<body style="border:0;padding:0;margin-top:5px;margin-left:10px;">
	<div class="xi_img">
	   <%
	      if(StringUtils.isNotBlank(videoSrc)) {
	          out.print("<img src=\"../images/playerSubmit.png\" style=\"display:block;cursor:pointer;width:124px;height:79px;border:6px solid #ccc;background: url('"+videoSrc+"');background-size:124px 79px;background-repeat:no-repeat;\" onclick=\"showPlayer('"+videoId+"',this)\"></img>");
	      }else{ 
	          out.print("<img src=\"../images/video_preview.png\" style=\"cursor:pointer\" onclick=\"showPlayer('"+videoId+"',this)\"> </img>");
	      }
	   %>
	</div>
</body>
</html>