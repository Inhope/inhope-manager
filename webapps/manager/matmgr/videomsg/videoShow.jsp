<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/select.css" type="text/css" />
<link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />
<link type="text/css" rel="stylesheet" href="css/material.css" />
<title>视频–iBotCloud小i机器人智能云服务平台</title>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery.jplayer.min.js"></script>
<script type="text/javascript" src="js/adjust.js"></script>
<script language="javascript">
	function selectClick(videoId, name, isClick) {
		document.getElementById(videoId).checked = true;
		document.getElementById('selectedVal').value = videoId;
		if (isClick)
			parent.answerRendered({
				id : videoId,
				type : 'videomsg',
				title : name
			});
	}

	function hidePlayer() {
		$("#jquery_jplayer").jPlayer("stop");
		$("#jp_container_1").css("display", "none");
	}
</script>
</head>
<body>
	<div id="jp_container_1" class="jp-video "
		style="display:none;position:absolute">
		<div style="position:absolute;top:3px;right:3px;cursor:pointer;"
			onclick="hidePlayer()">
			<img src="images/video_close.png" />
		</div>
		<div class="jp-type-single">
			<div id="jquery_jplayer" class="jp-jplayer"></div>
			<div class="jp-gui">
				<div class="jp-video-play">
					<a href="javascript:;" class="jp-video-play-icon" tabindex="1">play</a>
				</div>
				<div class="jp-interface">
					<div class="jp-progress">
						<div class="jp-seek-bar">
							<div class="jp-play-bar"></div>
						</div>
					</div>
					<div class="jp-current-time"></div>
					<div class="jp-duration"></div>
					<div class="jp-controls-holder">
						<ul class="jp-controls">
							<li><a href="javascript:;" class="jp-play" tabindex="1">play</a>
							</li>
							<li><a href="javascript:;" class="jp-pause" tabindex="1">pause</a>
							</li>
							<li><a href="javascript:;" class="jp-stop" tabindex="1">stop</a>
							</li>
							<li><a href="javascript:;" class="jp-mute" tabindex="1"
								title="mute">mute</a>
							</li>
							<li><a href="javascript:;" class="jp-unmute" tabindex="1"
								title="unmute">unmute</a>
							</li>
							<li><a href="javascript:;" class="jp-volume-max"
								tabindex="1" title="max volume">max volume</a>
							</li>
						</ul>
						<div class="jp-volume-bar">
							<div class="jp-volume-bar-value"></div>
						</div>
						<ul class="jp-toggles">
							<li><a href="javascript:;" class="jp-full-screen"
								tabindex="1" title="full screen">full screen</a>
							</li>
							<li><a href="javascript:;" class="jp-restore-screen"
								tabindex="1" title="restore screen">restore screen</a>
							</li>
							<li><a href="javascript:;" class="jp-repeat" tabindex="1"
								title="repeat">repeat</a>
							</li>
							<li><a href="javascript:;" class="jp-repeat-off"
								tabindex="1" title="repeat off">repeat off</a>
							</li>
						</ul>
					</div>
					<!-- <div class="jp-title">
            <ul>
              <li>Big Buck Bunny Trailer</li>
            </ul>
          </div>
           -->
				</div>
			</div>
			<div class="jp-no-solution">
				<span>Update Required</span> To play the media you will need to
				either update your browser to a recent version or update your <a
					href="http://get.adobe.com/flashplayer/" target="_blank">Flash
					plugin</a>.
			</div>
		</div>
	</div>


	<div id="mainDiv" class="dialog">
		<div class="dialog_content">
			<div class="dialog_list">
				<input type="hidden" id="selectedVal" /> <input type="hidden"
					id="type" value="videomsg" />
<!-- 				<div class="page_nav"> -->
<!-- 					<%@include file="../nav-templatePage.jsp"%> -->
<!-- 				</div> -->
				<!----------------------循环列表内容------------------------->
				<div class="list_ul">
					<c:choose>
						<c:when test="${pg.totalCount>0}">
							<ul>
								<c:forEach items="${pg.result}" var="videoDomain" varStatus="s">
									<li class="float-p">
										<div class="left checkbox">
											<c:choose>
												<c:when test="${videoDomain.id==param.materialId}">
													<input type="radio" value="${videoDomain.id}" id="${videoDomain.id}" name="file"
														checked="checked" onclick="selectClick(this.value,'${videoDomain.name}',true)">
														<script>
															selectClick('${videoDomain.id}','${videoDomain.name}',false);
														</script>
												</c:when>
												<c:otherwise>
													<input type="radio" value="${videoDomain.id}" name="file" id="${videoDomain.id}"
														onclick="selectClick(this.value,'${videoDomain.name}',true)">
												</c:otherwise>
											</c:choose>
										</div>
										<div class="left file_infor">
											<div class="left">
												<div class="fileName">${videoDomain.name}</div>
												<c:choose>
													<c:when test="${not empty videoDomain.imageId}">
														<div class="xi_img">
															<img src="images/playerSubmit.png"
																style="cursor:pointer;width:124px;height:79px;border:6px solid #ccc;
													background: url(video-msg!showVideoImage.action?objectId=${videoDomain.imageId});
													background-size:124px 79px;
                                                                background-repeat:no-repeat;">
															</img>
														</div>
													</c:when>
													<c:otherwise>
														<div class="xi_img">
															<img src="images/video_preview.png"
															onclick="selectClick('${videoDomain.id}','${videoDomain.name}',true)"
																style="cursor:pointer"> </img>
														</div>
													</c:otherwise>
												</c:choose>
											</div>
											<div class="right tr">
												<div>${videoDomain.size}</div>
											</div>
											<div class="clr"></div>
											<div></div>
										</div>
									</li>
								</c:forEach>
							</ul>
						</c:when>
						<c:otherwise>
							  没有数据
							</c:otherwise>
					</c:choose>
				</div>
				<div class="page_nav"><%@include
						file="../nav-templatePage.jsp"%></div>
			</div>
		</div>
	</div>
</body>
</html>