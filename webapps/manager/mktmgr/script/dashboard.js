Dashboard = {
	u : function() {
		return this._usr ? this._usr : this._usr = new LoginUser('mkt')
	},
	setAlert : function(msg, status, delay) {
		if (!status)
			status = 'ok'
		if (!delay)
			delay = 3
		if (!this.msgCt) {
			this.msgCt = Ext.DomHelper.insertFirst(document.body, {
				id : 'msg-div'
			}, true);
			this.msgCt.setStyle('position', 'absolute');
			this.msgCt.setStyle('z-index', 9999);
			this.msgCt.setWidth(300);
		}
		this.msgCt.alignTo(document, 't-t');
		var html = [ '<div class="app-msg">', '<div class="x-box-tl"><div class="x-box-tr"><div class="x-box-tc"></div></div></div>',
				'<div class="x-box-ml"><div class="x-box-mr"><div class="x-box-mc"><h3 class="x-icon-text icon-status-' + status + '">', msg,
				'</h3>', '</div></div></div>', '<div class="x-box-bl"><div class="x-box-br"><div class="x-box-bc"></div></div></div>', '</div>' ]
				.join('');
		Ext.DomHelper.append(this.msgCt, {
			'html' : html
		}, true).slideIn('t').pause(delay).ghost("t", {
			remove : true
		});
	},
	utils : {
		showFormItems : function(itemsArr) {
			for ( var i = 0; i < itemsArr.length; i++) {
				itemsArr[i].setVisible(true);
				itemsArr[i].container.up('div.x-form-item').dom.style.display = '';
			}
		},
		hideFormItems : function(itemsArr) {
			for ( var i = 0; i < itemsArr.length; i++) {
				itemsArr[i].setVisible(false);
				itemsArr[i].container.up('div.x-form-item').dom.style.display = 'none';
			}
		},
		arrayDataToMap : function(arr) {
			var _all = [], ret = {};
			for ( var i = 0; i < arr.length; i++) {
				_all = _all.concat(arr[i]);
			}
			for ( var i = 0; i < _all.length; i++) {
				ret[_all[i][0]] = _all[i][1];
			}
			return ret;
		}
	},
	createXHR : function() {
		var _xhr = false;
		try {
			_xhr = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				_xhr = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e2) {
				_xhr = false;
			}
		}
		if (!_xhr && window.XMLHttpRequest)
			_xhr = new XMLHttpRequest();
		return _xhr;
	},
	createDimChooser : function(_width) {
		return Ext.create({
			width : _width,
			name : 'dimension',
			xtype : 'dimensionbox',
			clearBtn : true,
			emptyText : '请选择维度',
			menuConfig : {
				labelAlign : 'top'
			},
			valueTransformer : function(value) {
				if (value == 'ALLDIM')
					return '';
				var tags = this.tags;
				var groupTags = {};
				if (tags)
					Ext.each(tags, function(t) {
						groupTags[t.dimId] = t.id;
					});
				return Ext.encode(groupTags);
			}
		});
	},
	nav : function() {
		var panelId = arguments[0];
		var node = Dashboard.navPanel.getRootNode().findChild('id', panelId, true);
		node.external = true;
		Dashboard.navPanel.fireEvent('click', node);
		var tab = Dashboard.mainPanel.get(panelId + 'Tab');
		Dashboard.mainPanel.activate(tab);
		tab.items.items[0].externalLoad.apply(tab.items.items[0], Array.prototype.slice.call(arguments, 1));
		node.external = false;
	}
};