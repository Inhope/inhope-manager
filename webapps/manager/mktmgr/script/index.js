Ext.override(Ext.DataView, {
	onContextMenu : function(e) {
		var item = e.getTarget(this.itemSelector, this.el);
		this.fireEvent("contextmenu", this, item ? this.indexOf(item) : -1, item, e);
	}
});

getTopWindow().mktmgr_Dashboard = Dashboard;

Ext.onReady(function() {
	Ext.QuickTips.init();

	var xhr = Dashboard.createXHR();
	xhr.open('GET', 'service-module!listDimenstion.action?dim=platform', false);
	xhr.send();
	var platformList = Ext.decode(xhr.responseText);
	Dashboard.platformMap = {};
	if (platformList && platformList.length) {
		for ( var i = 0; i < platformList.length; i++) {
			Dashboard.platformMap[platformList[i].tag] = platformList[i];
		}
	}

	xhr.open('GET', 'robot-log!listDimensionNames.action', false);
	xhr.send();
	Dashboard.dimensionNames = Ext.decode(xhr.responseText);

	var mainPanel = new Ext.TabPanel({
		region : 'center',
		activeTab : 0,
		border : false,
		style : 'border-left: 1px solid ' + sys_bdcolor,
		resizeTabs : true,
		tabWidth : 150,
		minTabWidth : 120,
		enableTabScroll : true,
		layoutOnTabChange : true
	});
	var navs = Dashboard.u().filterAllowed([ {
		id : '',
		authName : 'mkt.svc',
		leaf : true,
		text : '系统服务管理',
		iconCls : 'icon-message-resource',
		panelClass : BizInfoMainPanel
	}, {
		id : 'messageResource',
		authName : 'mkt.msgr',
		leaf : true,
		text : '服务参数配置',
		iconCls : 'icon-message-resource',
		panelClass : MessageResourcePanel
	}, {
		id : 'welcomeResource',
		authName : 'mkt.welr',
		leaf : true,
		text : '特殊欢迎语配置',
		iconCls : 'icon-message-resource',
		panelClass : WelcomeResourcePanel
	}, {
		id : 'simleDialog',
		authName : 'mkt.sdlg',
		leaf : true,
		text : '对话预处理',
		iconCls : 'icon-simple-dialog',
		panelClass : SimpleDialogPanel
	},
	// {
	// id : 'signAct',
	// leaf : true,
	// authName : 'mkt.sig',
	// text : '签名活动管理',
	// iconCls : 'icon-sign-act',
	// panelClass : SignActivityPanel
	// },
	// {
	// id : 'vote',
	// leaf : true,
	// authName : 'mkt.vote',
	// text : '调查投票管理',
	// iconCls : 'icon-vote',
	// panelClass : VotePanel
	// },
	{
		id : 'dynamicMenu',
		leaf : true,
		authName : 'mkt.dm',
		text : '动态菜单管理',
		iconCls : 'icon-dynamic-menu',
		panelClass : DynamicMenuPanel
	},
	// {
	// id : 'robotLib',
	// authName : 'mkt.lib',
	// leaf : true,
	// text : '二次开发管理',
	// iconCls : 'icon-business-lib',
	// panelClass : BusinessLibPanel
	// },
	{
		id : 'pushMsg',
		authName : 'mkt.push',
		text : '消息推送管理',
		iconCls : 'icon-push-msg',
		children : [ {
			id : 'pushMsgList',
			leaf : true,
			authName : 'mkt.push.list',
			text : '消息列表',
			iconCls : 'icon-push-msg-list',
			panelClass : PushMessagePanel
		}, {
			id : 'pushHistory',
			authName : 'mkt.push.his',
			leaf : true,
			text : '推送历史',
			iconCls : 'icon-push-msg-history',
			panelClass : PushHistoryPanel
		} ]
	}, {
		id : 'hotQuestion',
		text : '热点问题管理',
		authName : 'mkt.hq',
		iconCls : 'icon-hot-question',
		children : [ {
			id : 'hotQuestionCus',
			leaf : true,
			authName : 'mkt.hq.cus',
			text : '定制热点问题',
			iconCls : 'icon-hot-question-cus',
			panelClass : HotQuestionCusPanel
		}, {
			id : 'hotQuestionAuto',
			leaf : true,
			authName : 'mkt.hq.auto',
			text : '动态热点问题',
			iconCls : 'icon-hot-question-auto',
			panelClass : HotQuestionAutoPanel
		} ]
	}
	// , {
	// id : 'notice',
	// leaf : true,
	// authName : 'mkt.ntc',
	// text : '公告内容管理',
	// iconCls : 'icon-notice',
	// panelClass : NoticePanel
	// }
	]);

	var navPanel = new Ext.tree.TreePanel({
		region : 'west',
		width : 200,
		minSize : 175,
		maxSize : 400,
		split : true,
		collapsible : true,
		collapseMode : 'mini',
		border : false,
		style : 'border-right: 1px solid ' + sys_bdcolor,
		title : '控制台',
		autoScroll : true,
		rootVisible : false,
		lines : false,
		root : {
			id : '0',
			text : 'root',
			children : navs
		}
	});

	Dashboard.navPanel = navPanel;
	Dashboard.mainPanel = mainPanel;

	navPanel.showTab = function(panelClass, tabId, title, iconCls, closable) {
		if (!panelClass)
			return false;
		var tab = mainPanel.get(tabId);
		if (!tab) {
			var tabPanel = new panelClass();
			tabPanel.navPanel = this;
			tabPanel.tabId = tabId;
			tab = mainPanel.add({
				id : tabId,
				layout : 'fit',
				title : title,
				items : tabPanel,
				iconCls : iconCls,
				closable : closable
			});
			tab.contentPanel = tabPanel;
			tab.show();
		} else {
			mainPanel.activate(tabId);
			if (title)
				tab.setTitle(title)
			if (iconCls)
				tab.setIconClass(iconCls)
		}
		return tab.get(0);
	};
	navPanel.closeTab = function(tabId) {
		mainPanel.remove(tabId)
	};
	navPanel.on('click', function(n) {
		if (!n)
			return false;
		var panelClass = n.attributes.panelClass;
		if (panelClass) {
			var p = this.showTab(panelClass, n.id + 'Tab', n.text, n.attributes.iconCls, true);
			if (p && p.init && !n.external)
				p.init();
		}
	});
	navPanel.on('expandnode', function(node) {
		if (node.childNodes && node.childNodes.length) {
			this.fireEvent('click', node.firstChild);
			if (getTopWindow().mktmgr_initcb) {
				getTopWindow().mktmgr_initcb.call();
				getTopWindow().mktmgr_initcb = null;
			}
		}
	}, navPanel);
	mainPanel.on('tabchange', function(tabPanel, tab) {
		if (tab)
			tab.contentPanel.fireEvent('onTabActive');
	})

	var viewport = new Ext.Viewport({
		layout : 'border',
		items : [ navPanel, mainPanel ]
	});
});