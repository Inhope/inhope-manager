BizInfoMainPanel = Ext.extend(Ext.TabPanel, {
	activeTab : 0,
	tabPosition : 'bottom',
	initComponent : function() {
		var bizinfo = new BizInfoPanel();
		var bizinterceptor = new BizInterceptorPanel();
		this.items = [ {
			autoScroll : true,
			items : bizinfo,
			title : '服务管理'
		}, {
			autoScroll : true,
			items : bizinterceptor,
			title : '拦截器管理'
		} ];
		BizInfoMainPanel.superclass.initComponent.call(this);
	}
});