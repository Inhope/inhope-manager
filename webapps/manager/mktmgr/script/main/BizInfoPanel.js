BizInfoPanel = function() {
	var self = this;

	var fields = ['id', 'uid', 'name', 'classname', 'type', 'scope', 'descriptionDetail', 'bundle', 'state', 'flowId', 'flowData'];
	var baseStore = new Ext.data.JsonStore({
				url : 'biz-info!list.action?bundle=base',
				root : 'data',
				idProperty : 'uid',
				autoLoad : true,
				fields : fields
			});
	var dynStore = new Ext.data.JsonStore({
				url : 'biz-info!list.action?bundle=dyn',
				root : 'data',
				idProperty : 'uid',
				autoLoad : true,
				fields : fields
			});
	// debug code >>>
	// setTimeout(function() {
	// _editWin.startEdit(0)
	// }, 500)
	// debug code <<<
	var devStore = new Ext.data.JsonStore({
				url : 'biz-info!list.action?bundle=dev',
				root : 'data',
				idProperty : 'uid',
				autoLoad : true,
				fields : fields
			});

	var tpl = new Ext.XTemplate(
			'<tpl for=".">',
			'<div class="bizinfo" title="{descriptionDetail}" style="float:left;margin:3px;width:195px;" uid="{uid}" state="{state}">',
			'<table border="1" style="',
			'<tpl if="type==\'TextEvent\'">',
			'background-color:#FB8841;',
			'</tpl>',
			'<tpl if="type==\'CommandEvent\' && type!=\'TextEvent\'">',
			'background-color:rgb(255,255,204);',
			'</tpl>',
			'<tpl if="type!=\'CommandEvent\' && type!=\'TextEvent\'">',
			'background-color:#F5B6FC;',
			'</tpl>',
			'<tpl if="this.isDisabled(state)">',
			'background-color:#CCC;',
			'</tpl>',
			'border:1px solid rgb(153,0,51);border-collapse:collapse;" width="100%"><tr><td style="padding:2px;',
			'<tpl if="this.isDisabled(state)">',
			'text-decoration:line-through;',
			'</tpl>',
			'" align="center">{name}' + '<tpl if="bundle==\'dyn\'">',
			'<span class="icon-table-edit" onclick="window.editWin__bizInfo.startEdit({#}-1);"></span>',
			'</tpl>',
			'</td></tr><tr><td style="padding:2px;">',
			'<tpl if="type==\'CommandEvent\'">',
			'指令: <span title="点击查询日志" onclick="var servId=this.innerHTML;getTopWindow().vtab.setActiveTab(4);if(!getTopWindow().OM_Dashboard){getTopWindow().om_serv_cb=function(){getTopWindow().OM_Dashboard.navToService(servId);}}else{getTopWindow().om_serv_cb=null;getTopWindow().OM_Dashboard.navToService(this.innerHTML);}" style="color:blue;cursor:pointer;text-decoration:underline;">{id}</span>',
			'</tpl>', '<tpl if="type!=\'CommandEvent\'">', '指令: 无', '</tpl>', '<br/>范围: ', '<tpl if="\'SESSION\' == scope">', '会话', '</tpl>',
			'<tpl if="\'APPLICATION\' == scope">', '全局', '</tpl>', '<br/>类名: {classname}</td></tr></table></div>', '</tpl>', '<div style="clear:both"></div>', {
				isDisabled : function(state) {
					return (state & 1) == 1;
				}
			});

	var _getColorRef = function(color, ref) {
		return '<span style="background-color:' + color + ';width:8px;height:8px;margin-left:8px;border:1px solid rgb(153,0,51);">&nbsp;&nbsp;&nbsp;</span> ' + ref;
	};

	var menu = new Ext.menu.Menu({
				items : [{
							text : '编辑',
							ref : 'edit',
							iconCls : 'icon-edit',
							handler : function() {
								var idx = menu.currentItem.getAttribute('index');
								_editWin.startEdit(idx);
							}
						}, {
							text : '激活',
							ref : 'activate',
							iconCls : 'icon-hot-question',
							handler : function() {
								Ext.Msg.confirm('提示', '确认激活服务？', function(btn) {
											if (btn == 'yes') {
												Ext.Ajax.request({
															url : 'biz-info!activate.action',
															params : {
																uid : menu.currentItem.getAttribute('uid')
															},
															success : function() {
																Dashboard.setAlert('激活服务成功。');
																menu.currentView.getStore().reload();
															}
														});
											}
										});
							}
						}, {
							text : '钝化',
							ref : 'deactivate',
							iconCls : 'icon-hot-question-auto',
							handler : function() {
								Ext.Msg.confirm('提示', '确认钝化服务？', function(btn) {
											if (btn == 'yes') {
												Ext.Ajax.request({
															url : 'biz-info!deactivate.action',
															params : {
																uid : menu.currentItem.getAttribute('uid')
															},
															success : function() {
																Dashboard.setAlert('钝化服务成功。');
																menu.currentView.getStore().reload();
															}
														});
											}
										});
							}
						}, {
							text : '删除',
							ref : '_delete',
							iconCls : 'icon-delete',
							handler : function() {
								Ext.Msg.confirm('提示', '确认删除服务？', function(btn) {
											if (btn == 'yes') {
												Ext.Ajax.request({
															url : 'biz-info!remove.action',
															params : {
																uid : menu.currentItem.getAttribute('uid')
															},
															success : function() {
																Dashboard.setAlert('删除服务成功。');
																menu.currentView.getStore().reload();
															}
														});
											}
										});
							}
						}]
			});

	var viewCfg = {
		tpl : tpl,
		itemSelector : 'div.bizinfo',
		autoHeight : true,
		prepareData : function(data) {
			data.classname = data.classname.substring(data.classname.lastIndexOf('.') + 1);
			if (!data.name)
				data.name = data.classname;
			return data;
		},
		listeners : {
			contextmenu : function(dataview, index, item, e) {
				e.preventDefault();
				if (!item)
					return;
				item.setAttribute('index', index);
				menu.currentItem = item;
				menu.currentView = dataview;
				menu.activate.setDisabled(false);
				menu.deactivate.setDisabled(false);
				menu.edit.setDisabled(false);
				if (item.getAttribute('state') == 1)
					menu.deactivate.setDisabled(true);
				else
					menu.activate.setDisabled(true);
				if (dataview.bundle != 'dyn') {
					menu._delete.setDisabled(true);
					menu.edit.setDisabled(true);
				}
				menu.showAt(e.getXY());
			}
		}
	};
	var baseViewCfg = {
		store : baseStore,
		emptyText : '',
		bundle : 'base'
	}, devViewCfg = {
		store : devStore,
		emptyText : '尚无定制服务',
		bundle : 'dev'
	}, dynViewCfg = {
		store : dynStore,
		emptyText : '尚无动态服务',
		bundle : 'dyn'
	};
	Ext.apply(baseViewCfg, viewCfg);
	Ext.apply(devViewCfg, viewCfg);
	Ext.apply(dynViewCfg, viewCfg);

	var _editForm = new Ext.form.FormPanel({
				width : '100%',
				frame : false,
				border : false,
				labelWidth : 75,
				autoHeight : true,
				waitMsgTarget : true,
				tbar : [{
							text : '保存',
							iconCls : 'icon-save',
							handler : function() {
								this.disable();
								self.saveHandler(this);
							}
						}],
				bodyStyle : 'padding : 3px 10px; background-color:' + sys_bgcolor,
				defaults : {
					anchor : '98%',
					allowBlank : false
				},
				items : [new Ext.form.Hidden({
									name : 'uid'
								}), new Ext.form.Hidden({
									name : 'state'
								}), new Ext.form.Hidden({
									name : 'flowId'
								}), new Ext.form.Hidden({
									name : 'flowData'
								}), new Ext.form.Hidden({
									name : 'type',
									value : 'CommandEvent'
								}), new Ext.form.Hidden({
									name : 'bundle',
									value : 'dyn'
								}), {
							ref : 'idField',
							xtype : 'textfield',
							fieldLabel : '服务指令',
							name : 'id',
							blankText : '内容不能为空'
						}, {
							xtype : 'textfield',
							fieldLabel : '服务名称',
							name : 'name',
							allowBlank : true
						}, {
							xtype : 'textfield',
							fieldLabel : '服务描述',
							name : 'descriptionDetail',
							allowBlank : true
						}, new Ext.form.ComboBox({
									fieldLabel : '服务作用域',
									name : 'scope',
									triggerAction : 'all',
									editable : false,
									value : 'APPLICATION',
									hiddenName : 'scope',
									mode : 'local',
									store : new Ext.data.ArrayStore({
												fields : ['scope', 'displayText'],
												data : [['APPLICATION', '全局'], ['SESSION', '会话']]
											}),
									valueField : 'scope',
									displayField : 'displayText'
								})]
			});
	var _editFrame = new IFramePanel({
				tbar : [{
							text : '清空流程图',
							iconCls : 'icon-eraser',
							handler : function() {
								_editFrame.getFrameWin().reset()
							}
						}, {
							text : '删除选中组件',
							iconCls : 'icon-delete',
							handler : function() {
								_editFrame.getFrameWin().removeComponent()
							}
						}],
				region : 'center',
				frameURL : '../commons/scripts/raphael/dynFlow.html',
				style : 'border-width:0px;border-right-width:1px'
			});
	var _editWin = new Ext.Window({
				defaults : {
					border : false
				},
				title : '编辑服务',
				height : 700,
				width : 910,
				resizable : true,
				layout : 'border',
				items : [{
							style : 'border-width:0px;border-left-width:1px',
							region : 'east',
							minWidth : 150,
							split : true,
							collapseMode : 'mini',
							width : 200,
							layout : 'vbox',
							items : [_editForm, {
										layout : 'accordion',
										border : false,
										width : '100%',
										defaults : {
											border : false
										},
										flex : 1,
										items : [new BizToolBox({
															editFrame : _editFrame
														}), this.cmppropeditor = new BizCmpPropEditor({
															editFrame : _editFrame
														})]
									}]
						}, _editFrame],
				modal : true,
				plain : true,
				shim : true,
				closable : true,
				closeAction : 'hide',
				draggable : true,
				animCollapse : true,
				constrainHeader : true,
				shadow : false,
				listeners : {
					'beforehide' : function(p) {
						_editForm.getForm().reset();
						var frame = _editFrame.getEl().dom.getElementsByTagName('iframe')[0];
						frame.contentWindow.reset();
						if (self.cmppropeditor)
							self.cmppropeditor.reset();
					}
				},
				startEdit : function(idx) {
					this.show();
					var rec = dynStore.getAt(idx);
					if (!_editFrame.getEl() || !_editFrame.getFrameWin().loadPic) {
						window.__setFlowData = function() {
							_editFrame.getFrameWin().reset();
							_editFrame.getFrameWin().loadPic(rec.get('flowData'));
						};
					} else {
						window.__setFlowData = null;
						_editFrame.getFrameWin().reset();
						_editFrame.getFrameWin().loadPic(rec.get('flowData'));
					}
					_editForm.getForm().loadRecord(rec);
				}
			});
	this.editWin = _editWin;
	window.editWin__bizInfo = this.editWin;
	this.editFrame = _editFrame;
	this.editForm = _editForm;

	var dynView = new Ext.DataView(dynViewCfg);
	this.dynView = dynView;

	var config = {
		frame : false,
		border : false,
		layout : 'fit',
		autoScroll : true,
		bodyStyle : 'padding:3px',
		tbar : [{
					text : '新增服务',
					iconCls : 'icon-add',
					handler : function() {
						_editWin.show();
					}
				}, '-', '颜色说明:' + _getColorRef('rgb(255,255,204)', '指令服务') + _getColorRef('#F5B6FC', '事件服务') + _getColorRef('#FB8841', '文本服务') + _getColorRef('#CCC', '钝化服务')],
		items : [{
					xtype : 'fieldset',
					style : 'background-color:#BDE1FF',
					title : '动态服务',
					collapsible : true,
					autoHeight : true,
					items : [dynView]
				}, {
					xtype : 'fieldset',
					style : 'background-color:#CAFFE4',
					title : '定制服务',
					collapsible : true,
					autoHeight : true,
					items : [new Ext.DataView(devViewCfg)]
				}, {
					xtype : 'fieldset',
					style : 'background-color:#EEEEEE',
					title : '内置服务 ',
					collapsible : true,
					autoHeight : true,
					items : [new Ext.DataView(baseViewCfg)]
				}]
	};

	BizInfoPanel.superclass.constructor.call(this, config);
};

Ext.extend(BizInfoPanel, Ext.Panel, {
			destroy : function() {
				BizInfoPanel.superclass.destroy.call(this)
				if (this.cmppropeditor)
					delete this.cmppropeditor
			},
			saveHandler : function(btn) {
				var self = this;
				var f = this.editForm.getForm();
				if (!f.isValid()) {
					Ext.Msg.alert('提示', '您尚未正确完整的输入表单内容，请检查！');
					btn.enable();
					return false;
				}
				var vals = f.getValues();
				if (!vals.uid)
					delete vals.uid;
				if (!vals.flowId)
					delete vals.flowId;
				if (!vals.state)
					vals.state = 0;
				var flowData = this.editFrame.getEl().dom.getElementsByTagName('iframe')[0].contentWindow.saveElement();
				vals.flowData = flowData;
				Ext.Ajax.request({
							url : 'biz-info!add.action',
							params : {
								'data' : Ext.encode(vals)
							},
							success : function(resp) {
								Dashboard.setAlert('保存成功！');
								self.dynView.getStore().reload();
								btn.enable();
								self.editWin.hide();
							},
							failure : function(resp) {
								Ext.Msg.alert('错误', resp.responseText);
								btn.enable();
								self.editWin.hide();
							}
						});
			}
		});
var IFramePanel = Ext.extend(Ext.Panel, {
			border : false,
			initComponent : function() {
				this.html = "<iframe id=" + (this.fid = Ext.id())
						+ " height='100%' width='100%' frameborder='no' border='0' marginwidth='0' marginheight='0' scrolling='auto' allowtransparency='yes'></iframe>"
				IFramePanel.superclass.initComponent.call(this)
				this.on('afterrender', function() {
							if (this.frameURL) {
								this.setLocation(this.frameURL)
							}
						})
			},
			getFrame : function() {
				return Ext.getDom(this.fid);
			},
			getFrameWin : function() {
				return Ext.getDom(this.fid).contentWindow;
			},
			setLocation : function(url) {
				Ext.getDom(this.fid).src = url;
				this.frameURL = url
			}
		})
var BizToolBox = Ext.extend(Ext.Panel, {
			title : '工具箱',
			layout : {
				type : 'vbox',
				padding : '5',
				align : 'stretch'
			},
			defaults : {
				margins : '0 5 5 0',
				width : 100,
				cls : 'btn-align-left'
			},
			autoScroll : true,
			rootVisible : false,
			lines : false,
			addComponent : function(btn, event) {
				var data = []
				if (btn._type == 'codeRect') {
					data.push({
								key : 'code'
							})
				} else if (btn._type == 'branch') {
					for (var i = 1; i <= (btn.value ? btn.value : 2); i++) {
						data.push({
									key : 'condition' + i
								})
					}
				} else if (btn._type == 'procedureContainRect') {
					data.push({
								key : 'name'
							})
				}
				this.editFrame.getFrameWin().addComponent(btn._type, data)
			},
			editFrame : null,
			destroy : function() {
				BizToolBox.superclass.destroy.call(this)
				if (this.editFrame)
					delete this.editFrame
			},
			initComponent : function() {
				// init config
				var branchSelector = [];
				for (var i = 2; i <= 5; i++) {
					branchSelector.push({
								text : i + '个条件分支',
								_type : 'branch',
								value : i,
								handler : this.addComponent,
								scope : this
							})
				}
				var menu = [{
							text : '直线',
							iconCls : 'icon-flow-line',
							_type : 'simpleLine'
						}, {
							text : '折线（左右）',
							iconCls : 'icon-flow-line-lr',
							_type : 'complexLRLine'
						}, {
							text : '折线（上下）',
							iconCls : 'icon-flow-line-ud',
							_type : 'complexUDLine'
						}, {
							text : '判断条件',
							iconCls : 'icon-flow-branch',
							_type : 'branch',
							xtype : 'splitbutton',
							menu : {
								items : branchSelector
							}
						}, {
							text : '代码',
							iconCls : 'icon-flow-code',
							_type : 'codeRect'
						}, {
							text : '过程',
							iconCls : 'icon-flow-procedure',
							_type : 'procedureContainRect'
						}, {
							text : '可配置过程',
							iconCls : 'icon-flow-procedure-cfg',
							_type : 'procedureCfg'
						}]
				this.items = []
				for (var i = 0; i < menu.length; i++) {
					this.items.push(Ext.applyIf(menu[i], {
								xtype : 'button',
								handler : this.addComponent,
								scope : this
							}))
				}
				// init
				BizToolBox.superclass.initComponent.call(this);
			}
		})
var BizCmpPropEditor = Ext.extend(Ext.Panel, {
			title : '属性',
			editFrame : null,
			layout : 'border',
			defaults : {
				border : false
			},
			reset : function() {
				this.propsEditP.getStore().removeAll()
				this.descriptionPanel.body.dom.innerHTML = ''
			},
			destroy : function() {
				BizToolBox.superclass.destroy.call(this)
				if (this.editFrame)
					delete this.editFrame
			},
			setDescription : function(r) {
				var key = r.get('key'), value = r.get('value'), description = r.get('description')
				description = description || ''

				if (!description) {
					switch (this.editObj.type) {
						case 'codeRect' :
							description = 'The code snippet'
							break;
						case 'branch' :
							if (key == 'condition1')
								description = 'The condition expression("if")'
							else if (key == 'condition2') {
								if (value) {
									description = 'The condition expression("else if ")'
								} else {
									description = 'The condition expression("else")'
								}
							}
							break;
					}
				}
				this.descriptionPanel.body.dom.innerHTML = (String.format('<div style="font-weight:bold;font-size:14pt">{0}</div><div>{1}</div>', key, description))
			},
			initComponent : function() {
				this.descriptionPanel = new Ext.Panel({
							region : 'south',
							split : true,
							height : 100,
							style : 'border-top:1px solid #A3BAE9',
							bodyStyle : 'background:#eef;padding:5px',
							html : ''
						})
				this.propsEditP = this.createPropsEditP({
							region : 'center',
							style : 'border-bottom:1px solid #A3BAE9'
						});
				this.items = [this.descriptionPanel, this.propsEditP]
				BizCmpPropEditor.superclass.initComponent.call(this);

				window.editComponent = this.editComponent.dg(this)
				this.propsEditP.on('afteredit', function(e) {
							var data = []
							e.grid.store.each(function(r) {
										data.push(r.data)
									})
							this.editFrame.getFrameWin().updateComponent(this.editObj.id, data);
						}, this)
				this.propsEditP.getSelectionModel().on('rowselect', function(sm, row, r) {
							this.setDescription(r)
						}, this)
			},
			editComponent : function(id, type, data) {
				this.expand();
				this.editObj = {
					id : id,
					type : type,
					data : data
				}
				this.propsEditP.store.loadData(data)
				if (this.propsEditP.store.getCount() > 0)
					this.propsEditP.getSelectionModel().selectRow(0)
				if (this.editFrame) {
					this.editFrame.getFrameWin().focus()
				}
			},
			createPropsEditP : function(cfg) {
				var grid = new Ext.grid.EditorGridPanel(Ext.apply({
							clicksToEdit : 1,
							hideHeaders : true,
							sm : new Ext.grid.RowSelectionModel(),
							store : new Ext.data.JsonStore({
										root : '',
										fields : [{
													name : 'type'
												}, {
													name : 'key'
												}, {
													name : 'value'
												}]
									}),
							columns : [{
										id : 'key',
										dataIndex : 'key'
									}, {
										id : 'value',
										dataIndex : 'value',
										editor : {
											alignment : "tr-tr",
											autoSize : 'none',
											field : Ext.create({
														height : 100,
														width : 150,
														xtype : 'textarea'
													})
										}
									}],
							autoExpandColumn : 'value'
						}, cfg));
				return grid;
			}
		})
