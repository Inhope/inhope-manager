BizInterceptorPanel = function() {
	var self = this;

	var fields = ['id', 'uid', 'name', 'priority', 'type', 'detail', 'bundle', 'state', 'data'];
	var iStore = this.iStore = new Ext.data.JsonStore({
		  url : 'biz-interceptor!list.action?type=0',
		  root : 'data',
		  idProperty : 'uid',
		  autoLoad : true,
		  fields : fields
	  });
	var hiStore = this.hiStore = new Ext.data.JsonStore({
		  url : 'biz-interceptor!list.action?type=1',
		  root : 'data',
		  idProperty : 'uid',
		  autoLoad : true,
		  fields : fields
	  });
	var tpl = new Ext.XTemplate('<tpl for=".">', '<div class="bizinfo" title="{detail}" style="float:left;margin:3px;width:195px;" uid="{uid}" state="{state}">',
	  '<table border="1" style="', '<tpl if="bundle==\'base\'">', 'background-color:rgb(255,255,204);', '</tpl>', '<tpl if="bundle==\'dev\'">', 'background-color:#F5B6FC;', '</tpl>',
	  '<tpl if="bundle==\'dyn\'">', 'background-color:#FB8841;', '</tpl>', '<tpl if="this.isDisabled(state)">', 'background-color:#CCC;', '</tpl>',
	  'border:1px solid rgb(153,0,51);border-collapse:collapse;" width="100%"><tr><td style="padding:2px;', '<tpl if="this.isDisabled(state)">', 'text-decoration:line-through;',
	  '</tpl>', '" align="center">{[values.name?values.name:values.id]}' + '<tpl if="bundle==\'dyn\'">', '<span class="icon-table-edit"></span>', '</tpl>',
	  '</td></tr><tr><td style="padding:2px;">', '优先级: {priority}<br/>描述:{detail}', '</td></tr></table></div>', '</tpl>', '<div style="clear:both"></div>', {
		  isDisabled : function(state) {
			  return (state & 1) == 1;
		  }
	  });

	var _getColorRef = function(color, ref) {
		return '<span style="background-color:' + color + ';width:8px;height:8px;margin-left:8px;border:1px solid rgb(153,0,51);">&nbsp;&nbsp;&nbsp;</span> ' + ref;
	};

	var menu = new Ext.menu.Menu({
		  items : [{
			    text : '编辑',
			    ref : 'edit',
			    iconCls : 'icon-edit',
			    handler : function() {
				    var idx = menu.currentItem.getAttribute('index');
				    _editWin.startEdit(menu.currentView, idx);
			    }
		    }, {
			    text : '激活',
			    ref : 'activate',
			    iconCls : 'icon-hot-question',
			    handler : function() {
				    Ext.Msg.confirm('提示', '确认激活拦截器？', function(btn) {
					      if (btn == 'yes') {
						      Ext.Ajax.request({
							        url : 'biz-interceptor!activate.action',
							        params : {
								        uid : menu.currentItem.getAttribute('uid')
							        },
							        success : function() {
								        Dashboard.setAlert('激活拦截器成功。');
								        menu.currentView.getStore().reload();
							        }
						        });
					      }
				      });
			    }
		    }, {
			    text : '禁用',
			    ref : 'deactivate',
			    iconCls : 'icon-hot-question-auto',
			    handler : function() {
				    Ext.Msg.confirm('提示', '确认禁用拦截器？', function(btn) {
					      if (btn == 'yes') {
						      Ext.Ajax.request({
							        url : 'biz-interceptor!deactivate.action',
							        params : {
								        uid : menu.currentItem.getAttribute('uid')
							        },
							        success : function() {
								        Dashboard.setAlert('禁用拦截器成功。');
								        menu.currentView.getStore().reload();
							        }
						        });
					      }
				      });
			    }
		    }, {
			    text : '删除',
			    ref : '_delete',
			    iconCls : 'icon-delete',
			    handler : function() {
				    Ext.Msg.confirm('提示', '确认删除拦截器？', function(btn) {
					      if (btn == 'yes') {
						      Ext.Ajax.request({
							        url : 'biz-interceptor!remove.action',
							        params : {
								        uid : menu.currentItem.getAttribute('uid')
							        },
							        success : function() {
								        Dashboard.setAlert('删除拦截器成功。');
								        menu.currentView.getStore().reload();
							        }
						        });
					      }
				      });
			    }
		    }]
	  });

	var viewCfg = {
		tpl : tpl,
		itemSelector : 'div.bizinfo',
		autoHeight : true,
		prepareData : function(data) {
			return data;
		},
		listeners : {
			contextmenu : function(dataview, index, item, e) {
				e.preventDefault();
				if (!item)
					return;
				item.setAttribute('index', index);
				menu.currentItem = item;
				menu.currentView = dataview;
				menu.activate.setDisabled(false);
				menu.deactivate.setDisabled(false);
				if (item.getAttribute('state') == 1)
					menu.deactivate.setDisabled(true);
				else
					menu.activate.setDisabled(true);
				var dyn = dataview.store.getAt(index).get('bundle') == 'dyn'
				menu._delete.setDisabled(!dyn);
				menu.edit.setDisabled(!dyn);
				menu.showAt(e.getXY());
			},
			click : function(view, i, node, e) {
				if (e.getTarget('.icon-table-edit')) {
					_editWin.startEdit(view, i)
				}
			}.dg(this)
		}
	};
	var iViewCfg = {
		store : iStore,
		emptyText : '尚无定制拦截器',
		bundle : 'base'
	}, hiViewCfg = {
		store : hiStore,
		emptyText : '尚无定制拦截器',
		bundle : 'dev'
	};
	Ext.apply(iViewCfg, viewCfg);
	Ext.apply(hiViewCfg, viewCfg);

	var _editForm = new Ext.form.FormPanel({
		  width : '100%',
		  height : '100%',
		  frame : false,
		  border : false,
		  labelWidth : 75,
		  waitMsgTarget : true,
		  tbar : [{
			    text : '保存',
			    iconCls : 'icon-save',
			    handler : function() {
				    this.disable();
				    self.saveHandler(this);
			    }
		    }],
		  bodyStyle : 'padding : 3px 10px; background-color:' + sys_bgcolor,
		  defaults : {
			  anchor : '98%',
			  allowBlank : false
		  },
		  items : [new Ext.form.Hidden({
			      name : 'uid'
		      }), new Ext.form.Hidden({
			      name : 'state'
		      }), new Ext.form.Hidden({
			      name : 'bundle',
			      value : 'dyn'
		      }), {
			    xtype : 'textfield',
			    fieldLabel : '名称',
			    name : 'name',
			    allowBlank : false
		    }, {
			    xtype : 'textfield',
			    fieldLabel : '优先级',
			    name : 'priority',
			    allowBlank : true
		    }, {
			    xtype : 'textfield',
			    fieldLabel : '描述',
			    name : 'detail',
			    allowBlank : true
		    }, new Ext.form.ComboBox({
			      fieldLabel : '类型',
			      name : 'type',
			      triggerAction : 'all',
			      editable : false,
			      hiddenName : 'type',
			      mode : 'local',
			      store : new Ext.data.ArrayStore({
				        fields : ['val', 'txt'],
				        data : [[0, '请求拦截器'], [1, '服务拦截器']]
			        }),
			      valueField : 'val',
			      displayField : 'txt',
			      allowBlank : false
		      }), {
			    xtype : 'textarea',
			    fieldLabel : '业务代码',
			    name : 'data',
			    allowBlank : false,
			    anchor : '100% -110'
		    }]
	  });
	var _editWin = new Ext.Window({
		  defaults : {
			  border : false
		  },
		  title : '编辑拦截器',
		  height : 500,
		  width : 910,
		  resizable : true,
		  layout : 'fit',
		  items : _editForm,
		  modal : true,
		  plain : true,
		  shim : true,
		  closable : true,
		  closeAction : 'hide',
		  draggable : true,
		  animCollapse : true,
		  constrainHeader : true,
		  shadow : false,
		  startEdit : function(view, i) {
			  this.show();
			  var rec = view.store.getAt(i);
			  if (rec) {
				  _editForm.getForm().loadRecord(rec);
				  _editForm.find('name', 'type')[0].setReadOnly(true)
			  }
		  },
		  listeners : {
			  beforehide : function() {
				  _editForm.find('name', 'type')[0].setReadOnly(false)
				  _editForm.getForm().reset();
			  }
		  }
	  });
	this.editWin = _editWin;
	this.editForm = _editForm;

	var config = {
		frame : false,
		border : false,
		layout : 'fit',
		autoScroll : true,
		bodyStyle : 'padding:3px',
		tbar : [{
			  text : '新增拦截器',
			  iconCls : 'icon-add',
			  handler : function() {
				  _editWin.show();
			  }
		  }, '-', '颜色说明:' + _getColorRef('rgb(255,255,204)', '内置拦截器') + _getColorRef('#F5B6FC', '定制拦截器') + _getColorRef('#FB8841', '动态拦截器') + _getColorRef('#CCC', '已禁用的拦截器')],
		items : [{
			  xtype : 'fieldset',
			  style : 'background-color:#BDE1FF',
			  title : '请求拦截器',
			  collapsible : true,
			  autoHeight : true,
			  items : [new Ext.DataView(iViewCfg)]
		  }, {
			  xtype : 'fieldset',
			  style : 'background-color:#CAFFE4',
			  title : '服务拦截器',
			  collapsible : true,
			  autoHeight : true,
			  items : [new Ext.DataView(hiViewCfg)]
		  }]
	};

	BizInterceptorPanel.superclass.constructor.call(this, config);
};

Ext.extend(BizInterceptorPanel, Ext.Panel, {
	  destroy : function() {
		  BizInterceptorPanel.superclass.destroy.call(this)
		  if (this.cmppropeditor)
			  delete this.cmppropeditor
	  },
	  saveHandler : function(btn) {
		  var self = this;
		  var f = this.editForm.getForm();
		  if (!f.isValid()) {
			  Ext.Msg.alert('提示', '您尚未正确完整的输入表单内容，请检查！');
			  btn.enable();
			  return false;
		  }
		  var vals = f.getValues();
		  if (!vals.uid)
			  delete vals.uid;
		  if (!vals.state)
			  vals.state = 0;
		  ux.util.resetEmptyString(vals);
		  Ext.Ajax.request({
			    url : 'biz-interceptor!add.action',
			    params : {
				    'data' : Ext.encode(vals)
			    },
			    success : function(resp) {
			    	this.iStore.reload()
			    	this.hiStore.reload()
				    Dashboard.setAlert('保存成功！');
				    btn.enable();
				    self[vals.bundle == 'dyn' ? hiStore : iStore].reload();
				    self.editWin.hide();
			    },
			    failure : function(resp) {
				    Ext.Msg.alert('错误', resp.responseText);
				    btn.enable();
				    self.editWin.hide();
			    },
			    scope:this
		    });
	  }
  });
