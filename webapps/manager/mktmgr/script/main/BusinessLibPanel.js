BusinessLibPanel = function() {
	var self = this;
	this.FileRecord = Ext.data.Record.create([ 'id', 'name', 'lastModified',
			'length', 'createTime' ]);
	var _filePanel = new Ext.grid.GridPanel({
		region : 'center',
		columnLines : true,
		style : 'border-left: 1px solid ' + sys_bdcolor,
		border : false,
		title : '文件列表',
		store : new Ext.data.Store({
			proxy : new Ext.data.HttpProxy({
				url : 'business-lib!listFile.action'
			}),
			reader : new Ext.data.JsonReader({
				idProperty : 'id',
				root : 'data',
				fields : self.FileRecord
			})
		}),
		tbar : [ {
			text : '新增',
			iconCls : 'icon-add',
			handler : function() {
				self.addFile();
			}
		}, {
			text : '删除',
			iconCls : 'icon-delete',
			handler : function() {
				self.deleteFile();
			}
		}, {
			text : '刷新',
			iconCls : 'icon-refresh',
			handler : function() {
				_filePanel.getStore().reload();
			}
		} ],
		sm : new Ext.grid.RowSelectionModel({
			singleSelect : true
		}),
		loadMask : true,
		autoExpandColumn : 'libNameCol',
		colModel : new Ext.grid.ColumnModel({
			columns : [ new Ext.grid.RowNumberer(), {
				id : 'libNameCol',
				header : '文件名',
				width : 100,
				dataIndex : 'name'
			}, {
				header : '上传时间',
				width : 130,
				dataIndex : 'createTime'
			}, {
				header : '文件大小',
				width : 100,
				dataIndex : 'length',
				renderer : function(v) {
					return v / 1000 + ' KB';
				}
			} ]
		})
	});
	this.filePanel = _filePanel;
	_filePanel.getStore().on('load', function() {
		this.getSelectionModel().selectRow(0);
	}, _filePanel)

	var formCfg = {
		frame : false,
		border : false,
		labelWidth : 70,
		autoHeight : true,
		waitMsgTarget : true,
		bodyStyle : 'padding : 3px 10px; background-color:' + sys_bgcolor
	};
	var winCfg = {
		width : 425,
		defaults : {
			border : false
		},
		modal : true,
		plain : true,
		shim : true,
		closable : true,
		closeAction : 'hide',
		collapsible : true,
		resizable : false,
		draggable : true,
		animCollapse : true,
		constrainHeader : true,
		listeners : {
			'beforehide' : function(p) {
				p.formPanel.getForm().reset();
			}
		}
	};

	var _fileForm = new Ext.FormPanel({
		fileUpload : true,
		frame : false,
		border : false,
		labelWidth : 70,
		autoHeight : true,
		waitMsgTarget : true,
		bodyStyle : 'padding : 3px 10px; background-color:' + sys_bgcolor,
		defaults : {
			anchor : '95%',
			allowBlank : false,
			msgTarget : 'side'
		},
		items : [ {
			xtype : 'fileuploadfield',
			emptyText : '选择文件',
			fieldLabel : '文件路径',
			name : 'lib',
			buttonText : '',
			buttonCfg : {
				iconCls : 'icon-upload-lib'
			}
		} ],
		tbar : [ {
			text : '保存',
			iconCls : 'icon-add',
			handler : function() {
				var btn = this;
				btn.disable();
				if (_fileForm.getForm().isValid()) {
					_fileForm.getForm().submit(
							{
								url : 'business-lib!save.action',
								params : {
									path : self.getPath(_listDirPanel
											.getSelectionModel()
											.getSelectedNode())
								},
								waitMsg : '正在上传文件...',
								success : function(form, act) {
									Dashboard.setAlert('保存成功！');
									btn.enable();
									_fileWin.hide();
									_filePanel.getStore().reload();
								},
								failure : function(form, act) {
									Ext.Msg.alert('错误', act.result.message);
									btn.enable();
								}
							});
				} else {
					Ext.Msg.alert('错误', '您尚未正确完整的输入表单内容，请检查！', function() {
						btn.enable()
					});
				}
			}
		} ]
	});
	var _fileWin = new Ext.Window(Ext.apply(winCfg, {
		title : '上传扩展包',
		shadow : false,
		items : [ _fileForm ]
	}));
	_fileWin.formPanel = _fileForm;
	this.fileWin = _fileWin;

	var _listDirPanel = new Ext.tree.TreePanel({
		region : 'west',
		lines : true,
		split : true,
		width : 280,
		collapsible : true,
		collapseMode : 'mini',
		title : '目录结构',
		dataUrl : 'business-lib!listDir.action',
		style : 'border-right: 1px solid ' + sys_bdcolor,
		border : false,
		root : {
			id : '0',
			text : 'WebRoot'
		// lazyLoad : true
		}
	});
	_listDirPanel.getLoader().on('load', function(_this, node, resp) {
		if (resp.responseText) {
			var respObj = Ext.decode(resp.responseText);
			if (respObj.message && respObj.message == 'NA')
				self.getEl().mask('不可操作，请先在机器人前端进行路径配置。');
		}
	});
	this.getPath = function() {
		var node = this.listDirPanel.getSelectionModel().getSelectedNode();
		if (!node)
			return null;
		var path = node.getPath('text');
		path = path.replace(_listDirPanel.getRootNode().text, '');
		if (path.length > 0)
			path = path.substring(1, path.length);
		return path;
	}
	_listDirPanel.getLoader().on('beforeload', function(_this, node, resp) {
		if (!node.isSelected())
			node.select();
		var path = self.getPath();
		_this.baseParams.path = path;
	});
	_listDirPanel.getSelectionModel().on('selectionchange', function(n) {
		if (!n.selNode)
			return false;
		_filePanel.getStore().load({
			params : {
				path : self.getPath()
			}
		});
	}, _listDirPanel);
	this.listDirPanel = _listDirPanel;

	var _dirForm = new Ext.form.FormPanel(Ext.apply(formCfg, {
		items : [ {
			xtype : 'textfield',
			fieldLabel : '目录名称',
			name : 'libFileName',
			allowBlank : false,
			blankText : '请输入目录名称',
			anchor : '98%'
		} ],
		tbar : [ {
			text : '保存',
			iconCls : 'icon-save',
			handler : function() {
				this.disable();
				self.saveDir(this);
			}
		} ]
	}));
	var _dirWin = new Ext.Window(Ext.apply(winCfg, {
		title : '新建目录',
		shadow : false,
		items : [ _dirForm ]
	}));
	_dirWin.formPanel = _dirForm;
	this.dirWin = _dirWin;

	var listMenu = new Ext.menu.Menu({
		items : [ {
			text : '刷新',
			iconCls : 'icon-refresh',
			width : 50,
			handler : function() {
				listMenu.currentNode.reload();
			}
		}, '-', {
			ref : 'newItem',
			text : '新建目录',
			iconCls : 'icon-add',
			width : 50,
			handler : function() {
				self.addDir(this);
			}
		}, {
			ref : 'delFunc',
			text : '删除目录',
			iconCls : 'icon-delete',
			width : 50,
			handler : function() {
				self.deleteDir(self.listMenu.currentNode);
			}
		} ]
	});
	this.listMenu = listMenu;
	_listDirPanel.on('contextmenu', function(node, event) {
		var menu = listMenu
		event.preventDefault();
		menu.currentNode = node;
		node.select();
		menu.showAt(event.getXY());
	});

	var config = {
		layout : 'border',
		border : false,
		items : [ _listDirPanel, _filePanel ]
	};
	BusinessLibPanel.superclass.constructor.call(this, config);

}

Ext.extend(BusinessLibPanel, Ext.Panel, {
	init : function() {
		var self = this;
		this.listDirPanel.getRootNode().expand(false, true, function(n) {
			if (!n.isSelected()) {
				n.select();
				self.listDirPanel.fireEvent('click', n);
			}
		});
	},
	addDir : function() {
		this.dirWin.show();
	},
	addFile : function() {
		this.fileWin.show();
	},
	deleteFile : function() {
		var self = this;
		var rec = self.filePanel.getSelectionModel().getSelected();
		if (!rec) {
			Ext.Msg.alert('提示', '请先选择一个文件！');
			return false;
		}
		Ext.Msg.confirm('提示', '确认删除？', function(btn) {
			if (btn == 'yes') {
				Ext.Ajax.request({
					url : 'business-lib!deleteFile.action',
					params : {
						'path' : self.getPath(),
						'libFileName' : rec.get('name')
					},
					success : function(resp) {
						self.filePanel.getStore().reload();
					},
					failure : function(resp) {
						Ext.Msg.alert('错误', resp.responseText);
					}
				});
			}
		});
	},
	saveDir : function(btn) {
		var self = this;
		var f = this.dirWin.formPanel.getForm();
		if (!f.isValid()) {
			Ext.Msg.alert('提示', '您尚未正确完整的输入表单内容，请检查！');
			btn.enable();
			return false;
		}
		var vals = f.getFieldValues();
		Ext.Ajax.request({
			url : 'business-lib!save.action',
			params : {
				path : this.getPath(),
				libFileName : vals.libFileName
			},
			success : function(resp) {
				var parentNode = self.listDirPanel.getSelectionModel()
						.getSelectedNode();
				if (parentNode) {
					var n = new Ext.tree.AsyncTreeNode({
						id : vals.libFileName,
						text : vals.libFileName,
						leaf : false
					})
					parentNode.appendChild(n);
					if (!parentNode.isExpanded())
						parentNode.expand(false, true);
				}
				btn.enable();
				self.dirWin.hide();
			},
			failure : function(resp) {
				Ext.Msg.alert('错误', resp.responseText);
				btn.enable();
				self.dirWin.hide();
			}
		});
	},
	deleteDir : function(n) {
		var self = this;
		Ext.Ajax.request({
			url : 'business-lib!list.action',
			params : {
				path : this.getPath()
			},
			success : function(resp) {
				var respObj = Ext.decode(resp.responseText);
				if (respObj.message == 'false') {
					Ext.Msg.alert('提示', '不能删除尚有内容的目录！');
					return false;
				} else {
					Ext.Msg.confirm('提示', '确认删除此目录？', function(btn) {
						if (btn == 'yes') {
							Ext.Ajax.request({
								url : 'business-lib!deleteDir.action',
								params : {
									path : self.getPath()
								},
								success : function(resp) {
									n.parentNode.removeChild(n);
								},
								failure : function(resp) {
									Ext.Msg.alert('错误', resp.responseText);
								}
							});
						}
					});
				}
			},
			failure : function(resp) {
				Ext.Msg.alert('错误', resp.responseText);
			}
		});
	}
});