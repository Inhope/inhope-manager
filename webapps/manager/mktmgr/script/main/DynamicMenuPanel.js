DynamicMenuPanel = function() {

	var __dynmenu__init = http_get('dynamic-menu!init.action');
	var __scan_enabled = false;
	if (__dynmenu__init) {
		__dynmenu_init_obj  = Ext.decode(__dynmenu__init);
		__scan_enabled = __dynmenu_init_obj['menuScanInterval'] != null;
	}
	
	this.importType = '';
	var self = this;
	this.propsHolder = {
		TYPE_TEXT_MENU : 1,
		TYPE_CMD_MENU : 2,
		TYPE_NODE_MENU : 3,
		activeArr : [[1, '启用'], [0, '禁用']]
	};
	this.MenuRecord = Ext.data.Record.create(['id', 'key', 'parentId', 'name',
			'header', 'content', 'footer', 'command', 'priority', 'enabled',
			'createTime', 'bh', 'status']);

	var _fileForm=new Ext.FormPanel({
					frame : true,
					border : false,
					autoHeight : true,
					waitMsgTarget : true,
					defaults : {
						bodyStyle : 'padding:10px'
					},
					margins : '0 0 0 0',
					labelAlign : "left",
					labelAlign : 'top',
					fileUpload : true,
					items : [{
								xtype : 'fieldset',
								ref : 'fileFieldset',
								title : '选择文件',
								autoHeight : true,
								items : [{
											name : 'file',
											xtype : "textfield",
											fieldLabel : '文件',
											inputType : 'file',
											anchor : '96%',
											allowBlank : false
										}, {
											id : 'dynmicMenuImportIsDelete',
											hideLabel : true,
											boxLabel : '删除全部',
											name : 'isDelete',
											xtype : "checkbox",
											inputValue : true,
											checked : true
										}]
							}]
				});
	var _importPanel = new Ext.Panel({
		layout : "fit",
		layoutConfig : {
			animate : true
		},
		items : [_fileForm],
		buttons : [{
			text : "开始导入",
			handler : function() {
				var fileForm = _fileForm.getForm();
				if (!fileForm.isValid()) {
					Ext.Msg.alert("错误提示", "表单未填写完整");
				} else {
					this.disable();
					btn = this;
					// 开始导入
					var isDelete = Ext.getCmp("dynmicMenuImportIsDelete").getValue();
					importType = self.importType;
					fileForm.submit({
						params : {
							'importType' : importType,
							'isDelete' : isDelete
						},
						url : 'dynamic-menu!doImport.action',
						waitMsg : '正在导入菜单内容...',
						success : function(form, action) {
							btn.enable();
							var result = action.result.data;
							var timer = new ProgressTimer({
								initData : result,
								progressId : 'importDynamicMenuStatus',
								boxConfig : {
									title : '正在导入动态菜单...'
								},
								finish : function(p, response) {
									if (p.currentCount == -1) {
										Ext.Msg.hide();
										var message = Ext
												.decode(response.responseText).message;
										Ext.Msg.alert("导入失败", message);
									} else if (p.currentCount == -2) {
										// 下载验证失败的信息
										if (!self.downloadIFrame) {
											self.downloadIFrame = self
													.getEl()
													.createChild({
														tag : 'iframe',
														style : 'display:none;'
													});
										}
										self.downloadIFrame.dom.src = "dynamic-menu!downExceptionFile.action?&_t="
												+ new Date().getTime();
									}
									self.listPanel.getStore().reload();
								}
							});
							timer.start();
						},
						failure : function(form, action) {
							btn.enable();
							var result = action.result;
							Ext.Msg.alert('失败', action.result.message);
						}
					});
				}
			}
		}, {
			text : "关闭",
			handler : function() {
				this.hide();
			}
		}]
	});
	var DynamicMenuImportWin =new Ext.Window({
		title : '动态菜单导入',
		width : 400,
		autoHeight : true,
		defaults : {
			border : false
		},
		plain : true,// 方角 默认
		modal : true,
		plain : true,
		shim : true,
		closeAction : 'hide',
		closable : true, // 关闭
		constrainHeader : true,
		items : [_importPanel]
	});
	this.tbarEx = new Ext.Toolbar({
		  items : [{
			    xtype : 'textfield',
			    ref:'searchName',
			    emptyText : '输入名称搜索...',
			    listeners : {
				    specialkey : function(field, e) {
					    if (e.getKey() == Ext.EventObject.ENTER) {
						    self.searchMenuname();
					    }
				    }
			    }
		    }, {
			    text : '搜索',
			    xtype : 'button',
			    iconCls : 'icon-search',
			    handler : function() {
				    self.searchMenuname();
			    }
		    }, {
		    	xtype: "checkbox",        
		    	ref: "onlyRoot",     
	    	    boxLabel: "只搜索列表名称",      
	    	    inputValue: "true",        
	    	    uncheckedValue: "false",    
	    	    checked: false              
		    }]
	  });
    this.menuRootName;
    this.prevSearchNameIdx = -1;
    this.queryAllMatchName = [];
	var _listPanel = new Ext.grid.GridPanel({
		id : '_menuListPanel',
		region : 'west',
		width : 350,
		split : true,
		columnLines : true,
		collapsible : true,
		collapseMode : 'mini',
		style : 'border-right: 1px solid ' + sys_bdcolor,
		border : false,
		title : '菜单列表',
		store : new Ext.data.Store({
					proxy : new Ext.data.HttpProxy({
								url : 'dynamic-menu!list.action'
							}),
					reader : new Ext.data.JsonReader({
								idProperty : 'id',
								root : 'data',
								fields : self.MenuRecord
							})
				}),
		tbar : [{
					text : '新增',
					iconCls : 'icon-add',
					handler : function() {
						self.addMenu();
					}
				}, {
					text : '编辑',
					iconCls : 'icon-edit',
					handler : function() {
						self.modifyMenu();
					}
				}, {
					text : '删除',
					iconCls : 'icon-delete',
					handler : function() {
						self.deleteMenu(self.listPanel);
					}
				}, {
					text : '刷新',
					iconCls : 'icon-refresh',
					handler : function() {
						self.listPanel.getStore().reload();
					}
				}, {
					iconCls : 'icon-ok',
					text : '应用',
					handler : function() {
						Ext.Msg.confirm('提示', '确定应用？', function(btn) {
									if (btn == 'yes') {
										Ext.Ajax.request({
													url : 'dynamic-menu!apply.action',
													success : function(resp) {
														Dashboard
																.setAlert('应用成功！');
													},
													failure : function(resp) {
														Ext.Msg
																.alert(
																		'错误',
																		resp.responseText);
													}
												});
									}
								});
					}
				}, {
					text : '导入',
					iconCls : 'icon-ontology-import',
					handler : function() {
						Ext.Msg.show({
							title : '导入文件类型',
							msg : '您选择要导入文件格式',
							buttons : {
								no : 'Excel07',
								yes : 'XML',
								cancel : true
							},
							fn : function(choice) {
								if (choice == 'cancel')
									return
								choice == 'yes' ? self.importType = 'xml' : '';
								choice == 'no' ? self.importType = 'excel' : '';
								if (!this.importwin) {
									this.importwin = DynamicMenuImportWin;
								}
								var checkBoxImport = Ext
										.getCmp("dynmicMenuImportIsDelete");
								if (choice == 'yes')
									checkBoxImport.disable();
								else
									checkBoxImport.enable();
								this.importwin.show();
							}
						});
					}.dg(this)
				}, {
					text : '导出',
					iconCls : 'icon-ontology-export',
					handler : function() {
						var self = this;
						var exportType = '';
						Ext.Msg.show({
							title : '导出文件类型',
							msg : '您选择要导出文件格式',
							buttons : {
								no : 'Excel07',
								yes : 'XML',
								cancel : true
							},
							fn : function(choice) {
								if (choice == 'cancel')
									return
								choice == 'yes' ? exportType = 'xml' : '';
								choice == 'no' ? exportType = 'excel' : '';
								if (choice == 'no') {
									Ext.Ajax.request({
										url : 'dynamic-menu!doExport.action',
										params : {
											'exportType' : exportType
										},
										success : function(resp) {
											var obj = Ext
													.decode(resp.responseText);
											var result = obj.data;
											var timer = new ProgressTimer({
												initData : result,
												progressId : 'exportDynamicMenuStatus',
												boxConfig : {
													title : '正在导出动态菜单...'
												},
												finish : function(p, response) {
													if (p.currentCount == -1) {
														Ext.Msg.hide();
														var message = Ext
																.decode(response.responseText).message;
														Ext.Msg.alert("导出失败",
																message);
													} else {
														// 下载验证失败的信息
														if (!self.downloadIFrame) {
															self.downloadIFrame = self
																	.getEl()
																	.createChild(
																			{
																				tag : 'iframe',
																				style : 'display:none;'
																			});
														}
														self.downloadIFrame.dom.src = "dynamic-menu!downExportExcelFile.action?&_t="
																+ new Date()
																		.getTime();
													}
												}
											});
											timer.start();
										},
										failure : function(resp) {
											Ext.Msg.alert('错误',
													resp.responseText);
											btn.enable();
											win.hide();
										}
									});

								} else if (choice == 'yes') {
									if (!this.downloadIFrame) {
										this.downloadIFrame = self.getEl()
												.createChild({
															tag : 'iframe',
															style : 'display:none;'
														});
									}
									this.downloadIFrame.dom.src = 'dynamic-menu!doExport.action?exportType='
											+ exportType
											+ '&_t='
											+ new Date().getTime();
								}
							}
						});
					}.dg(this)
				}],
		sm : new Ext.grid.RowSelectionModel({
					singleSelect : true
				}),
		loadMask : true,
		autoExpandColumn : 'nameCol',
		colModel : new Ext.grid.ColumnModel({
					columns : [new Ext.grid.RowNumberer(), {
								id : 'nameCol',
								header : '名称',
								width : 100,
								dataIndex : 'name',
								renderer : function(v, meta, rec) {
									if (!rec.get('enabled'))
										return '<font color="gray">' + v
												+ '</font/>';
									else if (rec.get('status') == 1)
										return '<font color="red">' + v
												+ '</font/>';
									return v;
								}
							}, {
								header : '触发键',
								width : 80,
								dataIndex : 'key'
							}]
				})
	});
	
	_listPanel.addListener('render', function() {
		  self.tbarEx.render(_listPanel.tbar);
	  }, _listPanel);

	var formCfg = {
		frame : false,
		border : false,
		labelWidth : 70,
		autoHeight : true,
		waitMsgTarget : true,
		bodyStyle : 'padding : 3px 10px; background-color:' + sys_bgcolor
	};
	var winCfg = {
		width : 425,
		defaults : {
			border : false
		},
		modal : true,
		plain : true,
		shim : true,
		closable : true,
		closeAction : 'hide',
		collapsible : true,
		resizable : false,
		draggable : true,
		animCollapse : true,
		constrainHeader : true,
		listeners : {
			'beforehide' : function(p) {
				p.formPanel.getForm().reset();
			}
		}
	};

	var _listFormPanel = new Ext.form.FormPanel(Ext.apply(formCfg, {
		items : [new Ext.form.Hidden({
							name : 'id'
						}), {
					xtype : 'textfield',
					fieldLabel : '菜单名称',
					allowBlank : false,
					name : 'name',
					blankText : '请输入菜单名称',
					anchor : '98%'
				}, {
					xtype : 'textarea',
					fieldLabel : '菜单标题',
					name : 'header',
					anchor : '98%'
				}, {
					xtype : 'textarea',
					fieldLabel : '菜单脚注',
					name : 'footer',
					anchor : '98%'
				}, {
					xtype : 'textfield',
					fieldLabel : '触发键',
					allowBlank : false,
					name : 'key',
					blankText : '请输入触发键',
					anchor : '98%'
				}, {
					xtype : 'trigger',
					fieldLabel : '指令',
					name : 'command',
					anchor : '98%',
					hideMode : 'display',
					allowBlank : true,
					onTriggerClick : function() {
						var p = self;
						if (!p.__cmdChooser) {
							p.__cmdChooser = new CommandChooserMenu();
							p.__cmdChooser.init();
							p.__cmdChooser.on("commandChanged", function() {
										this.setValue(p.__cmdChooser
												.getCommand());
									}, this);
							p.__cmdChooser.on("initcomplete", function() {
										p.__cmdChooser.setCommand(this
												.getValue());
										p.__cmdChooser.show(this.getEl(), 'tl');
										p.__cmdChooser.initcomplete = true;
									}, this);
						}
						if (p.__cmdChooser.initcomplete) {
							p.__cmdChooser.setCommand(this.getValue());
							p.__cmdChooser.show(this.getEl(), 'tl');
						}
					}
				}, {
					xtype : 'combo',
					fieldLabel : '是否启用',
					store : new Ext.data.ArrayStore({
								autoDestroy : true,
								data : this.propsHolder.activeArr,
								fields : ['val', 'name']
							}),
					mode : 'local',
					name : 'enabled',
					allowBlank : false,
					triggerAction : 'all',
					editable : false,
					value : 1,
					valueField : 'val',
					displayField : 'name'
				}],
		tbar : [{
					text : '保存',
					iconCls : 'icon-save',
					handler : function() {
						this.disable();
						self.saveMenu(this, self.listWin, function(resp, isAdd,
										btn) {
									self.listPanel.getStore().reload();
									btn.enable();
									this.listWin.hide();
								});
					}
				}]
	}));

	var _listWin = new Ext.Window(Ext.apply(winCfg, {
				title : '编辑菜单',
				items : [_listFormPanel]
			}));
	_listWin.formPanel = _listFormPanel;
	this.listWin = _listWin;
	this.listPanel = _listPanel;

	_listPanel.getStore().on('load', function() {
				this.getSelectionModel().selectRow(0);
			}, _listPanel);
	_listPanel.on('rowdblclick', function(g, rowIdx, evt) {
				var rec = g.getStore().getAt(rowIdx);
				self.modifyMenu(rec);
			}, _listPanel);
	_rootLoaded = false;
	_listPanel.getSelectionModel().on('rowselect', function(sm, rowIdx, rec) {
				if (rec) {
					var menuRoot = new Ext.tree.AsyncTreeNode({
								text : rec.get('name'),
								id : rec.get('id'),
								attachment : {
									enabled : rec.get('enabled')
								},
								leaf : false
							});
					if (!_rootLoaded) {
						_detailPanel.renderRootNode();
						_rootLoaded = true;
					}
					var _root = _detailPanel.getRootNode();
					_root.removeAll(true);
					_root.appendChild(menuRoot);
					_root.expand();
					menuRoot.expand(false, true);
				}
			});

	var _detailPanel = new Ext.tree.TreePanel({
				region : 'center',
				lines : true,
				title : '菜单详情',
				dataUrl : 'dynamic-menu!list.action',
				style : 'border-left: 1px solid ' + sys_bdcolor,
				border : false,
				root : new Ext.tree.TreeNode({
							id : '0',
							text : 'root'
						}),
				rootVisible : false,
				autoScroll : true,
				containerScroll : true,
				autoWidth : true,
				tbar : [{
					ref : 'refresh',
					text : '刷新',
					iconCls : 'icon-refresh',
					width : 50,
					handler : function() {
						var currentNode = _detailPanel.getSelectionModel().getSelectedNode();
						if (currentNode && currentNode.reload)
							currentNode.reload();
					}
				}, {
					ref : 'newItem',
					text : '新建菜单项',
					iconCls : 'icon-add',
					width : 50,
					handler : function() {
						self.addItem(_detailPanel.getSelectionModel().getSelectedNode());
					}
				}, {
					ref : 'delItem',
					text : '删除菜单项',
					iconCls : 'icon-delete',
					width : 50,
					handler : function() {
						self.deleteMenu(self.detailPanel);
					}
				}, {
					ref : 'newContent',
					text : '新建内容项',
					iconCls : 'icon-add',
					width : 50,
					handler : function() {
						var currentNode = _detailPanel.getSelectionModel().getSelectedNode();
						self.addItem(currentNode, true);
					}
				}, {
					ref : 'delContent',
					text : '删除内容项',
					iconCls : 'icon-delete',
					width : 50,
					handler : function() {
						self.deleteMenu(self.detailPanel, true);
					}
				}, {
					ref : 'expandAll',
					text : '全部展开',
					iconCls : 'icon-add',
					width : 50,
					handler : function() {
						var currentNode = _detailPanel.getSelectionModel().getSelectedNode();
						currentNode.expandChildNodes(true);
					}
				}, {
					ref : 'collapseAll',
					text : '全部收起',
					iconCls : 'icon-delete',
					width : 50,
					handler : function() {
						var currentNode = _detailPanel.getSelectionModel().getSelectedNode();
						currentNode.collapseChildNodes(true);
					}
				}, {
					ref : 'moveUp',
					text : '上移',
					iconCls : 'icon-arrow-up',
					width : 50,
					handler : function() {
						var currentNode = _detailPanel.getSelectionModel().getSelectedNode();
						self.moveMenu(currentNode, true);
					}
				}, {
					ref : 'moveDown',
					text : '下移',
					iconCls : 'icon-arrow-down',
					width : 50,
					handler : function() {
						var currentNode = _detailPanel.getSelectionModel().getSelectedNode();
						self.moveMenu(currentNode, false);
					}
				}]
			});
	
	
	
	var _getNodeErr=function(isLeaf, err) {
		if (!isLeaf)
			err = '含' + err +'项';
		return ' <span style="color:red;">' + err + '</span>';
	};

	_detailPanel.on('beforeappend', function(tree, parent, node) {
		if (__scan_enabled && node && node.attributes && node.attributes.attachment) {
			  var attach = node.attributes.attachment;
			  var state = attach.enabled;
			  if (state == 2)
				  node.text = node.text + _getNodeErr(node.leaf, '内容未开始');
			  else if (state == 3)
				  node.text = node.text + _getNodeErr(node.leaf, '内容已过期');
			  else if (state == 4)
				  node.text = node.text + _getNodeErr(node.leaf, '内容无效');
		  }
	});
	
	_detailPanel.on('click', function(record, index, eOpts){
		self.modifyItem(record, record.leaf);
	});
	
	
	var _treeCombo = new TreeComboBox({
				fieldLabel : '父级菜单项',
				emptyText : '请选择父级菜单项',
				editable : false,
				anchor : '98%',
				allowBlank : false,
				minHeight : 250,
				name : 'parentId',
				loader : new Ext.tree.TreeLoader({
							dataUrl : 'dynamic-menu!list.action'
						})
			});
	_treeCombo.on('expand', function() {
				var node = self.detailPanel.getSelectionModel()
						.getSelectedNode();
				this.expandByPath(this.tree.getRootNode(), node.getPath('id'));
			}, _treeCombo);
	this.treeCombo = _treeCombo;

	var _treeLeafCombo = new TreeComboBox({
				fieldLabel : '父级菜单项',
				emptyText : '请选择父级菜单项',
				editable : false,
				anchor : '98%',
				allowBlank : false,
				minHeight : 250,
				name : 'parentId',
				loader : new Ext.tree.TreeLoader({
							dataUrl : 'dynamic-menu!list.action'
						})
			});
	_treeLeafCombo.on('expand', function() {
				var node = self.detailPanel.getSelectionModel()
						.getSelectedNode();
				this.expandByPath(this.tree.getRootNode(), node.getPath('id'));
			}, _treeLeafCombo);
	this.treeLeafCombo = _treeLeafCombo;
	
	var editConfig = {
		region : 'east',
		split : true,
		columnLines : true,
		collapsible : true,
		collapseMode : 'mini',
		style : 'border-left: 1px solid ' + sys_bdcolor,
		width : 400,
		border : false,
		layout : 'hbox'
	}
	
	var _editMenuPanel = new Ext.form.FormPanel({
//		title : '编辑菜单项',
		bodyStyle : 'padding : 3px 10px;',
		labelWidth : 70,
		autoWidth : true,
		border : false,
		hidden : true,
		items : [
		new Ext.form.Hidden({
			name : 'id'
		}), _treeCombo, {
			xtype : 'textfield',
			fieldLabel : '菜单名称',
			name : 'name',
			allowBlank : false,
			blankText : '请输入菜单名称',
			anchor : '98%'
		}, {
			xtype : 'textfield',
			fieldLabel : '触发键',
			allowBlank : true,
			name : 'key',
			blankText : '请输入触发键',
			anchor : '98%'
		}, {
			xtype : 'trigger',
			fieldLabel : '指令',
			name : 'command',
			anchor : '98%',
			hideMode : 'display',
			allowBlank : true,
			onTriggerClick : function() {
				var p = self;
				if (!p.__cmdChooser) {
					p.__cmdChooser = new CommandChooserMenu();
					p.__cmdChooser.init();
					p.__cmdChooser.on("commandChanged", function() {
								this.setValue(p.__cmdChooser
										.getCommand());
							}, this);
					p.__cmdChooser.on("initcomplete", function() {
								p.__cmdChooser.setCommand(this
										.getValue());
								p.__cmdChooser.show(this.getEl(), 'tl');
								p.__cmdChooser.initcomplete = true;
							}, this);
				}
				if (p.__cmdChooser.initcomplete) {
					p.__cmdChooser.setCommand(this.getValue());
					p.__cmdChooser.show(this.getEl(), 'tl');
				}
			}
		}, {
			xtype : 'textarea',
			fieldLabel : '菜单标题',
			allowBlank : true,
			name : 'header',
			anchor : '98%'
		}, {
			xtype : 'textarea',
			fieldLabel : '菜单脚注',
			allowBlank : true,
			name : 'footer',
			anchor : '98%'
		}, {
			xtype : 'numberfield',
			fieldLabel : '排序',
			allowBlank : true,
			name : 'priority',
			anchor : '98%'
		}],
		
		tbar : [{
			text : '保存',
			iconCls : 'icon-save',
			handler : function() {
				this.disable();
				self.saveMenu(this, self.editMenuPanel, function(resp, isAdd, btn) {
							var nodeCfg = Ext.decode(resp.responseText);
							if (isAdd) {
								var newItem = new Ext.tree.TreeNode(nodeCfg);
								var pn = _detailPanel.getSelectionModel().getSelectedNode();
								if (!pn.isExpanded())
									pn.expand();
								pn.appendChild(newItem);
								if (!nodeCfg.leaf) {
									var ui = newItem.getUI();
									ui.removeClass('x-tree-node-leaf');
									ui.addClass('x-tree-node-collapsed');
								}
							} else {
								var n = _detailPanel.getSelectionModel().getSelectedNode();
								var newPid = nodeCfg.attachment.parentId;
								var oldPid = n.attributes.attachment.parentId;
								parentNew = self.detailPanel
										.getNodeById(newPid);
								n.attributes = nodeCfg;
								n.setText(nodeCfg.text);
								if (oldPid != newPid) {
									var parentBefore = n.parentNode;
									if (parentNew) {
										if (!parentNew.isExpanded())
											parentNew.expand();
										if (parentNew.leaf) {
											var ui = parentNew.getUI();
											ui.removeClass('.x-tree-node-leaf');
											ui
													.addClass('.x-tree-node-expanded');
										}
										parentNew.appendChild(n);
									} else {
										parentBefore.removeChild(n);
									}
								}
							}
							btn.enable();
							Dashboard.setAlert('保存成功！');
						});
			}
		}]
	});
	
	this.editMenuPanel = _editMenuPanel;
	
	var _editContentPanel = new Ext.form.FormPanel({
//		title : '编辑内容项',
		bodyStyle : 'padding : 3px 10px;',
		labelWidth : 70,
		autoWidth : true,
		border : false,
		hidden : true,
		items : [new Ext.form.Hidden({
					name : 'id'
				}), _treeLeafCombo, {
				xtype : 'textarea',
				fieldLabel : '菜单内容',
				name : 'content',
				anchor : '98%',
				hideMode : 'display',
				blankText : '请输入菜单内容',
				allowBlank : true
			}, {
				xtype : 'textfield',
				fieldLabel : '触发键',
				allowBlank : true,
				name : 'key',
				blankText : '请输入触发键',
				anchor : '98%'
			}, {
				ref : 'command',
				xtype : 'trigger',
				fieldLabel : '指令',
				name : 'command',
				anchor : '98%',
				hideMode : 'display',
				allowBlank : true,
				onTriggerClick : function() {
					var p = self;
					if (!p.__cmdChooser) {
						p.__cmdChooser = new CommandChooserMenu();
						p.__cmdChooser.init();
						p.__cmdChooser.on("commandChanged", function() {
									this.setValue(p.__cmdChooser
											.getCommand());
								}, this);
						p.__cmdChooser.on("initcomplete", function() {
									p.__cmdChooser.setCommand(this
											.getValue());
									p.__cmdChooser.show(this.getEl(), 'tl');
									p.__cmdChooser.initcomplete = true;
								}, this);
					}
					if (p.__cmdChooser.initcomplete) {
						p.__cmdChooser.setCommand(this.getValue());
						p.__cmdChooser.show(this.getEl(), 'tl');
					}
				}
			}, {
				xtype : 'numberfield',
				fieldLabel : '排序',
				allowBlank : true,
				name : 'priority',
				anchor : '98%'
			}],
			tbar : [{
			text : '保存',
			iconCls : 'icon-save',
			handler : function() {
			this.disable();
			self.saveMenu(this, self.editContentPanel, function(resp, isAdd,
							btn) {
						var nodeCfg = Ext.decode(resp.responseText);
						if (isAdd) {
							var newItem = new Ext.tree.TreeNode(nodeCfg);
							var pn = _detailPanel.getSelectionModel().getSelectedNode();
							if (!pn.isExpanded())
								pn.expand();
							pn.appendChild(newItem);
						} else {
							var n = _detailPanel.getSelectionModel().getSelectedNode();
							var newPid = nodeCfg.attachment.parentId;
							var oldPid = n.attributes.attachment.parentId;
							parentNew = self.detailPanel
									.getNodeById(newPid);
							n.attributes = nodeCfg;
							n.setText(nodeCfg.text);
							if (oldPid != newPid) {
								var parentBefore = n.parentNode;
								if (parentNew) {
									if (!parentNew.isExpanded())
										parentNew.expand();
									if (parentNew.leaf) {
										var ui = parentNew.getUI();
										ui.removeClass('.x-tree-node-leaf');
										ui
												.addClass('.x-tree-node-expanded');
									}
									parentNew.appendChild(n);
								} else {
									parentBefore.removeChild(n);
								}
							}
						}
						btn.enable();
						this.detailLeafWin.hide();
					});
				}
			}]
		});
	
	this.editContentPanel = _editContentPanel;
	
	var _editPanel = new Ext.Panel(Ext.apply(editConfig, {
		items : [_editMenuPanel, _editContentPanel]
	}));
	
	this.editPanel = _editPanel;
	
	var _detailFormPanel = new Ext.form.FormPanel(Ext.apply(formCfg, {
		items : [new Ext.form.Hidden({
							name : 'id'
						}), {
					xtype : 'textfield',
					fieldLabel : '菜单名称',
					name : 'name',
					allowBlank : false,
					blankText : '请输入菜单名称',
					anchor : '98%'
				}, {
					xtype : 'textfield',
					fieldLabel : '触发键',
					allowBlank : true,
					name : 'key',
					blankText : '请输入触发键',
					anchor : '98%'
				}, {
					xtype : 'trigger',
					fieldLabel : '指令',
					name : 'command',
					anchor : '98%',
					hideMode : 'display',
					allowBlank : true,
					onTriggerClick : function() {
						var p = self;
						if (!p.__cmdChooser) {
							p.__cmdChooser = new CommandChooserMenu();
							p.__cmdChooser.init();
							p.__cmdChooser.on("commandChanged", function() {
										this.setValue(p.__cmdChooser
												.getCommand());
									}, this);
							p.__cmdChooser.on("initcomplete", function() {
										p.__cmdChooser.setCommand(this
												.getValue());
										p.__cmdChooser.show(this.getEl(), 'tl');
										p.__cmdChooser.initcomplete = true;
									}, this);
						}
						if (p.__cmdChooser.initcomplete) {
							p.__cmdChooser.setCommand(this.getValue());
							p.__cmdChooser.show(this.getEl(), 'tl');
						}
					}
				}, {
					xtype : 'textarea',
					fieldLabel : '菜单标题',
					allowBlank : true,
					name : 'header',
					anchor : '98%'
				}, {
					xtype : 'textarea',
					fieldLabel : '菜单脚注',
					allowBlank : true,
					name : 'footer',
					anchor : '98%'
				}, {
					xtype : 'numberfield',
					fieldLabel : '排序',
					allowBlank : true,
					name : 'priority',
					anchor : '98%'
				}],
		tbar : [{
			text : '保存',
			iconCls : 'icon-save',
			handler : function() {
				this.disable();
				self.saveMenu(this, self.detailWin, function(resp, isAdd, btn) {
							var nodeCfg = Ext.decode(resp.responseText);
							if (isAdd) {
								var newItem = new Ext.tree.TreeNode(nodeCfg);
								var pn = detailMenu.currentNode;
								if (!pn.isExpanded())
									pn.expand();
								pn.appendChild(newItem);
								if (!nodeCfg.leaf) {
									var ui = newItem.getUI();
									ui.removeClass('x-tree-node-leaf');
									ui.addClass('x-tree-node-collapsed');
								}
							} else {
								var n = detailMenu.currentNode;
								var newPid = nodeCfg.attachment.parentId;
								var oldPid = n.attributes.attachment.parentId;
								parentNew = self.detailPanel
										.getNodeById(newPid);
								n.attributes = nodeCfg;
								n.setText(nodeCfg.text);
								if (oldPid != newPid) {
									var parentBefore = n.parentNode;
									if (parentNew) {
										if (!parentNew.isExpanded())
											parentNew.expand();
										if (parentNew.leaf) {
											var ui = parentNew.getUI();
											ui.removeClass('.x-tree-node-leaf');
											ui
													.addClass('.x-tree-node-expanded');
										}
										parentNew.appendChild(n);
									} else {
										parentBefore.removeChild(n);
									}
								}
							}
							btn.enable();
							this.detailWin.hide();
							Dashboard.setAlert('保存成功！');
						});
				}
			}]
	}));

	var _detailWin = new Ext.Window(Ext.apply(winCfg, {
				title : '编辑菜单项',
				shadow : false,
				items : [_detailFormPanel]
			}));

	var _detailLeafFormPanel = new Ext.form.FormPanel(Ext.apply(formCfg, {
		items : [new Ext.form.Hidden({
							name : 'id'
						}), {
					xtype : 'textarea',
					fieldLabel : '菜单内容',
					name : 'content',
					anchor : '98%',
					hideMode : 'display',
					blankText : '请输入菜单内容',
					allowBlank : true
				}, {
					xtype : 'textfield',
					fieldLabel : '触发键',
					allowBlank : true,
					name : 'key',
					blankText : '请输入触发键',
					anchor : '98%'
				}, {
					ref : 'command',
					xtype : 'trigger',
					fieldLabel : '指令',
					name : 'command',
					anchor : '98%',
					hideMode : 'display',
					allowBlank : true,
					onTriggerClick : function() {
						var p = self;
						if (!p.__cmdChooser) {
							p.__cmdChooser = new CommandChooserMenu();
							p.__cmdChooser.init();
							p.__cmdChooser.on("commandChanged", function() {
										this.setValue(p.__cmdChooser
												.getCommand());
									}, this);
							p.__cmdChooser.on("initcomplete", function() {
										p.__cmdChooser.setCommand(this
												.getValue());
										p.__cmdChooser.show(this.getEl(), 'tl');
										p.__cmdChooser.initcomplete = true;
									}, this);
						}
						if (p.__cmdChooser.initcomplete) {
							p.__cmdChooser.setCommand(this.getValue());
							p.__cmdChooser.show(this.getEl(), 'tl');
						}
					}
				}, {
					xtype : 'numberfield',
					fieldLabel : '排序',
					allowBlank : true,
					name : 'priority',
					anchor : '98%'
				}],
		tbar : [{
			text : '保存',
			iconCls : 'icon-save',
			handler : function() {
				this.disable();
				self.saveMenu(this, self.detailLeafWin, function(resp, isAdd,
								btn) {
							var nodeCfg = Ext.decode(resp.responseText);
							if (isAdd) {
								var newItem = new Ext.tree.TreeNode(nodeCfg);
								var pn = detailMenu.currentNode;
								if (!pn.isExpanded())
									pn.expand();
								pn.appendChild(newItem);
							} else {
								var n = detailLeafMenu.currentNode;
								var newPid = nodeCfg.attachment.parentId;
								var oldPid = n.attributes.attachment.parentId;
								parentNew = self.detailPanel
										.getNodeById(newPid);
								n.attributes = nodeCfg;
								n.setText(nodeCfg.text);
								if (oldPid != newPid) {
									var parentBefore = n.parentNode;
									if (parentNew) {
										if (!parentNew.isExpanded())
											parentNew.expand();
										if (parentNew.leaf) {
											var ui = parentNew.getUI();
											ui.removeClass('.x-tree-node-leaf');
											ui
													.addClass('.x-tree-node-expanded');
										}
										parentNew.appendChild(n);
									} else {
										parentBefore.removeChild(n);
									}
								}
							}
							btn.enable();
							this.detailLeafWin.hide();
						});
			}
		}]
	}));

	var _detailLeafWin = new Ext.Window(Ext.apply(winCfg, {
				title : '编辑内容项',
				shadow : false,
				items : [_detailLeafFormPanel]
			}));

	var detailMenu = new Ext.menu.Menu({
		items : [{
					text : '刷新',
					iconCls : 'icon-refresh',
					width : 50,
					handler : function() {
						if (detailMenu.currentNode && detailMenu.currentNode.reload)
							detailMenu.currentNode.reload();
					}
				}, '-', {
					ref : 'newItem',
					text : '新建菜单项',
					iconCls : 'icon-add',
					width : 50,
					handler : function() {
						self.addItem(detailMenu.currentNode);
					}
				}, {
					text : '编辑菜单项',
					iconCls : 'icon-edit',
					width : 50,
					handler : function() {
						if (detailMenu.currentNode.parentNode.id == 0) {
							_listPanel
									.fireEvent(
											'rowdblclick',
											_listPanel,
											_listPanel
													.getStore()
													.indexOf(_listPanel
															.getSelectionModel()
															.getSelected()));
						} else {
							self.modifyItem(detailMenu.currentNode);
						}
					}
				}, {
					ref : 'delFunc',
					text : '删除菜单项',
					iconCls : 'icon-delete',
					width : 50,
					handler : function() {
						self.deleteMenu(self.detailPanel);
					}
				}, '-', {
					ref : 'newContent',
					text : '新建内容项',
					iconCls : 'icon-add',
					width : 50,
					handler : function() {
						self.addItem(detailMenu.currentNode, true);
					}
				}, {
					text : '全部展开',
					iconCls : 'icon-add',
					width : 50,
					handler : function() {
						detailMenu.currentNode.expandChildNodes(true);
					}
				}, {
					text : '全部收起',
					iconCls : 'icon-delete',
					width : 50,
					handler : function() {
						detailMenu.currentNode.collapseChildNodes(true);
					}
				}]
	});
	var detailLeafMenu = new Ext.menu.Menu({
				items : [{
							text : '编辑内容项',
							iconCls : 'icon-edit',
							width : 50,
							handler : function() {
								self.modifyItem(detailLeafMenu.currentNode,
										true);
							}
						}, {
							ref : 'delFunc',
							text : '删除内容项',
							iconCls : 'icon-delete',
							width : 50,
							handler : function() {
								self.deleteMenu(self.detailPanel, true);
							}
						}]
			});

//	_detailPanel.on('contextmenu', function(node, event) {
//				var menu = node.isLeaf() ? detailLeafMenu : detailMenu;
//				event.preventDefault();
//				menu.currentNode = node;
//				menu.delFunc.setDisabled(node.parentNode.id == '0');
//				node.select();
//				if (!node.isLeaf()) {
//					menu.newItem.setDisabled(false);
//					menu.newContent.setDisabled(false);
//					node.expand(false, true, function() {
//								if (node.childNodes.length) {
//									if (node.childNodes.length == 1
//											&& node.childNodes[0].isLeaf()) {
//										menu.newItem.setDisabled(true);
//										menu.newContent.setDisabled(true);
//									} else {
//										menu.newContent.setDisabled(true);
//									}
//								}
//								menu.showAt(event.getXY());
//							}, node);
//
//				} else {
//					menu.showAt(event.getXY());
//				}
//
//			});

	_detailPanel.detailMenu = detailMenu;
	_detailPanel.detailLeafMenu = detailLeafMenu;
	this.detailPanel = _detailPanel;
	_detailWin.formPanel = _detailFormPanel;
	_detailLeafWin.formPanel = _detailLeafFormPanel;
	this.detailWin = _detailWin;
	this.detailLeafWin = _detailLeafWin;

	_editMenuPanel.on('show', function() {
				var rec = _listPanel.getSelectionModel().getSelected();
				var n = new Ext.tree.AsyncTreeNode({
							id : rec.get('id'),
							text : rec.get('name')
						});
				if (!self.treeCombo.tree)
					self.treeCombo.root = n;
				else {
					var _root = self.treeCombo.root;
					_root.removeAll();
					_root.appendChild(n);
					_root.expand();
					n.expand(false, true);
				}
			});
	_detailLeafWin.on('show', function() {
				var rec = _listPanel.getSelectionModel().getSelected();
				var n = new Ext.tree.AsyncTreeNode({
							id : rec.get('id'),
							text : rec.get('name')
						});
				if (!self.treeLeafCombo.tree)
					self.treeLeafCombo.root = n;
				else {
					var _root = self.treeLeafCombo.root;
					_root.removeAll();
					_root.appendChild(n);
					_root.expand();
					n.expand(false, true);
				}
			});

	var config = {
		id : 'menuPanel',
		layout : 'border',
		border : false,
		items : [_listPanel, _detailPanel, _editPanel]
	};
	DynamicMenuPanel.superclass.constructor.call(this, config);
};

Ext.extend(DynamicMenuPanel, Ext.Panel, {
	init : function() {
		this.listPanel.getStore().load();
	},
	addMenu : function() {
		this.listWin.setTitle("新增菜单");
		this.listWin.show();
	},
	modifyMenu : function() {
		this.listWin.setTitle("编辑菜单");
		this.listWin.show();
		var rec = this.listPanel.getSelectionModel().getSelected();
		this.listWin.formPanel.getForm().loadRecord(rec);
	},
	searchMenuname : function(){
		  var onlyRoot = this.tbarEx.onlyRoot.getValue();
		  var searchName = this.tbarEx.searchName.getValue();
		  if(searchName){
			  if(onlyRoot){
				  if(!(this.menuRootName && searchName == this.menuRootName)){
					  var searchRecord = [];
					  Ext.each(this.listPanel.getStore().getRange(),function(record){
						  if(record.data.name.indexOf(searchName)>-1){
							  searchRecord.push(record);
						  }
					  });
					  this.queryAllMatchName = searchRecord;
					  this.menuRootName = searchName;
				  }
				  if(this.prevSearchNameIdx < this.queryAllMatchName.length-1)
					  this.prevSearchNameIdx = this.prevSearchNameIdx+1
					  else
						  this.prevSearchNameIdx = 0;
				  var selectRecord = [this.queryAllMatchName[this.prevSearchNameIdx]];
				  if(this.listPanel.getSelectionModel().getSelected() != selectRecord[0])
					  this.listPanel.getSelectionModel().selectRecords(selectRecord);
			  }else{
// if (!this.getSelectionModel().getSelectedNode()) {
// this.getRootNode().childNodes[0].select();
// }
				  var self = this;
				  Ext.Ajax.request({
					  url : 'dynamic-menu!search.action',
					  params : {
						  menuName : searchName
// ispub : this.isPub(this.getSelectedNode()),
// matchExactly : this.matchExactly ? true : false
					  },
					  success : function(form, action) {
						  if (!form.responseText) {
							  Dashboard.setAlert('没有查找到更多的"' + searchName + '"，请修改后重新查找');
							  return;
						  }
						  var idPath = Ext.util.JSON.decode(form.responseText);
						  var rec = self.listPanel.getStore().getById(idPath[0]);
						  if(rec != self.listPanel.getSelectionModel().getSelections()[0])
							  self.listPanel.getSelectionModel().selectRecords([rec]);
						  if (idPath.length > 1) {
							  var targetId = idPath.pop();
							  self.detailPanel.expandPath('/0/' + idPath.join('/'), '', function(suc, n) {
								  self.detailPanel.getNodeById(targetId).select();
// if (focusTarget)
// focusTarget.focus()
							  });
						  }
					  },
					  failure : function(form, action) {
						  Ext.Msg.alert('提示', '查找错误');
					  }
				  });
			  }
		  } else {
			  Ext.Msg.alert('提示', '您输入要查询名称！');
		  }
	},
	saveMenu : function(btn, win, callback) {
		//loading效果代码
		var myMask = new Ext.LoadMask(Ext.getBody(), {
			 	msg : "正在保存,请稍后...",
			 	msgCls : 'x-mask-loading z-index:10000;'
			 });
		var self = this;
		var f = win.formPanel ? win.formPanel.getForm() : win.getForm();
		if (!f.isValid()) {
			Ext.Msg.alert('提示', '您尚未正确完整的输入表单内容，请检查！');
			btn.enable();
			return false;
		}
		var vals = f.getFieldValues();
		var isAdd = !vals['id'];
		if (isAdd)
			delete vals['id'];
		if (win == self.editMenuPanel || win == self.editContentPanel)
			vals['enabled'] = 1;
		// this.listPanel.getSelectionModel().getSelected().get('enabled') == 0
		// ? 0 : 1;
		if (!vals['priority']) {
			vals['priority'] = 0;
			if(f.items.items[1].dataNode) {
				var childrens = f.items.items[1].dataNode.childNodes;
			    vals['priority'] = childrens.length;
			}
		}
		myMask.show();
		Ext.Ajax.request({
					url : 'dynamic-menu!save.action',
					params : {
						'data' : Ext.encode(vals)
					},
					success : function(resp) {
						myMask.hide();
						if (resp.responseText.indexOf('_error_') != -1) {
							Ext.Msg.alert('错误', '菜单结构错误，无法保存！');
							btn.enable();
							return false;
						}
						var obj = Ext.decode(resp.responseText);
						if (obj.success == false) {
							Ext.Msg.alert('错误', obj.message);
							btn.enable();
							return false;
						}
						btn.enable();
						callback.call(self, resp, isAdd, btn);
					},
					failure : function(resp) {
						myMask.hide();
						Ext.Msg.alert('错误', resp.responseText);
						btn.enable();
					}
				});
	},
	deleteMenu : function(panel, leaf) {
		var rec = null, name = null, self = this;
		if (panel == this.listPanel) {
			rec = panel.getSelectionModel().getSelected();
			name = rec.get('name');
		} else {
			rec = panel.getSelectionModel().selNode;
			name = rec.text;
		}
		Ext.Msg.confirm('提示', '确认删除菜单【' + name + '】？'
						+ (leaf ? '' : '所有子项将被一并删除！'), function(btn) {
					if (btn == 'yes') {
						Ext.Ajax.request({
							url : 'dynamic-menu!delete.action',
							params : {
								'node' : rec.id
							},
							success : function(resp) {
								if (panel == self.listPanel) {
									var st = panel.getStore();
									st.reload({
												callback : function() {
													var c = st.getCount();
													if (c == 0)
														self.detailPanel
																.getRootNode()
																.removeAll(true);
												}
											});
								} else
									rec.parentNode.removeChild(rec);
							},
							failure : function(resp) {
								Ext.Msg.alert('错误', resp.responseText);
							}
						});
					}
				});
	},
	//'id', 'key', 'parentId', 'name',
//	'header', 'content', 'footer', 'command', 'priority', 'enabled',
//	'createTime', 'bh', 'status'
	addItem : function(pnode, leaf) {
		var title = leaf ? '新建内容项' : '新建菜单项';
		this.editPanel.setTitle(title);
		var closeWin = leaf ? this.editMenuPanel : this.editContentPanel;
		if(closeWin.isVisible())
			closeWin.hide();
		var win = leaf ? this.editContentPanel : this.editMenuPanel;
		if(!win.isVisible())
			win.show();
		this.editPanel.doLayout();
		win.getForm().reset();
		var combo = leaf ? this.treeLeafCombo : this.treeCombo;
		combo.setValueEx(pnode);
	},
	modifyItem : function(cnode, leaf) {
		var title = leaf ? '编辑内容项' : '编辑菜单项';
		this.editPanel.setTitle(title);
		var closeWin = leaf ? this.editMenuPanel : this.editContentPanel;
		if(closeWin.isVisible())
			closeWin.hide();
		var win = leaf ? this.editContentPanel : this.editMenuPanel;
		if(!win.isVisible())
			win.show();
		this.editPanel.doLayout();
		var rec = new this.MenuRecord(cnode.attributes.attachment, cnode.id);
		win.getForm().reset();
		win.getForm().loadRecord(rec);
		var combo = leaf ? this.treeLeafCombo : this.treeCombo;
		combo.setValueEx(cnode.parentNode.isRoot ? cnode : cnode.parentNode);
		var toolbar = this.detailPanel.getTopToolbar();
		Ext.each(toolbar.items.items, function(item){
			item.setDisabled(false);
		});
		cnode.expand(false, true, function(){
			toolbar.refresh.setDisabled(leaf);
			toolbar.newItem.setDisabled(leaf ? true : cnode.firstChild ? cnode.firstChild.isLeaf() : false);
			toolbar.delItem.setDisabled(leaf ? true : cnode.parentNode.isRoot);
			toolbar.newContent.setDisabled(leaf ? true : cnode.firstChild);
			toolbar.delContent.setDisabled(!leaf);
			toolbar.expandAll.setDisabled(!cnode.parentNode.isRoot);
			toolbar.collapseAll.setDisabled(!cnode.parentNode.isRoot);
			toolbar.moveUp.setDisabled(cnode.isFirst());
			toolbar.moveDown.setDisabled(cnode.isLast());
		});
		
	},
	destroy : function() {
		DynamicMenuPanel.superclass.destroy.call(this);
		if (this.__cmdChooser) {
			this.__cmdChooser.destroy();
			delete this.__cmdChooser;
		}
	},
	
	moveMenu : function(node, isUp) {
		//loading效果代码
		var myMask = new Ext.LoadMask(Ext.getBody(), {
			 	msg : "正在移动,请稍后...",
			 	msgCls : 'x-mask-loading z-index:10000;'
			 });
		var origNode = node;
		var destNode;
		if(isUp) {
			destNode = node.previousSibling;
		}else {
			destNode = node.nextSibling;
		}
		var self = this;
		myMask.show();
		Ext.Ajax.request({
			url : 'dynamic-menu!moveMenu.action',
			params : {
				'origNode' : origNode.id,
				'destNode' : destNode.id
			},
			success : function(resp) {
				myMask.hide();
				var path = node.getPath();
				var targetId = node.id;
				var obj = Ext.decode(resp.responseText);
				if(obj.success) {
					Dashboard.setAlert('移动成功！');
					self.detailPanel.root.firstChild.reload(function(){
						self.detailPanel.expandPath(path, '',  function(suc, n) {
							var selectNode = self.detailPanel.getNodeById(targetId);
							self.detailPanel.getSelectionModel().select(selectNode);
							self.modifyItem(selectNode, selectNode.isLeaf());
						});
					});
				}
			},
			failure : function(resp) {
				myMask.hide();
				Ext.Msg.alert('错误', resp.responseText);
			}
		});
	}
});