HotQuestionAutoPanel = function() {
	var self = this;
	var store = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({
			url : 'hot-question!list.action?mode=1'
		}),
		reader : new Ext.data.JsonReader({
			idProperty : 'id',
			root : 'data',
			fields : [ {
				name : 'question',
				type : 'string'
			}, {
				name : 'rank',
				type : 'int'
			}, {
				name : 'platform',
				type : 'string'
			}, {
				name : 'platformName',
				type : 'string'
			}, {
				name : 'location',
				type : 'string'
			}, {
				name : 'locationName',
				type : 'string'
			}, {
				name : 'brand'
			}, {
				name : 'brandName'
			}, {
				name : 'custom1'
			}, {
				name : 'custom1Name'
			}, {
				name : 'custom2'
			}, {
				name : 'custom2Name'
			}, {
				name : 'custom3'
			}, {
				name : 'custom3Name'
			}  ]
		}),
		autoLoad : true
	});
	var sm = new Ext.grid.RowSelectionModel();

	var _columns = [ new Ext.grid.RowNumberer(), {
		id : 'question_id',
		header : '问题内容',
		dataIndex : 'question'
	}, {
		header : Dashboard.dimensionNames.platform,
		dataIndex : 'platformName',
		width : 145
	}, {
		header : Dashboard.dimensionNames.location,
		dataIndex : 'locationName',
		width : 150
	}, {
		header : Dashboard.dimensionNames.brand,
		dataIndex : 'brandName',
		width : 150
	} ];

	if (Dashboard.dimensionNames.custom1) {
		_columns.push({
			header : Dashboard.dimensionNames.custom1,
			dataIndex : 'custom1Name',
			width : 150
		});
	}
	if (Dashboard.dimensionNames.custom2) {
		_columns.push({
			header : Dashboard.dimensionNames.custom2,
			dataIndex : 'custom2Name',
			width : 150
		});
	}
	if (Dashboard.dimensionNames.custom3) {
		_columns.push({
			header : Dashboard.dimensionNames.custom3,
			dataIndex : 'custom3Name',
			width : 150
		});
	}

	var config = {
		id : 'hotQuestionAutoPanel',
		store : store,
		margins : '0 5 5 5',
		border : false,
		autoExpandColumn : 'question_id',
		tbar : [ {
			iconCls : 'icon-refresh',
			text : '刷新',
			handler : function() {
				store.reload();
			}
		} ],
		sm : sm,
		loadMask : true,
		colModel : new Ext.grid.ColumnModel({
			columns : _columns
		})
	};
	HotQuestionAutoPanel.superclass.constructor.call(this, config);
}

Ext.extend(HotQuestionAutoPanel, Ext.grid.GridPanel, {});
