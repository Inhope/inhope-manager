HotQuestionCusPanel = function() {

	var self = this;

	var store = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({
			api : {
				read : 'hot-question!list.action',
				create : 'hot-question!save.action',
				update : 'hot-question!save.action',
				destroy : 'hot-question!delete.action'
			}
		}),
		reader : new Ext.data.JsonReader({
			idProperty : 'id',
			root : 'data',
			fields : [ {
				name : 'question',
				type : 'string'
			}, {
				name : 'rank',
				type : 'int'
			}, {
				name : 'platform',
				type : 'string'
			}, {
				name : 'platformName',
				type : 'string'
			}, {
				name : 'location',
				type : 'string'
			}, {
				name : 'locationName',
				type : 'string'
			}, {
				name : 'brand'
			}, {
				name : 'brandName'
			}, {
				name : 'custom1'
			}, {
				name : 'custom1Name'
			}, {
				name : 'custom2'
			}, {
				name : 'custom2Name'
			}, {
				name : 'custom3'
			}, {
				name : 'custom3Name'
			}  ]
		}),
		writer : new Ext.data.JsonWriter({
			writeAllFields : true
		}),
		listeners : {
			beforewrite : function(store, data, rs) {
				if (!rs.get('question'))
					return false;
				if (!rs.get('rank')) {
					var selr = self.getSelectionModel().getSelected();
					if (selr)
						rs.data.rank = selr.get('rank');
				}
			},
			write : function() {
				self.getStore().reload();
			}
		}
	});
	var editor = new Ext.ux.grid.RowEditor({
		saveText : '保存',
		cancelText : '取消',
		errorSummary : false
	});
	editor.on('canceledit', function(rowEditor, changes, record, rowIndex) {
		var r, i = 0;
		while ((r = store.getAt(i++))) {
			if (r.phantom) {
				store.remove(r);
			}
		}
	});
	var sm = new Ext.grid.RowSelectionModel();
	this.selectByProgram = true;
	var autoRadio = new Ext.form.Radio({
		name : 'mode',
		boxLabel : '动态模式',
		handler : function(r, checked) {
			if (checked && !self.selectByProgram) {
				Ext.Msg.confirm('提示', '确认切换为动态模式？', function(btn) {
					if (btn == 'yes') {
						self.selectByProgram = false;
						self.setMode(1);
					} else {
						self.selectByProgram = true;
						cusRadio.setValue(true);
					}
				});
			} else {
				self.selectByProgram = false;
			}
		}
	});
	var cusRadio = new Ext.form.Radio({
		name : 'mode',
		boxLabel : '定制模式',
		handler : function(r, checked) {
			if (checked && !self.selectByProgram) {
				Ext.Msg.confirm('提示', '确认切换为定制模式？', function(btn) {
					if (btn == 'yes') {
						self.selectByProgram = false;
						self.setMode(2);
					} else {
						self.selectByProgram = true;
						autoRadio.setValue(true);
					}
				});
			} else {
				self.selectByProgram = false;
			}
		}
	});
	this.cusRadio = cusRadio;
	this.autoRadio = autoRadio;

	var _columns = [ new Ext.grid.RowNumberer(), {
		id : 'question_id',
		header : '问题内容',
		dataIndex : 'question',
		editor : {
			xtype : 'textfield',
			allowBlank : false,
			blankText : '请输入规则'
		}
	}, {
		header : Dashboard.dimensionNames.platform,
		dataIndex : 'platform',
		width : 120,
		renderer : function(v, meta, rec) {
			return rec.get('platformName');
		},
		editor : {
			xtype : 'combo',
			triggerAction : 'all',
			allowBlank : false,
			editable : false,
			store : new Ext.data.JsonStore({
				url : 'hot-question!listDims.action?dim=platform',
				fields : [ 'tag', 'name' ],
				autoLoad : true
			}),
			valueField : 'tag',
			displayField : 'name'
		}
	}, {
		header : Dashboard.dimensionNames.location,
		dataIndex : 'location',
		width : 120,
		renderer : function(v, meta, rec) {
			return rec.get('locationName');
		},
		editor : {
			xtype : 'combo',
			triggerAction : 'all',
			editable : false,
			allowBlank : false,
			store : new Ext.data.JsonStore({
				url : 'hot-question!listDims.action?dim=location',
				fields : [ 'tag', 'name' ],
				autoLoad : true
			}),
			valueField : 'tag',
			displayField : 'name'
		}
	}, {
		header : Dashboard.dimensionNames.brand,
		dataIndex : 'brand',
		width : 120,
		renderer : function(v, meta, rec) {
			return rec.get('brandName');
		},
		editor : {
			xtype : 'combo',
			triggerAction : 'all',
			editable : false,
			allowBlank : false,
			store : new Ext.data.JsonStore({
				url : 'hot-question!listDims.action?dim=brand',
				fields : [ 'tag', 'name' ],
				autoLoad : true
			}),
			valueField : 'tag',
			displayField : 'name'
		}
	} ];
	if (Dashboard.dimensionNames.custom1) {
		_columns.push({
			header : Dashboard.dimensionNames.custom1,
			dataIndex : 'custom1',
			width : 120,
			renderer : function(v, meta, rec) {
				return rec.get('custom1Name');
			},
			editor : {
				xtype : 'combo',
				triggerAction : 'all',
				editable : false,
				allowBlank : false,
				store : new Ext.data.JsonStore({
					url : 'hot-question!listDims.action?dim=custom1',
					fields : [ 'tag', 'name' ],
					autoLoad : true
				}),
				valueField : 'tag',
				displayField : 'name'
			}
		});
	}
	if (Dashboard.dimensionNames.custom2) {
		_columns.push({
			header : Dashboard.dimensionNames.custom2,
			dataIndex : 'custom2',
			width : 120,
			renderer : function(v, meta, rec) {
				return rec.get('custom2Name');
			},
			editor : {
				xtype : 'combo',
				triggerAction : 'all',
				editable : false,
				allowBlank : false,
				store : new Ext.data.JsonStore({
					url : 'hot-question!listDims.action?dim=custom2',
					fields : [ 'tag', 'name' ],
					autoLoad : true
				}),
				valueField : 'tag',
				displayField : 'name'
			}
		});
	}
	if (Dashboard.dimensionNames.custom3) {
		_columns.push({
			header : Dashboard.dimensionNames.custom3,
			dataIndex : 'custom3',
			width : 120,
			renderer : function(v, meta, rec) {
				return rec.get('custom3Name');
			},
			editor : {
				xtype : 'combo',
				triggerAction : 'all',
				editable : false,
				allowBlank : false,
				store : new Ext.data.JsonStore({
					url : 'hot-question!listDims.action?dim=custom3',
					fields : [ 'tag', 'name' ],
					autoLoad : true
				}),
				valueField : 'tag',
				displayField : 'name'
			}
		});
	}
	_columns.push({
		header : '操作',
		width : 85,
		renderer : function() {
			return "<a class='_upButton' href='javascript:void(0);' onclick='javscript:return false;'>上移</a>&nbsp;&nbsp;&nbsp;"
					+ "<a class='_downButton' href='javascript:void(0);' onclick='javscript:return false;'>下移</a>";
		}
	});

	var config = {
		id : 'hotQuestionCusPanel',
		store : store,
		margins : '0 5 5 5',
		border : false,
		autoExpandColumn : 'question_id',
		plugins : [ editor ],
		tbar : [ {
			iconCls : 'icon-add',
			text : '添加问题',
			handler : function() {
				var fix = new store.recordType({
					name : '',
					platform : 'all',
					location : 'all',
					brand : 'all',
					custom1 : 'all',
					custom2 : 'all',
					custom3 : 'all'
				});
				editor.stopEditing();
				var sm = self.getSelectionModel();
				var rec = sm.getSelected();
				if (!rec) {
					sm.selectLastRow();
					rec = sm.getSelected();
				}
				var idx = store.indexOf(rec) + 1;
				store.insert(idx, fix);
				editor.startEditing(idx, 1);
			}
		}, {
			iconCls : 'icon-delete',
			text : '删除问题',
			handler : function() {
				editor.stopEditing();
				var rows = self.getSelectionModel().getSelections();
				if (rows.length == 0) {
					Ext.Msg.alert('提示', '请至少选择一条记录！');
					return false;
				}
				Ext.Msg.confirm('提示', '确认删除这' + rows.length + '条记录？', function(btn) {
					if (btn == 'yes') {
						var ids = [];
						Ext.each(rows, function() {
							ids.push(this.id)
						});
						Ext.Ajax.request({
							url : 'hot-question!delete.action',
							params : {
								ids : ids.join(',')
							},
							success : function(resp) {
								Ext.Msg.alert('提示', '删除成功！', function() {
									store.reload();
								});
							},
							failure : function(resp) {
								Ext.Msg.alert('错误', resp.responseText);
							}
						})
					}
				});
			}
		}, {
			iconCls : 'icon-refresh',
			text : '刷新',
			handler : function() {
				store.reload();
			}
		}, '-', autoRadio, ' ', cusRadio, '-', {
			iconCls : 'icon-ok',
			text : '应用',
			handler : function() {
				Ext.Msg.confirm('提示', '确认应用？', function(btn) {
					if (btn == 'yes') {
						Ext.Ajax.request({
							url : 'hot-question!apply.action',
							success : function(resp) {
								Dashboard.setAlert('应用成功！');
							},
							failure : function(resp) {
								Ext.Msg.alert('错误', resp.responseText);
							}
						});
					}
				})
			}
		} ],
		sm : sm,
		loadMask : true,
		colModel : new Ext.grid.ColumnModel({
			columns : _columns
		})
	};
	HotQuestionCusPanel.superclass.constructor.call(this, config);

	var switchRank = function(id1, id2) {
		Ext.Ajax.request({
			url : 'hot-question!switchRank.action',
			params : {
				ids : id1 + ',' + id2
			},
			success : function(resp) {
				self.getStore().reload();
			},
			failure : function(resp) {
				Ext.Msg.alert('错误', resp.responseText);
			}
		});
	}
	this.on('cellclick', function(grid, row, col, e) {
		var upBtn = e.getTarget('._upButton');
		var downBtn = e.getTarget('._downButton');
		var s = this.getStore();
		var rec = s.getAt(row);
		if (upBtn) {
			if (row > 0) {
				var uprec = s.getAt(row - 1);
				switchRank(uprec.id, rec.id)
			}
			return false;
		}
		if (downBtn) {
			if (row < s.getCount() - 1) {
				var downrec = s.getAt(row + 1);
				switchRank(downrec.id, rec.id)
			}
			return false;
		}
	}, this);
}

Ext.extend(HotQuestionCusPanel, Ext.grid.GridPanel, {
	init : function() {
		this.getStore().load({
			params : {
				mode : 2
			}
		});
		var self = this;
		Ext.Ajax.request({
			url : 'hot-question!getMode.action',
			success : function(resp) {
				self.selectByProgram = true;
				if ('1' == resp.responseText)
					self.autoRadio.setValue(true);
				else
					self.cusRadio.setValue(true);
			},
			failure : function(resp) {
				Ext.Msg.alert('错误', resp.responseText);
			}
		})
	},
	setMode : function(mode) {
		Ext.Ajax.request({
			url : 'hot-question!setMode.action',
			params : {
				mode : mode
			},
			success : function(resp) {
				Dashboard.setAlert('设置成功！');
			},
			failure : function(resp) {
				Ext.Msg.alert('错误', resp.responseText);
			}
		})
	}
});
