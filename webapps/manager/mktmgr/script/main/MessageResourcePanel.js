if (!window._dimChooserInitData) {
	var dimData = http_get('ontology-dimension!listAllowed.action');
	if (dimData) {
		dimData = Ext.decode(dimData);
		if(dimData)
			window._dimChooserInitData = dimData.data;
	}
}

var groupParam = {};
var groupData = Ext.decode(http_get('message-resource!listGroupParam.action'));
for ( var i = 0; i < groupData.length; i++) {
	groupParam[groupData[i].category] = groupData[i].isedit;
}

MessageResourcePanel = function() {
	var self = this;

	var store = new Ext.data.GroupingStore({
		groupField : 'categoryName',
		proxy : new Ext.data.HttpProxy({
			url : 'message-resource!list.action'
		}),
		remoteSort : true,
		remoteGroup : true,
		reader : new Ext.data.JsonReader({
			idProperty : 'id',
			root : 'data',
			fields : [ {
				name : 'id'
			}, {
				name : 'key'
			}, {
				name : 'message'
			}, {
				name : 'platform'
			}, {
				name : 'platformName'
			}, {
				name : 'categoryName'
			}, {
				name : 'title'
			}, {
				name : 'dimenTags',
				type : 'object'
			}, {
				name : 'description'
			} ]
		}),
		listeners : {
			beforewrite : function(store, data, rs) {
				if (!rs.get('categoryName'))
					return false;
			}
		}
	});
	var config = {
		store : store,
		columnLines : true,
		margins : '0 5 5 5',
		border : false,
		style : 'border-right: 1px solid ' + sys_bdcolor,
		tbar : [
				{
					iconCls : 'icon-add',
					text : '添加',
					handler : function() {
						AUTH.authorize(Dashboard.u(), "mkt.msgr.C", function() {
							_messageResourceGroup.enable();
							self.keyStore.setBaseParam('category', _messageResourceGroup.getRawValue());
							self.keyStore.load();
							self.formWin.show();
						}, self);
					}
				},
				{
					iconCls : 'icon-edit',
					text : '编辑',
					handler : function() {
						AUTH.authorize(Dashboard.u(), "mkt.msgr.M", function() {
							var rows = self.getSelectionModel().getSelections();
							if (rows.length != 1) {
								Ext.Msg.alert('提示', '请选择一条记录进行编辑！');
								return false;
							}
							self.editResource(rows[0]);
						}, self);
					}
				},
				{
					iconCls : 'icon-delete',
					text : '删除',
					handler : function() {
						AUTH.authorize(Dashboard.u(), "mkt.msgr.D", function() {
							var rows = self.getSelectionModel().getSelections();
							if (rows.length == 0) {
								Ext.Msg.alert('提示', '请至少选择一条记录！');
								return false;
							}
							Ext.Msg.confirm('提示', '确认删除这' + rows.length + '条记录？', function(btn) {
								if (btn == 'yes') {
									var ids = [];
									Ext.each(rows, function() {
										ids.push(this.id)
									});
									self.getEl().mask('正在删除...');
									Ext.Ajax.request({
										url : 'message-resource!delete.action',
										params : {
											ids : ids.join(',')
										},
										success : function(resp) {
											self.getEl().unmask();
											Dashboard.setAlert('删除成功');
											store.reload();
										},
										failure : function(resp) {
											self.getEl().unmask();
											Ext.Msg.alert('错误', resp.responseText);
										}
									});
								}
							});
						}, self);
					}
				},
				{
					iconCls : 'icon-refresh',
					text : '刷新',
					handler : function() {
						store.reload();
					}
				},
				{
					iconCls : 'icon-ok',
					text : '应用',
					handler : function() {
						Ext.Msg.confirm('提示', '确定应用？', function(btn) {
							if (btn == 'yes') {
								Ext.Ajax.request({
									url : 'message-resource!apply.action',
									success : function(resp) {
										Dashboard.setAlert('应用成功！');
									},
									failure : function(resp) {
										Ext.Msg.alert('错误', resp.responseText);
									}
								});
							}
						});
					}
				},
				'-',
				{
					iconCls : 'icon-ontology-import',
					text : '导入',
					handler : function() {
						AUTH.authorize(Dashboard.u(), "mkt.msgr.EXP", function() {
							self.uploadWin.show();
						}, self);
					}
				},
				{
					iconCls : 'icon-ontology-export',
					text : '导出',
					handler : function() {
						AUTH.authorize(Dashboard.u(), "mkt.msgr.EXP", function() {
							Ext.MessageBox.show({
								title : '提示',
								msg : '请选择导出excle文件格式',
								buttons : {
									yes : '07',
									no : '03',
									cancel : '取消'
								},
								fn : function(btn) {
									var type;
									if (btn == 'yes')
										type = 'xlsx';
									else if (btn == 'no')
										type = 'xls';
									if (type) {
										if (!self.downloadIFrame) {
											self.downloadIFrame = self.getEl().createChild({
												tag : 'iframe',
												style : 'display:none;'
											})
										}
										var _messageField = self.tbarEx.s_message;
										var _keyField = self.tbarEx.s_key;
										var _key = _keyField.getValue().trim();
										var _message = _messageField.getValue();
										var _dimTags = self.tbarDimChooser.getValue();
										var _dimTagsPart = '';
										if (_dimTags && _dimTags.length) {
											for ( var i = 0; i < _dimTags.length; i++) {
												_dimTagsPart += '&queryParam.dimenTags=' + _dimTags[i];
											}
										}
										self.downloadIFrame.dom.src = 'message-resource!export.action?ts=' + new Date().getTime() + '&type=' + type
												+ (_key ? '&queryParam.key=' + _key : '')
												+ (_message ? '&queryParam.message=' + encodeURIComponent(_message) : '') + _dimTagsPart;
									}
								},
								icon : Ext.MessageBox.QUESTION
							});
						}, self);
					}
				} ],
		// bbar : pagingBar,
		sm : new Ext.grid.RowSelectionModel(),
		loadMask : true,
		autoExpandColumn : '_mr_messageCol',
		colModel : new Ext.grid.ColumnModel({
			columns : [ new Ext.grid.RowNumberer(), {
				width : 200,
				header : '参数标识',
				dataIndex : 'key',
				renderer : function(data, metadata, record) {
					// build the qtip:
					//var title = record.get('title');
					//metadata.attr = 'ext:qtip="' + title + '"';
					return data;
				}
			}, {
				width : 200,
				header : '参数描述',
				dataIndex : 'description'
			}, {
				header : '参数内容',
				id : '_mr_messageCol',
				dataIndex : 'message'
			}, {
				width : 120,
				header : '维度',
				dataIndex : 'dimenTags',
				renderer : function(v, meta, rec) {
					return self.dimChooser.m.getNameStrById(v);
				}
			}, {
				width : 80,
				header : '类别',
				hidden : true,
				dataIndex : 'categoryName'
			} ]
		}),
		view : new Ext.grid.GroupingView({
			showGroupName : false,
			groupTextTpl : '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "个配置" : "个配置"]})'
		})
	};

	var tbarDimChooser = Ext.create({
		width : 180,
		name : 'dimension',
		xtype : 'dimensionbox',
		clearBtn : true,
		emptyText : '请选择维度',
		menuConfig : {
			labelAlign : 'top'
		},
		valueTransformer : function(value) {
			if (value == 'ALLDIM')
				return '';
			var tags = this.tags;
			var groupTags = [];
			if (tags)
				Ext.each(tags, function(t) {
					groupTags.push(t.id);
				});
			return groupTags;
		}
	});
	this.tbarDimChooser = tbarDimChooser;

	this.tbarEx = new Ext.Toolbar({
		items : [ '标识关键字:', {
			ref : 's_key',
			xtype : 'textfield',
			width : 120,
			emptyText : '输入关键字搜索...',
			enableKeyEvents : true,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == e.ENTER) {
						self.search();
					}
				}
			}
		}, '内容关键字:', {
			ref : 's_message',
			xtype : 'textfield',
			width : 120,
			emptyText : '输入关键字搜索...',
			enableKeyEvents : true,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == e.ENTER) {
						self.search();
					}
				}
			}
		}, '维度:', tbarDimChooser, ' ', {
			text : '搜索',
			iconCls : 'icon-search',
			handler : function() {
				self.search();
			}
		}, ' ', {
			text : '全部',
			iconCls : 'icon-search',
			handler : function() {
				self.search(true);
			}
		} ]
	});

	MessageResourcePanel.superclass.constructor.call(this, config);

	this.addListener('render', function() {
		this.tbarEx.render(this.tbar);
	}, this);
	this.on('rowdblclick', function(g, rowIdx, evt) {
		AUTH.authorize(Dashboard.u(), "mkt.msgr.M", function() {
			var rec = this.getStore().getAt(rowIdx);
			this.editResource(rec);
		}, self);
	}, this);
	// var _platCombo = new Ext.form.ComboBox({
	// triggerAction : 'all',
	// fieldLabel : '适用平台',
	// anchor : '60%',
	// name : 'platform',
	// editable : false,
	// allowBlank : false,
	// blankText : '请选择平台',
	// store : _platComboStore,
	// valueField : 'tag',
	// displayField : 'name'
	// });

	// 参数标识
	var _keyStore = new Ext.data.JsonStore({
		url : 'message-resource!listKeys.action',
		fields : [ 'tag', 'name', 'title' ]
	});
	this.keyConfigCombo = new Ext.form.ComboBox({
		triggerAction : 'all',
		fieldLabel : '参数标识',
		anchor : '60%',
		name : 'key',
		editable : false,
		lazyInit : true,
		allowBlank : false,
		blankText : '请选择参数标识',
		store : _keyStore,
		valueField : 'tag',
		displayField : 'name',
		tpl : '<tpl for="."><div class="x-combo-list-item" ext:qtip="{title}" >{name}</div></tpl>'
	});
	this.keyStore = _keyStore;

	// 组别
	var _groupComboStore = new Ext.data.JsonStore({
		url : 'message-resource!listGroup.action',
		fields : [ 'tag', 'name' ],
		autoLoad : true
	});
	var _messageResourceGroup = new Ext.form.ComboBox({
		fieldLabel : '参数组别',
		triggerAction : 'all',
		width : 80,
		editable : false,
		name : 'groupName',
		store : _groupComboStore,
		valueField : 'tag',
		displayField : 'name',
		listeners : {
			'select' : function(t, r) {
				self.keyStore.setBaseParam('category', t.value);
				self.keyConfigCombo.setEditable(groupParam[t.value] == "true" ? true : false);
				self.keyStore.load();
			}
		}
	});

	this.messageResourceGroup = _messageResourceGroup;

	this.dimChooser = Ext.create({
		fieldLabel : '参数维度',
		name : 'dimenTags',
		xtype : 'dimensionbox',
		clearBtn : true,
		allowBlank : false,
		menuConfig : {
			labelAlign : 'top'
		},
		value : [],
		valueTransformer : function(value) {
			var ret = [];
			if (value != 'ALLDIM') {
				if (value)
					Ext.each(value, function(t) {
						ret.push(t);
					});
			}
			return ret;
		}
	});
	
	this.fieldincre = 0;
	this.createTimableContentPanel = function(d) {
		this.fieldincre++;
		var textfield = {
			xtype : 'textarea',
			fieldLabel : '消息内容',
			height : 100,
			value : d && d.txt ? d.txt : '',
			name : 'message' + this.fieldincre,
			blankText : '内容不能为空'
		}
		var selfp = this
		var descfield = {
				xtype : 'textarea',
				fieldLabel : '参数描述',
				allowBlank : true,
				value : d && d.desc ? d.desc : '',
				style : 'margin-bottom:-9px',
				name : 'description'
		}
		var dtfield = {
			fieldLabel : '时间早于',
			name : 'msgdate' + this.fieldincre,
			xtype : 'twintriggerdatetimefield',
			value : d && d.date ? Date.parseDate(d.date, 'YmdHis') : '',
			onTrigger1Click : function() {
				var i = 0
				_editForm.items.each(function(item) {
					if (item.name == '_date_content')
						i++
					if (i > 1)
						return false
				})
				if (i > 1) {
					_editForm.remove(this.ownerCt)
					_editForm.doLayout()
				}
			},
			onTrigger2Click : function() {
				_editForm.add(selfp.createTimableContentPanel())
				_editForm.doLayout()
			},
			trigger1Class : 'x-form-clear-trigger',
			trigger2Class : 'x-form-add-trigger',
			minValue : new Date()
		}
		return {
			name : '_date_content',
			frame : false,
			border : false,
			bodyStyle : 'padding : 0px 0px; background-color:' + sys_bgcolor,
			layout : 'form',
			defaults : {
				anchor : '100%'
			},
			items : [ textfield, descfield, dtfield ]
		}
	}
	this.parseMsgValue = function(v) {
		// #[before 201212060831]
		// AAA
		// #[before 201212060832]
		// BBB
		// #[before 201212060832]
		// CCC
		if (!v)
			return []
		var p = /#\[before(\s+\d{4,14})?\]/
		var lines = v.split(/\r\n|\r|\n/)
		var ret = []
		var txtbuf = ''
		var date = null
		var result = null
		for ( var i = 0; i < lines.length; i++) {
			var l = lines[i]
			if ((result = p.exec(l)) != null) {
				if (txtbuf)
					ret.push({
						txt : txtbuf.trim(),
						date : date
					})
				txtbuf = ''
				date = result[1] ? result[1].trim() : ''
			} else if (date != null) {
				if (txtbuf)
					txtbuf += '\r\n'
				txtbuf += l
			}
		}
		if (txtbuf) {
			ret.push({
				txt : txtbuf.trim(),
				date : date
			})
		}
		return ret;
	}
	this.wrapMsgValue = function(data) {
		// #[before 201212060831]
		// AAA
		// #[before 201212060832]
		// BBB
		// #[before 201212060832]
		// CCC
		var ret = ''
		Ext.each(data, function(d) {
			if (d.txt)
				ret += '#[before' + (d.date ? ' ' + d.date.trim() : '') + ']\r\n' + d.txt + '\r\n'
		})
		return ret.trim();
	}

	var _editForm = new Ext.form.FormPanel({
		frame : false,
		border : false,
		labelWidth : 75,
		autoHeight : true,
		waitMsgTarget : true,
		bodyStyle : 'padding : 3px 10px; background-color:' + sys_bgcolor,
		defaults : {
			anchor : '96%'
//			allowBlank : false
		},
		loadData : function(rec) {
			this.getForm().loadRecord(rec)
			var datemgs = []
			var rawmsg = rec.get('message')
			var descmsg = rec.get('description')
			if (rawmsg) {
				datemgs = self.parseMsgValue(rawmsg)
			}
			if (!datemgs.length)
				datemgs.push({
					txt : rawmsg || '',
					desc : descmsg || ''
				})
			var removals = []
			this.items.each(function(item) {
				if (item.name == '_date_content')
					removals.push(item)
			})
			Ext.each(removals, function(rm) {
				this.remove(rm)
			}, this)
			Ext.each(datemgs, function(m) {
				this.add(self.createTimableContentPanel(m))
			}, this)
			this.doLayout()
		},
		items : [ new Ext.form.Hidden({
			name : 'id'
		}), _messageResourceGroup, this.dimChooser, self.keyConfigCombo, new Ext.form.Hidden({
			name : 'message'
		}), this.createTimableContentPanel() ],
		tbar : [ {
			text : '保存',
			iconCls : 'icon-add',
			handler : function() {
				this.disable();
				self.saveResource(this);
			}
		}, '-', {
			text : '上一个',
			iconCls : 'icon-prev',
			handler : function() {
				prevNext(true);
			}
		}, {
			text : '下一个',
			iconCls : 'icon-next',
			handler : function() {
				prevNext();
			}
		} ]
	});

	var prevNext = function(isPrev) {
		var sm = self.getSelectionModel();
		var selected = isPrev ? sm.selectPrevious() : sm.selectNext();
		if (selected)
			_editForm.loadData(self.getSelectionModel().getSelected());
		else
			Dashboard.setAlert('没有' + (isPrev ? '上' : '下') + '一个了');
	};

	var winCfg = {
		defaults : {
			border : false
		},
		modal : true,
		plain : true,
		shim : true,
		closable : true,
		closeAction : 'hide',
		collapsible : true,
		resizable : false,
		draggable : true,
		animCollapse : true,
		constrainHeader : true,
		shadow : false,
		listeners : {
			'beforehide' : function(p) {
				p.form.getForm().reset();
			}
		}
	};
	var _formWin = new Ext.Window(Ext.apply({
		width : 420,
		title : '编辑服务参数',
		items : [ _editForm ]
	}, winCfg));

	_formWin.form = _editForm;
	this.formWin = _formWin;

	var menu = new Ext.menu.Menu({
		items : [ {
			text : '添加此项配置',
			iconCls : 'icon-add',
			handler : function() {
				_messageResourceGroup.disable();
				_formWin.show();
			}
		} ]
	});
	this.on('groupcontextmenu', function(g, gf, gv, e) {
		self.keyStore.setBaseParam('category', gv);
		self.keyStore.load();
		self.keyConfigCombo.setEditable(groupParam[gv] == "true" ? true : false);
		e.preventDefault();
		menu.showAt(e.getXY());
		var values = [ {
			id : 'name',
			value : gv
		} ];
		var recs = self.getStore().getRange();
		for ( var i = 0; i < recs.length; i++) {
			if (recs[i].get('name') == gv) {
				values.push({
					id : 'key',
					value : recs[i].get('key')
				});
				break;
			}
		}
		_messageResourceGroup.setValue(gv);
		_editForm.getForm().setValues(values);
	});

	var _uploadForm = null;
	_uploadForm = new Ext.FormPanel({
		fileUpload : true,
		frame : false,
		border : false,
		labelWidth : 70,
		autoHeight : true,
		waitMsgTarget : true,
		bodyStyle : 'padding : 3px 10px; background-color:' + sys_bgcolor,
		defaults : {
			anchor : '95%',
			allowBlank : false,
			msgTarget : 'side'
		},
		items : [ {
			xtype : 'fileuploadfield',
			emptyText : '选择文件',
			fieldLabel : '文件路径',
			name : 'sheetFile',
			buttonText : '',
			buttonCfg : {
				iconCls : 'icon-upload-excel'
			},
			validator : function(v) {
				v = v.trim();
				if (v) {
					if (v.indexOf('.xls') < 0)
						return '文件格式不正确';
					return true;
				}
				return '请选择文件';
			}
		} ],
		tbar : [ {
			text : '导入',
			iconCls : 'icon-import',
			handler : function() {
				var btn = this;
				btn.disable();
				var params = {};
				if (_uploadForm.getTopToolbar().clearAllChk.checked)
					params.clear = 'true';
				if (_uploadForm.getForm().isValid()) {
					_uploadForm.getForm().submit({
						url : 'message-resource!_import.action',
						waitMsg : '正在导入服务参数...',
						params : params,
						success : function(form, a) {
							var o = Ext.decode(a.response.responseText);
							var result = o.data;
							var msg = '成功导入' + result.succTotal + '条记录。';
							if (result.failed.length > 0)
								msg += '第' + result.failed.join(',') + '行数据有误，导入失败。';
							if (o.message != '0')
								msg += '有' + o.message + '行数据重复，导入失败。';
							Ext.Msg.alert('导入结果', msg, function() {
								btn.enable();
								_uploadWin.hide();
								self.getStore().reload();
							});
						},
						failure : function(form, act) {
							Ext.Msg.alert('错误', act.result.message);
							btn.enable();
						}
					});
				} else {
					Ext.Msg.alert('错误', '您尚未正确完整的输入表单内容，请检查！', function() {
						btn.enable();
					});
				}
			}
		}, '-', {
			xtype : 'checkbox',
			name : 'clearAll',
			ref : 'clearAllChk',
			boxLabel : '清空导入'
		} ]
	});
	var _uploadWin = new Ext.Window(Ext.apply({
		title : '导入服务参数',
		items : [ _uploadForm ],
		width : 400
	}, winCfg));
	_uploadWin.form = _uploadForm;
	this.uploadWin = _uploadWin;
};

Ext.extend(MessageResourcePanel, Ext.grid.GridPanel, {
	init : function() {
		this.getStore().load();
	},
	editResource : function(rec) {
		this.keyStore.setBaseParam('category', rec.data.categoryName);
		this.keyStore.load();
		this.keyConfigCombo.setEditable(groupParam[rec.data.categoryName] == "true" ? true : false);
		this.messageResourceGroup.setValue(rec.data.categoryName);
		// if (rec.data.isEdit)
		// this.keyConfigCombo.setEditable(rec.data.isEdit);
		this.messageResourceGroup.disable();
		// this.keyStore.setBaseParam('category', rec.data.categoryName);
		// this.keyStore.load();
		this.formWin.show();
		// this.formWin.form.getForm().loadRecord(rec);
		this.formWin.form.loadData(rec);
		this.dimChooser.setValue(rec.get('dimenTags'));
	},
	afterSave : function(btn) {
		btn.enable();
		this.formWin.hide();
	},
	saveResource : function(btn) {
		var self = this;
		var f = this.formWin.form.getForm();
		if (!f.isValid()) {
			Ext.Msg.alert('提示', '您尚未正确完整的输入表单内容，请检查！');
			btn.enable();
			return false;
		}
		var vals = f.getFieldValues();
		var datemsg = []
		for ( var key in vals) {
			if (key && key.indexOf('message') == 0 && key.length > 'message'.length) {
				var dkey = 'msgdate' + key.substring('message'.length)
				var d = {
					txt : vals[key],
					date : vals[dkey]
				}
				if (d.date)
					d.date = d.date.format('YmdHis')
				datemsg.push(d)
				delete vals[key]
				delete vals[dkey]
			}
		}
		datemsg.sort(function(d1, d2) {
			if (!d1.date)
				return 1;
			if (!d2.date)
				return -1;
			return parseInt(d1.date) - parseInt(d2.date)
		})
		if (datemsg.length == 1 && !datemsg[0].date) {
			vals.message = datemsg[0].txt;
		} else
			vals.message = this.wrapMsgValue(datemsg);
		if (!vals.message) {
			Ext.Msg.alert('提示', '您尚未正确完整的输入表单内容，请检查！');
			btn.enable();
			return false;
		}
		delete vals.groupName;
		if (!vals.id)
			delete vals.id;
		Ext.Ajax.request({
			url : 'message-resource!save.action',
			params : {
				'data' : Ext.encode(vals)
			},
			success : function(resp) {
				var ret = Ext.decode(resp.responseText);
				if (ret.message && ret.message == 'fail') {
					Ext.Msg.alert('错误', '保存失败，已存在相应的服务参数！');
					btn.enable();
				} else {
					Dashboard.setAlert('保存成功！');
					self.getStore().reload();
					self.afterSave(btn);
				}
			},
			failure : function(resp) {
				Ext.Msg.alert('错误', resp.responseText);
				self.afterSave(btn);
			}
		});
	},
	search : function(showAll) {
		var _messageField = this.tbarEx.s_message;
		var _keyField = this.tbarEx.s_key;
		if (showAll) {
			_messageField.setValue('');
			_keyField.setValue('');
			this.tbarDimChooser.setValue([]);
		}
		var store = this.getStore();
		for ( var key in store.baseParams) {
			if (key && key.indexOf('queryParam.') != -1)
				delete store.baseParams[key];
		}
		var _key = _keyField.getValue().trim();
		var _message = _messageField.getValue();
		var _dimTags = this.tbarDimChooser.getValue();
		if (_key)
			store.setBaseParam('queryParam.key', _key);
		if (_message)
			store.setBaseParam('queryParam.message', _message);
		if (_dimTags && _dimTags.length)
			store.setBaseParam('queryParam.dimenTags', _dimTags);
		store.load();
	},
	externalLoad : function(v) {
		var platform = arguments[0];
		this.store.baseParams['queryParam.platform'] = platform;
		var platCombo = this.tbarEx.s_plat;
		if (platCombo.getStore().getCount() == 0)
			platCombo.getStore().load({
				callback : function() {
					platCombo.setValue(platform);
				}
			});
		else
			platCombo.setValue(platform);
		this.store.load({
			params : {
				start : 0,
				limit : 20
			}
		});
	}
});
