NoticePanel = function() {

	var self = this;

	var store = new Ext.data.Store({
				proxy : new Ext.data.HttpProxy({
							url : 'notice!list.action'
						}),
				reader : new Ext.data.JsonReader({
							idProperty : 'id',
							root : 'data',
							fields : [{
										name : 'id',
										type : 'string'
									}, {
										name : 'title',
										type : 'string'
									}, {
										name : 'content',
										type : 'string'
									}, {
										name : 'rank',
										type : 'int'
									}, {
										name : 'platform',
										type : 'string'
									}, {
										name : 'platformName',
										type : 'string'
									}, {
										name : 'location',
										type : 'string'
									}, {
										name : 'locationName',
										type : 'string'
									}]
						}),
				autoLoad : true
			});
	var sm = new Ext.grid.RowSelectionModel();
	var config = {
		id : 'noticePanel',
		store : store,
		margins : '0 5 5 5',
		border : false,
		autoExpandColumn : 'content_col',
		tbar : [{
					iconCls : 'icon-add',
					text : '添加公告',
					handler : function() {
						self.formWin.show();
					}
				}, {
					iconCls : 'icon-edit',
					text : '编辑公告',
					handler : function() {
						var rows = self.getSelectionModel().getSelections();
						if (rows.length != 1) {
							Ext.Msg.alert('提示', '请选择一条记录进行编辑！');
							return false;
						}
						self.editNotice(rows[0]);
					}
				}, {
					iconCls : 'icon-delete',
					text : '删除公告',
					handler : function() {
						var rows = self.getSelectionModel().getSelections();
						if (rows.length == 0) {
							Ext.Msg.alert('提示', '请至少选择一条记录！');
							return false;
						}
						Ext.Msg.confirm('提示', '确认删除这' + rows.length + '条记录？',
								function(btn) {
									if (btn == 'yes') {
										var ids = [];
										Ext.each(rows, function() {
													ids.push(this.id)
												});
										Ext.Ajax.request({
													url : 'notice!delete.action',
													params : {
														ids : ids.join(',')
													},
													success : function(resp) {
														Ext.Msg.alert('提示',
																'删除成功！',
																function() {
																	store
																			.reload();
																});
													},
													failure : function(resp) {
														Ext.Msg
																.alert(
																		'错误',
																		resp.responseText);
													}
												})
									}
								});
					}
				}, {
					iconCls : 'icon-refresh',
					text : '刷新',
					handler : function() {
						store.reload();
					}
				}, {
					iconCls : 'icon-ok',
					text : '应用',
					handler : function() {
						Ext.Msg.confirm('提示', '确认应用？', function(btn) {
									if (btn == 'yes') {
										Ext.Ajax.request({
													url : 'notice!apply.action',
													success : function(resp) {
														Dashboard
																.setAlert('应用成功！');
													},
													failure : function(resp) {
														Ext.Msg
																.alert(
																		'错误',
																		resp.responseText);
													}
												})
									}
								});
					}
				}],
		sm : sm,
		loadMask : true,
		colModel : new Ext.grid.ColumnModel({
			columns : [new Ext.grid.RowNumberer(), {
						header : '公告标题',
						dataIndex : 'title',
						width : 160
					}, {
						id : 'content_col',
						header : '问题内容',
						dataIndex : 'content'
					}, {
						header : '平台',
						dataIndex : 'platform',
						width : 120,
						renderer : function(v, meta, rec) {
							return rec.get('platformName');
						}
					}, {
						header : '归属地',
						dataIndex : 'location',
						width : 120,
						renderer : function(v, meta, rec) {
							return rec.get('locationName');
						}
					}, {
						header : '操作',
						width : 85,
						renderer : function() {
							return "<a class='_upButton' href='javascript:void(0);' onclick='javscript:return false;'>上移</a>&nbsp;&nbsp;&nbsp;"
									+ "<a class='_downButton' href='javascript:void(0);' onclick='javscript:return false;'>下移</a>";
						}
					}]
		})
	};

	var _editForm = new Ext.form.FormPanel({
				frame : false,
				border : false,
				labelWidth : 75,
				autoHeight : true,
				waitMsgTarget : true,
				bodyStyle : 'padding : 3px 10px; background-color:' + sys_bgcolor,
				defaults : {
					anchor : '96%',
					allowBlank : false
				},
				items : [new Ext.form.Hidden({
									name : 'id'
								}), {
							xtype : 'textfield',
							fieldLabel : '公告标题',
							name : 'title',
							blankText : '公告标题不能为空'
						}, {
							xtype : 'textarea',
							fieldLabel : '公告内容',
							height : 100,
							name : 'content',
							blankText : '公告内容不能为空'
						}, {
							xtype : 'combo',
							triggerAction : 'all',
							allowBlank : false,
							editable : false,
							name : 'platform',
							fieldLabel : '平台',
							store : new Ext.data.JsonStore({
										url : 'notice!listPlatformDims.action',
										fields : ['tag', 'name'],
										autoLoad : true
									}),
							valueField : 'tag',
							displayField : 'name'
						}, {
							xtype : 'combo',
							triggerAction : 'all',
							allowBlank : false,
							editable : false,
							name : 'location',
							fieldLabel : '归属地',
							store : new Ext.data.JsonStore({
										url : 'notice!listLocationDims.action',
										fields : ['tag', 'name'],
										autoLoad : true
									}),
							valueField : 'tag',
							displayField : 'name'
						}],
				tbar : [{
							text : '保存',
							iconCls : 'icon-add',
							handler : function() {
								this.disable();
								self.saveNotice(this);
							}
						}]
			});
	var _formWin = new Ext.Window({
				defaults : {
					border : false
				},
				modal : true,
				plain : true,
				shim : true,
				closable : true,
				closeAction : 'hide',
				collapsible : true,
				resizable : false,
				draggable : true,
				animCollapse : true,
				constrainHeader : true,
				shadow : false,
				listeners : {
					'beforehide' : function(p) {
						p.form.getForm().reset();
					}
				},
				width : 420,
				title : '编辑消息资源',
				items : [_editForm]
			});
	_formWin.form = _editForm;
	this.formWin = _formWin;

	NoticePanel.superclass.constructor.call(this, config);
	this.on('rowdblclick', function(g, rowIdx, evt) {
				var rec = this.getStore().getAt(rowIdx);
				this.editNotice(rec);
			}, this);

	var switchRank = function(id1, id2) {
		Ext.Ajax.request({
					url : 'notice!switchRank.action',
					params : {
						ids : id1 + ',' + id2
					},
					success : function(resp) {
						self.getStore().reload();
					},
					failure : function(resp) {
						Ext.Msg.alert('错误', resp.responseText);
					}
				});
	}
	this.on('cellclick', function(grid, row, col, e) {
				var upBtn = e.getTarget('._upButton');
				var downBtn = e.getTarget('._downButton');
				var s = this.getStore();
				var rec = s.getAt(row);
				if (upBtn) {
					if (row > 0) {
						var uprec = s.getAt(row - 1);
						switchRank(uprec.id, rec.id)
					}
					return false;
				}
				if (downBtn) {
					if (row < s.getCount() - 1) {
						var downrec = s.getAt(row + 1);
						switchRank(downrec.id, rec.id)
					}
					return false;
				}
			}, this);
}

Ext.extend(NoticePanel, Ext.grid.GridPanel, {
			editNotice : function(rec) {
				this.formWin.show();
				this.formWin.form.getForm().loadRecord(rec);
			},
			afterSave : function(btn) {
				btn.enable();
				this.formWin.hide();
			},
			saveNotice : function(btn) {
				var self = this;
				var f = this.formWin.form.getForm();
				if (!f.isValid()) {
					Ext.Msg.alert('提示', '您尚未正确完整的输入表单内容，请检查！');
					btn.enable();
					return false;
				}
				var vals = f.getFieldValues();
				if (!vals.id)
					delete vals.id;
				Ext.Ajax.request({
							url : 'notice!save.action',
							params : {
								'data' : Ext.encode(vals)
							},
							success : function(resp) {
								Dashboard.setAlert('保存成功！');
								self.getStore().reload();
								self.afterSave(btn);
							},
							failure : function(resp) {
								Ext.Msg.alert('错误', resp.responseText);
								self.afterSave(btn);
							}
						});
			}
		});