Ext.namespace('obj.detail')
obj.detail.P4Win = Ext.extend(Ext.Window, {
	p4EditUrl : '../kbmgr/p4editor/index.jsp',
	getFrame : function() {
		return Ext.getDom(this._id);
	},
	initComponent : function() {
		window.p4Saved = this.onSave.dg(this);
		this._id = Ext.id();
		this.tbar = ['设置大小:', {
					text : '500*500',
					handler : this.setSize.dg(this, [500, 500])
				}]
		this.items = [{
			html : "<iframe id='"
					+ this._id
					+ "' src='' height='100%' width='100%' frameborder='no' border='0' marginwidth='0' marginheight='0' scrolling='auto' allowtransparency='yes'></iframe>"
		}]
		obj.detail.P4Win.superclass.initComponent.call(this)
	},
	editP40 : function(id) {
		this.getFrame().src = this.p4EditUrl + '?' + (id ? 'p4id=' + id : '')
				+ '&url=' + encodeURIComponent(('../../kbmgr/p4SaveSuc.jsp'))
				+ '&_t=' + new Date().getTime();
	},
	editP4 : function(id) {
		if (!this.p4EditUrl) {
			this.p4EditUrl = 'p4editor/index.jsp'
//			Ext.Ajax.request({
//						url : 'property!get.action?key=kbmgr.robot.p4editURL',
//						success : function(response) {
//							this.p4EditUrl = response.responseText
//							this.editP40(id);
//						},
//						scope : this
//					})
		} else {
			this.editP40(id);
		}
	},
	onSave : function(id) {
		this.fireEvent('save', id)
	},

	layout : 'fit',
	title : 'P4编辑器',
	width : 500,
	height : 500,
	defaults : {
		border : false
	},

	plain : true,// 方角 默认
	modal : true,
	plain : true,
	shim : true,
	closeAction : 'hide',
	closable : true, // 关闭
	resizable : true,// 改变大小
	constrainHeader : true
})