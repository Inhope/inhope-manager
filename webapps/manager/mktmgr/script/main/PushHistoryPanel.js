PushHistoryPanel = function() {
	var self = this;

	var store = new Ext.data.Store({
				proxy : new Ext.data.HttpProxy({
							url : 'push-history!list.action'
						}),
				reader : new Ext.data.JsonReader({
							idProperty : 'id',
							root : 'data',
							fields : [{
										name : 'id',
										type : 'string'
									}, {
										name : 'user',
										type : 'string'
									}, {
										name : 'robot',
										type : 'string'
									}, {
										name : 'sendTime',
										type : 'string',
										convert : function(v) {
											return v.replace('T', ' ');
										}
									}, {
										name : 'content',
										type : 'string',
										mapping : 'message.content'
									}]
						})
			});
	var sm = new Ext.grid.RowSelectionModel();
	var pagingBar = new Ext.PagingToolbar({
				store : store,
				displayInfo : true,
				pageSize : 20,
				prependButtons : true,
				beforePageText : '第',
				afterPageText : '页，共{0}页',
				displayMsg : '第{0}到{1}条记录，共{2}条',
				emptyMsg : "没有记录"
			});
	var config = {
		id : 'pushHistoryPanel',
		columnLines : true,
		store : store,
		margins : '0 5 5 5',
		border : false,
		style : 'border-right: 1px solid ' + sys_bdcolor,
		tbar : new Ext.Toolbar({
					items : ['用户账号:', {
								id : 's_user',
								xtype : 'textfield',
								width : 120,
								emptyText : '输入用户账号搜索...'
							}, '机器人账号:', {
								id : 's_robot',
								xtype : 'textfield',
								width : 120,
								emptyText : '输入机器人账号搜索...'
							}, '发送时间:从', {
								id : 's_send_bdate',
								xtype : 'datefield',
								width : 95,
								emptyText : '开始时间',
								format : 'Y-m-d'
							}, '到', {
								id : 's_send_edate',
								xtype : 'datefield',
								width : 95,
								emptyText : '结束时间',
								format : 'Y-m-d'
							}, ' ', {
								text : '搜索',
								iconCls : 'icon-search',
								handler : function() {
									self.search();
								}
							}, ' ', {
								text : '全部',
								iconCls : 'icon-search',
								handler : function() {
									self.search(true);
								}
							}]
				}),
		bbar : pagingBar,
		sm : sm,
		loadMask : true,
		autoExpandColumn : 'content',
		colModel : new Ext.grid.ColumnModel({
					columns : [new Ext.grid.RowNumberer(), {
								width : 200,
								header : '用户账号',
								dataIndex : 'user'
							}, {
								width : 200,
								header : '机器人账号',
								dataIndex : 'robot'
							}, {
								width : 75,
								header : '发送时间',
								dataIndex : 'sendTime'
							}, {
								header : '内容',
								id : 'content',
								dataIndex : 'content'
							}]
				})
	};

	PushHistoryPanel.superclass.constructor.call(this, config);

}

Ext.extend(PushHistoryPanel, Ext.grid.GridPanel, {
			init : function() {
				this.getStore().load();
			},
			search : function(showAll) {
				var _tbar = this.getTopToolbar();
				var _userField = _tbar.findById('s_user');
				var _robotField = _tbar.findById('s_robot');
				var _startField = _tbar.findById('s_send_bdate');
				var _expireField = _tbar.findById('s_send_edate');
				if (showAll) {
					_userField.setValue('');
					_robotField.setValue('');
					_startField.setValue('');
					_expireField.setValue('');
				}
				var store = this.getStore();
				for (var key in store.baseParams) {
					if (key && key.indexOf('queryParam.') != -1)
						delete store.baseParams[key];
				}
				var _user = _userField.getValue().trim();
				var _robot = _robotField.getValue().trim();
				var _start = _startField.getValue();
				var _expire = _expireField.getValue();
				if (_user)
					store.setBaseParam('queryParam.user', _user);
				if (_robot)
					store.setBaseParam('queryParam.robot', _robot);
				if (_start)
					store.setBaseParam('queryParam.begin', _start);
				if (_expire)
					store.setBaseParam('queryParam.end', _expire);
				store.load();
			}
		});