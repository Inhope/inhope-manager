PushMessagePanel = function() {
	var self = this;
	this.helper = {
		TYPE_SPEC : 'spec',
		TYPE_ALL : 'all',
		types : [ [ 'all', '群发' ], [ 'spec', '指定用户' ] ],
		specP4 : [ [ 'p4_invite_friend', '默认好友邀请地址' ] ],
		getTypes4Search : function() {
			var arr = [];
			arr.push([ '', '全部' ]);
			for ( var i = 0; i < this.types.length; i++) {
				arr.push(this.types[i]);
			}
			return arr;
		},
		getTypeName : function(typeVal) {
			if (!this.typesMap) {
				this.typesMap = Dashboard.utils.arrayDataToMap([ this.types ]);
			}
			return this.typesMap[typeVal];
		},
		getSpecP4Name : function(val) {
			if (!this.specP4Map) {
				this.specP4Map = Dashboard.utils
						.arrayDataToMap([ this.specP4 ]);
			}
			return this.specP4Map[val];
		}
	};

	var store = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({
			url : 'push-message!list.action'
		}),
		reader : new Ext.data.JsonReader({
			idProperty : 'id',
			root : 'data',
			fields : [ {
				name : 'id'
			}, {
				name : 'content'
			}, {
				name : 'p4'
			}, {
				name : 'platform'
			}, {
				name : 'arrival',
				type : 'int'
			}, {
				name : 'begin',
				convert : function(v) {
					return v.replace('T', ' ');
				}
			}, {
				name : 'expire',
				convert : function(v) {
					return v.replace('T', ' ');
				}
			}, {
				name : 'type'
			}, {
				name : 'createTime',
				convert : function(v) {
					return v.replace('T', ' ');
				}
			} ]
		})
	});
	var pagingBar = new Ext.PagingToolbar({
		store : store,
		displayInfo : true,
		pageSize : 20,
		prependButtons : true,
		beforePageText : '第',
		afterPageText : '页，共{0}页',
		displayMsg : '第{0}到{1}条记录，共{2}条',
		emptyMsg : "没有记录"
	});
	var config = {
		store : store,
		columnLines : true,
		margins : '0 5 5 5',
		border : false,
		style : 'border-right: 1px solid ' + sys_bdcolor,
		tbar : [
				{
					iconCls : 'icon-add',
					text : '添加消息',
					handler : function() {
						self.addMsg();
					}
				},
				{
					iconCls : 'icon-delete',
					text : '删除消息',
					handler : function() {
						var rows = self.getSelectionModel().getSelections();
						if (rows.length == 0) {
							Ext.Msg.alert('提示', '请至少选择一条记录！');
							return false;
						}
						Ext.Msg.confirm('提示', '确认删除这' + rows.length + '条记录？',
								function(btn) {
									if (btn == 'yes') {
										var ids = [];
										Ext.each(rows, function() {
											ids.push(this.id);
										});
										self.getEl().mask('正在删除...');
										Ext.Ajax.request({
											url : 'push-message!delete.action',
											params : {
												ids : ids.join(',')
											},
											success : function(resp) {
												self.getEl().unmask();
												Dashboard.setAlert('删除成功');
												store.reload();
											},
											failure : function(resp) {
												self.getEl().unmask();
												Ext.Msg.alert('错误',
														resp.responseText);
											}
										});
									}
								});
					}
				}, {
					iconCls : 'icon-refresh',
					text : '刷新',
					handler : function() {
						store.reload();
					}
				} ],
		bbar : pagingBar,
		sm : new Ext.grid.RowSelectionModel(),
		loadMask : true,
		autoExpandColumn : '__push__content',
		colModel : new Ext.grid.ColumnModel(
				{
					columns : [
							new Ext.grid.RowNumberer(),
							{
								header : '内容',
								id : '__push__content',
								dataIndex : 'content'
							},
							{
								header : '平台',
								width : 90,
								dataIndex : 'platform',
								renderer : function(v) {
									var platTag = Dashboard.platformMap[v];
									if (platTag)
										return platTag.name;
									return 'N/A';
								}
							},
							{
								header : '成功数',
								width : 70,
								dataIndex : 'arrival'
							},
							{
								width : 130,
								header : '起始时间',
								dataIndex : 'begin'
							},
							{
								width : 130,
								header : '过期时间',
								dataIndex : 'expire'
							},
							{
								width : 70,
								header : '消息类型',
								dataIndex : 'type',
								renderer : function(v) {
									var ret = self.helper.getTypeName(v);
									if (v == self.helper.TYPE_SPEC)
										ret += "<br/><a class='_viewButton' href='javascript:void(0);' onclick='javscript:return false;'>查看</a>";
									return ret;
								}
							}, {
								width : 130,
								header : '创建时间',
								dataIndex : 'createTime'
							} ]
				})
	};

	var _now = new Date();

	var _typeCombo = new Ext.form.ComboBox({
		triggerAction : 'all',
		fieldLabel : '消息类别',
		anchor : '60%',
		name : 'type',
		editable : false,
		allowBlank : false,
		blankText : '请选择类别',
		value : 'all',
		mode : 'local',
		store : new Ext.data.ArrayStore({
			fields : [ 'type', 'displayText' ],
			data : this.helper.types
		}),
		valueField : 'type',
		displayField : 'displayText'
	});
	var _p4Combo = new Ext.form.ComboBox({
		triggerAction : 'all',
		hidden : true,
		fieldLabel : 'p4地址',
		anchor : '98%',
		name : 'p4',
		allowBlank : true,
		mode : 'local',
		store : new Ext.data.ArrayStore({
			fields : [ 'p4url', 'displayText' ],
			data : this.helper.specP4
		}),
		valueField : 'p4url',
		displayField : 'displayText'
	});
	var _receiversField = new Ext.form.TextArea({
		fieldLabel : '消息接受者',
		name : 'receivers',
		anchor : '98%',
		hideMode : 'display',
		emptyText : '多个接受者请回车换行隔开',
		height : 150
	});
	var _platformStore = new Ext.data.JsonStore({
		url : 'service-module!listDimenstion.action?dim=platform',
		fields : [ 'tag', 'name' ],
		autoLoad : true
	});

	var _editForm = new Ext.form.FormPanel({
		frame : false,
		border : false,
		labelWidth : 80,
		autoHeight : true,
		waitMsgTarget : true,
		bodyStyle : 'padding : 3px 10px; background-color:' + sys_bgcolor,
		items : [ new Ext.form.Hidden({
			name : 'id'
		}), {
			xtype : 'textarea',
			fieldLabel : '消息内容',
			height : 100,
			allowBlank : false,
			name : 'content',
			blankText : '消息内容不能为空',
			emptyText : '消息内容请控制在70字以内',
			anchor : '98%'
		}, new Ext.form.ComboBox({
			triggerAction : 'all',
			fieldLabel : '适用平台',
			anchor : '98%',
			name : 'platform',
			editable : false,
			allowBlank : false,
			blankText : '请选择平台',
			store : _platformStore,
			valueField : 'tag',
			displayField : 'name',
			listeners : {
				'select' : function(combo, rec, idx) {
					var val = combo.getValue();
					if ('msn' == val)
						Dashboard.utils.showFormItems([ _p4Combo ]);
					else
						Dashboard.utils.hideFormItems([ _p4Combo ]);
				}
			}
		}), _p4Combo, {
			xtype : 'xdatetime',
			allowBlank : false,
			fieldLabel : '起始时间',
			dateFormat : 'Y-m-d',
			timeFormat : 'H:i',
			value : new Date(),
			name : 'begin',
			anchor : '80%'
		}, {
			xtype : 'xdatetime',
			allowBlank : false,
			fieldLabel : '过期时间',
			dateFormat : 'Y-m-d',
			timeFormat : 'H:i',
			value : new Date().setHours(_now.getHours() + 72),
			name : 'expire',
			anchor : '80%'
		}, _typeCombo, _receiversField ],
		tbar : [ {
			text : '保存',
			iconCls : 'icon-add',
			handler : function() {
				this.disable();
				self.saveMsg(this);
			}
		} ]
	});
	_editForm.typeCombo = _typeCombo;
	_editForm.receiversField = _receiversField;

	_typeCombo.on('select', function(combo, rec, idx) {
		var type = _typeCombo.getValue();
		var receiversField = _editForm.receiversField;
		if (type == self.helper.TYPE_SPEC) {
			Dashboard.utils.showFormItems([ receiversField ]);
		} else {
			Dashboard.utils.hideFormItems([ receiversField ]);
		}
		receiversField.validate();
	});

	var _formWin = new Ext.Window({
		width : 420,
		title : '编辑消息',
		defaults : {
			border : false
		},
		modal : true,
		plain : true,
		shim : true,
		closable : true,
		closeAction : 'hide',
		collapsible : true,
		resizable : false,
		draggable : true,
		animCollapse : true,
		constrainHeader : true,
		shadow : false,
		listeners : {
			'beforehide' : function(p) {
				p.editForm.getForm().reset();
			}
		},
		items : [ _editForm ]
	});

	_formWin.editForm = _editForm;
	this.formWin = _formWin;

	PushMessagePanel.superclass.constructor.call(this, config);

	this.on('rowdblclick', function(g, rowIdx, evt) {
		var rec = this.getStore().getAt(rowIdx);
		this.editMsg(rec);
	}, this);

	this.tbarEx = new Ext.Toolbar({
		items : [ '内容关键字:', {
			ref : 's_content',
			xtype : 'textfield',
			width : 120,
			emptyText : '输入关键字搜索...'
		}, '平台:', new Ext.form.ComboBox({
			ref : 's_plat',
			value : '',
			emptyText : '请选择平台',
			triggerAction : 'all',
			width : 100,
			editable : false,
			store : _platformStore,
			valueField : 'tag',
			displayField : 'name'
		}), '消息类型:', new Ext.form.ComboBox({
			ref : 's_type',
			triggerAction : 'all',
			width : 80,
			editable : false,
			value : '',
			mode : 'local',
			store : new Ext.data.ArrayStore({
				fields : [ 'type', 'displayText' ],
				data : this.helper.getTypes4Search()
			}),
			valueField : 'type',
			displayField : 'displayText'
		}), '有效发送时间:从', {
			ref : 's_bdate',
			xtype : 'datefield',
			width : 100,
			emptyText : '选择开始时间',
			format : 'Y-m-d'
		}, '到', {
			ref : 's_edate',
			xtype : 'datefield',
			width : 100,
			emptyText : '选择过期时间',
			format : 'Y-m-d'
		}, ' ', {
			text : '搜索',
			iconCls : 'icon-search',
			handler : function() {
				self.search();
			}
		}, ' ', {
			text : '全部',
			iconCls : 'icon-search',
			handler : function() {
				self.search(true);
			}
		} ]
	});
	this.addListener('render', function() {
		this.tbarEx.render(this.tbar);
	}, this);

	var _assginStore = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({
			url : 'push-message!listSpec.action'
		}),
		reader : new Ext.data.JsonReader({
			idProperty : 'id',
			root : 'data',
			fields : [ {
				name : 'id',
				type : 'string'
			}, {
				name : 'user',
				type : 'string'
			}, {
				name : 'robot',
				type : 'string'
			} ]
		})
	});
	var _assginGrid = new Ext.grid.GridPanel(
			{
				store : _assginStore,
				maxHeight : 500,
				autoHeight : true,
				autoWidth : true,
				margins : '0 5 5 5',
				sm : new Ext.grid.RowSelectionModel(),
				loadMask : true,
				autoExpandColumn : '__push__user',
				colModel : new Ext.grid.ColumnModel(
						{
							columns : [
									new Ext.grid.RowNumberer(),
									{
										header : '接收用户',
										id : '__push__user',
										dataIndex : 'user'
									},
									{
										width : 210,
										header : '机器人账号',
										dataIndex : 'robot'
									},
									{
										width : 40,
										header : '操作',
										dataIndex : 'misc',
										renderer : function() {
											return "<a class='_deleteButton' href='javascript:void(0);' onclick='javscript:return false;'>删除</a>";
										}
									} ]
						})
			});
	var _assginWin = new Ext.Window({
		width : 500,
		title : '指定发送消息列表',
		defaults : {
			border : false
		},
		plain : true,
		closable : true,
		closeAction : 'hide',
		collapsible : true,
		draggable : true,
		animCollapse : true,
		constrainHeader : true,
		shadow : false,
		items : [ _assginGrid ]
	});

	_assginGrid.on('cellclick', function(grid, row, col, e) {
		var btn = e.getTarget('._deleteButton');
		if (btn) {
			var self = this;
			Ext.Msg.confirm('提示', '确认删除这条指定消息推送？', function(btn) {
				if (btn == 'yes') {
					var rec = self.getStore().getAt(row);
					Ext.Ajax.request({
						url : 'push-message!deleteSpec.action',
						params : {
							id : rec.get('id')
						},
						success : function(resp) {
							self.getStore().remove(rec);
						},
						failure : function(resp) {
							Ext.Msg.alert('错误', resp.responseText);
						}
					});
				}
			});
		}
	}, _assginGrid);

	this.on('cellclick', function(grid, row, col, e) {
		var btn = e.getTarget('._viewButton');
		if (btn) {
			var rec = this.getStore().getAt(row);
			_assginStore.load({
				params : {
					id : rec.get('id')
				},
				callback : function() {
					_assginWin.show();
				}
			});
		}
	}, this);
};

Ext.extend(PushMessagePanel, Ext.grid.GridPanel, {
	init : function() {
		this.getStore().load();
	},
	editMsg : function(rec) {
		this.formWin.show();
		var f = this.formWin.editForm.getForm();
		var combo = this.formWin.editForm.typeCombo;
		if (rec.json.type == this.helper.TYPE_SPEC) {
			Ext.Ajax.request({
				url : 'push-message!loadReceivers.action',
				params : {
					'id' : rec.json.id
				},
				success : function(resp) {
					var _rec = {};
					Ext.apply(_rec, rec);
					_rec.set('receivers', Ext.decode(resp.responseText).data);
					f.loadRecord(_rec);
					combo.fireEvent('select');
				},
				failure : function(resp) {
					Ext.Msg.alert('错误', resp.responseText);
				}
			});
		} else {
			f.loadRecord(rec);
			combo.fireEvent('select');
		}
	},
	addMsg : function() {
		this.formWin.show();
		this.formWin.editForm.typeCombo.fireEvent('select');
	},
	afterSave : function(btn) {
		btn.enable();
		this.formWin.hide();
	},
	saveMsg : function(btn) {
		var self = this;
		var f = this.formWin.editForm.getForm();
		if (!f.isValid()) {
			Ext.Msg.alert('提示', '您尚未正确完整的输入表单内容，请检查！');
			btn.enable();
			return false;
		}
		var vals = f.getFieldValues();
		if (vals.type == this.helper.TYPE_SPEC) {
			if (Ext.isIE)
				vals.receivers = vals.receivers.split('\r\n');
			else
				vals.receivers = vals.receivers.split('\n');
		} else
			delete vals.receivers;
		Ext.Ajax.request({
			url : 'push-message!save.action',
			params : {
				'data' : Ext.encode(vals)
			},
			success : function(resp) {
				Dashboard.setAlert('保存成功！');
				self.getStore().reload();
				self.afterSave(btn);
			},
			failure : function(resp) {
				Ext.Msg.alert('错误', resp.responseText);
				self.afterSave(btn);
			}
		});
	},
	search : function(showAll) {
		var _contentField = this.tbarEx.s_content;
		var _typeCombo = this.tbarEx.s_type;
		var _platformCombo = this.tbarEx.s_plat;
		var _beginField = this.tbarEx.s_bdate;
		var _expireField = this.tbarEx.s_edate;
		if (showAll) {
			_contentField.setValue('');
			_typeCombo.setValue('');
			_beginField.setValue('');
			_expireField.setValue('');
			_platformCombo.setValue('');
		}
		var store = this.getStore();
		for ( var key in store.baseParams) {
			if (key && key.indexOf('queryParam.') != -1)
				delete store.baseParams[key];
		}
		var _keyCont = _contentField.getValue().trim();
		var _type = _typeCombo.getValue();
		var _begin = _beginField.getValue();
		var _expire = _expireField.getValue();
		var _platform = _platformCombo.getValue();
		if (_keyCont)
			store.setBaseParam('queryParam.content', _keyCont);
		if (_type)
			store.setBaseParam('queryParam.type', _type);
		if (_begin)
			store.setBaseParam('queryParam.begin', _begin);
		if (_expire)
			store.setBaseParam('queryParam.expire', _expire);
		if (_platform)
			store.setBaseParam('queryParam.platform', _platform);
		store.load();
	}
});