SignActivityPanel = function() {
	var self = this;

	var store = new Ext.data.Store({
				proxy : new Ext.data.HttpProxy({
							url : 'sign-activity!list.action'
						}),
				reader : new Ext.data.JsonReader({
							idProperty : 'id',
							root : 'data',
							fields : [{
										name : 'id',
										type : 'string'
									}, {
										name : 'content',
										type : 'string'
									}, {
										name : 'begin',
										type : 'string',
										convert : function(v) {
											return v.replace('T', ' ');
										}
									}, {
										name : 'expire',
										type : 'string',
										convert : function(v) {
											return v.replace('T', ' ');
										}
									}]
						})
			});
	var config = {
		id : 'signActPanel',
		store : store,
		columnLines : true,
		margins : '0 5 5 5',
		border : false,
		style : 'border-right: 1px solid ' + sys_bdcolor,
		tbar : [{
					iconCls : 'icon-add',
					text : '添加签名',
					handler : function() {
						self.addSign();
					}
				}, {
					iconCls : 'icon-delete',
					text : '删除签名',
					handler : function() {
						var rows = self.getSelectionModel().getSelections();
						if (rows.length == 0) {
							Ext.Msg.alert('提示', '请至少选择一条记录！');
							return false;
						}
						Ext.Msg.confirm('提示', '确认删除这' + rows.length + '条记录？',
								function(btn) {
									if (btn == 'yes') {
										var ids = [];
										Ext.each(rows, function() {
													ids.push(this.id)
												});
										self.getEl().mask('正在删除...');
										Ext.Ajax.request({
											url : 'sign-activity!delete.action',
											params : {
												ids : ids.join(',')
											},
											success : function(resp) {
												self.getEl().unmask();
												Dashboard.setAlert('删除成功');
												store.reload();
											},
											failure : function(resp) {
												self.getEl().unmask();
												Ext.Msg.alert('错误',
														resp.responseText);
											}
										})
									}
								});
					}
				}, {
					iconCls : 'icon-refresh',
					text : '刷新',
					handler : function() {
						store.reload();
					}
				}],
		sm : new Ext.grid.RowSelectionModel(),
		loadMask : true,
		autoExpandColumn : 'content',
		colModel : new Ext.grid.ColumnModel({
					columns : [new Ext.grid.RowNumberer(), {
								header : '内容',
								id : 'content',
								dataIndex : 'content'
							}, {
								width : 135,
								header : '起始时间',
								dataIndex : 'begin'
							}, {
								width : 135,
								header : '过期时间',
								dataIndex : 'expire'
							}]
				})
	};

	var _now = new Date();
	var _editForm = new Ext.form.FormPanel({
				frame : false,
				border : false,
				labelWidth : 80,
				autoHeight : true,
				waitMsgTarget : true,
				bodyStyle : 'padding : 3px 10px; background-color:' + sys_bgcolor,
				items : [new Ext.form.Hidden({
									name : 'id'
								}), {
							xtype : 'textarea',
							fieldLabel : '签名内容',
							height : 100,
							allowBlank : false,
							name : 'content',
							blankText : '签名内容不能为空',
							anchor : '98%'
						}, {
							xtype : 'xdatetime',
							allowBlank : false,
							fieldLabel : '起始时间',
							dateFormat : 'Y-m-d',
							timeFormat : 'H:i',
							value : new Date(),
							name : 'begin',
							anchor : '80%'
						}, {
							xtype : 'xdatetime',
							allowBlank : false,
							fieldLabel : '过期时间',
							dateFormat : 'Y-m-d',
							timeFormat : 'H:i',
							value : new Date().setHours(_now.getHours() + 72),
							name : 'expire',
							anchor : '80%'
						}],
				tbar : [{
							text : '保存',
							iconCls : 'icon-add',
							handler : function() {
								this.disable();
								self.saveSign(this);
							}
						}]
			});

	var _formWin = new Ext.Window({
				width : 420,
				title : '编辑签名',
				defaults : {
					border : false
				},
				modal : true,
				plain : true,
				shim : true,
				closable : true,
				closeAction : 'hide',
				collapsible : true,
				resizable : false,
				draggable : true,
				animCollapse : true,
				constrainHeader : true,
				shadow : false,
				listeners : {
					'beforehide' : function(p) {
						p.editForm.getForm().reset();
					}
				},
				items : [_editForm]
			});

	_formWin.editForm = _editForm;
	this.formWin = _formWin;

	SignActivityPanel.superclass.constructor.call(this, config);

	this.on('rowdblclick', function(g, rowIdx, evt) {
				var rec = this.getStore().getAt(rowIdx);
				this.editSign(rec);
			}, this);

}

Ext.extend(SignActivityPanel, Ext.grid.GridPanel, {
			init : function() {
				this.getStore().load();
			},
			editSign : function(rec) {
				this.formWin.show();
				var f = this.formWin.editForm.getForm();
				f.loadRecord(rec);
			},
			addSign : function() {
				this.formWin.show();
			},
			afterSave : function(btn) {
				btn.enable();
				this.formWin.hide();
			},
			saveSign : function(btn) {
				var self = this;
				var f = this.formWin.editForm.getForm();
				if (!f.isValid()) {
					Ext.Msg.alert('提示', '您尚未正确完整的输入表单内容，请检查！');
					btn.enable();
					return false;
				}
				var vals = f.getFieldValues();
				if (!vals.id)
					vals.id = null;
				Ext.Ajax.request({
							url : 'sign-activity!save.action',
							params : {
								'data' : Ext.encode(vals)
							},
							success : function(resp) {
								Dashboard.setAlert('保存成功！');
								self.getStore().reload();
								self.afterSave(btn);
							},
							failure : function(resp) {
								Ext.Msg.alert('错误', resp.responseText);
								self.afterSave(btn);
							}
						});
			}
		});