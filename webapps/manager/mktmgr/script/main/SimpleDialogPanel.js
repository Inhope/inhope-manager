SimpleDialogPanel = function() {
	var self = this;

	var store = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({
			url : 'simple-dialog!list.action'
		}),
		reader : new Ext.data.JsonReader({
			idProperty : 'id',
			root : 'data',
			fields : [ {
				name : 'id',
				type : 'string'
			}, {
				name : 'key',
				type : 'string'
			}, {
				name : 'value',
				type : 'string'
			}, {
				name : 'type',
				type : 'string'
			}, {
				name : 'ruleType',
				type : 'string'
			}, {
				name : 'platform',
				type : 'string'
			}, {
				name : 'platformName',
				type : 'string'
			}, {
				name : 'description',
				type : 'string'
			} ]
		})
	});
	var pagingBar = new Ext.PagingToolbar({
		store : store,
		displayInfo : true,
		pageSize : 20,
		prependButtons : true,
		beforePageText : '第',
		afterPageText : '页，共{0}页',
		displayMsg : '第{0}到{1}条记录，共{2}条',
		emptyMsg : "没有记录"
	});
	var config = {
		id : 'simpleDialogPanel',
		store : store,
		columnLines : true,
		margins : '0 5 5 5',
		border : false,
		style : 'border-right: 1px solid ' + sys_bdcolor,
		tbar : [ {
			iconCls : 'icon-add',
			text : '添加',
			handler : function() {
				self.formWin.show();
			}
		}, {
			iconCls : 'icon-edit',
			text : '编辑',
			handler : function() {
				var rows = self.getSelectionModel().getSelections();
				if (rows.length != 1) {
					Ext.Msg.alert('提示', '请选择一条记录进行编辑！');
					return false;
				}
				self.editDialog(rows[0]);
			}
		}, {
			iconCls : 'icon-delete',
			text : '删除',
			handler : function() {
				var rows = self.getSelectionModel().getSelections();
				if (rows.length == 0) {
					Ext.Msg.alert('提示', '请至少选择一条记录！');
					return false;
				}
				Ext.Msg.confirm('提示', '确认删除这' + rows.length + '条记录？', function(btn) {
					if (btn == 'yes') {
						var ids = [];
						Ext.each(rows, function() {
							ids.push(this.id);
						});
						self.getEl().mask('正在删除...');
						Ext.Ajax.request({
							url : 'simple-dialog!delete.action',
							params : {
								ids : ids.join(',')
							},
							success : function(resp) {
								self.getEl().unmask();
								Dashboard.setAlert('删除成功');
								store.reload();
							},
							failure : function(resp) {
								self.getEl().unmask();
								Ext.Msg.alert('错误', resp.responseText);
							}
						});
					}
				});
			}
		}, {
			iconCls : 'icon-refresh',
			text : '刷新',
			handler : function() {
				store.reload();
			}
		}, {
			iconCls : 'icon-ok',
			text : '应用',
			handler : function() {
				Ext.Msg.confirm('提示', '确定应用？', function(btn) {
					if (btn == 'yes') {
						Ext.Ajax.request({
							url : 'simple-dialog!apply.action',
							success : function(resp) {
								Dashboard.setAlert('应用成功！');
							},
							failure : function(resp) {
								Ext.Msg.alert('错误', resp.responseText);
							}
						});
					}
				});
			}
		}, '-', {
			iconCls : 'icon-ontology-import',
			text : '导入',
			handler : function() {
				self.uploadWin.show();
			}
		}, {
			iconCls : 'icon-ontology-export',
			text : '导出',
			handler : function() {
				Ext.MessageBox.show({
					title : '提示',
					msg : '请选择导出excel文件格式',
					buttons : {
						yes : '07',
						no : '03',
						cancel : '取消'
					},
					fn : function(btn) {
						var type = null;
						if (btn == 'yes')
							type = 'xlsx';
						else if (btn == 'no')
							type = 'xls';
						if (type) {
							if (!self.downloadIFrame) {
								self.downloadIFrame = self.getEl().createChild({
									tag : 'iframe',
									style : 'display:none;'
								});
							}
							self.downloadIFrame.dom.src = 'simple-dialog!export.action?ts=' + new Date().getTime() + '&type=' + type;
						}
					},
					icon : Ext.MessageBox.QUESTION
				});
			}
		} ],
		bbar : pagingBar,
		sm : new Ext.grid.RowSelectionModel(),
		loadMask : true,
		autoExpandColumn : 'targetCol',
		colModel : new Ext.grid.ColumnModel({
			columns : [ new Ext.grid.RowNumberer(), {
				width : 180,
				header : '咨询问题',
				dataIndex : 'key'
			}, {
				header : '预处理结果',
				id : 'targetCol',
				dataIndex : 'value'
			}, {
				width : 100,
				header : '类型',
				dataIndex : 'type',
				renderer : function(v) {
					return typeMap[v];
				}
			}, {
				width : 100,
				header : '匹配规则',
				dataIndex : 'ruleType',
				renderer : function(v) {
					if (!v)
						v = '0';
					return ruleTypeMap[v];
				}
			}, {
				width : 80,
				header : '适用平台',
				dataIndex : 'platformName'
			}, {
				header : '描述',
				width : 180,
				dataIndex : 'description'
			} ]
		})
	};
	var _platComboStore = new Ext.data.JsonStore({
		url : 'simple-dialog!listDimenstion.action',
		fields : [ 'tag', 'name' ],
		autoLoad : true
	});
	var typeData = [ [ '1', '直接回复答案' ], [ '2', '转AI引擎处理' ], [ '3', '转指令处理' ] ];
	var typeMap = Dashboard.utils.arrayDataToMap([ typeData ]);
	var ruleTypeData = [ [ '0', '完全匹配' ], [ '1', '以规则开头' ], [ '2', '以规则结尾' ], [ '3', '正则表达式' ] ];
	var ruleTypeMap = Dashboard.utils.arrayDataToMap([ ruleTypeData ]);
	this.tbarEx = new Ext.Toolbar({
		items : [ '问题关键字:', {
			ref : 's_key',
			xtype : 'textfield',
			width : 120,
			emptyText : '输入关键字搜索...'
		}, '结果关键字:', {
			ref : 's_value',
			xtype : 'textfield',
			width : 120,
			emptyText : '输入关键字搜索...'
		}, '消息类型:', new Ext.form.ComboBox({
			ref : 's_type',
			triggerAction : 'all',
			width : 100,
			editable : false,
			value : '',
			mode : 'local',
			store : new Ext.data.ArrayStore({
				fields : [ 'type', 'displayText' ],
				data : [ [ '', '全部' ] ].concat(typeData)
			}),
			valueField : 'type',
			displayField : 'displayText'
		}), '匹配规则:', new Ext.form.ComboBox({
			ref : 's_ruletype',
			triggerAction : 'all',
			width : 100,
			editable : true,
			value : '',
			mode : 'local',
			store : new Ext.data.ArrayStore({
				fields : [ 'type', 'displayText' ],
				data : [ [ '', '全部' ] ].concat(ruleTypeData)
			}),
			valueField : 'type',
			displayField : 'displayText'
		}), '适用平台:', new Ext.form.ComboBox({
			ref : 's_plat',
			value : '',
			emptyText : '请选择平台',
			triggerAction : 'all',
			width : 100,
			editable : false,
			store : _platComboStore,
			valueField : 'tag',
			displayField : 'name'
		}), ' ', {
			text : '搜索',
			iconCls : 'icon-search',
			handler : function() {
				self.search();
			}
		}, ' ', {
			text : '全部',
			iconCls : 'icon-search',
			handler : function() {
				self.search(true);
			}
		} ]
	});

	SimpleDialogPanel.superclass.constructor.call(this, config);

	this.addListener('render', function() {
		this.tbarEx.render(this.tbar);
	}, this);
	this.on('rowdblclick', function(g, rowIdx, evt) {
		var rec = this.getStore().getAt(rowIdx);
		this.editDialog(rec);
	}, this);
	
	var _platCombo = new Ext.form.ComboBox({
		triggerAction : 'all',
		fieldLabel : '适用平台',
		anchor : '60%',
		name : 'platform',
		editable : false,
		allowBlank : false,
		blankText : '请选择平台',
		store : _platComboStore,
		valueField : 'tag',
		displayField : 'name'
	});

	var _typeCombo = new Ext.form.ComboBox({
		ref : 'typeCombo',
		triggerAction : 'all',
		fieldLabel : '类型',
		anchor : '60%',
		name : 'type',
		editable : false,
		allowBlank : false,
		blankText : '请选择类型',
		value : '1',
		mode : 'local',
		store : new Ext.data.ArrayStore({
			fields : [ 'type', 'display' ],
			data : typeData
		}),
		valueField : 'type',
		displayField : 'display'
	});
	var _ryleTypeCombo = new Ext.form.ComboBox({
		ref : 'ruleTypeCombo',
		triggerAction : 'all',
		fieldLabel : '匹配规则',
		anchor : '60%',
		name : 'ruleType',
		editable : true,
		allowBlank : false,
		blankText : '请选择规则',
		value : '0',
		mode : 'local',
		store : new Ext.data.ArrayStore({
			fields : [ 'type', 'display' ],
			data : ruleTypeData
		}),
		valueField : 'type',
		displayField : 'display'
	});
	var _editForm = new Ext.form.FormPanel({
		frame : false,
		border : false,
		labelWidth : 75,
		autoHeight : true,
		waitMsgTarget : true,
		bodyStyle : 'padding : 3px 10px; background-color:' + sys_bgcolor,
		defaults : {
			anchor : '96%',
			allowBlank : false
		},
		items : [ new Ext.form.Hidden({
			name : 'id'
		}), _typeCombo, _ryleTypeCombo, _platCombo,{
			xtype : 'textfield',
			fieldLabel : '咨询问题',
			name : 'key',
			blankText : '问题不能为空'
		}, {
			xtype : 'textarea',
			fieldLabel : '预处理结果',
			height : 100,
			name : 'value',
			blankText : '结果不能为空'
		}, {
			xtype : 'textarea',
			fieldLabel : '描述',
			height : 60,
			name : 'description',
			allowBlank : true
		} ],
		tbar : [ {
			text : '保存',
			iconCls : 'icon-add',
			handler : function() {
				this.disable();
				self.saveDialog(this);
			}
		} ]
	});
	var winCfg = {
		defaults : {
			border : false
		},
		modal : true,
		plain : true,
		shim : true,
		closable : true,
		closeAction : 'hide',
		collapsible : true,
		resizable : false,
		draggable : true,
		animCollapse : true,
		constrainHeader : true,
		shadow : false,
		listeners : {
			'beforehide' : function(p) {
				p.form.getForm().reset();
			}
		}
	};
	var _formWin = new Ext.Window(Ext.apply({
		width : 450,
		title : '编辑对话',
		items : [ _editForm ]
	}, winCfg));

	_formWin.form = _editForm;
	this.formWin = _formWin;

	var _uploadForm = new Ext.FormPanel({
		fileUpload : true,
		frame : false,
		border : false,
		labelWidth : 70,
		autoHeight : true,
		waitMsgTarget : true,
		bodyStyle : 'padding : 3px 10px; background-color:' + sys_bgcolor,
		defaults : {
			anchor : '95%',
			allowBlank : false,
			msgTarget : 'side'
		},
		items : [ {
			xtype : 'fileuploadfield',
			emptyText : '选择文件',
			fieldLabel : '文件路径',
			name : 'sheetFile',
			buttonText : '',
			buttonCfg : {
				iconCls : 'icon-upload-excel'
			},
			validator : function(v) {
				v = v.trim();
				if (v) {
					if (v.indexOf('.xls') < 0)
						return '文件格式不正确';
					return true;
				}
				return '请选择文件';
			}
		}, {
			id : 'simpleDialogImportIsDelete',
			hideLabel : true,
			boxLabel : '删除全部',
			name : 'isDelete',
			xtype : "checkbox",
			inputValue : true,
			checked : true
		} ],
		tbar : [ {
			text : '导入',
			iconCls : 'icon-ontology-import',
			handler : function() {
				var btn = this;
				btn.disable();
				if (_uploadForm.getForm().isValid()) {
					_uploadForm.getForm().submit({
						url : 'simple-dialog!_import.action',
						params : {
							isDelete : Ext.getCmp("simpleDialogImportIsDelete").getValue()
						},
						waitMsg : '正在导入对话内容...',
						success : function(form, act) {
							var result = Ext.decode(act.response.responseText).data;
							var msg = '成功导入' + result.succTotal + '条记录。' + '失败' + result.failed.length + '条记录。';
							if (result.failed.length > 0)
								msg += '其中第' + result.failed.join(',') + '行数据有误，导入失败。';
							Ext.Msg.alert('导入结果', msg, function() {
								btn.enable();
								_uploadWin.hide();
								self.getStore().reload();
							});
						},
						failure : function(form, act) {
							Ext.Msg.alert('错误', act.result.message);
							btn.enable();
						}
					});
				} else {
					Ext.Msg.alert('错误', '您尚未正确完整的输入表单内容，请检查！', function() {
						btn.enable();
					});
				}
			}
		} ]
	});
	var _uploadWin = new Ext.Window(Ext.apply({
		title : '导入对话',
		items : [ _uploadForm ],
		width : 400
	}, winCfg));
	_uploadWin.form = _uploadForm;
	this.uploadWin = _uploadWin;
};

Ext.extend(SimpleDialogPanel, Ext.grid.GridPanel, {
	init : function() {
		this.getStore().load();
	},
	editDialog : function(rec) {
		this.formWin.show();
		this.formWin.form.getForm().loadRecord(rec);
	},
	afterSave : function(btn) {
		btn.enable();
		this.formWin.hide();
	},
	saveDialog : function(btn) {
		var self = this;
		var f = this.formWin.form.getForm();
		if (!f.isValid()) {
			Ext.Msg.alert('提示', '您尚未正确完整的输入表单内容，请检查！');
			btn.enable();
			return false;
		}
		var vals = f.getFieldValues();
		if (!vals.id)
			delete vals.id;
		Ext.Ajax.request({
			url : 'simple-dialog!save.action',
			params : {
				'data' : Ext.encode(vals)
			},
			success : function(resp) {
				Dashboard.setAlert('保存成功！');
				self.getStore().reload();
				self.afterSave(btn);
			},
			failure : function(resp) {
				Ext.Msg.alert('错误', resp.responseText);
				self.afterSave(btn);
			}
		});
	},
	search : function(showAll) {
		var _valField = this.tbarEx.s_value;
		var _keyField = this.tbarEx.s_key;
		var _combo = this.tbarEx.s_type;
		var _ruleTypeCombo = this.tbarEx.s_ruletype;
		var _platCombo = this.tbarEx.s_plat;
		if (showAll) {
			_valField.setValue('');
			_combo.setValue('');
			_keyField.setValue('');
			_ruleTypeCombo.setValue('');
			_platCombo.setValue('');
		}
		var store = this.getStore();
		for ( var key in store.baseParams) {
			if (key && key.indexOf('queryParam.') != -1)
				delete store.baseParams[key];
		}
		var _src = _keyField.getValue().trim();
		var _type = _combo.getValue();
		var _tgt = _valField.getValue();
		var _ruleType = _ruleTypeCombo.getValue();
		var _plat = _platCombo.getValue();
		if (_src)
			store.setBaseParam('queryParam.key', _src);
		if (_type)
			store.setBaseParam('queryParam.type', _type);
		if (_tgt)
			store.setBaseParam('queryParam.value', _tgt);
		if (_ruleType)
			store.setBaseParam('queryParam.ruleType', _ruleType);
		if (_plat)
			store.setBaseParam('queryParam.platform', _plat);
		store.load();
	}
});
