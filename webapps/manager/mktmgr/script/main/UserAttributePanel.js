UserAttributePanel = function() {

	var self = this;

	var store = new Ext.data.Store({
				proxy : new Ext.data.HttpProxy({
							api : {
								read : 'user-attr!list.action',
								create : 'user-attr!save.action',
								update : 'user-attr!save.action',
								destroy : 'user-attr!delete.action'
							}
						}),
				reader : new Ext.data.JsonReader({
							idProperty : 'id',
							root : 'data',
							fields : [{
										name : 'id',
										type : 'string'
									}, {
										name : 'mark',
										type : 'string'
									}, {
										name : 'name',
										type : 'string'
									}]
						}),
				writer : new Ext.data.JsonWriter({
							writeAllFields : true
						}),
				listeners : {
					beforewrite : function(store, data, rs) {
						if (!rs.get('name'))
							return false;
					}
				}
			});

	var editor = new Ext.ux.grid.RowEditor({
				saveText : '保存',
				cancelText : '取消',
				errorSummary : false
			});
	editor.on('canceledit', function(editor, obj, rec, rowIdx) {
				var r, i = 0;
				while ((r = store.getAt(i++))) {
					if (r.phantom) {
						store.remove(r);
					}
				}
			});
	store.on('save', function(st, batch, data) {
				var respObjs = data['create'] || data['update'];
				if (respObjs[0].id == 'err')
					Ext.Msg.alert('提示', '属性标识"' + respObjs[0].mark + '"已存在',
							function() {
								st.reload();
							});
			}, store);
	var pagingBar = new Ext.PagingToolbar({
				store : store,
				displayInfo : true,
				pageSize : 20,
				prependButtons : true,
				beforePageText : '第',
				afterPageText : '页，共{0}页',
				displayMsg : '第{0}到{1}条记录，共{2}条',
				emptyMsg : "没有记录"
			});
	var config = {
		id : 'userAttrPanel',
		store : store,
		border : false,
		columnLines : true,
		margins : '0 5 5 5',
		autoExpandColumn : 'nameCol',
		plugins : [editor],
		tbar : [{
					iconCls : 'icon-add',
					text : '添加属性',
					handler : function() {
						var attr = new store.recordType({
									id : '',
									name : ''
								});
						editor.stopEditing();
						store.insert(0, attr);
						self.getSelectionModel().selectRow(0);
						editor.startEditing(0, 1);
					}
				}, {
					iconCls : 'icon-delete',
					text : '删除属性',
					handler : function() {
						editor.stopEditing();
						var rows = self.getSelectionModel().getSelections();
						if (rows.length == 0) {
							Ext.Msg.alert('提示', '请至少选择一条记录！');
							return false;
						}
						Ext.Msg.confirm('提示', '确认删除这' + rows.length + '条记录？',
								function(btn) {
									if (btn == 'yes') {
										var ids = [];
										Ext.each(rows, function() {
													ids.push(this.id)
												});
										Ext.Ajax.request({
													url : 'user-attr!delete.action',
													params : {
														ids : ids.join(',')
													},
													success : function(resp) {
														Ext.Msg.alert('提示',
																'删除成功！',
																function() {
																	store
																			.reload();
																});
													},
													failure : function(resp) {
														Ext.Msg
																.alert(
																		'错误',
																		resp.responseText);
													}
												})
									}
								});
					}
				}, {
					iconCls : 'icon-refresh',
					text : '刷新',
					handler : function() {
						store.reload();
					}
				}],
		bbar : pagingBar,
		sm : new Ext.grid.RowSelectionModel(),
		loadMask : true,
		colModel : new Ext.grid.ColumnModel({
					columns : [new Ext.grid.RowNumberer(), {
								header : '属性标识',
								dataIndex : 'mark',
								width : 200,
								editor : {
									xtype : 'textfield',
									allowBlank : false,
									blankText : '请输入属性标识'
								}
							}, {
								header : '属性名称',
								dataIndex : 'name',
								id : 'nameCol',
								editor : {
									xtype : 'textfield',
									allowBlank : false,
									blankText : '请输入属性名称'
								}
							}]
				})
	};

	UserAttributePanel.superclass.constructor.call(this, config);
}

Ext.extend(UserAttributePanel, Ext.grid.GridPanel, {
			init : function() {
				this.getStore().load()
			}
		});
