VotePanel = function() {
	var self = this;

	this.VoteRecord = Ext.data.Record
			.create(['id', 'topic', 'viewMode', 'type', 'position',
					'frequency', 'interval', 'state', 'triggerKey']);
	var _getRowEditor = function() {
		return new Ext.ux.grid.RowEditor({
					saveText : '保存',
					cancelText : '取消',
					errorSummary : false
				});
	}
	var _removePhantom = function(store) {
		var r, i = 0;
		while ((r = store.getAt(i++))) {
			if (r.phantom) {
				store.remove(r);
			}
		}
	}
	var _voteEditor = _getRowEditor();
	_voteEditor.on('canceledit', function(re, changes, rec, rowIdx) {
				_removePhantom(self.getStore());
			});
	var _positions = [[0, 'WEB'], [1, 'IM']];
	var _viewModes = [[2, '投票后可查看'], [0, '不投票可查看'], [1, '不可查看']];
	var _types = [[0, '单选'], [1, '多选']];
	var _states = [[0, '停用'], [1, '启用']];
	var _positionsMap = Dashboard.utils.arrayDataToMap([_positions]);
	var _viewModesMap = Dashboard.utils.arrayDataToMap([_viewModes]);
	var _typesMap = Dashboard.utils.arrayDataToMap([_types]);
	var _statesMap = Dashboard.utils.arrayDataToMap([_states]);
	var _comboProto = {
		xtype : 'combo',
		allowBlank : false,
		editable : false,
		triggerAction : 'all',
		mode : 'local',
		valueField : 'val',
		displayField : 'displayText'
	};
	var _getArrStore = function(data) {
		return new Ext.data.ArrayStore({
					fields : ['val', 'displayText'],
					data : data
				});
	}
	var _isUnsignInt = function(v) {
		return /^[0-9]+$/.test(v);
	}
	var config = {
		id : 'votePanel',
		width : 630,
		columnLines : true,
		style : 'border-right: 1px solid ' + sys_bdcolor,
		border : false,
		title : '投票列表',
		plugins : [_voteEditor],
		store : new Ext.data.Store({
					proxy : new Ext.data.HttpProxy({
								api : {
									read : 'vote!list.action',
									create : 'vote!save.action',
									update : 'vote!save.action',
									destroy : 'vote!delete.action'
								}
							}),
					reader : new Ext.data.JsonReader({
								idProperty : 'id',
								root : 'data',
								fields : self.VoteRecord
							}),
					writer : new Ext.data.JsonWriter({
								writeAllFields : true
							}),
					listeners : {
						beforewrite : function(store, data, rs) {
							if (!rs.get('topic'))
								return false;
						}
					}
				}),
		tbar : [{
					iconCls : 'icon-add',
					text : '新增',
					handler : function() {
						var vote = new self.VoteRecord({
									topic : '',
									viewMode : 2,
									type : 0,
									position : 0,
									frequency : 0,
									interval : 0,
									state : 1
								});
						_voteEditor.stopEditing();
						self.getStore().insert(0, vote);
						self.getSelectionModel().selectRow(0);
						_voteEditor.startEditing(0, 1);
					}
				}, {
					iconCls : 'icon-refresh',
					text : '刷新',
					handler : function() {
						self.getStore().reload();
					}
				}],
		sm : new Ext.grid.RowSelectionModel({
					singleSelect : true
				}),
		loadMask : true,
		autoExpandColumn : 'topicCol',
		colModel : new Ext.grid.ColumnModel({
			columns : [new Ext.grid.RowNumberer(), {
						header : '主题',
						id : 'topicCol',
						dataIndex : 'topic',
						editor : {
							xtype : 'textfield',
							allowBlank : false,
							blankText : '请输入投票主题'
						}
					}, {
						header : '触发键',
						width : 60,
						dataIndex : 'triggerKey',
						editor : {
							xtype : 'textfield',
							allowBlank : false
						}
					}, {
						header : '位置',
						width : 60,
						dataIndex : 'position',
						renderer : function(v, meta, rec) {
							return _positionsMap[v];
						},
						editor : Ext.apply({
									store : _getArrStore(_positions)
								}, _comboProto)
					}, {
						header : '投票方式',
						width : 70,
						dataIndex : 'type',
						renderer : function(v, meta, rec) {
							return _typesMap[v];
						},
						editor : Ext.apply({
									store : _getArrStore(_types)
								}, _comboProto)
					}, {
						header : '结果查看方式',
						width : 100,
						dataIndex : 'viewMode',
						renderer : function(v, meta, rec) {
							return _viewModesMap[v];
						},
						editor : Ext.apply({
									store : _getArrStore(_viewModes)
								}, _comboProto)
					}, {
						header : '次数限制',
						width : 70,
						dataIndex : 'frequency',
						renderer : function(v) {
							if (v > 0)
								return v + ' 次';
							else
								return '不限';
						},
						editor : {
							xtype : 'textfield',
							validator : function(v) {
								v = v.trim();
								if (!_isUnsignInt(v))
									return '请输入一个整数'
								return true;
							}
						}
					}, {
						header : '间隔限制',
						dataIndex : 'interval',
						width : 70,
						renderer : function(v) {
							if (v > 0)
								return v + ' 小时';
							else
								return '不限';
						},
						editor : {
							xtype : 'textfield',
							validator : function(v) {
								v = v.trim();
								if (!_isUnsignInt(v))
									return '请输入一个整数'
								return true;
							}
						}
					}, {
						header : '状态',
						width : 70,
						dataIndex : 'state',
						renderer : function(v, meta, rec) {
							return _statesMap[v];
						},
						editor : Ext.apply(_comboProto, {
									store : _getArrStore(_states)
								})
					}, {
						width : 120,
						header : '操作',
						dataIndex : 'operations',
						renderer : function(v, meta, rec) {
							return "<a class='_itemsButton' href='javascript:void(0);' onclick='javscript:return false;'>投票项及统计</a>&nbsp;&nbsp;"
									+ "<a class='_delButton' href='javascript:void(0);' onclick='javscript:return false;'>删除</a>";

						}
					}]
		})
	}
	VotePanel.superclass.constructor.call(this, config);

	this.on('cellclick', function(grid, row, col, e) {
				var itemsBtn = e.getTarget('._itemsButton');
				var delBtn = e.getTarget('._delButton');
				var rec = this.getStore().getAt(row);
				if (itemsBtn) {
					_itemPanel.getStore().load({
						params : {
							voteId : rec.get('id')
						},
						callback : function() {
							_chartStore.reload({
										params : {
											id : self.getSelectedVote()
													.get('id')
										}
									});
							self.itemWin.show();
						}
					});
					return false;
				}
				if (delBtn) {
					self.deleteFn(this, 'vote!delete.action');
					return false;
				}
			}, this);
	this.getStore().on('save', function(store, batch, data) {
				if (data['create']) {
					this.getSelectionModel().selectRow(0);
				}
			}, this);

	this.ItemRecord = Ext.data.Record.create(['id', 'content', 'sequence',
			'voteCount']);
	var _itemEditor = _getRowEditor();
	_itemEditor.on('canceledit', function(re, changes, rec, rowIdx) {
				_removePhantom(self.itemPanel.getStore());
			});
	var _itemPanel = new Ext.grid.GridPanel({
		id : 'voteItemPanel',
		columnLines : true,
		width : 300,
		height : 300,
		region : 'center',
		border : false,
		plugins : [_itemEditor],
		store : new Ext.data.Store({
					proxy : new Ext.data.HttpProxy({
								api : {
									read : 'vote-item!list.action',
									create : 'vote-item!save.action',
									update : 'vote-item!save.action',
									destroy : 'vote-item!delete.action'
								}
							}),
					reader : new Ext.data.JsonReader({
								idProperty : 'id',
								root : 'data',
								fields : self.ItemRecord
							}),
					writer : new Ext.data.JsonWriter({
								writeAllFields : true
							}),
					listeners : {
						beforewrite : function(store, data, rec, opts) {
							if (!rec.get('content'))
								return false;
							opts.params.voteId = self.getSelectedVote()
									.get('id')
						}
					}
				}),
		sm : new Ext.grid.RowSelectionModel({
					singleSelect : true
				}),
		loadMask : true,
		tbar : [{
					iconCls : 'icon-add',
					text : '新增',
					handler : function() {
						var vote = new self.ItemRecord({
									content : '',
									sequence : 1,
									vote : {
										id : self.getSelectedVote().get('id')
									}
								});
						_itemEditor.stopEditing();
						self.itemPanel.getStore().insert(0, vote);
						self.itemPanel.getSelectionModel().selectRow(0);
						_itemEditor.startEditing(0, 1);
					}
				}, {
					iconCls : 'icon-refresh',
					text : '刷新',
					handler : function() {
						self.itemPanel.getStore().reload();
					}
				}],
		autoExpandColumn : 'contentCol',
		colModel : new Ext.grid.ColumnModel({
			columns : [{
						header : '序号',
						dataIndex : 'sequence',
						width : 50,
						editor : {
							xtype : 'textfield',
							validator : function(v) {
								v = v.trim();
								if (!_isUnsignInt(v))
									return '请正确输入序号';
								return true;
							}
						}
					}, {
						header : '内容',
						id : 'contentCol',
						dataIndex : 'content',
						editor : {
							xtype : 'textfield',
							allowBlank : false,
							blankText : '请输入投票项内容'
						}
					}, {
						width : 50,
						header : '操作',
						dataIndex : 'operations',
						renderer : function() {
							return "<a class='_delButton' href='javascript:void(0);' onclick='javscript:return false;'>删除</a>";
						}
					}]
		})
	});
	_itemPanel.on('cellclick', function(grid, row, col, e) {
				var delBtn = e.getTarget('._delButton');
				if (delBtn) {
					var rec = this.getStore().getAt(row);
					self.deleteFn(this, 'vote-item!delete.action', function() {
								_chartStore.reload();
							});
					return false;
				}
			}, _itemPanel);
	_itemPanel.getStore().on('save', function(store, batch, data) {
				_chartStore.reload();
			}, _itemPanel);

	var _chartStore = new Ext.data.Store({
				proxy : new Ext.data.HttpProxy({
							url : 'vote!stat.action'
						}),
				reader : new Ext.data.JsonReader({
							idProperty : 'id',
							root : 'data',
							fields : self.ItemRecord
						})
			});
	var _chartPanel = new Ext.Panel({
				width : 320,
				region : 'west',
				tbar : new Ext.Toolbar({
							enableOverflow : true,
							items : ['从', {
										id : 's_bdate_vote',
										xtype : 'datefield',
										width : 100,
										emptyText : '统计开始时间',
										format : 'Y-m-d'
									}, '到', {
										id : 's_edate_vote',
										xtype : 'datefield',
										width : 100,
										emptyText : '统计结束时间',
										format : 'Y-m-d'
									}, ' ', {
										iconCls : 'icon-statistic',
										text : '统计',
										handler : function() {
											var _tbar = _chartPanel
													.getTopToolbar();
											var _startField = _tbar
													.findById('s_bdate_vote');
											var _endField = _tbar
													.findById('s_edate_vote');
											var _params = {
												id : self.getSelectedVote()
														.get('id')
											};
											var _start = _startField.getValue();
											var _end = _endField.getValue();
											if (_start.getTime() >= _end
													.getTime()) {
												Ext.Msg.alert('提示',
														'统计结束时间必须大于开始时间');
												return false;
											}
											if (_start)
												_params.start = _start
											if (_end)
												_params.end = _end
											_chartStore.reload({
														params : _params
													});
										}
									}
							// , {
							// iconCls : 'icon-report',
							// text : '生成报表',
							// handler : function() {
							// Ext.Msg.alert('提示', 'coming soon...');
							// }
							// }
							]
						}),
				style : 'border-right: 1px solid ' + sys_bdcolor,
				border : false,
				items : {
					xtype : 'columnchart',
					store : _chartStore,
					yField : 'voteCount',
					url : '../ext/resources/charts.swf',
					xField : 'sequence',
					xAxis : new Ext.chart.CategoryAxis({
								title : '投票项序号'
							}),
					yAxis : new Ext.chart.NumericAxis({
								title : '票数'
							}),
					extraStyle : {
						xAxis : {
							labelRotation : 0
						}
					}
				}
			});
	var _winPanel = new Ext.Panel({
				height : 360,
				border : false,
				layout : 'border',
				items : [_itemPanel, _chartPanel]
			})

	var _itemWin = new Ext.Window({
				defaults : {
					border : false
				},
				modal : true,
				plain : true,
				shim : true,
				closable : true,
				closeAction : 'hide',
				collapsible : true,
				resizable : true,
				draggable : true,
				animCollapse : true,
				constrainHeader : true,
				shadow : false,
				title : '投票项及统计图',
				width : 780,
				listeners : {
					'beforehide' : function(p) {
						var _tbar = _chartPanel.getTopToolbar();
						_tbar.findById('s_bdate_vote').setValue('');
						_tbar.findById('s_edate_vote').setValue('');
					}
				},
				items : [_winPanel]
			});

	this.itemPanel = _itemPanel;
	this.itemWin = _itemWin;

}

Ext.extend(VotePanel, Ext.grid.GridPanel, {
			init : function() {
				var self = this;
				this.getStore().load();
			},
			getSelectedVote : function() {
				return this.getSelectionModel().getSelected();
			},
			deleteFn : function(grid, url, cb) {
				var self = this;
				var row = grid.getSelectionModel().getSelected();
				Ext.Msg.confirm('提示', '确认删除？', function(btn) {
							if (btn == 'yes') {
								grid.getEl().mask('删除中...');
								Ext.Ajax.request({
											url : url,
											params : {
												id : row.get('id')
											},
											success : function(resp) {
												grid.getEl().unmask();
												Dashboard.setAlert('删除成功！');
												grid.getStore().reload();
												if (typeof cb == 'function')
													cb();
											},
											failure : function(resp) {
												grid.getEl().unmask();
												Ext.Msg.alert('错误',
														resp.responseText);
											}
										})
							}
						});
			}
		});