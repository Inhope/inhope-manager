WelcomeResourcePanel = function() {
	var self = this;

	var store = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({
			url : 'welcome-resource!list.action'
		}),
		reader : new Ext.data.JsonReader({
			idProperty : 'id',
			root : 'data',
			fields : [ {
				name : 'id',
				type : 'string'
			}, {
				name : 'title',
				type : 'string'
			}, {
				name : 'semantic',
				type : 'string'
			}, {
				name : 'welcome',
				type : 'string'
			}, {
				name : 'type',
				type : 'string'
			}, {
				name : 'platform',
				type : 'string'
			}, {
				name : 'platformName',
				type : 'string'
			}, {
				name : 'typeName',
				type : 'string'
			} ]
		})
	});
	var pagingBar = new Ext.PagingToolbar({
		store : store,
		displayInfo : true,
		pageSize : 100,
		prependButtons : true,
		beforePageText : '第',
		afterPageText : '页，共{0}页',
		displayMsg : '第{0}到{1}条记录，共{2}条',
		emptyMsg : "没有记录"
	});
	var config = {
		store : store,
		columnLines : true,
		margins : '0 5 5 5',
		border : false,
		style : 'border-right: 1px solid ' + sys_bdcolor,
		tbar : [ {
			iconCls : 'icon-add',
			text : '添加',
			handler : function() {
				self.formWin.show();
			}
		}, {
			iconCls : 'icon-edit',
			text : '编辑',
			handler : function() {
				var rows = self.getSelectionModel().getSelections();
				if (rows.length != 1) {
					Ext.Msg.alert('提示', '请选择一条记录进行编辑！');
					return false;
				}
				self.editResource(rows[0]);
			}
		}, {
			iconCls : 'icon-delete',
			text : '删除',
			handler : function() {
				var rows = self.getSelectionModel().getSelections();
				if (rows.length == 0) {
					Ext.Msg.alert('提示', '请至少选择一条记录！');
					return false;
				}
				Ext.Msg.confirm('提示', '确认删除这' + rows.length + '条记录？', function(btn) {
					if (btn == 'yes') {
						var ids = [];
						Ext.each(rows, function() {
							ids.push(this.id)
						});
						self.getEl().mask('正在删除...');
						Ext.Ajax.request({
							url : 'welcome-resource!delete.action',
							params : {
								ids : ids.join(',')
							},
							success : function(resp) {
								self.getEl().unmask();
								Dashboard.setAlert('删除成功');
								store.reload();
							},
							failure : function(resp) {
								self.getEl().unmask();
								Ext.Msg.alert('错误', resp.responseText);
							}
						})
					}
				});
			}
		}, {
			iconCls : 'icon-refresh',
			text : '刷新',
			handler : function() {
				store.reload();
			}
		}, {
			iconCls : 'icon-ok',
			text : '应用',
			handler : function() {
				Ext.Msg.confirm('提示', '确定应用？', function(btn) {
					if (btn == 'yes') {
						Ext.Ajax.request({
							url : 'welcome-resource!apply.action',
							success : function(resp) {
								Dashboard.setAlert('应用成功！');
							},
							failure : function(resp) {
								Ext.Msg.alert('错误', resp.responseText);
							}
						});
					}
				});
			}
		}, '-', {
			iconCls : 'icon-ontology-import',
			text : '导入',
			handler : function() {
				self.uploadWin.show();
			}
		}, {
			iconCls : 'icon-ontology-export',
			text : '导出',
			handler : function() {
				Ext.MessageBox.show({
					title : '提示',
					msg : '请选择导出excel文件格式',
					buttons : {
						yes : '07',
						no : '03',
						cancel : '取消'
					},
					fn : function(btn) {
						var type;
						if (btn == 'yes')
							type = 'xlsx';
						else if (btn == 'no')
							type = 'xls';
						if (type) {
							if (!self.downloadIFrame) {
								self.downloadIFrame = self.getEl().createChild({
									tag : 'iframe',
									style : 'display:none;'
								})
							}
							self.downloadIFrame.dom.src = 'welcome-resource!export.action?ts=' + new Date().getTime() + '&type=' + type;
						}
					},
					icon : Ext.MessageBox.QUESTION
				});
			}
		} ],
		bbar : pagingBar,
		sm : new Ext.grid.RowSelectionModel(),
		loadMask : true,
		autoExpandColumn : '__welcome_col',
		colModel : new Ext.grid.ColumnModel({
			columns : [ new Ext.grid.RowNumberer(), {
				width : 100,
				header : '消息描述',
				dataIndex : 'title'
			}, {
				header : '消息内容',
				id : '__welcome_col',
				dataIndex : 'welcome'
			}, {
				width : 120,
				header : '时段',
				dataIndex : 'semantic'
			}, {
				width : 80,
				header : '类型',
				dataIndex : 'typeName'
			}, {
				width : 80,
				header : '适用平台',
				dataIndex : 'platformName'
			} ]
		})
	};
	var _platComboStore = new Ext.data.JsonStore({
		url : 'welcome-resource!listDimenstion.action',
		fields : [ 'tag', 'name' ],
		autoLoad : true
	});
	this.tbarEx = new Ext.Toolbar({
		items : [ '标识关键字:', {
			ref : 's_title',
			xtype : 'textfield',
			width : 120,
			emptyText : '输入关键字搜索...'
		}, '内容关键字:', {
			ref : 's_welcome',
			xtype : 'textfield',
			width : 120,
			emptyText : '输入关键字搜索...'
		}, '适用平台:', new Ext.form.ComboBox({
			ref : 's_plat',
			triggerAction : 'all',
			width : 80,
			editable : false,
			store : _platComboStore,
			valueField : 'tag',
			displayField : 'name'
		}), ' ', {
			text : '搜索',
			iconCls : 'icon-search',
			handler : function() {
				self.search();
			}
		}, ' ', {
			text : '全部',
			iconCls : 'icon-search',
			handler : function() {
				self.search(true);
			}
		} ]
	});

	WelcomeResourcePanel.superclass.constructor.call(this, config);

	this.addListener('render', function() {
		this.tbarEx.render(this.tbar);
	}, this);
	this.on('rowdblclick', function(g, rowIdx, evt) {
		var rec = this.getStore().getAt(rowIdx);
		this.editResource(rec);
	}, this);
	var _platCombo = new Ext.form.ComboBox({
		triggerAction : 'all',
		fieldLabel : '适用平台',
		anchor : '60%',
		name : 'platform',
		editable : false,
		allowBlank : false,
		blankText : '请选择平台',
		store : _platComboStore,
		valueField : 'tag',
		displayField : 'name'
	});

	var monthCombo = new Ext.form.ComboBox({
		triggerAction : 'all',
		mode : 'local',
		name : 'month_combo',
		value : '1',
		anchor : '60%',
		// allowBlank : false,
		editable : false,
		// blankText : '请选择哪个月',
		store : new Ext.data.ArrayStore({
			fields : [ 'type', 'name' ],
			data : [ [ '1', '1' ], [ '2', '2' ], [ '3', '3' ], [ '4', '4' ], [ '5', '5' ], [ '6', '6' ], [ '7', '7' ], [ '8', '8' ], [ '9', '9' ],
					[ '10', '10' ], [ '11', '11' ], [ '12', '12' ] ]
		}),
		valueField : 'type',
		displayField : 'name',
		width : 45
	});

	var dayCombo = new Ext.form.ComboBox({
		triggerAction : 'all',
		mode : 'local',
		name : 'day_combo',
		value : '1',
		anchor : '60%',
		// allowBlank : false,
		editable : false,
		// blankText : '请选择哪天',
		store : new Ext.data.ArrayStore({
			fields : [ 'type', 'name' ],
			data : [ [ '1', '1' ], [ '2', '2' ], [ '3', '3' ], [ '4', '4' ], [ '5', '5' ], [ '6', '6' ], [ '7', '7' ], [ '8', '8' ], [ '9', '9' ],
					[ '10', '10' ], [ '11', '11' ], [ '12', '12' ], [ '13', '13' ], [ '14', '14' ], [ '15', '15' ], [ '16', '16' ], [ '17', '17' ],
					[ '18', '18' ], [ '19', '19' ], [ '20', '20' ], [ '21', '21' ], [ '22', '22' ], [ '23', '23' ], [ '24', '24' ], [ '25', '25' ],
					[ '26', '26' ], [ '27', '27' ], [ '28', '28' ], [ '29', '29' ], [ '30', '30' ], [ '31', '31' ] ]
		}),
		valueField : 'type',
		displayField : 'name',
		width : 45
	});

	var monthDayPanel = new Ext.Panel({
		style : 'padding : 0;font: 12px tahoma,arial,helvetica,sans-serif;',
		bodyStyle : 'padding : 0; background-color:#d3e1f1;border:0;',
		layout : 'hbox',
		items : [ {
			xtype : 'label',
			text : '时间:',
			width : 80,
			style : 'margin-top:3px;'
		}, monthCombo, {
			xtype : 'label',
			text : '月',
			width : 20,
			style : 'margin-top:3px;padding-left:3px;'
		}, dayCombo, {
			xtype : 'label',
			text : '日',
			width : 20,
			style : 'margin-top:3px;padding-left:3px;'
		} ],
		border : false
	});

	var monthCombo_spe = new Ext.form.ComboBox({
		triggerAction : 'all',
		mode : 'local',
		name : 'month_combo_spe',
		value : '1',
		anchor : '60%',
		// allowBlank : false,
		editable : false,
		// blankText : '请选择哪个月',
		store : new Ext.data.ArrayStore({
			fields : [ 'type', 'name' ],
			data : [ [ '1', '1' ], [ '2', '2' ], [ '3', '3' ], [ '4', '4' ], [ '5', '5' ], [ '6', '6' ], [ '7', '7' ], [ '8', '8' ], [ '9', '9' ],
					[ '10', '10' ], [ '11', '11' ], [ '12', '12' ] ]
		}),
		valueField : 'type',
		displayField : 'name',
		width : 45
	});

	var weekCombo_spe = new Ext.form.ComboBox({
		id : 'weekCombo_spe',
		triggerAction : 'all',
		mode : 'local',
		name : 'week_combo_spe',
		value : '1',
		anchor : '60%',
		// allowBlank : false,
		editable : false,
		// blankText : '请选择哪个周',
		store : new Ext.data.ArrayStore({
			fields : [ 'type', 'name' ],
			data : [ [ '1', '1' ], [ '2', '2' ], [ '3', '3' ], [ '4', '4' ] ]
		}),
		valueField : 'type',
		displayField : 'name',
		width : 45
	});

	var dayCombo_spe = new Ext.form.ComboBox({
		id : 'dayCombo_spe',
		triggerAction : 'all',
		mode : 'local',
		name : 'day_combo_spe',
		value : '1',
		anchor : '60%',
		// allowBlank : false,
		editable : false,
		// blankText : '请选择哪个天',
		store : new Ext.data.ArrayStore({
			fields : [ 'type', 'name' ],
			data : [ [ '1', '1' ], [ '2', '2' ], [ '3', '3' ], [ '4', '4' ], [ '5', '5' ], [ '6', '6' ], [ '7', '7' ] ]
		}),
		valueField : 'type',
		displayField : 'name',
		width : 45
	});

	var specialPanel = new Ext.Panel({
		style : 'padding : 0;font: 12px tahoma,arial,helvetica,sans-serif;',
		bodyStyle : 'padding : 0; background-color:#d3e1f1;border:0;',
		layout : 'hbox',
		items : [ {
			xtype : 'label',
			text : '时间:',
			width : 80,
			style : 'margin-top:3px;'
		}, monthCombo_spe, {
			xtype : 'label',
			text : '月',
			width : 20,
			style : 'margin-top:3px;padding-left:3px;'
		}, weekCombo_spe, {
			xtype : 'label',
			text : '周',
			width : 20,
			style : 'margin-top:3px;padding-left:3px;'
		}, dayCombo_spe, {
			xtype : 'label',
			text : '天',
			width : 20,
			style : 'margin-top:3px;padding-left:3px;'
		} ],
		border : false
	});

	var weekCheckboxGroup = new Ext.form.CheckboxGroup({
		id : 'weekCheckbox',
		allowBlank : true,
		fieldLabel : '时间',
		xtype : 'checkboxgroup',
		columns : 7,
		items : [ {
			xtype : 'checkbox',
			name : '1',
			boxLabel : '周一'
		}, {
			xtype : 'checkbox',
			name : '2',
			boxLabel : '周二'
		}, {
			xtype : 'checkbox',
			name : '3',
			boxLabel : '周三'
		}, {
			xtype : 'checkbox',
			name : '4',
			boxLabel : '周四'
		}, {
			xtype : 'checkbox',
			name : '5',
			boxLabel : '周五'
		}, {
			xtype : 'checkbox',
			name : '6',
			boxLabel : '周六'
		}, {
			xtype : 'checkbox',
			name : '7',
			boxLabel : '周日'
		} ]
	});

	var weekStartField = new Ext.form.TimeField({
		id : 'weekStartField',
		allowBlank : false,
		fieldLabel : '开始',
		editable : true,
		format : 'H:i',
		blankText : '请选择开始时间',
		value : new Date(),
		increment : 30
	});

	var weekEndField = new Ext.form.TimeField({
		id : 'weekEndField',
		allowBlank : false,
		fieldLabel : '结束',
		editable : true,
		format : 'H:i',
		blankText : '请选择结束时间',
		increment : 30,
		value : new Date()
	});

	// 农/公历（类型）
	var monthDay_typeChange = function() {
		monthDayPanel.show();
		specialPanel.hide();
		weekCheckboxGroup.hide();
		weekStartField.hide();
		weekEndField.hide();
	}

	// 特殊日子（类型）
	var special_typeChange = function() {
		monthDayPanel.hide();
		specialPanel.show();
		weekCheckboxGroup.hide();
		weekStartField.hide();
		weekEndField.hide();
	}

	// 时间段（类型）
	var week_typeChange = function() {
		monthDayPanel.hide();
		specialPanel.hide();
		weekCheckboxGroup.show();
		weekStartField.show();
		weekEndField.show();
	}

	var _typeCombo = new Ext.form.ComboBox({
		id : 'typeCombo',
		triggerAction : 'all',
		fieldLabel : '类型',
		mode : 'local',
		anchor : '60%',
		name : 'type',
		value : '1',
		editable : false,
		allowBlank : false,
		blankText : '请选择类型',
		store : new Ext.data.ArrayStore({
			fields : [ 'type', 'name' ],
			data : [ [ '1', '公历' ], [ '2', '农历' ], [ '3', '特殊日子' ], [ '4', '时间段' ] ]
		}),
		valueField : 'type',
		displayField : 'name',
		listeners : {
			'select' : function(combo, record, index) {
				if (index == 0 || index == 1) {
					monthDay_typeChange();
				} else if (index == 2) {
					special_typeChange();
				} else if (index == 3) {
					week_typeChange();
				}
			}
		}
	});

	var _editForm = new Ext.form.FormPanel({
		frame : false,
		border : false,
		labelWidth : 75,
		autoHeight : true,
		waitMsgTarget : true,
		bodyStyle : 'padding : 3px 10px; background-color:' + sys_bgcolor,
		defaults : {
			anchor : '99%'
		// allowBlank : false
		},
		items : [ new Ext.form.Hidden({
			name : 'id'
		}), _platCombo, {
			xtype : 'textfield',
			fieldLabel : '消息描述',
			name : 'title',
			allowBlank : true
		}, {
			xtype : 'textarea',
			fieldLabel : '消息内容',
			height : 100,
			name : 'welcome',
			blankText : '内容不能为空',
			allowBlank : false
		}, _typeCombo, monthDayPanel, specialPanel, weekCheckboxGroup, weekStartField, weekEndField ],
		tbar : [ {
			text : '保存',
			iconCls : 'icon-add',
			handler : function() {
				this.disable();
				self.saveResource(this);
			}
		} ]
	});

	monthDay_typeChange();

	var winCfg = {
		defaults : {
			border : false
		},
		modal : true,
		plain : true,
		shim : true,
		closable : true,
		closeAction : 'hide',
		collapsible : true,
		resizable : false,
		draggable : true,
		animCollapse : true,
		constrainHeader : true,
		shadow : false,
		listeners : {
			'beforehide' : function(p) {
				p.form.getForm().reset();
				monthDay_typeChange();
			}
		}
	}
	var _formWin = new Ext.Window(Ext.apply({
		width : 420,
		title : '编辑消息资源',
		items : [ _editForm ]
	}, winCfg));

	_formWin.form = _editForm;
	this.formWin = _formWin;

	var _uploadForm = new Ext.FormPanel({
		fileUpload : true,
		frame : false,
		border : false,
		labelWidth : 70,
		autoHeight : true,
		waitMsgTarget : true,
		bodyStyle : 'padding : 3px 10px; background-color:' + sys_bgcolor,
		defaults : {
			anchor : '95%',
			allowBlank : false,
			msgTarget : 'side'
		},
		items : [ {
			xtype : 'fileuploadfield',
			emptyText : '选择文件',
			fieldLabel : '文件路径',
			name : 'sheetFile',
			buttonText : '',
			buttonCfg : {
				iconCls : 'icon-upload-excel'
			},
			validator : function(v) {
				v = v.trim();
				if (v) {
					if (v.indexOf('.xls') < 0)
						return '文件格式不正确';
					return true;
				}
				return '请选择文件';
			}
		} ],
		tbar : [ {
			text : '导入',
			iconCls : 'icon-import',
			handler : function() {
				var btn = this;
				btn.disable();
				var params = {};
				if (_uploadForm.getTopToolbar().clearAllChk.checked)
					params.clear = 'true';
				if (_uploadForm.getForm().isValid()) {
					_uploadForm.getForm().submit({
						url : 'welcome-resource!_import.action',
						waitMsg : '正在导入消息资源...',
						params : params,
						success : function(form, a) {
							var o = Ext.decode(a.response.responseText);
							var result = o.data;
							var msg = '成功导入' + result.succTotal + '条记录。';
							if (result.failed.length > 0)
								msg += '第' + result.failed.join(',') + '行数据有误，导入失败。';
							if (o.message != '0')
								msg += '有' + o.message + '行数据重复，导入失败。';
							Ext.Msg.alert('导入结果', msg, function() {
								btn.enable();
								_uploadWin.hide();
								self.getStore().reload();
							})
						},
						failure : function(form, act) {
							Ext.Msg.alert('错误', act.result.message);
							btn.enable();
						}
					});
				} else {
					Ext.Msg.alert('错误', '您尚未正确完整的输入表单内容，请检查！', function() {
						btn.enable()
					});
				}
			}
		}, '-', {
			xtype : 'checkbox',
			name : 'clearAll',
			ref : 'clearAllChk',
			boxLabel : '清空导入'
		} ]
	});
	var _uploadWin = new Ext.Window(Ext.apply({
		title : '导入消息资源',
		items : [ _uploadForm ],
		width : 400
	}, winCfg));
	_uploadWin.form = _uploadForm;
	this.uploadWin = _uploadWin;

	this.diy_loadRecord = function(type, semantic) {
		if (type == '1' || type == '2') {
			semantic = semantic.replace("M", "");
			var mds = semantic.split("D");
			monthCombo.setValue(parseInt(mds[0]) + "");
			dayCombo.setValue(parseInt(mds[1]) + "");
		} else if (type == '3') {
			semantic = semantic.replace("M", "");
			semantic = semantic.toUpperCase();
			var mWws = semantic.split("W");
			monthCombo_spe.setValue(parseInt(mWws[0]) + "");
			weekCombo_spe.setValue(parseInt(mWws[1]) + "");
			dayCombo_spe.setValue(parseInt(mWws[2]) + "");
			special_typeChange();
		} else if (type == '4') {
			var dts = semantic.split("|");
			var dayOfWeekArray = dts[0].split("");
			var checkArray = [ false, false, false, false, false, false, false ];
			for ( var i = 0; i < dayOfWeekArray.length; i++) {
				checkArray[parseInt(dayOfWeekArray[i]) - 1] = true;
			}
			weekCheckboxGroup.setValue(checkArray);
			var timeArray = dts[1].split("-");
			weekStartField.setValue(timeArray[0]);
			weekEndField.setValue(timeArray[1]);
			week_typeChange();
		}
	}
}

Ext.extend(WelcomeResourcePanel, Ext.grid.GridPanel, {
	init : function() {
		this.getStore().load()
	},
	editResource : function(rec) {
		this.formWin.show();
		this.formWin.form.getForm().loadRecord(rec);

		if (rec.data) {
			var record_data = rec.data;
			if (record_data.type && record_data.semantic) {
				this.diy_loadRecord(record_data.type, record_data.semantic);
			}
		}
	},
	afterSave : function(btn) {
		btn.enable();
		this.formWin.hide();
	},
	saveResource : function(btn) {
		var self = this;
		var f = this.formWin.form.getForm();
		if (!f.isValid()) {
			Ext.Msg.alert('提示', '您尚未正确完整的输入表单内容，请检查！');
			btn.enable();
			return false;
		}
		var vals = f.getFieldValues();
		// 自己手动根据类型来整理时间数据
		if (vals.type == '1' || vals.type == '2') {
			if (vals.day_combo_spe) {
				delete vals.day_combo_spe;
			}
			if (vals.month_combo_spe) {
				delete vals.month_combo_spe;
			}
			if (vals.weekCheckbox) {
				delete vals.weekCheckbox;
			}
			if (vals.weekEndField) {
				delete vals.weekEndField;
			}
			if (vals.weekStartField) {
				delete vals.weekStartField;
			}
			if (vals.week_combo_spe) {
				delete vals.week_combo_spe;
			}
			if (vals.day_combo && vals.month_combo) {
				vals["semantic"] = "M" + vals.month_combo + "D" + vals.day_combo;
				delete vals.day_combo;
				delete vals.month_combo;
			}
		} else if (vals.type == '3') {
			if (vals.day_combo) {
				delete vals.day_combo;
			}
			if (vals.month_combo) {
				delete vals.month_combo;
			}
			if (vals.weekCheckbox) {
				delete vals.weekCheckbox;
			}
			if (vals.weekEndField) {
				delete vals.weekEndField;
			}
			if (vals.weekStartField) {
				delete vals.weekStartField;
			}
			if (vals.day_combo_spe && vals.month_combo_spe && vals.week_combo_spe) {
				vals["semantic"] = "M" + vals.month_combo_spe + "W" + vals.week_combo_spe + "w" + vals.day_combo_spe;
				delete vals.day_combo_spe;
				delete vals.month_combo_spe;
				delete vals.week_combo_spe;
			}
		} else if (vals.type == '4') {
			if (vals.day_combo_spe) {
				delete vals.day_combo_spe;
			}
			if (vals.month_combo_spe) {
				delete vals.month_combo_spe;
			}
			if (vals.week_combo_spe) {
				delete vals.week_combo_spe;
			}
			if (vals.day_combo) {
				delete vals.day_combo;
			}
			if (vals.month_combo) {
				delete vals.month_combo;
			}
			if (vals.weekEndField && vals.weekStartField) {
				var timeStr = vals.weekStartField + "-" + vals.weekEndField;
				delete vals.weekEndField;
				delete vals.weekStartField;
				if (vals.weekCheckbox && vals.weekCheckbox.length > 0) {
					var dayOfWeek = "";
					for ( var i = 0; i < vals.weekCheckbox.length; i++) {
						dayOfWeek += vals.weekCheckbox[i]["name"];
					}
					vals["semantic"] = dayOfWeek + "|" + timeStr;
					delete vals.weekCheckbox;
				} else {
					vals["semantic"] = "1234567|" + timeStr;
					if (vals.weekCheckbox) {
						delete vals.weekCheckbox;
					}
				}
			}
		}

		if (!vals.id)
			delete vals.id;
		Ext.Ajax.request({
			url : 'welcome-resource!save.action',
			params : {
				'data' : Ext.encode(vals)
			},
			success : function(resp) {
				var ret = Ext.decode(resp.responseText);
				if (ret.message && ret.message == 'fail') {
					Ext.Msg.alert('错误', '保存失败，已存在相应的消息资源！');
					btn.enable();
				} else {
					Dashboard.setAlert('保存成功！');
					self.getStore().reload();
					self.afterSave(btn);
				}
			},
			failure : function(resp) {
				Ext.Msg.alert('错误', resp.responseText);
				self.afterSave(btn);
			}
		});
	},
	search : function(showAll) {
		var _welcomeField = this.tbarEx.s_welcome;
		var _titleField = this.tbarEx.s_title;
		var _platCombo = this.tbarEx.s_plat;
		if (showAll) {
			_welcomeField.setValue('');
			_platCombo.setValue('');
			_titleField.setValue('');
		}
		var store = this.getStore();
		for ( var key in store.baseParams) {
			if (key && key.indexOf('queryParam.') != -1)
				delete store.baseParams[key];
		}
		var _title = _titleField.getValue().trim();
		var _plat = _platCombo.getValue();
		var _welcome = _welcomeField.getValue();
		if (_title)
			store.setBaseParam('queryParam.title', _title);
		if (_plat)
			store.setBaseParam('queryParam.platform', _plat);
		if (_welcome)
			store.setBaseParam('queryParam.welcome', _welcome);
		store.load();
	},
	externalLoad : function(v) {
		var platform = arguments[0];
		this.store.baseParams['queryParam.platform'] = platform;
		var platCombo = this.tbarEx.s_plat;
		if (platCombo.getStore().getCount() == 0)
			platCombo.getStore().load({
				callback : function() {
					platCombo.setValue(platform);
				}
			});
		else
			platCombo.setValue(platform);
		this.store.load({
			params : {
				start : 0,
				limit : 20
			}
		});
	}
});
