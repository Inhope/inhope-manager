// var _onTriggerClick = TwinTriggerCombo.prototype.onTriggerClick
// Ext.override(TwinTriggerCombo, {
// trigger1Class : 'x-combo-btn',// x-form-clear-trigger
// onTriggerClick : function(e) {
// if (e.getTarget('.x-combo-btn')) {
// this.onTrigger1Click(e);
// } else {
// return _onTriggerClick.apply(this, arguments);
// }
// }
// }
var ALL_DIM_STRING = '所有维度'
var DimensionChooserMenuConstructor = function(cfg) {
	cfg = cfg || {}
	var EventManager = Ext.extend(Ext.util.Observable, {});
	if (!DimensionChooserMenuConstructor.evtmgr)
		DimensionChooserMenuConstructor.evtmgr = new EventManager();
	var evtmgr = DimensionChooserMenuConstructor.evtmgr;
	var self = this;
	var comboList = [];
	this.init = function(cb) {
		if (this._inited) {
			if (cb)
				cb()
			return;
		}
		this.initProcess = function(data) {
			this.renderUI(data[1]);
			this.allDimension = data[0];
			if (evtmgr.dims) {
				this.setDimensions(evtmgr.dims);
			} else {
				this.fireEvent('dimensionChanged', self.getSelectedDimensions());
			}
			if (cb)
				cb();
		};
		this._inited = true;
		if (window._dimChooserInitData) {
			this.initProcess(window._dimChooserInitData)
		} else {
			Ext.Ajax.request({
				url : 'ontology-dimension!listAllowed.action',
				success : function(response) {
					var result = Ext.decode(response.responseText);
					this.initProcess(result.data)
				},
				failure : function() {
					if (cb)
						cb()
				},
				scope : this
			})
		}
	};

	this.BUTTONS = {
		OK : {
			xtype : 'button',
			text : '确定',
			colspan : 2,
			width : 50,
			handler : function() {
				self.fireEvent('dimensionChoosed', self.getSelectedDimensions())
				self.sd = null;
				self.hide()
			}
		},
		CANCEL : {
			xtype : 'button',
			text : '取消',
			colspan : 2,
			width : 50,
			handler : function() {
				self.hide()
			}
		}
	}
	var comboGroup = this.comboGroup = {};
	this.renderUI = function(dimensions) {
		var viewList = [];
		for ( var i = 0; i < dimensions.length; i++) {
			var dimension = dimensions[i];
			var combo = this.createCombo(dimension);
			comboGroup[dimension.id] = [ combo ]
			viewList.push(combo);
		}
		// evtmgr.on('dimensionSync', function(from, dimension) {
		// if (from != self) {
		// self.setDimensions(dimension);
		// }
		// });
		var buttons = [];
		if (cfg.buttonsToAdd) {
			buttons = buttons.concat(cfg.buttonsToAdd);
		} else {
			buttons = [ this.BUTTONS.OK, this.BUTTONS.CANCEL ];
		}
		for ( var i = buttons.length - 1; i >= 0; i--) {
			var b = buttons[i];
			if (typeof b == 'string') {
				buttons[i] = b = this.BUTTONS[buttons[i]];
				if (!b) {
					buttons.splice(i, 1);
					continue
				}
			}
			if (!b.width) {
				b.width = 50
			}
		}
		var items = [ viewList, {
			xtype : 'buttongroup',
			layout : 'hbox',
			border : false,
			frame : false,
			defaults : {
				xtype : 'label'
			},
			items : [ {
				xtype : 'spacer',
				flex : 1
			} ].concat(buttons)
		} ]
		self.add(items);
		self.doLayout();
	}
	this.addEvents('dimensionChanged', 'dimensionChoosed');
	this.on('show', function() {
		this.init()
		this.sd = this.getSelectedDimensions();
	}, this);
	if (cfg.labelAlign == 'top')
		this.cls = 'dimension-chooser-vertical';
	return DimensionChooserMenu.superclass.constructor.call(this, Ext.apply({
		style : {
			overflow : 'visible', // For the Combo popup
			'background-image' : 'none'
		},
		layout : 'form',
		labelWidth : 40,
		frame : true
	}, cfg))
}

var DimensionChooserMenu = Ext.extend(Ext.menu.Menu, {
	constructor : DimensionChooserMenuConstructor,
	/**
	 * @return array[tag(id,name,dimId)]
	 */
	getSelectedDimensions : function() {
		var ret = [];
		for ( var dimid in this.comboGroup) {
			var combos = this.comboGroup[dimid];
			Ext.each(combos, function(c) {
				if (c.isVisible() && c.getValue()) {
					ret.push({
						id : c.getValue(),
						name : c.getRawValue(),
						dimId : combos[0].hiddenName
					})
				}
			});
		}
		return ret;
	},
	/**
	 * 
	 * @param {object}
	 *            groupValueMap - {key=dimId,value=[tagIds...]}
	 */
	updateUI : function(groupValueMap) {
		for ( var dimId in this.comboGroup) {
			var combos = this.comboGroup[dimId];
			var tagIds = groupValueMap[dimId];
			var combosToRm = []
			for ( var i = 0; tagIds && i < tagIds.length || i < combos.length; i++) {
				var tagId = tagIds ? tagIds[i] : null
				var combo = combos[i];
				if (tagId) {
					if (!combo) {
						combo = this.addCombo(combos[0].hiddenName, true);
					}
					combo.setValue(tagId)
				} else if (i == 0) {
					combo.setValue('')
				} else {
					combosToRm.push(combo)
				}
			}
			Ext.each(combosToRm, function(c) {
				this.removeCombo(c)
			}, this)
		}
		this.doLayout();
	},
	findTagIds : function(tagnames) {
		var result = {}
		var combos = this._getBaseCombos();
		Ext.each(tagnames, function(n) {
			for ( var i = 0; i < combos.length; i++) {
				var combo = combos[i];// base one
				var rp = combo.getStore().findExact('name', n);
				if (rp == -1)
					continue;
				var r = combo.getStore().getAt(rp);
				if (r) {
					result[n] = r.get('id')
					continue;
				}
			}
		})
		return result
	},
	setDimensions : function(tags) {
		var groupValueMap = {};
		for ( var j = 0; j < tags.length; j++) {
			var t = tags[j];
			var vlist = groupValueMap[t.dimId];
			if (!vlist) {
				groupValueMap[t.dimId] = vlist = [];
			}
			vlist.push(t.id);
		}
		this.updateUI(groupValueMap);
		this.fireEvent('dimensionChanged', tags);
	},
	setDimensionsByTagId : function(tagIds) {
		if (!tagIds)
			return;
		var groupValueMap = {};
		var combos = this._getBaseCombos();
		loop1: for ( var j = 0; j < tagIds.length; j++) {
			var tagId = tagIds[j];
			for ( var i = 0; i < combos.length; i++) {
				var combo = combos[i];// base one
				var r = combo.getStore().getById(tagId);
				if (r) {
					var vlist = groupValueMap[combo.hiddenName];
					if (!vlist) {
						groupValueMap[combo.hiddenName] = vlist = [];
					}
					vlist.push(tagId);
					continue loop1;
				}
			}
		}
		this.updateUI(groupValueMap)
	},
	_getBaseCombos : function() {
		var ret = []
		for ( var key in this.comboGroup) {
			ret.push(this.comboGroup[key][0])
		}
		return ret;
	},
	filterAllowed : function(tagIds) {
		if (!tagIds)
			return []
		var dims = {};
		var c = this._getBaseCombos();
		loop: for ( var j = tagIds.length; j >= 0; j--) {
			var t = tagIds[j];
			for ( var i = 0; i < c.length; i++) {
				if (c[i].getStore().getById(t)) {
					continue loop;
				}
			}
			tagIds.splice(j, 1)
		}
	},
	getNameStrById : function(tagIds) {
		if (!tagIds)
			return []
		var dims = {};
		var dim = this.allDimension;
		if (!dim)
			return null;
		for ( var i = 0; i < dim.length; i++) {
			var d = dim[i];
			for ( var j = 0; j < tagIds.length; j++) {
				var t = tagIds[j];
				for ( var m = 0; m < d.tags.length; m++) {
					if (d.tags[m].id == t) {
						if (!dims[d.id])
							dims[d.id] = ''
						dims[d.id] += d.tags[m].name + ',';
					}
				}
			}
		}

		var ret = ''
		for ( var k in dims) {
			ret += dims[k].slice(0, -1) + ';'
		}
		if (!ret)
			return ALL_DIM_STRING;
		return ret.slice(0, -1);
	},
	// combo management
	createCombo : function(dimension, child) {
		var cls = 'x-form-add-trigger';
		if (child) {
			cls = 'x-form-clear-trigger'
		}
		cls += ' x-combo-btn';

		var combo = new TwinTriggerCombo({
			trigger1Class : cls,
			onTrigger1Click : function(e) {
				if (e.getTarget('.x-form-add-trigger')) {
					this.ownerCt.addComboByUser(this.hiddenName)
				} else if (e.getTarget('.x-form-clear-trigger')) {
					this.ownerCt.removeCombo(this, true)
				}
			},
			store : new Ext.data.JsonStore({
				root : 'tags',
				idProperty : 'id',
				fields : [ 'id', 'name' ]
			}),
			hiddenName : dimension.id,
			fieldLabel : !child ? dimension.name : '',
			hideLabel : child && this.labelAlign == 'top',
			// style : 'margin-bottom:0;padding-bottom:0',
			defaultValue : dimension.defaultValue,
			anchor : '100%',
			value : dimension.defaultValue,
			typeAhead : true,
			mode : 'local',
			triggerAction : 'all',
			emptyText : '请选择...',
			selectOnFocus : true,
			valueField : 'id',
			displayField : 'name',
			editable : true,
			getListParent : function() {
				return this.el.up('.x-menu');
			}
		})
		if (!child) {
			combo.dimension = dimension;
			if (dimension.tags && dimension.tags.length && !dimension.tags[dimension.tags.length - 1].id) {
				dimension.tags[dimension.tags.length - 1].name = '全部' + combo.fieldLabel
			}
		}
		if (!dimension.tags) {
			dimension.tags = []
		}
		combo.getStore().loadData(dimension)
		combo.on('change', function(combo, newValue, oldValue) {
			var dims = this.getSelectedDimensions();
			this.fireEvent('dimensionChanged', dims);
		}, this);
		return combo;
	},
	removeCombo : function(combo, dolayout) {
		var list = this.comboGroup[combo.hiddenName];
		Ext.each(list, function(c, i) {
			if (c == combo) {
				this.remove(combo);
				list.splice(i, 1)
				delete combo
				return false;
			}
		}, this)
		if (dolayout)
			this.doLayout();
		if (this.positionFn)
			this.positionFn();
	},
	setPositionFn : function(fn) {
		this.positionFn = fn
	},
	addComboByUser : function(dimId) {
		var c = this.comboGroup[dimId][0]
		var list = this.comboGroup[dimId];
		if (list) {
			var valueSet;
			c.store.each(function(r) {
				for ( var i = 0; i < list.length; i++) {
					if (list[i].getValue() == r.get('id')) {
						return true;
					}
				}
				valueSet = r.get('id');
				return false;
			})
			if (valueSet) {
				var combo = this.addCombo(dimId, true);
				combo.setValue(valueSet)
			}
		}
		this.doLayout()
	},
	addCombo : function(dimId, dolayout) {
		var list = this.comboGroup[dimId];
		var newCombo;
		var baseCombo = list[0];
		var basePos = this.items.indexOf(baseCombo);
		var insertPos = basePos + list.length;
		var newCombo = this.createCombo(baseCombo.dimension, true)
		this.insert(insertPos, newCombo);
		list.push(newCombo)
		newCombo.show();
		if (dolayout)
			this.doLayout();
		if (this.positionFn)
			this.positionFn();
		return newCombo
	}
});

var DimensionChooserButton = Ext.extend(Ext.SplitButton, {
	constructor : function(cfg) {
		var self = this;
		var m = new DimensionChooserMenu();
		m.addListener('dimensionChanged', function(v) {
			var names = [];
			Ext.each(v, function(tag) {
				names.push(tag.name)
			})
			self.setTooltip(names.join('+'));
		})
		this.m = m;
		this.addEvents('dimensionChanged');
		this.relayEvents(m, [ "dimensionChanged" ]);
		return DimensionChooserButton.superclass.constructor.call(this, {
			text : '维度',
			menu : m
		});
	},
	/**
	 * @return {DimensionChooserMenu}
	 */
	getMenu : function() {
		return this.m;
	}
});

DimensionBox = Ext.extend(Ext.form.TriggerField, {
	constructor : function(cfg) {
		var self = this;

		var m = new DimensionChooserMenu(cfg.menuConfig ? cfg.menuConfig : null);
		m.init();
		m.on('dimensionChoosed', function(v) {
			var tags = this.m.getSelectedDimensions();
			if (tags == null)
				return;
			this.tags = tags;
			var tagIds = []
			for ( var i = 0; i < tags.length; i++)
				tagIds.push(tags[i].id);
			this.setValue(tagIds);
			if (this.cleared)
				delete this.cleared
		}, this)
		this.m = m;
		this.addEvents('dimensionChanged');
		this.relayEvents(m, [ "dimensionChanged" ]);

		cfg = cfg || {}
		Ext.apply(cfg, {
			editable : false
		});
		if (cfg.clearBtn) {
			this.triggerClass = 'x-form-clear-trigger'
		}
		return DimensionBox.superclass.constructor.call(this, cfg);

	},
	getMenu : function() {
		return this.m;
	},
	onTriggerClick : function(e) {
		if (this.clearBtn && e.getTarget('.x-form-clear-trigger')) {
			this.cleared = true;
			if (this._value != null)
				this._value = [];
			DimensionBox.superclass.setValue.call(this, null);
		} else {
			this.m.setWidth(this.getWidth())
			this.m.show(this.getEl())
			this.m.setDimensionsByTagId(this._value);
		}
	},
	reset : function() {
		delete this._value
		DimensionBox.superclass.reset.call(this);
	},
	setValue : function(tagIds) {
		if (typeof tagIds == 'string')
			return;
		this._value = tagIds;
		this.m.setDimensionsByTagId(tagIds);
		var names = this.m.getNameStrById(tagIds);
		if (names)
			DimensionBox.superclass.setValue.call(this, names);
	},
	getValue : function() {
		if (this.clearBtn) {
			if (this.cleared)
				return null;
			if (this._value && !this._value.length)
				return this.valueTransformer ? this.valueTransformer('ALLDIM') : 'ALLDIM';
		}
		if (this.valueTransformer)
			return this.valueTransformer(this._value);
		return this._value;
	},
	destroy : function() {
		DimensionBox.superclass.destroy.call(this)
		this.valueTransformer ? delete this.valueTransformer : 0
		this._value ? delete this._value : 0
		this.tags ? delete this.tags : 0
		this.m ? delete this.m : 0
	}
});
Ext.reg('dimensionbox', DimensionBox)