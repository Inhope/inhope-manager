<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="res" uri="/WEB-INF/tld/import-res.tld"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="usr" uri="/WEB-INF/tld/login-user.tld"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>服务管理</title>
	<%@include file="/commons/extjsvable.jsp"%>
	<link rel="stylesheet" type="text/css" href="custom.css" />
	<script type="text/javascript" src="script/dashboard.js"></script>
	<res:import dir="script/ux" recursive="true" include=".*\.js"/>
	<script type="text/javascript" src="script/main/MessageResourcePanel.js"></script>
	<usr:checkPerm jsExportVar="mkt_check_perm_result" permissionNames="mkt" dump="true" />
	<usr:name jsExportVar="mkt_username"/>
</head>
<body>
<script type="text/javascript">
var panel = new MessageResourcePanel();
panel.on('afterrender', function(){
	this.init();
});
var viewport = new Ext.Viewport({
	layout : 'fit',
	items : [panel]
});
</script>
</body>
</html>