<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="mdl" uri="/WEB-INF/tld/mdl-ft.tld"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<title><mdl:dump name="systemName" jsExportVar="#out" unique="true" type="props"/></title>
<base target="_self"/>
<%@include file="/commons/extjsvable.jsp" %>
<link rel="stylesheet" type="text/css" href="../authmgr/css/login_standard.css"/>
<link rel="shortcut icon" type="image/x-icon" href="../favicon.ico"/>
<script>
(function(){
	if (getTopWindow() != window){
		getTopWindow().location = window.location
		return;
	}
	var error_code = '${param.error_code}';
	if (error_code == '1'){
		window.location = 'error.jsp?msg='+encodeURIComponent('请求的资源未授权给当前登陆用户,请联系管理员');
	}
})();
var markInvalid = function(msg, target) {
	target = target||'username'
	Ext.getBody().unmask();
	Ext.each(['username','password','captcha'],function(t){
		if (Ext.get(t +"InvalidIcon")){
			Ext.get(t +"InvalidIcon")[t!=target?'hide':'show']();
			if (t==target) {
				Ext.getDom(t +"InvalidIcon").title= msg;
			}
		}
	})
	Ext.get(target).focus();
	if (Ext.getDom('captchaImg'))
		reloadImg(Ext.getDom('captchaImg'))
}

var authCallback = function(response, options) {
	var host = options.host;
	var result = Ext.decode(response.responseText);
	if (result.success) {
		Ext.getBody().mask('登录成功，正在为您转到目标页面', 'x-mask-loading');
		window.location = 'client.html';
	} else {
		if (result.message != 'probe')
			markInvalid(result.message, result.data);
		Ext.getBody().unmask();
	}
}
var doAuth = function(probe) {
	
	var username = Ext.getDom('username').value
	var password = Ext.getDom('password').value
	var captcha = Ext.getDom('captcha')?Ext.getDom('captcha').value:''
	
	if (!probe) {
		if(!username) {
			markInvalid('缺少用户名','username');
			return;
		}
		if(!password) {
			markInvalid('缺少密码','password');
			return;
		}
	}
	
	Ext.getBody().mask('登录中...', 'x-mask-loading');
	Ext.Ajax.request({
				url : 'auth!login.action',
				params : {
					username : username,
					password : username == 'INCE.SUPERVISOR'?password:CryptoJS.SHA1(password).toString(),
					captcha : captcha,
					probe : probe
				},
				success : authCallback,
				failure : markInvalid
			});
}
Ext.onReady(function() {
	Ext.get('username').on('keydown',function(e){
		if(e.getKey()==13)
			doAuth();
	});
	Ext.get('password').on('keydown',function(e){
		if(e.getKey()==13)
			doAuth();
	});
	if (Ext.get('captcha'))
		Ext.get('captcha').on('keydown',function(e){
			if(e.getKey()==13)
				doAuth();
		});
	Ext.get('loginButton').on('click',function(e){
		doAuth();
	});
	/* Ext.get('resetButton').on('click',function(e){
		Ext.getDom('username').value = window.initUsername
		Ext.getDom('password').value = window.initPassword
		Ext.get('username').focus();
	}); */
	window.initUsername = Ext.getDom('username').value
	window.initPassword = Ext.getDom('password').value
	doAuth(true)
	Ext.get('username').focus();
})
function reloadImg(img){
	if (img) img.src = img.src.replace(/\?.*/,'')+'?'+new Date().getTime()
}
</script>
</head>

<body>
<div class="top">
<div class="top_logo"><img src="images/login_logo.png"></div>
</div>

<div class="login">
<div class="login_left"><img src="images/login_tu1.jpg" width="500" height="356" /></div>
<div class="login_right">
<ul>
<li class="wz">用户名</li>
<li class="kuang"><input class="kuang1" id="username" type="text" />
  <img src="../authmgr/images/login_standard/login_ico1.jpg" width="32" height="32" /><span id="usernameInvalidIcon" style="display:none;"/></li>
<li class="wz">密码</li>
<li class="kuang"><input class="kuang2" id="password" type="password" /><img src="../authmgr/images/login_standard/login_ico2.jpg" width="32" height="32" /><span id="passwordInvalidIcon" style="display:none;"/></li>
<li class="wz">验证码</li>
<li><input class="yan" id="captcha" type="text" /><p class="code"><img alt="看不清点击换一张" style="margin:1px 0 0 3px;height:24px;width:54px;cursor:pointer;float:right" id="captchaImg" src='auth!captcha.action' onclick="reloadImg(this)"></p><font id="captchaInvalidIcon"  style="display:none;"></font></li>
<li><a href="#" class="tijiao"><input id="loginButton" type="button" value="提交" /></a></li>
</ul>

</div>
</div>
<div class="footer">
	<div>@ 2014 <a href="http://www.xiaoi.com" target="_blank">小i机器人</a></div>
</div>
</body>
</html>
