var generateRandomID = function() {
	return 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
		var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
		return v.toString(16);
	});
};
var styles = [ {
	r : [ 50, 60 ],
	color : '#ffc81f',
	borderColor : '#ffc81f',
	borderWidth : 5,
	fontColor : '#FFF',
	fontSize : 40
}, {
	r : [ 35, 35 ],
	color : '#FFF',
	borderColor : '#8fd422',
	borderWidth : 3,
	fontColor : '#8fd422',
	fontSize : 12
}, {
	r : [ 35, 35 ],
	color : '#FFF',
	borderColor : '#8fd422',
	borderWidth : 3,
	fontColor : '#8fd422',
	fontSize : 12
}, {
	r : [ 35, 35 ],
	color : '#FFF',
	borderColor : '#cbe6ff',
	borderWidth : 3,
	fontColor : '#4fa6f6',
	fontSize : 12
} ];

var data = null, nodesMap = {}, topNodeId = null, root = null, debugClientId = null;

$.ajax({
	url : 'ontology-graph!loadGraph.action',
	type : 'GET',
	async : false,
	success : function(respTxt) {
		if (respTxt)
			eval('data = ' + respTxt);
	}
});

if (data && data.elements) {
	for ( var i = 0; i < data.elements.length; i++) {
		var d = data.elements[i];
		var node = {
			fixed : true,
			name : d.name,
			id : d.id,
			clientId : generateRandomID(),
			type : d.type,
			classtype : d.attributes ? d.attributes.classtype : '',
			attributes : d.attributes
		};
		if (!d.pid) {
			node.x = 200;
			node.y = 320;
			node.level = 0;
			node.name = "i";
			root = node;
		} else {
			if (node.name == '小米电视')
				debugClientId = node.clientId;
			node.pid = d.pid;
			if (node.classtype == 0)
				node.level = 1;
			else
				node.level = d.type + 1;
			var pnode = nodesMap[d.pid];
			if (pnode) {
				pnode.children = pnode.children ? pnode.children : [];
				pnode.children.push(node);
			}
		}
		nodesMap[d.id] = node;
	}
};

var m = [ 40, 120, 20, 120 ], w = 1280 - m[1] - m[3], h = 800 - m[0] - m[2], i = 0, root;

var tree = d3.layout.tree().size([ h, w ]);

var selectedNode, nodes;

var diagonal = d3.svg.diagonal().projection(function(d) {
	return [ d.y, d.x ];
});

var vis = d3.select("#graph").append("svg:svg").attr("width", w + m[1] + m[3]).attr("height", h + m[0] + m[2]).append("svg:g").attr("transform",
		"translate(" + m[3] + "," + m[0] + ")");

var dragListener = d3.behavior.drag().on("dragstart", function(d) {
	// console.log("dragstart:", d.x, d.y, d3.event.x, d3.event.y)
}).on("drag", function(d) {
	d.x0 += d3.event.dy;
	d.y0 += d3.event.dx;
	var node = d3.select(this);
	node.attr("transform", "translate(" + d.y0 + "," + d.x0 + ")");

	var source = d;

	nodes.forEach(function(d) {
		if (d.id == source.id) {
			d.x = source.x0;
			d.y = source.y0;
		}
	});
	var link = vis.selectAll("path.link").data(tree.links(nodes), function(d) {
		return d.target.id;
	});

	link.enter().append("path").attr("class", "link").attr("d", diagonal).attr('pointer-events', 'none');

	link.attr("d", diagonal);

	link.exit().remove();
}).on("dragend", function(d) {
	// console.log("dragend:", d.x, d.y, d.px, d.py)
});

root.x0 = h / 2;
root.y0 = 0;

update(root);

function update(source, single) {
	var duration = d3.event && d3.event.altKey ? 5000 : 500;

	var x = source.x, y = source.y;

	nodes = tree.nodes(root).reverse();

	nodes.forEach(function(d) {
		d.y = d.depth * 180;
		if (d.id == source.id && single) {
			d.x = x;
			d.y = y;
		}
	});

	// Update the nodes…
	var node = vis.selectAll("g.node").data(nodes, function(d) {
		return d.id || (d.id = ++i);
	});

	// Enter any new nodes at the parent's previous position.
	var nodeEnter = node.enter().append("svg:g")
	if (!single) {
		nodeEnter.attr("class", "node").attr("transform", function(d) {
			return "translate(" + source.y0 + "," + source.x0 + ")";
		}).on("dblclick", function(d) {
			toggle(d);
			update(d);
		});
	}

	nodeEnter.each(function(d) {
		if (single)
			return;
		var newG = d3.select(this);
		newG.append("circle").attr("class", "node").attr("r", function(d) {
			return styles[d.level].r[1];
		}).style("fill", '#FFF').style("stroke", function(d) {
			return styles[d.level].borderColor;
		}).style("stroke-width", function(d) {
			return styles[d.level].borderWidth;
		}).style("stroke-dasharray", function(d) {
			return styles[d.level].borderDasharray || null;
		});

		newG.append("circle").attr("class", "node0").attr("r", function(d) {
			return styles[d.level].r[0];
		}).style("fill", function(d) {
			return styles[d.level].color || '#FFF';
		}).style("cursor", "pointer");

		if (d.level == 0)
			newG.append("image").attr("width", 142).attr("height", 142).attr("x", -71).attr("y", -71).attr("xlink:href", function(d) {
				return 'images/big_i.png';
			})

		if (d.level == 1)
			newG.append("image").attr("width", 72).attr("height", 72).attr("x", -36).attr("y", -36).attr("xlink:href", function(d) {
				return 'images/bgedit1.png';
			}).style("cursor", "pointer");

		newG.append("text").attr("dy", 4).attr("text-anchor", "middle").attr('class', 'text').filter(function(d) {
			return d.level > 0;
		}).style("fill", function(d) {
			return styles[d.level].fontColor || '#FFF';
		}).text(function(d) {
			var name = d.name;
			if (d.name.lastIndexOf('*') == d.name.length - 1)
				name = d.name.substring(0, d.name.length - 1);
			return name;
		}).style("font-size", function(d) {
			return styles[d.level].fontSize;
		}).style("font-family", "微软雅黑").style("font-weight", "bold").style("cursor", "pointer");

		newG.on("click", function(d0, i0) {
			if (onNodeClick)
				onNodeClick.call(this, d0);
		}).call(dragListener);
		newG.on("click").call(newG[0][0], newG.data()[0]); // sucks...
	});

	// Transition nodes to their new position.
	var nodeUpdate = node.transition().duration(duration).attr("transform", function(d) {
		return "translate(" + d.y + "," + d.x + ")";
	});

	var nodeExit = node.exit().transition().duration(duration).attr("transform", function(d) {
		return "translate(" + source.y + "," + source.x + ")";
	}).remove();

	var link = vis.selectAll("path.link").data(tree.links(nodes), function(d) {
		return d.target.id;
	});

	// #4fa6f6
	link.enter().insert("svg:path", "g").attr("class", "link").attr("d", function(d) {
		var o = {
			x : source.x0,
			y : source.y0
		};
		return diagonal({
			source : o,
			target : o
		});
	}).transition().duration(duration).attr("d", diagonal);

	link.transition().duration(duration).attr("d", diagonal);

	link.exit().transition().duration(duration).attr("d", function(d) {
		var o = {
			x : source.x,
			y : source.y
		};
		return diagonal({
			source : o,
			target : o
		});
	}).remove();

	// Stash the old positions for transition.
	nodes.forEach(function(d) {
		d.x0 = d.x;
		d.y0 = d.y;
	});
}

var delBtn = $('#delBtn'), addOntologyBtn = $('#addOntologyBtn'), addObjectBtn = $('#addObjectBtn');

var onNodeClick = function(d0) {
	if (d0.level > 0) {
		vis.selectAll('.node0').filter(function(d) {
			return d.level >= 1;
		}).style("fill", function(d, i) {
			return '#FFF';
		});
		d3.select(this.childNodes[1]).style("fill", function(d) {
			var s = styles[d.level];
			return s.fontColor;
		});
		vis.selectAll('text').filter(function(d) {
			return d.level >= 1;
		}).style("fill", function(d, i) {
			return styles[d.level].fontColor;
		});
		d3.select(this.childNodes[2]).style('fill', function(d) {
			return '#FFF';
		});
		selectedNode = d0;
		delBtn.removeAttr('disabled');
		addOntologyBtn.removeAttr('disabled');
		addObjectBtn.removeAttr('disabled');
		d3.selectAll('g').selectAll('image').filter(function(d) {
			return d.level == 1;
		}).attr("xlink:href", function(d) {
			return 'images/bgedit1.png';
		});
		if (!selectedNode.id || typeof selectedNode.id == 'number') {
			addOntologyBtn.attr('disabled', '');
			addObjectBtn.attr('disabled', '');
		} else if (selectedNode.level == 1) {
			d3.select(this).selectAll('image').attr("xlink:href", function(d) {
				return 'images/bgedit0.png';
			});
			d3.select(this).selectAll('text').style('fill', '#FFF');
			delBtn.attr('disabled', '');
		} else if (selectedNode.level == 2) {
			if (selectedNode.children && selectedNode.children.length) {
				delBtn.attr('disabled', '');
			}
		} else if (selectedNode.level == 3) {
			addOntologyBtn.attr('disabled', '');
			addObjectBtn.attr('disabled', '');
		}
		renderEditPanel(selectedNode);
	}
};

function toggleAll(d) {
	if (d.children) {
		d.children.forEach(toggleAll);
		toggle(d);
	}
}
// Toggle children.
function toggle(d) {
	if (d.children) {
		d._children = d.children;
		d.children = null;
	} else {
		d.children = d._children;
		d._children = null;
	}
}

function addNode(isOntology) {
	if (!selectedNode)
		return;
	var newNode = {
		level : isOntology ? 2 : 3,
		name : isOntology ? '新本体类' : '新实例',
		type : isOntology ? 1 : 2,
		pid : selectedNode.id,
		clientId : generateRandomID(),
		fixed : true
	};
	var r = findNode(root, selectedNode.id);
	if (r) {
		var p = r[0].children[r[1]];
		if (!p.children)
			p.children = [ newNode ];
		else
			p.children.push(newNode);
		update(p);
	}
}

function deleteNode() {
	if (!selectedNode)
		return;
	var r = findNode(root, selectedNode.id);
	// console.log("deleteNode:" + r)
	if (r) {
		var removeNode = r[0].children.splice(r[1], 1);
		removeNode = removeNode[0];
		if (!removeNode.id || typeof removeNode.id == 'number')
			update(r[0]);
		else
			removeElement(removeNode, function() {
				update(r[0]);
			});
	}
}

function findNode(node, id, f) {
	if (node.children) {
		for ( var i = 0; i < node.children.length; i++) {
			var n = node.children[i];
			var field = f ? n[f] : n.id;
			if (field == id)
				return [ node, i ];
			var r = findNode(n, id, f);
			if (r)
				return r;
		}
	} else
		return null;
}

$(function() {
	delBtn.click(function() {
		XIDialog.confirm({
			confirm : function(yes) {
				if (yes)
					deleteNode();
			},
			msg : '确认删除该节点？',
			title : '提示'
		});
	});
	addOntologyBtn.click(function() {
		addNode(true);
	});
	addObjectBtn.click(function() {
		addNode();
	});

	$('a', $('#qlist')).each(function() {
		$(this).attr('target', 'robot').attr('href', 'http://122.226.240.160/robot/demo/weixin/?q=' + encodeURIComponent($(this).html()));
	});

	var g = d3.selectAll('g.node').filter(function(d) {
		return d.name.lastIndexOf('*') == d.name.length - 1;
		// return d.attributes && d.attributes.sb == 'EMPTY';
	});
	if (g[0].length)
		g.on("click").call(g[0][0], g.data()[0]); // sucks again...
});

var elementSaved = function(d) {
	var ret = findNode(root, d.clientId, 'clientId');
	var node = ret[0].children[ret[1]];
	for ( var key in d) {
		node[key] = d[key];
	}
	d3.selectAll('g.node').filter(function(d) {
		return d.id == node.id;
	}).select("text").text(function(d) {
		return d.name;
	});
};

var elementRemoved = function(d) {
};

// setTimeout(function() {
// elementSaved({
// name : 'fred',
// id : '123',
// clientId : debugClientId
// });
// }, 2000);
$(".function-block").draggable({
	opacity : 0.7,
	helper : "clone"
});
$(".content_center").droppable({
	drop : function(event, ui) {
		// alert(ui.draggable.find('span').html());
	}
});

var buildGraph = function(name) {
	$('iframe', $('#graph0')).attr('src', "test.html?n=" + encodeURIComponent(name));
	$('#graph0').dialog({
		close : function(event, ui) {
			$('iframe', $('#graph0')).attr('src', '');
		},
		modal : true,
		width : 640,
		height : 500
	});
};