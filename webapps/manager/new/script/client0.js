var generateRandomID = function() {
	return 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
		var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
		return v.toString(16);
	});
};
var getRandomInt = function(min, max) {
	return Math.floor(Math.random() * (max - min + 1) + min);
};

var data = a, nodes = [], nodesMap = {}, lines = [], topNodeId = null, treeData = [];
// if (data && data.elements) {
// for ( var i = 0; i < data.elements.length; i++) {
// var d = data.elements[i];
// var node = {
// name : d.name,
// id : d.id
// };
// if (!d.pid) {
// topNodeId = d.id;
// node.x = 200;
// node.y = 320;
// node.level = 0;
// node.fixed = true;
// } else if (d.pid == topNodeId) {
// node.level = 1;
// } else {
// node.level = d.type + 1;
// }
// nodes.push(node);
// nodesMap[d.id] = node;
// if (d.pid) {
// lines.push({
// target : node,
// source : nodesMap[d.pid],
// id : generateRandomID()
// });
// }
// }
// }

if (data && data.elements) {
	for ( var i = 0; i < data.elements.length; i++) {
		var d = data.elements[i];
		var node = {
			fixed : true,
			name : d.name,
			id : d.id
		};
		if (!d.pid) {
			node.x = 200;
			node.y = 320;
			node.level = 0;
			node.name = "i";
			topNodeId = d.id;
			treeData = node;
		} else {
			if (d.pid == topNodeId)
				node.level = 1;
			else
				node.level = d.type + 1;
			var pnode = nodesMap[d.pid];
			if (pnode) {
				pnode.children = pnode.children ? pnode.children : [];
				pnode.children.push(node);
			}
		}
		nodesMap[d.id] = node;
	}
}

// $.ajax({
// url : 'ontology-graph!loadGraph.action',
// type : 'GET',
// async : false,
// success : function(respTxt) {
// if (respTxt)
// eval('data = ' + respTxt);
// }
// });

// {
// r : [ 35, 40 ],
// color : '#8fd422',
// borderColor : '#8fd422',
// borderWidth : 3,
// borderDasharray : '10 5',
// fontColor : '#FFF',
// fontSize : 14
// }

var styles = [ {
	r : [ 50, 60 ],
	color : '#ffc81f',
	borderColor : '#ffc81f',
	borderWidth : 5,
	fontColor : '#FFF',
	fontSize : 40
}, {
	r : [ 35, 35 ],
	color : '#FFF',
	borderColor : '#8fd422',
	borderWidth : 3,
	fontColor : '#8fd422',
	fontSize : 12
}, {
	r : [ 35, 35 ],
	color : '#FFF',
	borderColor : '#8fd422',
	borderWidth : 3,
	fontColor : '#8fd422',
	fontSize : 12
}, {
	r : [ 35, 35 ],
	color : '#FFF',
	borderColor : '#cbe6ff',
	borderWidth : 3,
	fontColor : '#4fa6f6',
	fontSize : 12
} ];

var editDescs = [ {
	title : "基本属性",
	nameLabel : "公司名称",
	buttonLabel : ""
}, {
	title : "本体类属性",
	nameLabel : "类名称",
	buttonLabel : "创建实例"
}, {
	title : "用户类属性",
	nameLabel : "类名称",
	buttonLabel : "创建实例"
}, {
	title : "实例属性",
	nameLabel : "实例名称",
	buttonLabel : "编辑模板"
} ];

var width = 1200;
var height = 800;

var svg = d3.select("#graph").append("svg").attr("width", width).attr("height", height);

var tree = d3.layout.tree().size([ $('.content_center')[0].offsetHeight - 100, $('.content_center')[0].offsetWidth - 120 ]).separation(
		function(a, b) {
			return (a.parent == b.parent ? 2 : 1) / a.depth;
		});

nodes = tree.nodes(treeData);
lines = tree.links(nodes);
for ( var i = 0; i < nodes.length; i++) {
	var x = nodes[i].x;
	var y = nodes[i].y;
	nodes[i].x = y + 70;
	nodes[i].y = x + 50;
}

var force = d3.layout.force().nodes(nodes).links(lines).size([ width, height ]).linkDistance(100).charge([ -700 ]).gravity(0).start();

var links0 = force.links(), nodes0 = force.nodes();

function buildForce() {
	// force.nodes(nodes0).links(links0);
	var xx = svg.selectAll(".line").data(links0);

	xx.enter().append("line").each(function(d) {
		d3.select(this).attr("class", "line").style("stroke", "#4fa6f6").style("stroke-width", 2);
	});

	// xx.exit().remove();
	// svg.selectAll(".gnode").remove();
	var yy = svg.selectAll(".gnode").data(nodes0);
	yy.enter().append('g').classed('gnode', true).each(function(d) {
		var newG = d3.select(this);

		newG.append("circle").attr("class", "node").attr("r", function(d) {
			return styles[d.level].r[1];
		}).style("fill", '#FFF').style("stroke", function(d) {
			return styles[d.level].borderColor;
		}).style("stroke-width", function(d) {
			return styles[d.level].borderWidth;
		}).style("stroke-dasharray", function(d) {
			return styles[d.level].borderDasharray || null;
		});

		newG.append("circle").attr("class", "node0").attr("r", function(d) {
			return styles[d.level].r[0];
		}).style("fill", function(d) {
			return styles[d.level].color || '#FFF';
		}).style("cursor", "pointer");

		newG.append("text").attr("dy", 4).attr("text-anchor", "middle").attr('class', 'text').style("fill", function(d) {
			return styles[d.level].fontColor || '#FFF';
		}).text(function(d) {
			return d.name;
		}).style("font-size", function(d) {
			return styles[d.level].fontSize;
		}).style("font-family", "微软雅黑").style("font-weight", "bold").style("cursor", "pointer");

		if (d.level == 1)
			newG.append("image").attr("width", 15).attr("height", 19).attr("x", -6).attr("y", 7).attr("xlink:href", function(d) {
				return 'images/lock0.png';
			});

		newG.attr("transform", function(d) {
			return 'translate(' + [ d.x, d.y ] + ')';
		}).on("click", function(d0, i0) {
			onNodeClick.call(this, d0);
		}).call(force.drag);
		$(newG[0][0]).trigger('click');
	});
	// yy.exit().remove();

	force.start();
}

var delBtn = $('#delBtn'), addOntologyBtn = $('#addOntologyBtn'), addObjectBtn = $('#addObjectBtn');

var onNodeClick = function(d0) {
	if (d0.level >= 1) {
		svg.selectAll('.node0').filter(function(d) {
			return d.level >= 1;
		}).style("fill", function(d, i) {
			return '#FFF';
		});
		d3.select(this.childNodes[1]).style("fill", function(d) {
			var s = styles[d.level];
			return s.fontColor;
		});
		svg.selectAll('text').filter(function(d) {
			return d.level >= 1;
		}).style("fill", function(d, i) {
			return styles[d.level].fontColor;
		});
		d3.select(this.childNodes[2]).style('fill', function(d) {
			return '#FFF';
		});
		selectedNode = d0;
		delBtn.removeAttr('disabled');
		addOntologyBtn.removeAttr('disabled');
		addObjectBtn.removeAttr('disabled');
		if (selectedNode.level == 1) {
			delBtn.attr('disabled', '');
			d3.selectAll('.gnode').selectAll('image').attr("xlink:href", function(d) {
				return 'images/lock0.png';
			});
			d3.select(this).selectAll('image').attr("xlink:href", function(d) {
				return 'images/lock1.png';
			});
		} else if (selectedNode.level == 2) {
			var hasChild = false;
			for ( var i = 0; i < links0.length; i++) {
				if ((links0[i].target.id == selectedNode.id) || (links0[i].source.id == selectedNode.id)) {
					hasChild = true;
					break;
				}
			}
			if (hasChild) {
				delBtn.attr('disabled', '');
			}
		} else if (selectedNode.level == 3) {
			addOntologyBtn.attr('disabled', '');
			addObjectBtn.attr('disabled', '');
		}
	}
	if (d0.level > 0)
		renderEditPanel(d0);
};

var selectedNode = null;

var addNode = function(isOntology) {
	var newNode = {
		x : selectedNode.x + getRandomInt(-100, 100),
		y : selectedNode.y + getRandomInt(-100, 100),
		level : isOntology ? 2 : 3,
		name : '新实例',
		id : generateRandomID(),
		fixed : true
	};
	var newLine = {
		source : selectedNode,
		target : newNode,
		id : generateRandomID()
	};
	nodes0.push(newNode);
	links0.push(newLine);
	buildForce();
};

var removeNode = function() {
	if (!selectedNode) {
		alert('请先选择你需要删除的节点。');
		return false;
	}
	removeNode0();
};

var removeNode0 = function() {
	for ( var i = 0; i < links0.length; i++) {
		if (links0[i].target.id == selectedNode.id) {
			links0.splice(i, 1);
		}
	}
	for ( var i = 0; i < nodes0.length; i++) {
		if (nodes0[i].id == selectedNode.id) {
			nodes0.splice(i, 1);
		}
	}
	svg.selectAll(".line").data(links0, function(d) {
		return d.target.id;
	}).exit().remove();
	svg.selectAll(".gnode").data(nodes0, function(d) {
		return d.id;
	}).exit().remove();
};

$(function() {
	$('#delBtn').click(function() {
		removeNode();
	});
	$('#addOntologyBtn').click(function() {
		addNode(true);
	});
	$('#addObjectBtn').click(function() {
		addNode();
	});
});

var renderEditPanel = function(d) {
	var editDesc = editDescs[d.level];
	$("#epName").val(d.name);
	$("#epTitle").html(editDesc.title);
	$("#epNameLabel").html(editDesc.nameLabel);
	$("#epButtonLabel").html(editDesc.buttonLabel);

	var b = $("#epButton");
	b.unbind("click");
	b.css('display', editDesc.buttonLabel ? 'block' : 'none');
	if (d.level == 1 || d.level == 2) {
		b.bind("click", function() {

		});
	} else if (d.level == 3) {
		b.bind("click", function() {
			var p = $("#tmpltEditPanel");
			var x = ($(window).height() - p.height()) / 2;
			var y = ($(window).width() - p.width()) / 2;
			p.css("top", x).css("left", y).show("slow");
			var btn = $("[name=tmpltCloseBtn]");
			btn.bind("click", function() {
				btn.unbind("click");
				p.unbind("click");
				p.hide("slow");
			});
		});
	}
};

var tick = function() {
	// Update the links
	svg.selectAll(".line").attr("x1", function(d) {
		return d.source.x;
	}).attr("y1", function(d) {
		return d.source.y;
	}).attr("x2", function(d) {
		return d.target.x;
	}).attr("y2", function(d) {
		return d.target.y;
	});
	// Translate the groups
	svg.selectAll(".gnode").attr("transform", function(d) {
		return 'translate(' + [ d.x, d.y ] + ')';
	});
};

buildForce();
force.on("tick", tick);