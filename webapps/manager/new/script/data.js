var a = {
	"obj_relations" : [ {
		"id" : "00142225938326200000f01fafe6bf45",
		"part" : "008080814b390d8e014b3ebb06a80064"
	} ],
	"elements" : [ {
		"id" : "008080814b390d8e014b4ecf766400c3",
		"type" : 2,
		"name" : "迪信通",
		"attributes" : {}
	}, {
		"id" : "008080814b390d8e014b3eb681dd0059",
		"pid" : "008080814b390d8e014b4ecf766400c3",
		"type" : 1,
		"name" : "手机cpu",
		"attributes" : {
			"sb" : "[手机?][cpu]"
		}
	}, {
		"id" : "008080814b390d8e014b3eb93ed8005a",
		"pid" : "008080814b390d8e014b3eb681dd0059",
		"type" : 1,
		"name" : "联发科cpu",
		"attributes" : {
			"sb" : "[联发科][cpu]"
		}
	}, {
		"id" : "008080814b390d8e014b3eb943cf005b",
		"pid" : "008080814b390d8e014b3eb681dd0059",
		"type" : 1,
		"name" : "高通cpu",
		"attributes" : {
			"sb" : "[高通][cpu]"
		}
	}, {
		"id" : "008080814b390d8e014b3eb9d965005c",
		"pid" : "008080814b390d8e014b3eb681dd0059",
		"type" : 1,
		"name" : "苹果cpu",
		"attributes" : {
			"sb" : "[苹果][cpu]"
		}
	}, {
		"id" : "028ba6c070d4418b8c644e6281ac01cf",
		"pid" : "008080814b390d8e014b4ecf766400c3",
		"type" : 1,
		"name" : "手机",
		"attributes" : {
			"sb" : "[手机]"
		}
	}, {
		"id" : "008080814af7e8a6014af8128ac20002",
		"pid" : "028ba6c070d4418b8c644e6281ac01cf",
		"type" : 1,
		"name" : "小米手机",
		"attributes" : {
			"sb" : "[小米][手机?]"
		}
	}, {
		"id" : "008080814af7e8a6014af8130e080003",
		"pid" : "028ba6c070d4418b8c644e6281ac01cf",
		"type" : 1,
		"name" : "三星手机",
		"attributes" : {
			"sb" : "[三星][手机?]"
		}
	}, {
		"id" : "008080814af7e8a6014af8135ebf0004",
		"pid" : "028ba6c070d4418b8c644e6281ac01cf",
		"type" : 1,
		"name" : "魅族手机",
		"attributes" : {
			"sb" : "[魅族][手机?]"
		}
	}, {
		"id" : "873ab4d971bb424f808f3881cc6714e9",
		"pid" : "008080814b390d8e014b4ecf766400c3",
		"type" : 1,
		"name" : "家电类",
		"attributes" : {
			"sb" : "电视"
		}
	}, {
		"id" : "00142225938339000039f01fafe6bf45",
		"pid" : "008080814af7e8a6014af8128ac20002",
		"type" : 2,
		"name" : "小米2",
		"attributes" : {
			"sb" : "[小米2][手机?]"
		}
	}, {
		"id" : "00142225938348900078f01fafe6bf45",
		"pid" : "008080814af7e8a6014af8128ac20002",
		"type" : 2,
		"name" : "小米3",
		"attributes" : {
			"sb" : "[小米3][手机?]"
		}
	}, {
		"id" : "00142225938326200000f01fafe6bf45",
		"pid" : "008080814af7e8a6014af8128ac20002",
		"type" : 2,
		"name" : "红米",
		"attributes" : {
			"sb" : "[红米][手机?]"
		}
	}, {
		"id" : "00142225938399100117f01fafe6bf45",
		"pid" : "008080814af7e8a6014af8128ac20002",
		"type" : 2,
		"name" : "小米4",
		"attributes" : {
			"sb" : "[小米4][手机?]"
		}
	}, {
		"id" : "00142225938406400156f01fafe6bf45",
		"pid" : "008080814af7e8a6014af8135ebf0004",
		"type" : 2,
		"name" : "魅族4",
		"attributes" : {
			"sb" : "[魅族4]"
		}
	}, {
		"id" : "008080814b390d8e014b3eba8b11005d",
		"pid" : "008080814b390d8e014b3eb943cf005b",
		"type" : 2,
		"name" : "高通805",
		"attributes" : {
			"sb" : "[高通805]"
		}
	}, {
		"id" : "008080814b390d8e014b3f6afa7100b0",
		"pid" : "008080814b390d8e014b3eb93ed8005a",
		"type" : 2,
		"name" : "联发科6595",
		"attributes" : {
			"sb" : "[联发科6595]"
		}
	}, {
		"id" : "008080814b390d8e014b3ebb6c7b006b",
		"pid" : "008080814b390d8e014b3eb943cf005b",
		"type" : 2,
		"name" : "高通800",
		"attributes" : {
			"sb" : "[高通800]"
		}
	}, {
		"id" : "008080814b390d8e014b3ebbd3180072",
		"pid" : "008080814b390d8e014b3eb9d965005c",
		"type" : 2,
		"name" : "苹果A8",
		"attributes" : {
			"sb" : "[苹果A8]"
		}
	}, {
		"id" : "008080814b390d8e014b3ebb06a80064",
		"pid" : "008080814b390d8e014b3eb943cf005b",
		"type" : 2,
		"name" : "高通801",
		"attributes" : {
			"sb" : "[高通801]"
		}
	}, {
		"id" : "008080814b390d8e014b3ebdfd0a007c",
		"pid" : "008080814b390d8e014b3eb93ed8005a",
		"type" : 2,
		"name" : "联发科6592",
		"attributes" : {
			"sb" : "[联发科6592]"
		}
	}, {
		"id" : "008080814b298264014b29b099fb0082",
		"pid" : "028ba6c070d4418b8c644e6281ac01cf",
		"type" : 2,
		"name" : "IPHONE6",
		"attributes" : {
			"sb" : "[IPHONE6]"
		}
	} ]
};

// nodes = [ {
// name : "企业",
// level : 0,
// fixed : true,
// x : 180,
// y : 320
// }, {
// name : "电脑",
// level : 1
// }, {
// name : "CPU",
// level : 1
// }, {
// name : "手机",
// level : 1
// }, {
// name : "高通",
// level : 2
// }, {
// name : "联发科",
// level : 2
// }, {
// name : "苹果",
// level : 2
// }, {
// name : "小米",
// level : 2
// }, {
// name : "魅族",
// level : 2
// }, {
// name : "苹果",
// level : 2
// }, {
// name : "高通800",
// level : 3
// }, {
// name : "高通801",
// level : 3
// }, {
// name : "小米2",
// level : 3
// }, {
// name : "小米3",
// level : 3
// }, {
// name : "小米4",
// level : 3
// } ];
// for ( var i = 0; i < nodes.length; i++) {
// nodes[i].id = generateRandomID();
// }

// lines = [ {
// source : nodes[0],
// target : nodes[1]
// }, {
// source : nodes[0],
// target : nodes[2]
// }, {
// source : nodes[0],
// target : nodes[3]
// }, {
// source : nodes[2],
// target : nodes[4]
// }, {
// source : nodes[2],
// target : nodes[5]
// }, {
// source : nodes[2],
// target : nodes[6]
// }, {
// source : nodes[3],
// target : nodes[7]
// }, {
// source : nodes[3],
// target : nodes[8]
// }, {
// source : nodes[3],
// target : nodes[9]
// }, {
// source : nodes[4],
// target : nodes[10]
// }, {
// source : nodes[4],
// target : nodes[11]
// }, {
// source : nodes[7],
// target : nodes[12]
// }, {
// source : nodes[7],
// target : nodes[13]
// }, {
// source : nodes[7],
// target : nodes[14]
// }, {
// source : nodes[11],
// target : nodes[13]
// }, {
// source : nodes[11],
// target : nodes[14]
// } ];
// for ( var i = 0; i < lines.length; i++) {
// lines[i].id = generateRandomID();
// }

// var treeData = {
// "name" : "A",
// level : 0,
// fixed : true,
// "children" : [ {
// fixed : true,
// "name" : "A1",
// level : 1,
// "children" : [ {
// fixed : true,
// "name" : "A1",
// level : 1
// }, {
// fixed : true,
// "name" : "A2",
// level : 1
// }, {
// fixed : true,
// "name" : "A3",
// level : 1
// } ]
// }, {
// fixed : true,
// "name" : "A2",
// level : 1,
// "children" : [ {
// fixed : true,
// "name" : "A1",
// level : 1
// }, {
// fixed : true,
// "name" : "A2",
// level : 1
// }, {
// fixed : true,
// "name" : "A3",
// level : 1
// } ]
// }, {
// fixed : true,
// "name" : "A3",
// level : 1
// } ]
// };

// var root = {
// "name" : "flare",
// "level" : 0,
// "children" : [ {
// "name" : "analytics",
// "level" : 2,
// "children" : [ {
// "name" : "cluster",
// "level" : 3,
// "children" : [ {
// "name" : "AgglomerativeCluster",
// "level" : 3
// }, {
// "name" : "CommunityStructure",
// "level" : 3
// }, {
// "name" : "HierarchicalCluster",
// "level" : 3
// }, {
// "name" : "MergeEdge",
// "level" : 3
// } ]
// }, {
// "name" : "graph",
// "level" : 3,
// "children" : [ {
// "name" : "BetweennessCentrality",
// "level" : 3
// }, {
// "name" : "LinkDistance",
// "level" : 3
// }, {
// "name" : "MaxFlowMinCut",
// "level" : 3
// }, {
// "name" : "ShortestPaths",
// "level" : 3
// }, {
// "name" : "SpanningTree",
// "level" : 3
// } ]
// }, {
// "name" : "optimization",
// "level" : 3,
// "children" : [ {
// "name" : "AspectRatioBanker",
// "level" : 3
// } ]
// } ]
// }, {
// "name" : "animate",
// "level" : 2,
// "children" : [ {
// "name" : "Easing",
// "level" : 3
// }, {
// "name" : "FunctionSequence",
// "level" : 3
// }, {
// "name" : "interpolate",
// "level" : 3,
// "children" : [ {
// "name" : "ArrayInterpolator",
// "level" : 3
// }, {
// "name" : "ColorInterpolator",
// "level" : 3
// }, {
// "name" : "DateInterpolator",
// "level" : 3
// }, {
// "name" : "Interpolator",
// "level" : 3
// } ]
// }, {
// "name" : "ISchedulable",
// "level" : 3
// }, {
// "name" : "Parallel",
// "level" : 3
// } ]
// } ]
// };
