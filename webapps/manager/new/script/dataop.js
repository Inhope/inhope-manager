var editDescs = ['', {
	  title : "本体类属性",
	  nameLabel : "类名称",
	  buttonLabel : "创建实例"
  }, {
	  title : "实例属性",
	  nameLabel : "实例名称",
	  buttonLabel : "编辑模板"
  }]
var rowtpl = '<div class="input_box cl6"><span class="epNameLabel"></span><input class="epName" type="text"></div>'
var TYPE_CLASS = 1;
var TYPE_OBJ = 2;

// var elementSaved = function(d) {
// console.log('elementSaved', d)
// }
// var elementRemoved = function(d) {
// console.log('elementRemoved', d)
// }
var removeElement = function(d, cb) {
	if (d.type == TYPE_OBJ) {// 实例
		$.post("ontology-object!del.action", {
			  objectId : d.id
		  }, function(data, textStatus, jqXHR) {
			  if (data) {
				  XIDialog.alert('删除成功');
				  cb ? cb(d) : 0
			  } else {
				  XIDialog.alert('no data')
			  }
		  });
	} else if (d.type == TYPE_CLASS) {
		$.post("ontology-class!del.action", {
			  classId : d.id
		  }, function(data, textStatus, jqXHR) {
			  if (data) {
				  XIDialog.alert('删除成功')
				  cb ? cb(d) : 0
			  } else {
				  XIDialog.alert('no data')
			  }
		  });
	}
}
var renderEditPanel = function(d) {
	// d.type = TYPE_OBJ
	// save
	// d.pid = '008080814af7e8a6014af8128ac20002'
	// d.client_id = 'newitem' + new Date().getTime()
	// update
	// d.pid = '008080814af7e8a6014af8128ac20002'
	// d.id = '00142225938326200000f01fafe6bf45'

	// d.type = TYPE_CLASS
	// save
	// d.pid = '028ba6c070d4418b8c644e6281ac01cf'
	// d.client_id = 'newitem' + new Date().getTime()
	// update
	// d.id = '2c90819f4b553a81014b5541e308000c'
	if (typeof d.id == 'number') {
		delete d.id
	}
	$('#ep').block({
		  message : '加载中...',
		  overlayCSS : {
			backgroundColor : '#EEE'
		  },
		  css : {
			  width : '200px',
			  backgroundColor : '#eee',
			  border : '2px solid #aaa'
		  }
	  });
	if (d.type == TYPE_OBJ) {// 实例
		var o = {
			objectId : d.id,
			catePath : '专业知识',
			classes : [{
				  id : d.pid
			  }]
		}
		$.post("ontology-object!edit.action", {
			  data : $.toJSON(o),
			  embedSemanticWordclass : true
		  }, function(data, textStatus, jqXHR) {
			  $('#ep').unblock();
			  if (data) {
				  editElement(d, $.evalJSON(data).data)
			  } else {
				  XIDialog.alert('no data')
			  }
		  });
	} else if (d.type == TYPE_CLASS) {
		if (d.classtype == null)
			d.classtype = 1
		$.post("ontology-graph!editClass.action", {
			  id : d.id,
			  pid : d.pid
		  }, function(data, textStatus, jqXHR) {
			  $('#ep').unblock();
			  if (data) {
				  editElement(d, $.evalJSON(data).data)
			  } else {
				  XIDialog.alert('no data')
			  }
		  });
	}
}
var varP = /\$\{([^{}]+)\}/g;
function grabVariables(ans, first) {
	var vars = []
	while (varP.test(ans)) {
		vars.push(RegExp.$1);
	}
	return first ? (vars.length ? vars[0] : null) : vars;
}
function createRow(name, value, i, d_id) {
	var rowel = $(rowtpl)
	rowel.find('.epNameLabel').html(name);
	var field = rowel.find('.epName').attr('idx', '' + i).attr('name', name)
	value ? field.val(value) : 0
	$('#ep_body').append(rowel)
	if (name == '生产商' && d_id) {
		var w = $('input', rowel).css('width');
		$('input', rowel).css('width', parseInt(w) - 30);
		var img = $('<img/>').css('width', '30px').css('height', '30').css('cursor', 'pointer').attr('src', 'images/graph.gif');
		img.click(function() {
			  if (window.buildGraph)
				  buildGraph(value);
		  });
		rowel.append(img);
	}
	return [rowel, field]
}
var curtarget;
var IDX_OBJ_NAME = -1
var IDX_OBJ_SB = -2
var editElement = function(uitarget, data) {
	curtarget = uitarget;
	var editDesc = editDescs[uitarget.type];
	if (uitarget.type == TYPE_OBJ) {// 实例
		uitarget.attachment = data
		$('#ep_body').html('')
		// 添加实例的字段属性
		createRow('实例名称', data.name, IDX_OBJ_NAME, uitarget.id)
		var sb = ''
		var keepsb = false
		if (data.semanticBlock) {
			if (data.semanticBlock.indexOf('::') != -1) {
				var parts = data.semanticBlock.split('::')
				data.semanticBlock = parts[0]
				sb = parts[1].split(',')
				for (var i = 0; i < sb.length; i++) {
					if (sb[i] == data.name) {
						sb.splice(i, 1)
						i--;
					}
				}
				sb = sb.join(',')
			} else {
				keepsb = true
			}
		}
		var f = createRow('对象别名', sb, IDX_OBJ_SB)[1].attr('placeholder', '多个别名用逗号分隔')
		keepsb ? f.attr('readOnly', true) : 0
		var attrs = data.attributes
		for (var i = 0; i < attrs.length; i++) {
			if (!(attrs[i].values && attrs[i].values.length)) {
				attrs.splice(i, 1)
				i--
			}
		}
		// 添加知识点属性
		$.each(attrs, function(i, a) {
			  var ans = a.values[0].value;
			  if (ans) {
				  var var0 = grabVariables(ans, true);
				  if (!var0) {
					  // console.log('属性' + a.attributeName + '的答案模板没有编写变量，请检查')
					  return;
				  }
				  var varvalue = ''
				  if (a.variables) {
					  $.each(a.variables, function(ii, v) {
						    if (v.name == var0 && v.type != 4 && v.value) {
							    varvalue = v.value
							    return false;
						    }
					    })
				  }
				  createRow(a.attributeName, varvalue, i, uitarget.id)
			  }
		  })
	} else if (uitarget.type == TYPE_CLASS) {
		// 编辑本体类
		uitarget.attachment = data
		$('#ep_body').html('')
		// 添加本体类的字段属性
		createRow('本体类名称', data.name, IDX_OBJ_NAME)
		var sb = ''
		data.semanticBlock = data.sb
		var keepsb = false
		if (data.semanticBlock) {
			if (data.semanticBlock.indexOf('::') != -1) {
				var parts = data.semanticBlock.split('::')
				data.semanticBlock = parts[0]
				sb = parts[1].split(',')
				for (var i = 0; i < sb.length; i++) {
					if (sb[i] == data.name) {
						sb.splice(i, 1)
						i--;
					}
				}
				sb = sb.join(',')
			} else {
				keepsb = true
			}
		}
		var f = createRow('本体类别名', sb, IDX_OBJ_SB)[1].attr('placeholder', '多个别名用逗号分隔')
		keepsb ? f.attr('readOnly', true) : 0
		// 添加只读的属性列表
		$.each(data.attributeNames, function(i, a) {
			  $('#ep_body').append('<div class="input_box cl6"><span style="width:100%"> - ' + a + '</span></div>')
		  })
		$('#saveBtn')[uitarget.classtype ? 'show' : 'hide']()
	}
	$("#epTitle").html(editDesc.title);
	// $("#epButtonLabel").html(editDesc.buttonLabel);

	// var b = $("#epButton");
	// b.unbind("click");
	// b.css('display', editDesc.buttonLabel ? 'block' : 'none');
	// if (uitarget.type == TYPE_OBJ) {
	// b.bind("click", function() {
	// var p = $("#tmpltEditPanel");
	// var x = ($(window).height() - p.height()) / 2;
	// var y = ($(window).width() - p.width()) / 2;
	// p.css("top", x).css("left", y).show("slow");
	// var btn = $("[name=tmpltCloseBtn]");
	// btn.bind("click", function() {
	// btn.unbind("click");
	// p.unbind("click");
	// p.hide("slow");
	// })
	// });
	// }
}
var saveElementData = function() {
	var t = curtarget
	if (!t)
		return;
	if (t.type == TYPE_OBJ) {// 实例
		var o = $.extend({}, t.attachment)
		var attrs = o.attributes
		var valid = true
		$('#ep_body .epName').each(function(i, field) {
			  field = $(field)
			  var idx = parseInt($(field).attr('idx'))
			  var val = field.val();
			  if (idx == IDX_OBJ_NAME) {
				  if (!val) {
					  XIDialog.alert(field.attr('name') + '未填写')
					  return valid = false;
				  }
				  o.name = val
			  } else if (idx == IDX_OBJ_SB) {
				  if (o.semanticBlock && o.semanticBlock.indexOf('[') != -1) {// 存在原始语义块,不处理
					  ;
				  } else {
					  var wc = o.semanticBlock
					  if (!wc)
						  wc = o.name
					  if (!val)
						  val = o.name
					  var sb = val.split(',')
					  var onamefound = false
					  for (var k = 0; k < sb.length; k++) {
						  if (sb[k] == o.name) {
							  onamefound = true
							  break;
						  }
					  }
					  if (!onamefound) {
						  sb.push(o.name)
					  }
					  o.semanticBlock = wc + '::' + sb.join(',')
				  }
			  } else {
				  var a = attrs[idx]
				  if (a) {
					  var ans = a.values && a.values.length && a.values[0].value;
					  if (ans) {
						  var var0 = grabVariables(ans, true);
						  if (!var0) {
							  // console.log('属性' + a.attributeName +
							  // '的答案模板没有编写变量，请检查')
							  return;
						  }
						  var varset = false;
						  if (a.variables && a.variables.length) {
							  for (var k = 0; k < a.variables.length; k++) {
								  var v = a.variables[k]
								  if (v.name == var0) {
									  if (val) {// set
										  v.value = val
									  } else {// remove
										  a.variables.splice(k, 1)
										  k--
									  }
									  varset = true
									  break
								  }
							  }
						  }
						  if (!varset && val) {
							  !a.variables ? a.variables = [] : 0
							  a.variables.push({
								    name : var0,
								    value : val
							    })
						  }
					  }
				  }
			  }
		  })
		if (!valid)
			return;
		$('#ep').block({
			  message : '保存中...',
			  overlayCSS : {
				  backgroundColor : '#EEE'
			  },
			  css : {
				  width : '200px',
				  backgroundColor : '#eee',
				  border : '2px solid #aaa'
			  }
		  });
		// console.log($.toJSON(o))
		$.post("ontology-object!save.action", {
			  data : $.toJSON(o),
			  embedSemanticWordclass : true,
			  sync : true
		  }, function(res, textStatus, jqXHR) {
			  $('#ep').unblock()
			  if (res) {
				  res = $.evalJSON(res)
				  if (res.data) {
					  var data = res.data;
					  editElement(t, data)
					  t.id = data.objectId
					  t.name = data.name
					  elementSaved(t)
					  XIDialog.alert('保存成功')
				  } else {
					  XIDialog.alert('发生错误:' + res.message)
				  }
			  } else {
				  XIDialog.alert('no data')
			  }
		  });
	} else if (t.type == TYPE_CLASS) {
		// 本体类
		var o = $.extend({}, t.attachment)
		var attrs = o.attributes
		var valid = true
		$('#ep_body .epName').each(function(i, field) {
			  field = $(field)
			  var idx = parseInt($(field).attr('idx'))
			  var val = field.val();
			  if (idx == IDX_OBJ_NAME) {
				  if (!val) {
					  XIDialog.alert(field.attr('name') + '未填写')
					  return valid = false;
				  }
				  o.name = val
			  } else if (idx == IDX_OBJ_SB) {
				  if (o.semanticBlock && o.semanticBlock.indexOf('[') != -1) {// 存在原始语义块,不处理
					  ;
				  } else {
					  var wc = o.semanticBlock
					  if (!wc)
						  wc = o.name
					  if (!val)
						  val = o.name
					  var sb = val.split(',')
					  var onamefound = false
					  for (var k = 0; k < sb.length; k++) {
						  if (sb[k] == o.name) {
							  onamefound = true
							  break;
						  }
					  }
					  if (!onamefound) {
						  sb.push(o.name)
					  }
					  o.semanticBlock = wc + '::' + sb.join(',')
				  }
			  }
		  })
		if (o.semanticBlock) {
			o.sb = o.semanticBlock
			delete o.semanticBlock
		}
		if (!valid)
			return;
		$('#ep').block({
			  message : '保存中...',
			  overlayCSS : {
				  backgroundColor : '#EEE'
			  },
			  css : {
				  width : '200px',
				  backgroundColor : '#eee',
				  border : '2px solid #aaa'
			  }
		  });
		$.post("ontology-graph!saveClass.action", {
			  data : $.toJSON(o)
		  }, function(res, textStatus, jqXHR) {
			  $('#ep').unblock()
			  if (res) {
				  res = $.evalJSON(res)
				  if (res.data) {
					  var data = res.data;
					  editElement(t, data)
					  t.id = data.classId
					  t.name = data.name
					  elementSaved(t)
					  XIDialog.alert('保存成功')
				  } else {
					  XIDialog.alert('发生错误:' + res.message)
				  }
			  } else {
				  XIDialog.alert('no data')
			  }
		  });
	}
}
$('#saveBtn').bind('click', saveElementData)