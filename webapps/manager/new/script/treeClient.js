// Define the drag listeners for drag/drop behaviour of nodes.
dragListener = d3.behavior.drag().on("dragstart", function(d) {
	console.log("dragstart:", d.x, d.y, d3.event.x, d3.event.y)
}).on("drag", function(d) {
	if (d.px != d3.event.x || d.py != d3.event.y) {
		d.px = d3.event.x, d.py = d3.event.y;
		d.x = d.py, d.y = d.px;
		update(d);
	}
}).on("dragend", function(d) {
	console.log("dragend:", d.x, d.y, d.px, d.py)
});

var styles = [ {
	r : [ 50, 60 ],
	color : '#ffc81f',
	borderColor : '#ffc81f',
	borderWidth : 5,
	fontColor : '#FFF',
	fontSize : 26
}, {
	r : [ 35, 40 ],
	color : '#8fd422',
	borderColor : '#8fd422',
	borderWidth : 3,
	borderDasharray : '10 5',
	fontColor : '#FFF',
	fontSize : 14
}, {
	r : [ 35, 35 ],
	color : '#FFF',
	borderColor : '#8fd422',
	borderWidth : 3,
	fontColor : '#8fd422',
	fontSize : 12
}, {
	r : [ 35, 35 ],
	color : '#FFF',
	borderColor : '#cbe6ff',
	borderWidth : 3,
	fontColor : '#4fa6f6',
	fontSize : 12
} ];

var m = [ 20, 120, 20, 120 ], w = 1280 - m[1] - m[3], h = 800 - m[0] - m[2], i = 0, root;

var tree = d3.layout.tree().size([ h, w ]);

var diagonal = d3.svg.diagonal().projection(function(d) {
	return [ d.y, d.x ];
});

var vis = d3.select("#body").append("svg:svg").attr("width", w + m[1] + m[3]).attr("height", h + m[0] + m[2]).append("svg:g").attr("transform",
		"translate(" + m[3] + "," + m[0] + ")");

function toggleAll(d) {
	if (d.children) {
		d.children.forEach(toggleAll);
		toggle(d);
	}
}

root = {
	"name" : "flare",
	"level" : 0,
	"children" : [ {
		"name" : "analytics",
		"level" : 1,
		"children" : [ {
			"name" : "cluster",
			"level" : 3,
			"children" : [ {
				"name" : "AgglomerativeCluster",
				"level" : 3
			}, {
				"name" : "CommunityStructure",
				"level" : 3
			}, {
				"name" : "HierarchicalCluster",
				"level" : 3
			}, {
				"name" : "MergeEdge",
				"level" : 3
			} ]
		}, {
			"name" : "graph",
			"level" : 3,
			"children" : [ {
				"name" : "BetweennessCentrality",
				"level" : 3
			}, {
				"name" : "LinkDistance",
				"level" : 3
			}, {
				"name" : "MaxFlowMinCut",
				"level" : 3
			}, {
				"name" : "ShortestPaths",
				"level" : 3
			}, {
				"name" : "SpanningTree",
				"level" : 3
			} ]
		}, {
			"name" : "optimization",
			"level" : 3,
			"children" : [ {
				"name" : "AspectRatioBanker",
				"level" : 3
			} ]
		} ]
	}, {
		"name" : "animate",
		"level" : 1,
		"children" : [ {
			"name" : "Easing",
			"level" : 3
		}, {
			"name" : "FunctionSequence",
			"level" : 3
		}, {
			"name" : "interpolate",
			"level" : 3,
			"children" : [ {
				"name" : "ArrayInterpolator",
				"level" : 3
			}, {
				"name" : "ColorInterpolator",
				"level" : 3
			}, {
				"name" : "DateInterpolator",
				"level" : 3
			}, {
				"name" : "Interpolator",
				"level" : 3
			} ]
		}, {
			"name" : "ISchedulable",
			"level" : 3
		}, {
			"name" : "Parallel",
			"level" : 3
		} ]
	} ]
};

root.x0 = h / 2;
root.y0 = 0;

// Initialize the display to show a few nodes.
// root.children.forEach(toggleAll);

update(root);

var nodes;
function update(source) {
	var duration = d3.event && d3.event.altKey ? 5000 : 500;

	var x = source.x, y = source.y;
	// Compute the new tree layout.
	if (!nodes)
		nodes = tree.nodes(root).reverse();

	// Normalize for fixed-depth.

	// nodes.forEach(function(d) { d.y = d.depth * 180;
	// if(d.name==source.name){console.log(d.name,d.x,source.x,d.y,source.y);d.x=source.x;d.y=source.y;}});

	// Update the nodes…
	var node = vis.selectAll("g.node").data(nodes, function(d) {
		return d.id || (d.id = ++i);
	});

	// Enter any new nodes at the parent's previous position.
	var nodeEnter = node.enter().append("svg:g").attr("class", "node").attr("transform", function(d) {
		return "translate(" + source.y0 + "," + source.x0 + ")";
	}).on("dblclick", function(d) {
		toggle(d);
		update(d);
	});
	/*
	 * nodeEnter.append("svg:circle") .attr("r", 1e-6) .style("fill",
	 * function(d) { return d._children ? "lightsteelblue" : "#fff"; });
	 * 
	 * nodeEnter.append("svg:text") .attr("x", function(d) { return d.children ||
	 * d._children ? -10 : 10; }) .attr("dy", ".35em") .attr("text-anchor",
	 * function(d) { return d.children || d._children ? "end" : "start"; })
	 * .text(function(d) { return d.name; }) .style("fill-opacity", 1e-6);
	 */
	nodeEnter.each(function(d) {
		var newG = d3.select(this);
		newG.append("circle").attr("class", "node").attr("r", function(d) {
			return styles[d.level].r[1];
		}).style("fill", '#FFF').style("stroke", function(d) {
			return styles[d.level].borderColor;
		}).style("stroke-width", function(d) {
			return styles[d.level].borderWidth;
		}).style("stroke-dasharray", function(d) {
			return styles[d.level].borderDasharray || null;
		});
		newG.append("circle").attr("class", "node0").attr("r", function(d) {
			return styles[d.level].r[0];
		}).style("fill", function(d) {
			return styles[d.level].color || '#FFF';
		}).style("cursor", "pointer");
		newG.append("text").attr("dy", 4).attr("text-anchor", "middle").attr('class', 'text').style("fill", function(d) {
			return styles[d.level].fontColor || '#FFF';
		}).text(function(d) {
			return d.name;
		}).style("font-size", function(d) {
			return styles[d.level].fontSize;
		}).style("font-family", "微软雅黑").style("font-weight", "bold").style("cursor", "pointer");
		newG.on("click", function(d0, i0) {
			if (d0.level > 1) {
				vis.selectAll('.node0').filter(function(d) {
					return d.level >= 2;
				}).style("fill", function(d, i) {
					return '#FFF';
				});
				d3.select(this.childNodes[1]).style("fill", function(d) {
					var s = styles[d.level];
					return s.fontColor;
				});
				vis.selectAll('text').filter(function(d) {
					return d.level >= 2;
				}).style("fill", function(d, i) {
					return styles[d.level].fontColor;
				});
				d3.select(this.childNodes[2]).style('fill', function(d) {
					return '#FFF';
				});
				selectedNode = d0;
			}
			// renderEditPanel(d0);
		}).call(dragListener);
	});

	// Transition nodes to their new position.
	var nodeUpdate = node.transition().duration(duration).attr("transform", function(d) {
		return "translate(" + d.y + "," + d.x + ")";
	});

	/*
	 * nodeUpdate.select("circle") .attr("r", 4.5) .style("fill", function(d) {
	 * return d._children ? "lightsteelblue" : "#fff"; });
	 * 
	 * nodeUpdate.select("text") .style("fill-opacity", 1);
	 */

	// Transition exiting nodes to the parent's new position.
	var nodeExit = node.exit().transition().duration(duration).attr("transform", function(d) {
		return "translate(" + source.y + "," + source.x + ")";
	}).remove();

	/*
	 * nodeExit.select("circle") .attr("r", 1e-6);
	 * 
	 * nodeExit.select("text") .style("fill-opacity", 1e-6);
	 */

	// Update the links…
	var link = vis.selectAll("path.link").data(tree.links(nodes), function(d) {
		return d.target.id;
	});

	// Enter any new links at the parent's previous position.
	link.enter().insert("svg:path", "g").attr("class", "link").attr("d", function(d) {
		var o = {
			x : source.x0,
			y : source.y0
		};
		return diagonal({
			source : o,
			target : o
		});
	}).transition().duration(duration).attr("d", diagonal);

	// Transition links to their new position.
	link.transition().duration(duration).attr("d", diagonal);

	// Transition exiting nodes to the parent's new position.
	link.exit().transition().duration(duration).attr("d", function(d) {
		var o = {
			x : source.x,
			y : source.y
		};
		return diagonal({
			source : o,
			target : o
		});
	}).remove();

	// Stash the old positions for transition.
	nodes.forEach(function(d) {
		d.x0 = d.x;
		d.y0 = d.y;
	});
}

// Toggle children.
function toggle(d) {
	if (d.children) {
		d._children = d.children;
		d.children = null;
	} else {
		d.children = d._children;
		d._children = null;
	}
}

$('#addBtn').click(function() {
	if (selectedNode) {
		var node = {
			name : "fred",
			level : 3,
			x : selectedNode.x + 100,
			y : selectedNode.y + 200,
			parent : selectedNode
		};
		for ( var i = 0; i < nodes.length; i++) {
			if (nodes[i].id == selectedNode.id) {
				nodes[i].children.push[node];
				break;
			}
		}
		nodes.push(node);
		update(root);
	}
});
