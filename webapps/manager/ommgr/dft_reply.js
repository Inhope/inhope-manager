Dashboard = {
	u : function() {
		return this._usr ? this._usr : this._usr = new LoginUser('om')
	},
	setAlert : function(msg, status, delay) {
		if (!status)
			status = 'ok'
		if (!delay)
			delay = 3
		if (!this.msgCt) {
			this.msgCt = Ext.DomHelper.insertFirst(document.body, {
				id : 'msg-div'
			}, true);
			this.msgCt.setStyle('position', 'absolute');
			this.msgCt.setStyle('z-index', 9999);
			this.msgCt.setWidth(300);
		}
		this.msgCt.alignTo(document, 't-t');
		var html = [ '<div class="app-msg">', '<div class="x-box-tl"><div class="x-box-tr"><div class="x-box-tc"></div></div></div>',
				'<div class="x-box-ml"><div class="x-box-mr"><div class="x-box-mc"><h3 class="x-icon-text icon-status-' + status + '">', msg,
				'</h3>', '</div></div></div>', '<div class="x-box-bl"><div class="x-box-br"><div class="x-box-bc"></div></div></div>', '</div>' ]
				.join('');
		Ext.DomHelper.append(this.msgCt, {
			'html' : html
		}, true).slideIn('t').pause(delay).ghost("t", {
			remove : true
		});
	},
	navRegistry : [],
	registerNav : function(navClass, sort) {
		this.navRegistry.push({
			'createNav' : function(id) {
				return new navClass({
					'id' : id
				})
			},
			'sort' : sort
		})
	},
	getDaysOfMonth : function(date) {
		if (!date)
			date = new Date();
		date.setMonth(date.getMonth() + 1);
		date.setDate(0);
		return date.getDate();
	},
	createXHR : function() {
		var _xhr = false;
		try {
			_xhr = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				_xhr = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e2) {
				_xhr = false;
			}
		}
		if (!_xhr && window.XMLHttpRequest)
			_xhr = new XMLHttpRequest();
		return _xhr;
	},
	createDimChooser : function(_width) {
		return Ext.create({
			width : _width,
			name : 'dimension',
			xtype : 'dimensionbox',
			clearBtn : true,
			emptyText : '请选择维度',
			menuConfig : {
				labelAlign : 'top'
			},
			valueTransformer : function(value) {
				if (value == 'ALLDIM')
					return '';
				var tags = this.tags;
				var groupTags = {};
				if (tags)
					Ext.each(tags, function(t) {
						groupTags[t.dimId] = t.id;
					});
				return Ext.encode(groupTags);
			}
		});
	},
	navToService : function(servId) {
		var node = Dashboard.navPanel.getRootNode().findChild('module', 'serviceDetail', true);
		node.external = true;
		Dashboard.navPanel.fireEvent('click', node);
		var tab = Dashboard.mainPanel.get('serviceDetailTab');
		tab.items.items[0].externalLoad(servId);
		node.external = false;
	},
	nav : function() {
		var module = arguments[0];
		var node = Dashboard.navPanel.getRootNode().findChild('module', module, true);
		node.external = true;
		Dashboard.navPanel.fireEvent('click', node);
		var tab = Dashboard.mainPanel.get(module + 'Tab');
		Dashboard.mainPanel.activate(tab);
		tab.items.items[0].externalLoad.apply(tab.items.items[0], Array.prototype.slice.call(arguments, 1));
		node.external = false;
	}
};

getTopWindow().OM_Dashboard = getTopWindow().ommgr_Dashboard = Dashboard;

DefaultReplyPanel = function(_cfg) {
	var self = this;
	this.tableName = 'OM_LOG_ASK_DETAIL';
	this.isChangeTable = false;
	this.tagSize = 0;
	this.data = {};
	this.multiCondition;
	this.extraAttrs = {};
	var _pageSize = 20;

	var reader = new Ext.data.JsonReader({
		idProperty : 'id',
		root : 'data',
		fields : [ {
			name : 'userId',
			type : 'string'
		}, {
			name : 'id',
			type : 'string'
		}, {
			name : 'qcontent',
			type : 'string'
		}, {
			name : 'faqName',
			type : 'string'
		}, {
			name : 'acontent',
			type : 'string'
		}, {
			name : 'visitTime',
			type : 'string'
		}, {
			name : 'answerType',
			type : 'string'
		}, {
			name : 'cityName',
			type : 'string'
		}, {
			name : 'questionType',
			type : 'string'
		}, {
			name : 'brand',
			type : 'string'
		}, {
			name : 'channels',
			type : 'string'
		}, {
			name : 'similarity',
			type : 'float'
		}, {
			name : 'moduleId',
			type : 'string'
		}, {
			name : 'platform'
		} ]
	});
	var _store = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({
			url : 'log-ask-detail!listExx.action'
		}),
		reader : reader,
		writer : new Ext.data.JsonWriter()
	});

	var pagingBar = new Ext.PagingToolbarEx({
		store : _store,
		pageSize : _pageSize,
		tableName : this.tableName,
		isChangeTable : this.isChangeTable,
		prependButtons : true,
		tagSize : this.tagSize,
		displayMsg : '第{0}到{1}条记录',
		emptyMsg : "没有记录",
		listeners : {
			change : function() {
				var object = reader.jsonData;
				if (object && object.tableName && object.tableName.length) {
					if (this.tableName != "OM_LOG_ASK_DETAIL" && this.tableName != object.tableName) {
						this.isChangeTable = true;
					} else {
						this.isChangeTable = false;
					}
					if (object.tableName == null || object.tableName == 'null') {
					} else {
						this.tableName = object.tableName;
					}
				}
				if (object && object.tagSize) {
					if (object.tagSize != 0) {
						this.tagSize = object.tagSize;
					}
				}
			}
		}
	});
	pagingBar.setCountHandler(function(btn) {
		btn.setDisabled(true);
		pagingBar.updateInfo('正在计数...');
		Ext.Ajax.request({
			url : 'log-ask-detail!count.action',
			params : Ext.urlEncode(this.getStore().baseParams),
			success : function(response) {
				var result = Ext.util.JSON.decode(response.responseText);
				if (result && result.data != null) {
					pagingBar.updateInfo('共' + result.data + '条记录');
				}
				btn.setDisabled(false);
			},
			failure : function() {
				pagingBar.updateInfo('计数失败');
				btn.setDisabled(false);
			}
		});
	}, this);
	this.pagingBar = pagingBar;

	var _sm = new Ext.grid.CheckboxSelectionModel();
	var userIdT = new Ext.form.TextField({
		fieldLabel : '用户Id',
		name : 'userId',
		xtype : 'textfield',
		allowBlank : true,
		width : 100
	});
	var userQuesT = new Ext.form.TextField({
		fieldLabel : '用户问题',
		name : 'userQues',
		xtype : 'textfield',
		allowBlank : true,
		width : 100
	});
	var manualCombo = new Ext.form.ComboBox({
		fieldLabel : '是否人工回答',
		hiddenName : 'manual',
		valueField : 'id',
		displayField : 'text',
		mode : 'local',
		editable : false,
		typeAhead : true,
		loadMask : true,
		allowBlank : false,
		selectOnFocus : false,
		triggerAction : 'all',
		value : '-1',
		width : 100,
		store : new Ext.data.SimpleStore({
			fields : [ "id", "text" ],
			data : [ [ -1, "全部" ], [ 1, "是" ], [ 2, "否" ] ]
		})
	});
	this.floatReg = /^\d+(\.\d+)?$/;
	var similarityLowT = new Ext.form.TextField({
		name : 'similarity_low',
		xtype : 'textfield',
		allowBlank : true,
		width : 37,
		value : '',
		validator : function(val) {
			val = val.trim();
			if (val) {
				if (self.floatReg.test(val)) {
					return true;
				}
				return false;
			}
			return true;
		}
	});
	this.similarityLowT = similarityLowT;
	var similarityUpT = new Ext.form.TextField({
		name : 'similarity_up',
		xtype : 'textfield',
		allowBlank : true,
		width : 37,
		value : '',
		validator : function(val) {
			val = val.trim();
			if (val) {
				if (self.floatReg.test(val)) {
					return true;
				}
				return false;
			}
			return true;
		}
	});
	this.similarityUpT = similarityUpT;

	var comboStoreData = [ [ '-1', '全部' ], [ '1', '标准回复' ], [ '2', '指令回复' ], [ '-3', '聊天' ], [ '0', '默认回复' ], [ '-2', '其它' ] ];
	var answerTypeCombo = new Ext.form.ComboBox({
		fieldLabel : '回答类型',
		hiddenName : 'answerType',
		valueField : 'id',
		displayField : 'text',
		mode : 'local',
		editable : true,
		typeAhead : true,
		loadMask : true,
		allowBlank : false,
		selectOnFocus : false,
		validator : function(val) {
			val = val.trim();
			for ( var i = 0; i < comboStoreData.length; i++) {
				if (val == comboStoreData[i][1])
					return true;
			}
			if (/^[0-9]+$/.test(val))
				return true;
			return false;
		},
		triggerAction : 'all',
		value : '-1',
		width : 75,
		store : new Ext.data.SimpleStore({
			fields : [ "id", "text" ],
			data : comboStoreData
		})
	});
	var quesTypeComboData = [ [ '-1', '全部' ], [ '1', '全数字' ], [ '2', '全字母' ], [ '4', '字母数字' ], [ '8', '含中文' ] ];
	var questionTypeCombo = new Ext.form.ComboBox({
		fieldLabel : '问题类型',
		hiddenName : 'questionType',
		valueField : 'id',
		displayField : 'text',
		mode : 'local',
		editable : false,
		typeAhead : true,
		loadMask : true,
		allowBlank : false,
		selectOnFocus : false,
		triggerAction : 'all',
		value : '-1',
		width : 75,
		store : new Ext.data.SimpleStore({
			fields : [ "id", "text" ],
			data : quesTypeComboData
		})
	});
	var answerT = new Ext.form.TextField({
		fieldLabel : '答案',
		name : 'question',
		xtype : 'textfield',
		allowBlank : true,
		value : '',
		width : 120
	});
	var moduleT = new Ext.form.TextField({
		fieldLabel : '模块',
		name : 'moduleId',
		xtype : 'textfield',
		allowBlank : true,
		value : '',
		width : 55
	});
	var questionT = new Ext.form.TextField({
		fieldLabel : '问题',
		name : 'questionT',
		xtype : 'textfield',
		allowBlank : true,
		value : '',
		width : 150
	});
	var faqNameT = new Ext.form.TextField({
		fieldLabel : '标准问题',
		name : 'faqNameT',
		xtype : 'textfield',
		allowBlank : true,
		value : '',
		width : 120
	});

	this.questionT = questionT;
	this.userIdT = userIdT;
	this.answerTypeCombo = answerTypeCombo;
	this.questionTypeCombo = questionTypeCombo;
	this.answerT = answerT;
	this.faqNameT = faqNameT;

	var multiCondition = Dashboard.createDimChooser(150);
	this.multiCondition = multiCondition;
	var initDate = new Date();
	initDate.setDate(1);
	initDate.setHours(0, 0, 0, 0);
	var beginTimeField = new ClearableDateTimeField({
		editable : false,
		value : initDate,
		width : 160,
		validator : function(v) {
			if (v) {
				var ev = endTimeField.getValue();
				if (ev && v.getTime() >= ev.getTime())
					return '开始时间不能晚于结束时间';
			}
			return true;
		}
	});
	this.beginTimeField = beginTimeField;
	var endTimeField = new ClearableDateTimeField({
		editable : false,
		width : 160
	});
	this.endTimeField = endTimeField;
	beginTimeField.on('change', function(o, v) {
		if (v) {
			endTimeField.setMinValue(v);
			var max = new Date();
			max.setFullYear(v.getFullYear());
			max.setMonth(v.getMonth() + 1);
			max.setDate(0);
			endTimeField.setMaxValue(max);
		}
	});
	beginTimeField.fireEvent('change', beginTimeField, initDate);
	endTimeField.on('change', function(o, v) {
		beginTimeField.setMaxValue(v);
		var min = new Date();
		min.setFullYear(v.getFullYear());
		min.setMonth(v.getMonth());
		min.setDate(1);
		beginTimeField.setMinValue(min);
	});

	var _columns = [ new Ext.grid.RowNumberer(), {
		header : '用户ID',
		dataIndex : 'userId',
		width : 75
	}, {
		header : 'id',
		dataIndex : 'id',
		hidden : true
	}, {
		header : '用户问题',
		dataIndex : 'qcontent',
		width : 80,
		renderer : function(v) {
			return v.replace(/>/g, '&gt;').replace(/</g, '&lt;');
		}
	}, {
		header : '标准问题',
		dataIndex : 'faqName',
		width : 80,
		hidden : true
	}, {
		header : '问题答案',
		dataIndex : 'acontent',
		hidden : true,
		hidden : true
	}, {
		header : '访问时间',
		dataIndex : 'visitTime',
		width : 80
	}, {
		header : '问题类型',
		dataIndex : 'questionType',
		width : 45
	}, {
		header : '回答类型',
		dataIndex : 'answerType',
		width : 45,
		renderer : function(val) {
			for ( var i = 0; i < comboStoreData.length; i++) {
				if (val == comboStoreData[i][1])
					return val;
			}
			return '其他';
		},
		hidden : true
	}, {
		header : '相似度',
		dataIndex : 'similarity',
		width : 30,
		renderer : function(val) {
			if (!val)
				return 'N/A';
			return val;
		},
		hidden : true
	}, {
		header : '模块',
		dataIndex : 'moduleId',
		width : 30,
		renderer : function(val) {
			if (!val)
				return 'N/A';
			return val;
		},
		hidden : true
	}, {
		header : Dashboard.dimensionNames.platform,
		dataIndex : 'channels',
		width : 45
	} ];
	if (Dashboard.dimensionNames.location)
		_columns.push({
			header : Dashboard.dimensionNames.location,
			dataIndex : 'cityName',
			width : 45,
			renderer : function(val) {
				if (!val || !val.trim())
					return 'N/A';
				return val;
			},
			hidden : true
		});
	if (Dashboard.dimensionNames.brand)
		_columns.push({
			header : Dashboard.dimensionNames.brand,
			dataIndex : 'brand',
			width : 45,
			renderer : function(val) {
				if (!val || !val.trim())
					return 'N/A';
				return val;
			},
			hidden : true
		});

	var cfg = {
		columnLines : true,
		store : _store,
		margins : '0 5 5 5',
		border : false,
		style : 'border-right: 1px solid ' + sys_bdcolor,
		tbar : new Ext.Toolbar({
			enableOverflow : true,
			items : [ '从', beginTimeField, '到', endTimeField, '用户ID:', self.userIdT, ' 用户问题:', userQuesT, ' 问题类型:', self.questionTypeCombo, '维度:',
					multiCondition, {
						text : '搜索',
						iconCls : 'icon-search',
						handler : function() {
							self.search();
						}
					} ]
		}),
		colModel : new Ext.grid.ColumnModel({
			columns : _columns
		}),
		sm : _sm,
		bbar : pagingBar,
		loadMask : true,
		viewConfig : {
			forceFit : true
		}
	};
	DefaultReplyPanel.superclass.constructor.call(this, Ext.applyIf(_cfg || {}, cfg));

	this.getPageSize = function() {
		return _pageSize;
	};

	this.store = _store;
	this.addListener('render', function() {
		this.loadData();
	}, this);

	this.faqNameT = faqNameT;
	this.userIdT = userIdT;
	this.userQuesT = userQuesT;
	this.manualCombo = manualCombo;
	this.answerTypeCombo = answerTypeCombo;
	this.answerT = answerT;
	this.moduleT = moduleT;
	this.questionT = questionT;

	/** -------push start------- */
	var menu = new Ext.menu.Menu({
		items : [ {
			ref : 'pushBtn',
			text : '回复消息',
			iconCls : 'icon-ontology-object',
			width : 50,
			handler : function() {
				_pushForm.platform.setValue(menu.currentRow.get('platform'));
				_pushForm.receiver.setValue(menu.currentRow.get('userId'));
				_pushForm.source.setValue(menu.currentRow.get('id'));
				_pushWin.show();
			}
		}, {
			ref : 'sinkBtn',
			text : '置底',
			iconCls : 'icon-arrow-down',
			width : 50,
			handler : function() {
				self.sinkRecord(menu.currentRow.get('id'), menu.currentRow.get('visitTime'));
			}
		} ]
	});
	this.on('cellcontextmenu', function(grid, rowIndex, cellIndex, e) {
		e.stopEvent();
		this.getSelectionModel().selectRow(rowIndex);
		menu.currentRow = this.getStore().getAt(rowIndex);
		menu.showAt(e.xy);
	}, this);
	var _now = new Date();
	var _pushForm = new Ext.form.FormPanel({
		frame : false,
		border : false,
		labelWidth : 80,
		autoHeight : true,
		waitMsgTarget : true,
		bodyStyle : 'padding:3px 10px;background-color:' + sys_bgcolor,
		items : [ new Ext.form.Hidden({
			ref : 'receiver',
			name : 'receivers'
		}), new Ext.form.Hidden({
			ref : 'source',
			name : 'source'
		}), new Ext.form.Hidden({
			name : 'type',
			value : 'spec'
		}), new Ext.form.Hidden({
			ref : 'platform',
			name : 'platform'
		}), {
			xtype : 'textarea',
			fieldLabel : '消息内容',
			height : 100,
			allowBlank : false,
			name : 'content',
			blankText : '消息内容不能为空',
			anchor : '98%'
		}, {
			xtype : 'xdatetime',
			allowBlank : false,
			fieldLabel : '起始时间',
			dateFormat : 'Y-m-d',
			timeFormat : 'H:i',
			value : _now,
			name : 'begin',
			anchor : '80%'
		}, {
			xtype : 'xdatetime',
			allowBlank : false,
			fieldLabel : '过期时间',
			dateFormat : 'Y-m-d',
			timeFormat : 'H:i',
			value : new Date().setHours(_now.getHours() + 8),
			name : 'expire',
			anchor : '80%'
		} ],
		tbar : [ {
			text : '保存',
			iconCls : 'icon-add',
			handler : function() {
				var btn = this;
				btn.disable();
				var f = _pushWin.editForm.getForm();
				if (!f.isValid()) {
					Ext.Msg.alert('提示', '您尚未正确完整的输入表单内容，请检查！');
					btn.enable();
					return false;
				}
				var vals = f.getFieldValues();
				vals.receivers = [ vals.receivers ];
				Ext.Ajax.request({
					url : 'push-message!save.action',
					params : {
						'data' : Ext.encode(vals)
					},
					success : function(resp) {
						Dashboard.setAlert('保存成功！');
						self.sinkRecord(menu.currentRow.get('id'), menu.currentRow.get('visitTime'));
						_pushWin.hide();
						btn.enable();
					},
					failure : function(resp) {
						Ext.Msg.alert('错误', resp.responseText);
						_pushWin.hide();
						btn.enable();
					}
				});
			}
		} ]
	});
	var _pushWin = new Ext.Window({
		width : 420,
		title : '编辑消息',
		defaults : {
			border : false
		},
		modal : true,
		plain : true,
		shim : true,
		closable : true,
		closeAction : 'hide',
		collapsible : true,
		resizable : false,
		draggable : true,
		animCollapse : true,
		constrainHeader : true,
		shadow : false,
		listeners : {
			'beforehide' : function(p) {
				p.editForm.getForm().reset();
			}
		},
		items : [ _pushForm ]
	});

	_pushWin.editForm = _pushForm;
};

Ext.extend(DefaultReplyPanel, Ext.grid.GridPanel, {
	sinkRecord : function(id, time) {
		var self = this;
		Ext.Ajax.request({
			url : 'log-ask-detail!sinkRecord.action',
			params : {
				'id' : id,
				'time' : time
			},
			success : function(resp) {
				self.getStore().reload();
			},
			failure : function(resp) {
				Ext.Msg.alert('错误', resp.responseText);
			}
		});

	},
	loadData : function() {
		var bt = this.beginTimeField.getValue().format('Y-m-d H:i:s');
		this.store.baseParams.beginTime = bt;
		this.store.baseParams.answerType = 0;
		this.store.baseParams.pushOrder = 'true';
		this.store.load({
			params : {
				beginTime : bt,
				start : 0,
				answerType : 0,
				limit : this.getPageSize(),
				tagsize : 0,
				isnext : true
			}
		});
	},
	resetData : function() {
		this.questionT.setValue('');
		this.answerT.setValue('');
		this.faqNameT.setValue('');
		this.manualCombo.setValue(-1);
		this.startTimeLog.setRawValue('');
		this.userIdT.setValue('');
		this.endTimeLog.setRawValue('');
		this.answerTypeCombo.setValue(-1);
		this.questionTypeCombo.setValue(-1);
		this.cityCombo.setValue('');
		this.brandCombo.setValue('');
		this.channels.setValue('');
	},
	search : function() {
		var beginTime = this.beginTimeField.getValue();
		var endTime = this.endTimeField.getValue();
		if (!beginTime && !endTime) {
			var _bt = new Date();
			_bt.setDate(1);
			_bt.setHours(0, 0, 0, 0);
			this.beginTimeField.setValue(_bt);
			beginTime = _bt;
		}
		if (beginTime && endTime && beginTime.getTime() >= endTime.getTime()) {
			Ext.Msg.alert('提示', '开始时间不能晚于结束时间');
			return false;
		}
		var beginTimeStr = '', endTimeStr = '';
		if (beginTime)
			beginTimeStr = beginTime.format('Y-m-d H:i:s');
		if (endTime)
			endTimeStr = endTime.format('Y-m-d H:i:s');
		var acontent = this.answerT.getValue().trim();
		var manual = this.manualCombo.getValue();// 得到的是Id
		var userId = this.userIdT.getValue().trim();
		var userQues = this.userQuesT.getValue().trim();
		if (userQues || acontent) {
			if (!beginTime || !endTime) {
				Ext.Msg.alert('提示', '根据用户问题或答案查询时，需指定时间跨度(小于7天)');
				return false;
			}
			if (endTime.getDate() - beginTime.getDate() > 7) {
				Ext.Msg.alert('提示', '根据用户问题或答案查询时，时间跨度不可大于7天');
				return false;
			}
		}
		var questionType = this.questionTypeCombo.getValue();
		var multiCondition = this.multiCondition.getValue();
		var store = this.getStore();
		for ( var key in store.baseParams) {
			if (key && key.indexOf('acontent') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('manual') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('userId') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('userQues') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('multiCondition') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('questionType') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('beginTime') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('endTime') != -1) {
				delete store.baseParams[key];
			}
		}
		if (acontent)
			store.setBaseParam('acontent', acontent);
		if (manual)
			store.setBaseParam('manual', manual);
		if (userId)
			store.setBaseParam('userId', userId);
		if (userQues)
			store.setBaseParam('userQues', userQues);
		if (questionType)
			store.setBaseParam('questionType', questionType);
		if (multiCondition)
			store.setBaseParam('multiCondition', multiCondition);
		if (beginTimeStr)
			store.setBaseParam('beginTime', beginTimeStr);
		if (endTimeStr)
			store.setBaseParam('endTime', endTimeStr);
		this.getStore().load({
			params : {
				start : 0,
				limit : this.getPageSize(),
				tablename : this.tableName,
				tagsize : 0,
				isnext : true,
				'acontent' : acontent,
				'manual' : manual,
				'userId' : userId,
				'userQues' : userQues,
				'questionType' : questionType,
				'multiCondition' : multiCondition,
				'beginTime' : beginTimeStr,
				'endTime' : endTimeStr
			}
		});
		this.pagingBar.updateInfo('');
	}
});

Ext.onReady(function() {
	Ext.QuickTips.init();

	var xhr = Dashboard.createXHR();
	xhr.open('GET', 'robot-log!listDimensionNames.action', false);
	xhr.send();
	Dashboard.dimensionNames = Ext.decode(xhr.responseText);

	xhr.open('GET', 'ontology-dimension!listAllowed.action', false);
	xhr.send();
	window._dimChooserInitData = Ext.decode(xhr.responseText).data;

	var p = new DefaultReplyPanel();
	new Ext.Viewport({
		layout : 'fit',
		items : [ p ]
	});
});