<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="res" uri="/WEB-INF/tld/import-res.tld" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="usr" uri="/WEB-INF/tld/login-user.tld"%>
<%@taglib prefix="mdl" uri="/WEB-INF/tld/mdl-ft.tld"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>运维管理</title>
	
	<mdl:dump name="ext.rowexpander.bodystyle" jsExportVar="ext_rowexpander_bodystyle" type="props" unique="true"/>
	<%@include file="/commons/extjsvable.jsp" %>
	<link rel="stylesheet" type="text/css" href="custom.css" />
	<res:import dir="script" recursive="true" include="ux/.*\.js"/>
	<script type="text/javascript" src="dft_reply.js"></script>
</head>

<body>
</body>
</html>