<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="res" uri="/WEB-INF/tld/import-res.tld"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="usr" uri="/WEB-INF/tld/login-user.tld"%>
<%@taglib prefix="mdl" uri="/WEB-INF/tld/mdl-ft.tld"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>运维管理</title>

		<mdl:dump name="ext.rowexpander.bodystyle"
			jsExportVar="ext_rowexpander_bodystyle" type="props" unique="true" />
		<%@include file="/commons/extjsvable.jsp"%>
		<!-- 加载extjs4.2.1 -->
		<script src="<c:url value="/ext4/ext-all-sandbox.js"/>"
			type="text/javascript"></script>
		<link rel="stylesheet" type="text/css"
			href="<c:url value="/ext4/ext-sandbox.css"/>" />
		<!-- 加载extjs4.2.1结束 -->
		<link rel="stylesheet" type="text/css" href="custom.css" />
		<res:import dir="script" recursive="true" include=".*\.js" />
		<usr:checkPerm jsExportVar="om_check_perm_result" permissionNames="om"
			dump="true" />
		<usr:name jsExportVar="om_username" />
	</head>

	<body>
	</body>
</html>