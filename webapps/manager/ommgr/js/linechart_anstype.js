Ext.require([ 'Ext.data.*' ]);
Ext.require('Ext.chart.*');
Ext.require([ 'Ext.layout.container.Fit', 'Ext.window.MessageBox' ]);
Ext.onReady(function() {
	var fields = [ 'chartField', 'typeFinal', 'typeFinalList', 'typeCommonChat', 'typeIllegalWord', 'typeMisinput', 'typeNull' ];

	var data = [];
	var data0 = parent.ommgr.accDataStore.getRange();
	var statByDay = parent.ommgr.statBy == 'day';

	for ( var i = 0; i < data0.length; i++) {
		if (data0[i].data.chartField != '-1')
			data.push(Ext.apply({}, data0[i].data));
	}
	var dataMap = {};
	for ( var i = 0; i < data.length; i++) {
		var d = data[i];
		for ( var key in d) {
			if (key != 'chartField')
				d[key] = parseInt(d[key]);
			else {
				var _chartField = d.chartField;
				if (_chartField.length == 1)
					_chartField = '0' + _chartField;
				else if (statByDay)
					_chartField = _chartField.substring(2);
				dataMap[_chartField] = d;
			}
		}
	}

	var max = parent.ommgr.xAxisMax;
	var data0 = [];
	if (max && data.length < max) {
		for ( var i = 0; i < max; i++) {
			var _chartField = (i + 1) + '';
			if (_chartField.length == 1)
				_chartField = '0' + _chartField;
			var d = dataMap[_chartField];
			var _typeFinal = 0, _typeFinalList = 0, _typeCommonChat = 0, _typeIllegalWord = 0, _typeMisinput = 0, _typeNull = 0;
			if (d) {
				_typeFinal = d.typeFinal;
				_typeFinalList = d.typeFinalList;
				_typeCommonChat = d.typeCommonChat;
				_typeIllegalWord = d.typeIllegalWord;
				_typeMisinput = d.typeMisinput;
				_typeNull = d.typeNull;
			}
			data0.push({
				chartField : _chartField,
				typeFinal : _typeFinal,
				typeFinalList : _typeFinalList,
				typeCommonChat : _typeCommonChat,
				typeIllegalWord : _typeIllegalWord,
				typeMisinput : _typeMisinput,
				typeNull : _typeNull
			});
		}
	} else {
		data0 = data;
	}

	var store1 = Ext.create('Ext.data.JsonStore', {
		fields : fields,
		data : data0
	});

	var chart1 = Ext.create('Ext.chart.Chart', {
		animate : false,
		store : store1,
		insetPadding : 10,
		shadow : false,
		legend : {
			position : 'right'
		},
		axes : [ {
			type : 'Numeric',
			minimum : 0,
			position : 'left',
			fields : [ 'typeFinal', 'typeFinalList', 'typeCommonChat', 'typeIllegalWord', 'typeMisinput', 'typeNull' ],
			title : false,
			grid : true,
			label : {
				renderer : Ext.util.Format.numberRenderer('0,0'),
				font : '10px Arial'
			}
		}, {
			type : 'Category',
			position : 'bottom',
			fields : [ 'chartField' ],
			title : false,
			label : {
				font : '11px Arial',
				renderer : function(name) {
					return name;
				}
			}
		} ],
		series : [ {
			type : 'line',
			axis : 'left',
			xField : 'chartField',
			yField : 'typeFinal',
			title : '标准回复',
			tips : {
				trackMouse : true,
				width : 120,
				height : 23,
				renderer : function(storeItem, item) {
					this.update(storeItem.get('typeFinal') + '个标准回复');
				}
			},
			style : {
				fill : '#20CE20',
				stroke : '#20CE20',
				'stroke-width' : 1.5
			},
			markerConfig : {
				type : 'circle',
				size : 2,
				radius : 2,
				'stroke-width' : 1,
				fill : '#20CE20',
				stroke : '#20CE20'
			}
		}, {
			type : 'line',
			axis : 'left',
			xField : 'chartField',
			yField : 'typeFinalList',
			title : '指令回复',
			tips : {
				trackMouse : true,
				width : 120,
				height : 23,
				renderer : function(storeItem, item) {
					this.update(storeItem.get('typeFinalList') + '个指令回复');
				}
			},
			style : {
				fill : '#00BEEE',
				stroke : '#00BEEE',
				'stroke-width' : 1.5
			},
			markerConfig : {
				type : 'circle',
				size : 2,
				radius : 2,
				'stroke-width' : 1,
				fill : '#00BEEE',
				stroke : '#00BEEE'
			}
		}, {
			type : 'line',
			axis : 'left',
			xField : 'chartField',
			yField : 'typeCommonChat',
			title : '聊天',
			tips : {
				trackMouse : true,
				width : 120,
				height : 23,
				renderer : function(storeItem, item) {
					this.update(storeItem.get('typeCommonChat') + '个聊天');
				}
			},
			style : {
				fill : '#FF9900',
				stroke : '#FF9900',
				'stroke-width' : 1.5
			},
			markerConfig : {
				type : 'circle',
				size : 2,
				radius : 2,
				'stroke-width' : 1,
				fill : '#FF9900',
				stroke : '#FF9900'
			}
		}, {
			type : 'line',
			axis : 'left',
			xField : 'chartField',
			yField : 'typeIllegalWord',
			title : '敏感词',
			tips : {
				trackMouse : true,
				width : 120,
				height : 23,
				renderer : function(storeItem, item) {
					this.update(storeItem.get('typeIllegalWord') + '个敏感词');
				}
			},
			style : {
				fill : '#FF0000',
				stroke : '#FF0000',
				'stroke-width' : 1.5
			},
			markerConfig : {
				type : 'circle',
				size : 2,
				radius : 2,
				'stroke-width' : 1,
				fill : '#FF0000',
				stroke : '#FF0000'
			}
		}, {
			type : 'line',
			axis : 'left',
			xField : 'chartField',
			yField : 'typeMisinput',
			title : '其他',
			tips : {
				trackMouse : true,
				width : 120,
				height : 23,
				renderer : function(storeItem, item) {
					this.update(storeItem.get('typeMisinput') + '个其他');
				}
			},
			style : {
				fill : '#BE00EE',
				stroke : '#BE00EE',
				'stroke-width' : 1.5
			},
			markerConfig : {
				type : 'circle',
				size : 2,
				radius : 2,
				'stroke-width' : 1,
				fill : '#BE00EE',
				stroke : '#BE00EE'
			}
		}, {
			type : 'line',
			axis : 'left',
			xField : 'chartField',
			yField : 'typeNull',
			title : '默认回复',
			tips : {
				trackMouse : true,
				width : 120,
				height : 23,
				renderer : function(storeItem, item) {
					this.update(storeItem.get('typeNull') + '个默认回复');
				}
			},
			style : {
				fill : '#777777',
				stroke : '#777777',
				'stroke-width' : 1.5
			},
			markerConfig : {
				type : 'circle',
				size : 2,
				radius : 2,
				'stroke-width' : 1,
				fill : '#777777',
				stroke : '#777777'
			}
		} ]
	});

	var panel = Ext.create('widget.panel', {
		region : 'center',
		border : 0,
		renderTo : Ext.getBody(),
		layout : 'fit',
		items : chart1
	});

	Ext.create('Ext.container.Viewport', {
		layout : 'border',
		items : panel
	});
});