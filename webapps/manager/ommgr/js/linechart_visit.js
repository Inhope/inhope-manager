Ext.require([ 'Ext.data.*' ]);
Ext.require('Ext.chart.*');
Ext.require([ 'Ext.layout.container.Fit', 'Ext.window.MessageBox' ]);
Ext.onReady(function() {
	var fields = [ 'chartField', 'dynamicTotal', 'totalAcc', 'totalTime' ];

	var data = [];
	var data0 = parent.ommgr.accDataStore.getRange();
	var statByDay = parent.ommgr.statBy == 'day';

	for ( var i = 0; i < data0.length; i++) {
		if (data0[i].data.chartField != '-1')
			data.push(Ext.apply({}, data0[i].data));
	}
	var dataMap = {};
	for ( var i = 0; i < data.length; i++) {
		var d = data[i];
		for ( var key in d) {
			if (key != 'chartField')
				d[key] = parseInt(d[key]);
			else {
				var _chartField = d.chartField;
				if (_chartField.length == 1)
					_chartField = '0' + _chartField;
				else if (statByDay)
					_chartField = _chartField.substring(2);
				dataMap[_chartField] = d;
			}
		}
	}

	var max = parent.ommgr.xAxisMax;
	var data0 = [];
	if (max && data.length < max) {
		for ( var i = 0; i < max; i++) {
			var _chartField = (i + 1) + '';
			if (_chartField.length == 1)
				_chartField = '0' + _chartField;
			var d = dataMap[_chartField];
			var _dynamicTotal = 0, _totalAcc = 0, _totalTime = 0;
			if (d) {
				_dynamicTotal = d.dynamicTotal;
				_totalAcc = d.totalAcc;
				_totalTime = d.totalTime;
			}
			data0.push({
				chartField : _chartField,
				dynamicTotal : _dynamicTotal,
				totalAcc : _totalAcc,
				totalTime : _totalTime
			});
		}
	} else {
		data0 = data;
	}

	var store1 = Ext.create('Ext.data.JsonStore', {
		fields : fields,
		data : data0
	});

	var chart1 = Ext.create('Ext.chart.Chart', {
		animate : false,
		store : store1,
		insetPadding : 10,
		shadow : false,
		legend : {
			position : 'right'
		},
		axes : [ {
			type : 'Numeric',
			minimum : 0,
			position : 'left',
			fields : [ 'dynamicTotal', 'totalAcc', 'totalTime' ],
			title : false,
			grid : true,
			label : {
				renderer : Ext.util.Format.numberRenderer('0,0'),
				font : '10px Arial'
			}
		}, {
			type : 'Category',
			position : 'bottom',
			fields : [ 'chartField' ],
			title : false,
			label : {
				font : '11px Arial',
				renderer : function(name) {
					return name;
				}
			}
		} ],
		series : [ {
			type : 'line',
			axis : 'left',
			xField : 'chartField',
			yField : 'dynamicTotal',
			title : '提问数',
			tips : {
				trackMouse : true,
				width : 130,
				height : 23,
				renderer : function(storeItem, item) {
					this.update(storeItem.get('dynamicTotal') + '句提问');
				}
			},
			style : {
				fill : '#15428B',
				stroke : '#15428B',
				'stroke-width' : 1.5
			},
			markerConfig : {
				type : 'cross',
				size : 3,
				radius : 3,
				'stroke-width' : 1,
				fill : '#15428B',
				stroke : '#15428B'
			}
		}, {
			type : 'line',
			axis : 'left',
			xField : 'chartField',
			yField : 'totalAcc',
			title : '会话数',
			tips : {
				trackMouse : true,
				width : 130,
				height : 23,
				renderer : function(storeItem, item) {
					this.update(storeItem.get('totalAcc') + '个会话');
				}
			},
			style : {
				fill : '#99BBE8',
				stroke : '#99BBE8',
				'stroke-width' : 1.5
			},
			markerConfig : {
				type : 'cross',
				size : 3,
				radius : 3,
				'stroke-width' : 1,
				fill : '#99BBE8',
				stroke : '#99BBE8'
			}
		}, {
			type : 'line',
			axis : 'left',
			xField : 'chartField',
			yField : 'totalTime',
			title : '独立用户数',
			tips : {
				trackMouse : true,
				width : 130,
				height : 23,
				renderer : function(storeItem, item) {
					this.update(storeItem.get('totalTime') + '个独立用户数');
				}
			},
			style : {
				fill : '#EE00EE',
				stroke : '#EE00EE',
				'stroke-width' : 1.5
			},
			markerConfig : {
				type : 'cross',
				size : 3,
				radius : 3,
				'stroke-width' : 1,
				fill : '#EE00EE',
				stroke : '#EE00EE'
			}
		} ]
	});

	var panel = Ext.create('widget.panel', {
		region : 'center',
		border : 0,
		renderTo : Ext.getBody(),
		layout : 'fit',
		items : chart1
	});

	Ext.create('Ext.container.Viewport', {
		layout : 'border',
		items : panel
	});
});