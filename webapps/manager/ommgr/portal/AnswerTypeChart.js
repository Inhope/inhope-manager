Ext.define('Ext.portlets.AnswerTypeChart', {

	extend : 'Ext.panel.Panel',

	requires : [ 'Ext.data.JsonStore', 'Ext.chart.theme.Base', 'Ext.chart.series.Series', 'Ext.chart.series.Line', 'Ext.chart.axis.Numeric' ],

	initComponent : function() {
		var self0 = this;
		var store = Ext.create('Ext.data.JsonStore', {
			fields : [ 'target', 'quesNum' ],
			proxy : {
				type : 'ajax',
				url : 'log-portal!ansType.action',
				reader : {
					type : 'json',
					root : 'data',
					idProperty : 'target'
				}
			},
			autoLoad : true
		});
		store.on('load', function(st, records) {
			self0.ownerCt.setLoading(false);
		});
		this.store = store;
		Ext.apply(this, {
			layout : 'fit',
			height : 308,
			border : false,
			items : {
				xtype : 'chart',
				animate : false,
				store : store,
				shadow : true,
				insetPadding : 22,
				theme : Ext.chart.randomTheme(),
				legend : {
					position : 'right'
				},
				series : [ {
					type : 'pie',
					field : 'quesNum',
					showInLegend : true,
					donut : true,
					tips : {
						trackMouse : true,
						width : 140,
						height : 22,
						renderer : function(storeItem, item) {
							var total = 0;
							store.each(function(rec) {
								total += rec.get('quesNum');
							});
							this.setTitle('总数:' + storeItem.get('quesNum') + ', 占比:' + Math.round(storeItem.get('quesNum') / total * 100) + '%');
						}
					},
					highlight : {
						segment : {
							margin : 20
						}
					},
					label : {
						field : 'target',
						display : 'rotate',
						contrast : true,
						font : '15px Arial',
						renderer : function(v, chart, storeItem) {
							var total = 0;
							store.each(function(rec) {
								total += rec.get('quesNum');
							});
							var percent = Math.round(storeItem.get('quesNum') / total * 100);
							return percent >= 5 ? v : "";
						}
					}
				} ]
			}
		});

		this.callParent(arguments);
	}
});