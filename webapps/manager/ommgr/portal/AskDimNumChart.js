Ext.define('Ext.portlets.AskDimNumChart', {

	extend : 'Ext.panel.Panel',

	requires : [ 'Ext.data.JsonStore', 'Ext.chart.theme.Base', 'Ext.chart.series.Series', 'Ext.chart.series.Line', 'Ext.chart.axis.Numeric' ],

	initComponent : function() {
		var self0 = this;
		var store = Ext.create('Ext.data.JsonStore', {
			fields : [ 'target', 'quesNum' ],
			proxy : {
				type : 'ajax',
				url : 'log-portal!askDim.action',
				reader : {
					type : 'json',
					root : 'data',
					idProperty : 'target'
				}
			},
			autoLoad : true
		});
		store.on('load', function(st, records) {
			self0.ownerCt.setLoading(false);
		});
		this.store = store;
		Ext.apply(this, {
			layout : 'fit',
			height : 308,
			border : false,
			items : {
				xtype : 'chart',
				animate : false,
				store : store,
				insetPadding : 15,
				theme : Ext.chart.randomTheme(),
				axes : [ {
					type : 'Numeric',
					position : 'left',
					fields : [ 'quesNum' ],
					label : {
						renderer : Ext.util.Format.numberRenderer('0,0')
					},
					grid : true,
					minimum : 0
				}, {
					type : 'Category',
					position : 'bottom',
					fields : [ 'target' ]
				} ],
				series : [ {
					type : 'column',
					axis : 'left',
					highlight : true,
					tips : {
						trackMouse : true,
						width : 140,
						height : 22,
						renderer : function(storeItem, item) {
							this.setTitle(storeItem.get('quesNum') + '句提问');
						}
					},
					xField : 'target',
					yField : 'quesNum'
				} ]
			}
		});

		this.callParent(arguments);
	}
});