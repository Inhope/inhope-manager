Ext.define('Ext.portlets.AskNumChart', {

	extend : 'Ext.panel.Panel',

	requires : [ 'Ext.data.JsonStore', 'Ext.chart.theme.Base', 'Ext.chart.series.Series', 'Ext.chart.series.Line', 'Ext.chart.axis.Numeric' ],

	initComponent : function() {
		var self0 = this;
		var store = Ext.create('Ext.data.JsonStore', {
			fields : [ 'countItem', 'totalAcc', 'totalQue', 'totalTime' ],
			proxy : {
				type : 'ajax',
				url : 'log-portal!ask.action',
				reader : {
					type : 'json',
					root : 'data',
					idProperty : 'countItem'
				}
			},
			autoLoad : true
		});
		store.on('load', function(st, records) {
			if (records && records.length) {
				for ( var i = 0; i < records.length; i++) {
					for ( var key in records[i].data) {
						if ('id countItem'.indexOf(key) == -1)
							records[i].data[key] = parseInt(records[i].data[key]);
					}
				}
			}
			st.loadData(records);
			self0.ownerCt.setLoading(false);
		});
		this.store = store;

		Ext.apply(this, {
			layout : 'fit',
			height : 308,
			border : false,
			closable : false,
			items : {
				xtype : 'chart',
				animate : false,
				store : store,
				insetPadding : 15,
				legend : {
					position : 'bottom'
				},
				axes : [ {
					type : 'Numeric',
					minimum : 0,
					majorTickSteps : 1,
					position : 'left',
					fields : [ 'totalQue' ],
					title : false,
					grid : true,
					position : 'left'
				// title : '数量',
				// labelTitle : {
				// font : '9px Arial'
				// }
				}, {
					type : 'Category',
					position : 'bottom',
					fields : [ 'countItem' ],
					title : false,
					label : {
						font : '11px Arial'
					},
					position : 'bottom'
				// title : '统计日期',
				// labelTitle : {
				// font : '9px Arial'
				// }
				} ],
				series : [ {
					type : 'line',
					axis : 'left',
					xField : 'countItem',
					yField : 'totalQue',
					title : '提问数',
					tips : {
						trackMouse : true,
						width : 120,
						height : 22,
						renderer : function(storeItem, item) {
							this.setTitle(storeItem.get('totalQue') + '句提问');
						}
					},
					style : {
						fill : '#15428B',
						stroke : '#15428B',
						'stroke-width' : 2
					},
					markerConfig : {
						type : 'circle',
						size : 3,
						radius : 3,
						'stroke-width' : 0,
						fill : '#15428B',
						stroke : '#15428B'
					}
				}, {
					type : 'line',
					axis : 'left',
					xField : 'countItem',
					yField : 'totalAcc',
					title : '会话数',
					tips : {
						trackMouse : true,
						width : 120,
						height : 22,
						renderer : function(storeItem, item) {
							this.setTitle(storeItem.get('totalAcc') + '个会话数');
						}
					},
					style : {
						fill : '#99BBE8',
						stroke : '#99BBE8',
						'stroke-width' : 2
					},
					markerConfig : {
						type : 'circle',
						size : 3,
						radius : 3,
						'stroke-width' : 0,
						fill : '#99BBE8',
						stroke : '#99BBE8'
					}
				}, {
					type : 'line',
					axis : 'left',
					xField : 'countItem',
					yField : 'totalTime',
					title : '独立用户数',
					tips : {
						trackMouse : true,
						width : 130,
						height : 22,
						renderer : function(storeItem, item) {
							this.setTitle(storeItem.get('totalTime') + '个独立用户数');
						}
					},
					style : {
						fill : '#EE00EE',
						stroke : '#EE00EE',
						'stroke-width' : 2
					},
					markerConfig : {
						type : 'circle',
						size : 3,
						radius : 3,
						'stroke-width' : 0,
						fill : '#EE00EE',
						stroke : '#EE00EE'
					}
				} ]
			}
		});

		this.callParent(arguments);

	}
});