Ext.define('Ext.portlets.ServiceNumChart', {

	extend : 'Ext.panel.Panel',

	requires : [ 'Ext.data.JsonStore', 'Ext.chart.theme.Base', 'Ext.chart.series.Series', 'Ext.chart.series.Line', 'Ext.chart.axis.Numeric' ],

	initComponent : function() {
		var self0 = this;
		var store = Ext.create('Ext.data.JsonStore', {
			fields : [ 'target', 'quesNum' ],
			proxy : {
				type : 'ajax',
				url : 'log-portal!service.action',
				reader : {
					type : 'json',
					root : 'data',
					idProperty : 'target'
				}
			},
			autoLoad : true
		});
		store.on('load', function(st, records) {
			self0.ownerCt.setLoading(false);
		});
		this.store = store;
		Ext.apply(this, {
			layout : 'fit',
			height : 308,
			border : false,
			items : {
				xtype : 'chart',
				animate : true,
				style : 'background:#fff',
				shadow : false,
				store : store,
				axes : [ {
					type : 'Numeric',
					position : 'bottom',
					fields : [ 'quesNum' ],
					label : {
						renderer : Ext.util.Format.numberRenderer('0,0')
					},
					minimum : 0
				}, {
					type : 'Category',
					position : 'left',
					fields : [ 'target' ],
				} ],
				series : [ {
					type : 'bar',
					axis : 'bottom',
					label : {
						display : 'insideEnd',
						field : 'quesNum',
						renderer : Ext.util.Format.numberRenderer('0'),
						orientation : 'horizontal',
						color : '#333',
						'text-anchor' : 'right',
						contrast : true
					},
					xField : 'target',
					yField : [ 'quesNum' ],
					renderer : function(sprite, record, attr, index, store) {
						var value = (record.get('quesNum')) % 5;
						var color = [ 'rgb(213, 70, 121)', 'rgb(44, 153, 201)', 'rgb(146, 6, 157)', 'rgb(49, 149, 0)', 'rgb(249, 153, 0)' ][value];
						return Ext.apply(attr, {
							fill : color
						});
					}
				} ]
			}
		});

		this.callParent(arguments);
	}
});