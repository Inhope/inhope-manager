Ext.define('Ext.portlets.TopFaqPanel', {

	extend : 'Ext.grid.Panel',

	requires : [ 'Ext.data.JsonStore' ],

	initComponent : function() {
		var self0 = this;
		var store = Ext.create('Ext.data.JsonStore', {
			fields : [ 'target', 'quesNum' ],
			proxy : {
				type : 'ajax',
				url : 'log-portal!topFaq.action',
				reader : {
					type : 'json',
					root : 'data',
					idProperty : 'target'
				}
			},
			autoLoad : true
		});
		store.on('load', function(st, records) {
			self0.ownerCt.setLoading(false);
		});
		this.store = store;
		Ext.apply(this, {
			store : store,
			columnLines : true,
			height : 308,
			viewConfig : {
				stripeRows : false,
				getRowClass : function(record) {
					return record.index % 2 == 1 ? 'grid-row-alt-custom' : '';
				},
				loadMask : false
			},
			columns : [ {
				text : '问题',
				flex : 1,
				sortable : true,
				dataIndex : 'target',
				renderer : function(v) {
					return v.replace(/>/g, '&gt;').replace(/</g, '&lt;');
				}
			}, {
				text : '提问次数',
				width : 75,
				sortable : true,
				dataIndex : 'quesNum',
				renderer : function(v) {
					return '<span style="color:green;">' + v + '</span>';
				}
			} ]
		});
		this.callParent(arguments);
		this.setLoading(false);
	}
});