/**
 * @class Ext.app.Portal
 * @extends Object A sample portal layout application class.
 */
Ext.tip.QuickTipManager.init();
Ext.Loader.setPath('Ext.portlets', '.');
Ext.Loader.setPath('Ext.app', '.');
Ext.chart.randomTheme = function() {
	var arr = [ 'Base', 'Green', 'Sky', 'Red', 'Purple', 'Blue', 'Yellow', 'Category1', 'Category2', 'Category3', 'Category4', 'Category5',
			'Category6' ];
	return arr[Math.floor(Math.random() * arr.length)];
};

Ext.define('Ext.app.Portal', {

	extend : 'Ext.container.Viewport',

	initComponent : function() {

		var getTools = function() {
			return [ {
				style : {
					marginRight : '2px'
				},
				xtype : 'tool',
				tooltip : '切换周/今日视图',
				type : 'gear',
				handler : function(e, target, header, tool) {
					var ownerCt = header.ownerCt;
					var contentPanel = ownerCt.items.items[0];
					if (contentPanel.isLoading)
						return false;
					var title = ownerCt.title;
					var isWeekToogle = ownerCt.isWeekToogle;
					var isWeekly = title.indexOf('周') > 0;
					if (!contentPanel.store)
						return false;
					ownerCt.setLoading('正在加载数据...');
					contentPanel.store.load({
						params : {
							sdate: Ext.Date.format(Ext.getCmp('sdate').getValue(),'Ymd'),
							edate: Ext.Date.format(Ext.getCmp('edate').getValue(),'Ymd'),
							type : isWeekly ? 'daily' : 'weekly'
						},
						callback : function() {
							if(isWeekToogle)
								ownerCt.setTitle(title.replace(isWeekly ? '周' : '今日', isWeekly ? '今日' : '周'));
						}
					});
				}
			} ];
		};
		var getListeners = function() {
			return {
				'render' : function() {
					this.setLoading('正在加载数据...');
				}
			};
		};

		Ext.apply(this, {
			layout : {
				type : 'border',
				padding : '0',
				border : false
			},
			items : [{
				xtype : 'container',
				region : 'center',
				layout : 'border',
				items : [ {
					xtype : 'portalpanel',
					region : 'center',
					bodyStyle : 'border-width:1px 0 0 0; solid #8db2e3',
					tbar : new Ext.Toolbar({
						items : [{
				            xtype: 'datefield',
				            id: 'sdate',
				            width: 220,
				            fieldLabel: '开始时间',
				            labelWidth: 60,
				            labelAlign: 'right',
				            editable:false, 
				            format: 'Y-m-d',
				            maxValue: Ext.Date.add(new Date(), Ext.Date.DAY, -1),
				            value: Ext.Date.add(new Date(), Ext.Date.DAY, -7),
				            listeners: {
				                'select': function () {
				                    var start = Ext.getCmp('sdate').getValue();
				                    Ext.getCmp('edate').setMinValue(start);
				                    var endDate = Ext.Date.add(start, Ext.Date.DAY, 6);
				                    Ext.getCmp('edate').setMaxValue(endDate);
				                    Ext.getCmp('edate').setValue(endDate);
				                }
				            }
				        }, {
				            xtype: 'datefield',
				            id: 'edate',
				            width: 175,
				            labelWidth: 60,
				            fieldLabel: '结束时间',
				            labelAlign: 'right',
				            editable:false, 
				            format: 'Y-m-d',
				            maxValue: Ext.Date.add(new Date(), Ext.Date.DAY, -1),
				            value: Ext.Date.add(new Date(), Ext.Date.DAY, -1),
				            listeners: {
				                'select': function () {
				                	var start = Ext.getCmp('sdate').getValue();
				                	var endDate = Ext.getCmp('edate').getValue(); 
				                	if(endDate < start)
				                		start = endDate;
				                	Ext.getCmp('sdate').setValue(start);
				                }
				            }
				        }, {
							text : '搜索',
							xtype : 'button',
							iconCls : 'icon-search',
							handler : function() {
								for(var i = 1 ; i <= 3; i++){
									console.info(i)
									var ownerCt = Ext.getCmp('portalline' + i);
									var items = ownerCt.items.items;
									for(var j = 0 ; j < items.length ; j++){
										var contentPanel = items[j];
										if (items[j].items.items[0].isLoading)
											continue;
										var title = contentPanel.title;
										var isWeekly = title.indexOf('周') > 0;
										if (!contentPanel.items.items[0].store)
											continue;
										contentPanel.setLoading('正在加载');
										contentPanel.items.items[0].store.load({
											params : {
												sdate: Ext.Date.format(Ext.getCmp('sdate').getValue(),'Ymd'),
												edate: Ext.Date.format(Ext.getCmp('edate').getValue(),'Ymd'),
												type : isWeekly ? 'weekly' : 'daily'
											},
											callback : function() {
												contentPanel.setLoading(false);
											}
										});
									}
																				}
							}
						}]
					}),
					items : [ {
						id : 'portalline1',
						items : [ {
							title : '访问数据统计 - 时间 - 周',
							tools : getTools(),
							border : false,
							closable : false,
							items : Ext.create('Ext.portlets.AskNumChart'),
							listeners : getListeners(),
							isWeekToogle : true
						}, {
							title : '回答类型统计 - 周',
							tools : getTools(),
							border : false,
							closable : false,
							items : Ext.create('Ext.portlets.AnswerTypeChart')
						}, {
							title : '未匹配问题Top20 - 周',
							tools : getTools(),
							border : false,
							closable : false,
							items : Ext.create('Ext.portlets.UnresolvedPanel'),
							isWeekToogle : true
						} ]
					}, {
						id : 'portalline2',
						items : [ {
							title : '访问数据统计 - 维度 - 周',
							tools : getTools(),
							border : false,
							closable : false,
							items : Ext.create('Ext.portlets.AskDimNumChart')
						}, {
							title : '问题类型统计 - 周',
							tools : getTools(),
							border : false,
							closable : false,
							items : Ext.create('Ext.portlets.QuestionTypeChart')
						} ]
					}, {
						id : 'portalline3',
						items : [ {
							title : '服务交互Top10 - 周',
							tools : getTools(),
							border : false,
							closable : false,
							items : Ext.create('Ext.portlets.ServiceNumChart')
						}, {
							title : '热点问题Top20 - 周',
							tools : getTools(),
							border : false,
							closable : false,
							items : Ext.create('Ext.portlets.TopFaqPanel')
						} ]
					} ]
				} ]
			} ]
		});
		this.callParent(arguments);
	},

	onPortletClose : function(portlet) {
		this.showMsg('"' + portlet.title + '" was removed');
	},

	showMsg : function(msg) {
		var el = Ext.get('app-msg'), msgId = Ext.id();

		this.msgId = msgId;
		el.update(msg).show();

		Ext.defer(this.clearMsg, 3000, this, [ msgId ]);
	},

	clearMsg : function(msgId) {
		if (msgId === this.msgId) {
			Ext.get('app-msg').hide();
		}
	}
});
Ext.require([ 'Ext.layout.container.*', 'Ext.resizer.Splitter', 'Ext.fx.target.Element', 'Ext.fx.target.Component', 'Ext.window.Window',
		'Ext.app.Portlet', 'Ext.app.PortalColumn', 'Ext.app.PortalPanel', 'Ext.app.Portlet', 'Ext.app.PortalDropZone' ]);

Ext.onReady(function() {
	Ext.getBody().addCls(Ext.baseCSSPrefix + 'theme-neptune');
	Ext.create('Ext.app.Portal');
});