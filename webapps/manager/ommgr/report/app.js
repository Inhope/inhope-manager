Ext.onReady(function() {

			Ext.QuickTips.init();
			
			Ext.Ajax.defaultPostHeader += ";charset=utf-8"; 

			Ext.Loader.setConfig({
						enabled : true
					});

			Ext.application({
						name : 'XI',
						appFolder : 'app',
						launch : function() {
							Ext.create('Ext.container.Viewport', {
										layout : 'fit',
										items : {
											border : false,
											xtype : '_mainlayoutview'
										}
									});
						},
						controllers : ['main.MainController',
								'base.BaseController', 'group.GroupController',
								'define.DefineController']
					});

		})