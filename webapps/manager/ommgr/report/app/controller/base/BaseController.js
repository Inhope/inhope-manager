Ext.define('XI.controller.base.BaseController', {
			init : function() {
				this.control({
							'_basewestview' : {
								checkchange : function(node, checked, options) {
									var reportUtil = Ext.create("XI.util.ExtUtil");
									var mixHead_ = Ext.getCmp('mixHead_');
									var rootMixNode = mixHead_.getStore().getRootNode();
									if (node.data.leaf == false) {
										if (checked) {
											node.expand();
											node.eachChild(function(n) {
												if(!n.data.checked) {
													reportUtil.doSetTreeNode(n, rootMixNode);
												}
												n.set('checked', true);
											});
										} else {
											node.expand();
											node.eachChild(function(n) {
												n.set('checked', false);
												reportUtil.doRemoveTreeNode(rootMixNode, reportUtil.getFullNodeText(n));
												reportUtil.doRemoveTreeNode(rootMixNode, reportUtil.getFullNodeText(n));
											});
										}
									} else {
										if(!checked) {
											node.parentNode.set('checked', false);
											reportUtil.doRemoveTreeNode(rootMixNode, reportUtil.getFullNodeText(node));
											reportUtil.doRemoveTreeNode(rootMixNode, reportUtil.getFullNodeText(node));
										} else {
											reportUtil.doSetTreeNode(node, rootMixNode);
										}
									}
								}
							}
						});
			},
			extend : 'Ext.app.Controller',
			views : ['base.BaseLayoutView', 'base.BaseWestView',
					'base.BaseFromView', 'base.BaseFormJoinView',
					'base.BaseGridJoinView', 'base.BaseGridWhereView',
					'base.BaseFormWhereView','base.BaseFormWhereFieldCondView'],
			stores : ['base.BaseWestStore', 'base.BaseFromComboStore',
					'base.BaseGridJoinStore', 'base.BaseGridWhereStore'],
			models : ['base.BaseWestModel', 'base.BaseGridJoinModel',
					'base.BaseGridWhereModel']
		})