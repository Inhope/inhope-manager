Ext.define('XI.controller.group.GroupController', {
			init : function() {
				this.control({});
			},
			extend : 'Ext.app.Controller',
			views : ['group.GroupLayoutView', 'group.GroupByGridView',
					'group.GroupByFormView', 'group.GroupHavingGridView',
					'group.GroupHavingFormView',
					'group.GroupAggregateGridView',
					'group.GroupAggregateFormView', 'group.GroupHeadTreeView',
					'group.GroupHeadFormView', 'group.GroupHavingNoneFormView',
					'group.GroupChartFormView'],
			stores : ['group.GroupByGridStore', 'group.GroupHavingGridStore',
					'group.GroupAppregateGridStore',
					'group.GroupHeadTreeStore', 'group.GroupChartComboStore'],
			models : ['group.GroupByGridModel', 'group.GroupHavingGridModel',
					'group.GroupAppregateGridModel', 'group.GroupHeadTreeModel']
		})