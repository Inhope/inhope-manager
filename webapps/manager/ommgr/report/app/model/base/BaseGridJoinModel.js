Ext.define('XI.model.base.BaseGridJoinModel', {
	extend: 'Ext.data.Model',
    fields:[
		{name:'left',type:'string'},
		{name:'right',type:'string'},
		{name:'condition',type:'string'}
	]
});