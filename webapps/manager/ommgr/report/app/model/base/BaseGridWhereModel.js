Ext.define('XI.model.base.BaseGridWhereModel', {
			extend : 'Ext.data.Model',
			fields : [{
						name : 'whereOrAnd',
						type : 'string'
					}, {
						name : 'whereField',
						type : 'string'
					}, {
						name : 'whereCond',
						type : 'string'
					}, {
						name : 'whereValue',
						type : 'string'
					}]
		});