Ext.define("XI.model.base.BaseWestModel", {
			extend : 'Ext.data.Model',
			fields : [{
						name : 'text',
						type : 'string',
						sortable : true
					}, {
						name : 'comments',
						type : 'string',
						sortable : true
					}, {
						name : 'numb',
						type : 'string',
						sortable : true
					}]
		});