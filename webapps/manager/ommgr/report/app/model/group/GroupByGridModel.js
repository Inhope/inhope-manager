Ext.define('XI.model.group.GroupByGridModel', {
			extend : 'Ext.data.Model',
			fields : [{
						name : 'groupby',
						type : 'string'
					}, {
						name : 'groupbyvalue',
						type : 'string'
					}]
		});