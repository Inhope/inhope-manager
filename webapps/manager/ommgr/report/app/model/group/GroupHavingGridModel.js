Ext.define('XI.model.group.GroupHavingGridModel', {
			extend : 'Ext.data.Model',
			fields : [{
						name : 'havingOrAnd',
						type : 'string'
					},{
						name : 'having',
						type : 'string'
					}, {
						name : 'havingFunction',
						type : 'string'
					}, {
						name : 'havingField',
						type : 'string'
					}, {
						name : 'havingCond',
						type : 'string'
					}, {
						name : 'havingValue',
						type : 'string'
					}]
		});