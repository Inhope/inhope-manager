Ext.define('XI.store.base.BaseFromComboStore', {
	extend : 'Ext.data.Store',
    fields: ['name', 'value'],
    data : []
});