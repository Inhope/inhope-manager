Ext.define('XI.store.base.BaseWestStore', {
	extend : 'Ext.data.TreeStore',
	model : 'XI.model.base.BaseWestModel',
	proxy : {
		type : 'ajax',
		url : 'report!fields.action',
		reader : 'json'
	},
	root : {
	    expanded : false
    },
    autoLoad : false
})