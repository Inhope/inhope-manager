Ext.define('XI.store.group.GroupChartComboStore', {
	extend : 'Ext.data.Store',
    fields: ['name', 'value'],
    data : []
});