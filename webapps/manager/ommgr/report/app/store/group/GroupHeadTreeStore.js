Ext.define("XI.store.group.GroupHeadTreeStore", {
			extend : 'Ext.data.TreeStore',
			model : 'XI.model.group.GroupHeadTreeModel',
			root : {
				text : '报表预显示字段',
				expanded : true,
				children : []
			}
		});