Ext.define('XI.store.main.MainWestStore', {
	extend : 'Ext.data.TreeStore',
	proxy : {
		type : 'ajax',
		url : 'report!tables.action',
		reader : 'json'
	},
	root : {
	    expanded : true
    },
    autoLoad : false
})