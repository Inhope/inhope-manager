Ext.define('XI.util.ExtUtil', {

	/**
	 * 
	 * @param {}
	 *            node
	 * @param {}
	 *            rootNode
	 */
	doSetTreeNode : function(node, rootNode) {
		var tempObj = {
			text : node.parentNode.data.text + '.' + node.data.text,
			id : Ext.id(),
			comments : node.data.comments,
			checked : null,
			numb : node.data.numb,
			appear : true,
			leaf : true
		};
		rootNode.appendChild(tempObj);
	},

	doGridToSetTreeNode : function(data, rootNode) {
		var tempObj = {
			text : data.appearAggregate + '(' + data.appearAggregateFiled + ')',
			id : Ext.id(),
			comments : data.appearAggregateComments,
			checked : null,
			numb : 'true',
			appear : true,
			leaf : true
		};
		rootNode.appendChild(tempObj);
	},

	/**
	 * 
	 * @param {}
	 *            node
	 * @param {}
	 *            text
	 */
	doRemoveTreeNode : function(node, text) {
		function iterationChildNode(node, text) {
			var resultNode = node.findChild('text', text);
			if (resultNode) {
				resultNode.remove();
			}
			node.eachChild(function(childNode) {
						var resultNodeChild = childNode.findChild('text', text);
						if (resultNodeChild) {
							resultNodeChild.remove();
							return;
						} else {
							iterationChildNode(childNode, text);
						}
					})
		};
		iterationChildNode(node, text);
	},
	/**
	 * 
	 * @param {}
	 *            node
	 * @return {}
	 */
	getFullNodeText : function(node) {
		return node.parentNode.data.text + '.' + node.data.text;
	},

	validateMixHead : function(node) {
		var res = true;
		function validNotLeaf(node) {
			node.eachChild(function(childNode) {
						if (!childNode.isLeaf() && !childNode.hasChildNodes()) {
							res = false;
							return;
						} else {
							validNotLeaf(childNode);
						}
					})
		};
		validNotLeaf(node);
		return res;
	},

	doBuildMixJson : function(rootNode) {
		var nodeArr = [];
		function buildNodeArr(rootNode) {
			rootNode.eachChild(function(childNode) {
						var nodeObj = {
							text : childNode.data.text,
							id : childNode.data.id,
							pid : childNode.parentNode.data.id,
							appear : childNode.data.appear,
							numb : childNode.data.numb,
							comments : childNode.data.comments
						}
						nodeArr.push(nodeObj);
						if (!childNode.isLeaf()) {
							buildNodeArr(childNode);
						}
					});
		};
		buildNodeArr(rootNode);
		return nodeArr;
	},

	/**
	 * 判断是否合并表头是否有显示的字段，并且是叶子节点
	 * 
	 * @param {}
	 *            rootNode
	 * @return {}
	 */
	hasLeafChildNode : function(rootNode) {
		var res = true;
		if (rootNode.hasChildNodes()) {
			function iterateChild(rootNode) {
				rootNode.eachChild(function(childNode) {
							if (childNode.isLeaf()) {
								res = false;
								return false;
							} else {
								iterateChild(childNode);
							}
						});
			};
			iterateChild(rootNode);
		}
		return res;
	},

	/**
	 * 变更选择表后，清空所有的组件数据
	 */
	clearPanelData : function() {
		var grouphavinggridview_ = Ext.getCmp('grouphavinggridview_');
		var mixHead_ = Ext.getCmp('mixHead_');
		var maintable_ = Ext.getCmp('maintable_');
		var chartform_ = Ext.getCmp('groupchartformview_');
		var basegridwhereview_ = Ext.getCmp('basegridwhereview_');
		var groupappregategridview_ = Ext.getCmp('groupappregategridview_');
		var basegridjoinview_ = Ext.getCmp('basegridjoinview_');
		var groupbygridview_ = Ext.getCmp('groupbygridview_');
		grouphavinggridview_.getStore().removeAll();
		basegridwhereview_.getStore().removeAll();
		basegridjoinview_.getStore().removeAll();
		groupbygridview_.getStore().removeAll();
		maintable_.clearValue();
		chartform_.getForm().reset();
		groupappregategridview_.getStore().removeAll();
		mixHead_.getStore().getRootNode().removeAll();
	},

	/**
	 * 设置basewestview_的treenode节点的是否有启用选择框
	 * 
	 * @param {}
	 *            node
	 * @param {}
	 *            text
	 * @param {}
	 *            state
	 */
	setTreeNodeState : function(node, text, state) {
		function iterateState(node, text, state) {
			node.eachChild(function(childNode) {
						if (node.data.text + '.' + childNode.data.text == text) {
							childNode.set('checked', state);
						} else {
							iterateState(childNode, text, state);
						}
					});
		};
		iterateState(node, text, state);
	},

	/**
	 * 设置basewestview_的treenode节点的启用全部选择框
	 * 
	 * @param {}
	 *            node
	 * @param {}
	 *            state
	 */
	setTreeNodeDefaultFalse : function(node) {
		function iterateSetState(node, state) {
			node.eachChild(function(childNode) {
						childNode.set('checked', state);
						iterateSetState(childNode, state);
					});
		};
		iterateSetState(node, false);
	},

	/**
	 * 截取字符串
	 * 
	 * @param {}
	 *            dataStr
	 * @return {}
	 */
	dataSubstr : function(dataStr) {
		return dataStr.substr(dataStr.indexOf('.') + 1, dataStr.length);
	}

})