Ext.define('XI.view.ComboboxTree', {
	extend : "Ext.form.field.Picker",
	alias : 'widget._comboboxtree',
	emptyText : '请选择字段',
	editable : false,
	extend : "Ext.form.field.Picker",
	requires : ["Ext.tree.Panel"],
	_idValue : '',
	_txtValue : '',
	initComponent : function() {
		this.callParent();
		var self = this;
		self.store = Ext.create('Ext.data.TreeStore', {
					proxy : {
						type : 'ajax',
						url : 'report!fields.action',
						reader : 'json'
					},
					root : {
						expanded : false
					},
					autoLoad : false
				});
		self.picker = new Ext.tree.Panel({
					id : Ext.id(),
					height : self.treeHeight || 300,
					resizable : true,
					minWidth : 300,
					maxWidth : 500,
					minHeight : 200,
					maxHeight : 500,
					width : 400,
					autoScroll : true,
					floating : true,
					focusOnToFront : false,
					shadow : true,
					ownerCt : this.ownerCt,
					useArrows : self.useArrows || true,
					store : self.store,
					rootVisible : false,
					listeners : {
						load : function() {
							this.expandAll();
						}
					}
				});
		self.picker.on({
			'itemclick' : function(view, rec) {
				if (rec
						&& ((self.selectMode == 'leaf' && rec.isLeaf() == true) || self.selectMode == 'all')) {
					// self.setRawValue(rec.get('id'));// 隐藏值
					self._idValue = rec.get('id');
					var pnode = rec.parentNode;
					self.setValue(self._txtValue = pnode.data.text + '.'
							+ rec.get('text'));// 显示值
					self.collapse();
					self.fireEvent('select', self, rec);
				};
			}
		});
	},
	getValue : function() {
		return this._idValue;
	},
	getTextValue : function() {
		return this._txtValue;
	},
	reLoad : function(id, url) {
		var store = this.picker.getStore();
		var root = this.picker.getRootNode();
		store.proxy.url = url;
		root.set('id', id);
		store.load();
	},
	alignPicker : function() {
		var store = this.picker.getStore();
		this.loadParam(store);
		var me = this, picker, isAbove, aboveSfx = '-above';
		if (this.isExpanded) {
			picker = me.getPicker();
			if (me.matchFieldWidth) {
				picker.setWidth(me.bodyEl.getWidth());
			}
			if (picker.isFloating()) {
				picker.alignTo(me.inputEl, "", me.pickerOffset);
				isAbove = picker.el.getY() < me.inputEl.getY();
				me.bodyEl[isAbove ? 'addCls' : 'removeCls'](me.openCls
						+ aboveSfx);
				picker.el[isAbove ? 'addCls' : 'removeCls'](picker.baseCls
						+ aboveSfx);
			}
		}
	},

	loadParam : function(store) {
		store.getProxy().extraParams = {
			'tablenames' : this.setParam(),
			'removenodes' : this.setNode(),
			'showNumb' : this.setNumb()
		};
		store.load();
	},

	setParam : function() {
		return "";
	},
	
	setNode : function() {
		return "";
	},
	
	setNumb : function () {
		return false;
	}
});