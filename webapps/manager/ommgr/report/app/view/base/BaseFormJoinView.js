Ext.define('XI.view.base.BaseFormJoinView', {
			extend : 'Ext.form.Panel',
			alias : 'widget._baseformjoinview',
			listeners : {
				create : function(form, data) {
					var join = Ext.getCmp('basegridjoinview_');
					join.getStore().insert(0, data);
					form.up('window').close();
				}
			},
			initComponent : function() {
				this.addEvents('create');
				Ext.apply(this, {
							defaultType : 'textfield',
							activeRecord : null,
							bodyStyle : {
								background : '#ffc',
								padding : '50px 50px'
							},
							defaults : {
								labelSeparator : " ： ",
								allowBlank : false,
								msgTarget : 'side',
								labelAlign : 'left'
							},
							fieldDefaults : {
								anchor : '100%'
							},
							items : [{
										name : 'left',
										id : 'left',
										fieldLabel : '选择字段',
										xtype : '_comboboxtree',
										setParam : this.onBuildPrimary,
										selectMode : 'leaf'
									}, {
										id : 'select',
										fieldLabel : '关联关系',
										name : 'condition',
										xtype : 'combobox',
										typeAhead : true,
										emptyText : '请选择字段',
										editable : false,
										triggerAction : 'all',
										selectOnTab : true,
										store : [['left join', '左关联'],
												['right join', '右关联'],
												['inner join', '内关联'],
												['full join', '全关联']],
										lazyRender : true
									}, {
										fieldLabel : '选择字段',
										name : 'right',
										id : 'right',
										xtype : '_comboboxtree',
										setParam : this.onBuildForeign,
										selectMode : 'leaf'
									}],
							dockedItems : [{
										xtype : 'toolbar',
										dock : 'bottom',
										ui : 'footer',
										items : [{
													iconCls : 'icon-user-add',
													text : '添加',
													scope : this,
													handler : this.onCreate
												}, {
													iconCls : 'icon-reset',
													text : '重置',
													scope : this,
													handler : this.onReset
												}]
									}]
						});
				this.callParent(arguments);
			},
			onCreate : function() {
				var form = this.getForm();
				if (form.isValid()) {
					this.fireEvent('create', this, form.getValues());
					form.reset();
				}
			},
			onReset : function() {
				this.getForm().reset();
			},
			onBuildPrimary : function() {
				var _mainTab = Ext.getCmp('maintable_');
				var primary = [];
				var mt = _mainTab.getValue();
				if (mt) {
					primary.push(mt);
					var joingrid = Ext.getCmp('basegridjoinview_');
					joingrid.getStore().each(function(rec) {
								var da = rec.get('right');
								primary.push(da.substr(0, da.indexOf('.')));
							})
				}
				return primary.join(',');
			},
			onBuildForeign : function() {
				var nodes = Ext.getCmp('mainwestview_').getChecked();
				var allArr = [];
				Ext.Array.each(nodes, function(res) {
							allArr.push(res.get('text'));
						});
				var _mainTab = Ext.getCmp('maintable_');
				var primaryArr = [];
				var mt = _mainTab.getValue();
				if (mt) {
					primaryArr.push(mt);
					var joingrid = Ext.getCmp('basegridjoinview_');
					joingrid.getStore().each(function(rec) {
								var da = rec.get('right');
								primaryArr.push(da.substr(0, da.indexOf('.')));
							});
				}
				var arr_dive = function(aArr, bArr) {
					if (bArr.length == 0) {
						return aArr
					}
					var diff = [];
					var str = bArr.join("&quot;&quot;");
					for (var e in aArr) {
						if (str.indexOf(aArr[e]) == -1) {
							diff.push(aArr[e]);
						}
					}
					return diff;
				}
				return arr_dive(allArr, primaryArr).join(',');
			}
		})