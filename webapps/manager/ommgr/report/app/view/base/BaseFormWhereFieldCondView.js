Ext.define('XI.view.base.BaseFormWhereFieldCondView', {
			extend : 'Ext.form.Panel',
			alias : 'widget._baseformwherefieldcondview',
			listeners : {
				create : function(form, data) {
					var join = Ext.getCmp('basegridwhereview_');
					join.getStore().insert(0, data);
					form.up('window').close();
				}
			},
			initComponent : function() {
				this.addEvents('create');
				Ext.apply(this, {
							defaultType : 'textfield',
							activeRecord : null,
							bodyStyle : {
								background : '#ffc',
								padding : '20px 50px'
							},
							defaults : {
								labelSeparator : " ： ",
								msgTarget : 'side',
								labelAlign : 'left'
							},
							fieldDefaults : {
								anchor : '100%'
							},
							items : [{
										allowBlank : false,
										fieldLabel : '条件与或',
										name : 'whereOrAnd',
										xtype : 'combobox',
										typeAhead : true,
										emptyText : '请选择字段',
										editable : false,
										triggerAction : 'all',
										selectOnTab : true,
										store : [['and', '条件与'], ['or', '条件或']],
										lazyRender : true
									}, {
										allowBlank : false,
										name : 'whereField',
										xtype : '_comboboxtree',
										fieldLabel : '条件字段',
										setParam : this.onBuildTree,
										selectMode : 'leaf'
									}, {
										allowBlank : false,
										name : 'whereCond',
										fieldLabel : '选择条件',
										xtype : 'combobox',
										typeAhead : true,
										emptyText : '请选择条件',
										editable : false,
										triggerAction : 'all',
										selectOnTab : true,
										store : [['>', '大于'], ['<', '小于'],
												['=', '等于'], ['!=', '不等于']],
										lazyRender : true
									}, {
										allowBlank : false,
										fieldLabel : '对比字段',
										emptyText : '请先选择条件字段',
										xtype : '_comboboxtree',
										name : 'whereValue',
										fieldLabel : '条件字段',
										setParam : this.onBuildTree,
										selectMode : 'leaf'
									}],
							dockedItems : [{
										xtype : 'toolbar',
										dock : 'bottom',
										ui : 'footer',
										items : [{
													iconCls : 'icon-user-add',
													text : '添加',
													scope : this,
													handler : this.onCreate
												}, {
													iconCls : 'icon-reset',
													text : '重置',
													scope : this,
													handler : this.onReset
												}]
									}]
						});
				this.callParent(arguments);
			},
			setActiveRecord : function(record) {
				this.activeRecord = record;
				if (record) {
					this.getForm().loadRecord(record);
				} else {
					this.getForm().reset();
				}
			},

			onSave : function() {
				var active = this.activeRecord, form = this.getForm();
				if (!active) {
					return;
				}
				if (form.isValid()) {
					form.updateRecord(active);
					this.onReset();
				}
			},

			onCreate : function() {
				var form = this.getForm();
				if (form.isValid()) {
					this.fireEvent('create', this, form.getValues());
					form.reset();
				}
			},

			onReset : function() {
				this.setActiveRecord(null);
				this.getForm().reset();
			},

			onBuildTree : function() {
				var dataSubstr = function(dataStr) {
					return dataStr.substr(0, dataStr.indexOf('.'));
				}
				var primaryStr = Ext.getCmp('maintable_').getValue();
				var join = Ext.getCmp('basegridjoinview_');
				var allArr = [];
				allArr.push(primaryStr);
				join.getStore().each(function(data) {
							allArr.push(dataSubstr(data.get('right')));
						})
				return allArr.join(',');
			}

		})