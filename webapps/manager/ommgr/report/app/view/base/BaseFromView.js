Ext.define('XI.view.base.BaseFromView', {
			extend : 'Ext.form.Panel',
			alias : 'widget._basefromview',
			initComponent : function() {
				Ext.apply(this, {
							bodyStyle : {
								padding : '15px 15px'
							},
							defaultType : 'textfield',
							items : [{
										id : 'maintable_',
										xtype : 'combobox',
										editable : false,
										labelWidth : 80,
										queryMode : 'local',
										displayField : 'name',
										valueField : 'value',
										triggerAction : 'all',
										store : 'base.BaseFromComboStore',
										emptyText : '请选择主表',
										fieldLabel : '选择主表'
									}, {
										id : 'reportname_',
										labelWidth : 80,
										fieldLabel : '报表名称',
										maskRe : /[A-Za-z0-9\u4e00-\u9fa5\-]/,
										allowBlank : false,
										name : 'reportname'
									}],
							defaults : {
								labelSeparator : " ： ",
								allowBlank : false,
								msgTarget : 'side',
								labelAlign : 'right'
							},
							fieldDefaults : {
								anchor : '100%'
							}
						});
				this.callParent(arguments);
			}

		})