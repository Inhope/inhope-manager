Ext.define('XI.view.base.BaseGridJoinView', {
			extend : 'Ext.grid.Panel',
			alias : 'widget._basegridjoinview',
			initComponent : function() {
				Ext.apply(this, {
							store : 'base.BaseGridJoinStore',
							bodyStyle : 'overflow-x:hidden; overflow-y:hidden',
							frame : true,
							dockedItems : [{
										xtype : 'toolbar',
										items : [{
													text : '删除关系',
													disabled : true,
													itemId : 'delete',
													scope : this,
													handler : this.onDeleteClick
												}, {
													text : '创建关系',
													disabled : false,
													itemId : 'create',
													scope : this,
													handler : this.onCreateWin
												}]
									}],
							columns : [{
										header : '字段',
										flex : 1,
										style : "text-align:center;",
										align : "center",
										dataIndex : 'left'
									}, {
										header : '关联关系',
										flex : 1,
										style : "text-align:center;",
										align : "center",
										dataIndex : 'condition'
									}, {
										header : '字段',
										flex : 1,
										style : "text-align:center;",
										align : "center",
										dataIndex : 'right'
									}]
						});
				this.win = Ext.create('widget.window', {
							closable : true,
							closeAction : 'hide',
							title : '创建join关系',
							id : 'createjoin_',
							width : 600,
							minWidth : 350,
							height : 300,
							modal : true,
							layout : {
								type : 'fit',
								padding : 5
							},
							items : [{
										id : 'baseformjoinview_',
										split : true,
										xtype : '_baseformjoinview'
									}]
						});
				this.getSelectionModel().on('selectionchange',
						this.onSelectChange, this);
				this.callParent(arguments);
			},

			listeners : {
				selectionchange : function(selModel, selected) {
					// this.win.child('#relationInfo').setActiveRecord(selected[0]
					// || null);
				}
			},

			onSelectChange : function(selModel, selections) {
				this.down('#delete').setDisabled(selections.length === 0);
			},

			onDeleteClick : function() {
				var selection = this.getView().getSelectionModel()
						.getSelection()[0];
				if (selection) {
					this.store.remove(selection);
				}
			},

			onCreateWin : function() {

				if (this.isAllowShowWin()) {
					var primaryStr = this.getPrimaryTable();
					if (primaryStr) {
						this.win.child('form').getForm().reset();
						this.win.show();
					} else {
						Ext.MessageBox.alert('提示：', '请设置主表后才能建立关系!');
					}
				}
			},

			arr_dive : function(aArr, bArr) {
				if (bArr.length == 0) {
					return aArr
				}
				var diff = [];
				var str = bArr.join("&quot;&quot;");
				for (var e in aArr) {
					if (str.indexOf(aArr[e]) == -1) {
						diff.push(aArr[e]);
					}
				}
				return diff
			},

			isAllowShowWin : function() {
				var mainwestview = Ext.getCmp('mainwestview_');
				var nodesArr = mainwestview.getChecked();
				if (nodesArr.length > 1) {
					return true;
				}
				Ext.MessageBox.alert('提示：', '请选择至少两张表,才能建立关系!');
				return false;
			},

			getPrimaryTable : function() {
				var _mainTab = Ext.getCmp('maintable_');
				return _mainTab.getValue();
			}

		})