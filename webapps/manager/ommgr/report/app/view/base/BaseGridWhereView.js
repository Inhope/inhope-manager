Ext.define('XI.view.base.BaseGridWhereView', {
			extend : 'Ext.grid.Panel',
			alias : 'widget._basegridwhereview',
			initComponent : function() {
				Ext.apply(this, {
							store : 'base.BaseGridWhereStore',
							bodyStyle : 'overflow-x:hidden; overflow-y:hidden',
							frame : true,
							dockedItems : [{
										xtype : 'toolbar',
										items : [{
													text : '删除条件',
													disabled : true,
													itemId : 'delete',
													scope : this,
													handler : this.onDeleteClick
												}, {
													text : '创建普通条件',
													disabled : false,
													itemId : 'create',
													scope : this,
													handler : this.onCreateWin
												}, {
													text : '创建字段对比条件',
													disabled : false,
													itemId : 'createFieldCond',
													scope : this,
													handler : this.onCreateWinFieldCond
												}]
									}],
							columns : [{
										header : '条件与或',
										flex : 1,
										style : "text-align:center;",
										align : "center",
										dataIndex : 'whereOrAnd'
									}, {
										header : '条件字段',
										flex : 3,
										style : "text-align:center;",
										align : "center",
										dataIndex : 'whereField'
									}, {
										header : '条件',
										flex : 1,
										style : "text-align:center;",
										align : "center",
										dataIndex : 'whereCond'
									}, {
										header : '条件值',
										flex : 2,
										style : "text-align:center;",
										align : "center",
										dataIndex : 'whereValue'
									}]
						});
				this.win = Ext.create('widget.window', {
							closable : true,
							closeAction : 'hide',
							title : '创建普通条件',
							id : 'createwhere_',
							width : 600,
							minWidth : 350,
							height : 300,
							modal : true,
							layout : {
								type : 'fit',
								padding : 5
							},
							items : [{
										id : 'baseformwhereview_',
										split : true,
										xtype : '_baseformwhereview'
									}]
						});
				this.winFieldCond = Ext.create('widget.window', {
							closable : true,
							closeAction : 'hide',
							title : '创建字段对比条件',
							id : 'createwhereFeildCond_',
							width : 600,
							minWidth : 350,
							height : 300,
							modal : true,
							layout : {
								type : 'fit',
								padding : 5
							},
							items : [{
										id : 'baseformwherefieldcondview_',
										split : true,
										xtype : '_baseformwherefieldcondview'
									}]
						});
				this.getSelectionModel().on('selectionchange', this.onSelectChange, this);
				this.callParent(arguments);
			},
			onDeleteClick : function() {
				var selection = this.getView().getSelectionModel()
						.getSelection()[0];
				if (selection) {
					this.store.remove(selection);
				}
			},
			onSelectChange : function(selModel, selections) {
				this.down('#delete').setDisabled(selections.length === 0);
			},
			onCreateWin : function() {
				var maintable = Ext.getCmp('maintable_');
				if (maintable.getValue()) {
					this.win.child('form').getForm().reset();
					this.win.show();
				} else {
					Ext.MessageBox.alert('提示：', '请选择主表后,才能建立条件!');
				}
			},
			onCreateWinFieldCond : function() {
				var maintable = Ext.getCmp('maintable_');
				if (maintable.getValue()) {
					this.winFieldCond.child('form').getForm().reset();
					this.winFieldCond.show();
				} else {
					Ext.MessageBox.alert('提示：', '请选择主表后,才能建立条件!');
				}
			}
		})