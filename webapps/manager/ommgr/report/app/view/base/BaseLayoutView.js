Ext.define('XI.view.base.BaseLayoutView', {
			extend : 'Ext.panel.Panel',
			alias : 'widget._baselayoutview',
			layout : 'border',
			defaults : {
				split : true
			},
			items : [{
						title : '已选择表结构',
						region : 'west',
						width : '50%',
						xtype : '_basewestview',
						id : 'basewestview_'
					}, {
						region : 'center',
						layout : 'border',
						defaults : {
							split : true,
							bodyStyle : 'padding : 1px'
						},
						items : [{
									title : '设置主表',
									height : 120,
									region : 'north',
									xtype : '_basefromview',
									id : 'baseformview_'
								}, {
									title : '设置关联关系',
									region : 'center',
									id : 'basegridjoinview_',
									xtype : '_basegridjoinview'
								}, {
									title : '设置过滤条件',
									height : '55%',
									id : 'basegridwhereview_',
									xtype : '_basegridwhereview',
									region : 'south'
								}]
					}]
		})
