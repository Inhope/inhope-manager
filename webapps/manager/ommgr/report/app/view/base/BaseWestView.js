var cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
			clicksToEdit : 1
		});
Ext.define('XI.view.base.BaseWestView', {
			extend : 'Ext.tree.Panel',
			alias : 'widget._basewestview',
			store : 'base.BaseWestStore',
			rootVisible : false,
			bodyStyle : 'overflow-x:hidden; overflow-y:hidden',
			animate : true,
			plugins : [cellEditing],
			columns : [{
						xtype : 'treecolumn',
						text : '字段名称',
						flex : 1,
						dataIndex : 'text'
					}, {
						text : '字段注释',
						flex : 1,
						dataIndex : 'comments',
						editor : {
							allowBlank : true
						}
					}],
			listeners : {
				load : function() {
					this.expandAll();
				}
			}
		})