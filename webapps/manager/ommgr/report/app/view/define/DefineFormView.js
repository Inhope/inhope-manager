Ext.define('XI.view.define.DefineFormView', {
	extend : 'Ext.form.Panel',
	alias : 'widget._defineformview',
	initComponent : function() {
		Ext.apply(this, {
			title : '自定义sql',
			bodyStyle : {
				padding : '50px 50px'
			},
			border : true,
			fieldDefaults : {
				labelAlign : 'right',
				labelSeparator : " ： ",
				allowBlank : false,
				msgTarget : 'side',
				labelWidth : 140,
				anchor : '100%'
			},
			items : [{
						xtype : 'textfield',
						name : 'menuStr',
						fieldLabel : '自定义报表名称',
						maxLength : 20,
						emptyText : '请输入报表名称',
						blankText : '请输入报表名称',
						maskRe : /[A-Za-z0-9\u4e00-\u9fa5\-]/,
						maxLengthText : '报表名称不能超过20个字符'
					}, {
						xtype : 'textfield',
						name : 'tableStr',
						maxLength : 80,
						fieldLabel : '主表名称',
						emptyText : '请输入主表名称',
						blankText : '请输入主表名称',
						maxLengthText : '主表名称不能超过80个字符',
						maskRe : /[A-Za-z0-9\_]/,
						regex : /^[A-Za-z0-9]+(\_[A-Za-z0-9]+)*$/,
						regexText : '主表英文字母加下划线'
					}, {
						xtype : 'textfield',
						name : 'headStr',
						fieldLabel : '报表显示列的表头',
						emptyText : '编号-名称-xxx-xxx',
						blankText : '请输入报表显示列的表头',
						maxLength : 60,
						maxLengthText : '报表名称不能超过60个字符',
						maskRe : /[A-Za-z0-9\u4e00-\u9fa5\-]/,
						regex : /^[A-Za-z0-9\u4e00-\u9fa5]+(\-[A-Za-z0-9\u4e00-\u9fa5]+)*$/,
						regexText : '填写格式：编号-名称-xxx...'
					}, {
						xtype : 'textareafield',
						name : 'defineSql',
						fieldLabel : '完整的sql语句',
						blankText : '请输入完整的sql语句',
						emptyText : '输入的sql语句不应含有update|delete|insert|trancate|into|ascii|declare|exec|master|drop|execute等非法字符!',
						height : 300,
						maxLength : 5000,
						maxLengthText : 'sql语句不能超过5000个字符',
						preventScrollbars : true
					}],
			dockedItems : [{
						xtype : 'toolbar',
						dock : 'bottom',
						ui : 'footer',
						items : [{
									iconCls : 'icon-user-add',
									text : '生成自定义sql报表',
									scope : this,
									handler : this.onCreate
								}, {
									iconCls : 'icon-reset',
									text : '清空',
									scope : this,
									handler : this.onReset
								}]
					}]
		});
		this.callParent(arguments);
	},
	onCreate : function() {
		var form = this.getForm();
		if (form.isValid()) {
			var obj = form.getValues();
			Ext.Ajax.request({
						url : 'report!storageDefineSql.action',
						params : {
							tableStr : obj.tableStr,
							menuStr : obj.menuStr,
							headStr : obj.headStr,
							defineSql : obj.defineSql
						},
						method : 'POST',
						success : function(response, options) {
							var json = Ext.JSON.decode(response.responseText);
							Ext.MessageBox.alert('提示:', json.message);
						},
						failure : function(response, options) {
							Ext.MessageBox.alert('失败', '请求超时或网络故障,错误编号：'
											+ response.status);
						}
					});
		}
	},
	onReset : function() {
		this.getForm().reset();
	}
});