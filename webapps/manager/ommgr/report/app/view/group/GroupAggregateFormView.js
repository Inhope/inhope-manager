Ext.define('XI.view.group.GroupAggregateFormView', {
			extend : 'Ext.form.Panel',
			alias : 'widget._groupappregateformview',
			listeners : {
				create : function(form, data) {
					var join = Ext.getCmp('groupappregategridview_');
					join.getStore().insert(0, data);
					var reportUtil = Ext.create("XI.util.ExtUtil");
					var mixHead_ = Ext.getCmp('mixHead_');
					var rootMixNode = mixHead_.getStore().getRootNode();
					reportUtil.doGridToSetTreeNode(data, rootMixNode);
					form.up('window').close();
				}
			},
			initComponent : function() {
				this.addEvents('create');
				Ext.apply(this, {
							defaultType : 'textfield',
							activeRecord : null,
							bodyStyle : {
								padding : '50px 50px'
							},
							defaults : {
								labelSeparator : " ： ",
								allowBlank : false,
								msgTarget : 'side',
								labelAlign : 'right'
							},
							fieldDefaults : {
								anchor : '100%'
							},
							items : [{
								id : 'appearAggregate',
								name : 'appearAggregate',
								fieldLabel : '聚合函数',
								xtype : 'combobox',
								typeAhead : true,
								editable : false,
								value : 'avg',
								triggerAction : 'all',
								selectOnTab : true,
								store : [['max', '最大值'], ['min', '最小值'],
										['avg', '平均值'],  ['sum', '总和']],
								lazyRender : true
							}, {
								id : 'appearAggregateFiled',
								name : 'appearAggregateFiled',
								fieldLabel : '选择字段',
								xtype : '_comboboxtree',
								setParam : this.onBuildPrimary,
//								setNumb : this.onSetNumb,
								selectMode : 'leaf'
							}, {
								id : 'appearAggregateComments',
								name : 'appearAggregateComments',
								fieldLabel : '填写聚合表头'
							}],
							dockedItems : [{
										xtype : 'toolbar',
										dock : 'bottom',
										ui : 'footer',
										items : [{
												iconCls : 'icon-user-add',
												text : '添加',
												scope : this,
												handler : this.onCreate
											}, {
												iconCls : 'icon-reset',
												text : '重置',
												scope : this,
												handler : this.onReset
											}]
									}]
						});
				this.callParent(arguments);
			},
			onCreate : function() {
				var form = this.getForm();
				if (form.isValid()) {
					this.fireEvent('create', this, form.getValues());
					form.reset();
				}
			},
			onReset : function() {
				this.getForm().reset();
			},
			onBuildPrimary : function() {
				var dataSubstr = function(dataStr) {
					return dataStr.substr(0, dataStr.indexOf('.'));
				}
				var primaryStr = Ext.getCmp('maintable_').getValue();
				var join = Ext.getCmp('basegridjoinview_');
				var allArr = [];
				allArr.push(primaryStr);
				join.getStore().each(function(data) {
							allArr.push(dataSubstr(data.get('right')));
						})
				return allArr.join(',');
			},
			onSetNumb : function () {
				return true;
			}
		})