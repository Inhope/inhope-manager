Ext.define('XI.view.group.GroupAggregateGridView', {
			extend : 'Ext.grid.Panel',
			alias : 'widget._groupappregategridview',
			initComponent : function() {
				Ext.apply(this, {
							store : 'group.GroupAppregateGridStore',
							bodyStyle : 'overflow-x:hidden; overflow-y:hidden',
							frame : true,
							dockedItems : [{
										xtype : 'toolbar',
										items : [{
													text : '删除聚合字段',
													disabled : true,
													itemId : 'delete',
													scope : this,
													handler : this.onDeleteClick
												}, {
													text : '创建聚合字段',
													disabled : false,
													itemId : 'create',
													scope : this,
													handler : this.onCreateWin
												}]
									}],
							columns : [{
										header : '聚合函数',
										flex : 1.5,
										style : "text-align:center;",
										align : "center",
										dataIndex : 'appearAggregate'
									}, {
										header : '显示字段',
										flex : 3,
										style : "text-align:center;",
										align : "center",
										dataIndex : 'appearAggregateFiled'
									}, {
										header : '聚合表头',
										flex : 3,
										style : "text-align:center;",
										align : "center",
										dataIndex : 'appearAggregateComments'
									}]
						});
				this.win = Ext.create('widget.window', {
							closable : true,
							closeAction : 'hide',
							title : '创建显示聚合字段',
							id : 'creategroupaggregategrid_',
							width : 600,
							minWidth : 350,
							height : 300,
							modal : true,
							layout : {
								type : 'fit',
								padding : 5
							},
							items : [{
										id : 'groupappregateformview_',
										split : true,
										xtype : '_groupappregateformview'
									}]
						});
				this.getSelectionModel().on('selectionchange',
						this.onSelectChange, this);
				this.callParent(arguments);
			},
			onDeleteClick : function() {
				var selection = this.getView().getSelectionModel()
						.getSelection()[0];
				if (selection) {
					this.store.remove(selection);
					var mixHead_ = Ext.getCmp('mixHead_');
					var rootMixNode = mixHead_.getStore().getRootNode();
					Ext.create("XI.util.ExtUtil").doRemoveTreeNode(rootMixNode, 
						selection.data.appearAggregate + '(' + selection.data.appearAggregateFiled + ')');
				}
			},
			onSelectChange : function(selModel, selections) {
				this.down('#delete').setDisabled(selections.length === 0);
			},
			onCreateWin : function() {
				var groupbygridview_ = Ext.getCmp('groupbygridview_');
				if (groupbygridview_.getStore().getCount() > 0) {
					this.win.child('form').getForm().reset();
					this.win.show();
				} else {
					Ext.MessageBox.alert('提示', '请先设置group by条件！');
				}
			}
		})