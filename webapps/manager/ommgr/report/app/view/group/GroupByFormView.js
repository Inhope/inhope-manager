Ext.define('XI.view.group.GroupByFormView', {
	extend : 'Ext.form.Panel',
	alias : 'widget._groupbyformview',
	listeners : {
		create : function(form, data) {
			var joinStore = Ext.getCmp('groupbygridview_').getStore();
			joinStore.insert(0, data);

			// controll basewestview_ selection
			var mixHead_ = Ext.getCmp('mixHead_');
			var rootMixNode = mixHead_.getStore().getRootNode();

			var base = Ext.getCmp('basewestview_');
			var dataSubstr = function(dataStr) {
				return dataStr.substr(dataStr.indexOf('.') + 1, dataStr.length);
			}
			var isEquals = function(textArr, textStr) {
				var result = false;
				textArr.each(function(txtArr) {
							if (txtArr.data.groupbyvalue == textStr) {
								result = true;
								return result;
							}
						})
				return result;
			}
			var iterationChild = function(node, flag) {
				node.eachChild(function(child) {
							if (isEquals(joinStore, node.data.text + '.'
											+ child.data.text)) {
								child.set('checked', false);
							} else {
								child.set('checked', flag);
							}
							Ext.create("XI.util.ExtUtil").doRemoveTreeNode(
									rootMixNode,
									node.data.text + '.' + child.data.text);
						});
			};
			base.getRootNode().eachChild(function(n) {
						n.set('checked', null);
						iterationChild(n, null);
					});
			var chartform_ = Ext.getCmp('groupchartformview_');		
			chartform_.getForm().reset();
			form.up('window').close();
		}
	},
	initComponent : function() {
		this.addEvents('create');
		Ext.apply(this, {
					defaultType : 'textfield',
					activeRecord : null,
					bodyStyle : {
						padding : '50px 50px'
					},
					defaults : {
						labelSeparator : " ： ",
						allowBlank : false,
						msgTarget : 'side',
						labelAlign : 'right'
					},
					fieldDefaults : {
						anchor : '100%'
					},
					items : [{
								id : 'selectgroupby_',
								name : 'groupby',
								fieldLabel : '分组',
								xtype : 'combobox',
								typeAhead : true,
								editable : false,
								value : 'group by',
								triggerAction : 'all',
								selectOnTab : true,
								store : [['group by', '分组']],
								lazyRender : true
							}, {
								id : 'selectgroupbyvalue_',
								name : 'groupbyvalue',
								fieldLabel : '选择字段',
								xtype : '_comboboxtree',
								setParam : this.setParamTree,
								setNode : this.setNodeTree,
								selectMode : 'leaf'
							}],
					dockedItems : [{
								xtype : 'toolbar',
								dock : 'bottom',
								ui : 'footer',
								items : [{
											iconCls : 'icon-user-add',
											text : '添加',
											scope : this,
											handler : this.onCreate
										}, {
											iconCls : 'icon-reset',
											text : '重置',
											scope : this,
											handler : this.onReset
										}]
							}]
				});
		this.callParent(arguments);
	},
	onCreate : function() {
		var form = this.getForm();
		if (form.isValid()) {
			this.fireEvent('create', this, form.getValues());
			form.reset();
		}
	},
	onReset : function() {
		this.getForm().reset();
	},
	setParamTree : function() {
		// var nodes = Ext.getCmp('mainwestview_').getChecked();
		// var allArr = [];
		// Ext.Array.each(nodes, function(res) {
		// allArr.push(res.get('text'));
		// });
		// return allArr.join(',');
		var dataSubstr = function(dataStr) {
			return dataStr.substr(0, dataStr.indexOf('.'));
		}
		var primaryStr = Ext.getCmp('maintable_').getValue();
		var join = Ext.getCmp('basegridjoinview_');
		var allArr = [];
		allArr.push(primaryStr);
		join.getStore().each(function(data) {
					allArr.push(dataSubstr(data.get('right')));
				})
		return allArr.join(',');
	},
	setNodeTree : function() {
		var dataArr = [];
		var groupbygrid = Ext.getCmp('groupbygridview_');
		groupbygrid.getStore().each(function(rec) {
					dataArr.push(rec.get('groupbyvalue'));
				});
		return dataArr.join(',');
	}
})