Ext.define('XI.view.group.GroupByGridView', {
	extend : 'Ext.grid.Panel',
	alias : 'widget._groupbygridview',
	initComponent : function() {
		Ext.apply(this, {
					store : 'group.GroupByGridStore',
					bodyStyle : 'overflow-x:hidden; overflow-y:hidden',
					frame : true,
					dockedItems : [{
								xtype : 'toolbar',
								items : [{
											text : '删除分组',
											disabled : true,
											itemId : 'delete',
											scope : this,
											handler : this.onDeleteClick
										}, {
											text : '创建分组',
											disabled : false,
											itemId : 'create',
											scope : this,
											handler : this.onCreateWin
										}]
							}],
					columns : [{
								header : '分组',
								flex : 1,
								style : "text-align:center;",
								align : "center",
								dataIndex : 'groupby'
							}, {
								header : '分组字段',
								flex : 3,
								style : "text-align:center;",
								align : "center",
								dataIndex : 'groupbyvalue'
							}]
				});
		this.win = Ext.create('widget.window', {
					closable : true,
					closeAction : 'hide',
					title : '创建group by条件',
					id : 'creategroupbygrid_',
					width : 600,
					minWidth : 350,
					height : 300,
					modal : true,
					layout : {
						type : 'fit',
						padding : 5
					},
					items : [{
								id : 'groupbyformview_',
								split : true,
								xtype : '_groupbyformview'
							}]
				});
		this.getSelectionModel().on('selectionchange', this.onSelectChange,
				this);
		this.callParent(arguments);
	},
	onDeleteClick : function() {
		var selection = this.getView().getSelectionModel().getSelection()[0];
		if (selection) {
			this.store.remove(selection);
			var havingStore = Ext.getCmp('grouphavinggridview_').getStore();
			havingStore.each(function(data) {
						if (selection.get('groupbyvalue') == data
								.get('havingField')) {
							havingStore.remove(data);
							return false;
						}
					})
		}
		var base = Ext.getCmp('basewestview_');
		var mixHeadRootNode = Ext.getCmp('mixHead_').getStore().getRootNode();
		var reportUtil = Ext.create('XI.util.ExtUtil');
		if (this.store.getCount() == 0) {
			var groupappregategridview_ = Ext.getCmp('groupappregategridview_');
			var grouphavinggridview_ = Ext.getCmp('grouphavinggridview_');
			var mixHead_ = Ext.getCmp('mixHead_');
			groupappregategridview_.getStore().removeAll();
			grouphavinggridview_.getStore().removeAll();
			reportUtil.setTreeNodeDefaultFalse(base.getRootNode());
			mixHead_.getStore().getRootNode().removeAll();
		} else {
			reportUtil.setTreeNodeState(base.getRootNode(),
					selection.data.groupbyvalue, null);
		}
		reportUtil.doRemoveTreeNode(mixHeadRootNode,
				selection.data.groupbyvalue);
	},
	onSelectChange : function(selModel, selections) {
		this.down('#delete').setDisabled(selections.length === 0);
	},
	onCreateWin : function() {
		var maintable = Ext.getCmp('maintable_');
		if (maintable.getValue()) {
			this.win.child('form').getForm().reset();
			this.win.show();
		} else {
			Ext.MessageBox.alert('提示：', '请选择主表！');
		}

	}
})