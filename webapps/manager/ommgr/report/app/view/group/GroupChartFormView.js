Ext.define('XI.view.group.GroupChartFormView', {
	extend : 'Ext.form.Panel',
	alias : 'widget._groupchartformview',
	initComponent : function() {
		Ext.apply(this, {
			defaultType : 'combobox',
			activeRecord : null,
			bodyStyle : {
				padding : '40px 10px'
			},
			defaults : {
				labelSeparator : " ： ",
				editable : false,
				msgTarget : 'side',
				disabled : true,
				width : 200,
				labelWidth : 80,
				labelAlign : 'right'
			},
			fieldDefaults : {
				anchor : '100%'
			},
			items : [{
					fieldLabel : '显示图表',
					xtype : 'radiogroup',
					disabled : false,
					items : [{
								boxLabel : '是',
								name : 'isChart',
								inputValue : 1
							}, {
								boxLabel : '否',
								name : 'isChart',
								inputValue : 0,
								checked : true
							}],
					listeners : {
						change : function(radio, value) {
							this.up('panel').onSetComboState(value.isChart);
						}
					},
					id : 'isChart_'
				}, {
					fieldLabel : '图表类型',
					emptyText : '选择图表类型',
					name : 'charttype',
					queryMode : 'local',
					displayField : 'name',
					valueField : 'value',
					triggerAction : 'all',
					selectOnTab : true,
					store : [['column', '柱状图'], ['line', '折线图']],
					id : 'charttype_'
				}, {
					fieldLabel : 'X轴字段',
					emptyText : '请选择X轴字段',
					name : 'chartx',
					queryMode : 'local',
					displayField : 'name',
					valueField : 'value',
					triggerAction : 'all',
					store : {
						type : 'array',
						fields : ["name", "value"]
					},
					listeners : {
						expand : function(combox) {
							var chartx_ = this.up('panel').child('#chartx_');
							chartx_.getStore().loadData(this.up('panel').onSelectSource(true));
						}
					},
					id : 'chartx_'
				}, {
					fieldLabel : 'Y轴字段',
					emptyText : '请选择Y轴字段',
					multiSelect: true,
					name : 'charty',
					queryMode : 'local',
					displayField : 'name',
					valueField : 'value',
					triggerAction : 'all',
					store : {
						type : 'array',
						fields : ["name", "value"]
					},
					listeners : {
						expand : function(combox) {
							var charty_ = this.up('panel').child('#charty_');
							charty_.getStore().loadData(this.up('panel').onSelectSource(false));
						}
					},
					id : 'charty_'
				}]
		});
		this.callParent(arguments);
	},
	onSelectSource : function(value) {
		var rootNode = Ext.getCmp('mixHead_').getStore().getRootNode();
		var dataArr = [];
		function iterateChildAll(rootNode) {
			rootNode.eachChild(function(child) {
					if (child.isLeaf()) {
						var data = {};
						//data['name'] = child.data.comments ? child.data.comments : child.data.text;
						data['name'] = child.data.text;
						data['value'] = child.data.text;
						dataArr.push(data);
					} else {
						iterateChildAll(child);
					}
				})
		};
		function iterateChildCond(rootNode) {
			rootNode.eachChild(function(child) {
					if (child.isLeaf()) {
						if(child.data.numb=='true') {
							var data = {};
							//data['name'] = child.data.comments ? child.data.comments : child.data.text;
							data['name'] = child.data.text;
							data['value'] = child.data.text;
							dataArr.push(data);
						}
					} else {
						iterateChildCond(child);
					}
				})
		};
		if(value) {
			iterateChildAll(rootNode);
		} else {
			iterateChildCond(rootNode);
		}
		return dataArr;
	},
	onSetComboState : function(value) {
		var charttype_ = this.child('#charttype_');
		var chartx_ = this.child('#chartx_');
		var charty_ = this.child('#charty_');
		if (value == 0) {
			charttype_.setDisabled(true);
			chartx_.clearValue();
			charty_.clearValue();
			chartx_.setDisabled(true);
			charty_.setDisabled(true);
		} else {
			charttype_.setDisabled(false);
			chartx_.setDisabled(false);
			charty_.setDisabled(false);
		}
	}
})