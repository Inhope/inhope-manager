Ext.define('XI.view.group.GroupHavingGridView', {
			extend : 'Ext.grid.Panel',
			alias : 'widget._grouphavinggridview',
			initComponent : function() {
				Ext.apply(this, {
							store : 'group.GroupHavingGridStore',
							bodyStyle : 'overflow-x:hidden; overflow-y:hidden',
							frame : true,
							dockedItems : [{
										xtype : 'toolbar',
										items : [{
													text : '删除分组条件',
													disabled : true,
													itemId : 'delete',
													scope : this,
													handler : this.onDeleteClick
												}, {
													text : '创建聚合条件',
													disabled : false,
													itemId : 'create',
													scope : this,
													handler : this.onCreateWin
												}, {
													text : '创建非聚合条件',
													disabled : false,
													itemId : 'createNoneAppregate',
													scope : this,
													handler : this.onCreateNoneAppregate
												}]
									}],
							columns : [{
										header : 'Having',
										flex : 1.5,
										style : "text-align:center;",
										align : "center",
										dataIndex : 'having'
									}, {
										header : '与或',
										flex : 1.5,
										style : "text-align:center;",
										align : "center",
										dataIndex : 'havingOrAnd'
									}, {
										header : '函数',
										flex : 1.5,
										style : "text-align:center;",
										align : "center",
										dataIndex : 'havingFunction'
									}, {
										header : '字段',
										flex : 3,
										style : "text-align:center;",
										align : "center",
										dataIndex : 'havingField'
									}, {
										header : '条件',
										flex : 1,
										style : "text-align:center;",
										align : "center",
										dataIndex : 'havingCond'
									}, {
										header : '条件值',
										flex : 3,
										style : "text-align:center;",
										align : "center",
										dataIndex : 'havingValue'
									}]
						});
				this.win = Ext.create('widget.window', {
							closable : true,
							closeAction : 'hide',
							title : '创建having条件',
							id : 'creategrouphavinggrid_',
							width : 600,
							minWidth : 350,
							height : 300,
							modal : true,
							layout : {
								type : 'fit',
								padding : 5
							},
							items : [{
										id : 'grouphavingformview_',
										split : true,
										xtype : '_grouphavingformview'
									}]
						});
				this.winCond = Ext.create('widget.window', {
							closable : true,
							closeAction : 'hide',
							title : '创建非聚合条件',
							id : 'creategrouphavingnonegrid_',
							width : 600,
							minWidth : 350,
							height : 300,
							modal : true,
							layout : {
								type : 'fit',
								padding : 5
							},
							items : [{
										id : 'grouphavingnoneformview_',
										split : true,
										xtype : '_grouphavingnoneformview'
									}]
						});
				this.getSelectionModel().on('selectionchange',
						this.onSelectChange, this);
				this.callParent(arguments);
			},
			onDeleteClick : function() {
				var selection = this.getView().getSelectionModel()
						.getSelection()[0];
				if (selection) {
					this.store.remove(selection);
				}
			},
			onSelectChange : function(selModel, selections) {
				this.down('#delete').setDisabled(selections.length === 0);
			},
			onCreateWin : function() {
				var groupbygridview_ = Ext.getCmp('groupbygridview_');
				if (groupbygridview_.getStore().getCount() > 0) {
					this.win.child('form').getForm().reset();
					this.win.show();
				} else {
					Ext.MessageBox.alert('提示', '请先设置group by条件！');
				}
			},
			onCreateNoneAppregate : function() {
				var groupbystore = Ext.getCmp('groupbygridview_').getStore();
				if (groupbystore.getCount() > 0) {
					var dataObjArr = [];
					groupbystore.each(function(data) {
						var dataObj = {};
						dataObj['name'] = data.get('groupbyvalue');
						dataObj['value'] = data.get('groupbyvalue');
						dataObjArr.push(dataObj);
					})
					var comboStore = Ext.getCmp('selectgrouphavingnone').getStore();
					comboStore.loadData(dataObjArr);
					this.winCond.child('form').getForm().reset();
					this.winCond.show();
				} else {
					Ext.MessageBox.alert('提示', '请先设置group by条件！');
				}
			}
		})