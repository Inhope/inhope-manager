Ext.define('XI.view.group.GroupHavingNoneFormView', {
			extend : 'Ext.form.Panel',
			alias : 'widget._grouphavingnoneformview',
			listeners : {
				create : function(form, data) {
					var join = Ext.getCmp('grouphavinggridview_');
					join.getStore().insert(0, data);
					form.up('window').close();
				}
			},
			initComponent : function() {
				this.addEvents('create');
				Ext.apply(this, {
							defaultType : 'textfield',
							activeRecord : null,
							bodyStyle : {
								padding : '30px 50px'
							},
							defaults : {
								labelSeparator : " ： ",
								allowBlank : false,
								msgTarget : 'side',
								labelAlign : 'right'
							},
							fieldDefaults : {
								anchor : '100%'
							},
							items : [{
										fieldLabel : '条件与或',
										name : 'havingOrAnd',
										id : 'orAndHavingnone_',
										xtype : 'combobox',
										typeAhead : true,
										emptyText : '请选择字段',
										editable : false,
										triggerAction : 'all',
										selectOnTab : true,
										store : [['and', '条件与'], ['or', '条件或']],
										lazyRender : true
									}, {
										id : 'selectgrouphavingnone',
										name : 'having',
										fieldLabel : 'Having',
										xtype : 'combobox',
										typeAhead : true,
										editable : false,
										value : 'having',
										triggerAction : 'all',
										selectOnTab : true,
										store : [['having', 'Having']],
										lazyRender : true
									}, {
										id : 'selectgrouphavingnone',
										name : 'havingField',
										xtype : 'combobox',
										editable : false,
										queryMode : 'local',
										displayField : 'name',
										valueField : 'value',
										triggerAction : 'all',
										store : {
											fields: ['name', 'value']
										},
										emptyText : '请选择字段',
										fieldLabel : '选择字段'
									}, {
										id : 'havingcond',
										name : 'havingCond',
										fieldLabel : '选择条件',
										xtype : 'combobox',
										typeAhead : true,
										emptyText : '请选择条件',
										editable : false,
										triggerAction : 'all',
										selectOnTab : true,
										store : [['>', '大于'], ['<', '小于'],
												['=', '等于'], ['!=', '不等于'],
												['like', '模糊'],
												['between', '区间'], ['in', '范围']],
										lazyRender : true
									}, {
										name : 'havingValue',
										fieldLabel : '条件值',
										id : 'havingcondnonevalue'
									}],
							dockedItems : [{
										xtype : 'toolbar',
										dock : 'bottom',
										ui : 'footer',
										items : [{
													iconCls : 'icon-user-add',
													text : '添加',
													scope : this,
													handler : this.onCreate
												}, {
													iconCls : 'icon-reset',
													text : '重置',
													scope : this,
													handler : this.onReset
												}]
									}]
						});
				this.callParent(arguments);
			},
			onCreate : function() {
				var form = this.getForm();
				if (form.isValid()) {
					this.fireEvent('create', this, form.getValues());
					form.reset();
				}
			},
			onReset : function() {
				this.getForm().reset();
			} 
		})