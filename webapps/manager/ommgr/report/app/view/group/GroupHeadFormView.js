Ext.define('XI.view.group.GroupHeadFormView', {
			extend : 'Ext.form.Panel',
			alias : 'widget._groupheadformview',
			initComponent : function() {
				this.addEvents('create');
				Ext.apply(this, {
							defaultType : 'textfield',
							defaults : {
								labelSeparator : " ： ",
								allowBlank : false,
								msgTarget : 'side',
								labelAlign : 'right'
							},
							bodyStyle : {
								padding : '18px 15px'
							},
							fieldDefaults : {
								anchor : '100%'
							},
							items : [{
										labelWidth : 50,
										fieldLabel : '名称',
										allowBlank : false,
										name : 'title'
									}],
							dockedItems : [{
										xtype : 'toolbar',
										dock : 'bottom',
										ui : 'footer',
										items : [{
													iconCls : 'icon-user-add',
													text : '添加',
													scope : this,
													handler : this.onCreate
												}, {
													iconCls : 'icon-reset',
													text : '重置',
													scope : this,
													handler : this.onReset
												}]
									}]
						});
				this.callParent(arguments);
			},
			listeners : {
				create : function(form, data, node) {
					node.appendChild({
								text : data['title'],
								id : Ext.id(),
								comments : data['title'],
								children : [],
								appear : false,
								expanded : true
							});
					form.up('window').close();
				}
			},
			onCreate : function() {
				var parentNode = this.getParentNode();
				var form = this.getForm();
				if (form.isValid()) {
					this.fireEvent('create', this, form.getValues(), parentNode);
					form.reset();
				}
			},
			onReset : function() {
				this.getForm().reset();
			},
			getParentNode : function() {
			}
		})