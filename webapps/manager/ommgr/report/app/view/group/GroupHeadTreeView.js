Ext.define('XI.view.group.GroupHeadTreeView', {
	extend : 'Ext.tree.Panel',
	alias : 'widget._groupheadtreeview',
	rootVisible : true,
	animate : true,
	store : 'group.GroupHeadTreeStore',
	viewConfig : {
		plugins : {
			ptype : 'treeviewdragdrop',
			enableDrag : true,
			enableDrop : true,
			appendOnly : true
		}
	},
	listeners : {
		itemcontextmenu : function(his, record, item, index, e) {
			if (!record.isLeaf()) {
				e.preventDefault();
				e.stopEvent();
				var nodemenu = new Ext.menu.Menu({
					items : [{
						text : '添加合并表头title',
						handler : function() {
							Ext.create('widget.window', {
										closable : true,
										closeAction : 'hide',
										title : '创建合并表头title',
										border : false,
										width : 300,
										minWidth : 200,
										height : 140,
										modal : true,
										layout : {
											type : 'fit',
											padding : 5
										},
										items : [{
													xtype : '_groupheadformview',
													id : 'groupheadformview_',
													getParentNode : function() {
														return record;
													},
													split : true
												}]
									}).show();
						}
					}, {
						text : '删除合并表头title',
						handler : function() {
							if (!record.hasChildNodes()) {
								record.remove();
							} else {
								Ext.MessageBox.alert('提示：', '有子节点不能被删除！')
							}
						}
					}]
				});
				nodemenu.showAt(e.getXY());
			}
		}
	}
});