Ext.define('XI.view.group.GroupLayoutView', {
			extend : 'Ext.panel.Panel',
			alias : 'widget._grouplayoutview',
			layout : 'border',
			defaults : {

				split : true
			},
			items : [{
						region : 'center',
						layout : 'border',
						items : [{
									title : '设置分组字段',
									region : 'north',
									xtype : '_groupbygridview',
									id : 'groupbygridview_',
									height : '35%'
								}, {
									title : '设置分组条件',
									region : 'center',
									xtype : '_grouphavinggridview',
									id : 'grouphavinggridview_'
								}, {
									title : '设置聚合字段',
									xtype : '_groupappregategridview',
									id : 'groupappregategridview_',
									region : 'south',
									height : '40%'
								}]
					}, {
						region : 'east',
						width : '30%',
						layout : 'border',
						items : [{
									region : 'center',
									title : '设置分组合并表头',
									xtype : '_groupheadtreeview',
									id : 'mixHead_'
								}, {
									title : '设置图表',
									region : 'south',
									height : 300,
									xtype : '_groupchartformview',
									id : 'groupchartformview_'
								}]
					}]
		})