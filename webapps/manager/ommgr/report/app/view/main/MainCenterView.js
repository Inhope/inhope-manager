Ext.define('XI.view.main.MainCenterView', {
	extend : 'Ext.tab.Panel',
	width : '100%',
	height : '100%',
	alias : 'widget._maincenterview',
	border : false,
	items : [{
				title : '基础设置',
				autoScroll : true,
				border : false,
				xtype : '_baselayoutview',
				id : 'baselayoutview_'
			}, {
				title : '分组设置',
				autoScroll : true,
				border : false,
				xtype : '_grouplayoutview',
				listeners: { 
					activate: function(tab) {
						if (tab.title = '分组设置') {
							var reportUtil = Ext.create('XI.util.ExtUtil');
							var mixHeadRootNode_ = Ext.getCmp('mixHead_').getStore()
									.getRootNode();
							var res = reportUtil.hasLeafChildNode(mixHeadRootNode_);
							if (res) {
								var chart_ = Ext.getCmp('groupchartformview_');
									chart_.onSetComboState(0);
									chart_.child('#isChart_').items.items[1].setValue(0);
								}
						}
					}
				},
				id : 'grouplayoutview_'
			}, {
				title : '自定义sql',
				autoScroll : true,
				border : false,
				xtype : '_defineformview',
				id : 'defineformview_'
			}],
	buttons : [{
		text : '确定生成报表',
		handler : function(btn) {

			// mixHead_
			var reportUtil = Ext.create('XI.util.ExtUtil');
			var mixHeadRootNode_ = Ext.getCmp('mixHead_').getStore().getRootNode();
			var res = reportUtil.hasLeafChildNode(mixHeadRootNode_);
			if (res) {
				Ext.MessageBox.alert('提示：', '合并表头中还没有设置需要显示的字段，请设置后再提交！');
				return;
			}
			res = reportUtil.validateMixHead(mixHeadRootNode_);
			if (!res) {
				Ext.MessageBox.alert('提示：', '新建表头中存在空的非叶子节点，请删除后再提交！');
				return;
			}
			var tableStr = Ext.getCmp('maintable_');
			if (!tableStr.getValue()) {
				Ext.MessageBox.alert('提示：', '请设置主表！');
				return;
			}
			var menuStr = Ext.getCmp('reportname_');
			if (!menuStr.getValue()) {
				Ext.MessageBox.alert('提示：', '请填写报表名称！');
				return;
			}

			// join
			var basegridjoinview_ = Ext.getCmp('basegridjoinview_');
			var joinArr = [];
			basegridjoinview_.getStore().each(function(rec) {
						joinArr.push(rec.data);
					});
			// where
			var basegridwhereview_ = Ext.getCmp('basegridwhereview_');
			var whereArr = [];
			basegridwhereview_.getStore().each(function(rec) {
						whereArr.push(rec.data);
					});
			// groupby
			var groupbygridview_ = Ext.getCmp('groupbygridview_');
			var groupbyArr = [];
			groupbygridview_.getStore().each(function(rec) {
						groupbyArr.push(rec.data);
					});
			// having
			var grouphavinggridview_ = Ext.getCmp('grouphavinggridview_');
			var havingArr = [];
			grouphavinggridview_.getStore().each(function(rec) {
						havingArr.push(rec.data);
					});
			// aggregate
			var groupappregategridview_ = Ext.getCmp('groupappregategridview_');
			var aggregateArr = [];
			groupappregategridview_.getStore().each(function(rec) {
						aggregateArr.push(rec.data);
					});

			var mixHeadArr = reportUtil.doBuildMixJson(mixHeadRootNode_);
			
			// chart
			var chart = Ext.getCmp('groupchartformview_');
			var obj = chart.getValues();
			if (obj.isChart == 1) {
				if (obj.charttype == '' || obj.chartx == '' || obj.charty == '') {
					Ext.MessageBox.alert('提示：', '请完整填写图表配置！');
					return;
				}
			}

			Ext.Ajax.request({
						url : 'report!storage.action',
						params : {
							tableStr : tableStr.getValue(),
							menuStr : menuStr.getValue(),
							joinArr : Ext.JSON.encode(joinArr),
							whereArr : Ext.JSON.encode(whereArr),
							groupbyArr : Ext.JSON.encode(groupbyArr),
							havingArr : Ext.JSON.encode(havingArr),
							aggregateArr : Ext.JSON.encode(aggregateArr),
							mixHeadArr : Ext.JSON.encode(mixHeadArr),
							chartStr : Ext.JSON.encode(chart.getValues())
						},
						method : 'POST',
						success : function(response, options) {
							var json = Ext.JSON.decode(response.responseText);
							Ext.MessageBox.alert('提示:', json.message);
						},
						failure : function(response, options) {
							Ext.MessageBox.alert('失败', '请求超时或网络故障,错误编号：'
											+ response.status);
						}
					});
		}
	}]
});