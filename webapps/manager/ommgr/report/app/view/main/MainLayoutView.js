Ext.define('XI.view.main.MainLayoutView', {
			extend : 'Ext.panel.Panel',
			alias : 'widget._mainlayoutview',
			defaults : {
				split : true,
				bodyStyle : 'padding : 1px'
			},
			layout : 'border',
			items : [{
						title : '请选择数据表',
						region : 'west',
						width : 250,
						xtype : '_mainwestview',
						id : 'mainwestview_'
					}, {
						region : 'center',
						xtype : '_maincenterview',
						id : 'maincenterview_'
					}]
		})