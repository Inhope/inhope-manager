Ext.define('XI.view.main.MainWestView', {
			extend : 'Ext.tree.Panel',
			alias : 'widget._mainwestview',
			store : 'main.MainWestStore',
			rootVisible : false,
			draggable : true,
			autoScroll : true,
			animate : true,
			listeners : {
				checkchange : function(node, checked, options) {
					Ext.create("XI.util.ExtUtil").clearPanelData();
					var basewestview = Ext.getCmp('basewestview_');
					var nodesArr = this.getChecked();
					var paramStr = '', namesArr = [];
					if (nodesArr.length > 0) {
						for (i = 0; i < nodesArr.length; i++) {
							namesArr.push(nodesArr[i].data.text);
							paramStr += nodesArr[i].data.text + ',';
						}
					}
					basewestview.store.getProxy().extraParams = {
						'tablenames' : paramStr
					};
					basewestview.store.load();
					this.setMainTable(namesArr);
				}
			},
			setMainTable : function(namesArr) {
				var firstCombo = Ext.getCmp('maintable_');
				var dataObjArr = [];
				for (var i in namesArr) {
					var dataObj = {};
					dataObj['name'] = namesArr[i];
					dataObj['value'] = namesArr[i];
					dataObjArr.push(dataObj);
				}
				var comboStore = firstCombo.getStore();
				comboStore.loadData(dataObjArr);
			}
		})