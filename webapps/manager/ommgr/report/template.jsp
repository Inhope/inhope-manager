<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${menuStr }</title>
<script src="<c:url value="/ext4/ext-all.js"/>" type="text/javascript"></script>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/ext4/resources/css/ext-all-neptune.css"/>" />
<script src="<c:url value="/ext4/locale/ext-lang-zh_CN.js"/>"
	type="text/javascript"></script>
<script
	src="<c:url value="/ommgr/report/app/util/Exporter2Excel-all.js"/>"
	type="text/javascript"></script>
<script type="text/javascript">
		Ext.onReady(function() {
			
			function dynBuildWindow(store, charttype_, chartx_, charty_) {
				return Ext.create('widget.window', {
	                closable: true,
	                modal : true,
	                title : '查看图表',
	                closeAction: 'hide',
	                width: 800,
	                minWidth: 350,
	                height: 500,
	                layout: 'fit',
	                items: dynBuildChart(store, charttype_, chartx_, charty_)
	            });
			}
			
			function dynBuildChart(store, charttype_, chartx_, charty_) {
				return Ext.create('Ext.chart.Chart', {
		            style: 'background:#fff',
		            animate: true,
		            shadow: true,
		            store: store,
		            axes: [{
		                type: 'Numeric',
		                position: 'left',
		                fields: charty_,
		                grid: true,
		                minimum: 0
		            }, {
		                type: 'Category',
		                position: 'bottom',
		                label: { rotate: { degrees: 315} },
		                fields: chartx_
		            }],
		            series: dynBuildSeries(charttype_, chartx_, charty_)
		        });
			}
			
			function dynBuildSeries(charttype_, chartx_, charty_) {
				var objArr = []
				if(charttype_ == 'column') {
					var yObj = {
						type: charttype_,
                        xField: chartx_,
                        yField: charty_,
                        label: {
                            contrast: true,
                            display: 'insideEnd',
                            field: charty_,
                            color: '#000',
                            orientation: 'vertical',
                            'text-anchor': 'middle'
                        }
                    }
					objArr.push(yObj);
				} else if(charttype_ == 'line') {
					for (var i in charty_) {
						var yObj = {
							type: charttype_,
							tips: {
                                trackMouse: true,
                                width: 140,
                                height: 28,
                                renderer: function(storeItem, item) {
                                  this.setTitle(storeItem.get(chartx_) + ' : ' + storeItem.get(charty_[i]));
                                }
                            },
                            xField: chartx_,
                            yField: charty_[i]
                        }
						objArr.push(yObj);
					}
				}
				return objArr;
			}
			
<%---------------------------------------------------------------------------------------------------%>
		
			Ext.define('GModel', {
			     extend: 'Ext.data.Model',
			     fields: ${fields}
			 });
			 
			var store = Ext.create('Ext.data.Store', {
				model : 'GModel',
				pageSize : 30,
				proxy: {
			         type: 'ajax',
			         url: 'report!renderGrid.action',
			         extraParams : {
			         	menuId : '${menuId}',
			         	isgroup : ${isgroup}
			         },
			         reader: {
			             type: 'json',
			             root: 'data',
			             totalProperty: 'total'
			         }
			     }
			}); 
			
			var grid = Ext.create('Ext.grid.Panel', {
				columns : ${columns},
				id : 'grid',
				columnLines : true,
				store : store,
				forceFit : true,
				displayInfo : true,
				loadMask : true,
				emptyMsg : '没有数据显示',
				dockedItems : [
				{
					store : store,
					id : 'page',
					xtype : 'pagingtoolbar',
					dock : 'bottom',
					displayInfo : true
				}, 
				{
                    xtype : 'toolbar',
                    dock : 'top',
                    defaultType : 'combobox',
                    defaults : {
                    	allowBlank : false,
        				labelSeparator : " ： ",
        				labelWidth : 70,
        				queryMode : 'local',
						displayField : 'name',
						valueField : 'value',
						triggerAction : 'all',
        				editable : false
        			},
                    items : [{
                    	xtype : 'button',
                    	text : '导出EXCEL',
                    	handler :  function(btn, me) {
				            var grid= btn.up('panel');
				            var url = getExcelUrl.getExcelUrl(grid, '${menuStr }', '', null);
				            window.location = url;
					    }
                    }
                    <c:if test="${ischart eq null || ischart eq '' || ischart eq 0}"> 
                    , '->',
                    {
						fieldLabel : '图表类型',
						emptyText : '选择图表类型',
						name : 'charttype',
						store : [['column', '柱状图'], ['line', '折线图']],
						id : 'charttype_'
					}, {
						fieldLabel : 'X轴字段',
						emptyText : '请选择X轴字段',
						name : 'chartx',
						store : ${fields},
						id : 'chartx_'
					}, {
						fieldLabel : 'Y轴字段',
						emptyText : '请选择Y轴字段',
						multiSelect: true,
						name : 'charty',
						store : ${fields},
						id : 'charty_'
					}, {
						id : 'excute-chart',
						xtype : 'button',
               	        text: '生成图表',
               	        handler: function () {
	           				var charttype_ = this.up('toolbar').child('#charttype_');
	           				var chartx_ = this.up('toolbar').child('#chartx_');
	           				var charty_ = this.up('toolbar').child('#charty_');
	           				if(charttype_.isValid() && chartx_.isValid() && charty_.isValid()) {
	           					var win = dynBuildWindow(store, charttype_.getValue(), chartx_.getValue(), charty_.getValue());
	               				win.show();
	           				}
	           	        }
               	    }
                    </c:if>
                    ]
                }]
			});

			store.load();
			
			<c:choose>
			    <c:when test="${ischart ne '' && ischart eq 1}">
		        var chart = Ext.create('Ext.chart.Chart', {
	                        style : 'background:#fff',
	                        animate : true, 
	                        store : store,
	                        axes : [{
	                            type: 'Numeric', 
	                            position: 'left', 
	                            fields: ${charty}, 
	                            grid: true 
	                        }, {
	                            type: 'Category', 
	                            position: 'bottom', 
	                            fields: ['${chartx}'],
	                            label: { rotate: { degrees: 315} }
	                        }],
	                        legend : {
	                            position: 'bottom'
	                        },
	                        series : [
							<c:if test="${charttype eq 'line'}">       
							<c:forEach items='${chartyArr}' var='y'>           
                            {
	                            type: '${charttype}',
	                            tips: {
	                                trackMouse: true,
	                                width: 140,
	                                height: 28,
	                                renderer: function(storeItem, item) {
	                                  this.setTitle(storeItem.get('${chartx}') + ': ' + storeItem.get('${y}'));
	                                }
	                            },
	                            markerConfig: {
	                                type: 'circle',
	                                size: 4,
	                                radius: 4,
	                                'stroke-width': 0
	                            },
	                            xField: '${chartx}',
	                            yField: '${y}'
	                        },
                            </c:forEach></c:if>
                            <c:if test="${charttype eq 'column'}">       
                            {
	                            type: '${charttype}',
	                            xField: '${chartx}',
	                            yField: ${charty},
	                            label: {
	                                contrast: true,
	                                display: 'insideEnd',
	                                field: ${charty},
	                                color: '#000',
	                                orientation: 'vertical',
	                                'text-anchor': 'middle'
	                            }
	                        }
                            </c:if>
                            ]
		        });
				var viewport = new Ext.Viewport({
					layout : 'border',
					items : [{
						region : 'north',
						layout:'fit',
						items : grid,
						height : '50%'
					},{
						region : 'center',
						layout:'fit',
						bodyStyle : 'padding : 10px', 
	                    maximizable: true,
	                    items: chart
					}]
				});				
				</c:when>
				<c:otherwise>
				var viewport = new Ext.Viewport({
					layout : 'fit',
					items : [grid]
				});
				</c:otherwise>
			  </c:choose>
		});
	</script>
</head>
<body>
</body>
</html>