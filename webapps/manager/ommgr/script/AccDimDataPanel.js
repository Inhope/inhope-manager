AccDimDataPanel = function(_cfg) {
	var self = this;
	this.statBy = null;

	var fields = [ {
		name : 'countItem'
	}, {
		name : 'totalAcc'
	}, {
		name : 'totalPeo'
	}, {
		name : 'totalQue'
	}, {
		name : 'totalTime'
	}, {
		name : 'typeFinal'
	}, {
		name : 'typeFinalList'
	}, {
		name : 'typeCommonChat'
	}, {
		name : 'typeIllegalWord'
	}, {
		name : 'typeRepeat'
	}, {
		name : 'typeNull'
	}, {
		name : 'typeMisinput'
	}, {
		name : 'typeSimple'
	}, {
		name : 'typeWrongSpell'
	}, {
		name : 'typeCustomChat'
	}, {
		name : 'typeExpired'
	}, {
		name : 'couBizQue'
	}, {
		name : 'turnAtr'
	}, {
		type : 'int',
		name : 'totalNewUser'
	}, {
		type : 'int',
		name : 'totalActiveUser'
	}, {
		type : 'int',
		name : 'totalSuggestedInput'
	}, {
		type : 'int',
		name : 'typeSuggested'
	} , {
		name : 'groupName'
	}];
	var rowIndex = new Ext.grid.RowNumberer();
	var _renderer = function(v, m, rec, type, requireNum) {
		var hasQuesType = typeof dataPanel.quesType != 'undefined' && dataPanel.quesType != -1;
		if (hasQuesType && type != 'turnAtr')
			v = rec.json.quesTypeNums[type][dataPanel.quesType];
		v = parseInt(v);
		if (requireNum || dataPanel.display != 'rate')
			return v;
		var total = 0;
		if (hasQuesType)
			total = parseInt(rec.json.quesTypeNums['couBizQue'][dataPanel.quesType]);
		else
			total = parseInt(rec.get('couBizQue'));
		if (type == 'couBizQue' || type == 'turnAtr')
			total += parseInt(rec.get('turnAtr'));
		if (total == 0)
			return '0.0%';
		var vs = ((v / total) * 100).toString();
		if (vs.indexOf('.') == -1)
			vs += '.0';
		return vs.substr(0, vs.indexOf('.') + 2) + '%';
	};
	var _chartStore = new Ext4.data.JsonStore({
		fields : [ 'type', 'total' ],
		data : []
	});
	var _quesTypes = [ '全数字', '全字母', '字母数字', '含中文' ];
	window.__showQuesTypeChart = function(d, t) {
		_chartStore.removeAll(true);
		var recs = [];
		Ext4.regModel('data', {
			fields : [ {
				name : 'type',
				type : 'string'
			}, {
				name : 'total',
				type : 'int'
			} ]
		});
		for ( var i = 0; i < d.length; i++) {
			var types = _quesTypes[i];
			var totals = d[i];
			if (totals > 0) {
				var data = Ext4.ModelMgr.create({
					type : types,
					total : totals
				}, 'data');
				recs.push(data);
			}
		}
		_chartStore.add(recs);
		var chartPanel = new Ext4.extend(Ext4.panel.Panel, {
			height : 450,
			initComponent : function() {
				var self = this;
				var donut = false;
				this.dimChart = new Ext4.chart.Chart({
					animate : true,
					store : _chartStore,
					shadow : true,
					legend : {
						position : 'right'
					},
					insetPadding : 60,
					theme : 'Base:gradients',
					series : [ {
						type : 'pie',
						field : 'total',
						showInLegend : true,
						donut : donut,
						tips : {
							trackMouse : true,
							width : 240,
							height : 28,
							renderer : function(storeItem, item) {
								var total = 0;
								_chartStore.each(function(rec) {
									total += rec.get('total');
								});
								var percent = '' + storeItem.get('total') / total * 100;
								var idx = percent.indexOf('.');
								if (idx != -1)
									percent = percent.substring(0, idx + 2);
								this.setTitle(storeItem.get('type') + ': ' + percent + '%(数量:' + storeItem.get('total') + ')');
							}
						},
						highlight : {
							segment : {
								margin : 20
							}
						},
						label : {
							field : 'type',
							display : 'rotate',
							contrast : true,
							font : '18px Arial',
							renderer : function(value, label, storeItem, item, i, display, animate, index) {
								var total = 0;
								_chartStore.each(function(rec) {
									total += rec.get('total');
								});
								var a = Math.round(storeItem.get('total') / total * 100);
								if (a < 5)
									return "";
								else
									return value;
							}
						}
					} ]
				});

				this.items = self.dimChart;
				this.tbar = [ {
					enableToggle : true,
					pressed : false,
					text : '圆环图',
					toggleHandler : function(btn, pressed) {
						self.dimChart.series.first().donut = pressed ? 35 : false;
						self.dimChart.refresh();
					}
				} ];
				chartPanel.superclass.initComponent.call(this);
			},
			// ui
			_buildTbar : function() {
			},
			layout : 'fit',
			border : false
		});
		var dimPanel = new chartPanel();
		var _chartWin = new Ext4.Window({
			title : '问题类型分布图 - ' + t,
			height : 400,
			width : 500,
			closeAction : 'hide',
			closable : true,
			layout : 'fit',
			region : 'center',
			maximizable : true,
			draggable : true,
			shadow : false,
			frame : true,
			shim : false,
			animCollapse : true,
			constrainHeader : true,
			collapsible : true,
			modal : false,
			plain : true,
			shim : true,
			items : [ dimPanel ]
		});
		_chartWin.show();
	}

	var curarray = null;

	var rangeFromTf = this.rangeFromTf = new Ext4.form.TextField({
		xtype : 'textfield',
		ref : 'rangeFrom',
		width : 40
	})

	var rangeToTf = this.rangeToTf = new Ext4.form.TextField({
		xtype : 'textfield',
		ref : 'rangeTo',
		width : 40
	})

	function parseValue(v) {
		if (v.indexOf("-") >= 0) {
			var start = v.split("-")[0];
			var end = v.split("-")[1];
			if (start != null && "" != start && !(end != null && "" != end)) {
				v = ">=" + start;
			} else if (end != null && "" != end && !(start != null && "" != start)) {
				v = "<=" + end;
			}
		}
		return v;
	}

	function refreshTbar(cPanel, _arrayObj) {
		var ct = cPanel.dockedItems.items[0].removeAll();
		if (_arrayObj != null && _arrayObj.length > 0) {
			for ( var n = 0; n < _arrayObj.length; n++) {
				var v = parseValue("" + _arrayObj[n]);

				cPanel.dockedItems.items[0]
						.add({
							text : "<span>"
									+ v
									+ "<span style=\"background-image: url('../ommgr/images/cross.gif'); background-repeat:no-repeat;  width:15px; height:15px; float:right;\"></span></span>",
							enableToggle : true,
							pressed : true,
							stateId : n,
							handler : function() {
								_arrayObj.splice(this.stateId, 1);
								refreshTbar(cPanel, _arrayObj);
							}
						});
			}
		}
	}

	var sDimarr = null;
	var cDimPanel = null;
	window.__showUserDimRatChart = function(_curarray, _title, _sarr) {
		curarray = _curarray;
		_chartStore.removeAll(true);
		var recs = [];
		Ext4.regModel('data', {
			fields : [ {
				name : 'type',
				type : 'string'
			}, {
				name : 'total',
				type : 'int'
			} ]
		});
		if (_sarr == null || _sarr.length == 0)
			sDimarr = [ "1-1", "2-5", "6-10", "10-" ];
		if (sDimarr != null && sDimarr.length > 0) {
			for ( var _index = 0; _index < sDimarr.length; _index++) {
				var v = "" + sDimarr[_index];
				var totals = 0;
				if (v.indexOf("-") >= 0) {
					var start = v.split("-")[0];
					var end = v.split("-")[1];
					for ( var key in curarray) {
						if (start != null && "" != start) {
							if (Number(key) < Number(start)) {
								continue;
							}
						}
						if (end != null && "" != end) {
							if (Number(key) > Number(end)) {
								continue;
							}
						}
						totals += curarray[key];
					}
				} else {
					for ( var key in d) {
						if (key == v) {
							totals += d[key];
						}
					}
				}
				var types = parseValue(v) + "次";
				if (totals > 0) {
					var data = Ext4.ModelMgr.create({
						type : types,
						total : totals
					}, 'data');
					recs.push(data);
				}
			}
		}
		_chartStore.add(recs);

		var chartPanel = new Ext4.extend(Ext4.panel.Panel, {
			height : 450,
			initComponent : function() {
				var self = this;
				var donut = false;
				this.dataChart = new Ext4.chart.Chart({
					animate : true,
					store : _chartStore,
					shadow : true,
					region : 'center',
					legend : {
						position : 'right'
					},
					insetPadding : 60,
					theme : 'Base:gradients',
					series : [ {
						type : 'pie',
						field : 'total',
						showInLegend : true,
						donut : donut,
						tips : {
							trackMouse : true,
							width : 240,
							height : 28,
							renderer : function(storeItem, item) {
								var total = 0;
								_chartStore.each(function(rec) {
									total += rec.get('total');
								});
								var percent = '' + storeItem.get('total') / total * 100;
								var idx = percent.indexOf('.');
								if (idx != -1)
									percent = percent.substring(0, idx + 2);
								this.setTitle(storeItem.get('type') + ': ' + percent + '%(数量:' + storeItem.get('total') + ')');
							}
						},
						highlight : {
							segment : {
								margin : 20
							}
						},
						label : {
							field : 'type',
							display : 'rotate',
							contrast : true,
							font : '18px Arial',
							renderer : function(value, label, storeItem, item, i, display, animate, index) {
								var total = 0;
								_chartStore.each(function(rec) {
									total += rec.get('total');
								});
								var percent = '' + storeItem.get('total') / total * 100;
								var idx = percent.indexOf('.');
								if (idx != -1)
									percent = percent.substring(0, idx + 2);
								return percent + "%";
							}
						}
					} ]
				});
				this.items = self.dataChart;
				this.tbar = {
					xtype : 'toolbar',
					height : 32,
					items : []
				};
				chartPanel.superclass.initComponent.call(this);
			},
			// ui
			_buildTbar : function() {
			},
			layout : 'border',
			border : false
		});

		cDimPanel = new chartPanel();

		refreshTbar(cDimPanel, sDimarr);
		var existRatChar = Ext4.getCmp("ratDimchartWin");
		if (existRatChar) {
			existRatChar.items.items[0] = cDimPanel;
			existRatChar.setTitle('活跃度分布图 - ' + _title);
			existRatChar.show();
			existRatChar.doLayout();
			return;
		}

		var _chartWin = this._chartWin = new Ext4.Window(
				{
					id : 'ratDimchartWin',
					title : '活跃度分布图 - ' + _title,
					height : 400,
					width : 500,
					closeAction : 'hide',
					closable : true,
					layout : 'fit',
					region : 'center',
					maximizable : true,
					draggable : true,
					shadow : false,
					frame : true,
					shim : false,
					animCollapse : true,
					constrainHeader : true,
					collapsible : true,
					modal : false,
					plain : true,
					shim : true,
					tbar : [
							rangeFromTf,
							'~',
							rangeToTf,
							{
								text : "<span style=\"background-image: url('../ommgr/images/add.gif'); background-repeat:no-repeat;  width:15px; height:15px; float:right;\"></span>",
								xtype : 'button',
								// 添加
								handler : function() {
									var rangeFrom = self.rangeFromTf.getValue();
									var rangeTo = self.rangeToTf.getValue();

									var rangeFlag = false;
									if (rangeFrom != null && "" != rangeFrom)
										if (isNaN(rangeFrom)) {
											Dashboard.setAlert("请输入数字");
											return;
										} else {
											rangeFrom = Number(rangeFrom);
											rangeFlag = true;
										}

									if (rangeTo != null && "" != rangeTo)
										if (isNaN(rangeTo)) {
											Dashboard.setAlert("请输入数字");
											return;
										} else {
											rangeTo = Number(rangeTo);
											rangeFlag = true;
										}
									if (rangeFlag) {
										if (rangeFrom != null && "" != rangeFrom && rangeTo != null && "" != rangeTo && rangeFrom > rangeTo) {
											Dashboard.setAlert("后面的数字必须大于前面的数字");
											return;
										}
										self.rangeFromTf.setValue("");
										self.rangeToTf.setValue("");
										sDimarr.push(rangeFrom + "-" + rangeTo);
										refreshTbar(cDimPanel, sDimarr);
									}

								}
							}, '-', {
								text : '搜索',
								xtype : 'button',
								iconCls : 'icon-search',
								handler : function() {
									__showUserDimRatChart(curarray, _title, sDimarr);
								}
							}, '-', {
								enableToggle : true,
								pressed : false,
								text : '圆环图',
								toggleHandler : function(btn, pressed) {
									cDimPanel.dataChart.series.first().donut = pressed ? 35 : false;
									cDimPanel.dataChart.refresh();
								}
							} ],
					items : [ cDimPanel ]
				});
		_chartWin.show();
	};

	var _getCountItem = function(v) {
		return v;
	}
	var _isQuesTypeChosen = function() {
		return typeof dataPanel.quesType != 'undefined' && dataPanel.quesType != -1;
	}
	var _getChartLink = function(v, rec, type) {
		var title = '';
		for ( var i = 0; i < columns.length; i++) {
			if (type == columns[i].dataIndex) {
				title = columns[i].header;
			}
		}
		title = _getCountItem(rec.get('countItem')) + ' - ' + title;
		return '<span style="text-decoration:underline;color:blue;cursor:pointer" onclick="__showQuesTypeChart([' + rec.json.quesTypeNums[type]
				+ '],\'' + title + '\')">' + v + '</span>'
	}
	var _getUserRatChartLink = function(v, rec, type) {
		var map = rec.json.userRatMap;
		if (map == null) {
			return v;
		}
		var title = _getCountItem(rec.get('countItem'));
		var str = '';
		var arr = [];
		for ( var i in map)
			arr.push("'" + i + "':" + map[i]);
		str = '{' + arr.join(',') + '}';

		return '<span style="text-decoration:underline;color:blue;cursor:pointer" onclick="__showUserDimRatChart(' + str + ',\'' + title
				+ '\',null)">' + v + '</span>'
	}
	var columns = [ /** rowIndex, */
	{
		dataIndex : 'countItem',
		header : '统计项',
		align : 'center',
		width : 80
	}, {
		dataIndex : 'totalAcc',
		header : Dashboard.getColumnTitle('总会话数'),
		align : 'center',
		width : 70
	}, {
		dataIndex : 'totalQue',
		header : Dashboard.getColumnTitle('总提问数'),
		align : 'center',
		width : 70,
		renderer : function(v, meta, rec) {
			if (_isQuesTypeChosen())
				v = parseInt(rec.json.quesTypeNums['couBizQue'][dataPanel.quesType]) + parseInt(rec.get('turnAtr'));
			return v;
		}
	}, {
		dataIndex : 'totalTime',
		header : Dashboard.getColumnTitle('独立用户数'),
		align : 'center',
		width : 70
	}, {
		dataIndex : 'totalNewUser',
		header : Dashboard.getColumnTitle('新增用户数'),
		align : 'center',
		width : 70
	}, {
		dataIndex : 'totalActiveUser',
		header : Dashboard.getColumnTitle('活跃用户数'),
		align : 'center',
		width : 70,
		renderer : function(v, meta, rec) {
			return _getUserRatChartLink(v, rec, 'userRatMapList');
		}
	}, {
		dataIndex : 'totalPeo',
		header : Dashboard.getColumnTitle('总登录人数'),
		align : 'center',
		width : 80
	}, {
		dataIndex : 'typeFinal',
		header : Dashboard.getColumnTitle('标准回复'),
		align : 'center',
		width : 60,
		renderer : function(v, meta, rec) {
			if (!_isQuesTypeChosen() && dataPanel.display != 'rate')
				return _getChartLink(v, rec, 'typeFinal');
			else
				return _renderer(v, meta, rec, 'typeFinal');
		}
	}, {
		dataIndex : 'typeFinalList',
		header : Dashboard.getColumnTitle('指令回复'),
		align : 'center',
		width : 60,
		renderer : function(v, meta, rec) {
			if (!_isQuesTypeChosen() && dataPanel.display != 'rate')
				return _getChartLink(v, rec, 'typeFinalList');
			else
				return _renderer(v, meta, rec, 'typeFinalList');
		}
	}, {
		dataIndex : 'typeCustomChat',
		header : Dashboard.getColumnTitle('定制聊天'),
		align : 'center',
		width : 70,
		hidden : true
	}, {
		dataIndex : 'typeCommonChat',
		header : Dashboard.getColumnTitle('聊天'),
		align : 'center',
		width : 60,
		renderer : function(v, meta, rec) {
			if (!_isQuesTypeChosen() && dataPanel.display != 'rate')
				return _getChartLink(v, rec, 'typeCommonChat');
			else
				return _renderer(v, meta, rec, 'typeCommonChat');
		}
	}, {
		dataIndex : 'typeIllegalWord',
		header : Dashboard.getColumnTitle('敏感词'),
		align : 'center',
		width : 60,
		renderer : function(v, meta, rec) {
			if (!_isQuesTypeChosen() && dataPanel.display != 'rate')
				return _getChartLink(v, rec, 'typeIllegalWord');
			else
				return _renderer(v, meta, rec, 'typeIllegalWord');
		}
	}, {
		dataIndex : 'typeMisinput',
		header : Dashboard.getColumnTitle('其它'),
		align : 'center',
		width : 60,
		renderer : function(v, meta, rec) {
			if (!_isQuesTypeChosen() && dataPanel.display != 'rate')
				return _getChartLink(v, rec, 'typeMisinput');
			else
				return _renderer(v, meta, rec, 'typeMisinput');
		}
	}, {
		dataIndex : 'typeNull',
		header : Dashboard.getColumnTitle('默认回复'),
		align : 'center',
		width : 60,
		renderer : function(v, meta, rec) {
			if (!_isQuesTypeChosen() && dataPanel.display != 'rate')
				return _getChartLink(v, rec, 'typeNull');
			else
				return _renderer(v, meta, rec, 'typeNull');
		}
	}, {
		dataIndex : 'succRate',
		header : Dashboard.getColumnTitle('成功率'),
		align : 'center',
		width : 60,
		renderer : function(v, meta, rec) {
			var typeNull = _renderer(rec.get('typeNull'), meta, rec, 'typeNull', true);
			var typeMisc = _renderer(rec.get('typeMisinput'), meta, rec, 'typeMisinput', true);
			var excludeMisc = Dashboard.miscConfig.excludeMiscInAccuracy;
			var rst = '', total = 0, suggestedInput = 0, typeSuggested = 0;
			if (_isQuesTypeChosen()) {
				total = parseInt(rec.json.quesTypeNums['couBizQue'][dataPanel.quesType]);
				var suggestedInput0 = rec.json.quesTypeSuggestedInput[dataPanel.quesType];
				if (suggestedInput0)
					suggestedInput = suggestedInput0;
				var typeSuggested0 = parseInt(rec.json.quesTypeNums['typeSuggested'][dataPanel.quesType]);
				if (typeSuggested0)
					typeSuggested = typeSuggested0;
			} else {
				total = parseInt(rec.get('couBizQue'));
				var suggestedInput0 = rec.get('totalSuggestedInput');
				if (suggestedInput0)
					suggestedInput = suggestedInput0;
				var typeSuggested0 = rec.get('typeSuggested');
				if (typeSuggested0)
					typeSuggested = typeSuggested0;
			}
			if (total == 0)
				return '0.0%';
			if (excludeMisc) {
				var suggestedNum = typeSuggested > suggestedInput ? suggestedInput : typeSuggested;
				typeNull += (typeMisc - suggestedNum);
			}
			var vs = ((typeNull / total) * 100).toString();
			if (vs.indexOf('.') == -1)
				vs += '.0';
			rst = 100 - parseFloat(vs.substr(0, vs.indexOf('.') + 2));
			rst = rst.toString();
			if (rst.indexOf('.') != -1)
				rst = rst.substr(0, rst.indexOf('.') + 2);
			else
				rst += '.0';
			return rst + '%';
		}
	}, {
		dataIndex : 'couBizQue',
		header : Dashboard.getColumnTitle('机器人提问数'),
		align : 'center',
		width : 90,
		renderer : function(v, meta, rec) {
			if (!_isQuesTypeChosen() && dataPanel.display != 'rate')
				return _getChartLink(v, rec, 'couBizQue');
			else
				return _renderer(v, meta, rec, 'couBizQue');
		}
	}, {
		dataIndex : 'turnAtr',
		header : Dashboard.getColumnTitle('转人工提问数'),
		align : 'center',
		width : 90,
		renderer : function(v, meta, rec) {
			return _renderer(v, meta, rec, 'turnAtr');
			;
		}
	}, {
		dataIndex : 'groupName',
		header : '分组',
		align : 'center',
		hidden : true,
		width : 40
	}];
	var data = [];

	var _store = new Ext.data.GroupingStore({
		proxy : new Ext.data.HttpProxy({
			url : 'cou-access-data!list.action'
		}),
		reader : new JsonReaderEx({
			idProperty : 'id',
			root : 'data',
			fields : fields
		}),
		groupField:'groupName'
	});

	var _dimNames = Dashboard.dimensionNames;
	var _statItems = [];
	var dims = [ 'platform', 'location', 'brand', 'custom1', 'custom2', 'custom3' ];
	for ( var i = 0; i < dims.length; i++) {
		var dimName = _dimNames[dims[i]];
		if (dimName) {
			_statItems.push([ dims[i] + '_d', dimName + '(天)' ]);
			_statItems.push([ dims[i] + '_m', dimName + '(月份)' ]);
			_statItems.push([ dims[i] + '_p', dimName + '(区间)' ]);
		}
	}
//	var _statItems = [ [ 6, _dimNames.platform + "(天)" ], [ 4, _dimNames.platform + "(月)" ], [ 20, _dimNames.platform + "(区间)" ] ];
//	if (_dimNames.location) {
//		_statItems.push([ 7, _dimNames.location + "(天)" ]);
//		_statItems.push([ 5, _dimNames.location + "(月)" ]);
//		_statItems.push([ 21, _dimNames.location + "(区间)" ]);
//	}

	var queryByItem = new Ext.form.ComboBox({
		fieldLabel : '统计项',
		hiddenName : 'queryByItem',
		valueField : 'id',
		displayField : 'text',
		mode : 'local',
		editable : false,
		typeAhead : true,
		loadMask : true,
		allowBlank : false,
		selectOnFocus : false,
		triggerAction : 'all',
		emptyText : '请选择统计项',
		width : 120,
		store : new Ext.data.SimpleStore({
			fields : [ "id", "text" ],
			data : _statItems
		}),
		listeners : {
			'select' : function(tp) {
				self.checkItem();
			}
		}
	});

	this.initTime = function() {
		this.time = new Ext.form.DateField({
			name : 'timeText',
			xtype : 'datefield',
			fieldLabel : '选择日期',
			emptyText : '选择日期',
			format : 'Y-m-d',
			editable : false,
			width : 100
		})

	}

	this.initStartDay = function() {
		this.startDay = new Ext.form.DateField({
			name : 'startDay',
			xtype : 'datefield',
			fieldLabel : '起始时间',
			emptyText : '选择起始时间',
			format : 'Y-m-d',
			endDateField : 'endDay',
			editable : false,
			width : 100
		})
	}

	this.initEndDay = function() {
		this.endDay = new Ext.form.DateField({
			name : 'endDay',
			xtype : 'datefield',
			fieldLabel : '终止时间',
			emptyText : '选择终止时间',
			format : 'Y-m-d',
			startDateField : 'startDay',
			editable : false,
			width : 100
		})
	}

	this.initToMonth = function() {
		this.toMonth = new DateFieldEx({
			name : 'toMonth',
			xtype : 'datefield',
			fieldLabel : '月份',
			emptyText : '请选择月份',
			format : 'Y-m',
			editable : false,
			width : 100
		})
	}

	this.initChannel = function() {
		var channelStore = new ChannelStore({});
		channelStore.loadDatas();
		this.channel = new Ext.form.ComboBox({
			fieldLabel : _dimNames.platform,
			hiddenName : 'toChanel',
			valueField : 'id',
			displayField : 'name',
			emptyText : '请选择' + _dimNames.platform,
			mode : 'remote',
			editable : false,
			allowBlank : false,
			selectOnFocus : false,
			triggerAction : 'all',
			queryParam : 'platform',
			width : 110,
			store : channelStore
		});

	}

	this.initCity = function() {
		var cityStore = new CityStore({});
		cityStore.loadDatas();
		this.city = new Ext.form.ComboBox({
			fieldLabel : _dimNames.location,
			hiddenName : 'attCity',
			valueField : 'id',
			displayField : 'name',
			emptyText : '请选择' + _dimNames.location,
			mode : 'remote',
			editable : false,
			allowBlank : true,
			selectOnFocus : false,
			triggerAction : 'all',
			queryParam : 'location',
			width : 110,
			store : cityStore
		})
	}

	this.initDimChooser = function(excludeDims) {
		this.dimChooser = Dashboard.createDimChooser(150, excludeDims);
	};

	var queryButton = new Ext.Button({
		text : '统计',
		iconCls : 'icon-search',
		handler : function() {
			if (self.queryByItem.getValue() == null || self.queryByItem.getValue() == '') {
				Ext.Msg.alert('错误提示', '请选择统计项');
				return;
			}
			self.countData(self.queryByItem.getValue(), false);
		}

	});
	var exportButton = new Ext.SplitButton({
		text : '导出结果',
		menu : {
			defaults : {
				hideOnClick : false
			},
			items : [ {
				text : '问题类型筛选',
				checked : true,
				value : 1
			} ]
		},
		iconCls : 'icon-ontology-export',
		hidden : !Dashboard.u().allow('om.stat.tv.EXP'),
		handler : function() {
			var records = _store.getRange();
			if (records.length) {
				self.getEl().mask('正在导出数据...');
				var data = [];
				var quesTypeData = [];
				Ext.each(records, function(rec) {
					if(rec.json.quesTypeNums){
						data.push(rec.data);
						quesTypeData.push(Ext.encode(rec.json.quesTypeNums));
					}
				});
				var exportIdx = [];
				var colsConf = dataPanel.getColumnModel().config;
				for ( var k = 0; k < colsConf.length; k++) {
					if(!colsConf[k].hidden){
						exportIdx.push(colsConf[k].dataIndex);
					}
				}
				var month = '', day = '', startDay = '', endDay = '';
				if (self.toMonth && self.toMonth.getEl().dom && self.toMonth.getEl().dom.style.display != 'none')
					month = self.toMonth.getRawValue();
				if (self.time && self.time.getEl().dom && self.time.getEl().dom.style.display != 'none')
					day = self.time.getRawValue();
				if (self.startDay && self.startDay.getEl().dom && self.startDay.getEl().dom.style.display != 'none')
					startDay = self.startDay.getRawValue();
				if (self.endDay && self.endDay.getEl().dom && self.endDay.getEl().dom.style.display != 'none')
					endDay = self.endDay.getRawValue();

				var expConf = 0;
				Ext.each(this.menu.items.items, function(item) {
					if (item.checked)
						expConf = expConf | item.value;
				});
				Ext.Ajax.request({
					url : 'cou-access-data!export.action',
					params : {
						data : Ext.encode(data),
						exportIdx : Ext.encode(exportIdx),
						quesTypeData : '[' + quesTypeData.toString() + ']',
						statBy : self.statBy,
						month : month,
						day : day,
						startDay : startDay,
						endDay : endDay,
						byDim : true,
						conf : expConf,
						dims : self.dimChooser.getValue(),
						isGroup : dataPanel.isGroup
						
					},
					success : function(resp) {
						self.getEl().unmask();
						if (!self.downloadIFrame) {
							self.downloadIFrame = self.getEl().createChild({
								tag : 'iframe',
								style : 'display:none;'
							})
						}
						self.downloadIFrame.dom.src = 'cou-access-data!getExport.action?&_t=' + new Date().getTime();
					},
					failure : function(resp) {
						self.getEl().unmask();
						Ext.Msg.alert('错误', resp.responseText);
					}
				});
			} else {
				Ext.Msg.alert('提示', '无数据可供导出')
			}
		}
	});

	var fSet = new Ext.form.FieldSet({
		xtype : 'fieldset',
		border : false,
		style : 'padding : 3px;',
		items : [ {
			layout : 'column',
			labelAlign : "left",
			bodyStyle : 'padding-top:5px;background-color:' + sys_bgcolor,
			border : false,
			items : [ {
				border : false,
				items : queryButton
			}, {
				border : false,
				bodyStyle : 'margin-left : 2px',
				items : exportButton
			}, {
				border : false,
				layout : 'column',
				bodyStyle : 'padding-top:5px;background-color:' + sys_bgcolor,
				html : '&nbsp;&nbsp;统计项:&nbsp;&nbsp;'
			}, {
				border : false,
				items : queryByItem
			}, {
				border : false,
				layout : 'column',
				bodyStyle : 'background-color:' + sys_bgcolor + ' margin-top:4px;',
				html : '&nbsp;&nbsp;统计条件:&nbsp;&nbsp;'
			} ]
		} ]

	});

	var _toggleHandler = function(btn, type) {
		if (!btn.pressed) {
			btn.toggle(true);
			return;
		}
		dataPanel.quesType = type;
		dataPanel.getView().refresh();
		for ( var i = 0; i < _store.getRange().length; i++) {
			var rec = _store.getAt(i);
			if (type != -1)
				rec.data.dynamicTotal = parseInt(rec.json.quesTypeNums['couBizQue'][type]) + parseInt(rec.get('turnAtr'));
			else
				rec.data.dynamicTotal = rec.data.totalQue;
		}
		self.southPanel.removeAll(true);
		var chart = self.newColumnChart();
		self.southPanel.add(chart);
		self.doLayout();
	}
	
	var groupingDisplay =  function(){
		if(!dataPanel.isGroup){
			dataPanel.isGroup = true;
			Ext.getCmp("groupDisplayButton").toggle(true);
			var tempList ;
			for(var i = 0; i < dataPanel.store.getCount(); i++) {
				var record = dataPanel.store.getAt(i);
		    	if(record.data.countItem == "总合计"){
		    		tempList = record;
		    	}
			}
			dataPanel.store.remove(tempList);
	    	for(var i = 0; i < dataPanel.store.recordTempList.length; i++) {
	    		if(dataPanel.store.recordTempList[i].data.countItem == '合计'){
	    			var record = dataPanel.store.recordTempList[i];
	    			dataPanel.store.add(record);
	    		}
	    	}   
	    	//而且连续点击分组按钮时不会重复增添数据
	    	dataPanel.store.groupBy("groupName");
	    	if(tempList)
	    		dataPanel.store.insert(dataPanel.store.getCount(), tempList);
		}
    };
    
	var flatDisplay = function(){
		if(dataPanel.isGroup){
			dataPanel.isGroup = false;
			Ext.getCmp("flatDisplayButton").toggle(true);
			dataPanel.store.recordTempList = [];
		    for(var i = 0; i < dataPanel.store.getCount(); i++) {
	    	    var record = dataPanel.store.getAt(i);
	    	    if(record.data.countItem == "合计" || record.data.countItem == "总合计"){
	    	    	dataPanel.store.recordTempList.push(record);
	    	    }
	    	}
		    
		    for(var i = 0 ; i < dataPanel.store.recordTempList.length; i++){
		    	 var record = dataPanel.store.recordTempList[i];
		    	 if(record.data.countItem == "合计"){
		    		 dataPanel.store.remove(dataPanel.store.recordTempList[i]);
		    	 }
		    }
	    	dataPanel.store.clearGrouping();
		}
    };

	var dataPanelCfg = {
		name : 'accDimDataPanel',
		border : false,
		store : _store,
		columns : columns,
		stripeRows : true,
		region : 'center',
		isGroup : true,
		tbar : new Ext.Toolbar({
			items : [ '问题类型筛选: ', {
				text : '全部',
				enableToggle : true,
				handler : function() {
					_toggleHandler(this, -1);
				},
				pressed : true,
				toggleGroup : 'questype'
			}, {
				text : '全数字',
				enableToggle : true,
				handler : function() {
					_toggleHandler(this, 0);
				},
				pressed : false,
				toggleGroup : 'questype'
			}, {
				text : '全字母',
				enableToggle : true,
				handler : function() {
					_toggleHandler(this, 1);
				},
				pressed : false,
				toggleGroup : 'questype'
			}, {
				text : '字母数字',
				enableToggle : true,
				handler : function() {
					_toggleHandler(this, 2);
				},
				pressed : false,
				toggleGroup : 'questype'
			}, {
				text : '含中文',
				enableToggle : true,
				handler : function() {
					_toggleHandler(this, 3);
				},
				pressed : false,
				toggleGroup : 'questype'
			}, '-', '显示模式: ', {
				text : '数量',
				enableToggle : true,
				handler : function() {
					if (!this.pressed) {
						this.toggle(true);
						return;
					}
					dataPanel.display = 'num';
					dataPanel.getView().refresh();
				},
				pressed : true,
				toggleGroup : 'display'
			}, {
				text : '比例',
				enableToggle : true,
				handler : function() {
					if (!this.pressed) {
						this.toggle(true);
						return;
					}
					dataPanel.display = 'rate';
					dataPanel.getView().refresh();
				},
				pressed : false,
				toggleGroup : 'display'
			} ,'-',{
	            text:'分组显示',
	            id : 'groupDisplayButton',
	            enableToggle : true,
	            handler :groupingDisplay,
	            pressed : true,
	            toggleGroup : 'groupDisplay'
	        },{
	            text:'平铺显示',
	            id : 'flatDisplayButton',
	            enableToggle : true,
	            handler : flatDisplay,
	            pressed : false,
	            toggleGroup : 'groupDisplay'
	        }]
		}),
		view: new Ext.grid.GroupingView({
            forceFit:true,
            groupTextTpl: '{text} (共{[values.rs.length]}条)'
        })
	}
	var dataPanel = new Ext.grid.GridPanel(Ext.applyIf(_cfg || {}, dataPanelCfg));

	this.newColumnChart = function() {
		var fields = [ 'dynamicTotal', 'countItem' ];

		var data = [];

		_store.each(function(record) {
			if (record.data.chartField != -1) {
				data.push({
					dynamicTotal : record.data.dynamicTotal,
					countItem : record.data.countItem
				});
			}
		});
		
		//去除图表中的合计部分
		for(var i=0;i<data.length;i++){
			if(data[i].countItem == "合计" || data[i].countItem == "总合计"){
				data.splice(i, 1);
			}
		}

		var chartStore = new Ext.data.JsonStore({
			fields : fields,
			data : data
		});
		
		
		return new Ext.chart.ColumnChart({
			xtype : 'columnchart',
			store : chartStore,
			yField : 'dynamicTotal',
			url : '../ext/resources/charts.swf',
			xField : 'countItem',
			extraStyle : {
				font : {
					size : 9
				},
				xAxis : {
					labelRotation : 0
				}
			}
		})
	}

	var cfg = {
		layout : 'border',
		border : false,
		items : [ {
			layout : 'form',
			border : false,
			region : 'north',
			height : 38,
			bodyStyle : 'border-bottom: 1px solid ' + sys_bdcolor + ' border-right: 1px solid ' + sys_bdcolor,
			buttonAlign : 'center',
			items : [ {
				layout : 'form',
				border : false,
				bodyStyle : 'background-color:' + sys_bgcolor,
				labelAlign : "center",
				items : [ fSet ]
			} ]
		}, dataPanel, {
			region : 'south',
			xtype : 'panel',
			ref : 'southPanel',
			header : false,
			split : true,
			collapsible : true,
			collapseMode : 'mini',
			frame : false,
			border : false,
			height : 250
		} ]
	};

	AccDimDataPanel.superclass.constructor.call(this, cfg);

	this.queryByItem = queryByItem;
	this.fSet = fSet;
	this.store = _store;
	var hiddenHintState = 0;
	_store.on('load', function(_st, recs, opts) {
		dataPanel.store.recordTempList = [];
		for ( var i = 0; i < recs.length; i++) {
			var total = 0;
			for ( var j = 1; j < fields.length; j++) {
				if (fields[j].name.indexOf('countItem') != -1 || fields[j].name.indexOf('groupName') != -1)
					continue;
				var val = recs[i].get(fields[j].name);
				if (typeof val == 'string')
					total += parseInt(val);
				else
					total += val;
			}
			if (total == 0)
				_store.remove(recs[i]);
		}
		recs = _store.getRange();
		if (recs.length) {
			var totalRecord = new Ext.data.Record({
				chartField : 0,
				couBizQue : 0,
				countItem : "总合计",
				groupName : "总合计",
				totalAcc : 0,
				totalActiveUser : 0,
				totalNewUser : 0,
				totalPeo : 0,
				totalQue : 0,
				totalTime : 0,
				turnAtr : 0,
				typeCommonChat : 0,
				typeCustomChat : 0,
				typeExpired : 0,
				typeFinal : 0,
				typeFinalList : 0,
				typeIllegalWord : 0,
				typeMisinput : 0,
				typeNull : 0,
				typeRepeat : 0,
				typeSimple : 0,
				typeWrongSpell : 0,
				typeSuggested : 0
			});
			
			//用来存放不同分组的合计subTotalArr;
			var map = {};
			map.size = 0;
		
			for ( var i = 0; i < recs.length; i++) {
				recs[i].data.dynamicTotal = recs[i].data.totalQue;
				var groupName = recs[i].data.groupName;
				if(map[groupName] == null){
					var subTotalArr = [];
					var subTotalRecord = new Ext.data.Record({
						chartField : 0,
						couBizQue : 0,
						countItem : "合计",
						groupName : groupName,
						totalAcc : 0,
						totalActiveUser : 0,
						totalNewUser : 0,
						totalPeo : 0,
						totalQue : 0,
						totalTime : 0,
						turnAtr : 0,
						typeCommonChat : 0,
						typeCustomChat : 0,
						typeExpired : 0,
						typeFinal : 0,
						typeFinalList : 0,
						typeIllegalWord : 0,
						typeMisinput : 0,
						typeNull : 0,
						typeRepeat : 0,
						typeSimple : 0,
						typeWrongSpell : 0,
						typeSuggested : 0
					});
					var recordName = groupName + "Record";
					map[groupName] = subTotalArr;
					map[recordName] = subTotalRecord;
					map[map.size] = groupName;
					map.size++;
				}
			}
			
			var totalArr = [];
			if (recs.length) {
				var colsConf = dataPanel.getColumnModel().config;
				var colIdx = {};
				for ( var k = 0; k < colsConf.length; k++) {
					colIdx[colsConf[k].dataIndex] = k;
				}
				for ( var j = 0; j < fields.length; j++) {
					// filter login and acs if zero
					var _name = fields[j].name;
					if (_name.indexOf('countItem') != -1)
						continue;
					var _count = 0;
					totalArr[j] = 0;
					for ( var i = 0; i < recs.length; i++) {
						_count += parseInt(recs[i]['data'][_name]);
						if ("totalNewUser" == _name || "totalActiveUser" == _name || "typeClientMenu" == _name || "typeSuggested" == _name) {
							totalArr[j] = (parseInt(totalArr[j]) + parseInt(recs[i]['data'][_name]));
							if(map[recs[i].data.groupName] != null){
								if(map[recs[i].data.groupName][j] == null){
									map[recs[i].data.groupName][j] = 0;
								}
								map[recs[i].data.groupName][j] = (parseInt(map[recs[i].data.groupName][j]) + parseInt(recs[i]['data'][_name])); 
							}
						} else {
							totalArr[j] = '' + (parseInt(totalArr[j]) + parseInt(recs[i]['data'][_name]));
							if(map[recs[i].data.groupName] != null){
								if(map[recs[i].data.groupName][j] == null){
									map[recs[i].data.groupName][j] = 0;
								}
								map[recs[i].data.groupName][j] = '' + (parseInt(map[recs[i].data.groupName][j]) + parseInt(recs[i]['data'][_name]));
							}
						}
					}
					
					totalRecord.data[_name] = totalArr[j];
					
					//将map中的subTotalRecord遍历取出
					for(var i=0;i<map.size;i++){
						map[map[i] + "Record"].data[_name] = map[map[i]][j];
						map[map[i] + "Record"].data["groupName"] = map[i];
						map[map[i] + "Record"].json = {};
						map[map[i] + "Record"].json["quesTypeNums"] = -1;
						map[map[i] + "Record"].json["quesTypeSuggestedInput"] = -1;
					}
					
					if (_name == 'typeFinalList' || _name == 'turnAtr') {
						if (_count == 0) {
							if (hiddenHintState == 0)
								hiddenHintState = 1;
							dataPanel.getColumnModel().setHidden(colIdx[_name], true);
						} else {
							dataPanel.getColumnModel().setHidden(colIdx[_name], false);
						}
					}
				}
			}
			for(var m=0;m<map.size;m++){
				var quesTypeNums = {};
				var quesTypeSuggestedInput = [ 0, 0, 0, 0 ];
				for ( var i = 0; i < recs.length; i++) {
					if(map[m] != recs[i].data.groupName){
						continue;
					}
					var _quesTypeNums = recs[i].json.quesTypeNums;
					for ( var key in _quesTypeNums) {
						var _arr = _quesTypeNums[key];
						if (!quesTypeNums[key])
							quesTypeNums[key] = [ 0, 0, 0, 0 ];
						for ( var j = 0; j < _arr.length; j++) {
							quesTypeNums[key][j] += _arr[j];
						}
					}
					var _quesTypeSuggestedInput = recs[i].json.quesTypeSuggestedInput;
					if (_quesTypeSuggestedInput) {
						for ( var j = 0; j < _quesTypeSuggestedInput.length; j++) {
							quesTypeSuggestedInput[j] += _quesTypeSuggestedInput[j];
						}
					}
				}
				map[map[m] + "Record"].json["quesTypeNums"]  = 	quesTypeNums;
				map[map[m] + "Record"].json["quesTypeSuggestedInput"] = quesTypeSuggestedInput;
			}
			
			var quesTypeNums = {};
			var quesTypeSuggestedInput = [ 0, 0, 0, 0 ];
			for ( var i = 0; i < recs.length; i++) {
				var _quesTypeNums = recs[i].json.quesTypeNums;
				for ( var key in _quesTypeNums) {
					var _arr = _quesTypeNums[key];
					if (!quesTypeNums[key])
						quesTypeNums[key] = [ 0, 0, 0, 0 ];
					for ( var j = 0; j < _arr.length; j++) {
						quesTypeNums[key][j] += _arr[j];
					}
				}
				var _quesTypeSuggestedInput = recs[i].json.quesTypeSuggestedInput;
				if (_quesTypeSuggestedInput) {
					for ( var j = 0; j < _quesTypeSuggestedInput.length; j++) {
						quesTypeSuggestedInput[j] += _quesTypeSuggestedInput[j];
					}
				}
			}

//			totalRecord.data["countItem"] = "总合计";
			totalRecord.data["groupName"] = "总合计";
			totalRecord.data["chartField"] = "-1";
			totalRecord.json = {};
			totalRecord.json["quesTypeNums"] = quesTypeNums;
			totalRecord.json["quesTypeSuggestedInput"] = quesTypeSuggestedInput;
			
			
			if (recs.length){
				//分别将分组合计插入store
				for(var i=0;i<map.size;i++){
					dataPanel.store.insert(recs.length,map[map[i] + "Record"]);
				}
			}else
				return false;
			
			//将总合计之外的分组名称（groupName）都加上序号以保证排序，显示时将序号去掉
			var groupNameNumMap = {};
			var number = 0;
			for(var i = 0; i < dataPanel.store.getCount(); i++) {
        	    var record = dataPanel.store.getAt(i);
        	    if(record.data.countItem == "总合计"){
        	    	continue;
        	    }
    	    	if(!record.data.groupName){
    	    		record.data.groupName = "未分组";
    	    	}
    	    	if(groupNameNumMap[record.data.groupName] == null){
    	    		groupNameNumMap[record.data.groupName] = record.data.groupName;
    	    		number++;
    	    	}
        	}
			
			//由于合计record是后来手动加进去的，所以要做此动作，不然显示会有问题
			dataPanel.store.insert(dataPanel.store.getCount(), totalRecord);
			dataPanel.isGroup = true;
			flatDisplay();
			if(number > 1){
				groupingDisplay();
			}
		}
		if (hiddenHintState == 1) {
			Dashboard.setAlert('已自动隐藏无数据行列。');
			hiddenHintState = 2;
		}
	});
};
Ext.extend(AccDimDataPanel, Ext.Panel, {
	checkItem : function() {
		var self = this;
		var data = this.queryByItem.getValue();
		self.cleanData();
		var dataArr = data.split('_');
		var dim = dataArr[0], statType = dataArr[1];
		if (statType == 'd') {
			this.initTime();
			this.fSet.items.item(0).add(this.time);
			this.time.setValue(new Date());
			this.initDimChooser([ dim ]);
			this.fSet.items.item(0).add(this.dimChooser);
		} else if (statType == 'm') {
			this.initToMonth();
			this.fSet.items.item(0).add(this.toMonth);
			this.toMonth.setValue(new Date());
			this.initDimChooser([ dim ]);
			this.fSet.items.item(0).add(this.dimChooser);
		} else {
			this.initStartDay();
			this.initEndDay();
			this.fSet.items.item(0).add(this.startDay);
			this.fSet.items.item(0).add(this.endDay);
			this.initDimChooser([ dim ]);
			this.fSet.items.item(0).add(this.dimChooser);
			var date = new Date();
			date.setDate(date.getDate() - 1);
			this.endDay.setValue(date);
			date.setDate(date.getDate() - 7);
			this.startDay.setValue(date);
		}
		
		this.doLayout();
	},
	cleanData : function(data) {
		if (typeof (this.fSet.items.item(0).get(this.time)) != 'undefined') {
			this.time.getEl().dom.style.display = 'none';
		}
		if (typeof (this.fSet.items.item(0).get(this.startDay)) != 'undefined') {
			this.startDay.getEl().dom.style.display = 'none';
		}
		if (typeof (this.fSet.items.item(0).get(this.endDay)) != 'undefined') {
			this.endDay.getEl().dom.style.display = 'none';
		}
		if (typeof (this.fSet.items.item(0).get(this.toMonth)) != 'undefined') {
			this.toMonth.getEl().dom.style.display = 'none';
		}
		if (typeof (this.fSet.items.item(0).get(this.channel)) != 'undefined') {
			this.channel.getEl().dom.style.display = 'none';
		}
		if (typeof (this.fSet.items.item(0).get(this.city)) != 'undefined') {
			this.city.getEl().dom.style.display = 'none';
		}
		if (typeof (this.fSet.items.item(0).get(this.dimChooser)) != 'undefined') {
			this.dimChooser.getEl().dom.style.display = 'none';
			this.dimChooser = null;
		}
	},
	countData : function(data, isRate) {
		var dataArr = data.split('_');
		var dim = dataArr[0], statType = dataArr[1];
		var _dimNames = Dashboard.dimensionNames;
		var date = new Date();
		var city;
		var channel;
		var hour;
		var startday;
		var endday;
		var month;
		var statTitle;
		if (statType == 'd') {
			statTitle = "按" + _dimNames[dim] + "(天)统计";
			endday = this.time.getRawValue();
			this.statBy = "hour";
		} else if (statType == 'm') {
			statTitle = "按" + _dimNames[dim] + "(月)统计";
			month = this.toMonth.getValue();
			this.statBy = "day";
		} else if ((this.startDay.getValue() == null || this.startDay.getValue() == '')
				&& (this.endDay.getValue() == null || this.endDay.getValue() == '')) {
			Ext.Msg.alert('错误提示', '请选择时间段');
			return;
		} else {
			var start = this.startDay.getValue();
			var end = this.endDay.getValue();
			if (!end) {
				Ext.Msg.alert('错误提示', '请选择结束时间');
				return;
			}
			if (end.getTime() > Date.parseDate(date.format("Y-m-d"), 'Y-m-d').getTime()) {
				Ext.Msg.alert('错误提示', '只能统计今天及之前的数据');
				return;
			}
			if (end.getTime() < start.getTime()) {
				Ext.Msg.alert('错误提示', '结束时间不能小于开始时间');
				return;
			}
			if ((end.getTime() - start.getTime()) / (1000 * 3600 * 24) > 45) {
				Ext.Msg.alert('错误提示', '时间段跨度不可超过45天');
				return;
			}
			startday = this.startDay.getRawValue();
			endday = this.endDay.getRawValue();
			this.statBy = 'period';
		}
		var self = this;
		self.southPanel.removeAll(true);
		params = {
			'data' : data,
			'dims' : this.dimChooser.getValue(),
			'hour' : hour,
			'startday' : startday,
			'endday' : endday,
			'month' : month,
			'flag' : isRate,
			'cityName' : '',
			'platformName' : ''
		};
		Ext.Ajax.request({
			url : 'cou-access-data!list.action',
			params : Ext.urlEncode(params),
			success : function(response) {
				var result = Ext.util.JSON.decode(response.responseText);
				var _ts = result.data.data;
				var timer = new ProgressTimer({
					initData : result.data,
					progressId : 'statAskLog',
					boxConfig : {
						title : statTitle
					},
					finish : function() {
						Ext.Ajax.request({
							url : 'cou-access-data!getStatResult.action',
							params : {
								ts : _ts
							},
							success : function(resp) {
								var statResult = Ext.util.JSON.decode(resp.responseText);
								self.store.loadData(statResult);
								var chart = self.newColumnChart();
								self.southPanel.add(chart);
								self.doLayout();
							}
						});
					},
					scope : this
				});
				timer.start();
			},
			failure : function(response) {
				if (response.responseText)
					Ext.Msg.alert('出错了', response.responseText.replace(/\r\n/ig, '<br>'));
			},
			scope : this
		})
	}
});
