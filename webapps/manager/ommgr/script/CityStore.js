CityStore = function(_cfg) {

	var loadGridProxy = new Ext.data.HttpProxy({
				//dict-city!list.action
				url : 'ontology-dimension!getOntologyDimensionByName.action'
			});

	var dataReader = new Ext.data.JsonReader({
				//idProperty : 'cityid',
				root : 'data',
				fields : [{
							name : 'id',
							type : 'string'
						}, {
							name : 'name',
							type : 'string'
						}]
			});

	var cfg = {
		proxy : loadGridProxy,
		reader : dataReader,
		remoteSort : true,
		autoLoad : false,
		writer : new Ext.data.JsonWriter()
	};
	CityStore.superclass.constructor.call(this, Ext.applyIf(_cfg || {}, cfg));
}

Ext.extend(CityStore, Ext.data.Store, {
	loadDatas : function() {
		var store = this;
		store.setBaseParam('query', 'location');
		store.load();
	}
})
