CusAppPanel = function(_cfg) {
	var self = this;
	this.statBy = null;
	var fields = [ {
		type : 'string',
		name : 'countItem'
	}, {
		type : 'string',
		name : 'totalAcc'
	}, {
		type : 'string',
		name : 'totalPeo'
	}, {
		type : 'string',
		name : 'totalQue'
	}, {
		type : 'string',
		name : 'totalApp'
	}, {
		type : 'string',
		name : 'appRate'
	}, {
		type : 'string',
		name : 'couSolve'
	}, {
		type : 'string',
		name : 'couNso'
	}, {
		type : 'string',
		name : 'solveRate'
	}, {
		type : 'int',
		name : 'standardAnsNum'
	} ];
	var columns = [ new Ext.grid.RowNumberer(), {
		dataIndex : 'countItem',
		header : '统计项',
		align : 'left',
		renderer : function(v) {
			if (self.statBy == 'hour')
				return v + "时";
			else {
				v = v.length == 1 ? '0' + v : v;
				if (self.statBy == 'day')
					return self.toMonth.getRawValue() + '-' + v;
				else if (self.statBy == 'month') {
					return v;
					// var year = new Date().getYear();
					// if (Ext.isIE)
					// return year + '-' + v;
					// else
					// return (year + 1900) + '-' + v;
				}
			}
			return v;
		}
	}, {
		dataIndex : 'totalAcc',
		header : Dashboard.getColumnTitle('总会话数'),
		align : 'left'
	}, {
		dataIndex : 'totalPeo',
		header : Dashboard.getColumnTitle('总登录人数'),
		align : 'left'
	}, {
		dataIndex : 'totalQue',
		header : Dashboard.getColumnTitle('总提问数'),
		align : 'left'
	}, {
		dataIndex : 'standardAnsNum',
		header : Dashboard.getColumnTitle('标准回复') + '数',
		align : 'left',
		hidden : true
	}, {
		dataIndex : 'totalApp',
		header : Dashboard.getColumnTitle('参与评价数'),
		align : 'left'
	}, {
		dataIndex : 'appRate',
		header : Dashboard.getColumnTitle('评价比率'),
		align : 'left'
	}, {
		dataIndex : 'couSolve',
		header : Dashboard.getColumnTitle('解决数'),
		align : 'left'
	}, {
		dataIndex : 'couNso',
		header : Dashboard.getColumnTitle('未解决数'),
		align : 'left'
	}, {
		dataIndex : 'solveRate',
		header : Dashboard.getColumnTitle('解决率'),
		align : 'left'
	} ];
	var data = [];
	var continentGroupRow = [ {}, {
		colspan : 5,
		align : 'left'
	}, {
		header : Dashboard.getColumnTitle('客户评价'),
		colspan : 5,
		align : 'center'
	} ];

	var group = new Ext.ux.grid.ColumnHeaderGroup({
		// rows : [continentGroupRow, cityGroupRow]
		rows : [ continentGroupRow ]
	});

	var _store = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({
			url : 'cou-app-data!list.action'
		}),
		reader : new JsonReaderEx({
			idProperty : 'id',
			root : 'data',
			fields : fields
		})
	});

	var queryByItem = new Ext.form.ComboBox({
		fieldLabel : '统计项',
		hiddenName : 'queryByItem',
		valueField : 'id',
		displayField : 'text',
		mode : 'local',
		editable : false,
		typeAhead : true,
		loadMask : true,
		allowBlank : false,
		selectOnFocus : false,
		triggerAction : 'all',
		emptyText : '请选择统计项',
		width : 120,
		store : new Ext.data.SimpleStore({
			fields : [ "id", "text" ],
			data : [ [ 1, "时间(小时)" ], [ 2, "时间段(天)" ], [ 3, "时间(月)" ] ]
		// , [4, "归属地"]]
		}),
		listeners : {
			'select' : function(tp) {
				self.checkItem();
			}
		}
	});

	this.initTime = function() {
		this.time = new Ext.form.DateField({
			name : 'timeText',
			xtype : 'datefield',
			fieldLabel : '选择日期',
			emptyText : '选择日期',
			format : 'Y-m-d',
			editable : false,
			width : 120
		});
	};

	this.initStartDay = function() {
		this.startDay = new Ext.form.DateField({
			name : 'startDay',
			xtype : 'datefield',
			fieldLabel : '起始时间',
			emptyText : '选择起始时间',
			format : 'Y-m-d',
			endDateField : 'endDay',
			editable : false,
			width : 120
		});
	};

	this.initEndDay = function() {
		this.endDay = new Ext.form.DateField({
			name : 'endDay',
			xtype : 'datefield',
			fieldLabel : '终止时间',
			emptyText : '选择终止时间',
			format : 'Y-m-d',
			startDateField : 'startDay',
			editable : false,
			width : 120
		});
	};

	this.initToMonth = function() {
		this.toMonth = new DateFieldEx({
			name : 'toMonth',
			xtype : 'datefield',
			fieldLabel : '月份',
			emptyText : '请选择月份',
			format : 'Y-m',
			editable : false,
			width : 100
		});
	};

	this.initDimChooser = function() {
		this.dimChooser = Dashboard.createDimChooser(150);
	};

	// this.initreply = function() {
	// this.reply = new Ext.form.ComboBox({
	// fieldLabel : '回复方式',
	// hiddenName : 'toChanel',
	// valueField : 'replyid',
	// displayField : 'replyname',
	// mode : 'remote',
	// editable : false,
	// allowBlank : false,
	// selectOnFocus : false,
	// triggerAction : 'all',
	// value : -1,
	// width : 100,
	// store : new Ext.data.SimpleStore({
	// fields : ["replyid", "replyname"],
	// data : [[1, "业务标准回复"], [2, "业务可选"], [4, "聊天"],
	// [5, "敏感词"], [9, "菜单"], [0, "默认回复"],
	// [-1, "所有"]]
	// })
	// });
	// }
	// var _locationName = Dashboard.dimensionNames.location ?
	// Dashboard.dimensionNames.location : '';
	// this.initCity = function() {
	// var cityStore = new CityStore({});
	// cityStore.loadDatas();
	// this.city = new Ext.form.ComboBox({
	// fieldLabel : _locationName,
	// hiddenName : 'attCity',
	// valueField : 'id',
	// displayField : 'name',
	// emptyText : '请选择' + _locationName,
	// mode : 'remote',
	// editable : false,
	// allowBlank : true,
	// selectOnFocus : false,
	// triggerAction : 'all',
	// queryParam : 'location',
	// width : 120,
	// store : cityStore
	// })
	// }
	var queryButton = new Ext.Button({
		text : '统计',
		iconCls : 'icon-search',
		handler : function() {
			if (self.queryByItem.getValue() == null || self.queryByItem.getValue() == '') {
				Ext.Msg.alert('错误提示', '请选择统计项');
				return;
			}
			self.countData(self.queryByItem.getValue());
		}
	});
	var exportButton = new Ext.Button({
		text : '导出结果',
		iconCls : 'icon-ontology-export',
		hidden : !Dashboard.u().allow('om.stat.ki.EXP'),
		handler : function() {
			var records = _store.getRange();
			if (records.length) {
				self.getEl().mask('正在导出数据...');
				var data = [];
				Ext.each(records, function(rec) {
					data.push(rec.data);
				});
				var month = '', day = '';
				if (self.toMonth)
					month = self.toMonth.getRawValue();
				if (self.time)
					day = self.time.getRawValue();
				Ext.Ajax.request({
					url : 'cou-app-data!export.action',
					params : {
						data : Ext.encode(data),
						statBy : self.statBy,
						month : month,
						day : day,
						dims : self.dimChooser.getValue()
					},
					success : function(resp) {
						self.getEl().unmask();
						if (!self.downloadIFrame) {
							self.downloadIFrame = self.getEl().createChild({
								tag : 'iframe',
								style : 'display:none;'
							});
						}
						self.downloadIFrame.dom.src = 'cou-app-data!getExport.action?&_t=' + new Date().getTime();
					},
					failure : function(resp) {
						self.getEl().unmask();
						Ext.Msg.alert('错误', resp.responseText);
					}
				});
			} else {
				Ext.Msg.alert('提示', '无数据可供导出');
			}
		}
	});

	var fSet = new Ext.form.FieldSet({
		xtype : 'fieldset',
		style : 'padding : 3px;',
		border : false,
		items : [ {
			layout : 'column',
			labelAlign : "left",
			border : false,
			bodyStyle : 'padding-top:5px;background-color:' + sys_bgcolor,
			items : [ {
				border : false,
				items : queryButton
			}, {
				border : false,
				bodyStyle : 'margin-left:2px',
				items : exportButton
			}, {
				border : false,
				layout : 'column',
				bodyStyle : 'background-color:' + sys_bgcolor + ' margin-top:4px;',
				html : '&nbsp;&nbsp;统计项:&nbsp;&nbsp;'
			}, {
				border : false,
				items : queryByItem
			}, {
				border : false,
				layout : 'column',
				bodyStyle : 'background-color:' + sys_bgcolor + ' margin-top:4px;',
				html : '&nbsp;&nbsp;统计条件:&nbsp;&nbsp;'
			} ]
		} ]

	});

	var dataPanelCfg = {
		name : 'CusAppPanel',
		border : false,
		store : _store,
		stripeRows : true,
		columns : columns,
		region : 'center',
		viewConfig : {
			forceFit : true
		},
		plugins : group
	};
	var dataPanel = new Ext.grid.GridPanel(Ext.applyIf(_cfg || {}, dataPanelCfg));

	this.newColumnChart = function() {
		return new Ext.chart.ColumnChart({
			xtype : 'columnchart',
			store : _store,
			url : '../ext/resources/charts.swf',
			xField : 'countItem',
			yAxis : new Ext.chart.NumericAxis({
				displayName : '数量',
				labelRenderer : Ext.util.Format.numberRenderer('0,0')
			}),
			tipRenderer : function(chart, record, index, series) {
				if (series.yField == 'couSolve') {
					return '解决数:' + Ext.util.Format.number(+record.data.couSolve, '0,0');
				} else if (series.yField == 'couNso') {
					return '未解决数:' + Ext.util.Format.number(record.data.couNso, '0,0');
				}
			},
			chartStyle : {
				padding : 10,
				animationEnabled : true,
				font : {
					name : 'Tahoma',
					color : 0x444444,
					size : 11
				},
				dataTip : {
					padding : 5,
					border : {
						color : 0x99bbe8,
						size : 1
					},
					background : {
						color : 0xDAE7F6,
						alpha : .9
					},
					font : {
						name : 'Tahoma',
						color : 0x15428B,
						size : 10,
						bold : true
					}
				},
				xAxis : {
					color : 0x69aBc8,
					majorTicks : {
						color : 0x69aBc8,
						length : 4
					},
					minorTicks : {
						color : 0x69aBc8,
						length : 2
					},
					majorGridLines : {
						size : 1,
						color : 0xeeeeee
					}
				},
				yAxis : {
					color : 0x69aBc8,
					majorTicks : {
						color : 0x69aBc8,
						length : 4
					},
					minorTicks : {
						color : 0x69aBc8,
						length : 2
					},
					majorGridLines : {
						size : 1,
						color : 0xdfe8f6
					}
				}
			},
			series : [ {
				type : 'line',
				displayName : '解决数',
				yField : 'couSolve',
				style : {
					color : 0x99BBE8
				}
			}, {
				type : 'line',
				displayName : '未解决数',
				yField : 'couNso',
				style : {
					color : 0x15428B
				}
			} ]
		});
	};

	var cfg = ({
		border : false,
		layout : 'border',
		items : [ {
			region : 'north',
			height : 38,
			layout : 'form',
			border : false,
			bodyStyle : 'border-bottom: 1px solid ' + sys_bdcolor + ' border-right: 1px solid ' + sys_bdcolor,
			buttonAlign : 'center',
			items : [ {
				layout : 'form',
				border : false,
				bodyStyle : 'background-color:' + sys_bgcolor,
				labelAlign : "center",
				items : [ fSet ]
			} ]
		}, dataPanel, {
			region : 'south',
			xtype : 'panel',
			ref : 'southPanel',
			header : false,
			split : true,
			collapsible : true,
			collapseMode : 'mini',
			frame : false,
			border : false,
			height : 250
		} ]
	});

	CusAppPanel.superclass.constructor.call(this, cfg);

	this.queryByItem = queryByItem;
	this.fSet = fSet;
	this.store = _store;
};

Ext.extend(CusAppPanel, Ext.Panel, {
	checkItem : function() {
		var self = this;
		var data = this.queryByItem.getValue();
		// var _dimNames = Dashboard.dimensionNames;
		self.cleanData(data);
		if (data == 1) {
			this.initTime();
			this.fSet.items.item(0).add(this.time);
			// this.initreply();
			// this.fSet.items.item(0).add(this.reply);
			// if (_dimNames.location) {
			// this.initCity();
			// this.fSet.items.item(0).add(this.city);
			// }
			this.time.setValue(new Date());
			this.initDimChooser();
			this.fSet.items.item(0).add(this.dimChooser);
			this.statBy = 'hour';
		}
		if (data == 2) {
			this.initToMonth();
			this.fSet.items.item(0).add(this.toMonth);
			this.toMonth.setValue(new Date());
			// this.initStartDay();
			// this.initEndDay();
			// this.fSet.items.item(0).add(this.startDay);
			// this.fSet.items.item(0).add(this.endDay);
			// this.initreply();
			// this.fSet.items.item(0).add(this.reply);
			// if (_dimNames.location) {
			// this.initCity();
			// this.fSet.items.item(0).add(this.city);
			// }
			this.initDimChooser();
			this.fSet.items.item(0).add(this.dimChooser);
			this.statBy = 'day';
		}
		if (data == 3) {
			// alert("月");
			// this.initToMonth();
			// this.fSet.items.item(0).add(this.toMonth);
			// this.initreply();
			// this.fSet.items.item(0).add(this.reply);
			// if (_dimNames.location) {
			// this.initCity();
			// this.fSet.items.item(0).add(this.city);
			// }
			this.initDimChooser();
			this.fSet.items.item(0).add(this.dimChooser);
			this.statBy = 'month';
		}
		// if (data == 4) {
		// this.initStartDay();
		// this.initEndDay();
		// this.fSet.items.item(0).add(this.startDay);
		// this.fSet.items.item(0).add(this.endDay);
		// // this.initreply();
		// // this.fSet.items.item(0).add(this.reply);
		// if (_dimNames.location) {
		// this.initCity();
		// this.fSet.items.item(0).add(this.city);
		// }
		// }
		// if (data == 5) {
		// this.initStartDay();
		// this.initEndDay();
		// this.fSet.items.item(0).add(this.startDay);
		// this.fSet.items.item(0).add(this.endDay);
		// // this.initreply();
		// // this.fSet.items.item(0).add(this.reply);
		// if (_dimNames.location) {
		// this.initCity();
		// this.fSet.items.item(0).add(this.city);
		// }
		// }
		this.doLayout();
	},
	cleanData : function(data) {
		if (typeof (this.fSet.items.item(0).get(this.time)) != 'undefined') {
			this.time.getEl().dom.style.display = 'none';
		}
		if (typeof (this.fSet.items.item(0).get(this.startDay)) != 'undefined') {
			this.startDay.getEl().dom.style.display = 'none';
		}
		if (typeof (this.fSet.items.item(0).get(this.endDay)) != 'undefined') {
			this.endDay.getEl().dom.style.display = 'none';
		}
		if (typeof (this.fSet.items.item(0).get(this.toMonth)) != 'undefined') {
			this.toMonth.getEl().dom.style.display = 'none';
		}
		// if (typeof(this.fSet.items.item(0).get(this.reply))
		// != 'undefined') {
		// this.reply.getEl().dom.style.display = 'none';
		// }
		// if (typeof (this.fSet.items.item(0).get(this.city)) != 'undefined') {
		// this.city.getEl().dom.style.display = 'none';
		// }
		if (typeof (this.fSet.items.item(0).get(this.dimChooser)) != 'undefined') {
			this.dimChooser.getEl().dom.style.display = 'none';
			this.dimChooser = null;
		}
	},
	countData : function(data) {
		// var _dimNames = Dashboard.dimensionNames;
		var date = new Date();
		// var city;
		// var reply;
		var hour = null, startday = null, endday = null, month = null, statTitle = null;
		if (data == 1) {
			if (this.time.getRawValue() == null || this.time.getRawValue() == '') {
				Ext.Msg.alert('错误提示', '请选择日期');
				return;
			}
			if (this.time.getValue().getTime() > Date.parseDate(date.format("Y-m-d"), 'Y-m-d').getTime()) {
				Ext.Msg.alert('错误提示', '按小时统计，只能选择今天以前的数据');
				return;
			}
			hour = this.time.getRawValue();
			// if (this.city && this.city.getValue)
			// city = this.city.getValue();
			statTitle = '按小时统计';
			// reply = this.reply.getValue();
		}
		if (data == 2) {
			month = this.toMonth.getValue();
			// if (this.city && this.city.getValue)
			// city = this.city.getValue();
			statTitle = '按天统计';
		}
		// if (data == 4 || data == 5) {
		// if ((this.startDay.getValue() == null || this.startDay.getValue() ==
		// '')
		// && (this.endDay.getValue() == null || this.endDay.getValue() == ''))
		// {
		// Ext.Msg.alert('错误提示', '请选择时间段');
		// return;
		// } else {
		// var start = this.startDay.getValue();
		// var end = this.endDay.getValue();
		// if (Date.parseDate(start.format("Y-m"), 'Y-m').getTime() !=
		// Date.parseDate(end.format("Y-m"), 'Y-m').getTime()) {
		// Ext.Msg.alert('错误提示', '时间段只能是一个月内的数据');
		// return;
		// }
		//
		// if (end.getTime() > Date.parseDate(date.format("Y-m-d"),
		// 'Y-m-d').getTime()) {
		// Ext.Msg.alert('错误提示', '只能统计今天及之前的数据');
		// return;
		// }
		// if (end.getTime() < start.getTime()) {
		// Ext.Msg.alert('错误提示', '结束时间不能小于开始时间');
		// return;
		// }
		// startday = this.startDay.getRawValue();
		// endday = this.endDay.getRawValue();
		// if (this.city && this.city.getValue)
		// city = this.city.getValue();
		// // reply = this.reply.getValue();
		// }
		//
		// }
		if (data == 3) {
			// if (this.toMonth.getValue() == null
			// || this.toMonth.getValue() == '') {
			// Ext.Msg.alert('错误提示', '请选择统计的月');
			// return;
			// } else {
			// if (this.toMonth.getValue().getTime() >
			// Date.parseDate(
			// date.format("Y-m"), 'Y-m').getTime()) {
			// Ext.Msg.alert('错误提示', '只能统计本月及之前的信息')
			// return;
			// }
			// month = this.toMonth.getRawValue();
			// if (this.city && this.city.getValue)
			// city = this.city.getValue();
			statTitle = '按月统计';
			// reply = this.reply.getValue();
			// }

		}
		var self = this;
		self.southPanel.removeAll(true);
		var params = {
			'data' : data,
			// 'city' : city,
			// 'reply' : reply,
			'hour' : hour,
			'startday' : startday,
			'endday' : endday,
			'month' : month,
			dims : this.dimChooser.getValue()
		};
		// if (this.city && this.city.getRawValue)
		// params.cityName = this.city.getRawValue()
		Ext.Ajax.request({
			url : 'cou-app-data!list.action',
			params : Ext.urlEncode(params),
			success : function(response) {
				var result = Ext.util.JSON.decode(response.responseText);
				var _ts = result.data.data;
				var timer = new ProgressTimer({
					initData : result.data,
					progressId : 'statFaqVoteLog',
					boxConfig : {
						title : statTitle
					},
					finish : function() {
						Ext.Ajax.request({
							url : 'cou-app-data!getStatResult.action',
							params : {
								ts : _ts
							},
							success : function(resp) {
								var statResult = Ext.util.JSON.decode(resp.responseText);
								self.store.loadData(statResult);
								var chart = self.newColumnChart();
								self.southPanel.add(chart);
								self.doLayout();
							}
						});
					},
					scope : this
				});
				timer.start();
			},
			failure : function(response) {
				if (response.responseText)
					Ext.Msg.alert('出错了', response.responseText.replace(/\r\n/ig, '<br>'));
			},
			scope : this
		});
	}
});