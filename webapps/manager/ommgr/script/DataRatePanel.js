/**
 * 数据占比统计
 * 
 * @param {}
 *            _cfg
 */
DataRatePanel = function(_cfg) {
	var self = this;
	var fields = [ {
		type : 'string',
		name : 'countItem'
	}, {
		type : 'string',
		name : 'totalAcc'
	}, {
		type : 'string',
		name : 'totalPeo'
	}, {
		type : 'string',
		name : 'totalQue'
	}, {
		type : 'string',
		name : 'totalTime'
	}, {
		type : 'string',
		name : 'bizSta'
	}, {
		type : 'string',
		name : 'bizChi'
	}, {
		type : 'string',
		name : 'chatting'
	}, {
		type : 'string',
		name : 'aliveWor'
	}, {
		type : 'string',
		name : 'menu'
	}, {
		type : 'string',
		name : 'noRep'
	}, {
		type : 'string',
		name : 'couBizQue'
	}, {
		type : 'string',
		name : 'turnAtr'
	} ];
	var columns = [ new Ext.grid.RowNumberer(), {
		dataIndex : 'countItem',
		// header : '统计项',
		align : 'center'
	}, {
		dataIndex : 'totalAcc',
		// header : '总访问人次',
		align : 'center'
	}, {
		dataIndex : 'totalPeo',
		// header : '总访问人数',
		align : 'center'
	}, {
		dataIndex : 'totalQue',
		// header : '总提问数',
		align : 'center'
	}, {
		dataIndex : 'totalTime',
		header : '(分钟)',
		align : 'center'
	}, {
		dataIndex : 'bizSta',
		header : '业务标准回复(标准问)',
		align : 'center'
	}, {
		dataIndex : 'bizChi',
		header : '业务可选(列表)',
		align : 'center'
	}, {
		dataIndex : 'chatting',
		header : '聊天',
		align : 'center'
	}, {
		dataIndex : 'aliveWor',
		header : '敏感词',
		align : 'center'
	}, {
		dataIndex : 'menu',
		header : '菜单',
		align : 'center'
	}, {
		dataIndex : 'noRep',
		header : '默认回复',
		align : 'center'
	}, {
		dataIndex : 'couBizQue',
		align : 'center'
	}, {
		dataIndex : 'turnAtr',
		align : 'center'
	} ];
	var data = [];
	var continentGroupRow = [ {}, {
		header : '统计项',
		colspan : 1,
		align : 'center'
	}, {
		header : '总会话数',
		colspan : 1,
		align : 'center'
	}, {
		header : '总访问人数',
		colspan : 1,
		align : 'center'
	}, {
		header : '总提问数',
		colspan : 1,
		align : 'center'
	}, {
		header : '总访问时长',
		colspan : 1,
		align : 'center'
	}, {
		header : '回复比率',
		colspan : 6,
		align : 'center'
	}, {
		header : '业务回复率',
		colspan : 1,
		align : 'center'
	}, {
		header : '转人工比率',
		colspan : 1,
		align : 'center'
	} ];

	var group = new Ext.ux.grid.ColumnHeaderGroup({
		// rows : [continentGroupRow, cityGroupRow]
		rows : [ continentGroupRow ]
	});

	var _store = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({
			url : 'cou-access-rate-data!list.action'
		}),
		reader : new JsonReaderEx({
			idProperty : 'id',
			root : 'data',
			fields : fields
		}),
		writer : new Ext.data.JsonWriter()
	});

	var queryByItem = new Ext.form.ComboBox({
		fieldLabel : '统计项',
		hiddenName : 'queryByItem',
		valueField : 'id',
		displayField : 'text',
		mode : 'local',
		editable : false,
		typeAhead : true,
		loadMask : true,
		allowBlank : false,
		selectOnFocus : false,
		triggerAction : 'all',
		emptyText : '请选择统计项',
		width : 120,
		store : new Ext.data.SimpleStore({
			fields : [ "id", "text" ],
			data : [ [ 1, "时间(小时)" ], [ 2, "时间段(天)" ], [ 3, "时间(月)" ], [ 4, "平台" ], [ 5, "归属地" ] ]
		}),
		listeners : {
			'select' : function(tp) {
				self.checkItem();
			}
		}
	});

	this.initTime = function() {
		this.time = new Ext.form.DateField({
			name : 'timeText',
			xtype : 'datefield',
			fieldLabel : '选择日期',
			emptyText : '选择日期',
			format : 'Y-m-d',
			editable : false,
			width : 120
		})

	}

	this.initStartDay = function() {
		this.startDay = new Ext.form.DateField({
			name : 'startDay',
			xtype : 'datefield',
			fieldLabel : '起始时间',
			emptyText : '选择起始时间',
			format : 'Y-m-d',
			endDateField : 'endDay',
			editable : false,
			width : 120
		})
	}

	this.initEndDay = function() {
		this.endDay = new Ext.form.DateField({
			name : 'endDay',
			xtype : 'datefield',
			fieldLabel : '终止时间',
			emptyText : '选择终止时间',
			format : 'Y-m-d',
			startDateField : 'startDay',
			editable : false,
			width : 120
		})
	}

	this.initToMonth = function() {
		this.toMonth = new DateFieldEx({
			name : 'toMonth',
			xtype : 'datefield',
			fieldLabel : '月份',
			emptyText : '请选择月份',
			format : 'Y-m',
			editable : false,
			width : 100
		})
	}

	this.initChannel = function() {
		var channelStore = new ChannelStore({});
		this.channel = new Ext.form.ComboBox({
			fieldLabel : '平台',
			hiddenName : 'toChanel',
			valueField : 'channelid',
			displayField : 'channelname',
			mode : 'remote',
			editable : false,
			allowBlank : false,
			selectOnFocus : false,
			triggerAction : 'all',
			width : 100,
			store : channelStore
		});
		channelStore.on('load', function() {
			self.channel.setValue(-1);
		});
		channelStore.loadDatas();

	}

	this.initCity = function() {
		var cityStore = new CityStore({});
		this.city = new Ext.form.ComboBox({
			fieldLabel : '归属地',
			hiddenName : 'attCity',
			valueField : 'cityid',
			displayField : 'cityname',
			mode : 'remote',
			editable : false,
			allowBlank : false,
			selectOnFocus : false,
			triggerAction : 'all',
			width : 100,
			store : cityStore
		})

		cityStore.on('load', function() {
			self.city.setValue(10);
		});
		cityStore.loadDatas();
	}
	var queryButton = new Ext.Button({
		text : '开始统计',
		iconCls : 'icon-search',
		handler : function() {
			if (self.queryByItem.getValue() == null || self.queryByItem.getValue() == '') {
				Ext.Msg.alert('错误提示', '请选择统计项');
				return;
			}
			self.countData(self.queryByItem.getValue());
		}

	});

	var fSet = new Ext.form.FieldSet({
		xtype : 'fieldset',
		autoHeight : true,
		width : 800,
		height : 100,
		items : [ {
			layout : 'column',
			labelAlign : "left",
			border : false,
			bodyStyle : 'padding-top:5px;',
			items : [ {
				border : false,
				items : queryButton
			}, {
				border : false,
				layout : 'column',
				html : '&nbsp;&nbsp;统计项:&nbsp;&nbsp;',
				height : 22
			}, {
				border : false,
				items : queryByItem
			}, {
				border : false,
				layout : 'column',
				html : '&nbsp;&nbsp;统计条件:&nbsp;&nbsp;',
				height : 22
			} ]
		} ]

	});

	var cfg = ({
		name : 'DataRatePanel',
		border : false,
		width : 1000,
		height : 400,
		store : _store,
		columns : columns,
		stripeRows : true,
		items : [ {
			layout : 'form',
			border : false,
			buttonAlign : 'center',
			items : [ {
				layout : 'form',
				border : false,
				labelAlign : "center",
				items : [ fSet ]
			} ]
		} ],
		viewConfig : {
			forceFit : true
		},
		plugins : group
	});

	DataRatePanel.superclass.constructor.call(this, Ext.applyIf(_cfg || {}, cfg));

	this.queryByItem = queryByItem;
	this.fSet = fSet;
	this.store = _store;
}
Ext.extend(DataRatePanel, Ext.grid.GridPanel, {

	checkItem : function() {
		var self = this;
		var data = this.queryByItem.getValue()
		self.cleanData(data);
		if (data == 1) {
			this.initTime();
			this.fSet.items.item(0).add(this.time);
			this.initChannel();
			this.fSet.items.item(0).add(this.channel);
			this.initCity();
			this.fSet.items.item(0).add(this.city);
		}
		if (data == 2) {
			this.initStartDay();
			this.initEndDay();
			this.fSet.items.item(0).add(this.startDay);
			this.fSet.items.item(0).add(this.endDay);
			this.initChannel();
			this.fSet.items.item(0).add(this.channel);
			this.initCity();
			this.fSet.items.item(0).add(this.city);
		}
		if (data == 3) {
			// alert("月");
			this.initToMonth();
			this.fSet.items.item(0).add(this.toMonth);
			this.initChannel();
			this.fSet.items.item(0).add(this.channel);
			this.initCity();
			this.fSet.items.item(0).add(this.city);
		}
		if (data == 4) {
			this.initStartDay();
			this.initEndDay();
			this.fSet.items.item(0).add(this.startDay);
			this.fSet.items.item(0).add(this.endDay);
			this.initChannel();
			this.fSet.items.item(0).add(this.channel);
			this.initCity();
			this.fSet.items.item(0).add(this.city);
		}
		if (data == 5) {
			this.initStartDay();
			this.initEndDay();
			this.fSet.items.item(0).add(this.startDay);
			this.fSet.items.item(0).add(this.endDay);
			this.initChannel();
			this.fSet.items.item(0).add(this.channel);
			this.initCity();
			this.fSet.items.item(0).add(this.city);
		}
		this.doLayout();
	},

	cleanData : function(data) {
		if (typeof (this.fSet.items.item(0).get(this.time)) != 'undefined') {
			this.time.getEl().dom.style.display = 'none';
		}
		if (typeof (this.fSet.items.item(0).get(this.startDay)) != 'undefined') {
			this.startDay.getEl().dom.style.display = 'none';
		}
		if (typeof (this.fSet.items.item(0).get(this.endDay)) != 'undefined') {
			this.endDay.getEl().dom.style.display = 'none';
		}
		if (typeof (this.fSet.items.item(0).get(this.toMonth)) != 'undefined') {
			this.toMonth.getEl().dom.style.display = 'none';
		}
		if (typeof (this.fSet.items.item(0).get(this.channel)) != 'undefined') {
			this.channel.getEl().dom.style.display = 'none';
		}
		if (typeof (this.fSet.items.item(0).get(this.city)) != 'undefined') {
			this.city.getEl().dom.style.display = 'none';
		}
	},

	countData : function(data) {
		var date = new Date();
		var city;
		var channel;
		var hour;
		var startday;
		var endday;
		var month;
		if (data == 1) {
			if (this.time.getRawValue() == null || this.time.getRawValue() == '') {
				Ext.Msg.alert('错误提示', '请选择日期')
				return;
			}
			if (this.time.getValue().getTime() >= Date.parseDate(date.format("Y-m-d"), 'Y-m-d').getTime()) {
				Ext.Msg.alert('错误提示', '按小时统计，只能选择昨天以前的数据')
				return;
			}
			hour = this.time.getRawValue()
			city = this.city.getValue();
			channel = this.channel.getValue();
		}
		if (data == 2 || data == 4 || data == 5) {
			if ((this.startDay.getValue() == null || this.startDay.getValue() == '')
					&& (this.endDay.getValue() == null || this.endDay.getValue() == '')) {
				Ext.Msg.alert('错误提示', '请选择时间段');
				return;
			} else {
				var start = this.startDay.getValue().getTime();
				var addStart = parseInt(start) + parseInt(31 * 24 * 3600 * 1000) // 得到31天后的毫秒数
				var end = this.endDay.getValue().getTime();
				if (addStart < end) {
					Ext.Msg.alert('错误提示', '时间段不能超过31天');
					return;
				}

				if (end >= Date.parseDate(date.format("Y-m-d"), 'Y-m-d').getTime()) {
					Ext.Msg.alert('错误提示', '只能统计昨天及之前的数据');
					return;
				}
				if (end < start) {
					Ext.Msg.alert('错误提示', '结束时间不能小于开始时间');
					return;
				}
				startday = this.startDay.getRawValue();
				endday = this.endDay.getRawValue();
				city = this.city.getValue();
				channel = this.channel.getValue();
			}

		}
		if (data == 3) {
			if (this.toMonth.getValue() == null || this.toMonth.getValue() == '') {
				Ext.Msg.alert('错误提示', '请选择统计的月');
				return;
			} else {
				if (this.toMonth.getValue().getTime() >= Date.parseDate(date.format("Y-m"), 'Y-m').getTime()) {
					Ext.Msg.alert('错误提示', '只能统计上个月及之前的信息')
					return;
				}
				month = this.toMonth.getRawValue();
				city = this.city.getValue();
				channel = this.channel.getValue();
			}

		}
		this.store.load({
			params : {
				'data' : data,
				'city' : city,
				'channel' : channel,
				'hour' : hour,
				'startday' : startday,
				'endday' : endday,
				'month' : month
			}
		})
	}
});
