DefineReport = function(_cfg) {
	_cfg.border = false;
	_cfg.html = '<iframe src="/manager/ommgr/report/index.jsp" frameborder=0 scrolling=auto width="100%" height="100%"></iframe>';
	DefineReport.superclass.constructor.call(this, _cfg);
}

Ext.extend(DefineReport, Ext.Panel, {
	loadData : function() {
	}
});


var r_menu = new Ext.menu.Menu({
	items : [ {
		ref : 'refreshBtn',
		text : '刷新',
		iconCls : 'icon-refresh',
		width : 50,
		handler : function() {
			r_menu.currentNode.reload();
		}
	}, {
		ref : 'delBtn',
		text : '删除',
		iconCls : 'icon-delete',
		width : 50,
		handler : function() {
			Ext.Msg.confirm('提示', '确认删除' + r_menu.currentNode.text + '？', function(btn) {
				if (btn == 'yes') {
					Ext.Ajax.request({
						url : 'report!deleteRp.action',
						params : {
							menuId : r_menu.currentNode.id
						},
						success : function(response, options) {
							var json = Ext.util.JSON.decode(response.responseText);
							Dashboard.setAlert(json.message);
							var tab = Dashboard.mainPanel.get(r_menu.currentNode.id + 'Tab');
							if(tab) {
								tab.destroy();
							}
							r_menu.currentNode.parentNode.reload();
						},
						failure : function(resp) {
							Ext.Msg.alert('错误', resp.responseText);
						}
					});
				}
			});
		}
	} ]
});

