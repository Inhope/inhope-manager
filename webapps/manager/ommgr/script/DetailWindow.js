DetailWindow = function(_cfg) {
	var self = this;

	var logPanel = new Ext.Panel({
		region : 'center',
		autoScroll : true,
		bodyStyle : 'padding:5px 5px 5px 5px'
	});

	var cfg = {
		width : 400,
		height : 460,
		modal : true,
		plain : true,
		shim : true,
		title : '会话详细信息',
		closeAction : 'hide',
		collapsible : true,// 折叠
		closable : true, // 关闭
		resizable : false,// 改变大小
		draggable : true,// 拖动
		minimizable : false,// 最小化
		maximizable : false,// 最大化
		animCollapse : true,
		constrainHeader : true,
		// autoHeight:true,
		layout : 'border',
		bodyStyle : 'padding:5px;',
		items : [ logPanel ]
	};
	DetailWindow.superclass.constructor
			.call(this, Ext.applyIf(_cfg || {}, cfg));
	this.logPanel = logPanel;
	logPanel.on('afterrender', function() {
		logPanel.getUpdater().on('update', function(el, resp) {
			logPanel.body.dom.innerHTML = resp.responseText.replace(/<div(.*?)>/g, '&lt;div$1&gt;').replace(/<\/div>/g, '&lt;/div&gt;');
		})
	})
};

Ext.extend(DetailWindow, Ext.Window, {
	getPanel : function() {
		return this.logPanel;
	}
});