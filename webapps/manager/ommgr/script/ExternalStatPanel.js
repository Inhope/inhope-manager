ExternalStatPanel = function(_cfg) {

	_cfg.border = false;
	_cfg.html = '<iframe src="'
			+ _cfg.url
			+ '" frameborder=0 scrolling=auto width="100%" height="100%"></iframe>';
	ExternalStatPanel.superclass.constructor.call(this, _cfg);
}

Ext.extend(ExternalStatPanel, Ext.Panel, {
	loadData : function() {
	}
});