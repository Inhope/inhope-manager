Ext.namespace("IPTVVodSearchDetailPanel");
IPTVVodSearchDetailPanel = function(_cfg) {
	var self = this;
	this.urlRoot = 'http://101.95.48.118/iptvvod';
	this.VodDetailRecord = Ext.data.Record.create(['id', 'name','originalCountry', 'type', 'status', 'insertDate','actorDisplay','genre','writerDisplay']);
	var statusData = [[1, '上线'],[0, '下线']];
	var statusMap = {};
	for (var i = 0; i < statusData.length; i++)
		statusMap[statusData[i][0]] = statusData[i][1];
	var store = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({
			url : this.urlRoot+'/rs/program/search/json'
		}),
		baseParams: {
			sort : 'insertDate',
			order : 'desc'
		},
		reader : new Ext.data.JsonReader({
			idProperty : 'id',
			totalProperty : 'total',
			root : 'data',
			fields : this.VodDetailRecord
		})
	});
	this.store = store;
	var pagingBar = new Ext.PagingToolbar({
		store : this.store,
		displayInfo : true,
		pageSize : 20,
		prependButtons : true,
		beforePageText : '第',
		afterPageText : '页，共{0}页',
		displayMsg : '第{0}到{1}条记录，共{2}条',
		emptyMsg : "没有记录"
	});
	this.pagingBar = pagingBar;
	
	var config = {
		store : store,
		columnLines : true,
		margins : '0 5 5 5',
		border : false,
		style : 'border-right: 1px solid ' + sys_bdcolor,
		tbar : [ ],
		bbar : pagingBar,
		sm : new Ext.grid.RowSelectionModel(),
		loadMask : true,
		colModel : new Ext.grid.ColumnModel({
			columns : [new Ext.grid.RowNumberer(), {
							width : 250,
							header : 'CODE',
							dataIndex : 'id'
						},{
							width : 280,
							header : '影片名称',
							dataIndex : 'name'
						}, {
							header : '入库时间',
							width : 150,
							dataIndex : 'insertDate',
							renderer:Ext.util.Format.dateRenderer('Y-m-d H:i:s')
						}, {
							header : '地区',
							width : 60,
							dataIndex : 'originalCountry'
						}, {
							header : '类型',
							width : 80,
							dataIndex : 'type'
						}, {
							header : '影片状态',
							width : 65,
							dataIndex : 'status',
							renderer : function(v) {
								if(statusMap[v])
									return statusMap[v]
								return "未知";
							}
						}, {
							header : '演员',
							width : 150,
							dataIndex : 'actorDisplay'
						}, {
							header : '风格',
							width : 150,
							dataIndex : 'genre'
						}, {
							header : '导演',
							width : 80,
							dataIndex : 'writerDisplay'
						}]
			})
	};
	this.tbarEx = new Ext.Toolbar({
				enableOverflow : true,
				items : ['影片名称:', {
							id : 's_vod_movie_name',
							xtype : 'textfield',
							width : 90,
							emptyText : '输入影片名'
						}, '入库时间:从', {
							id : 's_vod_store_bdate',
							xtype : 'datefield',
							width : 90,
							emptyText : '选择时间',
							format : 'Y-m-d'
						}, '到', {
							id : 's_vod_store_edate',
							xtype : 'datefield',
							width : 90,
							emptyText : '选择时间',
							format : 'Y-m-d'
						}, '影片状态:', new Ext.form.ComboBox({
									id : 's_vod_movie_status',
									triggerAction : 'all',
									width : 85,
									editable : false,
									value : '-1',
									mode : 'local',
									store : new Ext.data.ArrayStore({
												fields : ['type', 'displayText'],
												data : [[-1, '全部']].concat(statusData)
											}),
									valueField : 'type',
									displayField : 'displayText'
								}), ' ', {
							text : '搜索',
							iconCls : 'icon-search',
							handler : function() {
								self.search();
							}
						}, ' ', {
							text : '全部',
							iconCls : 'icon-search',
							handler : function() {
								self.search(true);
							}
						}, '-', {
							iconCls : 'icon-ontology-export',
							text : '导出',
							handler : function() {
								self.expData();
							}
						}]
			});
	LeaveMessagePanel.superclass.constructor.call(this, config);
	this.addListener('render', function() {
				this.tbarEx.render(this.tbar);
			}, this);
	
};

Ext.extend(IPTVVodSearchDetailPanel, Ext.grid.GridPanel, {
	loadData : function() {
		this.store.load({
			params : {
				start : 0,
				limit : 20,
				sort : 'insertDate',
				order : 'desc'
			}
		});
		
	},
	search : function(showAll) {
		var store = this.store;
//		store.removeAll();
		var _movieNameField = this.tbarEx.findById('s_vod_movie_name');
		var _statusCombo = this.tbarEx.findById('s_vod_movie_status');
		var _bdateField = this.tbarEx.findById('s_vod_store_bdate');
		var _edateField = this.tbarEx.findById('s_vod_store_edate');
		if (showAll) {
			_movieNameField.setValue('');
			_statusCombo.setValue('-1');
			_bdateField.setValue('');
			_edateField.setValue('');
		}
		var _name = _movieNameField.getValue().trim();
		var _status = _statusCombo.getValue();
		var _beginDate = _bdateField.getValue();
		var _endDate = _edateField.getValue();
		store.setBaseParam('status', _status);
		store.setBaseParam('name', _name ? _name : '');
		store.setBaseParam('beginDate', _beginDate ? _beginDate : '');
		store.setBaseParam('endDate', _endDate ? _endDate : '');
		store.setBaseParam('sort','insertDate');
		store.setBaseParam('order', 'desc');
		var self = this;
		store.load({
			callback :function(r,options,success){
		        if(!success){
		            store.removeAll();
		            //更新page导航信息
		            self.pagingBar.updateInfo();
		            self.pagingBar.afterTextItem.text = String.format("页，共{0}页", 1);
		            self.pagingBar.first.setDisabled(true);
		            self.pagingBar.prev.setDisabled(true);
		            self.pagingBar.next.setDisabled(true);
		            self.pagingBar.last.setDisabled(true);
//		            self.pagingBar.loading.disable();
		        }
			}
		});
	},
	expData : function() {
		var _movieNameField = this.tbarEx.findById('s_vod_movie_name');
		var _statusCombo = this.tbarEx.findById('s_vod_movie_status');
		var _bdateField = this.tbarEx.findById('s_vod_store_bdate');
		var _edateField = this.tbarEx.findById('s_vod_store_edate');
		var _name = _movieNameField.getValue().trim();
		var _status = _statusCombo.getValue();
		var _beginDate = _bdateField.getValue();
		var _endDate = _edateField.getValue();
		if(!(_beginDate && _endDate)){
			 Ext.MessageBox.show({
		           title: '提示',
		           msg: '导出必须选择入库时间范围',
		           buttons: Ext.MessageBox.OK
		       });
		}else{
			var params = {
				'name' : _name ? _name : '',
				'status' : _status,
				'beginDate' : _beginDate ? _beginDate : '',
				'endDate' : _endDate ? _endDate : '',
				'sort' : 'insertDate',
				'order' : 'desc'
			};
			Ext.MessageBox.show({
	           title: '请稍等',
	           msg: '正在生成VOD EXCEL文件...',
	           width:300,
	           progress:false,
	           closable:false
			});
			Ext.Ajax.request({
				url : this.urlRoot+'/rs/program/export',
				scriptTag: true,
				params : Ext.urlEncode(params),
				success : function(response) {
					Ext.MessageBox.hide();
					window.location.href = this.urlRoot+'/rs/program/down/'+ new Date().getTime();
				},
				failure : function(response) {
					if (response.responseText)
						Ext.Msg.alert('出错了', response.responseText.replace(/\r\n/ig, '<br>'));
				},
				scope : this
			})
		}
	}
});