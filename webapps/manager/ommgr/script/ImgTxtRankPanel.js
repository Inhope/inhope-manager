ImgTxtRankPanel = function(_cfg) {
	var self = this;
	var _pageSize = 20;
	var platform = _cfg.platform;
	var fields = [ {
		name : 'rank'
	}, {
		name : 'num'
	}, {
		name : 'faqName'
	}, {
		name : 'faqId'
	}, {
		name : 'objectName'
	}, {
		name : 'itemTitle'
	}, {
		name : 'itemURL'
	}, {
		name : 'dailyNums',
		type : 'object'
	} ];
	var columns = [ {
		dataIndex : 'rank',
		header : '排名',
		width : 50,
		align : 'center'
	}, {
		dataIndex : 'itemTitle',
		header : '图文标题',
		width : 150,
		align : 'center'
	}, {
		dataIndex : 'itemURL',
		header : '图文链接地址',
		width : 250,
		align : 'center'
	}, {
		dataIndex : 'faqName',
		header : '知识点',
		width : 200,
		align : 'left'
	}, {
		dataIndex : 'objectName',
		header : '所属实例',
		width : 100,
		align : 'left'
	}, {
		dataIndex : 'num',
		header : '访问次数',
		width : 80,
		align : 'left'
	} ];

	for ( var i = 1; i <= 31; i++) {
		columns.push(new Ext.grid.Column({
			dataIndex : '_dailyNum',
			header : i + '日',
			width : 95,
			hidden : true,
			align : 'center',
			renderer : function(v, meta, rec, rowIdx, colIdx) {
				return rec.get('dailyNums')[colIdx - 5];
			}
		}));
	}

	var _store = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({
			url : 'img-txt-rank!list.action'
		}),
		reader : new JsonReaderEx({
			idProperty : 'id',
			root : 'data',
			fields : fields
		}),
		writer : new Ext.data.JsonWriter(),
		listeners : {
			load : function(st, records) {
				for ( var i = 1; i <= 31; i++) {
					dataPanel.getColumnModel().setHidden(i + 5, true);
				}
				if (records && records.length) {
					var dailyNums = records[0].get('dailyNums');
					if (dailyNums) {
						for ( var key in dailyNums) {
							dataPanel.getColumnModel().setHidden(parseInt(key) + 5, false);
						}
					}
				}
			}
		}
	});

	var queryByItem = new Ext.form.ComboBox({
		fieldLabel : '统计项',
		hiddenName : 'queryByItem',
		valueField : 'id',
		displayField : 'text',
		mode : 'local',
		editable : false,
		typeAhead : true,
		loadMask : true,
		allowBlank : false,
		selectOnFocus : false,
		triggerAction : 'all',
		emptyText : '请选择统计项',
		width : 120,
		store : new Ext.data.SimpleStore({
			fields : [ "id", "text" ],
			data : [ [ 1, "时间(天)" ], [ 2, "时间(月)" ], [ 3, "时间段" ] ]
		}),
		listeners : {
			'select' : function(tp) {
				self.checkItem();
			}
		}
	});

	this.initTime = function() {
		this.time = new Ext.form.DateField({
			name : 'timeText',
			xtype : 'datefield',
			fieldLabel : '选择日期',
			emptyText : '选择日期',
			format : 'Y-m-d',
			editable : false,
			width : 100
		});
		this.endtime = new Ext.form.DateField({
			name : 'endTimeText',
			xtype : 'datefield',
			fieldLabel : '选择日期',
			emptyText : '选择日期',
			format : 'Y-m-d',
			editable : false,
			width : 100
		});
	};

	this.initToMonth = function() {
		this.toMonth = new DateFieldEx({
			name : 'toMonth',
			xtype : 'datefield',
			fieldLabel : '月份',
			emptyText : '请选择月份',
			format : 'Y-m',
			editable : false,
			width : 100
		});
	};

	var queryButton = new Ext.Button({
		text : '统计',
		iconCls : 'icon-search',
		handler : function() {
			if (self.queryByItem.getValue() == null || self.queryByItem.getValue() == '') {
				Ext.Msg.alert('错误提示', '请选择统计项');
				return;
			}
			self.countData(self.queryByItem.getValue());
		}
	});
	var exportButton = new Ext.Button({
		text : '导出结果',
		iconCls : 'icon-ontology-export',
		hidden : !Dashboard.u().allow('om.stat.itr.EXP'),
		handler : function() {
			var records = _store.getRange();
			if (records.length) {
				self.getEl().mask('正在导出数据...');
				var _params = Ext.apply({}, _store.baseParams);
				_params.isExport = 'true';
				Ext.Ajax.request({
					url : 'img-txt-rank!list.action',
					params : _params,
					success : function(resp) {
						self.getEl().unmask();
						if (!self.downloadIFrame) {
							self.downloadIFrame = self.getEl().createChild({
								tag : 'iframe',
								style : 'display:none;'
							});
						}
						self.downloadIFrame.dom.src = 'img-txt-rank!getExport.action?&_t=' + new Date().getTime();
					},
					failure : function(resp) {
						self.getEl().unmask();
						Ext.Msg.alert('错误', resp.responseText);
					}
				});
			} else {
				Ext.Msg.alert('提示', '无数据可供导出');
			}
		}
	});

	var fSet = new Ext.form.FieldSet({
		xtype : 'fieldset',
		border : false,
		style : 'padding : 3px;',
		items : [ {
			layout : 'column',
			labelAlign : "left",
			border : false,
			bodyStyle : 'padding-top:5px;background-color:' + sys_bgcolor,
			items : [ {
				border : false,
				items : queryButton
			}, {
				border : false,
				bodyStyle : 'margin-left:2px',
				items : exportButton
			}, {
				border : false,
				layout : 'column',
				html : '&nbsp;&nbsp;统计项:&nbsp;&nbsp;',
				bodyStyle : 'background-color:' + sys_bgcolor + ' margin-top:4px;'
			}, {
				border : false,
				items : queryByItem
			}, {
				border : false,
				layout : 'column',
				html : '&nbsp;&nbsp;统计条件:&nbsp;&nbsp;',
				bodyStyle : 'background-color:' + sys_bgcolor + ' margin-top:4px;'
			} ]
		} ]

	});

	var pagingBarConf = {
		store : _store,
		displayInfo : true,
		pageSize : _pageSize,
		prependButtons : true,
		beforePageText : '第',
		afterPageText : '页，共{0}页',
		displayMsg : '第{0}到{1}条记录，共{2}条',
		emptyMsg : "没有记录",
		customBtn : {
			text : '总数',
			tooltip : '计算总数',
			handler : function(btn) {
				if (_store.getCount() == 0) {
					this.updateInfo0('无数据');
					return false;
				}
				btn.setDisabled(true);
				var self = this;
				Ext.Ajax.request({
					url : 'img-txt-rank!total.action',
					params : Ext.urlEncode(_store.baseParams),
					success : function(resp) {
						var result = Ext.util.JSON.decode(resp.responseText);
						if (result && result.data)
							self.updateInfo0('总点击次数: ' + result.data);
						btn.setDisabled(false);
					},
					failure : function(resp) {
						Ext.Msg.alert('错误', resp.responseText);
						btn.setDisabled(false);
					}
				});
			}
		}
	};

	var pagingBar = new Ext.CustomPagingToolbar(pagingBarConf);

	var dataPanelCfg = {
		name : 'imgTxtRankPanel',
		border : false,
		store : _store,
		loadMask : true,
		columns : columns,
		stripeRows : true,
		columnLines : true,
		viewConfig : {
			forceFit : true
		},
		bbar : pagingBar
	};
	var dataPanel = new Ext.grid.GridPanel(Ext.applyIf(_cfg || {}, dataPanelCfg));

	var _chartStore = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({
			url : 'img-txt-rank!chart.action'
		}),
		reader : new JsonReaderEx({
			idProperty : 'id',
			root : 'data',
			fields : [ {
				name : 'item'
			}, {
				name : 'hitNum',
				type : 'int'
			}, {
				name : 'userNum',
				type : 'int'
			} ]
		})
	});
	this.chartStore = _chartStore;

	this.newLineChart = function(max, min) {
		return new Ext.chart.LineChart({
			store : _chartStore,
			url : '../ext/resources/charts.swf',
			xField : 'item',
			tipRenderer : function(chart, record, index, series) {
				if (series.yField == 'hitNum') {
					var _d = record.data.hitNum;
					return Ext.util.Format.number(_d, '0,0') + '次点击';
				} else if (series.yField == 'userNum') {
					return Ext.util.Format.number(record.data.userNum, '0,0') + '个独立用户数';
				}
			},
			chartStyle : {
				padding : 10,
				animationEnabled : true,
				font : {
					name : 'Tahoma',
					color : 0x444444,
					size : 11
				},
				dataTip : {
					padding : 5,
					border : {
						color : 0x99bbe8,
						size : 1
					},
					background : {
						color : 0xDAE7F6,
						alpha : .9
					},
					font : {
						name : 'Tahoma',
						color : 0x15428B,
						size : 10,
						bold : true
					}
				}
			},
			xAxis : (max && min) ? new Ext.chart.NumericAxis({
				maximum : max,
				minimum : min,
				majorUnit : 1,
				color : 0x69aBc8,
				majorTicks : {
					color : 0x69aBc8,
					length : 4
				},
				minorTicks : {
					color : 0x69aBc8,
					length : 2
				},
				majorGridLines : {
					size : 1,
					color : 0xeeeeee
				}
			}) : null,
			yAxis : new Ext.chart.NumericAxis({
				color : 0x69aBc8,
				majorTicks : {
					color : 0x69aBc8,
					length : 4
				},
				minorTicks : {
					color : 0x69aBc8,
					length : 2
				},
				majorGridLines : {
					size : 1,
					color : 0xdfe8f6
				}
			}),
			extraStyle : {
				legend : {
					display : 'right',
					padding : 3,
					font : {
						size : 9
					}
				}
			},
			series : [ {
				type : 'line',
				displayName : '点击数',
				yField : 'hitNum',
				style : {
					color : 0x15428B,
					size : 6
				}
			}, {
				type : 'line',
				displayName : '独立用户数',
				yField : 'userNum',
				style : {
					color : 0xEE00EE,
					size : 6
				}
			} ]
		});
	};

	var cfg = ({
		layout : 'border',
		border : false,
		items : [ {
			layout : 'form',
			border : false,
			region : 'north',
			height : 38,
			bodyStyle : 'border-bottom: 1px solid ' + sys_bdcolor + ' border-right: 1px solid ' + sys_bdcolor,
			buttonAlign : 'center',
			items : [ {
				layout : 'form',
				border : false,
				bodyStyle : 'background-color:' + sys_bgcolor,
				labelAlign : "center",
				items : [ fSet ]
			} ]
		}, {
			region : 'center',
			header : false,
			ref : 'centerPanel',
			frame : false,
			border : false,
			layout : 'fit',
			items : [ dataPanel ]
		}, {
			region : 'south',
			header : false,
			ref : 'southPanel',
			split : true,
			collapsible : true,
			collapseMode : 'mini',
			frame : false,
			border : false,
			height : 250
		} ]
	});

	ImgTxtRankPanel.superclass.constructor.call(this, cfg);

	this.queryByItem = queryByItem;
	this.getPageSize = function() {
		return _pageSize;
	}, this.fSet = fSet;
	this.store = _store;
};
Ext.extend(ImgTxtRankPanel, Ext.Panel, {
	checkItem : function() {
		var self = this;
		var data = this.queryByItem.getValue();
		self.cleanData(data);
		if (data == 1) {
			this.initTime();
			this.fSet.items.item(0).add(this.time);
			this.time.setValue(new Date());
			// this.fSet.items.item(0).add(this.getDimChooser());
		}
		if (data == 2) {
			this.initToMonth();
			this.fSet.items.item(0).add(this.toMonth);
			this.toMonth.setValue(new Date());
			// this.fSet.items.item(0).add(this.getDimChooser());
		}
		if (data == 3) {
			this.initTime();
			this.fSet.items.item(0).add(this.time);
			var bd = new Date();
			bd.setDate(bd.getDate() - 8);
			this.time.setValue(bd);
			this.fSet.items.item(0).add(this.endtime);
			this.endtime.setValue(new Date());
			// this.fSet.items.item(0).add(this.getDimChooser());
		}
		this.doLayout();
	},
	cleanData : function(data) {
		if (typeof (this.fSet.items.item(0).get(this.time)) != 'undefined') {
			this.time.getEl().dom.style.display = 'none';
		}
		if (typeof (this.fSet.items.item(0).get(this.endtime)) != 'undefined') {
			this.endtime.getEl().dom.style.display = 'none';
		}
		if (typeof (this.fSet.items.item(0).get(this.toMonth)) != 'undefined') {
			this.toMonth.getEl().dom.style.display = 'none';
		}
		// this.fSet.items.item(0).remove(this.multiCondition);
	},
	getDimChooser : function() {
		var multiCondition = Dashboard.createDimChooser(150);
		this.multiCondition = multiCondition;
		return multiCondition;
	},
	countData : function(data) {
		var date = new Date();
		var time = null, etime = null, month = null;
		if (data == 1) {
			if (this.time.getRawValue() == null || this.time.getRawValue() == '') {
				Ext.Msg.alert('错误提示', '请选择日期');
				return;
			}
			if (this.time.getValue().getTime() > Date.parseDate(date.format("Y-m-d"), 'Y-m-d').getTime()) {
				Ext.Msg.alert('错误提示', '只能选择今天以前的时间');
				return;
			}
			time = this.time.getRawValue();
		}
		if (data == 2) {
			if (this.toMonth.getValue() == null || this.toMonth.getValue() == '') {
				Ext.Msg.alert('错误提示', '请选择统计的月');
				return;
			} else {
				if (this.toMonth.getValue().getTime() > Date.parseDate(date.format("Y-m"), 'Y-m').getTime()) {
					Ext.Msg.alert('错误提示', '只能统计本月及之前的信息');
					return;
				}
				month = this.toMonth.getRawValue();
			}
		}
		if (data == 3) {
			if (!this.time.getRawValue() || !this.endtime.getRawValue) {
				Ext.Msg.alert('错误提示', '请正确选择时间段');
				return;
			}
			var bt = this.time.getValue().getTime();
			var et = this.endtime.getValue().getTime();
			if (bt > Date.parseDate(date.format("Y-m-d"), 'Y-m-d').getTime() || et > Date.parseDate(date.format("Y-m-d"), 'Y-m-d').getTime()) {
				Ext.Msg.alert('错误提示', '只能选择今天以前的时间');
				return;
			}
			var t_off = et - bt;
			if (t_off < 0) {
				Ext.Msg.alert('错误提示', '开始时间应该小于结束时间');
				return;
			} else if (t_off / (1000 * 3600 * 24) > 15) {
				Ext.Msg.alert('错误提示', '时间段跨度不可超过15天');
				return;
			}
			time = this.time.getRawValue();
			etime = this.endtime.getRawValue();
		}
		var self = this;
		var store = this.store;
		for ( var key in store.baseParams) {
			if (key && key.indexOf('data') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('time') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('month') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('multiCondition') != -1) {
				delete store.baseParams[key];
			}
		}
		// var multiCondition = this.multiCondition.getValue();
		var multiCondition = Ext.encode({
			platform : [ 'weixin' ]
		});
		if (data) {
			store.setBaseParam('data', data);
			this.chartStore.setBaseParam('data', data);
		}
		if (time) {
			store.setBaseParam('time', time);
			this.chartStore.setBaseParam('time', time);
		}
		if (etime) {
			store.setBaseParam('endtime', etime);
			this.chartStore.setBaseParam('endtime', etime);
		}
		if (month) {
			store.setBaseParam('month', month);
			this.chartStore.setBaseParam('month', month);
		}
		if (multiCondition) {
			store.setBaseParam('multiCondition', multiCondition);
			this.chartStore.setBaseParam('multiCondition', multiCondition);
		}
		self.southPanel.removeAll(true);
		this.store.load({
			params : {
				start : 0,
				limit : this.getPageSize(),
				'data' : data,
				'time' : time,
				'endtime' : etime,
				'month' : month,
				'multiCondition' : multiCondition
			},
			callback : function(recs) {
				self.max = null, self.min = null;
				if (data == 1) {
					self.max = 24, self.min = 1;
				} else if (data == 2) {
					var d = new Date();
					self.max = new Date(d.getFullYear(), d.getMonth() + 1, 0).getDate();
					self.min = 1;
				}
				self.chartStore.load({
					callback : function() {
						var chart = self.newLineChart(self.max, self.min);
						self.southPanel.add(chart);
						self.doLayout();
						self.getEl().unmask();
					}
				});
			}
		});
	}
});