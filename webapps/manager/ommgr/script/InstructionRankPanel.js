InstructionRankPanel = function(_cfg) {
	var self = this;
	var _pageSize = 20;
	var fields = [{
				type : 'int',
				name : 'times'
			}, {
				type : 'string',
				name : 'content'
			}, {
				type : 'string',
				name : 'createTime'
			}, {
				type : 'string',
				name : 'updateTime'
			}];
	var columns = [new Ext.grid.RowNumberer(), {
				dataIndex : 'content',
				header : '指令内容',
				align : 'center'
			}, {
				dataIndex : 'times',
				header : '转发次数',
				width : 45,
				align : 'center'
			}, {
				dataIndex : 'createTime',
				header : '首次转发时间',
				width : 80,
				align : 'center'
			}, {
				dataIndex : 'updateTime',
				header : '末次转发时间',
				width : 80,
				align : 'center'
			}];

	var _store = new Ext.data.Store({
				proxy : new Ext.data.HttpProxy({
							url : 'instruction-rank!list.action'
						}),
				reader : new JsonReaderEx({
							idProperty : 'id',
							root : 'data',
							fields : fields
						})
			});

	var queryByItem = new Ext.form.ComboBox({
				fieldLabel : '统计项',
				hiddenName : 'queryByItem',
				valueField : 'id',
				displayField : 'text',
				mode : 'local',
				editable : false,
				typeAhead : true,
				loadMask : true,
				allowBlank : false,
				selectOnFocus : false,
				triggerAction : 'all',
				emptyText : '请选择统计项',
				width : 120,
				store : new Ext.data.SimpleStore({
							fields : ["id", "text"],
							data : [[1, "时间(天)"], [2, "时间(月)"], [3, "时间段"]]
						}),
				listeners : {
					'select' : function(tp) {
						self.checkItem();
					}
				}
			});

	this.initTime = function() {
		this.time = new Ext.form.DateField({
					name : 'timeText',
					xtype : 'datefield',
					fieldLabel : '选择日期',
					emptyText : '选择日期',
					format : 'Y-m-d',
					editable : false,
					width : 120
				})
		this.endtime = new Ext.form.DateField({
					name : 'endTimeText',
					xtype : 'datefield',
					fieldLabel : '选择日期',
					emptyText : '选择日期',
					format : 'Y-m-d',
					editable : false,
					width : 120
				})
	}
	this.initToMonth = function() {
		this.toMonth = new DateFieldEx({
					name : 'toMonth',
					xtype : 'datefield',
					fieldLabel : '月份',
					emptyText : '请选择月份',
					format : 'Y-m',
					editable : false,
					width : 100
				})
	}

	var queryButton = new Ext.Button({
				text : '统计',
				iconCls : 'icon-search',
				handler : function() {
					if (self.queryByItem.getValue() == null
							|| self.queryByItem.getValue() == '') {
						Ext.Msg.alert('错误提示', '请选择统计项');
						return;
					}
					self.countData(self.queryByItem.getValue());
				}
			});
	var exportButton = new Ext.Button({
		text : '导出结果',
		iconCls : 'icon-ontology-export',
		handler : function() {
			var records = _store.getRange();
			if (records.length) {
				self.getEl().mask('正在导出数据...');
				Ext.Ajax.request({
					url : 'instruction-rank!export.action',
					params : _store.baseParams,
					success : function(resp) {
						self.getEl().unmask();
						if (!self.downloadIFrame) {
							self.downloadIFrame = self.getEl().createChild({
										tag : 'iframe',
										style : 'display:none;'
									})
						}
						self.downloadIFrame.dom.src = 'instruction-rank!getExport.action?&_t='
								+ new Date().getTime();
					},
					failure : function(resp) {
						self.getEl().unmask();
						Ext.Msg.alert('错误', resp.responseText);
					}
				});
			} else {
				Ext.Msg.alert('提示', '无数据可供导出')
			}
		}
	});

	var fSet = new Ext.form.FieldSet({
		xtype : 'fieldset',
		border : false,
		style : 'padding : 3px;',
		items : [{
					layout : 'column',
					labelAlign : "left",
					border : false,
					bodyStyle : 'padding-top:5px;background-color:' + sys_bgcolor,
					items : [{
								border : false,
								items : queryButton
							}, {
								border : false,
								bodyStyle : 'margin-left:2px',
								items : exportButton
							}, {
								border : false,
								layout : 'column',
								html : '&nbsp;&nbsp;统计项:&nbsp;&nbsp;',
								bodyStyle : 'background-color:' + sys_bgcolor + ' margin-top:4px;'
							}, {
								border : false,
								items : queryByItem
							}, {
								border : false,
								layout : 'column',
								html : '&nbsp;&nbsp;统计条件:&nbsp;&nbsp;',
								bodyStyle : 'background-color:' + sys_bgcolor + ' margin-top:4px;'
							}]
				}]

	});
	var pagingBar = new Ext.PagingToolbar({
				store : _store,
				displayInfo : true,
				pageSize : _pageSize,
				prependButtons : true,
				beforePageText : '第',
				afterPageText : '页，共{0}页',
				displayMsg : '第{0}到{1}条记录，共{2}条',
				emptyMsg : "没有记录"
			});

	var dataPanelCfg = {
		name : 'instructionRankPanel',
		border : false,
		store : _store,
		columns : columns,
		stripeRows : true,
		region : 'center',
		viewConfig : {
			forceFit : true
		},
		bbar : pagingBar
	}
	var dataPanel = new Ext.grid.GridPanel(Ext
			.applyIf(_cfg || {}, dataPanelCfg));

	var cfg = ({
		layout : 'border',
		border : false,
		items : [{
			layout : 'form',
			border : false,
			region : 'north',
			height : 38,
			bodyStyle : 'border-bottom: 1px solid ' + sys_bdcolor + ' border-right: 1px solid ' + sys_bdcolor,
			buttonAlign : 'center',
			items : [{
						layout : 'form',
						border : false,
						bodyStyle : 'background-color:' + sys_bgcolor,
						labelAlign : "center",
						items : [fSet]
					}]
		}, dataPanel]
	});

	InstructionRankPanel.superclass.constructor.call(this, cfg);

	this.queryByItem = queryByItem;
	this.getPageSize = function() {
		return _pageSize;
	}, this.fSet = fSet;
	this.store = _store;
}
Ext.extend(InstructionRankPanel, Ext.Panel, {
			checkItem : function() {
				var self = this;
				var data = this.queryByItem.getValue()
				self.cleanData(data);
				if (data == 1) {
					this.initTime();
					this.fSet.items.item(0).add(this.time);
					this.time.setValue(new Date());
				}
				if (data == 2) {
					this.initToMonth();
					this.fSet.items.item(0).add(this.toMonth);
					this.toMonth.setValue(new Date());
				}
				if (data == 3) {
					this.initTime();
					this.fSet.items.item(0).add(this.time);
					var bd = new Date();
					bd.setDate(bd.getDate() - 8);
					this.time.setValue(bd);
					this.fSet.items.item(0).add(this.endtime);
					this.endtime.setValue(new Date());
				}
				this.doLayout();
			},
			cleanData : function(data) {
				if (typeof(this.fSet.items.item(0).get(this.time)) != 'undefined') {
					this.time.getEl().dom.style.display = 'none';
				}
				if (typeof(this.fSet.items.item(0).get(this.endtime)) != 'undefined') {
					this.endtime.getEl().dom.style.display = 'none';
				}
				if (typeof(this.fSet.items.item(0).get(this.toMonth)) != 'undefined') {
					this.toMonth.getEl().dom.style.display = 'none';
				}
			},
			countData : function(data) {
				var date = new Date();
				var time, etime, month;
				if (data == 1) {
					if (this.time.getRawValue() == null
							|| this.time.getRawValue() == '') {
						Ext.Msg.alert('错误提示', '请选择日期')
						return;
					}
					if (this.time.getValue().getTime() > Date.parseDate(
							date.format("Y-m-d"), 'Y-m-d').getTime()) {
						Ext.Msg.alert('错误提示', '只能选择今天以前的时间')
						return;
					}
					time = this.time.getRawValue()
				}
				if (data == 2) {
					if (this.toMonth.getValue() == null
							|| this.toMonth.getValue() == '') {
						Ext.Msg.alert('错误提示', '请选择统计的月');
						return;
					} else {
						if (this.toMonth.getValue().getTime() > Date.parseDate(
								date.format("Y-m"), 'Y-m').getTime()) {
							Ext.Msg.alert('错误提示', '只能统计本月及之前的信息')
							return;
						}
						month = this.toMonth.getRawValue();
					}
				}
				if (data == 3) {
					if (!this.time.getRawValue() || !this.endtime.getRawValue) {
						Ext.Msg.alert('错误提示', '请正确选择时间段');
						return;
					}
					var bt = this.time.getValue().getTime();
					var et = this.endtime.getValue().getTime();
					if (bt > Date.parseDate(date.format("Y-m-d"), 'Y-m-d')
							.getTime()
							|| et > Date.parseDate(date.format("Y-m-d"),
									'Y-m-d').getTime()) {
						Ext.Msg.alert('错误提示', '只能选择今天以前的时间')
						return;
					}
					var t_off = et - bt;
					if (t_off < 0) {
						Ext.Msg.alert('错误提示', '开始时间应该小于结束时间');
						return;
					} else if (t_off / (1000 * 3600 * 24) > 15) {
						Ext.Msg.alert('错误提示', '时间段跨度不可超过15天');
						return;
					}
					time = this.time.getRawValue();
					etime = this.endtime.getRawValue();
				}
				var store = this.store;
				for (var key in store.baseParams) {
					if (key && key.indexOf('data') != -1) {
						delete store.baseParams[key];
					}
					if (key && key.indexOf('time') != -1) {
						delete store.baseParams[key];
					}
					if (key && key.indexOf('month') != -1) {
						delete store.baseParams[key];
					}
					if (key && key.indexOf('endtime') != -1) {
						delete store.baseParams[key];
					}
				}
				if (data)
					store.setBaseParam('data', data);
				if (time)
					store.setBaseParam('time', time);
				if (month)
					store.setBaseParam('month', month);
				if (etime)
					store.setBaseParam('endtime', etime);
				this.store.load({
							callback : function(recs) {
								if (recs.length == 0) {
									Ext.Msg.alert('提示', '没有相应数据');
								}
							},
							params : {
								start : 0,
								limit : this.getPageSize()
							}
						})
			}
		});