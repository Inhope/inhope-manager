KnoOrdPanel = function(_cfg) {
	var self = this;
	var _pageSize = 20;
	var fields = [ {
		name : 'orderBy'
	}, {
		name : 'knowledges'
	}, {
		name : 'isType'
	}, {
		name : 'couAcc'
	}, {
		name : 'faqId'
	}, {
		name : 'categoryName'
	}, {
		name : 'dailyNums',
		type : 'object'
	}, {
		name : 'couSolve'
	}, {
		name : 'couNso'
	} ];
	var columns = [ {
		dataIndex : 'orderBy',
		header : '排名',
		width : 100,
		align : 'center'
	}, {
		dataIndex : 'knowledges',
		header : '知识点',
		width : 300,
		align : 'left'
	}, {
		dataIndex : 'isType',
		header : '所属实例',
		width : 200,
		align : 'left'
	}, {
		dataIndex : 'categoryName',
		header : '所属分类',
		width : 120,
		align : 'left',
		hidden : !(Dashboard.miscConfig && Dashboard.miscConfig.categoryDepth > 0)
	}, {
		dataIndex : 'couAcc',
		header : '访问次数',
		width : 100,
		align : 'left'
	}, {
		dataIndex : 'couSolve',
		header : '解决数',
		width : 100,
		align : 'left',
		hidden : !Dashboard.miscConfig.faqRankWithVote
	}, {
		dataIndex : 'couNso',
		header : '未解决数',
		width : 100,
		align : 'left',
		hidden : !Dashboard.miscConfig.faqRankWithVote
	} ];

	var colsLen = columns.length;
	for ( var i = 1; i <= 31; i++) {
		columns.push(new Ext.grid.Column({
			dataIndex : '_dailyNum',
			header : i + '日',
			width : 95,
			hidden : true,
			align : 'center',
			renderer : function(v, meta, rec, rowIdx, colIdx) {
				return rec.get('dailyNums')[colIdx - colsLen + 1];
			}
		}));
	}

	var _store = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({
			url : 'cou-kno-data!list.action'
		}),
		reader : new JsonReaderEx({
			idProperty : 'id',
			root : 'data',
			fields : fields
		}),
		writer : new Ext.data.JsonWriter(),
		listeners : {
			load : function(st, records) {
				for ( var i = 1; i <= 31; i++) {
					dataPanel.getColumnModel().setHidden(i + 6, true);
				}
				if (records && records.length) {
					var dailyNums = records[0].get('dailyNums');
					if (dailyNums) {
						for ( var key in dailyNums) {
							dataPanel.getColumnModel().setHidden(parseInt(key) + 6, false);
						}
					}
				}
			}
		}
	});

	var queryByItem = new Ext.form.ComboBox({
		fieldLabel : '统计项',
		hiddenName : 'queryByItem',
		valueField : 'id',
		displayField : 'text',
		mode : 'local',
		editable : false,
		typeAhead : true,
		loadMask : true,
		allowBlank : false,
		selectOnFocus : false,
		triggerAction : 'all',
		emptyText : '请选择统计项',
		width : 100,
		store : new Ext.data.SimpleStore({
			fields : [ "id", "text" ],
			data : [ [ 1, "时间(天)" ], [ 2, "时间(月)" ], [ 3, "时间段" ] ]
		}),
		listeners : {
			'select' : function(tp) {
				self.checkItem();
			}
		}
	});

	this.initTime = function() {
		this.time = new Ext.form.DateField({
			name : 'timeText',
			xtype : 'datefield',
			fieldLabel : '选择日期',
			emptyText : '选择日期',
			format : 'Y-m-d',
			editable : false,
			width : 90
		});
		this.endtime = new Ext.form.DateField({
			name : 'endTimeText',
			xtype : 'datefield',
			fieldLabel : '选择日期',
			emptyText : '选择日期',
			format : 'Y-m-d',
			editable : false,
			width : 90
		});
	};

	this.initToMonth = function() {
		this.toMonth = new DateFieldEx({
			name : 'toMonth',
			xtype : 'datefield',
			fieldLabel : '月份',
			emptyText : '请选择月份',
			format : 'Y-m',
			editable : false,
			width : 90
		});
	};

	var queryButton = new Ext.Button({
		text : '统计',
		iconCls : 'icon-search',
		handler : function() {
			if (self.queryByItem.getValue() == null || self.queryByItem.getValue() == '') {
				Ext.Msg.alert('错误提示', '请选择统计项');
				return;
			}
			self.countData(self.queryByItem.getValue());
		}
	});
	var exportButton = new Ext.Button({
		text : '导出结果',
		iconCls : 'icon-ontology-export',
		hidden : !Dashboard.u().allow('om.stat.kr.EXP'),
		handler : function() {
			var records = _store.getRange();
			if (records.length) {
				self.getEl().mask('正在导出数据...');
				var _params = Ext.apply({}, _store.baseParams);
				_params.isExport = 'true';
				Ext.Ajax.request({
					url : 'cou-kno-data!list.action',
					params : _params,
					success : function(resp) {
						self.getEl().unmask();
						if (!self.downloadIFrame) {
							self.downloadIFrame = self.getEl().createChild({
								tag : 'iframe',
								style : 'display:none;'
							});
						}
						self.downloadIFrame.dom.src = 'cou-kno-data!getExport.action?&_t=' + new Date().getTime();
					},
					failure : function(resp) {
						self.getEl().unmask();
						Ext.Msg.alert('错误', resp.responseText);
					}
				});
			} else {
				Ext.Msg.alert('提示', '无数据可供导出');
			}
		}
	});

	this.initTriggerSourceCombo = function() {
		this.triggerSourceCombo = new Ext.form.ComboBox({
			fieldLabel : '来源',
			hiddenName : 'triggerSource',
			valueField : 'id',
			displayField : 'text',
			mode : 'local',
			editable : false,
			typeAhead : true,
			loadMask : true,
			allowBlank : true,
			selectOnFocus : false,
			triggerAction : 'all',
			emptyText : '请选择来源',
			width : 90,
			store : new Ext.data.SimpleStore({
				fields : [ "id", "text" ],
				data : [ [ -1, '全部' ], [ 1, "用户输入" ], [ 2, "智能提示" ], [ 4, "建议问" ], [ 5, "3*5菜单" ] ]
			}),
			listeners : {
				'select' : function(tp) {
				}
			}
		});
	}

	this.initCategoryTreeCombo = function() {
		this.categoryTreeCombo = new TreeComboBox({
			emptyText : '请选择知识分类',
			editable : false,
			anchor : '100%',
			name : 'category',
			allowBlank : true,
			minHeight : 250,
			width : 210,
			root : {
				id : '0',
				text : 'root',
				iconCls : 'icon-ontology-root'
			},
			loader : new Ext.tree.TreeLoader({
				dataUrl : 'ontology-category!list.action?ignoreObjects=true'
			})
		});
	}

	var fSet = new Ext.form.FieldSet({
		xtype : 'fieldset',
		border : false,
		style : 'padding : 3px;',
		items : [ {
			layout : 'column',
			labelAlign : "left",
			border : false,
			bodyStyle : 'padding-top:5px;background-color:' + sys_bgcolor,
			items : [ {
				border : false,
				items : queryButton
			}, {
				border : false,
				bodyStyle : 'margin-left:2px',
				items : exportButton
			}, {
				border : false,
				layout : 'column',
				html : '&nbsp;&nbsp;统计项:&nbsp;&nbsp;',
				bodyStyle : 'background-color:' + sys_bgcolor + ' margin-top:4px;'
			}, {
				border : false,
				items : queryByItem
			}, {
				border : false,
				layout : 'column',
				html : '&nbsp;&nbsp;统计条件:&nbsp;&nbsp;',
				bodyStyle : 'background-color:' + sys_bgcolor + ' margin-top:4px;'
			} ]
		} ]

	});

	var pagingBarConf = {
		store : _store,
		displayInfo : true,
		pageSize : _pageSize,
		prependButtons : true,
		beforePageText : '第',
		afterPageText : '页，共{0}页',
		displayMsg : '第{0}到{1}条记录，共{2}条',
		emptyMsg : "没有记录",
		customBtn : {
			text : '总数',
			tooltip : '计算总数',
			handler : function(btn) {
				if (_store.getCount() == 0) {
					this.updateInfo0('无数据');
					return false;
				}
				btn.setDisabled(true);
				var self = this;
				Ext.Ajax.request({
					url : 'cou-kno-data!total.action',
					params : Ext.urlEncode(_store.baseParams),
					success : function(resp) {
						var result = Ext.util.JSON.decode(resp.responseText);
						if (result && result.data)
							self.updateInfo0('总提问次数: ' + result.data);
						btn.setDisabled(false);
					},
					failure : function(resp) {
						Ext.Msg.alert('错误', resp.responseText);
						btn.setDisabled(false);
					}
				});
			}
		}
	};

	var pagingBar = new Ext.CustomPagingToolbar(pagingBarConf);

	var dataPanelCfg = {
		name : 'KnoOrdPanel',
		border : false,
		store : _store,
		loadMask : true,
		columns : columns,
		stripeRows : true,
		columnLines : true,
		viewConfig : {
			forceFit : false
		},
		bbar : pagingBar
	};
	var dataPanel = new Ext.grid.GridPanel(Ext.applyIf(_cfg || {}, dataPanelCfg));

	var cfg = ({
		layout : 'border',
		border : false,
		items : [ {
			layout : 'form',
			border : false,
			region : 'north',
			height : 38,
			bodyStyle : 'border-bottom: 1px solid ' + sys_bdcolor + ' border-right: 1px solid ' + sys_bdcolor,
			buttonAlign : 'center',
			items : [ {
				layout : 'form',
				border : false,
				bodyStyle : 'background-color:' + sys_bgcolor,
				labelAlign : "center",
				items : [ fSet ]
			} ]
		}, {
			region : 'center',
			header : false,
			ref : 'centerPanel',
			frame : false,
			border : false,
			layout : 'fit',
			items : [ dataPanel ]
		} ]
	});

	KnoOrdPanel.superclass.constructor.call(this, cfg);

	this.queryByItem = queryByItem;
	this.getPageSize = function() {
		return _pageSize;
	}, this.fSet = fSet;
	this.store = _store;
};
Ext.extend(KnoOrdPanel, Ext.Panel, {
	checkItem : function() {
		var self = this;
		var data = this.queryByItem.getValue();
		self.cleanData(data);
		if (data == 1) {
			this.initTime();
			this.fSet.items.item(0).add(this.time);
			this.time.setValue(new Date());
			this.fSet.items.item(0).add(this.getDimChooser());
		}
		if (data == 2) {
			this.initToMonth();
			this.fSet.items.item(0).add(this.toMonth);
			this.toMonth.setValue(new Date());
			this.fSet.items.item(0).add(this.getDimChooser());
		}
		if (data == 3) {
			this.initTime();
			this.fSet.items.item(0).add(this.time);
			var bd = new Date();
			bd.setDate(bd.getDate() - 30);
			this.time.setValue(bd);
			this.fSet.items.item(0).add(this.endtime);
			this.endtime.setValue(new Date());
			this.fSet.items.item(0).add(this.getDimChooser());
		}
		this.initTriggerSourceCombo();
		this.fSet.items.item(0).add(this.triggerSourceCombo);
		if (Dashboard.miscConfig && Dashboard.miscConfig.categoryDepth) {
			this.initCategoryTreeCombo();
			this.fSet.items.item(0).add(this.categoryTreeCombo);
		}
		this.doLayout();
	},
	cleanData : function(data) {
		if (typeof (this.fSet.items.item(0).get(this.time)) != 'undefined') {
			this.time.getEl().dom.style.display = 'none';
		}
		if (typeof (this.fSet.items.item(0).get(this.endtime)) != 'undefined') {
			this.endtime.getEl().dom.style.display = 'none';
		}
		if (typeof (this.fSet.items.item(0).get(this.toMonth)) != 'undefined') {
			this.toMonth.getEl().dom.style.display = 'none';
		}
		if (typeof (this.fSet.items.item(0).get(this.triggerSourceCombo)) != 'undefined') {
			this.triggerSourceCombo.getEl().dom.style.display = 'none';
		}
		if (typeof (this.fSet.items.item(0).get(this.categoryTreeCombo)) != 'undefined') {
			this.categoryTreeCombo.getEl().dom.style.display = 'none';
		}
		this.fSet.items.item(0).remove(this.triggerSourceCombo);
		this.fSet.items.item(0).remove(this.categoryTreeCombo);
		this.fSet.items.item(0).remove(this.multiCondition);
	},
	getDimChooser : function() {
		var multiCondition = Dashboard.createDimChooser(150);
		this.multiCondition = multiCondition;
		return multiCondition;
	},
	countData : function(data) {
		var date = new Date();
		var time = null, etime = null, month = null;
		if (data == 1) {
			if (this.time.getRawValue() == null || this.time.getRawValue() == '') {
				Ext.Msg.alert('错误提示', '请选择日期');
				return;
			}
			if (this.time.getValue().getTime() > Date.parseDate(date.format("Y-m-d"), 'Y-m-d').getTime()) {
				Ext.Msg.alert('错误提示', '只能选择今天以前的时间');
				return;
			}
			time = this.time.getRawValue();
		}
		if (data == 2) {
			if (this.toMonth.getValue() == null || this.toMonth.getValue() == '') {
				Ext.Msg.alert('错误提示', '请选择统计的月');
				return;
			} else {
				if (this.toMonth.getValue().getTime() > Date.parseDate(date.format("Y-m"), 'Y-m').getTime()) {
					Ext.Msg.alert('错误提示', '只能统计本月及之前的信息');
					return;
				}
				month = this.toMonth.getRawValue();
			}
		}
		if (data == 3) {
			if (!this.time.getRawValue() || !this.endtime.getRawValue) {
				Ext.Msg.alert('错误提示', '请正确选择时间段');
				return;
			}
			var bt = this.time.getValue().getTime();
			var et = this.endtime.getValue().getTime();
			if (bt > Date.parseDate(date.format("Y-m-d"), 'Y-m-d').getTime() || et > Date.parseDate(date.format("Y-m-d"), 'Y-m-d').getTime()) {
				Ext.Msg.alert('错误提示', '只能选择今天以前的时间');
				return;
			}
			var t_off = et - bt;
			if (t_off < 0) {
				Ext.Msg.alert('错误提示', '开始时间应该小于结束时间');
				return;
			} else if (t_off / (1000 * 3600 * 24) > 30) {
				Ext.Msg.alert('错误提示', '时间段跨度不可超过30天');
				return;
			}
			time = this.time.getRawValue();
			etime = this.endtime.getRawValue();
		}
		var self = this;
		var store = this.store;
		for ( var key in store.baseParams) {
			if (key && key.indexOf('data') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('time') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('month') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('multiCondition') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('categoryId') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('inputType') != -1) {
				delete store.baseParams[key];
			}
		}
		var multiCondition = this.multiCondition.getValue();
		if (data)
			store.setBaseParam('data', data);
		if (time)
			store.setBaseParam('time', time);
		if (etime)
			store.setBaseParam('endtime', etime);
		if (month)
			store.setBaseParam('month', month);
		if (multiCondition)
			store.setBaseParam('multiCondition', multiCondition);
		var categoryId = null;
		if (this.categoryTreeCombo)
			categoryId = this.categoryTreeCombo.getValue();
		if (categoryId)
			store.setBaseParam('categoryId', categoryId);
		var source = this.triggerSourceCombo.getValue();
		if (source)
			store.setBaseParam('inputType', source);
		this.store.load({
			params : {
				start : 0,
				limit : this.getPageSize(),
				'data' : data,
				'time' : time,
				'endtime' : etime,
				'month' : month,
				'multiCondition' : multiCondition,
				'categoryId' : categoryId,
				'inputType' : source
			},
			callback : function(recs) {
				self.getEl().unmask();
			}
		});
	}
});