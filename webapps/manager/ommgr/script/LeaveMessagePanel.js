LeaveMessagePanel = function() {
	var self = this;
	this.LeaveMessageRecord = Ext.data.Record.create([ 'id', 'userId', 'messageContent', 'replyAddress', 'replyType', 'createTime' ]);
	var replyTypeData = [ [ 1, 'Email' ], [ 2, '短信' ] ];
	var replyTypeMap = {};
	for ( var i = 0; i < replyTypeData.length; i++)
		replyTypeMap[replyTypeData[i][0]] = replyTypeData[i][1];
	var store = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({
			url : 'feedback!listLeaveMessage.action'
		}),
		autoLoad : true,
		reader : new Ext.data.JsonReader({
			idProperty : 'id',
			root : 'data',
			fields : this.LeaveMessageRecord
		})
	});
	var pagingBar = new Ext.PagingToolbar({
		store : store,
		displayInfo : true,
		pageSize : 20,
		prependButtons : true,
		beforePageText : '第',
		afterPageText : '页，共{0}页',
		displayMsg : '第{0}到{1}条记录，共{2}条',
		emptyMsg : "没有记录"
	});
	var beginTimeField = new ClearableDateTimeField({
		editable : false,
		name : 'btime',
		width : 160,
		validator : function(v) {
			if (v) {
				var ev = endTimeField.getValue();
				if (ev && v.getTime() >= ev.getTime())
					return '开始时间不能晚于结束时间';
			}
			return true;
		}
	});
	this.beginTimeField = beginTimeField;
	var endTimeField = new ClearableDateTimeField({
		editable : false,
		name : 'etime',
		width : 160
	});
	this.endTimeField = endTimeField;

	var config = {
		id : 'leaveMessagePanel',
		store : store,
		columnLines : true,
		margins : '0 5 5 5',
		border : false,
		style : 'border-right: 1px solid ' + sys_bdcolor,
		tbar : [ '从', beginTimeField, '到', endTimeField, '用户名:', {
			id : 's_lm_user',
			name : 'user',
			xtype : 'textfield',
			width : 90,
			emptyText : '输入用户名'
		}, '留言:', {
			id : 's_lm_message',
			name : 'msg',
			xtype : 'textfield',
			width : 90,
			emptyText : '输入关键字'
		}, '答复类型:', new Ext.form.ComboBox({
			id : 's_lm_type',
			name : 'atype',
			triggerAction : 'all',
			width : 85,
			editable : false,
			value : '',
			mode : 'local',
			store : new Ext.data.ArrayStore({
				fields : [ 'type', 'displayText' ],
				data : [ [ 0, '全部' ] ].concat(replyTypeData)
			}),
			valueField : 'type',
			displayField : 'displayText'
		}), ' ', {
			text : '搜索',
			iconCls : 'icon-search',
			handler : function() {
				self.search();
			}
		}, {
			text : '重置',
			icon : 'images/cross.gif',
			handler : function() {
				this.getTopToolbar().findBy(function(cmp) {
					if (cmp.name)
						cmp.reset();
				});
			}.dg(this)
		}, {
			iconCls : 'icon-ontology-export',
			text : '导出',
			hidden : !Dashboard.u().allow('om.log.lw.EXP'),
			handler : function() {
				self.expData();
			}
		}, {
			iconCls : 'icon-delete',
			text : '删除',
			handler : function() {
				var rows = self.getSelectionModel().getSelections();
				if (rows.length == 0) {
					Ext.Msg.alert('提示', '请至少选择一条记录！');
					return false;
				}
				Ext.Msg.confirm('提示', '确认删除这' + rows.length + '条记录？', function(btn) {
					if (btn == 'yes') {
						var ids = [];
						Ext.each(rows, function() {
							ids.push(this.id);
						});
						self.getEl().mask('正在删除...');
						Ext.Ajax.request({
							url : 'feedback!deleteLeaveMessage.action',
							params : {
								ids : ids.join(',')
							},
							success : function(resp) {
								self.getEl().unmask();
								Dashboard.setAlert('删除成功');
								store.reload();
							},
							failure : function(resp) {
								self.getEl().unmask();
								Ext.Msg.alert('错误', resp.responseText);
							}
						});
					}
				});
			}
		}
		// , {
		// iconCls : 'icon-refresh',
		// text : '刷新',
		// handler : function() {
		// store.reload();
		// }
		// }
		],
		bbar : pagingBar,
		sm : new Ext.grid.RowSelectionModel(),
		loadMask : true,
		autoExpandColumn : 'messageCol',
		colModel : new Ext.grid.ColumnModel({
			columns : [ new Ext.grid.RowNumberer(), {
				width : 100,
				header : '用户名',
				dataIndex : 'userId'
			}, {
				header : '留言',
				id : 'messageCol',
				dataIndex : 'messageContent'
			}, {
				header : '留言时间',
				width : 130,
				dataIndex : 'createTime'
			}, {
				header : '答复类型',
				width : 65,
				dataIndex : 'replyType',
				renderer : function(v) {
					return replyTypeMap[v];
				}
			}, {
				header : '答复地址',
				width : 200,
				dataIndex : 'replyAddress'
			} ]
		})
	};
	LeaveMessagePanel.superclass.constructor.call(this, config);
};

Ext.extend(LeaveMessagePanel, Ext.grid.GridPanel, {
	search : function(showAll) {
		this.tbarEx = this.getTopToolbar();
		var _userField = this.tbarEx.findById('s_lm_user');
		var _msgField = this.tbarEx.findById('s_lm_message');
		var _typeCombo = this.tbarEx.findById('s_lm_type');
		var _bdateField = this.beginTimeField;
		var _edateField = this.endTimeField;
		var store = this.getStore();
		for ( var key in store.baseParams) {
			if (key && key.indexOf('leaveMessageParam.') != -1)
				delete store.baseParams[key];
		}
		var _user = _userField.getValue().trim();
		var _message = _msgField.getValue();
		var _type = _typeCombo.getValue();
		var _bdate = _bdateField.getValue();
		var _edate = _edateField.getValue();
		if (_user)
			store.setBaseParam('leaveMessageParam.userId', _user);
		if (_message)
			store.setBaseParam('leaveMessageParam.messageContent', _message);
		if (_type)
			store.setBaseParam('leaveMessageParam.replyType', _type);
		if (_bdate)
			store.setBaseParam('leaveMessageParam.beginTime', _bdate);
		if (_edate)
			store.setBaseParam('leaveMessageParam.endTime', _edate);
		store.load();
	},
	expData : function() {
		this.tbarEx = this.getTopToolbar();
		var _userField = this.tbarEx.findById('s_lm_user');
		var _msgField = this.tbarEx.findById('s_lm_message');
		var _typeCombo = this.tbarEx.findById('s_lm_type');
		var _bdateField = this.beginTimeField;
		var _edateField = this.endTimeField;
		var _user = _userField.getValue().trim();
		var _message = _msgField.getValue();
		var _type = _typeCombo.getValue();
		var _bdate = _bdateField.getValue();
		var _edate = _edateField.getValue();
		var params = {
			'leaveMessageParam.userId' : _user,
			'leaveMessageParam.messageContent' : _message,
			'leaveMessageParam.beginTime' : _bdate,
			'leaveMessageParam.endTime' : _edate,
			'exp' : true
		};
		if (_type)
			params['leaveMessageParam.replyType'] = _type;
		Ext.Ajax.request({
			url : 'feedback!listLeaveMessage.action',
			params : Ext.urlEncode(params),
			success : function(response) {
				var result = Ext.util.JSON.decode(response.responseText);
				var timer = new ProgressTimer({
					initData : result.data,
					progressId : 'leaveMessagefeedbackStatus',
					boxConfig : {
						title : '正在导出用户留言统计...'
					},
					finish : function() {
						if (!this.downloadIFrame) {
							this.downloadIFrame = this.getEl().createChild({
								tag : 'iframe',
								style : 'display:none;'
							});
						}
						this.downloadIFrame.dom.src = 'feedback!exportLeaveMessageExcelData.action?ts=' + new Date().getTime();
					},
					scope : this
				});
				timer.start();
			},
			failure : function(response) {
				if (response.responseText)
					Ext.Msg.alert('出错了', response.responseText.replace(/\r\n/ig, '<br>'));
			},
			scope : this
		});
	}
});
