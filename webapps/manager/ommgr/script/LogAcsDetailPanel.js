Ext.namespace("LogAcsDetailPanel");
LogAcsDetailPanel = function(_cfg) {
	var self = this;
	this.tableName = 'OM_LOG_ACS_DETAIL';
	this.isChangeTable = false;
	this.tagSize = 0;
	this.data = {};
	this.multiCondition;
	this.extraAttrs = {};
	var _pageSize = 20;

	var reader = new Ext.data.JsonReader({
		idProperty : 'id',
		root : 'data',
		fields : [ {
			name : 'id',
			type : 'string'
		}, {
			name : 'userid',
			type : 'string'
		}, {
			name : 'visittime',
			type : 'string'
		}, {
			name : 'messagetype',
			type : 'string'
		}, {
			name : 'messagecontent',
			type : 'string'
		}, {
			name : 'cityName',
			type : 'string'
		}, {
			name : 'brand',
			type : 'string'
		}, {
			name : 'channels',
			type : 'string'
		} ]
	});
	var _store = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({
			url : 'log-acs-detail!listEx.action'
		}),
		reader : reader
	});

	var pagingBar = new Ext.PagingToolbarEx({
		store : _store,
		pageSize : _pageSize,
		tableName : this.tableName,
		isChangeTable : this.isChangeTable,
		prependButtons : true,
		tagSize : this.tagSize,
		displayMsg : '第{0}到{1}条记录',
		emptyMsg : "没有记录",
		listeners : {
			change : function() {
				var object = reader.jsonData;
				if (object && object.tableName && object.tableName.length) {
					if (this.tableName != "OM_LOG_ACS_DETAIL" && this.tableName != object.tableName) {
						this.isChangeTable = true;
					} else {
						this.isChangeTable = false;
					}
					if (object.tableName == null || object.tableName == 'null') {
					} else {
						this.tableName = object.tableName;
					}
				}
				if (object && object.tagSize) {
					if (object.tagSize != 0) {
						this.tagSize = object.tagSize;
					}
				}
			}
		}
	});
	pagingBar.setCountHandler(function(btn) {
		btn.setDisabled(true);
		pagingBar.updateInfo('正在计数...');
		Ext.Ajax.request({
			url : 'log-acs-detail!count.action',
			params : Ext.urlEncode(this.getStore().baseParams),
			success : function(response) {
				var result = Ext.util.JSON.decode(response.responseText);
				if (result && result.data != null) {
					pagingBar.updateInfo('共' + result.data + '条记录');
				}
				btn.setDisabled(false);
			},
			failure : function() {
				pagingBar.updateInfo('计数失败');
				btn.setDisabled(false);
			}
		});
	}, this);
	this.pagingBar = pagingBar;

	var _sm = new Ext.grid.RowSelectionModel();

	var userIdT = new Ext.form.TextField({
		fieldLabel : '用户Id',
		name : 'userId',
		xtype : 'textfield',
		allowBlank : true,
		width : 100
	});

	var _days = [];
	var daysOfMonth = Dashboard.getDaysOfMonth();
	for ( var i = 1; i <= daysOfMonth; i++)
		_days.push([ i ]);
	var _startDayCombo = new Ext.form.ComboBox({
		valueField : 'day',
		displayField : 'day',
		mode : 'local',
		editable : true,
		selectOnFocus : false,
		triggerAction : 'all',
		validator : function(val) {
			val = val.trim();
			if (val) {
				if (/^[0-9]+$/.test(val)) {
					var intVal = parseInt(val);
					if (intVal > 0 && intVal <= 31)
						return true;
				}
				return false;
			}
			return true;
		},
		width : 50,
		listWidth : 50,
		store : new Ext.data.ArrayStore({
			fields : [ 'day' ],
			data : _days
		})
	});
	this.startDayCombo = _startDayCombo;
	var _endDayCombo = new Ext.form.ComboBox({
		valueField : 'day',
		displayField : 'day',
		mode : 'local',
		editable : true,
		validator : function(val) {
			val = val.trim();
			if (val) {
				if (/^[0-9]+$/.test(val)) {
					var intVal = parseInt(val);
					if (intVal > 0 && intVal <= 31)
						return true;
				}
				return false;
			}
			return true;
		},
		selectOnFocus : false,
		triggerAction : 'all',
		width : 50,
		listWidth : 50,
		store : new Ext.data.ArrayStore({
			fields : [ 'day' ],
			data : _days
		})
	});
	this.endDayCombo = _endDayCombo;
	var _monthCombo = new DateFieldEx({
		emptyText : '请选择月份',
		format : 'Y-m',
		value : new Date(),
		editable : false,
		width : 80
	});
	_monthCombo.on('change', function(c, nv) {
		var daysOfMonth = Dashboard.getDaysOfMonth(nv);
		_days = [];
		for ( var i = 1; i <= daysOfMonth; i++)
			_days.push([ i ]);
		_startDayCombo.getStore().loadData(_days);
		_endDayCombo.getStore().loadData(_days);
	});
	this.monthCombo = _monthCombo;

	var messageTypeCombo = new Ext.form.ComboBox({
		fieldLabel : '消息类型',
		name : 'mtype',
		hiddenName : 'messageType',
		valueField : 'id',
		displayField : 'text',
		mode : 'local',
		editable : false,
		typeAhead : true,
		loadMask : true,
		allowBlank : false,
		selectOnFocus : false,
		triggerAction : 'all',
		value : '-1',
		width : 90,
		store : new Ext.data.SimpleStore({
			fields : [ "id", "text" ],
			data : [ [ -1, "所有类型" ], [ 0, "打开" ], [ 1, "上行" ], [ 2, "下行" ], [ 3, "关闭" ] ]
		})
	});

	var multiCondition = Dashboard.createDimChooser(150);
	this.multiCondition = multiCondition;

	var logSessionWindow = new DetailWindow({
		title : '人工客户详细信息'
	});

	this.userIdT = userIdT;
	this.messageTypeCombo = messageTypeCombo;
	var initDate = new Date();
	initDate.setDate(1);
	initDate.setHours(0, 0, 0, 0);
	var beginTimeField = new ClearableDateTimeField({
		editable : false,
		name : 'btime',
		value : initDate,
		width : 160,
		validator : function(v) {
			if (v) {
				var ev = endTimeField.getValue();
				if (ev && v.getTime() >= ev.getTime())
					return '开始时间不能晚于结束时间';
			}
			return true;
		}
	});
	this.beginTimeField = beginTimeField;
	var endTimeField = new ClearableDateTimeField({
		editable : false,
		name : 'etime',
		width : 160
	});
	this.endTimeField = endTimeField;
	beginTimeField.fireEvent('change', beginTimeField, initDate);

	var _exportItems = [ new Ext.form.ComboBox({
		ref : 'exportNum',
		valueField : 'num',
		displayField : 'num',
		mode : 'local',
		editable : true,
		selectOnFocus : false,
		triggerAction : 'all',
		validator : function(val) {
			val = val.trim();
			if (val) {
				if (/^[0-9]+$/.test(val)) {
					// var intVal = parseInt(val);
					// if (intVal >= 100 && intVal <= 8000)
					return true;
				}
				return '请正确输入数字';
			}
			return true;
		},
		width : 100,
		listWidth : 100,
		value : 8000,
		store : new Ext.data.ArrayStore({
			fields : [ 'num' ],
			data : [ [ 100 ], [ 500 ], [ 1000 ], [ 2000 ], [ 5000 ], [ 8000 ] ]
		})
	}), '-', {
		text : '用户ID',
		checked : true,
		value : 1
	}, {
		text : '访问时间',
		checked : true,
		value : 2
	}, {
		text : '消息类型',
		checked : true,
		value : 4
	}, {
		text : '消息内容',
		checked : true,
		value : 8
	}, {
		text : Dashboard.dimensionNames.platform,
		checked : true,
		value : 64
	} ];
	if (Dashboard.dimensionNames.location)
		_exportItems.push({
			text : Dashboard.dimensionNames.location,
			checked : true,
			value : 16
		});
	if (Dashboard.dimensionNames.brand)
		_exportItems.push({
			text : Dashboard.dimensionNames.brand,
			checked : true,
			value : 32
		});

	var _columns = [ new Ext.grid.RowNumberer(),
	// {
	// header : 'id',
	// dataIndex : 'id',
	// hidden : true
	// },
	{
		header : '用户ID',
		dataIndex : 'userid',
		sortable : true
	}, {
		header : '访问时间',
		dataIndex : 'visittime',
		sortable : true
	}, {
		width : 35,
		header : '消息类型',
		dataIndex : 'messagetype',
		sortable : true
	}, {
		header : '消息内容',
		dataIndex : 'messagecontent',
		sortable : true
	}, {
		width : 50,
		header : Dashboard.dimensionNames.platform,
		dataIndex : 'channels',
		sortable : true
	} ];
	if (Dashboard.dimensionNames.location)
		_columns.push({
			width : 50,
			header : Dashboard.dimensionNames.location,
			dataIndex : 'cityName',
			sortable : true,
			renderer : function(v) {
				v = v.trim();
				if (!v)
					return 'N/A';
				return v;
			}
		});
	if (Dashboard.dimensionNames.brand)
		_columns.push({
			width : 40,
			header : Dashboard.dimensionNames.brand,
			dataIndex : 'brand',
			sortable : true,
			renderer : function(v) {
				v = v.trim();
				if (!v)
					return 'N/A';
				return v;
			}
		});

	var cfg = {
		columnLines : true,
		store : _store,
		margins : '0 5 5 5',
		border : false,
		style : 'border-right: 1px solid ' + sys_bdcolor,
		tbar : new Ext.Toolbar({
			items : [ '从', beginTimeField, '到', endTimeField, '用户ID:', self.userIdT, '消息类型:', messageTypeCombo, '维度:', multiCondition, {
				text : '搜索',
				iconCls : 'icon-search',
				handler : function() {
					self.search();
				}
			}, {
				text : '重置',
				icon : 'images/cross.gif',
				handler : function() {
					this.getTopToolbar().findBy(function(cmp) {
						if (cmp.name)
							cmp.reset();
					});
					multiCondition.setValue({});
				}.dg(this)
			}, {
				text : '导出',
				xtype : 'splitbutton',
				ref : 'exportBtn',
				hidden : !Dashboard.u().allow('om.log.acs.EXP'),
				menu : {
					defaults : {
						hideOnClick : false
					},
					items : _exportItems
				},
				iconCls : 'icon-ontology-export',
				handler : function() {
					self.exportData(true);
				}
			} ]
		}),
		columns : _columns,
		sm : _sm,
		bbar : pagingBar,
		loadMask : true,
		viewConfig : {
			forceFit : true
		}
	};
	LogAcsDetailPanel.superclass.constructor.call(this, Ext.applyIf(_cfg || {}, cfg));
	this.getPageSize = function() {
		return _pageSize;
	};

	/**
	 * this.addListener('rowdblclick', function() { var logDetailId =
	 * this.sm.getSelected().data.id; self.queryData(logDetailId); }, this);
	 */

	this.sm = _sm;
	this.store = _store;

	this.logSessionWindow = logSessionWindow;
	this.userIdT = userIdT;
	this.messageTypeCombo = messageTypeCombo;
};

Ext.extend(LogAcsDetailPanel, Ext.grid.GridPanel, {
	loadData : function() {
		this.getBottomToolbar().reset0();
		var store = this.getStore();
		var bt = this.beginTimeField.getValue().format('Y-m-d H:i:s');
		store.baseParams['beginTime'] = bt;
		store.load({
			params : {
				beginTime : bt,
				start : 0,
				limit : this.getPageSize(),
				tagsize : 0,
				isnext : true
			}
		});
	},

	resetData : function() {
		this.startTimeLog.setRawValue('');
		this.userIdT.setValue('');
		this.endTimeLog.setRawValue('');
		this.messageTypeCombo.setValue(-1);
		this.channelsCombo.setValue('');
		this.cityCombo.setValue('');
		this.brandCombo.setValue('');
	},
	search : function() {
		this.getBottomToolbar().reset0();
		var beginTime = this.beginTimeField.getValue();
		var endTime = this.endTimeField.getValue();
		if (!beginTime && !endTime) {
			var _bt = new Date();
			_bt.setDate(1);
			_bt.setHours(0, 0, 0, 0);
			this.beginTimeField.setValue(_bt);
			beginTime = _bt;
		}
		if (beginTime && endTime) {
			if (beginTime.getTime() >= endTime.getTime()) {
				Ext.Msg.alert('提示', '开始时间不能晚于结束时间');
				return false;
			}
			if ((endTime.getFullYear() * 12 + endTime.getMonth()) - (beginTime.getFullYear() * 12 + beginTime.getMonth()) > 1) {
				Ext.Msg.alert('提示', '只能选择相邻月份的时间');
				return false;
			}
		}
		// var beginTimeStr = '', endTimeStr = '';
		// if (beginTime)
		// beginTimeStr = beginTime.format('Y-m-d H:i:s');
		// if (endTime)
		// endTimeStr = endTime.format('Y-m-d H:i:s');
		var userId = this.userIdT.getValue().trim();
		var messageTypeId = this.messageTypeCombo.getValue();
		var multiCondition = this.multiCondition.getValue();
		var store = this.getStore();
		for ( var key in store.baseParams) {
			if (key && key.indexOf('userId') != -1)
				delete store.baseParams[key];
			if (key && key.indexOf('messageTypeId') != -1)
				delete store.baseParams[key];
			if (key && key.indexOf('multiCondition') != -1)
				delete store.baseParams[key];
			if (key && key.indexOf('beginTime') != -1)
				delete store.baseParams[key];
			if (key && key.indexOf('endTime') != -1)
				delete store.baseParams[key];
		}
		if (userId)
			store.setBaseParam('userId', userId);
		if (messageTypeId)
			store.setBaseParam('messageTypeId', messageTypeId);
		if (multiCondition)
			store.setBaseParam('multiCondition', multiCondition);
		if (beginTime)
			store.setBaseParam('beginTime', beginTime);
		if (endTime)
			store.setBaseParam('endTime', endTime);
		this.getStore().load({
			params : {
				start : 0,
				limit : this.getPageSize(),
				// tablename : this.tableName,
				tagsize : 0,
				isnext : true,
				'userId' : userId,
				'messageTypeId' : messageTypeId,
				'multiCondition' : multiCondition,
				'beginTime' : beginTime,
				'endTime' : endTime
			}
		});
		this.pagingBar.updateInfo('');
	},
	exportData : function(restart) {
		var self = this;
		var beginTime = this.beginTimeField.getValue();
		var endTime = this.endTimeField.getValue();
		if (!beginTime && !endTime) {
			var _bt = new Date();
			_bt.setDate(1);
			_bt.setHours(0, 0, 0, 0);
			this.beginTimeField.setValue(_bt);
			beginTime = _bt;
		}
		if (beginTime && endTime) {
			if (beginTime.getTime() >= endTime.getTime()) {
				Ext.Msg.alert('提示', '开始时间不能晚于结束时间');
				return false;
			}
			if ((endTime.getFullYear() * 12 + endTime.getMonth()) - (beginTime.getFullYear() * 12 + beginTime.getMonth()) > 1) {
				Ext.Msg.alert('提示', '只能选择相邻月份的时间');
				return false;
			}
		}
		// var beginTimeStr = '', endTimeStr = '';
		// if (beginTime)
		// beginTimeStr = beginTime.format('Y-m-d H:i:s');
		// if (endTime)
		// endTimeStr = endTime.format('Y-m-d H:i:s');
		var userId = this.userIdT.getValue().trim();
		var messageTypeId = this.messageTypeCombo.getValue();
		var multiCondition = self.multiCondition.getValue();
		var store = this.getStore();
		for ( var key in store.baseParams) {
			if (key && key.indexOf('userId') != -1)
				delete store.baseParams[key];
			if (key && key.indexOf('messageTypeId') != -1)
				delete store.baseParams[key];
			if (key && key.indexOf('multiCondition') != -1)
				delete store.baseParams[key];
			if (key && key.indexOf('beginTime') != -1)
				delete store.baseParams[key];
			if (key && key.indexOf('endTime') != -1)
				delete store.baseParams[key];
		}
		if (userId)
			store.setBaseParam('userId', userId);
		if (messageTypeId)
			store.setBaseParam('messageTypeId', messageTypeId);
		if (multiCondition)
			store.setBaseParam('multiCondition', multiCondition);
		if (beginTime)
			store.setBaseParam('beginTime', beginTime);
		if (endTime)
			store.setBaseParam('endTime', endTime);
		this.pagingBar.updateInfo('');

		if (!this.downloadIFrame) {
			this.downloadIFrame = this.getEl().createChild({
				tag : 'iframe',
				style : 'display:none;'
			});
		}
		var params = {
			// 'tablename' : 'OM_LOG_ACS_DETAIL',
			'userId' : userId,
			'messageTypeId' : messageTypeId,
			'multiCondition' : multiCondition,
			'beginTime' : beginTime,
			'endTime' : endTime
		};
		if (restart)
			params['restart'] = 'true';
		var exportcols = 0;
		var _exportItems = this.getTopToolbar().exportBtn.menu.items.items;
		var expNum = this.getTopToolbar().exportBtn.menu.exportNum.getValue();
		if (!/^[0-9]+$/.test(expNum)) {
			Ext.Msg.alert('提示', '您填写的导出数量不合法');
			return false;
		}
		Ext.each(_exportItems, function(item) {
			if (item.checked)
				exportcols = exportcols | item.value;
		});
		if (exportcols == 0) {
			Ext.Msg.alert('提示', '请至少选择一列进行导出');
			return false;
		}
		params['cols'] = exportcols;
		params['num'] = expNum;
		Ext.Ajax.request({
			url : 'log-acs-detail!export.action',
			params : Ext.urlEncode(params),
			success : function(response) {
				var result = Ext.util.JSON.decode(response.responseText);
				if (!result.success) {
					Ext.Msg.alert('提示', result.message);
					return false;
				}
				if (!result.data) {
					Ext.Msg.alert('提示', '已无数据可以导出');
					return false;
				}
				var timer = new ProgressTimer({
					initData : result.data,
					progressId : 'logAcsDetailStatus',
					boxConfig : {
						title : '正在导出人工客服明细...',
						buttons : {
							yes : '继续导出',
							no : '从头开始',
							cancel : '关闭'
						},
						fn : function(btn) {
							if (btn == 'yes')
								self.exportData();
							else if (btn == 'no')
								self.exportData(true);
						}
					},
					finish : function() {
						if (!this.downloadIFrame) {
							this.downloadIFrame = this.getEl().createChild({
								tag : 'iframe',
								style : 'display:none;'
							});
						}
						this.downloadIFrame.dom.src = 'log-acs-detail!exportExcelData.action?&_t=' + new Date().getTime();
					},
					scope : this
				});
				timer.start();
			},
			failure : function(response) {
				if (response.responseText)
					Ext.Msg.alert('出错了', response.responseText.replace(/\r\n/ig, '<br>'));
			},
			scope : this
		});
		// this.multiCondition = '';
	}
});