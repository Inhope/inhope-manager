Ext.namespace("LogAskDetailPanel");
LogAskDetailPanel = function(_cfg) {
	var self = this;
	this.tableName = 'OM_LOG_ASK_DETAIL';
	this.isChangeTable = false;
	this.tagSize = 0;
	this.data = {};
	this.multiCondition;
	this.extraAttrs = {};
	var _pageSize = 20;

	var reader = new Ext.data.JsonReader({
		idProperty : 'id',
		root : 'data',
		fields : [ {
			name : 'userId'
		}, {
			name : 'id'
		}, {
			name : 'qcontent'
		}, {
			name : 'faqName'
		}, {
			name : 'acontent'
		}, {
			name : 'visitTime'
		}, {
			name : 'answerType'
		}, {
			name : 'cityName'
		}, {
			name : 'questionType'
		}, {
			name : 'brand'
		}, {
			name : 'channels'
		}, {
			name : 'similarity',
			type : 'float'
		}, {
			name : 'moduleId'
		}, {
			name : 'platform'
		}, {
			name : 'custom1'
		}, {
			name : 'custom2'
		}, {
			name : 'custom3'
		}, {
			name : 'ipAddress'
		}, {
			name : 'processCost'
		}, {
			name : 'categoryId'
		}, {
			name : 'categoryName'
		}, {
			name : 'ex'
		}, {
			name : 'sessionId'
		}, {
			name : 'inputType'
		} ]
	});
	var _store = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({
			url : 'log-ask-detail!listEx.action'
		}),
		reader : reader,
		writer : new Ext.data.JsonWriter()
	});
	_store.on('load', function() {
		delete _store.baseParams.bookmark;
		delete _store.baseParams.bookmarkCursor;
	});

	var pagingBar = new Ext.AskPagingToolbar({
		store : _store,
		pageSize : _pageSize,
		tableName : this.tableName,
		isChangeTable : this.isChangeTable,
		prependButtons : true,
		tagSize : this.tagSize,
		displayMsg : '第{0}到{1}条记录',
		emptyMsg : "没有记录",
		bookmarkCategory : 1,
		bookmarkAction : {
			url : 'log-ask-detail!bookmark.action',
			cb : function(resp) {
				var bookmark = Ext.decode(resp);
				if (bookmark.targetCondition) {
					var c = Ext.decode(bookmark.targetCondition);
					if (c.beginTime)
						self.beginTimeField.setValue(c.beginTime);
					if (c.endTime)
						self.endTimeField.setValue(c.endTime);
					else
						self.endTimeField.setValue('');
					if (c.acontent)
						self.answerT.setValue(c.acontent);
					else
						self.answerT.setValue('');
					if (c.moduleId)
						self.moduleT.setValue(c.moduleId);
					else
						self.moduleT.setValue('');
					if (c.userId)
						self.userIdT.setValue(c.userId);
					else
						self.userIdT.setValue('');
					if (c.userQues)
						self.userQuesT.setValue(c.userQues);
					else
						self.userQuesT.setValue('');
					if (c.answerType)
						self.answerTypeCombo.setValue(c.answerType);
					else
						self.answerTypeCombo.setValue('-1');
					if (c.inputType)
						self.inputTypeCombo.setValue(c.inputType);
					else
						self.inputTypeCombo.setValue('-1');
					if (c.questionType)
						self.questionTypeCombo.setValue(c.questionType);
					else
						self.questionTypeCombo.setValue('-1');
					if (c.multiCondition) {
						var m = Ext.decode(c.multiCondition);
						var marr = [];
						for ( var key in m) {
							marr = marr.concat(m[key]);
						}
						self.multiCondition.setValue(marr);
					} else
						self.multiCondition.setValue([]);
					if (c.similarityUp)
						self.similarityUpT.setValue(c.similarityUp);
					else
						self.similarityUpT.setValue('');
					if (c.similarityLow)
						self.similarityLowT.setValue(c.similarityLow);
					else
						self.similarityLowT.setValue('');
				}
				_store.baseParams.bookmark = bookmark.targetId;
				_store.baseParams.bookmarkCursor = bookmark.cursor;
				self.pagingBar.setActivePage(parseInt(bookmark.cursor / 20) + 1);
				self.search();
			}
		},
		listeners : {
			change : function() {
				var object = reader.jsonData;
				if (object && object.tableName && object.tableName.length) {
					if (this.tableName != "OM_LOG_ASK_DETAIL" && this.tableName != object.tableName) {
						this.isChangeTable = true;
					} else {
						this.isChangeTable = false;
					}
					if (object.tableName == null || object.tableName == 'null') {
					} else {
						this.tableName = object.tableName;
					}
				}
				if (object && object.tagSize) {
					if (object.tagSize != 0) {
						this.tagSize = object.tagSize;
					}
				}
			}
		}
	});
	pagingBar.setCountHandler(function(btn) {
		btn.setDisabled(true);
		pagingBar.updateInfo('正在计数...');
		Ext.Ajax.request({
			url : 'log-ask-detail!count.action',
			params : Ext.urlEncode(this.getStore().baseParams),
			success : function(response) {
				var result = Ext.util.JSON.decode(response.responseText);
				if (result && result.data != null) {
					pagingBar.updateInfo('共' + result.data + '条记录');
				}
				btn.setDisabled(false);
			},
			failure : function() {
				pagingBar.updateInfo('计数失败');
				btn.setDisabled(false);
			}
		});
	}, this);
	this.pagingBar = pagingBar;

	var _sm = new Ext.grid.CheckboxSelectionModel();
	var userIdT = new Ext.form.TextField({
		fieldLabel : '用户Id',
		name : 'userId',
		xtype : 'textfield',
		allowBlank : true,
		width : 85
	});
	var userQuesT = new Ext.form.TextField({
		fieldLabel : '用户问题',
		name : 'userQues',
		xtype : 'textfield',
		allowBlank : true,
		width : 70
	});
	var manualCombo = new Ext.form.ComboBox({
		fieldLabel : '是否人工回答',
		hiddenName : 'manual',
		valueField : 'id',
		displayField : 'text',
		mode : 'local',
		editable : false,
		typeAhead : true,
		loadMask : true,
		allowBlank : false,
		selectOnFocus : false,
		triggerAction : 'all',
		value : '-1',
		width : 100,
		store : new Ext.data.SimpleStore({
			fields : [ "id", "text" ],
			data : [ [ -1, "全部" ], [ 1, "是" ], [ 2, "否" ] ]
		})
	});
	this.floatReg = /^\d+(\.\d+)?$/;
	var similarityLowT = new Ext.form.TextField({
		name : 'similarity_low',
		xtype : 'textfield',
		allowBlank : true,
		width : 37,
		validator : function(val) {
			val = val.trim();
			if (val) {
				if (self.floatReg.test(val)) {
					return true;
				}
				return false;
			}
			return true;
		}
	});
	this.similarityLowT = similarityLowT;
	var similarityUpT = new Ext.form.TextField({
		name : 'similarity_up',
		xtype : 'textfield',
		allowBlank : true,
		width : 37,
		validator : function(val) {
			val = val.trim();
			if (val) {
				if (self.floatReg.test(val)) {
					return true;
				}
				return false;
			}
			return true;
		}
	});
	this.similarityUpT = similarityUpT;
	var processCostLowT = new Ext.form.TextField({
		id : '__processCostLowT',
		name : 'process_cost_low',
		xtype : 'textfield',
		allowBlank : true,
		width : 37,
		validator : function(val) {
			val = val.trim();
			if (val) {
				if (self.floatReg.test(val)) {
					return true;
				}
				return false;
			}
			return true;
		}
	});
	this.processCostLowT = processCostLowT;
	var processCostUpT = new Ext.form.TextField({
		id : '__processCostUpT',
		name : 'process_cost_up',
		xtype : 'textfield',
		allowBlank : true,
		width : 37,
		validator : function(val) {
			val = val.trim();
			if (val) {
				if (self.floatReg.test(val)) {
					return true;
				}
				return false;
			}
			return true;
		}
	});
	this.processCostUpT = processCostUpT;
	var exField = new Ext.form.TextField({
		name : 'ex',
		xtype : 'textfield',
		allowBlank : true,
		width : 37
	});
	this.exField = exField;

	var comboStoreData = [ [ '-1', '全部' ], [ '1', '标准回复' ], [ '2', '指令回复' ], [ '-3', '聊天' ], [ '5', '敏感词' ], [ '11', '建议问' ], [ '0', '默认回复' ],
			[ '-2', '其它' ] ];
	var answerTypeCombo = new Ext.form.ComboBox({
		fieldLabel : '回答类型',
		name : 'atype',
		hiddenName : 'answerType',
		valueField : 'id',
		displayField : 'text',
		mode : 'local',
		editable : true,
		typeAhead : true,
		loadMask : true,
		allowBlank : false,
		selectOnFocus : false,
		validator : function(val) {
			val = val.trim();
			for ( var i = 0; i < comboStoreData.length; i++) {
				if (val == comboStoreData[i][1])
					return true;
			}
			if (/^[0-9]+$/.test(val))
				return true;
			return false;
		},
		triggerAction : 'all',
		value : '-1',
		width : 75,
		store : new Ext.data.SimpleStore({
			fields : [ "id", "text" ],
			data : comboStoreData
		})
	});
	var quesTypeComboData = [ [ '-1', '全部' ], [ '1', '全数字' ], [ '2', '全字母' ], [ '4', '字母数字' ], [ '8', '含中文' ] ];
	var questionTypeCombo = new Ext.form.ComboBox({
		fieldLabel : '问题类型',
		name : 'qtype',
		hiddenName : 'questionType',
		valueField : 'id',
		displayField : 'text',
		mode : 'local',
		editable : false,
		typeAhead : true,
		loadMask : true,
		allowBlank : false,
		selectOnFocus : false,
		triggerAction : 'all',
		value : '-1',
		width : 75,
		store : new Ext.data.SimpleStore({
			fields : [ "id", "text" ],
			data : quesTypeComboData
		})
	});
	var inputTypeCombo = new Ext.form.ComboBox({
		fieldLabel : '来源',
		name : 'inputType',
		hiddenName : 'triggerSource',
		valueField : 'id',
		displayField : 'text',
		mode : 'local',
		editable : false,
		typeAhead : true,
		loadMask : true,
		allowBlank : true,
		selectOnFocus : false,
		triggerAction : 'all',
		emptyText : '请选择来源',
		width : 90,
		store : new Ext.data.SimpleStore({
			fields : [ "id", "text" ],
			data : [ [ -1, '全部' ], [ 1, "用户输入" ], [ 2, "智能提示" ],[ 3, "相关问" ], [ 4, "建议问" ], [ 5, "3*5菜单" ] ]
		}),
		listeners : {
			'select' : function(tp) {
			}
		}
	});
	this.inputTypeCombo = inputTypeCombo;

	var answerT = new Ext.form.TextField({
		fieldLabel : '答案',
		name : 'question',
		xtype : 'textfield',
		allowBlank : true,
		width : 70
	});
	var moduleT = new Ext.form.TextField({
		fieldLabel : '模块',
		name : 'moduleId',
		xtype : 'textfield',
		allowBlank : true,
		width : 47
	});
	var questionT = new Ext.form.TextField({
		fieldLabel : '用户问题',
		name : 'questionT',
		xtype : 'textfield',
		allowBlank : true,
		width : 150
	});
	var faqNameT = new Ext.form.TextField({
		fieldLabel : '标准问题',
		name : 'faqNameT',
		xtype : 'textfield',
		allowBlank : true,
		width : 70
	});

	this.questionT = questionT;
	this.userIdT = userIdT;
	this.answerTypeCombo = answerTypeCombo;
	this.questionTypeCombo = questionTypeCombo;
	this.answerT = answerT;
	this.faqNameT = faqNameT;

	var multiCondition = Dashboard.createDimChooser(150);
	this.multiCondition = multiCondition;
	var myExpander = new Ext.grid.RowExpander({
		tpl : new Ext.XTemplate('<p style="color:#999;margin:2px 0px 2px 48px;">{[this.getContent(values.acontent)]}</p>', {
			getContent : function(content) {
				return content.replace(/\[\/?link.*?\]/ig, '').replace(/\[\/?url.*?\]/ig, '').replace(
						/[<\[][a-zA-Z]{1,8}[>|\]].*?[<\[]\/[a-zA-Z]{1,8}.*?[>|\]]/ig, '').replace(/[<\[][a-zA-Z]{1,8}[>|\]][^\u4e00-\u9eff]*$/i, '')
						.replace(/>/g, '&gt;').replace(/</g, '&lt;');
			}
		})
	});
	var initDate = new Date();
	initDate.setDate(1);
	initDate.setHours(0, 0, 0, 0);
	var beginTimeField = new ClearableDateTimeField({
		editable : false,
		value : initDate,
		name : 'btime',
		width : 160,
		validator : function(v) {
			if (v) {
				var ev = endTimeField.getValue();
				if (ev && v.getTime() >= ev.getTime())
					return '开始时间不能晚于结束时间';
			}
			return true;
		}
	});
	this.beginTimeField = beginTimeField;
	var endTimeField = new ClearableDateTimeField({
		editable : false,
		name : 'etime',
		width : 160
	});
	this.endTimeField = endTimeField;
	beginTimeField.on('change', function(o, v) {
		// if (v) {
		// endTimeField.setMinValue(v);
		// var max = new Date();
		// max.setFullYear(v.getFullYear());
		// max.setMonth(v.getMonth() + 1);
		// max.setDate(0);
		// endTimeField.setMaxValue(max);
		// }
	});
	beginTimeField.fireEvent('change', beginTimeField, initDate);
	endTimeField.on('change', function(o, v) {
		// beginTimeField.setMaxValue(v);
		// var min = new Date();
		// min.setFullYear(v.getFullYear());
		// min.setMonth(v.getMonth());
		// min.setDate(1);
		// beginTimeField.setMinValue(min);
	});

	var _exportItems = [ new Ext.form.ComboBox({
		ref : 'exportNum',
		valueField : 'num',
		displayField : 'num',
		mode : 'local',
		editable : true,
		selectOnFocus : false,
		triggerAction : 'all',
		validator : function(val) {
			val = val.trim();
			if (val) {
				if (/^[0-9]+$/.test(val)) {
					// var intVal = parseInt(val);
					// if (intVal >= 100 && intVal <= 8000)
					return true;
				}
				return '请正确输入数字';
			}
			return true;
		},
		width : 100,
		listWidth : 100,
		value : 8000,
		store : new Ext.data.ArrayStore({
			fields : [ 'num' ],
			data : [ [ 100 ], [ 500 ], [ 1000 ], [ 2000 ], [ 5000 ], [ 8000 ] ]
		})
	}), '-', {
		text : '全量导出',
		checked : false,
		value : 131072
	}, {
		text : '精简答案',
		checked : false,
		value : 16384
	}, '-', {
		text : '用户ID',
		checked : true,
		value : 1
	}, {
		text : '会话ID',
		checked : true,
		value : 262144
	}, {
		text : '用户IP',
		checked : false,
		value : 4096
	}, {
		text : '用户问题',
		checked : true,
		value : 2
	}, {
		text : '标准问题',
		checked : true,
		value : 4
	}, {
		text : '问题答案',
		checked : true,
		value : 256
	}, {
		text : '访问时间',
		checked : true,
		value : 8
	}, {
		text : '问题类型',
		checked : true,
		value : 512
	}, {
		text : '回答类型',
		checked : true,
		value : 16
	}, {
		text : '来源',
		checked : false,
		value : 4194304
	}, {
		text : '相似度',
		checked : true,
		value : 1024
	}, {
		text : '模块',
		checked : true,
		value : 2048
	}, {
		text : '处理耗时',
		checked : false,
		value : 8192
	}, {
		text : Dashboard.dimensionNames.platform,
		checked : true,
		value : 128
	} ];
	if (Dashboard.miscConfig && Dashboard.miscConfig.categoryDepth > 0)
		_exportItems.push({
			text : '知识分类',
			checked : true,
			value : 32768
		});
	if (Dashboard.dimensionNames.location)
		_exportItems.push({
			text : Dashboard.dimensionNames.location,
			checked : true,
			value : 32
		});
	if (Dashboard.dimensionNames.brand)
		_exportItems.push({
			text : Dashboard.dimensionNames.brand,
			checked : true,
			value : 64
		});
	if (Dashboard.dimensionNames.custom1)
		_exportItems.push({
			text : Dashboard.dimensionNames.custom1,
			checked : true,
			value : 524288
		});
	if (Dashboard.dimensionNames.custom2)
		_exportItems.push({
			text : Dashboard.dimensionNames.custom2,
			checked : true,
			value : 1048576
		});
	if (Dashboard.dimensionNames.custom3)
		_exportItems.push({
			text : Dashboard.dimensionNames.custom3,
			checked : true,
			value : 2097152
		});
	if (Dashboard.miscConfig && Dashboard.miscConfig.askExName)
		_exportItems.push({
			text : Dashboard.miscConfig.askExName,
			checked : true,
			value : 65536
		});

	var _columns = [ new Ext.grid.RowNumberer(), myExpander, {
		header : '用户ID',
		dataIndex : 'userId',
		width : 75
	}, {
		header : '用户IP',
		dataIndex : 'ipAddress',
		hidden : true
	}, {
		header : '会话ID',
		dataIndex : 'sessionId',
		hidden : true
	},
	// {
	// header : 'id',
	// dataIndex : 'id',
	// hidden : true
	// },
	{
		header : '用户问题',
		dataIndex : 'qcontent',
		width : 80,
		renderer : function(v,c,r) {
			v = v.replace(/>/g, '&gt;').replace(/</g, '&lt;');
			if(!r.data.faqName && false)
			   v = "<a style='text-decoration:none;color:black;' title='点击打开相似问题' href='javaScript:LogNavPanel.logAskDetailPanel.openSimilarQuestion(\"" + v + "\");'>" + v + "</a>";
			return v;
		}
	}, {
		header : '标准问题',
		dataIndex : 'faqName',
		width : 80,
		renderer : function(v) {
			return "<a style='text-decoration:underline;' title='点击打开标准问题' href='javaScript:LogNavPanel.logAskDetailPanel.openStanQuestion(\"" + v + "\");'>" + v + "</a>";
		}
	}, {
		header : '访问时间',
		dataIndex : 'visitTime',
		width : 80
	}, {
		header : '问题类型',
		dataIndex : 'questionType',
		width : 45
	}, {
		header : '回答类型',
		dataIndex : 'answerType',
		width : 45
	}, {
		header : '来源',
		dataIndex : 'inputType',
		width : 45,
		hidden : true,
		renderer : function(v) {
			var ret = '用户输入';
			if (v == 2)
				ret = '智能提示';
			if (v == 3)
				ret = '相关问';
			if (v == 4)
				ret = '建议问';
			if (v == 5)
				ret = '3*5菜单';
			return ret;
		}
	}, {
		header : '相似度',
		dataIndex : 'similarity',
		width : 30,
		renderer : function(val) {
			if (!val)
				return 'N/A';
			return val;
		}
	}, {
		header : '模块',
		dataIndex : 'moduleId',
		width : 30,
		renderer : function(val) {
			if (!val)
				return 'N/A';
			return val;
		}
	}, {
		header : Dashboard.dimensionNames.platform,
		dataIndex : 'channels',
		width : 45
	}, {
		header : '知识分类',
		dataIndex : 'categoryName',
		width : 75,
		hidden : !(Dashboard.miscConfig && Dashboard.miscConfig.categoryDepth > 0)
	} ];
	if (Dashboard.dimensionNames.location)
		_columns.push({
			header : Dashboard.dimensionNames.location,
			dataIndex : 'cityName',
			width : 45,
			renderer : function(val) {
				if (!val || !val.trim())
					return 'N/A';
				return val;
			}
		});
	if (Dashboard.dimensionNames.brand)
		_columns.push({
			header : Dashboard.dimensionNames.brand,
			dataIndex : 'brand',
			width : 45,
			renderer : function(val) {
				if (!val || !val.trim())
					return 'N/A';
				return val;
			}
		});
	if (Dashboard.dimensionNames.custom1)
		_columns.push({
			header : Dashboard.dimensionNames.custom1,
			dataIndex : 'custom1',
			width : 45,
			renderer : function(val) {
				if (!val || !val.trim())
					return 'N/A';
				return val;
			}
		});
	if (Dashboard.dimensionNames.custom2)
		_columns.push({
			header : Dashboard.dimensionNames.custom2,
			dataIndex : 'custom2',
			width : 45,
			renderer : function(val) {
				if (!val || !val.trim())
					return 'N/A';
				return val;
			}
		});
	if (Dashboard.dimensionNames.custom3)
		_columns.push({
			header : Dashboard.dimensionNames.custom3,
			dataIndex : 'custom3',
			width : 45,
			renderer : function(val) {
				if (!val || !val.trim())
					return 'N/A';
				return val;
			}
		});
	if (Dashboard.miscConfig && Dashboard.miscConfig.askExName)
		_columns.push({
			header : Dashboard.miscConfig.askExName,
			dataIndex : 'ex',
			width : 45,
			renderer : function(val) {
				if (!val || !val.trim())
					return 'N/A';
				return val;
			}
		});
	_columns.push({
		header : '处理耗时(ms)',
		width : 50,
		dataIndex : 'processCost',
		hidden : true,
		renderer : function(val) {
			if (val == 0)
				return 'N/A';
			else if (val < 0)
				return '<span style="color:red;">' + Math.abs(val) + ' (超时)</span>';
			return val;
		}
	});

	var tbar_pc_text0 = new Ext.Toolbar.TextItem({
		text : '耗时:从',
		id : 'tbar_pc_text0'
	});
	var tbar_pc_text1 = new Ext.Toolbar.TextItem({
		text : '到',
		id : 'tbar_pc_text1'
	});
	var pc_added = false;
	var tbarItems = [ '从', beginTimeField, '到', endTimeField, '用户ID:', self.userIdT, '用户问题:', userQuesT, '标准问题:', faqNameT, '答案:', self.answerT, {
		text : '搜索',
		iconCls : 'icon-search',
		handler : function() {
			self.getBottomToolbar().reset0();
			self.search();
		}
	}, {
		text : '重置',
		icon : 'images/cross.gif',
		handler : function() {
			this.getTopToolbar().findBy(function(cmp) {
				if (cmp.name)
					cmp.reset();
			});
			this.tbarEx.findBy(function(cmp) {
				if (cmp.name)
					cmp.reset();
			});
			multiCondition.setValue({});
			if (_categoryTreeCombo)
				_categoryTreeCombo.setValue('');
		}.dg(this)
	}, {
		text : '导出',
		xtype : 'splitbutton',
		ref : 'exportBtn',
		hidden : !Dashboard.u().allow('om.log.ask.EXP'),
		menu : {
			defaults : {
				hideOnClick : false
			},
			items : _exportItems
		},
		iconCls : 'icon-ontology-export',
		handler : function() {
			self.exportData(true);
		}
	} ];
	if (Dashboard.miscConfig && Dashboard.miscConfig.logJobEnabled) {
		window.afterExportZip = function() {
			Dashboard.setAlert('该天没有数据包可供导出。');
		};
		var now = new Date();
		var dateArr = [];
		for ( var i = 1; i <= 15; i++) {
			now.setDate(now.getDate() - 1);
			dateArr.push([ Ext.util.Format.date(now, 'Ymd') ]);
		}
		tbarItems.push({
			text : '导出包',
			xtype : 'splitbutton',
			ref : 'exportZipBtn',
			menu : {
				defaults : {
					hideOnClick : false
				},
				items : [ new Ext.form.ComboBox({
					ref : 'exportZipDate',
					valueField : 'date',
					displayField : 'date',
					mode : 'local',
					editable : true,
					selectOnFocus : false,
					triggerAction : 'all',
					validator : function(val) {
						val = val.trim();
						if (val) {
							if (/^[0-9]{8}$/.test(val)) {
								return true;
							}
							return '请正确输入日期';
						}
						return true;
					},
					width : 100,
					listWidth : 100,
					value : dateArr[0],
					store : new Ext.data.ArrayStore({
						fields : [ 'date' ],
						data : dateArr
					})
				}) ]
			},
			iconCls : 'icon-ontology-export',
			handler : function() {
				self.exportZip();
			}
		});
	}
	var cfg = {
		columnLines : true,
		store : _store,
		margins : '0 5 5 5',
		border : false,
		style : 'border-right: 1px solid ' + sys_bdcolor,
		tbar : new Ext.Toolbar({
			enableOverflow : true,
			items : tbarItems
		}),
		colModel : new Ext.grid.ColumnModel({
			columns : _columns,
			listeners : {
				hiddenchange : function(cm, colIndex, hidden) {
					if (colIndex == cm.columns.length - 1) {
						if (hidden) {
							tbar_pc_text0.hide();
							tbar_pc_text1.hide();
							processCostLowT.hide();
							processCostUpT.hide();
							processCostLowT.setValue('');
							processCostUpT.setValue('');
							self.getBottomToolbar().updateInfo('');
							self.getBottomToolbar().hideProcessCostBtn();
							self.tbarEx.doLayout();
						} else {
							if (pc_added) {
								tbar_pc_text0.show();
								tbar_pc_text1.show();
								processCostLowT.show();
								processCostUpT.show();
							} else {
								pc_added = true;
								self.tbarEx.addItem(tbar_pc_text0);
								self.tbarEx.addField(processCostLowT);
								self.tbarEx.addItem(tbar_pc_text1);
								self.tbarEx.addField(processCostUpT);
							}
							self.getBottomToolbar().showProcessCostBtn();
							self.tbarEx.doLayout();
						}
					}
				}
			}
		}),
		sm : _sm,
		bbar : pagingBar,
		loadMask : true,
		viewConfig : {
			forceFit : true
		},
		plugins : [ myExpander ]
	};

	var _categoryTreeCombo = new TreeComboBox({
		emptyText : '请选择知识分类',
		editable : false,
		anchor : '100%',
		name : 'category',
		allowBlank : true,
		minHeight : 250,
		width : 210,
		root : {
			id : '0',
			text : 'root',
			iconCls : 'icon-ontology-root'
		},
		loader : new Ext.tree.TreeLoader({
			dataUrl : 'ontology-category!list.action?ignoreObjects=true'
		})
	});
	this.categoryTreeCombo = _categoryTreeCombo;

	var _tbarExItems = [ ' 问题类型:', self.questionTypeCombo, ' 回答类型:', self.answerTypeCombo, ' 来源:', self.inputTypeCombo, '维度:', multiCondition, '模块:',
			moduleT, '相似度:从', similarityLowT, '到', similarityUpT ];
	if (Dashboard.miscConfig && Dashboard.miscConfig.categoryDepth)
		_tbarExItems.push('知识分类:', _categoryTreeCombo);
	if (Dashboard.miscConfig && Dashboard.miscConfig.askExName)
		_tbarExItems.push(Dashboard.miscConfig.askExName + ':', exField);
	this.tbarEx = new Ext.Toolbar({
		items : _tbarExItems
	});
	LogAskDetailPanel.superclass.constructor.call(this, Ext.applyIf(_cfg || {}, cfg));

	this.getPageSize = function() {
		return _pageSize;
	};

	this.store = _store;
	this.addListener('render', function() {
		this.tbarEx.render(this.tbar);
	}, this);

	this.faqNameT = faqNameT;
	this.userIdT = userIdT;
	this.userQuesT = userQuesT;
	this.manualCombo = manualCombo;
	this.answerTypeCombo = answerTypeCombo;
	this.answerT = answerT;
	this.moduleT = moduleT;
	this.questionT = questionT;

	/** -------push start------- */
	var menu = new Ext.menu.Menu({
		items : [
				{
					ref : 'userInfoBtn',
					text : '用户信息',
					iconCls : 'icon-userinfo',
					width : 50,
					handler : function() {
						var platform = menu.currentRow.get('platform');
						var userId = menu.currentRow.get('userId');
						Ext.Ajax.request({
							url : 'user-info!get.action',
							params : {
								'platform' : platform,
								'userId' : userId
							},
							success : function(resp) {
								var msg = '无法获取该用户信息', title = '提示';
								if (resp && resp.responseText) {
									var userInfo = Ext.decode(resp.responseText);
									msg = '昵称: ' + userInfo.nick + "<br/>" + '地市: ' + userInfo.location;
									title = '用户信息';
								}
								if (!menu.userTip) {
									menu.userTip = new Ext.ToolTip({
										html : msg,
										width : 150,
										title : title,
										autoHide : false,
										closable : true
									});
								} else {
									menu.userTip.update(msg);
								}
								menu.userTip.showAt(menu.xy);

							},
							failure : function(resp) {
								Ext.Msg.alert('错误', resp.responseText);
							}
						});
					}
				},
				{
					ref : 'dialogBtn',
					text : '完整对话',
					iconCls : 'icon-presuffix',
					width : 50,
					handler : function() {
						var sessionId = menu.currentRow.get('sessionId');
						var visitTime = menu.currentRow.get('visitTime');
						if (visitTime) {
							var month = visitTime.substring(0, 7).replace('-', '');
							if (!menu.dialogWin) {
								var logPanel = new Ext.Panel({
									region : 'center',
									autoScroll : true,
									bodyStyle : 'padding:5px 5px 5px 5px'
								});
								logPanel.on('afterrender', function() {
									logPanel.getUpdater().on(
											'update',
											function(el, resp) {
												logPanel.body.dom.innerHTML = resp.responseText.replace(/<div(.*?)>/g, '&lt;div$1&gt;').replace(
														/<\/div>/g, '&lt;/div&gt;');
											});
								});
								menu.dialogWin = new Ext.Window({
									width : 400,
									height : 460,
									modal : true,
									plain : true,
									shim : true,
									title : '会话详细信息',
									closeAction : 'hide',
									collapsible : true,
									closable : true,
									resizable : false,
									draggable : true,
									minimizable : false,
									maximizable : false,
									animCollapse : true,
									constrainHeader : true,
									layout : 'border',
									bodyStyle : 'padding:5px;',
									items : [ logPanel ],
									title : '会话详细信息'
								});
								menu.dialogWin.p = logPanel;
							}
							menu.dialogWin.show();
							menu.dialogWin.p.load({
								url : "log-session!queryLogSessionDetail.action",
								params : {
									"id" : sessionId,
									"month" : month
								},
								waitMsg : '正在加载数据...',
								success : function(response) {
								},
								failure : function(response) {
									Ext.Msg.alert("提示", "出错或者超时!");
								}
							});
						}
					}
				}, {
					ref : 'pushBtn',
					text : '回复消息',
					iconCls : 'icon-ontology-object',
					width : 50,
					handler : function() {
						_pushForm.platform.setValue(menu.currentRow.get('platform'));
						_pushForm.receiver.setValue(menu.currentRow.get('userId'));
						_pushWin.show();
					}
				} ]
	});
	this.on('cellcontextmenu', function(grid, rowIndex, cellIndex, e) {
		e.stopEvent();
		this.getSelectionModel().selectRow(rowIndex);
		menu.currentRow = this.getStore().getAt(rowIndex);
		if ('weixin' != menu.currentRow.get('platform'))
			menu.userInfoBtn.hide();
		else
			menu.userInfoBtn.show();
		menu.xy = e.xy;
		menu.showAt(e.xy);
	}, this);
	var _now = new Date();
	var _pushForm = new Ext.form.FormPanel({
		frame : false,
		border : false,
		labelWidth : 80,
		autoHeight : true,
		waitMsgTarget : true,
		bodyStyle : 'padding:3px 10px;background-color:' + sys_bgcolor,
		items : [ new Ext.form.Hidden({
			ref : 'receiver',
			name : 'receivers'
		}), new Ext.form.Hidden({
			name : 'type',
			value : 'spec'
		}), new Ext.form.Hidden({
			ref : 'platform',
			name : 'platform'
		}), {
			xtype : 'textarea',
			fieldLabel : '消息内容',
			height : 100,
			allowBlank : false,
			name : 'content',
			blankText : '消息内容不能为空',
			anchor : '98%'
		}, {
			xtype : 'xdatetime',
			allowBlank : false,
			fieldLabel : '起始时间',
			dateFormat : 'Y-m-d',
			timeFormat : 'H:i',
			value : _now,
			name : 'begin',
			anchor : '80%'
		}, {
			xtype : 'xdatetime',
			allowBlank : false,
			fieldLabel : '过期时间',
			dateFormat : 'Y-m-d',
			timeFormat : 'H:i',
			value : new Date().setHours(_now.getHours() + 8),
			name : 'expire',
			anchor : '80%'
		} ],
		tbar : [ {
			text : '保存',
			iconCls : 'icon-add',
			handler : function() {
				var btn = this;
				btn.disable();
				var f = _pushWin.editForm.getForm();
				if (!f.isValid()) {
					Ext.Msg.alert('提示', '您尚未正确完整的输入表单内容，请检查！');
					btn.enable();
					return false;
				}
				var vals = f.getFieldValues();
				vals.receivers = [ vals.receivers ];
				Ext.Ajax.request({
					url : 'push-message!save.action',
					params : {
						'data' : Ext.encode(vals)
					},
					success : function(resp) {
						Dashboard.setAlert('保存成功！');
						_pushWin.hide();
						btn.enable();
					},
					failure : function(resp) {
						Ext.Msg.alert('错误', resp.responseText);
						_pushWin.hide();
						btn.enable();
					}
				});
			}
		} ]
	});
	var _pushWin = new Ext.Window({
		width : 420,
		title : '编辑消息',
		defaults : {
			border : false
		},
		modal : true,
		plain : true,
		shim : true,
		closable : true,
		closeAction : 'hide',
		collapsible : true,
		resizable : false,
		draggable : true,
		animCollapse : true,
		constrainHeader : true,
		shadow : false,
		listeners : {
			'beforehide' : function(p) {
				p.editForm.getForm().reset();
			}
		},
		items : [ _pushForm ]
	});

	_pushWin.editForm = _pushForm;

	this.getView().on('refresh', function(view) {
		if (view && view.cm && view.cm.columns) {
			var cols = view.cm.columns;
			var colSpan = 0;
			for ( var i = 0; i < cols.length; i++) {
				if (!cols[i].hidden)
					colSpan++;
			}
			var trList = self.getEl().query('.x-grid3-row-body-tr');
			for ( var i = 0; i < trList.length; i++) {
				var td = trList[i].firstChild;
				if (td)
					td.colSpan = colSpan;
			}
		}
	});
};

Ext.extend(LogAskDetailPanel, Ext.grid.GridPanel, {
	loadData : function() {
		this.getBottomToolbar().reset0();
		var bt = this.beginTimeField.getValue().format('Y-m-d H:i:s');
		this.store.baseParams.beginTime = bt;
		this.store.load({
			params : {
				beginTime : bt,
				start : 0,
				limit : this.getPageSize(),
				tagsize : 0,
				isnext : true
			}
		});
	},
	resetData : function() {
		this.questionT.setValue('');
		this.answerT.setValue('');
		this.faqNameT.setValue('');
		this.manualCombo.setValue(-1);
		this.startTimeLog.setRawValue('');
		this.userIdT.setValue('');
		this.endTimeLog.setRawValue('');
		this.answerTypeCombo.setValue(-1);
		this.questionTypeCombo.setValue(-1);
		this.cityCombo.setValue('');
		this.brandCombo.setValue('');
		this.channels.setValue('');
	},
	search : function() {
		var beginTime = this.beginTimeField.getValue();
		var endTime = this.endTimeField.getValue();
		if (!beginTime && !endTime) {
			var _bt = new Date();
			_bt.setDate(1);
			_bt.setHours(0, 0, 0, 0);
			this.beginTimeField.setValue(_bt);
			beginTime = _bt;
		}
		if (beginTime && endTime) {
			if (beginTime.getTime() >= endTime.getTime()) {
				Ext.Msg.alert('提示', '开始时间不能晚于结束时间');
				return false;
			}
			if ((endTime.getFullYear() * 12 + endTime.getMonth()) - (beginTime.getFullYear() * 12 + beginTime.getMonth()) > 1) {
				Ext.Msg.alert('提示', '只能选择相邻月份的时间');
				return false;
			}
		}
		var beginTimeStr = '', endTimeStr = '';
		if (beginTime)
			beginTimeStr = beginTime.format('Y-m-d H:i:s');
		if (endTime)
			endTimeStr = endTime.format('Y-m-d H:i:s');
		var acontent = this.answerT.getValue().trim();
		var moduleId = this.moduleT.getValue().trim();
		var manual = this.manualCombo.getValue();// 得到的是Id
		var userId = this.userIdT.getValue().trim();
		var userQues = this.userQuesT.getValue().trim();
		var faqName = this.faqNameT.getValue().trim();
		// if (userQues || acontent) {
		// if (!beginTime || !endTime) {
		// Ext.Msg.alert('提示', '根据用户问题或答案查询时，需指定时间跨度(小于7天)');
		// return false;
		// }
		// if (endTime.getDate() - beginTime.getDate() > 7) {
		// Ext.Msg.alert('提示', '根据用户问题或答案查询时，时间跨度不可大于7天');
		// return false;
		// }
		// }
		var answerType = this.answerTypeCombo.getValue();
		var inputType = this.inputTypeCombo.getValue();
		var questionType = this.questionTypeCombo.getValue();
		if (!/^[0-9]+$/.test(answerType) && answerType != '-1' && answerType != '-2' && answerType != '-3') {
			Ext.Msg.alert('提示', '您输入的回答类型有误');
			return false;
		}
		var multiCondition = this.multiCondition.getValue();
		var similarityLow = this.similarityLowT.getValue().trim();
		var similarityUp = this.similarityUpT.getValue().trim();
		var processCostLow = this.processCostLowT.getValue();
		if (processCostLow)
			processCostLow = processCostLow.trim();
		var processCostUp = this.processCostUpT.getValue();
		if (processCostUp)
			processCostUp = processCostUp.trim();
		if ((similarityLow && !this.floatReg.test(similarityLow)) || (similarityUp && !this.floatReg.test(similarityUp))
				|| (processCostLow && !this.floatReg.test(processCostLow)) || (processCostUp && !this.floatReg.test(processCostUp))) {
			Ext.Msg.alert('提示', '您输入的数值有误');
			return false;
		}
		var ex = this.exField ? this.exField.getValue() : null;
		var categoryId = this.categoryTreeCombo.getValue();
		if (ex)
			ex = ex.trim();
		var store = this.getStore();
		for ( var key in store.baseParams) {
			if (key && key.indexOf('acontent') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('manual') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('userId') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('userQues') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('answerType') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('inputType') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('moduleId') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('multiCondition') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('questionType') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('beginTime') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('endTime') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('similarityLow') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('similarityUp') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('processCostLow') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('processCostUp') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('ex') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('categoryId') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('faqName') != -1) {
				delete store.baseParams[key];
			}
		}
		if (acontent)
			store.setBaseParam('acontent', acontent);
		if (moduleId)
			store.setBaseParam('moduleId', moduleId);
		if (manual)
			store.setBaseParam('manual', manual);
		if (userId)
			store.setBaseParam('userId', userId);
		if (userQues)
			store.setBaseParam('userQues', userQues);
		if (answerType)
			store.setBaseParam('answerType', answerType);
		if (inputType)
			store.setBaseParam('inputType', inputType);
		if (questionType)
			store.setBaseParam('questionType', questionType);
		if (multiCondition)
			store.setBaseParam('multiCondition', multiCondition);
		if (beginTimeStr)
			store.setBaseParam('beginTime', beginTimeStr);
		if (endTimeStr)
			store.setBaseParam('endTime', endTimeStr);
		if (similarityLow)
			store.setBaseParam('similarityLow', similarityLow);
		if (similarityUp)
			store.setBaseParam('similarityUp', similarityUp);
		if (processCostLow)
			store.setBaseParam('processCostLow', processCostLow);
		if (processCostUp)
			store.setBaseParam('processCostUp', processCostUp);
		if (ex)
			store.setBaseParam('ex', ex);
		if (categoryId)
			store.setBaseParam('categoryId', categoryId);
		if (faqName)
			store.setBaseParam('faqName', faqName);
		this.getStore().load({
			params : {
				start : 0,
				limit : this.getPageSize(),
				// tablename : this.tableName,
				tagsize : 0,
				isnext : true,
				'acontent' : acontent,
				'moduleId' : moduleId,
				'manual' : manual,
				'userId' : userId,
				'userQues' : userQues,
				'questionType' : questionType,
				'answerType' : answerType,
				'inputType' : inputType,
				'multiCondition' : multiCondition,
				'beginTime' : beginTimeStr,
				'endTime' : endTimeStr,
				'similarityUp' : similarityUp,
				'similarityLow' : similarityLow,
				'processCostLow' : processCostLow,
				'processCostUp' : processCostUp,
				'ex' : ex,
				'categoryId' : categoryId,
				'faqName' : faqName
			}
		});
		this.pagingBar.updateInfo('');
	},
	exportData : function(restart) {
		var self = this;
		var beginTime = this.beginTimeField.getValue();
		var endTime = this.endTimeField.getValue();
		if (!beginTime && !endTime) {
			var _bt = new Date();
			_bt.setDate(1);
			_bt.setHours(0, 0, 0, 0);
			this.beginTimeField.setValue(_bt);
			beginTime = _bt;
		}
		if (beginTime && endTime) {
			if (beginTime.getTime() >= endTime.getTime()) {
				Ext.Msg.alert('提示', '开始时间不能晚于结束时间');
				return false;
			}
			if ((endTime.getFullYear() * 12 + endTime.getMonth()) - (beginTime.getFullYear() * 12 + beginTime.getMonth()) > 1) {
				Ext.Msg.alert('提示', '只能选择相邻月份的时间');
				return false;
			}
		}
		var beginTimeStr = '', endTimeStr = '';
		if (beginTime)
			beginTimeStr = beginTime.format('Y-m-d H:i:s');
		if (endTime)
			endTimeStr = endTime.format('Y-m-d H:i:s');
		var userQues = this.userQuesT.getValue().trim();
		var faqName = this.faqNameT.getValue().trim();
		var acontent = this.answerT.getValue().trim();
		var moduleId = this.moduleT.getValue().trim();
		// if (userQues || acontent) {
		// if (!beginTime || !endTime) {
		// Ext.Msg.alert('提示', '根据用户问题或答案导出时，需指定时间跨度(小于7天)');
		// return false;
		// }
		// if (endTime.getDate() - beginTime.getDate() > 7) {
		// Ext.Msg.alert('提示', '根据用户问题或答案导出时，时间跨度不可大于7天');
		// return false;
		// }
		// }
		var similarityLow = this.similarityLowT.getValue().trim();
		var similarityUp = this.similarityUpT.getValue().trim();
		var processCostLow = this.processCostLowT.getValue();
		if (processCostLow)
			processCostLow = processCostLow.trim();
		var processCostUp = this.processCostUpT.getValue();
		if (processCostUp)
			processCostUp = processCostUp.trim();
		var ex = this.exField ? this.exField.getValue() : null;
		if (ex)
			ex = ex.trim();
		if ((similarityLow && !this.floatReg.test(similarityLow)) || (similarityUp && !this.floatReg.test(similarityUp))
				|| (processCostLow && !this.floatReg.test(processCostLow)) || (processCostUp && !this.floatReg.test(processCostUp))) {
			Ext.Msg.alert('提示', '您输入的数值有误');
			return false;
		}
		var manual = this.manualCombo.getValue();// 得到的是Id
		var userId = this.userIdT.getValue().trim();
		var answerType = this.answerTypeCombo.getValue();
		var inputType = this.inputTypeCombo.getValue();
		var questionType = this.questionTypeCombo.getValue();
		var multiCondition = this.multiCondition.getValue();
		var categoryId = this.categoryTreeCombo.getValue();
		if (!this.downloadIFrame) {
			this.downloadIFrame = this.getEl().createChild({
				tag : 'iframe',
				style : 'display:none;'
			});
		}
		var params = {
			// 'tablename' : 'OM_LOG_ASK_DETAIL',
			'acontent' : acontent,
			'moduleId' : moduleId,
			'manual' : manual,
			'userId' : userId,
			'userQues' : userQues,
			'faqName' : faqName,
			'questionType' : questionType,
			'answerType' : answerType,
			'inputType' : inputType,
			'multiCondition' : multiCondition,
			'beginTime' : beginTimeStr,
			'endTime' : endTimeStr,
			'similarityUp' : similarityUp,
			'similarityLow' : similarityLow,
			'processCostUp' : processCostUp,
			'processCostLow' : processCostLow,
			'ex' : ex,
			'categoryId' : categoryId
		};
		if (restart)
			params['restart'] = 'true';
		var exportcols = 0;
		var _exportItems = this.getTopToolbar().exportBtn.menu.items.items;
		var expNum = this.getTopToolbar().exportBtn.menu.exportNum.getValue();
		if (!/^[0-9]+$/.test(expNum)) {
			Ext.Msg.alert('提示', '您填写的导出数量不合法');
			return false;
		}
		// else {
		// var _expNum = parseInt(expNum);
		// if (_expNum < 100 || _expNum > 8000) {
		// Ext.Msg.alert('提示', '导出数量必须是100到8000间的整数');
		// return false;
		// }
		// }
		Ext.each(_exportItems, function(item) {
			if (item.checked)
				exportcols = exportcols | item.value;
		});
		if (exportcols == 0) {
			Ext.Msg.alert('提示', '请至少选择一列进行导出');
			return false;
		}
		params['cols'] = exportcols;
		params['num'] = expNum;
		Ext.Ajax.request({
			url : 'log-ask-detail!export.action',
			params : Ext.urlEncode(params),
			success : function(response) {
				var result = Ext.util.JSON.decode(response.responseText);
				if (!result.success) {
					Ext.Msg.alert('提示', result.message);
					return false;
				}
				if (!result.data) {
					Ext.Msg.alert('提示', '无数据可以导出');
					return false;
				}
				var timer = new ProgressTimer({
					initData : result.data,
					progressId : 'logAskDetailStatus',
					boxConfig : {
						title : '正在导出自动问答明细...',
						buttons : {
							yes : '继续导出',
							no : '从头开始',
							cancel : '关闭'
						},
						fn : function(btn) {
							if (btn == 'yes')
								self.exportData();
							else if (btn == 'no')
								self.exportData(true);
						}
					},
					finish : function() {
						// Ext.MessageBox.hide();
						if (!this.downloadIFrame) {
							this.downloadIFrame = this.getEl().createChild({
								tag : 'iframe',
								style : 'display:none;'
							});
						}
						this.downloadIFrame.dom.src = 'log-ask-detail!exportExcelData.action?&_t=' + new Date().getTime();
					},
					scope : this
				});
				timer.start();
			},
			failure : function(response) {
				if (response.responseText)
					Ext.Msg.alert('出错了', response.responseText.replace(/\r\n/ig, '<br>'));
			},
			scope : this
		});
	},
	externalLoad : function(v) {
		var platform = arguments[0];
		var dimChooser = this.multiCondition;
		dimChooser.setValue([ platform ]);
		this.store.baseParams.multiCondition = dimChooser.getValue();
		var bt = this.beginTimeField.getValue().format('Y-m-d H:i:s');
		this.store.baseParams.beginTime = bt;
		this.store.load({
			params : {
				start : 0,
				limit : this.getPageSize(),
				tagsize : 0,
				isnext : true
			}
		});
	},
	exportZip : function(restart) {
		if (!this.downloadIFrame0) {
			this.downloadIFrame0 = this.getEl().createChild({
				tag : 'iframe',
				style : 'display:none;'
			});
		}
		var expDate = this.getTopToolbar().exportZipBtn.menu.exportZipDate.getValue();
		if (!/^[0-9]{8}$/.test(expDate)) {
			Ext.Msg.alert('提示', '您填写的日期不合法');
			return false;
		}
		this.downloadIFrame0.dom.src = 'log-ask-detail!exportDailyData.action?ts=' + new Date().getTime() + '&date=' + expDate;
	},
    openStanQuestion : function(question) {
		  Ext.Ajax.request({
			    url : 'ontology-object-value!queryFaqQuestion.action',
			    params : {
				    question : question
			    },
			    success : function(form, action) {
				    var resultObj = Ext.util.JSON.decode(form.responseText);
				    if (resultObj.success) {
					    // ShortcutToolRegistry.execAction("knowledgeManager");
				    	var index=0;
				    	Ext.each(getTopWindow().modules, function(m, i) {
							if (m.id == 'kbmgr') {
				    	        getTopWindow().vtab.setActiveTab(index);
				    	        return false;
							}
							index++;
			            })
					    parent.navToKB(resultObj.data.objectId, resultObj.data.dimValueId, resultObj.data.valueId, true);
				    }
			    },
			    failure : function(response) {
				    Ext.Msg.alert("错误", response.responseText);
				    btn.enable();
			    }
		    })
	  },
    openSimilarQuestion : function(param) {
    	  var self = this;
		  Ext.Ajax.request({
			    url : 'aitester!getRelatedQus.action',
			    params : {
				    question : param
//				    ,
//				    location:param.location,
//				    platform: param.channels,
//				    custom1:param.custom1,
//				    custom2:param.custom2,
//				    custom3:param.custom3
			    },
			    success : function(form, action) {
				    var resultObj = Ext.util.JSON.decode(form.responseText);
				    if (resultObj.success) {
				    	self.showRelateQuWin();
				    	Ext.getCmp("logAskQueSegmentPanel").body.update(param+"<br /><br />"+resultObj.message);
				    	self.relatedResultStore.loadData(resultObj.data);
				    }
			    },
			    failure : function(response) {
				    Ext.Msg.alert("错误", response.responseText);
				    btn.enable();
			    }
		    })
	  },
	  showRelateQuWin :function(){
	  	if(!this.relatedQueWin){
		  	 var qContent = new Ext.form.TextField({
				  fieldLabel : '用户问题',
				  name : 'qContent',
				  anchor : '100%',
				  readOnly:true
		     })
		     this.qContent = qContent;
		     var relatedResultStore = new Ext.data.ArrayStore({
			  fields : ['relatedQue', 'ex']
		    });
		    this.relatedResultStore = relatedResultStore;
			var relatedResultGrid = new Ext.grid.GridPanel({
				  region : 'center',
				  border : false,
				  store : relatedResultStore,
				  colModel : new Ext.grid.ColumnModel({
					    columns : [{
						      header : '关联问题',
						      dataIndex : 'relatedQue',
						      sortable : false,
						      renderer:function(v){
						      	return "<a style='text-decoration:underline;' title='点击打开标准问题' href='javaScript:LogNavPanel.logAskDetailPanel.openStanQuestion(\"" + v + "\");'>" + v + "</a>";
						      }
					      }, {
						      header : 'ex',
						      dataIndex : 'ex',
						      sortable : false
					      }]
				    }),
				  viewConfig : {
					  forceFit : true
				  }
			  });
		  	  var formP =  new Ext.form.FormPanel({
				  frame : false,
				  border : false,
				  region : 'east',
				  width : 320,
				  split : true,
				  collapseMode : 'mini',
				  labelWidth : 75,
				  anchor : '100% 100%',
				  waitMsgTarget : true,
				  bodyStyle : 'padding : 5px; background-color:' + sys_bgcolor,
				  style : 'border-left: 1px solid ' + sys_bdcolor,
				  items : [new Ext.Panel({
				    id : 'logAskQueSegmentPanel',
				    region : 'north',
				    bodyStyle : 'border:solid 1px #b5b8c8;margin-bottom:5px;padding-top:2px;background:rgb(255,255,200);height:140px;',
				    html : '',
				    autoScroll : true
			    }),{
					    layout : 'fit',
					    anchor : '100% -30',
					    border : false,
					    items : relatedResultGrid
				    }]
		      });
			  this.relatedQueWin = new Ext.Window({
				  title : '问题分析工具',
				  iconCls : 'icon-grid',
				  closeAction : 'hide',
				  plain : true,
				  modal : false,
				  width : 500,
				  height : 500,
				  layout : 'fit',
				  maximizable : true,
				  collapsible : true,
				  bodyStyle : 'padding: 0 1px 1px 0px;',
				  items : formP
				  });
		  	}
	  	this.relatedQueWin.show();
	  }
});