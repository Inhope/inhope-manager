LogAuditPanel = function(_cfg) {
	var self = this;
	this.data = {};
	this.extraAttrs = {};
	var _pageSize = 20;

	var _store = new Ext.data.Store({
		  proxy : new Ext.data.HttpProxy({
			    url : 'log-audit!listDetail.action'
		    }),
		  reader : new Ext.data.JsonReader({
			    idProperty : 'id',
			    root : 'data',
			    fields : 'id question auditType objName editor submitTime submitOp auditor auditTime auditOp'.split(' ')
		    })
	  });
	var pagingBar = new Ext.PagingToolbar({
		  store : _store,
		  displayInfo : true,
		  pageSize : _pageSize,
		  prependButtons : true,
		  beforePageText : '第',
		  afterPageText : '页，共{0}页',
		  displayMsg : '第{0}到{1}条记录，共{2}条',
		  emptyMsg : "没有记录"
	  })
	var _sm = new Ext.grid.RowSelectionModel();

	var typeDataArr = "新增,修改,删除".split(",")
	var typeData = []
	Ext.each(typeDataArr, function(d, i) {
		  typeData.push(['' + (i + 1), d])
	  })
	  
	var auditType = new Ext.form.ComboBox({
		  name : 'auditType',
		  typeAhead : true,
		  triggerAction : 'all',
		  lazyRender : true,
		  editable : false,
		  loadMask : true,
		  allowBlank : false,
		  selectOnFocus : false,
		  width : 70,
		  mode : 'local',
		  value : -1,
		  store : new Ext.data.ArrayStore({
			    fields : ['id', 'text'],
			    data : [[-1, '全部'], [0, '标准问'], [1, '实例'], [2, '分类']]
		    }),
		  valueField : 'id',
		  displayField : 'text'
	  });
	var submitOp = new Ext.form.ComboBox({
		  name : 'submitOp',
		  valueField : 'id',
		  displayField : 'text',
		  mode : 'local',
		  editable : false,
		  typeAhead : true,
		  loadMask : true,
		  allowBlank : false,
		  selectOnFocus : false,
		  triggerAction : 'all',
		  width : 60,
		  store : new Ext.data.SimpleStore({
			    fields : ["id", "text"],
			    data : [['', "-清空-"]].concat(typeData)
		    })
	  });
	var typeDataArr = "通过,驳回".split(",")
	var typeData = []
	Ext.each(typeDataArr, function(d, i) {
		  typeData.push(['' + (i + 1), d])
	  })
	var auditOp = new Ext.form.ComboBox({
		  name : 'auditOp',
		  valueField : 'id',
		  displayField : 'text',
		  mode : 'local',
		  editable : false,
		  typeAhead : true,
		  loadMask : true,
		  allowBlank : false,
		  selectOnFocus : false,
		  triggerAction : 'all',
		  width : 60,
		  store : new Ext.data.SimpleStore({
			    fields : ["id", "text"],
			    data : [['', "-清空-"]].concat(typeData)
		    })
	  });
	var question = new Ext.form.TextField({
		  fieldLabel : '标准问',
		  name : 'question',
		  xtype : 'textfield',
		  allowBlank : true,
		  width : 80
	  });
	var objname = new Ext.form.TextField({
		  fieldLabel : '实例名',
		  name : 'objName',
		  xtype : 'textfield',
		  allowBlank : true,
		  width : 80
	  });
	var editor = new Ext.form.TextField({
		  fieldLabel : '编辑人',
		  name : 'editor',
		  xtype : 'textfield',
		  allowBlank : true,
		  width : 80
	  });
	var initDate = new Date();
	initDate.setDate(1);
	initDate.setHours(0, 0, 0, 0);
	var beginSubmitTimeField = this.beginSubmitTimeField = new ClearableDateTimeField({
		  editable : false,
		  value : initDate,
		  name : 'beginSubmitTime',
		  width : 160,
		  validator : function(v) {
			  if (v) {
				  var ev = endSubmitTimeField.getValue();
				  if (ev && v.getTime() >= ev.getTime())
					  return '开始时间不能晚于结束时间';
			  }
			  return true;
		  }
	  });
	var endSubmitTimeField = this.endSubmitTimeField = new ClearableDateTimeField({
		  editable : false,
		  name : 'endSubmitTime',
		  width : 160
	  });
	var beginAuditTimeField = this.beginAuditTimeField = new ClearableDateTimeField({
		  editable : false,
		  value : initDate,
		  name : 'beginAuditTime',
		  width : 160,
		  validator : function(v) {
			  if (v) {
				  var ev = endAuditTimeField.getValue();
				  if (ev && v.getTime() >= ev.getTime())
					  return '开始时间不能晚于结束时间';
			  }
			  return true;
		  }
	  });
	var endAuditTimeField = this.endAuditTimeField = new ClearableDateTimeField({
		  editable : false,
		  name : 'endAuditTime',
		  width : 160
	  });

	var _obj_date_format = 'Y-m-d H:i:s'
	var cfg = {
		columnLines : true,
		store : _store,
		margins : '0 5 5 5',
		border : false,
		style : 'border-right: 1px solid ' + sys_bdcolor,
		tbar : new Ext.Toolbar({
			  enableOverflow : true,
			  items : ['标准问:', question, '实例名:', objname, '提交人:', editor, '-', '提交时间:从', beginSubmitTimeField, '到:', endSubmitTimeField, '-', '提交操作:', submitOp, '审核类型:', auditType]
		  }),
		autoExpandColumn : 'logcontent',
		colModel : new Ext.grid.ColumnModel({
			  defaults : {
				  sortable : true
			  },
			  columns : [new Ext.grid.RowNumberer(), {
				    header : '审核目标',
				    dataIndex : 'question'
			    }, {
				    header : '审核类型',
				    dataIndex : 'auditType',
				    width : 60,
				    renderer : function(v) {
					    v == 0 ? v = '标准问' : "", v == 1 ? v = '实例' : "", v == 2 ? v = '分类' : ""
					    return v
				    }
			    }, {
				    header : '路径',
				    dataIndex : 'objName',
				    width : 250
			    }, {
				    header : '编辑人',
				    dataIndex : 'editor'
			    }, {
				    header : '提交时间',
				    dataIndex : 'submitTime'
			    }, {
				    header : '提交操作',
				    dataIndex : 'submitOp',
				    width : 60,
				    renderer : function(v) {
					    v == 1 ? v = '新增' : "", v == 2 ? v = '修改' : "", v == 3 ? v = '删除' : ""
					    return v
				    }
			    }, {
				    header : '审核人',
				    dataIndex : 'auditor'
			    }, {
				    header : '审核时间',
				    dataIndex : 'auditTime'
			    }, {
				    header : '审核结果',
				    dataIndex : 'auditOp',
				    width : 60,
				    renderer : function(v) {
					    v == 1 ? v = '通过' : "", v == 2 ? v = '驳回' : ""
					    return v
				    }
			    }]
		  }),
		sm : _sm,
		bbar : pagingBar,
		loadMask : true,
		viewConfig : {
			forceFit : true
		}
	}

	LogAuditPanel.superclass.constructor.call(this, Ext.applyIf(_cfg || {}, cfg));
	this.on('render', function() {
		  this.tbar2 = new Ext.Toolbar({
			    items : ['审核时间:从', beginAuditTimeField, '到:', endAuditTimeField, '-', '审核结果:', auditOp, '-', {
				      text : '搜索',
				      iconCls : 'icon-search',
				      handler : function() {
					      self.searchWord();
				      }
			      }, '-', {
				      text : '导出',
				      iconCls : 'icon-ontology-export',
				      handler : function() {
					      self.exportData();
				      }
			      }]
		    });
		  this.tbar2.render(this.tbar)
	  })
	this.getPageSize = function() {
		return _pageSize;
	}

}

Ext.extend(LogAuditPanel, Ext.grid.GridPanel, {
	  initComponent : function() {
		  LogAuditPanel.superclass.initComponent.call(this);
		  this.getTopToolbar().items.each(function(c) {
			    if (c instanceof Ext.form.TextField) {
				    c.on('specialkey', function(f, e) {
					      if (e.ENTER == e.getKey()) {
						      this.searchWord();
					      }
				      }, this)
			    }
		    }, this)
	  },
	  loadData : function() {
		  this.getStore().setBaseParam('data',null)
		  this.getStore().load({
			    params : {
				    start : 0,
				    limit : this.getPageSize()
			    }
		    })
	  },
	  getSearchQuery : function() {
		  var queryobj = {}
		  Ext.each([this.getTopToolbar(), this.tbar2], function(b) {
			    b.items.each(function(item) {
				      if (item.name && (item.getValue() != null && item.getValue() != "")) {
					      queryobj[item.name] = item.getValue().trim ? item.getValue().trim() : item.getValue();
				      }
			      })
		    });
		  return queryobj
	  },
	  searchWord : function() {
		  var self = this;
		  var store = this.getStore();
		  store.setBaseParam('data', Ext.encode(this.getSearchQuery(), 'Y-m-d H:i:s'));
		  this.getStore().load({
			    params : {
				    start : 0,
				    limit : this.getPageSize()
			    }
		    });
	  },
	  exportData : function() {
		  if (!this.downloadIFrame) {
			  this.downloadIFrame = this.getEl().createChild({
				    tag : 'iframe',
				    style : 'display:none;'
			    })
		  }
		  var params = Ext.urlEncode({
			    data : Ext.encode(this.getSearchQuery(), 'Y-m-d H:i:s')
		    })
		  this.downloadIFrame.dom.src = "log-audit!listDetail.action?" + params;
	  }
  });