LogFaqVotePanel = function() {
	var self = this;
	this.multiCondition;
	this.FaqVoteRecord = Ext.data.Record.create([ 'id', 'sessionId', 'userId', 'platform', 'city', 'brand', 'custom1', 'custom2', 'custom3',
			'action', 'userQuestion', 'question', 'answer', 'reason', 'voteTime', 'categoryId', 'categoryName' ]);
	var store = new Ext.data.Store({
		remoteSort : true,
		proxy : new Ext.data.HttpProxy({
			url : 'log-faq-vote!list.action'
		}),
		autoLoad : true,
		reader : new Ext.data.JsonReader({
			idProperty : 'id',
			root : 'data',
			fields : this.FaqVoteRecord
		})
	});
	var myExpander = new Ext.grid.RowExpander({
		tpl : new Ext.Template('<p style="color:#999;margin:2px 0px 2px 48px;">{answer}</p>')
	});
	var _categoryTreeCombo = new TreeComboBox({
		emptyText : '请选择知识分类',
		editable : false,
		anchor : '100%',
		name : 'categoryId',
		allowBlank : true,
		minHeight : 250,
		width : 210,
		root : {
			id : '0',
			text : 'root',
			iconCls : 'icon-ontology-root'
		},
		loader : new Ext.tree.TreeLoader({
			dataUrl : 'ontology-category!list.action?ignoreObjects=true'
		})
	});
	this.categoryTreeCombo = _categoryTreeCombo;
	var pagingBar = new Ext.PagingToolbar({
		store : store,
		displayInfo : true,
		pageSize : 20,
		prependButtons : true,
		beforePageText : '第',
		afterPageText : '页，共{0}页',
		displayMsg : '第{0}到{1}条记录，共{2}条',
		emptyMsg : "没有记录"
	});

	var _columns = [ new Ext.grid.RowNumberer(), myExpander, {
		header : '用户ID',
		dataIndex : 'userId',
		width : 110
	}, {
		header : '用户问题',
		width : 180,
		dataIndex : 'userQuestion'
	}, {
		header : '标准问题',
		width : 180,
		dataIndex : 'question'
	}, {
		header : '未解决原因',
		width : 100,
		dataIndex : 'reason'
	}, {
		header : '提问时间',
		width : 130,
		dataIndex : 'voteTime'
	}, {
		header : Dashboard.dimensionNames.platform,
		width : 75,
		dataIndex : 'platform'
	} ];
	if (Dashboard.dimensionNames.location)
		_columns.push({
			header : Dashboard.dimensionNames.location,
			width : 75,
			dataIndex : 'city',
			renderer : function(v) {
				v = v.trim();
				if (!v)
					return 'N/A';
				return v;
			}
		});
	if (Dashboard.dimensionNames.brand)
		_columns.push({
			header : Dashboard.dimensionNames.brand,
			width : 75,
			dataIndex : 'brand',
			renderer : function(v) {
				v = v.trim();
				if (!v)
					return 'N/A';
				return v;
			}
		});
	if (Dashboard.dimensionNames.custom1)
		_columns.push({
			header : Dashboard.dimensionNames.custom1,
			dataIndex : 'custom1',
			width : 75,
			renderer : function(val) {
				if (!val || !val.trim())
					return 'N/A';
				return val;
			}
		});
	if (Dashboard.dimensionNames.custom2)
		_columns.push({
			header : Dashboard.dimensionNames.custom2,
			dataIndex : 'custom2',
			width : 75,
			renderer : function(val) {
				if (!val || !val.trim())
					return 'N/A';
				return val;
			}
		});
	if (Dashboard.dimensionNames.custom3)
		_columns.push({
			header : Dashboard.dimensionNames.custom3,
			dataIndex : 'custom3',
			width : 75,
			renderer : function(val) {
				if (!val || !val.trim())
					return 'N/A';
				return val;
			}
		});
	_columns.push({
		header : '知识分类',
		dataIndex : 'categoryName',
		width : 75,
		hidden : !(Dashboard.miscConfig && Dashboard.miscConfig.categoryDepth > 0)
	});

	var config = {
		id : 'logFaqVotePanel',
		store : store,
		columnLines : true,
		margins : '0 5 5 5',
		border : false,
		style : 'border-right: 1px solid ' + sys_bdcolor,
		bbar : pagingBar,
		sm : new Ext.grid.RowSelectionModel(),
		loadMask : true,
		plugins : [ myExpander ],
		viewConfig : {
			forceFit : true
		},
		colModel : new Ext.grid.ColumnModel({
			columns : _columns
		})
	};

	var beginTimeField = new ClearableDateTimeField({
		editable : false,
		name : 'startDate',
		width : 160,
		validator : function(v) {
			if (v) {
				var ev = endTimeField.getValue();
				if (ev && v.getTime() >= ev.getTime())
					return '开始时间不能晚于结束时间';
			}
			return true;
		}
	});
	this.beginTimeField = beginTimeField;
	var endTimeField = new ClearableDateTimeField({
		editable : false,
		name : 'endDate',
		width : 160
	});
	this.endTimeField = endTimeField;

	var multiCondition = Dashboard.createDimChooser(180);
	this.multiCondition = multiCondition;
	var userIdT = new Ext.form.TextField({
		fieldLabel : '用户Id',
		name : 'userId',
		xtype : 'textfield',
		allowBlank : true,
		width : 110
	});
	var userQuesT = new Ext.form.TextField({
		fieldLabel : '用户问题',
		name : 'userQues',
		xtype : 'textfield',
		allowBlank : true,
		width : 110
	});
	var stdQuesT = new Ext.form.TextField({
		fieldLabel : '标准问题',
		name : 'stdQues',
		xtype : 'textfield',
		allowBlank : true,
		width : 110
	});
	var actionTypeCombo = new Ext.form.ComboBox({
		valueField : 'id',
		displayField : 'text',
		mode : 'local',
		name : 'type',
		editable : false,
		triggerAction : 'all',
		value : '2',
		width : 75,
		store : new Ext.data.SimpleStore({
			fields : [ "id", "text" ],
			data : [ [ '1', '解决' ], [ '2', '未解决' ] ]
		})
	});
	this.actionTypeCombo = actionTypeCombo;
	this.tbarEx = config.tbar = new Ext.Toolbar({
		enableOverflow : true,
		items : [ '从', beginTimeField, '到', endTimeField, '用户ID:', userIdT, '类别', actionTypeCombo, ' ', {
			text : '搜索',
			iconCls : 'icon-search',
			handler : function() {
				self.search();
			}
		}, {
			text : '重置',
			icon : 'images/cross.gif',
			handler : function() {
				this.tbarEx.findBy(function(cmp) {
					if (cmp.name)
						cmp.reset();
				});
				this.tbarExMain.findBy(function(cmp) {
					if (cmp.name)
						cmp.reset();
				});
				multiCondition.setValue({});
				if (_categoryTreeCombo)
					_categoryTreeCombo.setValue('');
			}.dg(this)
		}, {
			text : '导出',
			hidden : !Dashboard.u().allow('om.log.fv.EXP'),
			iconCls : 'icon-ontology-export',
			handler : function() {
				self.exportData(true);
			}
		}, {
			text : '排名',
			iconCls : 'icon-application_view',
			handler : function() {
				_rankPanel.getStore().baseParams['multiCondition'] = self.multiCondition.getValue();
				_rankPanel.getStore().baseParams['startDate'] = self.tbarEx.find('name', 'startDate')[0].getValue();
				_rankPanel.getStore().baseParams['endDate'] = self.tbarEx.find('name', 'endDate')[0].getValue();
				_rankPanel.getStore().baseParams['userId'] = self.tbarEx.find('name', 'userId')[0].getValue();
				_rankPanel.getStore().baseParams['stdQues'] = self.tbarExMain.find('name', 'stdQues')[0].getValue();
				_rankPanel.getStore().baseParams['stdQues'] = self.tbarExMain.find('name', 'stdQues')[0].getValue();
				_rankPanel.getStore().baseParams['type'] = self.tbarEx.find('name', 'type')[0].getValue();
				_rankPanel.getStore().baseParams['categoryId'] = self.categoryTreeCombo.getValue();
				_rankPanel.getStore().load({
					params : {},
					callback : function() {
						_rankWin.show();
					}
				});
			}
		} ]
	});

	var RankRecord = Ext.data.Record.create([ 'rank', 'question', 'times' ]);

	var rankStore = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({
			url : 'log-faq-vote!rank.action'
		}),
		reader : new Ext.data.JsonReader({
			idProperty : 'id',
			root : 'data',
			fields : RankRecord
		})
	});
	var _rankPanel = new Ext.grid.GridPanel({
		id : 'faqVoteRankPanel',
		columnLines : true,
		border : false,
		store : rankStore,
		sm : new Ext.grid.RowSelectionModel({
			singleSelect : true
		}),
		loadMask : true,
		tbar : [ {
			iconCls : 'icon-ontology-export',
			text : '导出',
			hidden : !Dashboard.u().allow('om.log.fv.EXP'),
			handler : function() {
				self.exportRank(true);
			}
		}, {
			text : ''
		} ],
		bbar : new Ext.PagingToolbar({
			store : rankStore,
			displayInfo : true,
			pageSize : 20,
			prependButtons : true,
			beforePageText : '第',
			afterPageText : '页，共{0}页',
			displayMsg : '第{0}到{1}条记录，共{2}条',
			emptyMsg : "没有记录"
		}),
		autoExpandColumn : '_questionCol',
		colModel : new Ext.grid.ColumnModel({
			columns : [ {
				header : '排名',
				dataIndex : 'rank',
				width : 35
			}, {
				id : '_questionCol',
				header : '标准问题',
				dataIndex : 'question'
			}, {
				header : '点击次数',
				dataIndex : 'times',
				width : 70
			} ]
		})

	});
	var _rankWin = new Ext.Window({
		defaults : {
			border : false
		},
		layout : 'fit',
		width : 700,
		height : 550,
		modal : true,
		plain : true,
		shim : true,
		closable : true,
		closeAction : 'hide',
		collapsible : true,
		resizable : true,
		draggable : true,
		animCollapse : true,
		constrainHeader : true,
		shadow : false,
		title : '点击次数排名',
		width : 715,
		listeners : {
			'beforeshow' : function() {
				var str = "";
				if (self.tbarEx.find('name', 'startDate')[0].getRawValue()) {
					if (str.length)
						str += ",";
					str += "时间:" + self.tbarEx.find('name', 'startDate')[0].getRawValue() + "至";
				}
				if (self.tbarEx.find('name', 'endDate')[0].getRawValue()) {
					str += self.tbarEx.find('name', 'endDate')[0].getRawValue();
				}

				if (str) {
					_rankWin.setTitle("点击次数排名 | " + str);
				}
			},
			scope : this

		},
		items : [ _rankPanel ]
	});

	var _tbarExItems = [ ' 用户问题:', userQuesT, '标准问题:', stdQuesT, '维度:', multiCondition ];
	if (Dashboard.miscConfig && Dashboard.miscConfig.categoryDepth)
		_tbarExItems.push('知识分类:', _categoryTreeCombo);
	this.tbarExMain = new Ext.Toolbar({
		items : _tbarExItems
	});

	LogFaqVotePanel.superclass.constructor.call(this, config);
	this.addListener('render', function() {
		this.tbarEx.render(this.tbar);
		this.tbarExMain.render(this.tbar);
	}, this);
};

Ext.extend(LogFaqVotePanel, Ext.grid.GridPanel, {
	search : function() {
		// var platform = this.tbarEx.find('name',
		// 'platform')[0];
		// var multiCondition = this.tbarEx.find('name',
		// 'multiCondition')[0];
		var multiCondition = this.multiCondition.getValue();
		var createTime_start = this.beginTimeField;
		var createTime_end = this.endTimeField;
		var userId = this.tbarEx.find('name', 'userId')[0];
		var userQues = this.tbarExMain.find('name', 'userQues')[0];
		var stdQues = this.tbarExMain.find('name', 'stdQues')[0];
		var actionType = this.tbarEx.find('name', 'type')[0];
		var searchFields = [ createTime_start, createTime_end, userId, userQues, stdQues, actionType, this.categoryTreeCombo ];
		var store = this.getStore();
		Ext.each(searchFields, function(f) {
			if (f && f.name) {
				if (f.getValue()) {
					store.setBaseParam(f.name, f.getValue())
				} else
					delete store.baseParams[f.name]
			}
		});
		if (multiCondition) {
			store.setBaseParam('multiCondition', multiCondition);
		} else {
			delete store.baseParams["multiCondition"];
		}
		store.load();
	},
	exportData : function(restart) {
		var self = this;
		// var multiCondition = this.tbarEx.find('name',
		// 'multiCondition')[0];
		var multiCondition = this.multiCondition.getValue();
		var createTime_start = this.beginTimeField;
		var createTime_end = this.endTimeField;
		var userId = this.tbarEx.find('name', 'userId')[0];
		var userQues = this.tbarExMain.find('name', 'userQues')[0];
		var stdQues = this.tbarExMain.find('name', 'stdQues')[0];
		var actionType = this.tbarEx.find('name', 'type')[0];
		if (!this.downloadIFrame) {
			this.downloadIFrame = this.getEl().createChild({
				tag : 'iframe',
				style : 'display:none;'
			});
		}
		var params = {
			'multiCondition' : multiCondition,
			'startDate' : createTime_start.getValue(),
			'endDate' : createTime_end.getValue(),
			'userId' : userId.getValue(),
			'userQues' : userQues.getValue(),
			'stdQues' : stdQues.getValue(),
			'type' : actionType.getValue(),
			'categoryId' : this.categoryTreeCombo.getValue()
		};
		if (restart)
			params['restart'] = 'true';
		Ext.Ajax.request({
			url : 'log-faq-vote!export.action',
			params : Ext.urlEncode(params),
			success : function(response) {
				var result = Ext.util.JSON.decode(response.responseText);
				if (!result.data) {
					Ext.Msg.alert('提示', '已无数据可以导出');
					return false;
				}
				var timer = new ProgressTimer({
					initData : result.data,
					progressId : 'logFaqVoteStatus',
					boxConfig : {
						title : '正在导出未解决问答明细...',
						buttons : {
							yes : '继续导出',
							no : '从头开始',
							cancel : '关闭'
						},
						fn : function(btn) {
							if (btn == 'yes')
								self.exportData();
							else if (btn == 'no')
								self.exportData(true);
						}
					},
					finish : function() {
						if (!this.downloadIFrame) {
							this.downloadIFrame = this.getEl().createChild({
								tag : 'iframe',
								style : 'display:none;'
							});
						}
						this.downloadIFrame.dom.src = 'log-faq-vote!exportExcelData.action?&_t=' + new Date().getTime();
					},
					scope : this
				});
				timer.start();
			},
			failure : function(response) {
				if (response.responseText)
					Ext.Msg.alert('出错了', response.responseText.replace(/\r\n/ig, '<br>'));
			},
			scope : this
		});
	},
	exportRank : function(restart) {
		var self = this;
		if (!this.downloadIFrame2) {
			this.downloadIFrame2 = this.getEl().createChild({
				tag : 'iframe',
				style : 'display:none;'
			});
		}
		var multiCondition = this.multiCondition.getValue();
		var categoryId = this.categoryTreeCombo.getValue();
		var params = {
			'multiCondition' : multiCondition,
			'startDate' : self.tbarEx.find('name', 'startDate')[0].getValue(),
			'endDate' : self.tbarEx.find('name', 'endDate')[0].getValue(),
			'userId' : self.tbarEx.find('name', 'userId')[0].getValue(),
			'stdQues' : self.tbarExMain.find('name', 'stdQues')[0].getValue(),
			'stdQues' : self.tbarExMain.find('name', 'stdQues')[0].getValue(),
			'type' : self.tbarEx.find('name', 'type')[0].getValue(),
			'categoryId' : categoryId
		};
		if (restart)
			params['restart'] = 'true';
		Ext.Ajax.request({
			url : 'log-faq-vote!exportRank.action',
			params : Ext.urlEncode(params),
			success : function(response) {
				var result = Ext.util.JSON.decode(response.responseText);
				if (!result.data) {
					Ext.Msg.alert('提示', '已无数据可以导出');
					return false;
				}
				var timer = new ProgressTimer({
					initData : result.data,
					progressId : 'logFaqVoteRankStatus',
					boxConfig : {
						title : '正在导出排名数据...',
						buttons : {
							yes : '继续导出',
							no : '从头开始',
							cancel : '关闭'
						},
						fn : function(btn) {
							if (btn == 'yes')
								self.exportRank();
							else if (btn == 'no')
								self.exportRank(true);
						}
					},
					finish : function() {
						if (!this.downloadIFrame2) {
							this.downloadIFrame2 = this.getEl().createChild({
								tag : 'iframe',
								style : 'display:none;'
							})
						}
						this.downloadIFrame2.dom.src = 'log-faq-vote!exportRankExcelData.action?&_t=' + new Date().getTime();
					},
					scope : this
				});
				timer.start();
			},
			failure : function(response) {
				if (response.responseText)
					Ext.Msg.alert('出错了', response.responseText.replace(/\r\n/ig, '<br>'));
			},
			scope : this
		})
	}
});
