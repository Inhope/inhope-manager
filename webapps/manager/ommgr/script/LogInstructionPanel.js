Ext.namespace("LogInstructionPanel");
LogInstructionPanel = function(_cfg) {
	var self = this;
	this.tableName = 'OM_LOG_ASK_DETAIL';
	this.isChangeTable = false;
	this.tagSize = 0;
	this.data = {};
	this.multiCondition;
	this.extraAttrs = {};
	var _pageSize = 20;

	var reader = new Ext.data.JsonReader({
				idProperty : 'id',
				root : 'data',
				fields : [{
							name : 'userId',
							type : 'string'
						}, {
							name : 'id',
							type : 'string'
						}, {
							name : 'qcontent',
							type : 'string'
						}, {
							name : 'faqName',
							type : 'string'
						}, {
							name : 'acontent',
							type : 'string'
						}, {
							name : 'visitTime',
							type : 'string'
						}, {
							name : 'answerType',
							type : 'string'
						}, {
							name : 'cityName',
							type : 'string'
						}, {
							name : 'brand',
							type : 'string'
						}, {
							name : 'channels',
							type : 'string'
						}]
			})
	var _store = new Ext.data.Store({
				proxy : new Ext.data.HttpProxy({
							url : 'log-instruction!list.action'
						}),
				reader : reader,
				writer : new Ext.data.JsonWriter()
			});

	var pagingBar = new Ext.PagingToolbarEx({
		store : _store,
		pageSize : _pageSize,
		tableName : this.tableName,
		isChangeTable : this.isChangeTable,
		prependButtons : true,
		tagSize : this.tagSize,
		displayMsg : '第{0}到{1}条记录',
		emptyMsg : "没有记录",
		listeners : {
			change : function() {
				var object = reader.jsonData
				if (object && object.tableName && object.tableName.length) {
					if (this.tableName != "OM_LOG_ASK_DETAIL"
							&& this.tableName != object.tableName) {
						this.isChangeTable = true;
					} else {
						this.isChangeTable = false;
					}
					if (object.tableName == null || object.tableName == 'null') {
					} else {
						this.tableName = object.tableName;
					}
				}
				if (object && object.tagSize) {
					if (object.tagSize != 0) {
						this.tagSize = object.tagSize;
					}
				}
			}
		}
	});
	pagingBar.setCountHandler(function(btn) {
				btn.setDisabled(true);
				pagingBar.updateInfo('正在计数...');
				Ext.Ajax.request({
							url : 'log-instruction!count.action',
							params : Ext.urlEncode(this.getStore().baseParams),
							success : function(response) {
								var result = Ext.util.JSON
										.decode(response.responseText);
								if (result && result.data != null) {
									pagingBar.updateInfo('共' + result.data
											+ '条记录');
								}
								btn.setDisabled(false);
							},
							failure : function() {
								pagingBar.updateInfo('计数失败');
								btn.setDisabled(false);
							}
						})
			}, this);
	this.pagingBar = pagingBar;

	var _sm = new Ext.grid.CheckboxSelectionModel();
	var userIdT = new Ext.form.TextField({
				fieldLabel : '用户Id',
				name : 'userId',
				xtype : 'textfield',
				allowBlank : true,
				width : 85
			});
	var quesT = new Ext.form.TextField({
				fieldLabel : '用户问题',
				name : 'userQues',
				xtype : 'textfield',
				allowBlank : true,
				width : 100
			});

	var _days = [];
	var daysOfMonth = Dashboard.getDaysOfMonth();
	for (var i = 1; i <= daysOfMonth; i++)
		_days.push([i]);
	var _startDayCombo = new Ext.form.ComboBox({
				valueField : 'day',
				displayField : 'day',
				mode : 'local',
				editable : true,
				selectOnFocus : false,
				triggerAction : 'all',
				validator : function(val) {
					val = val.trim();
					if (val) {
						if (/^[0-9]+$/.test(val)) {
							var intVal = parseInt(val);
							if (intVal > 0 && intVal <= 31)
								return true;
						}
						return false;
					}
					return true;
				},
				width : 50,
				listWidth : 50,
				store : new Ext.data.ArrayStore({
							fields : ['day'],
							data : _days
						})
			});
	this.startDayCombo = _startDayCombo;
	var _endDayCombo = new Ext.form.ComboBox({
				valueField : 'day',
				displayField : 'day',
				mode : 'local',
				editable : true,
				validator : function(val) {
					val = val.trim();
					if (val) {
						if (/^[0-9]+$/.test(val)) {
							var intVal = parseInt(val);
							if (intVal > 0 && intVal <= 31)
								return true;
						}
						return false;
					}
					return true;
				},
				selectOnFocus : false,
				triggerAction : 'all',
				width : 50,
				listWidth : 50,
				store : new Ext.data.ArrayStore({
							fields : ['day'],
							data : _days
						})
			});
	this.endDayCombo = _endDayCombo;
	var _monthCombo = new DateFieldEx({
				emptyText : '请选择月份',
				format : 'Y-m',
				value : new Date(),
				editable : false,
				width : 75
			})
	this.monthCombo = _monthCombo;
	this.userIdT = userIdT;
	this.quesT = quesT;

	var myExpander = new Ext.grid.RowExpander({
		tpl : new Ext.Template('<p style="color:#999;margin:2px 0px 2px 48px;">{acontent}</p>')
	})
	var cfg = {
		columnLines : true,
		store : _store,
		margins : '0 5 5 5',
		border : false,
		style : 'border-right: 1px solid ' + sys_bdcolor,
		tbar : new Ext.Toolbar({
			items : ['月:', _monthCombo, '从', _startDayCombo, '号到',
					_endDayCombo, '号', '用户ID:', self.userIdT, '用户问题:', quesT, {
						text : '搜索',
						iconCls : 'icon-search',
						handler : function() {
							self.searchWord();
						}
					}, {
						text : '导出',
						xtype : 'splitbutton',
						ref : 'exportBtn',
						menu : {
							defaults : {
								hideOnClick : false
							},
							items : [new Ext.form.ComboBox({
								ref : 'exportNum',
								valueField : 'num',
								displayField : 'num',
								mode : 'local',
								editable : true,
								selectOnFocus : false,
								triggerAction : 'all',
								validator : function(val) {
									val = val.trim();
									if (val) {
										if (/^[0-9]+$/.test(val)) {
											var intVal = parseInt(val);
											if (intVal >= 100 && intVal <= 8000)
												return true;
										}
										return '100到8000间的整数';
									}
									return true;
								},
								width : 100,
								listWidth : 100,
								value : 5000,
								store : new Ext.data.ArrayStore({
											fields : ['num'],
											data : [[100], [500], [1000],
													[2000], [5000], [8000]]
										})
							}), '-', {
								text : '用户ID',
								checked : true,
								value : 1
							}, {
								text : '用户问题',
								checked : true,
								value : 2
							}, {
								text : 'OPID',
								checked : true,
								value : 4
							}, {
								text : '问题答案',
								checked : true,
								value : 8
							}, {
								text : '访问时间',
								checked : true,
								value : 16
							}]
						},
						iconCls : 'icon-ontology-export',
						handler : function() {
							self.exportData(true);
						}
					}]
		}),
		colModel : new Ext.grid.ColumnModel({
					defaults : {
						sortable : true
					},
					columns : [new Ext.grid.RowNumberer(), myExpander, {
								header : '用户ID',
								dataIndex : 'userId',
								width : 75
							}, {
								header : 'id',
								dataIndex : 'id',
								hidden : true
							}, {
								header : '用户问题',
								dataIndex : 'qcontent',
								width : 80
							}, {
								header : '问题答案',
								dataIndex : 'acontent',
								hidden : true
							}, {
								header : 'OPID',
								dataIndex : 'acontent',
								width : 100,
								renderer : function(v) {
									var idx = v.indexOf('[');
									if (idx != -1) {
										v = v
												.substring(idx + 1, v
																.indexOf(']'));
										return v == '0' ? '无' : v;
									}
									return '无';
								}
							}, {
								header : '访问时间',
								dataIndex : 'visitTime',
								width : 100
							}]
				}),
		sm : _sm,
		bbar : pagingBar,
		loadMask : true,
		viewConfig : {
			forceFit : true
		},
		plugins : [myExpander]
	};
	LogInstructionPanel.superclass.constructor.call(this, Ext.applyIf(_cfg
							|| {}, cfg));
	this.getPageSize = function() {
		return _pageSize;
	};
	this.store = _store;
};

Ext.extend(LogInstructionPanel, Ext.grid.GridPanel, {
	loadData : function() {
		this.store.baseParams['month'] = this.monthCombo.getValue();
		this.store.load({
					params : {
						start : 0,
						limit : this.getPageSize(),
						tagsize : 0,
						isnext : true
					}
				});
	},
	searchWord : function() {
		var startDay = this.startDayCombo.getValue();
		var endDay = this.endDayCombo.getValue();
		if (startDay) {
			if (!/^[0-9]+$/.test(startDay)) {
				Ext.Msg.alert('提示', '开始日期不合法');
				return false;
			}
		}
		if (endDay) {
			if (!/^[0-9]+$/.test(endDay)) {
				Ext.Msg.alert('提示', '结束日期不合法');
				return false;
			}
		}
		var month = this.monthCombo.getValue();
		var userId = this.userIdT.getValue().trim();
		var ques = this.quesT.getValue().trim();
		var store = this.getStore();
		for (var key in store.baseParams) {
			if (key && key.indexOf('month') != -1) {
				delete store.baseParams[month];
			}
			if (key && key.indexOf('startDay') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('userId') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('endDay') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('question') != -1) {
				delete store.baseParams[key];
			}
		}
		if (month)
			store.setBaseParam('month', month);
		if (userId)
			store.setBaseParam('userId', userId);
		if (startDay)
			store.setBaseParam('startDay', startDay);
		if (endDay)
			store.setBaseParam('endDay', endDay);
		if (ques)
			store.setBaseParam('question', ques);
		this.getStore().load({
					params : {
						start : 0,
						limit : this.getPageSize(),
						tablename : this.tableName,
						tagsize : 0,
						isnext : true
					}
				});
		this.pagingBar.updateInfo('');
	},
	exportData : function(restart) {
		var self = this;
		var startDay = this.startDayCombo.getValue();
		var endDay = this.endDayCombo.getValue();
		if (startDay) {
			if (!/^[0-9]+$/.test(startDay)) {
				Ext.Msg.alert('提示', '开始日期不合法');
				return false;
			}
		}
		if (endDay) {
			if (!/^[0-9]+$/.test(endDay)) {
				Ext.Msg.alert('提示', '结束日期不合法');
				return false;
			}
		}
		var month = this.monthCombo.getValue();
		var userId = this.userIdT.getValue().trim();
		var ques = this.quesT.getValue().trim();
		if (!this.downloadIFrame) {
			this.downloadIFrame = this.getEl().createChild({
						tag : 'iframe',
						style : 'display:none;'
					})
		}
		var params = {
			'tablename' : 'OM_LOG_ASK_DETAIL',
			'month' : month,
			'userId' : userId,
			'startDay' : startDay,
			'endDay' : endDay,
			'question' : ques
		}
		if (restart)
			params['restart'] = 'true';
		var exportcols = 0;
		var _exportItems = this.getTopToolbar().exportBtn.menu.items.items;
		var expNum = this.getTopToolbar().exportBtn.menu.exportNum.getValue();
		if (!/^[0-9]+$/.test(expNum)) {
			Ext.Msg.alert('提示', '您填写的导出数量不合法');
			return false;
		} else {
			var _expNum = parseInt(expNum);
			if (_expNum < 100 || _expNum > 8000) {
				Ext.Msg.alert('提示', '导出数量必须是100到8000间的整数');
				return false;
			}
		}
		Ext.each(_exportItems, function(item) {
					if (item.checked)
						exportcols = exportcols | item.value;
				});
		if (exportcols == 0) {
			Ext.Msg.alert('提示', '请至少选择一列进行导出');
			return false;
		}
		params['cols'] = exportcols;
		params['num'] = expNum;
		Ext.Ajax.request({
			url : 'log-instruction!export.action',
			params : Ext.urlEncode(params),
			success : function(response) {
				var result = Ext.util.JSON.decode(response.responseText);
				if (!result.data) {
					Ext.Msg.alert('提示', '已无数据可以导出');
					return false;
				}
				var timer = new ProgressTimer({
					initData : result.data,
					progressId : 'logInstructionStatus',
					boxConfig : {
						title : '正在导出指令转发明细...',
						buttons : {
							yes : '继续导出',
							no : '从头开始',
							cancel : '关闭'
						},
						fn : function(btn) {
							if (btn == 'yes')
								self.exportData();
							else if (btn == 'no')
								self.exportData(true);
						}
					},
					finish : function() {
						if (!this.downloadIFrame) {
							this.downloadIFrame = this.getEl().createChild({
										tag : 'iframe',
										style : 'display:none;'
									})
						}
						this.downloadIFrame.dom.src = 'log-instruction!exportExcelData.action?&_t='
								+ new Date().getTime();
					},
					scope : this
				});
				timer.start();
			},
			failure : function(response) {
				if (response.responseText)
					Ext.Msg.alert('出错了', response.responseText.replace(
									/\r\n/ig, '<br>'));
			},
			scope : this
		})
	}
});