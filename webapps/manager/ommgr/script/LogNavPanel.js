LogNavPanel = function() {
	var self = this;
	this.data = {};
	var menu = Dashboard.u().filterAllowed([{
				id : '1',
				authName : 'om.log.logweb',
				name : '日志管理',
				icon : 'icon-expand'
			}, {
				id : '11',
				authName : 'om.log.logweb.0',
				name : '会话日志管理',
				module : 'webSession',
				parent : '1',
				icon : 'icon-toolbox',
				panelClass : LogSessionPanel
			}, {
				id : '12',
				authName : 'om.log.logweb.1',
				name : '自动问答明细',
				module : 'webDetail',
				parent : '1',
				icon : 'icon-toolbox',
				panelClass : LogAskDetailPanel
			}, {
				id : '13',
				authName : 'om.log.logweb.1',
				name : '人工客服明细',
				module : 'webLabourDetail',
				parent : '1',
				icon : 'icon-toolbox',
				panelClass : LogAcsDetailPanel
			},

			// {
			// id : '5',
			// authName : 'om.log.logsms',
			// name : '短信日志管理',
			// icon : 'icon-expand'
			// }, {
			// id : '52',
			// authName : 'om.log.logsms.1',
			// name : '会话明细管理',
			// module : 'wapDetail',
			// parent : '5',
			// icon : 'icon-toolbox',
			// panelClass : SMSLogDetailPanel
			// },
			{
				id : '7',
				name : '系统日志管理',
				icon : 'icon-expand'
			}, {
				id : '71',
				name : '操作日志管理',
				module : 'sysSession',
				parent : '7',
				icon : 'icon-toolbox',
				panelClass : SYSLogDetailPanel
			}]);
	var rootNode = new Ext.tree.TreeNode({
				id : '0',
				text : 'root'
			});
	var _nodeMap = {};
	for (var i = 0; i < menu.length; i++) {
		var _item = menu[i];
		var _node = new Ext.tree.TreeNode({
					id : _item.id,
					text : _item.name,
					iconCls : _item.icon,
					module : _item.module,
					panelClass : _item.panelClass
				});
		_nodeMap[_item.id] = _node;
		if (!_item.parent)
			rootNode.appendChild(_node);
		else
			_nodeMap[_item.parent].appendChild(_node);
	}
	var config = {
		title : '日志管理',
		border : false,
		autoScroll : true,
		rootVisible : false,
		lines : false,
		root : rootNode
	};
	LogNavPanel.superclass.constructor.call(this, config);

	this.modulePanels = {};
	this.on('click', function(n) {
				if (!n)
					return false;
				var module = n.attributes.module;
				var panelClass = n.attributes.panelClass;
				var p = self.showTab(panelClass, module + 'Tab', n.text,
						n.attributes.iconCls, true);
				if (p)
					p.loadData();
			});
}

Ext.extend(LogNavPanel, Ext.tree.TreePanel, {});

Dashboard.registerNav(LogNavPanel, 1);