LogServiceDetailPanel = function(_cfg) {
	var self = this;
	this.tableName = 'OM_LOG_SERVICE_DETAIL';
	this.isChangeTable = false;
	this.tagSize = 0;
	this.data = {};
	this.multiCondition;
	this.extraAttrs = {};
	var _pageSize = 20;

	var reader = new Ext.data.JsonReader({
		idProperty : 'id',
		root : 'data',
		fields : [ {
			name : 'userId'
		}, {
			name : 'sessionId'
		}, {
			name : 'id'
		}, {
			name : 'serviceId'
		}, {
			name : 'serviceType',
			type : 'int'
		}, {
			name : 'question'
		}, {
			name : 'answer'
		}, {
			name : 'createTime'
		}, {
			name : 'platform'
		}, {
			name : 'location'
		}, {
			name : 'brand'
		} ]
	});
	var _store = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({
			url : 'log-service-detail!list.action'
		}),
		reader : reader,
		writer : new Ext.data.JsonWriter()
	});

	var pagingBar = new Ext.PagingToolbarEx({
		store : _store,
		pageSize : _pageSize,
		tableName : this.tableName,
		isChangeTable : this.isChangeTable,
		prependButtons : true,
		tagSize : this.tagSize,
		displayMsg : '第{0}到{1}条记录',
		emptyMsg : "没有记录",
		listeners : {
			change : function() {
				var object = reader.jsonData;
				if (object && object.tableName && object.tableName.length) {
					if (this.tableName != "OM_LOG_SERVICE_DETAIL" && this.tableName != object.tableName) {
						this.isChangeTable = true;
					} else {
						this.isChangeTable = false;
					}
					if (object.tableName == null || object.tableName == 'null') {
					} else {
						this.tableName = object.tableName;
					}
				}
				if (object && object.tagSize) {
					if (object.tagSize != 0) {
						this.tagSize = object.tagSize;
					}
				}
			}
		}
	});
	pagingBar.setCountHandler(function(btn) {
		btn.setDisabled(true);
		pagingBar.updateInfo('正在计数...');
		Ext.Ajax.request({
			url : 'log-service-detail!count.action',
			params : Ext.urlEncode(this.getStore().baseParams),
			success : function(response) {
				var result = Ext.util.JSON.decode(response.responseText);
				if (result && result.data != null) {
					pagingBar.updateInfo('共' + result.data + '条记录');
				}
				btn.setDisabled(false);
			},
			failure : function() {
				pagingBar.updateInfo('计数失败');
				btn.setDisabled(false);
			}
		});
	}, this);
	this.pagingBar = pagingBar;
	var _sm = new Ext.grid.CheckboxSelectionModel();
	var userIdT = new Ext.form.TextField({
		fieldLabel : '用户Id',
		name : 'userId',
		xtype : 'textfield',
		allowBlank : true,
		width : 120
	});
	var userQuesT = new Ext.form.TextField({
		fieldLabel : '用户输入',
		name : 'userQues',
		xtype : 'textfield',
		allowBlank : true,
		width : 120
	});

	var _days = [];
	var daysOfMonth = Dashboard.getDaysOfMonth();
	for ( var i = 1; i <= daysOfMonth; i++)
		_days.push([ i ]);
	var _startDayCombo = new Ext.form.ComboBox({
		valueField : 'day',
		displayField : 'day',
		mode : 'local',
		editable : true,
		selectOnFocus : false,
		triggerAction : 'all',
		validator : function(val) {
			val = val.trim();
			if (val) {
				if (/^[0-9]+$/.test(val)) {
					var intVal = parseInt(val);
					if (intVal > 0 && intVal <= 31)
						return true;
				}
				return false;
			}
			return true;
		},
		width : 40,
		listWidth : 40,
		store : new Ext.data.ArrayStore({
			fields : [ 'day' ],
			data : _days
		})
	});
	this.startDayCombo = _startDayCombo;
	var _endDayCombo = new Ext.form.ComboBox({
		valueField : 'day',
		displayField : 'day',
		mode : 'local',
		editable : true,
		validator : function(val) {
			val = val.trim();
			if (val) {
				if (/^[0-9]+$/.test(val)) {
					var intVal = parseInt(val);
					if (intVal > 0 && intVal <= 31)
						return true;
				}
				return false;
			}
			return true;
		},
		selectOnFocus : false,
		triggerAction : 'all',
		width : 40,
		listWidth : 40,
		store : new Ext.data.ArrayStore({
			fields : [ 'day' ],
			data : _days
		})
	});
	this.endDayCombo = _endDayCombo;
	var _monthCombo = new DateFieldEx({
		emptyText : '请选择月份',
		format : 'Y-m',
		value : new Date(),
		editable : false,
		width : 75
	});
	_monthCombo.on('change', function(c, nv) {
		var daysOfMonth = Dashboard.getDaysOfMonth(nv);
		_days = [];
		for ( var i = 1; i <= daysOfMonth; i++)
			_days.push([ i ]);
		_startDayCombo.getStore().loadData(_days);
		_endDayCombo.getStore().loadData(_days);
	});
	this.monthCombo = _monthCombo;

	var cityStore = new CityStore({});
	cityStore.on('load', function() {
		cityCombo.setValue('');
	});
	cityStore.loadDatas();
	var cityCombo = new Ext.form.ComboBox({
		fieldLabel : '地市',
		hiddenName : 'city',
		displayField : 'name',
		valueField : 'id',
		mode : 'remote',
		editable : false,
		typeAhead : true,
		loadMask : true,
		queryParam : '地市',
		emptyText : '请选择地市...',
		allowBlank : true,
		selectOnFocus : false,
		triggerAction : 'all',
		width : 120,
		store : cityStore,
		getListParent : function() {
			return this.el.up('.x-menu');
		}
	});

	var brandStore = new brand.BrandStore({});
	brandStore.on('load', function() {
	});
	brandStore.loadDatas();
	var brandCombo = new Ext.form.ComboBox({
		fieldLabel : '品牌',
		valueField : 'id',
		displayField : 'name',
		mode : 'remote',
		editable : false,
		typeAhead : true,
		loadMask : true,
		allowBlank : true,
		selectOnFocus : false,
		triggerAction : 'all',
		emptyText : '请选择品牌...',
		queryParam : 'brand',
		width : 120,
		store : brandStore,
		getListParent : function() {
			return this.el.up('.x-menu');
		}
	});
	var answerT = new Ext.form.TextField({
		fieldLabel : '服务输出',
		name : 'question',
		xtype : 'textfield',
		allowBlank : true,
		width : 120
	});
	var serviceIdT = new Ext.form.TextField({
		fieldLabel : '服务ID',
		name : 'serviceIdT',
		xtype : 'textfield',
		allowBlank : true,
		width : 120
	});

	var channelsStore = new ChannelStore({});
	channelsStore.loadDatas();
	var channels = new Ext.form.ComboBox({
		fieldLabel : '平台',
		displayField : 'name',
		valueField : 'id',
		mode : 'remote',
		editable : false,
		typeAhead : true,
		loadMask : true,
		emptyText : '请选择平台...',
		allowBlank : true,
		selectOnFocus : false,
		triggerAction : 'all',
		width : 120,
		queryParam : '平台',
		store : channelsStore,
		getListParent : function() {
			return this.el.up('.x-menu');
		}
	});
	var servTypeData = [ [ '-1', "全部" ], [ '1', "非会话服务" ], [ '2', "会话服务(首次)" ], [ '3', "会话服务(其他)" ], [ '4', "会话服务(末次)" ] ];
	var serviceTypeCombo = new Ext.form.ComboBox({
		fieldLabel : '回答类型',
		name : 'atype',
		hiddenName : 'answerType',
		valueField : 'id',
		displayField : 'text',
		mode : 'local',
		editable : true,
		typeAhead : true,
		loadMask : true,
		allowBlank : false,
		selectOnFocus : false,
		triggerAction : 'all',
		value : '-1',
		width : 108,
		store : new Ext.data.SimpleStore({
			fields : [ "id", "text" ],
			data : servTypeData
		})
	});

	this.serviceIdT = serviceIdT;
	this.userIdT = userIdT;
	this.answerT = answerT;
	this.cityCombo = cityCombo;
	this.brandCombo = brandCombo;
	this.channels = channels;
	this.serviceTypeCombo = serviceTypeCombo;

	var multiCondition = Dashboard.createDimChooser(175);
	this.multiCondition = multiCondition;
	var myExpander = new Ext.grid.RowExpander({
		tpl : new Ext.XTemplate(
				'<p style="color:#999;margin:2px 0px 2px 48px;">{[values.answer.replace(/>/g, \'&gt;\').replace(/</g, \'&lt;\')]}</p>')
	});
	var initDate = new Date();
	initDate.setDate(1);
	initDate.setHours(0, 0, 0, 0);
	var beginTimeField = new ClearableDateTimeField({
		editable : false,
		name : 'btime',
		value : initDate,
		width : 160,
		validator : function(v) {
			if (v) {
				var ev = endTimeField.getValue();
				if (ev && v.getTime() >= ev.getTime())
					return '开始时间不能晚于结束时间';
			}
			return true;
		}
	});
	this.beginTimeField = beginTimeField;
	var endTimeField = new ClearableDateTimeField({
		editable : false,
		name : 'etime',
		width : 160
	});
	this.endTimeField = endTimeField;
	beginTimeField.fireEvent('change', beginTimeField, initDate);

	var _columns = [ new Ext.grid.RowNumberer(), myExpander, {
		header : '访问时间',
		dataIndex : 'createTime',
		width : 80
	},
	// {
	// header : 'id',
	// dataIndex : 'id',
	// hidden : true
	// },
	{
		header : '用户ID',
		dataIndex : 'userId',
		width : 100,
		renderer : function(v) {
			return v.replace(/>/g, '&gt;').replace(/</g, '&lt;');
		}
	}, {
		header : '服务ID',
		dataIndex : 'serviceId',
		width : 60
	}, {
		header : '服务类型',
		dataIndex : 'serviceType',
		width : 60,
		renderer : function(v) {
			if (v == 1)
				return "非会话服务";
			else if (v == 2)
				return "会话服务(首次)";
			else if (v == 3)
				return "会话服务(其他)";
			else if (v == 4)
				return "会话服务(末次)";
			return "";
		}
	}, {
		header : '用户输入',
		dataIndex : 'question',
		width : 200
	}, {
		header : '平台',
		dataIndex : 'platform',
		width : 50,
		renderer : function(val) {
			if (!val || !val.trim())
				return 'N/A';
			return val;
		}
	} ];
	if (Dashboard.dimensionNames.location)
		_columns.push({
			header : '地市',
			dataIndex : 'location',
			width : 50,
			renderer : function(val) {
				if (!val || !val.trim())
					return 'N/A';
				return val;
			}
		});
	if (Dashboard.dimensionNames.brand)
		_columns.push({
			header : '品牌',
			dataIndex : 'brand',
			width : 50,
			renderer : function(val) {
				if (!val || !val.trim())
					return 'N/A';
				return val;
			}
		});
	if (Dashboard.dimensionNames.custom1)
		_columns.push({
			header : Dashboard.dimensionNames.custom1,
			dataIndex : 'custom1',
			width : 50,
			renderer : function(val) {
				if (!val || !val.trim())
					return 'N/A';
				return val;
			}
		});
	if (Dashboard.dimensionNames.custom2)
		_columns.push({
			header : Dashboard.dimensionNames.custom2,
			dataIndex : 'custom2',
			width : 50,
			renderer : function(val) {
				if (!val || !val.trim())
					return 'N/A';
				return val;
			}
		});
	if (Dashboard.dimensionNames.custom3)
		_columns.push({
			header : Dashboard.dimensionNames.custom3,
			dataIndex : 'custom3',
			width : 50,
			renderer : function(val) {
				if (!val || !val.trim())
					return 'N/A';
				return val;
			}
		});

	var _exportItems = [ new Ext.form.ComboBox({
		ref : 'exportNum',
		valueField : 'num',
		displayField : 'num',
		mode : 'local',
		editable : true,
		selectOnFocus : false,
		triggerAction : 'all',
		validator : function(val) {
			val = val.trim();
			if (val) {
				if (/^[0-9]+$/.test(val)) {
					// var intVal = parseInt(val);
					// if (intVal >= 100 && intVal <= 8000)
					return true;
				}
				return '请正确输入数字';
			}
			return true;
		},
		width : 100,
		listWidth : 100,
		value : 8000,
		store : new Ext.data.ArrayStore({
			fields : [ 'num' ],
			data : [ [ 100 ], [ 500 ], [ 1000 ], [ 2000 ], [ 5000 ], [ 8000 ] ]
		})
	}), '-', {
		text : '访问时间',
		checked : true,
		value : 1
	}, {
		text : '用户ID',
		checked : true,
		value : 2
	}, {
		text : '服务ID',
		checked : true,
		value : 4
	}, {
		text : '服务类型',
		checked : true,
		value : 8
	}, {
		text : '用户输入',
		checked : true,
		value : 16
	}, {
		text : '服务输出',
		checked : true,
		value : 32
	}, {
		text : '平台',
		checked : true,
		value : 64
	} ];
	if (Dashboard.dimensionNames.location)
		_exportItems.push({
			text : '地市',
			checked : true,
			value : 128
		});
	if (Dashboard.dimensionNames.brand)
		_exportItems.push({
			text : '品牌',
			checked : true,
			value : 256
		});
	if (Dashboard.dimensionNames.custom1)
		_exportItems.push({
			text : Dashboard.dimensionNames.custom1,
			checked : true,
			value : 512
		});
	if (Dashboard.dimensionNames.custom2)
		_exportItems.push({
			text : Dashboard.dimensionNames.custom2,
			checked : true,
			value : 1024
		});
	if (Dashboard.dimensionNames.custom3)
		_exportItems.push({
			text : Dashboard.dimensionNames.custom3,
			checked : true,
			value : 2048
		});

	var cfg = {
		columnLines : true,
		store : _store,
		margins : '0 5 5 5',
		border : false,
		style : 'border-right: 1px solid ' + sys_bdcolor,
		tbar : new Ext.Toolbar({
			enableOverflow : true,
			items : [ '从', beginTimeField, '到', endTimeField, '用户ID:', self.userIdT, '服务ID:', self.serviceIdT, {
				text : '搜索',
				iconCls : 'icon-search',
				handler : function() {
					self.search();
				}
			}, {
				text : '重置',
				icon : 'images/cross.gif',
				handler : function() {
					this.getTopToolbar().findBy(function(cmp) {
						if (cmp.name)
							cmp.reset();
					});
					this.tbarEx.findBy(function(cmp) {
						if (cmp.name)
							cmp.reset();
					});
					multiCondition.setValue({});
				}.dg(this)
			}, {
				text : '导出',
				xtype : 'splitbutton',
				ref : 'exportBtn',
				hidden : !Dashboard.u().allow('om.log.svc.EXP'),
				menu : {
					defaults : {
						hideOnClick : false
					},
					items : _exportItems
				},
				iconCls : 'icon-ontology-export',
				handler : function() {
					self.exportData(true);
				}
			} ]
		}),
		colModel : new Ext.grid.ColumnModel({
			defaults : {
				sortable : true
			},
			columns : _columns
		}),
		sm : _sm,
		bbar : pagingBar,
		loadMask : true,
		viewConfig : {
			forceFit : true
		},
		plugins : [ myExpander ]
	};
	this.tbarEx = new Ext.Toolbar({
		items : [ ' 用户输入:', userQuesT, ' 服务输出:', self.answerT, ' 服务类型:', serviceTypeCombo, ' 维度:', multiCondition ]
	});
	LogServiceDetailPanel.superclass.constructor.call(this, Ext.applyIf(_cfg || {}, cfg));
	this.getPageSize = function() {
		return _pageSize;
	};

	this.store = _store;
	this.addListener('render', function() {
		this.tbarEx.render(this.tbar);
	}, this);

	this.userIdT = userIdT;
	this.userQuesT = userQuesT;
	this.cityCombo = cityCombo;
	this.channels = channels;
	this.answerT = answerT;
	this.serviceIdT = serviceIdT;
	this.brandCombo = brandCombo;
};

Ext.extend(LogServiceDetailPanel, Ext.grid.GridPanel, {
	loadData : function() {
		var bt = this.beginTimeField.getValue().format('Y-m-d H:i:s');
		this.store.baseParams.beginTime = bt;
		this.store.load({
			params : {
				beginTime : bt,
				start : 0,
				limit : this.getPageSize(),
				tagsize : 0,
				isnext : true
			}
		});
	},
	externalLoad : function(servId) {
		var bt = this.beginTimeField.getValue().format('Y-m-d H:i:s');
		this.store.baseParams.beginTime = bt;
		this.serviceIdT.setValue(servId);
		this.store.load({
			params : {
				beginTime : bt,
				start : 0,
				serviceId : servId,
				limit : this.getPageSize(),
				tagsize : 0,
				isnext : true
			}
		});
	},
	search : function() {
		var beginTime = this.beginTimeField.getValue();
		var endTime = this.endTimeField.getValue();
		if (!beginTime && !endTime) {
			var _bt = new Date();
			_bt.setDate(1);
			_bt.setHours(0, 0, 0, 0);
			this.beginTimeField.setValue(_bt);
			beginTime = _bt;
		}
		if (beginTime && endTime && beginTime.getTime() >= endTime.getTime()) {
			Ext.Msg.alert('提示', '开始时间不能晚于结束时间');
			return false;
		}
		var beginTimeStr = '', endTimeStr = '';
		if (beginTime)
			beginTimeStr = beginTime.format('Y-m-d H:i:s');
		if (endTime)
			endTimeStr = endTime.format('Y-m-d H:i:s');
		var acontent = this.answerT.getValue().trim();
		var userId = this.userIdT.getValue().trim();
		var userQues = this.userQuesT.getValue().trim();
		var serviceId = this.serviceIdT.getValue().trim();
		var serviceType = this.serviceTypeCombo.getValue();
		// if (userQues || acontent) {
		// if (!beginTime || !endTime) {
		// Ext.Msg.alert('提示',
		// '根据用户问题或答案查询时，需指定时间跨度(小于7天)');
		// return false;
		// }
		// if (endTime.getDate() - beginTime.getDate() > 7) {
		// Ext.Msg.alert('提示', '根据用户问题或答案查询时，时间跨度不可大于7天');
		// return false;
		// }
		// }
		var multiCondition = this.multiCondition.getValue();
		var store = this.getStore();
		for ( var key in store.baseParams) {
			if (key && key.indexOf('acontent') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('userId') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('userQues') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('serviceType') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('serviceId') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('multiCondition') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('beginTime') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('endTime') != -1) {
				delete store.baseParams[key];
			}
		}
		if (acontent)
			store.setBaseParam('acontent', acontent);
		if (serviceId)
			store.setBaseParam('serviceId', serviceId);
		if (userId)
			store.setBaseParam('userId', userId);
		if (userQues)
			store.setBaseParam('userQues', userQues);
		if (serviceType)
			store.setBaseParam('serviceType', serviceType);
		if (multiCondition)
			store.setBaseParam('multiCondition', multiCondition);
		if (beginTimeStr)
			store.setBaseParam('beginTime', beginTimeStr);
		if (endTimeStr)
			store.setBaseParam('endTime', endTimeStr);
		this.getStore().load({
			params : {
				start : 0,
				limit : this.getPageSize(),
				tablename : this.tableName,
				tagsize : 0,
				isnext : true,
				'acontent' : acontent,
				'userId' : userId,
				'userQues' : userQues,
				'serviceId' : serviceId,
				'serviceType' : serviceType,
				'multiCondition' : multiCondition,
				'beginTime' : beginTimeStr,
				'endTime' : endTimeStr
			}
		});
		this.pagingBar.updateInfo('');
	},
	exportData : function(restart) {
		var self = this;
		var beginTime = this.beginTimeField.getValue();
		var endTime = this.endTimeField.getValue();
		if (!beginTime && !endTime) {
			var _bt = new Date();
			_bt.setDate(1);
			_bt.setHours(0, 0, 0, 0);
			this.beginTimeField.setValue(_bt);
			beginTime = _bt;
		}
		if (beginTime && endTime && beginTime.getTime() >= endTime.getTime()) {
			Ext.Msg.alert('提示', '开始时间不能晚于结束时间');
			return false;
		}
		var beginTimeStr = '', endTimeStr = '';
		if (beginTime)
			beginTimeStr = beginTime.format('Y-m-d H:i:s');
		if (endTime)
			endTimeStr = endTime.format('Y-m-d H:i:s');
		var userQues = this.userQuesT.getValue().trim();
		var acontent = this.answerT.getValue().trim();
		// if (userQues || acontent) {
		// if (!beginTime || !endTime) {
		// Ext.Msg.alert('提示',
		// '根据用户输入或服务输出答案导出时，需指定时间跨度(小于7天)');
		// return false;
		// }
		// if (endTime.getDate() - beginTime.getDate() > 7) {
		// Ext.Msg.alert('提示',
		// '根据用户输入或服务输出答案导出时，时间跨度不可大于7天');
		// return false;
		// }
		// }
		var userId = this.userIdT.getValue().trim();
		var serviceType = this.serviceTypeCombo.getValue();
		var serviceId = this.serviceIdT.getValue().trim();
		var multiCondition = this.multiCondition.getValue();
		if (!this.downloadIFrame) {
			this.downloadIFrame = this.getEl().createChild({
				tag : 'iframe',
				style : 'display:none;'
			});
		}
		var params = {
			'tablename' : 'OM_LOG_SERVICE_DETAIL',
			'acontent' : acontent,
			'userId' : userId,
			'userQues' : userQues,
			'serviceType' : serviceType,
			'serviceId' : serviceId,
			'multiCondition' : multiCondition,
			'beginTime' : beginTimeStr,
			'endTime' : endTimeStr
		};
		if (restart)
			params['restart'] = 'true';
		var exportcols = 0;
		var _exportItems = this.getTopToolbar().exportBtn.menu.items.items;
		var expNum = this.getTopToolbar().exportBtn.menu.exportNum.getValue();
		if (!/^[0-9]+$/.test(expNum)) {
			Ext.Msg.alert('提示', '您填写的导出数量不合法');
			return false;
		}
		// else {
		// var _expNum = parseInt(expNum);
		// if (_expNum < 100 || _expNum > 8000) {
		// Ext.Msg.alert('提示', '导出数量必须是100到8000间的整数');
		// return false;
		// }
		// }
		Ext.each(_exportItems, function(item) {
			if (item.checked)
				exportcols = exportcols | item.value;
		});
		if (exportcols == 0) {
			Ext.Msg.alert('提示', '请至少选择一列进行导出');
			return false;
		}
		params['cols'] = exportcols;
		params['num'] = expNum;
		Ext.Ajax.request({
			url : 'log-service-detail!export.action',
			params : Ext.urlEncode(params),
			success : function(response) {
				var result = Ext.util.JSON.decode(response.responseText);
				if (!result.success) {
					Ext.Msg.alert('提示', result.message);
					return false;
				}
				if (!result.data) {
					Ext.Msg.alert('提示', '无数据可以导出');
					return false;
				}
				var timer = new ProgressTimer({
					initData : result.data,
					progressId : 'logServiceDetailStatus',
					boxConfig : {
						title : '正在导出服务日志明细...',
						buttons : {
							yes : '继续导出',
							no : '从头开始',
							cancel : '关闭'
						},
						fn : function(btn) {
							if (btn == 'yes')
								self.exportData();
							else if (btn == 'no')
								self.exportData(true);
						}
					},
					finish : function() {
						if (!this.downloadIFrame) {
							this.downloadIFrame = this.getEl().createChild({
								tag : 'iframe',
								style : 'display:none;'
							});
						}
						this.downloadIFrame.dom.src = 'log-service-detail!exportExcelData.action?&_t=' + new Date().getTime();
					},
					scope : this
				});
				timer.start();
			},
			failure : function(response) {
				if (response.responseText)
					Ext.Msg.alert('出错了', response.responseText.replace(/\r\n/ig, '<br>'));
			},
			scope : this
		});
	}
});