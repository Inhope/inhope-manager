Ext.namespace("LogSessionPanel");
LogSessionPanel = function(_cfg) {
	var self = this;
	this.tableName = 'OM_LOG_SESSION';
	this.tagSize = 0;
	this.data = {};
	this.multiCondition;
	this.extraAttrs = {};
	var _pageSize = 20;
	var reader = new Ext.data.JsonReader({
		idProperty : 'id',
		root : 'data',
		fields : [ {
			name : 'id',
			type : 'string'
		}, {
			name : 'sessionId',
			type : 'string'
		}, {
			name : 'userId',
			type : 'string'
		}, {
			name : 'ipAddress',
			type : 'string'
		}, {
			name : 'startTime',
			type : 'string'
		}, {
			name : 'endTime',
			type : 'string'
		}, {
			name : 'cityId',
			type : 'string'
		}, {
			name : 'city',
			type : 'string'
		}, {
			name : 'brand',
			type : 'string'
		}, {
			name : 'custom1'
		}, {
			name : 'custom2'
		}, {
			name : 'custom3'
		}, {
			name : 'channel',
			type : 'string'
		}, {
			name : 'type',
			type : 'int'
		} ]
	});
	var _store = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({
			url : 'log-session!listEx.action'
		}),
		reader : reader
	});
	_store.on('load', function() {
		delete _store.baseParams.bookmark;
		delete _store.baseParams.bookmarkCursor;
	});

	var pagingBar = new Ext.LogSessionPagingbar({
		store : _store,
		pageSize : _pageSize,
		tableName : this.tableName,
		prependButtons : true,
		tagSize : this.tagSize,
		displayMsg : '第{0}到{1}条记录',
		emptyMsg : "没有记录",
		bookmarkCategory : 2,
		bookmarkAction : {
			url : 'log-session!bookmark.action',
			cb : function(resp) {
				var bookmark = Ext.decode(resp);
				if (bookmark.targetCondition) {
					var c = Ext.decode(bookmark.targetCondition);
					if (c.beginTime)
						self.beginTimeField.setValue(c.beginTime);
					if (c.endTime)
						self.endTimeField.setValue(c.endTime);
					else
						self.endTimeField.setValue('');
					if (c.userId)
						self.getTopToolbar().userIdT.setValue(c.userId);
					else
						self.getTopToolbar().userIdT.setValue('');
					if (c.multiCondition) {
						var m = Ext.decode(c.multiCondition);
						var marr = [];
						for ( var key in m) {
							marr = marr.concat(m[key]);
						}
						self.multiCondition.setValue(marr);
					} else
						self.multiCondition.setValue([]);
					var activeChkbox = self.getTopToolbar().searchBtn.menu.activeOnly;
					if (c.type) {
						activeChkbox.setChecked(true);
						_store.baseParams['type'] = activeChkbox.value;
					} else {
						activeChkbox.setChecked(false);
						delete _store.baseParams['type'];
					}
				}
				_store.baseParams.bookmark = bookmark.targetId;
				_store.baseParams.bookmarkCursor = bookmark.cursor;
				self.pagingBar.setActivePage(parseInt(bookmark.cursor / 20) + 1);
				self.search();
			}
		},
		listeners : {
			change : function() {
				var object = reader.jsonData;
				if (object && object.tableName && object.tableName.length) {
					if (object.tableName == null || object.tableName == 'null') {
					} else {
						this.tableName = object.tableName;
					}
				}
				if (object && object.tagSize) {
					if (object.tagSize != 0) {
						this.tagSize = object.tagSize;
					}
				}
			}
		}
	});
	pagingBar.setCountHandler(function(btn) {
		var params = this.getStore().baseParams;
		var _params = {};
		if (arguments.length == 2) {
			Ext.apply(_params, {
				type : arguments[1]
			}, params);
		} else
			_params = params;
		btn.setDisabled(true);
		pagingBar.updateInfo('正在计数...');
		Ext.Ajax.request({
			url : 'log-session!count.action',
			params : Ext.urlEncode(_params),
			success : function(response) {
				var result = Ext.util.JSON.decode(response.responseText);
				if (result && result.data != null) {
					pagingBar.updateInfo('总数:共' + result.data + '条记录');
				}
				btn.setDisabled(false);
			},
			failure : function() {
				pagingBar.updateInfo('计数失败');
				btn.setDisabled(false);
			}
		});
	}, this);
	pagingBar.setCalTimeHandler(function(btn) {
		btn.setDisabled(true);
		pagingBar.updateInfo('正在计数...');
		Ext.Ajax.request({
			url : 'log-session!calTime.action',
			params : Ext.urlEncode(this.getStore().baseParams),
			success : function(response) {
				var result = Ext.util.JSON.decode(response.responseText);
				if (result && result.data != null) {
					pagingBar.updateInfo('平均会话时长:' + result.data + '秒');
				}
				btn.setDisabled(false);
			},
			failure : function() {
				pagingBar.updateInfo('计算失败');
				btn.setDisabled(false);
			}
		});
	}, this);
	this.pagingBar = pagingBar;

	var _sm = new Ext.grid.RowSelectionModel();
	var multiCondition = Dashboard.createDimChooser(150);
	this.multiCondition = multiCondition;

	var initDate = new Date();
	initDate.setDate(1);
	initDate.setHours(0, 0, 0, 0);
	var beginTimeField = new ClearableDateTimeField({
		editable : false,
		name : 'btime',
		value : initDate,
		width : 160,
		validator : function(v) {
			if (v) {
				var ev = endTimeField.getValue();
				if (ev && v.getTime() >= ev.getTime())
					return '开始时间不能晚于结束时间';
			}
			return true;
		}
	});
	this.beginTimeField = beginTimeField;
	var endTimeField = new ClearableDateTimeField({
		editable : false,
		name : 'etime',
		width : 160
	});
	this.endTimeField = endTimeField;
	beginTimeField.fireEvent('change', beginTimeField, initDate);

	var _exportItems = [ new Ext.form.ComboBox({
		ref : 'exportNum',
		valueField : 'num',
		displayField : 'num',
		mode : 'local',
		editable : true,
		selectOnFocus : false,
		triggerAction : 'all',
		validator : function(val) {
			val = val.trim();
			if (val) {
				if (/^[0-9]+$/.test(val)) {
					// var intVal = parseInt(val);
					// if (intVal >= 100 && intVal <= 8000)
					return true;
				}
				return '请正确输入数字';
			}
			return true;
		},
		width : 100,
		listWidth : 100,
		value : 8000,
		store : new Ext.data.ArrayStore({
			fields : [ 'num' ],
			data : [ [ 100 ], [ 500 ], [ 1000 ], [ 2000 ], [ 5000 ], [ 8000 ] ]
		})
	}), '-', {
		text : '用户ID',
		checked : true,
		value : 1
	}, {
		text : 'IP地址',
		checked : true,
		value : 2
	}, {
		text : '开始时间',
		checked : true,
		value : 4
	}, {
		text : '结束时间',
		checked : true,
		value : 8
	}, {
		text : '会话类型',
		checked : true,
		value : 256
	}, {
		text : '回答类型',
		checked : false,
		value : 128
	}, {
		text : Dashboard.dimensionNames.platform,
		checked : true,
		value : 64
	}, {
		text : '详细会话',
		checked : false,
		value : 512
	} ];
	if (Dashboard.dimensionNames.location)
		_exportItems.push({
			text : Dashboard.dimensionNames.location,
			checked : true,
			value : 16
		});
	if (Dashboard.dimensionNames.brand)
		_exportItems.push({
			text : Dashboard.dimensionNames.brand,
			checked : true,
			value : 32
		});
	if (Dashboard.dimensionNames.custom1)
		_exportItems.push({
			text : Dashboard.dimensionNames.custom1,
			checked : true,
			value : 1024
		});
	if (Dashboard.dimensionNames.custom2)
		_exportItems.push({
			text : Dashboard.dimensionNames.custom2,
			checked : true,
			value : 2048
		});
	if (Dashboard.dimensionNames.custom3)
		_exportItems.push({
			text : Dashboard.dimensionNames.custom3,
			checked : true,
			value : 4096
		});

	var _columns = [ new Ext.grid.RowNumberer(),
	// {
	// header : 'id',
	// dataIndex : 'id',
	// hidden : true
	// },
	{
		header : '用户ID',
		dataIndex : 'userId',
		width : 75
	}, {
		header : 'IP地址',
		dataIndex : 'ipAddress'
	}, {
		header : '开始时间',
		dataIndex : 'startTime',
		width : 100
	}, {
		header : '结束时间',
		dataIndex : 'endTime',
		width : 100
	}, {
		header : '会话类型',
		dataIndex : 'type',
		width : 60,
		renderer : function(val) {
			if (val == 0)
				return '无交互';
			if (val == 1)
				return '业务类';
			if (val == 2)
				return '聊天类';
			return 'N/A';
		}
	}, {
		header : Dashboard.dimensionNames.platform,
		dataIndex : 'channel',
		width : 60
	} ];
	if (Dashboard.dimensionNames.location)
		_columns.push({
			header : Dashboard.dimensionNames.location,
			dataIndex : 'city',
			width : 60,
			renderer : function(val) {
				if (!val || !val.trim())
					return 'N/A';
				return val;
			}
		});
	if (Dashboard.dimensionNames.brand)
		_columns.push({
			header : Dashboard.dimensionNames.brand,
			dataIndex : 'brand',
			width : 60,
			renderer : function(val) {
				if (!val || !val.trim())
					return 'N/A';
				return val;
			}
		});
	if (Dashboard.dimensionNames.custom1)
		_columns.push({
			header : Dashboard.dimensionNames.custom1,
			dataIndex : 'custom1',
			width : 60,
			renderer : function(val) {
				if (!val || !val.trim())
					return 'N/A';
				return val;
			}
		});
	if (Dashboard.dimensionNames.custom2)
		_columns.push({
			header : Dashboard.dimensionNames.custom2,
			dataIndex : 'custom2',
			width : 60,
			renderer : function(val) {
				if (!val || !val.trim())
					return 'N/A';
				return val;
			}
		});
	if (Dashboard.dimensionNames.custom3)
		_columns.push({
			header : Dashboard.dimensionNames.custom3,
			dataIndex : 'custom3',
			width : 60,
			renderer : function(val) {
				if (!val || !val.trim())
					return 'N/A';
				return val;
			}
		});
	var cfg = {
		id : 'LogSessionPanel',
		columnLines : true,
		store : _store,
		margins : '0 5 5 5',
		border : false,
		style : 'border-right: 1px solid ' + sys_bdcolor,
		tbar : new Ext.Toolbar({
			items : [ '从', beginTimeField, '到', endTimeField, '用户ID:', {
				ref : 'userIdT',
				name : 'userId',
				xtype : 'textfield',
				width : 110,
				emptyText : '输入用户ID搜索...'
			}, '维度:', multiCondition, {
				ref : 'searchBtn',
				text : '搜索',
				xtype : 'splitbutton',
				menu : {
					defaults : {
						hideOnClick : false
					},
					items : [ {
						ref : 'activeOnly',
						text : '仅显示有效会话',
						checked : false,
						value : 12,
						listeners : {
							click : function() {
								if (!this.checked)
									_store.baseParams['type'] = this.value;
								else
									delete _store.baseParams['type'];
							}
						}
					} ]
				},
				iconCls : 'icon-search',
				handler : function() {
					self.getBottomToolbar().reset0();
					self.search();
				}
			}, {
				text : '重置',
				icon : 'images/cross.gif',
				handler : function() {
					this.getTopToolbar().findBy(function(cmp) {
						if (cmp.name)
							cmp.reset();
					});
					multiCondition.setValue({});
				}.dg(this)
			}, {
				text : '导出',
				xtype : 'splitbutton',
				ref : 'exportBtn',
				hidden : !Dashboard.u().allow('om.log.sess.EXP'),
				menu : {
					defaults : {
						hideOnClick : false
					},
					items : _exportItems
				},
				iconCls : 'icon-ontology-export',
				handler : function() {
					self.exportData(true);
				}
			} ]
		}),
		colModel : new Ext.grid.ColumnModel({
			columns : _columns
		}),
		sm : _sm,
		bbar : pagingBar,
		loadMask : true,
		viewConfig : {
			forceFit : true
		}
	};
	LogSessionPanel.superclass.constructor.call(this, Ext.applyIf(_cfg || {}, cfg));
	var logSessionWindow = new DetailWindow({
		title : '会话详细信息'
	});
	this.logSessionWindow = logSessionWindow;
	this.getPageSize = function() {
		return _pageSize;
	},

	this.addListener('rowdblclick', function() {
		var sessionid = this.sm.getSelected().data.sessionId;
		var month = this.sm.getSelected().data.startTime;
		if (month) {
			month = month.substring(0, 7).replace('-', '');
			self.queryLogSession(sessionid, month);
		}
	}, this);
	this.sm = _sm;

};
Ext.extend(LogSessionPanel, Ext.grid.GridPanel, {
	loadData : function() {
		this.getBottomToolbar().reset0();
		var store = this.getStore();
		var bt = this.beginTimeField.getValue().format('Y-m-d H:i:s');
		store.baseParams['beginTime'] = bt;
		store.load({
			params : {
				beginTime : bt,
				start : 0,
				limit : this.getPageSize(),
				tagsize : 0,
				isnext : true
			}
		});
	},
	queryLogSession : function(id, month) {
		this.logSessionWindow.show();
		this.logSessionWindow.getPanel().load({
			url : "log-session!queryLogSessionDetail.action",
			params : {
				"id" : id,
				"month" : month
			},
			waitMsg : '正在加载数据...',
			success : function(response) {
			},
			failure : function(response) {
				Ext.Msg.alert("提示", "出错或者超时!");
			}
		});
	},
	search : function() {
		var beginTime = this.beginTimeField.getValue();
		var endTime = this.endTimeField.getValue();
		if (!beginTime && !endTime) {
			var _bt = new Date();
			_bt.setDate(1);
			_bt.setHours(0, 0, 0, 0);
			this.beginTimeField.setValue(_bt);
			beginTime = _bt;
		}
		if (beginTime && endTime) {
			if (beginTime.getTime() >= endTime.getTime()) {
				Ext.Msg.alert('提示', '开始时间不能晚于结束时间');
				return false;
			}
			if ((endTime.getFullYear() * 12 + endTime.getMonth()) - (beginTime.getFullYear() * 12 + beginTime.getMonth()) > 1) {
				Ext.Msg.alert('提示', '只能选择相邻月份的时间');
				return false;
			}
		}
		var beginTimeStr = '', endTimeStr = '';
		if (beginTime)
			beginTimeStr = beginTime.format('Y-m-d H:i:s');
		if (endTime)
			endTimeStr = endTime.format('Y-m-d H:i:s');
		var userId = this.getTopToolbar().userIdT.getValue().trim();
		var multiCondition = this.multiCondition.getValue();
		var store = this.getStore();
		for ( var key in store.baseParams) {
			if (key && key.indexOf('multiCondition') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('cityId') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('beginTime') != -1)
				delete store.baseParams[key];
			if (key && key.indexOf('endTime') != -1)
				delete store.baseParams[key];
			if (key && key.indexOf('userId') != -1)
				delete store.baseParams[key];
		}
		if (userId)
			store.setBaseParam('userId', userId);
		if (beginTimeStr)
			store.setBaseParam('beginTime', beginTimeStr);
		if (endTimeStr)
			store.setBaseParam('endTime', endTimeStr);
		if (multiCondition)
			store.setBaseParam('multiCondition', multiCondition);
		this.getStore().load({
			params : {
				start : 0,
				limit : this.getPageSize(),
				tagsize : 0,
				isnext : true,
				'userId' : userId,
				'beginTime' : beginTimeStr,
				'endTime' : endTimeStr,
				'multiCondition' : multiCondition
			}
		});
		this.pagingBar.updateInfo('');
	},
	exportData : function(restart) {
		var self = this;
		var beginTime = this.beginTimeField.getValue();
		var endTime = this.endTimeField.getValue();
		if (!beginTime && !endTime) {
			var _bt = new Date();
			_bt.setDate(1);
			_bt.setHours(0, 0, 0, 0);
			this.beginTimeField.setValue(_bt);
			beginTime = _bt;
		}
		if (beginTime && endTime) {
			if (beginTime.getTime() >= endTime.getTime()) {
				Ext.Msg.alert('提示', '开始时间不能晚于结束时间');
				return false;
			}
			if ((endTime.getFullYear() * 12 + endTime.getMonth()) - (beginTime.getFullYear() * 12 + beginTime.getMonth()) > 1) {
				Ext.Msg.alert('提示', '只能选择相邻月份的时间');
				return false;
			}
		}
		var beginTimeStr = '', endTimeStr = '';
		if (beginTime)
			beginTimeStr = beginTime.format('Y-m-d H:i:s');
		if (endTime)
			endTimeStr = endTime.format('Y-m-d H:i:s');
		var userId = this.getTopToolbar().userIdT.getValue().trim();
		var multiCondition = this.multiCondition.getValue();
		if (!this.downloadIFrame) {
			this.downloadIFrame = this.getEl().createChild({
				tag : 'iframe',
				style : 'display:none;'
			});
		}
		var params = {
			'userId' : userId,
			'beginTime' : beginTimeStr,
			'endTime' : endTimeStr,
			'multiCondition' : multiCondition
		};
		if (restart)
			params['restart'] = 'true';
		var exportcols = 0;
		var _exportItems = this.getTopToolbar().exportBtn.menu.items.items;
		var expNum = this.getTopToolbar().exportBtn.menu.exportNum.getValue();
		if (!/^[0-9]+$/.test(expNum)) {
			Ext.Msg.alert('提示', '您填写的导出数量不合法');
			return false;
		}
		// else {
		// var _expNum = parseInt(expNum);
		// if (_expNum < 100 || _expNum > 8000) {
		// Ext.Msg.alert('提示', '导出数量必须是100到8000间的整数');
		// return false;
		// }
		// }
		Ext.each(_exportItems, function(item) {
			if (item.checked)
				exportcols = exportcols | item.value;
		});
		if (exportcols == 0) {
			Ext.Msg.alert('提示', '请至少选择一列进行导出');
			return false;
		}
		params['cols'] = exportcols;
		params['num'] = expNum;
		Ext.Ajax.request({
			url : 'log-session!export.action',
			params : Ext.urlEncode(params),
			success : function(response) {
				var result = Ext.util.JSON.decode(response.responseText);
				if (!result.success) {
					Ext.Msg.alert('提示', result.message);
					return false;
				}
				if (!result.data) {
					Ext.Msg.alert('提示', '无数据可以导出');
					return false;
				}
				var timer = new ProgressTimer({
					initData : result.data,
					progressId : 'logSessionStatus',
					boxConfig : {
						title : '正在导出会话日志明细...',
						buttons : {
							yes : '继续导出',
							no : '从头开始',
							cancel : '关闭'
						},
						fn : function(btn) {
							if (btn == 'yes')
								self.exportData();
							else if (btn == 'no')
								self.exportData(true);
						}
					},
					finish : function() {
						if (!this.downloadIFrame) {
							this.downloadIFrame = this.getEl().createChild({
								tag : 'iframe',
								style : 'display:none;'
							});
						}
						this.downloadIFrame.dom.src = 'log-session!exportExcelData.action?&_t=' + new Date().getTime();
					},
					scope : this
				});
				timer.start();
			},
			failure : function(response) {
				if (response.responseText)
					Ext.Msg.alert('出错了', response.responseText.replace(/\r\n/ig, '<br>'));
			},
			scope : this
		});
	}
});