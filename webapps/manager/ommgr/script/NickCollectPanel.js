NickCollectPanel = function() {
	var self = this;
	this.LeaveMessageRecord = Ext.data.Record.create(['id', 'sessionId',
			'userId', 'platform', 'nickname', 'createTime']);
	var store = new Ext.data.Store({
				remoteSort : true,
				proxy : new Ext.data.HttpProxy({
							url : 'nick-collect!list.action'
						}),
				autoLoad : true,
				reader : new Ext.data.JsonReader({
							idProperty : 'id',
							root : 'data',
							fields : this.LeaveMessageRecord
						})
			});
	var pagingBar = new Ext.PagingToolbar({
				store : store,
				displayInfo : true,
				pageSize : 20,
				prependButtons : true,
				beforePageText : '第',
				afterPageText : '页，共{0}页',
				displayMsg : '第{0}到{1}条记录，共{2}条',
				emptyMsg : "没有记录"
			});
	var config = {
		id : 'NickCollectPanel',
		store : store,
		columnLines : true,
		margins : '0 5 5 5',
		border : false,
		style : 'border-right: 1px solid ' + sys_bdcolor,
		bbar : pagingBar,
		sm : new Ext.grid.RowSelectionModel(),
		loadMask : true,
		autoExpandColumn : 'nickname',
		colModel : new Ext.grid.ColumnModel({
					defaults : {
						sortable : true
					},
					columns : [new Ext.grid.RowNumberer(), {
								header : '会话ID',
								dataIndex : 'sessionId',
								width : 160
							}, {
								header : '用户名',
								dataIndex : 'userId',
								width : 160
							}, {
								header : '昵称',
								id : 'nickname',
								dataIndex : 'nickname'
							}, {
								header : '创建时间',
								width : 130,
								dataIndex : 'createTime'
							}, {
								header : '平台',
								width : 100,
								dataIndex : 'platform',
								renderer : function(v) {
									if (!v)
										return v
									var r = this.channelStore.getById(v);
									return r ? r.get('name') : ''
								}.dg(this)
							}]
				})
	};
	// this.sf_nickname;
	// this.sf_createTime_start;
	// this.sf_createTime_end;
	this.channelStore = new ChannelStore({});
	this.channelStore.loadDatas();
	this.tbarEx = config.tbar = new Ext.Toolbar({
				enableOverflow : true,
				items : ['平台:', new Ext.form.ComboBox({
									fieldLabel : '平台',
									name : 'platform',
									valueField : 'id',
									displayField : 'name',
									emptyText : '请选择平台',
									mode : 'remote',
									editable : false,
									allowBlank : false,
									selectOnFocus : false,
									triggerAction : 'all',
									queryParam : 'platform',
									width : 90,
									store : this.channelStore
								}), '创建时间:从', {
							name : 'createTime_start',
							xtype : 'datefield',
							width : 90,
							emptyText : '选择时间',
							format : 'Y-m-d'
						}, '到', {
							name : 'createTime_end',
							xtype : 'datefield',
							width : 90,
							emptyText : '选择时间',
							format : 'Y-m-d'
						}, ' ', {
							text : '搜索',
							iconCls : 'icon-search',
							handler : function() {
								self.search();
							}
						}, {
							text : '重置',
							icon : 'images/cross.gif',
							handler : function() {
								this.tbarEx.findBy(function(cmp) {
											if (cmp.name)
												cmp.reset()
										});
							}.dg(this)
						}, {
							text : '导出',
							iconCls : 'icon-ontology-export',
							handler : function() {
								self.exportData(true);
							}
						}]
			});
	NickCollectPanel.superclass.constructor.call(this, config);
	this.addListener('render', function() {
				this.tbarEx.render(this.tbar);
			}, this);
}

Ext.extend(NickCollectPanel, Ext.grid.GridPanel, {
	search : function() {
		var platform = this.tbarEx.find('name', 'platform')[0];
		var createTime_start = this.tbarEx.find('name', 'createTime_start')[0];
		var createTime_end = this.tbarEx.find('name', 'createTime_end')[0];
		var searchFields = [platform, createTime_start, createTime_end]
		var store = this.getStore();
		Ext.each(searchFields, function(f) {
					if (f && f.name) {
						if (f.getValue()) {
							store.setBaseParam(f.name, f.getValue())
						} else
							delete store.baseParams[f.name]
					}
				})
		store.load();
	},
	exportData : function(restart) {
		var self = this;
		var platform = this.tbarEx.find('name', 'platform')[0];
		var createTime_start = this.tbarEx.find('name', 'createTime_start')[0];
		var createTime_end = this.tbarEx.find('name', 'createTime_end')[0];
		if (!this.downloadIFrame) {
			this.downloadIFrame = this.getEl().createChild({
						tag : 'iframe',
						style : 'display:none;'
					})
		}
		var params = {
			'platform' : platform.getValue(),
			'createTime_start' : createTime_start.getValue(),
			'createTime_end' : createTime_end.getValue()
		}
		if (restart)
			params['restart'] = 'true';
		Ext.Ajax.request({
			url : 'nick-collect!export.action',
			params : Ext.urlEncode(params),
			success : function(response) {
				var result = Ext.util.JSON.decode(response.responseText);
				if (!result.data) {
					Ext.Msg.alert('提示', '已无数据可以导出');
					return false;
				}
				var timer = new ProgressTimer({
					initData : result.data,
					progressId : 'nickCollectStatus',
					boxConfig : {
						title : '正在导出征名活动明细...',
						buttons : {
							yes : '继续导出',
							no : '从头开始',
							cancel : '关闭'
						},
						fn : function(btn) {
							if (btn == 'yes')
								self.exportData();
							else if (btn == 'no')
								self.exportData(true);
						}
					},
					finish : function() {
						if (!this.downloadIFrame) {
							this.downloadIFrame = this.getEl().createChild({
										tag : 'iframe',
										style : 'display:none;'
									})
						}
						this.downloadIFrame.dom.src = 'nick-collect!exportExcelData.action?&_t='
								+ new Date().getTime();
					},
					scope : this
				});
				timer.start();
			},
			failure : function(response) {
				if (response.responseText)
					Ext.Msg.alert('出错了', response.responseText.replace(
									/\r\n/ig, '<br>'));
			},
			scope : this
		})
	}
});
