RepNavPanel = function() {
	var self = this;
	this.data = {};
	var menu = Dashboard.u().filterAllowed([{
				id : '1',
				authName : 'om.rep.acc',
				name : '访问数据统计',
				icon : 'icon-report-manager',
				module : 'accData',
				panelClass : AccDataPanel
			},

			// {
			// id : '2',
			// authName : 'om.rep.data',
			// name : '数据占比统计',
			// icon : 'icon-report-manager',
			// module : 'dataRate',
			// panelClass : DataRatePanel
			// },
			{
				id : '4',
				authName : 'om.rep.cus',
				name : '客户评价统计',
				icon : 'icon-report-manager',
				module : 'cusApp',
				panelClass : CusAppPanel
			}, {
				id : '5',
				authName : 'om.rep.kno',
				name : '知识点排名统计',
				icon : 'icon-report-manager',
				module : 'knoOrder',
				panelClass : KnoOrdPanel
			},

			// {
			// id : '6',
			// authName : 'om.rep.node',
			// name : '短信统计',
			// icon : 'icon-report-manager',
			// module : 'nodeCount',
			// panelClass : NodeCountPanel
			// },
			{
				id : '3',
				authName : 'om.rep.wel',
				name : '满意度调查统计',
				icon : 'icon-report-manager',
				module : 'userSatisfaction',
				panelClass : UserSatisfactionPanel
			}, {
				id : '7',
				authName : 'om.rep.lm',
				name : '用户留言统计',
				icon : 'icon-report-manager',
				module : 'leaveMessage',
				panelClass : LeaveMessagePanel
			},{
				id : '8',
				authName : 'om.rep.nckclk',
				name : '征名统计',
				module : 'NickCollectPanel',
				icon : 'icon-report-manager',
				panelClass : NickCollectPanel
			}]);
	var rootNode = new Ext.tree.TreeNode({
				id : '0',
				text : 'root'
			});
	var _nodeMap = {};
	for (var i = 0; i < menu.length; i++) {
		var _item = menu[i];
		var _node = new Ext.tree.TreeNode({
					id : _item.id,
					text : _item.name,
					iconCls : _item.icon,
					module : _item.module,
					panelClass : _item.panelClass
				});
		_nodeMap[_item.id] = _node;
		if (!_item.parent)
			rootNode.appendChild(_node);
		else
			_nodeMap[_item.parent].appendChild(_node);
	}
	var config = {
		title : '统计分析管理',
		border : false,
		autoScroll : true,
		rootVisible : false,
		lines : false,
		root : rootNode
	};
	RepNavPanel.superclass.constructor.call(this, config);

	this.modulePanels = {};
	this.on('click', function(n) {
		if (!n)
			return false;
		var module = n.attributes.module;
		var panelClass = n.attributes.panelClass;
		var p = self.showTab(panelClass, module + 'Tab', n.text,
				n.attributes.iconCls, true);
			// if (p)

		});
}

Ext.extend(RepNavPanel, Ext.tree.TreePanel, {});

Dashboard.registerNav(RepNavPanel, 2);