SMSLogDetailPanel = function(_cfg) {

	var self = this;
	this.data = {};
	this.extraAttrs = {};
	var _pageSize = 20;

	var _store = new Ext.data.Store({
				proxy : new Ext.data.HttpProxy({
							url : 'smslog!listDetail.action'
						}),
				reader : new Ext.data.JsonReader({
							idProperty : 'id',
							root : 'data',
							fields : [{
										name : 'id',
										type : 'string'
									}, {
										name : 'qcontent',
										type : 'string'
									}, {
										name : 'acontent',
										type : "string"
									}, {
										name : 'faqname',
										type : "string"
									}, {
										name : 'visittime',
										type : "string"
									}, {
										name : 'answertype',
										type : "string"
									}, {
										name : 'manual',
										type : "string"
									}]
						}),
				writer : new Ext.data.JsonWriter()
			});
	var pagingBar = new Ext.PagingToolbar({
				store : _store,
				displayInfo : true,
				pageSize : _pageSize,
				prependButtons : true,
				beforePageText : '第',
				afterPageText : '页，共{0}页',
				displayMsg : '第{0}到{1}条记录，共{2}条',
				emptyMsg : "没有记录"
			})
	var _sm = new Ext.grid.CheckboxSelectionModel();

	var qcText = new Ext.form.TextField({
				fieldLabel : '咨询问题',
				name : 'qcText',
				// id : 'qcText',
				xtype : 'qcText',
				allowBlank : true,
				width : 120
			});

	var acText = new Ext.form.TextField({
				fieldLabel : '回复答案',
				name : 'acText',
				// id : 'acText',
				xtype : 'textfield',
				allowBlank : true,
				width : 120
			});

	var faText = new Ext.form.TextField({
				fieldLabel : '标准知识点',
				name : 'faText',
				// id : 'faText',
				xtype : 'textfield',
				allowBlank : true,
				width : 120
			});

	var startTimeText = new Ext.form.DateField({
				// id : 'startTimeText',
				name : 'startTimeText',
				fieldLabel : '起始时间',
				xtype : 'datefield',
				format : 'Y-m-d',
				altFormats : 'Y-m-d',
				edit : 'false',
				width : 120
			});

	var endTimeText = new Ext.form.DateField({
				// id : 'endTimeText',
				name : 'endTimeText',
				fieldLabel : '结束时间',
				xtype : 'datefield',
				format : 'Y-m-d',
				altFormats : 'Y-m-d',
				edit : 'false',
				width : 120
			});

	var anType = new Ext.form.ComboBox({
				fieldLabel : '回复类型',
				hiddenName : 'anType',
				valueField : 'id',
				displayField : 'text',
				mode : 'local',
				editable : false,
				typeAhead : true,
				loadMask : true,
				allowBlank : false,
				selectOnFocus : false,
				triggerAction : 'all',
				value : '-1',
				width : 100,
				store : new Ext.data.SimpleStore({
							fields : ["id", "text"],
							data : [[-1, "所有类型"], [1, "有回复"], [2, "默认回复"]]
						})
			});

	this.qcText = qcText;
	this.acText = acText;
	this.startTimeText = startTimeText;
	this.endTimeText = endTimeText;
	this.faText = faText;
	this.anType = anType;

	var cfg = {
		columnLines : true,
		store : _store,
		margins : '0 5 5 5',
		border : false,
		style : 'border-right: 1px solid ' + sys_bdcolor,
		tbar : new Ext.Toolbar({
					items : ['咨询问题:', self.qcText, '时间:从', self.startTimeText,
							'到', self.endTimeText]
				}),
		colModel : new Ext.grid.ColumnModel({
					defaults : {
						sortable : true
					},
					columns : [new Ext.grid.RowNumberer(), _sm, {
								header : 'ID',
								dataIndex : 'id',
								hidden : true
							}, {
								header : '咨询问题',
								dataIndex : 'qcontent'
							}, {
								header : '回复答案',
								dataIndex : 'acontent'
							}, {
								header : '标准知识点',
								dataIndex : 'faqname'
							}, {
								header : '咨询时间',
								dataIndex : 'visittime'
							}, {
								header : '回复类型',
								dataIndex : 'answertype'
							}, {
								header : '是否人工回答',
								dataIndex : 'manual'
							}]
				}),
		sm : _sm,
		bbar : pagingBar,
		loadMask : true,
		viewConfig : {
			forceFit : true
		},
		listeners : {
			render : function() {
				var searchTbar = new Ext.Toolbar({
							items : ['回复答案:', self.acText, '知识点:',
									self.faText, '回复类型:', self.anType, {
										text : '搜索',
										iconCls : 'icon-search',
										handler : function() {
											self.searchWord();
										}
									}, {
										text : '导出',
										iconCls : 'icon-ontology-export',
										handler : function() {
											self.exportData();
										}
									}]
						});
				searchTbar.render(this.tbar);
			}
		}
	}

	SMSLogDetailPanel.superclass.constructor.call(this, Ext.applyIf(_cfg || {},
					cfg));

	this.getPageSize = function() {
		return _pageSize;
	}

	this.addListener('rowdblclick', function(grid, rowIndex, e) {
				var logSession = new DetailWindow();
				logSession.show(this);
				var id = grid.getStore().getAt(rowIndex).data.id;
				logSession.getPanel().load({
							url : 'smslog!queryDetail.action',
							params : {
								"id" : id
							},
							waitMsg : '正在加载数据...',
							success : function(response) {
							},
							failure : function(response) {
								Ext.Msg.alert("提示", "出错或者超时!");
							}
						});
			}, this);
}

Ext.extend(SMSLogDetailPanel, Ext.grid.GridPanel, {
			loadData : function() {
				this.getStore().load({
							params : {
								start : 0,
								limit : this.getPageSize()
							}
						})
			},
			searchWord : function() {
				var _qcText = this.qcText.getValue().trim();
				var _acText = this.acText.getValue().trim();
				var _faText = this.faText.getValue().trim();
				var _bdate = this.startTimeText.getValue();
				var _edate = this.endTimeText.getValue();
				var _anType = this.anType.getValue();
				var self = this;
				var store = this.getStore();
				for (var key in store.baseParams) {
					if (key && key.indexOf('qcontent') != -1) {
						delete store.baseParams[key];
					}
					if (key && key.indexOf('acontent') != -1) {
						delete store.baseParams[key];
					}
					if (key && key.indexOf('faqName') != -1) {
						delete store.baseParams[key];
					}
					if (key && key.indexOf('beginDate') != -1) {
						delete store.baseParams[key];
					}
					if (key && key.indexOf('endDate') != -1) {
						delete store.baseParams[key];
					}
					if (key && key.indexOf('answerType') != -1) {
						delete store.baseParams[key];
					}
				}
				// 绑定查询参数到store,否则点击分页时会出现无参数情况
				if (_qcText)
					store.setBaseParam('qcontent', _qcText);
				if (_acText)
					store.setBaseParam('acontent', _acText);
				if (_faText)
					store.setBaseParam('faqName', _faText);
				if (_bdate)
					store.setBaseParam('beginDate', _bdate);
				if (_edate)
					store.setBaseParam('endDate', _edate);
				if (_anType)
					store.setBaseParam('answerType', _anType);
				this.getStore().load({
							params : {
								start : 0,
								limit : this.getPageSize(),
								'qcontent' : _qcText,
								'acontent' : _acText,
								'faqName' : _faText,
								'beginDate' : _bdate,
								'endDate' : _edate,
								'answerType' : _anType
							}
						});
			},
			exportData : function() {
				var _qcText = this.qcText.getValue().trim();
				var _acText = this.acText.getValue().trim();
				var _faText = this.faText.getValue().trim();
				var _bdate = this.startTimeText.getValue();
				var _edate = this.endTimeText.getValue();
				var _anType = this.anType.getValue();
				if (_bdate != '' && _edate != '') {
					var start = _bdate.getTime();
					var addStart = parseInt(start)
							+ parseInt(7 * 24 * 3600 * 1000) // 得到7天后的毫秒数
					var end = _edate.getTime();
					if (addStart < end) {
						Ext.Msg.alert('错误提示', '导出数据时间段不能超过7天');
						return;
					}
				}
				if (!this.downloadIFrame) {
					this.downloadIFrame = this.getEl().createChild({
								tag : 'iframe',
								style : 'display:none;'
							})
				}
				var params = Ext.urlEncode({
							'qcontent' : _qcText,
							'acontent' : _acText,
							'faqName' : _faText,
							'beginDate' : _bdate,
							'endDate' : _edate,
							'answerType' : _anType
						})
				this.downloadIFrame.dom.src = "smslog!listDetail.action?"
						+ params;
			}
		});