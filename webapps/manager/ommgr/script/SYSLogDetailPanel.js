SYSLogDetailPanel = function(_cfg) {
	var self = this;
	this.data = {};
	this.extraAttrs = {};
	var _pageSize = 20;

	var _store = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({
			url : 'syslog!listDetail.action'
		}),
		reader : new Ext.data.JsonReader({
			idProperty : 'id',
			root : 'data',
			fields : [ {
				name : 'id',
				type : 'string'
			}, {
				name : 'logmodual',
				type : 'string'
			}, {
				name : 'logtype',
				type : "string"
			}, {
				name : 'username',
				type : "string"
			}, {
				name : 'userip',
				type : "string"
			}, 'logcontent', 'operatetime' ]
		}),
		writer : new Ext.data.JsonWriter()
	});
	var pagingBar = new Ext.PagingToolbar({
		store : _store,
		displayInfo : true,
		pageSize : _pageSize,
		prependButtons : true,
		beforePageText : '第',
		afterPageText : '页，共{0}页',
		displayMsg : '第{0}到{1}条记录，共{2}条',
		emptyMsg : "没有记录"
	})
	var _sm = new Ext.grid.RowSelectionModel();

	var typeDataArr = "新增,修改,删除,导入,导出,同步,跑错,重载,保存,应用,登录,授权".split(",")
	var typeData = []
	Ext.each(typeDataArr, function(d, i) {
		typeData.push([ '' + i, d ])
	})
	var logtype = new Ext.form.ComboBox({
		fieldLabel : '操作类型',
		name : 'logtype',
		hiddenName : 'logtype',
		valueField : 'id',
		displayField : 'text',
		mode : 'local',
		editable : false,
		typeAhead : true,
		loadMask : true,
		allowBlank : false,
		selectOnFocus : false,
		triggerAction : 'all',
		width : 60,
		store : new Ext.data.SimpleStore({
			fields : [ "id", "text" ],
			data : [ [ '', "全部" ] ].concat(typeData)
		})
	});
	var module = "知识库管理(0), 本体类管理(1), 词类管理(2), 辅助功能(3), 基础词管理(5), 上下文管理(6), 知识点(51),"
			+ "机器人账号管理(100), 自定义表情管理(101), 消息资源管理(102), 扩展包管理(103), 对话预处理(104)," + "形象套装设置(200), 调查投票管理(201), 动态菜单管理(202), 消息推送管理(203),"
			+ "用户管理(300), 角色管理(301), 资源管理(302), 权限管理(303), 接口管理(304);";
	var moduletypedata = []
	var mr = null;
	var p = /([^,\s]+)\((\d+)\)/ig;
	while (p.test(module)) {
		moduletypedata.push([ RegExp.$2, RegExp.$1 ])
	}
	var logmodual = new Ext.form.ComboBox({
		fieldLabel : '操作模块',
		name : 'logmod',
		hiddenName : 'logmodual',
		valueField : 'id',
		displayField : 'text',
		mode : 'local',
		editable : false,
		typeAhead : true,
		loadMask : true,
		allowBlank : true,
		selectOnFocus : false,
		triggerAction : 'all',
		width : 110,
		store : new Ext.data.SimpleStore({
			fields : [ "id", "text" ],
			data : [ [ '', "全部" ] ].concat(moduletypedata)
		})
	});
	var username = new Ext.form.TextField({
		fieldLabel : '用户名',
		name : 'username',
		xtype : 'textfield',
		allowBlank : true,
		width : 80
	});
	var userip = new Ext.form.TextField({
		fieldLabel : '用户IP',
		name : 'userip',
		xtype : 'textfield',
		allowBlank : true,
		width : 90
	});
	var logcontent = new Ext.form.TextField({
		fieldLabel : '操作内容',
		name : 'logcontent',
		xtype : 'textfield',
		allowBlank : true,
		width : 150
	});

	var starttime = new ClearableDateTimeField({
		editable : false,
		name : 'btime',
		width : 160,
		validator : function(v) {
			if (v) {
				var ev = endtime.getValue();
				if (ev && v.getTime() >= ev.getTime())
					return '开始时间不能晚于结束时间';
			}
			return true;
		}
	});
	this.starttime = starttime;
	var endtime = new ClearableDateTimeField({
		editable : false,
		name : 'etime',
		width : 160
	});
	this.endtime = endtime;

	// var starttime = new Ext.form.DateField({
	// xtype : 'datefield',
	// width : 90,
	// emptyText : '选择时间',
	// format : 'Y-m-d'
	// })
	// var endtime = new Ext.form.DateField({
	// xtype : 'datefield',
	// width : 90,
	// emptyText : '选择时间',
	// format : 'Y-m-d'
	// })

	this.starttime = starttime;
	this.endtime = endtime;
	this.logmodual = logmodual;
	this.logtype = logtype;
	this.username = username;
	this.userip = userip;
	this.logcontent = logcontent;
	var _exportItems = [ new Ext.form.ComboBox({
		ref : 'exportNum',
		valueField : 'num',
		displayField : 'num',
		mode : 'local',
		editable : true,
		selectOnFocus : false,
		triggerAction : 'all',
		validator : function(val) {
			val = val.trim();
			if (val) {
				if (/^[0-9]+$/.test(val)) {
					return true;
				}
				return '请正确输入数字';
			}
			return true;
		},
		width : 100,
		listWidth : 100,
		value : 8000,
		store : new Ext.data.ArrayStore({
			fields : [ 'num' ],
			data : [ [ 100 ], [ 500 ], [ 1000 ], [ 2000 ], [ 5000 ], [ 8000 ] ]
		})
	}), '-', {
		text : '用户名',
		checked : true,
		value : 1
	}, {
		text : '用户IP',
		checked : true,
		value : 2
	}, {
		text : '操作模块',
		checked : true,
		value : 4
	}, {
		text : '操作类型',
		checked : true,
		value : 8
	}, {
		text : '操作内容',
		checked : true,
		value : 16
	}, {
		text : '操作时间',
		checked : true,
		value : 32
	} ];

	var myExpander = new Ext.grid.RowExpander({
		store : _store,
		tpl : new Ext.XTemplate('<p style="color:#999;margin:2px 0px 2px 48px;">{[this.getTpl(values)]}</p>', {
			getTpl : function(v) {
				if (v && v.logcontent) {
					var c = v.logcontent;
					var idx = c.indexOf('->');
					if (idx > -1) {
						c = c.substring(0, idx + 2) + '<span style="color:red;">' + c.substring(idx + 2) + '</span>';
					}
					return c;
				}
				return v.logcontent;
			}
		})
	});

	var cfg = {
		columnLines : true,
		store : _store,
		margins : '0 5 5 5',
		border : false,
		style : 'border-right: 1px solid ' + sys_bdcolor,
		tbar : new Ext.Toolbar({
			enableOverflow : true,
			items : [ '从', self.starttime, '到', self.endtime, '用户名:', self.username, '用户IP:', self.userip, self.logtype, {
				text : '搜索',
				iconCls : 'icon-search',
				handler : function() {
					self.searchWord();
				}
			}, {
				text : '重置',
				icon : 'images/cross.gif',
				handler : function() {
					this.getTopToolbar().findBy(function(cmp) {
						if (cmp.name)
							cmp.reset();
					});
				}.dg(this)
			}, {
				text : '导出',
				xtype : 'splitbutton',
				ref : 'exportBtn',
				menu : {
					defaults : {
						hideOnClick : false
					},
					items : _exportItems
				},
				hidden : !Dashboard.u().allow('om.log.op.EXP'),
				iconCls : 'icon-ontology-export',
				handler : function() {
					self.exportData(true);
				}
			} ]
		}),
		autoExpandColumn : 'logcontent',
		plugins : [ myExpander ],
		colModel : new Ext.grid.ColumnModel({
			defaults : {
				sortable : true
			},
			columns : [ new Ext.grid.RowNumberer(), {
				header : '用户名',
				dataIndex : 'username'
			}, {
				header : '用户IP',
				dataIndex : 'userip'
			}, {
				header : '操作模块',
				dataIndex : 'logmodual'
			}, {
				header : '操作类型',
				dataIndex : 'logtype'
			}, {
				id : 'logcontent',
				header : '操作内容',
				dataIndex : 'logcontent',
				renderer : function(v) {
					if (v) {
						var idx = v.indexOf('->');
						if (idx > -1) {
							v = v.substring(0, idx + 2) + '<span style="color:red;">' + v.substring(idx + 2) + '</span>';
						}
					}
					return v;
				}
			}, {
				header : '操作时间',
				dataIndex : 'operatetime'
			} ]
		}),
		sm : _sm,
		bbar : pagingBar,
		loadMask : true,
		viewConfig : {
			forceFit : true
		}
	}

	this.tbarEx = new Ext.Toolbar({
		items : [ '操作模块:', self.logmodual, '操作类型:', self.logtype, '操作内容:', logcontent ]
	});

	SYSLogDetailPanel.superclass.constructor.call(this, Ext.applyIf(_cfg || {}, cfg));

	this.addListener('render', function() {
		this.tbarEx.render(this.tbar);
	}, this);

	this.getPageSize = function() {
		return _pageSize;
	}

	this.on('celldblclick', function(grid, rowIndex, columnIndex, e) {
		var r = this.store.getAt(rowIndex);
		var c = r.get('logcontent');
		if (columnIndex == 0 && c && c.indexOf('实例') != -1) {
			var t = r.get('operatetime');
			var oname;
			if (c.indexOf('实例名称') != -1) {
				oname = c.replace(/.+?:/, '')
			} else {
				var p = /'(.+?)'/
				var r;
				if (r = p.exec(c)) {
					oname = r[1];
				}
			}
			if (oname) {
				window.open('../kbmgr/ontology-object!revisionData.action?objname=' + oname + '&endDate=' + t);
			}
		}
	}, this);

}

Ext.extend(SYSLogDetailPanel, Ext.grid.GridPanel, {
	initComponent : function() {
		SYSLogDetailPanel.superclass.initComponent.call(this);
		this.getTopToolbar().items.each(function(c) {
			if (c instanceof Ext.form.TextField) {
				c.on('specialkey', function(f, e) {
					if (e.ENTER == e.getKey()) {
						this.searchWord();
					}
				}, this)
			}
		}, this)
	},
	loadData : function() {
		this.getStore().load({
			params : {
				start : 0,
				limit : this.getPageSize()
			}
		})
	},
	searchWord : function() {
		var _logmodual = this.logmodual.getValue().trim();
		var _logtype = this.logtype.getValue();
		var _username = this.username.getValue();
		var _userip = this.userip.getValue();
		var _start = this.starttime.getValue();
		var _end = this.endtime.getValue();
		var _logcontent = this.logcontent.getValue();
		var self = this;
		var store = this.getStore();
		for ( var key in store.baseParams) {
			if (key && key.indexOf('logmodual') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('logtype') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('username') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('userip') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('starttime') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('endtime') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('logcontent') != -1) {
				delete store.baseParams[key];
			}
		}
		if (_logmodual)
			store.setBaseParam('logmodual', _logmodual);
		if (_logtype)
			store.setBaseParam('logtype', _logtype);
		if (_username)
			store.setBaseParam('username', _username);
		if (_userip)
			store.setBaseParam('userip', _userip);
		if (_start)
			store.setBaseParam('starttime', _start);
		if (_end)
			store.setBaseParam('endtime', _end);
		if (_logcontent)
			store.setBaseParam('logcontent', _logcontent);
		this.getStore().load({
			params : {
				start : 0,
				limit : this.getPageSize()
			}
		});
	},
	// exportData : function() {
	// var _logmodual = this.logmodual.getValue().trim();
	// var _logtype = this.logtype.getValue();
	// var _username = this.username.getValue();
	// var _userip = this.userip.getValue();
	// var _start = this.starttime.getValue();
	// var _end = this.endtime.getValue();
	// if (!this.downloadIFrame) {
	// this.downloadIFrame = this.getEl().createChild({
	// tag : 'iframe',
	// style : 'display:none;'
	// })
	// }
	// var params = Ext.urlEncode({
	// 'logmodual' : _logmodual,
	// 'logtype' : _logtype,
	// 'username' : _username,
	// 'userip' : _userip,
	// 'starttime' : _start,
	// 'endtime' : _end
	// })
	// this.downloadIFrame.dom.src = "syslog!listDetail.action?" + params;
	// },
	exportData : function(restart) {
		var self = this;
		var _logmodual = this.logmodual.getValue().trim();
		var _logtype = this.logtype.getValue();
		var _username = this.username.getValue();
		var _userip = this.userip.getValue();
		var _logcontent = this.logcontent.getValue();
		var beginTime = this.starttime.getValue();
		var endTime = this.endtime.getValue();
		if (beginTime && endTime) {
			if (beginTime.getTime() >= endTime.getTime()) {
				Ext.Msg.alert('提示', '开始时间不能晚于结束时间');
				return false;
			}
		}
		var params = {
			'logmodual' : _logmodual,
			'logtype' : _logtype,
			'username' : _username,
			'userip' : _userip,
			'starttime' : beginTime,
			'endtime' : endTime,
			'logcontent' : _logcontent
		};
		if (restart)
			params['restart'] = 'true';
		var exportcols = 0;
		var _exportItems = this.getTopToolbar().exportBtn.menu.items.items;
		var expNum = this.getTopToolbar().exportBtn.menu.exportNum.getValue();
		if (!/^[0-9]+$/.test(expNum)) {
			Ext.Msg.alert('提示', '您填写的导出数量不合法');
			return false;
		}
		Ext.each(_exportItems, function(item) {
			if (item.checked)
				exportcols = exportcols | item.value;
		});
		if (exportcols == 0) {
			Ext.Msg.alert('提示', '请至少选择一列进行导出');
			return false;
		}
		params['cols'] = exportcols;
		params['num'] = expNum;
		Ext.Ajax.request({
			url : 'syslog!export.action',
			params : Ext.urlEncode(params),
			success : function(response) {
				var result = Ext.util.JSON.decode(response.responseText);
				if (!result.success) {
					Ext.Msg.alert('提示', result.message);
					return false;
				}
				if (!result.data) {
					Ext.Msg.alert('提示', '无数据可以导出');
					return false;
				}
				var timer = new ProgressTimer({
					initData : result.data,
					progressId : 'logOPStatus',
					boxConfig : {
						title : '正在导出操作日志明细...',
						buttons : {
							yes : '继续导出',
							no : '从头开始',
							cancel : '关闭'
						},
						fn : function(btn) {
							if (btn == 'yes')
								self.exportData();
							else if (btn == 'no')
								self.exportData(true);
						}
					},
					finish : function() {
						if (!this.downloadIFrame) {
							this.downloadIFrame = this.getEl().createChild({
								tag : 'iframe',
								style : 'display:none;'
							});
						}
						this.downloadIFrame.dom.src = 'syslog!exportExcelData.action?&_t=' + new Date().getTime();
					},
					scope : this
				});
				timer.start();
			},
			failure : function(response) {
				if (response.responseText)
					Ext.Msg.alert('出错了', response.responseText.replace(/\r\n/ig, '<br>'));
			},
			scope : this
		});
	}
});