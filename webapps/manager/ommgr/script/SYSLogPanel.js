SYSLogPanel = function(_cfg) {

	var self = this;
	this.data = {};
	this.extraAttrs = {};
	var _pageSize = 20;

	var _store = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({
			url : 'msnlogsession!listDetail.action'
		}),
		reader : new Ext.data.JsonReader({
			idProperty : 'id',
			root : 'data',
			fields : [ {
				name : 'qcontent',
				type : 'string'
			}, {
				name : 'acontent',
				type : "string"
			}, {
				name : 'faqname',
				type : "string"
			}, {
				name : 'visittime',
				type : "string"
			}, {
				name : 'answertype',
				type : "string"
			}, {
				name : 'manual',
				type : "string"
			} ]
		}),
		writer : new Ext.data.JsonWriter()
	});
	var pagingBar = new Ext.PagingToolbar({
		store : _store,
		displayInfo : true,
		pageSize : _pageSize,
		prependButtons : true,
		beforePageText : '第',
		afterPageText : '页，共{0}页',
		displayMsg : '第{0}到{1}条记录，共{2}条',
		emptyMsg : "没有记录"
	})
	var _sm = new Ext.grid.CheckboxSelectionModel();

	var qcText = new Ext.form.TextField({
		fieldLabel : '咨询问题',
		name : 'qcText',
		id : 'qcText',
		xtype : 'qcText',
		allowBlank : true,
		width : 180
	});

	var acText = new Ext.form.TextField({
		fieldLabel : '回复答案',
		name : 'acText',
		id : 'acText',
		xtype : 'textfield',
		allowBlank : true,
		width : 180
	});

	var faText = new Ext.form.TextField({
		fieldLabel : '标准知识点',
		name : 'faText',
		id : 'faText',
		xtype : 'textfield',
		allowBlank : true,
		width : 180
	});

	var startTimeText = new Ext.form.DateField({
		id : 'startTimeText',
		name : 'startTimeText',
		fieldLabel : '起始时间',
		xtype : 'datefield',
		format : 'Y-m-d',
		altFormats : 'Y-m-d',
		edit : 'false',
		width : 180
	});

	var endTimeText = new Ext.form.DateField({
		id : 'endTimeText',
		name : 'endTimeText',
		fieldLabel : '结束时间',
		xtype : 'datefield',
		format : 'Y-m-d',
		altFormats : 'Y-m-d',
		edit : 'false',
		width : 180
	});

	var anType = new Ext.form.ComboBox({
		fieldLabel : '回复类型',
		hiddenName : 'anType',
		valueField : 'id',
		displayField : 'text',
		mode : 'local',
		editable : false,
		typeAhead : true,
		loadMask : true,
		allowBlank : false,
		selectOnFocus : false,
		triggerAction : 'all',
		value : '2',
		width : 180,
		store : new Ext.data.SimpleStore({
			fields : [ "id", "text" ],
			data : [ [ 1, "是" ], [ 2, "否" ] ]
		})
	});

	var manualCombo = new Ext.form.ComboBox({
		fieldLabel : '是否人工回答',
		hiddenName : 'manual',
		valueField : 'id',
		displayField : 'text',
		mode : 'local',
		editable : false,
		typeAhead : true,
		loadMask : true,
		allowBlank : false,
		selectOnFocus : false,
		triggerAction : 'all',
		value : '2',
		width : 180,
		store : new Ext.data.SimpleStore({
			fields : [ "id", "text" ],
			data : [ [ 1, "是" ], [ 2, "否" ] ]
		})
	});

	this.qcText = qcText;
	this.acText = acText;
	this.startTimeText = startTimeText;
	this.endTimeText = endTimeText;
	this.faText = faText;
	this.anType = anType;
	this.manualCombo = manualCombo;

	var cfg = {
		id : 'SYSLogPanel',
		stripeRows : true,
		border : false,
		frame : false,
		bodyStyle : 'padding:20px 10px 10px 15px',
		store : _store,
		loadMask : true,
		items : [ {
			layout : 'column',
			border : false,
			buttonAlign : 'center',
			items : [ {
				border : false,
				columnWidth : .3,
				layout : 'form',
				autoHeight : true,
				items : [ qcText, acText ]
			}, {
				border : false,
				columnWidth : .3,
				layout : 'form',
				autoHeight : true,
				items : [ faText, startTimeText ]
			}, {
				border : false,
				columnWidth : .3,
				layout : 'form',
				autoHeight : true,
				items : [ endTimeText, anType ]
			}, {
				border : false,
				columnWidth : .3,
				layout : 'form',
				autoHeight : true,
				items : [ manualCombo ]
			} ],
			buttons : [ {
				text : '查询',
				handler : function() {
					self.searchWord();
				}
			}, {
				text : "导出",
				handler : function() {
					if (LogSession.form.logSessionFormPanel.getForm().isValid()) {
						LogSession.data.beginExport();
					}
				}
			}, {
				text : "重置",
				handler : function() {
					qcText.setValue('');
					acText.setValue('');
					faText.setValue('');
					startTimeText.setValue('');
					endTimeText.setValue('');
					anType.setValue('');
					manualCombo.setValue('');
				}
			} ]

		} ],
		colModel : new Ext.grid.ColumnModel({
			defaults : {
				sortable : true
			},
			columns : [ new Ext.grid.RowNumberer(), _sm, {
				header : '咨询问题',
				dataIndex : 'qcontent'
			}, {
				header : '回复答案',
				dataIndex : 'acontent'
			}, {
				header : '标准知识点',
				dataIndex : 'faqname'
			}, {
				header : '咨询时间',
				dataIndex : 'visittime'
			}, {
				header : '回复类型',
				dataIndex : 'answertype'
			}, {
				header : '是否人工回答',
				dataIndex : 'manual'
			} ]
		}),
		sm : _sm,
		bbar : pagingBar,
		viewConfig : {
			forceFit : true
		}
	}

	SYSLogPanel.superclass.constructor.call(this, Ext.applyIf(_cfg || {}, cfg));

	this.getPageSize = function() {
		return _pageSize;
	}

	this.addListener('rowdblclick', function() {
		alert('查看详情');
	}, this);

}

Ext.extend(SYSLogPanel, Ext.grid.GridPanel, {
	loadData : function() {
		this.getStore().load({
			params : {
				start : 0,
				limit : this.getPageSize()
			}
		})
	},
	searchWord : function() {
		var _qcText = this.qcText.getValue().trim();
		var _acText = this.acText.getValue().trim();
		var _faText = this.faText.getValue().trim();
		var _startTime = Ext.getCmp("startTimeText").getRawValue();
		var _endTime = document.getElementById("endTimeText").value;
		var _anType = this.anType.getValue().trim();
		var _manual = this.manualCombo.getValue().trim();
		var self = this;
		this.getStore().load({
			params : {
				start : 0,
				limit : this.getPageSize(),
				'qc' : _acText,
				'ac' : _acText,
				'fa' : _faText,
				'beginDate' : _bdate,
				'endDate' : _edate,
				'an' : _anType,
				'ma' : _manual
			}
		});
	}
});