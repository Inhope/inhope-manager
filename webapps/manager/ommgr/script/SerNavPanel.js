SerNavPanel = function() {
	var self = this;
	this.data = {};
	var menu = [{
		id : '1',
		name : '需求确定中',
		icon : 'icon-expand'
			// panelClass : developing
		}];
	var rootNode = new Ext.tree.TreeNode({
				id : '0',
				text : 'root'
			});
	var _nodeMap = {};
	for (var i = 0; i < menu.length; i++) {
		var _item = menu[i];
		var _node = new Ext.tree.TreeNode({
					id : _item.id,
					text : _item.name,
					iconCls : _item.icon,
					module : _item.module,
					panelClass : _item.panelClass
				});
		_nodeMap[_item.id] = _node;
		if (!_item.parent)
			rootNode.appendChild(_node);
		else
			_nodeMap[_item.parent].appendChild(_node);
	}
	var config = {
		title : '服务监控管理',
		border : false,
		autoScroll : true,
		rootVisible : false,
		lines : false,
		root : rootNode
	};
	SerNavPanel.superclass.constructor.call(this, config);

	this.modulePanels = {};
	this.on('click', function(n) {
		if (!n)
			return false;
		var module = n.attributes.module;
		var panelClass = n.attributes.panelClass;
		var p = self.showTab(panelClass, module + 'Tab', n.text,
				n.attributes.iconCls, true);
			// if (p)
			// p.loadData();

		});
}

Ext.extend(SerNavPanel, Ext.tree.TreePanel, {});

//Dashboard.registerNav(SerNavPanel, 4);