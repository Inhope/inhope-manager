StatServiceLogPanel = function(_cfg) {
	var self = this;
	this.statBy = null;

	var fields = [ {
		name : 'serviceId'
	}, {
		type : 'int',
		name : 'totalSession'
	}, {
		type : 'int',
		name : 'totalUser'
	}, {
		type : 'int',
		name : 'totalQues'
	}, {
		type : 'int',
		name : 'totalEnter'
	} ];
	var rowIndex = new Ext.grid.RowNumberer();
	var columns = [ rowIndex, {
		dataIndex : 'serviceId',
		header : Dashboard.getColumnTitle('服务ID'),
		align : 'center'
	}, {
		dataIndex : 'totalQues',
		header : Dashboard.getColumnTitle('总提问数'),
		align : 'center'
	}, {
		dataIndex : 'totalEnter',
		header : Dashboard.getColumnTitle('进入服务次数'),
		align : 'center'
	}, {
		dataIndex : 'totalUser',
		header : Dashboard.getColumnTitle('独立用户数'),
		align : 'center'
	}, {
		dataIndex : 'totalSession',
		header : Dashboard.getColumnTitle('总会话数'),
		align : 'center'
	} ];
	var _store = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({
			url : 'stat-service-log!stat.action'
		}),
		reader : new JsonReaderEx({
			idProperty : 'id',
			root : 'data',
			fields : fields
		})
	});

	var queryByItem = new Ext.form.ComboBox({
		fieldLabel : '统计项',
		hiddenName : 'queryByItem',
		valueField : 'id',
		displayField : 'text',
		mode : 'local',
		editable : false,
		typeAhead : true,
		loadMask : true,
		allowBlank : false,
		selectOnFocus : false,
		triggerAction : 'all',
		emptyText : '请选择统计项',
		width : 120,
		store : new Ext.data.SimpleStore({
			fields : [ "id", "text" ],
			data : [ [ 1, "时间段(天)" ], [ 2, "时间(月)" ] ]
		}),
		listeners : {
			'select' : function(tp) {
				self.checkItem();
			}
		}
	});

	this.initDateField = function() {
		this.dateField = new Ext.form.DateField({
			name : 'timeText',
			xtype : 'datefield',
			fieldLabel : '选择日期',
			emptyText : '选择日期',
			format : 'Y-m-d',
			editable : false,
			width : 100
		});
	};

	this.initMonthField = function() {
		this.monthField = new DateFieldEx({
			name : 'toMonth',
			xtype : 'datefield',
			fieldLabel : '月份',
			emptyText : '请选择月份',
			format : 'Y-m',
			editable : false,
			width : 100
		});
	};
	this.initDimChooser = function() {
		this.dimChooser = Dashboard.createDimChooser(150);
	};

	var queryButton = new Ext.Button({
		text : '统计',
		iconCls : 'icon-search',
		handler : function() {
			if (self.queryByItem.getValue() == null || self.queryByItem.getValue() == '') {
				Ext.Msg.alert('错误提示', '请选择统计项');
				return;
			}
			self.countData(self.queryByItem.getValue(), false);
		}
	});

	var exportButton = new Ext.Button({
		text : '导出结果',
		iconCls : 'icon-ontology-export',
		hidden : !Dashboard.u().allow('om.stat.svc.EXP'),
		handler : function() {
			var records = _store.getRange();
			if (records.length) {
				self.getEl().mask('正在导出数据...');
				var data = [];
				var quesTypeData = [];
				Ext.each(records, function(rec) {
					data.push(rec.data);
					quesTypeData.push(Ext.encode(rec.json.quesTypeNums));
				});
				var month = '', day = '', _city = '', _platform = '';
				if (self.monthField && self.monthField.getEl().dom.style.display != 'none')
					month = self.monthField.getRawValue();
				if (self.dateField && self.dateField.getEl().dom.style.display != 'none')
					day = self.dateField.getRawValue();
				if (self.city && self.city.getEl().dom.style.display != 'none')
					_city = self.city.getRawValue();
				if (self.channel && self.channel.getEl().dom.style.display != 'none')
					_platform = self.channel.getRawValue();
				Ext.Ajax.request({
					url : 'stat-service-log!export.action',
					params : {
						data : Ext.encode(data),
						quesTypeData : '[' + quesTypeData.toString() + ']',
						statBy : self.statBy,
						month : month,
						day : day,
						byDim : true,
						city : _city,
						platform : _platform,
						dims : self.dimChooser.getValue()
					},
					success : function(resp) {
						self.getEl().unmask();
						if (!self.downloadIFrame) {
							self.downloadIFrame = self.getEl().createChild({
								tag : 'iframe',
								style : 'display:none;'
							});
						}
						self.downloadIFrame.dom.src = 'stat-service-log!getExport.action?&_t=' + new Date().getTime();
					},
					failure : function(resp) {
						self.getEl().unmask();
						Ext.Msg.alert('错误', resp.responseText);
					}
				});
			} else {
				Ext.Msg.alert('提示', '无数据可供导出');
			}
		}
	});

	var fSet = new Ext.form.FieldSet({
		xtype : 'fieldset',
		border : false,
		style : 'padding : 3px;',
		items : [ {
			layout : 'column',
			labelAlign : "left",
			bodyStyle : 'padding-top:5px;background-color:' + sys_bgcolor,
			border : false,
			items : [ {
				border : false,
				items : queryButton
			}, {
				border : false,
				bodyStyle : 'margin-left : 2px',
				items : exportButton
			}, {
				border : false,
				layout : 'column',
				bodyStyle : 'background-color:' + sys_bgcolor + ' margin-top:4px;',
				html : '&nbsp;&nbsp;统计项:&nbsp;&nbsp;'
			}, {
				border : false,
				items : queryByItem
			}, {
				border : false,
				layout : 'column',
				bodyStyle : 'background-color:' + sys_bgcolor + ' margin-top:4px;',
				html : '&nbsp;&nbsp;统计条件:&nbsp;&nbsp;'
			} ]
		} ]

	});

	var dataPanelCfg = {
		name : 'statServiceLogPanel',
		border : false,
		store : _store,
		columns : columns,
		stripeRows : true,
		region : 'center',
		viewConfig : {
			forceFit : true
		}
	};
	var dataPanel = new Ext.grid.GridPanel(Ext.applyIf(_cfg || {}, dataPanelCfg));

	this.newColumnChart = function() {
		return new Ext.chart.ColumnChart({
			xtype : 'columnchart',
			store : _store,
			url : '../ext/resources/charts.swf',
			xField : 'serviceId',
			xAxis : new Ext.chart.CategoryAxis({
				title : '服务ID'
			}),
			yAxis : new Ext.chart.NumericAxis({
				title : '数量'
			}),
			series : [ {
				type : 'column',
				displayName : '提问次数',
				yField : 'totalQues',
				style : {
					mode : 'stretch',
					color : 0x99BBE8
				}
			}, {
				type : 'column',
				displayName : '进入服务次数',
				yField : 'totalEnter',
				style : {
					color : 0x15428B
				}
			} ],
			extraStyle : {
				font : {
					size : 9
				},
				yAxis : {}
			}
		});
	};

	var cfg = {
		layout : 'border',
		border : false,
		items : [ {
			layout : 'form',
			border : false,
			region : 'north',
			height : 38,
			bodyStyle : 'border-bottom: 1px solid ' + sys_bdcolor + ' border-right: 1px solid ' + sys_bdcolor,
			buttonAlign : 'center',
			items : [ {
				layout : 'form',
				border : false,
				bodyStyle : 'background-color:' + sys_bgcolor,
				labelAlign : "center",
				items : [ fSet ]
			} ]
		}, dataPanel, {
			region : 'south',
			xtype : 'panel',
			ref : 'southPanel',
			header : false,
			split : true,
			collapsible : true,
			collapseMode : 'mini',
			frame : false,
			border : false,
			height : 250
		} ]
	};

	StatServiceLogPanel.superclass.constructor.call(this, cfg);

	this.queryByItem = queryByItem;
	this.fSet = fSet;
	this.store = _store;
};
Ext.extend(StatServiceLogPanel, Ext.Panel, {
	checkItem : function() {
		var self = this;
		var data = this.queryByItem.getValue();
		self.cleanData(data);
		if (data == 1) {
			this.statBy = 'day';
			this.initDateField();
			this.fSet.items.item(0).add(this.dateField);
			this.dateField.setValue(new Date());
			this.initDimChooser();
			this.fSet.items.item(0).add(this.dimChooser);
		}
		if (data == 2) {
			this.statBy = 'month';
			this.initMonthField();
			this.fSet.items.item(0).add(this.monthField);
			this.monthField.setValue(new Date());
			this.initDimChooser();
			this.fSet.items.item(0).add(this.dimChooser);
		}
		this.doLayout();
	},

	cleanData : function(data) {
		if (typeof (this.fSet.items.item(0).get(this.dateField)) != 'undefined') {
			this.dateField.getEl().dom.style.display = 'none';
			this.dateField = null;
		}
		if (typeof (this.fSet.items.item(0).get(this.monthField)) != 'undefined') {
			this.monthField.getEl().dom.style.display = 'none';
			this.monthField = null;
		}
		if (typeof (this.fSet.items.item(0).get(this.dimChooser)) != 'undefined') {
			this.dimChooser.getEl().dom.style.display = 'none';
			this.dimChooser = null;
		}
	},

	countData : function(data, isRate) {
		var statKey;
		if (data == 1)
			statKey = this.dateField.getRawValue();
		else
			statKey = this.monthField.getRawValue();
		var dims = this.dimChooser.getValue();
		var self = this;
		self.southPanel.removeAll(true);
		params = {
			dims : dims,
			statkey : statKey.replace(/-/g, '')
		};
		Ext.Ajax.request({
			url : 'stat-service-log!stat.action',
			params : Ext.urlEncode(params),
			success : function(response) {
				var result = Ext.util.JSON.decode(response.responseText);
				var _ts = result.data.data;
				var timer = new ProgressTimer({
					initData : result.data,
					progressId : 'statServiceLog',
					boxConfig : {
						title : '正在统计' + statKey + '的数据'
					},
					finish : function() {
						Ext.Ajax.request({
							url : 'stat-service-log!getStatResult.action',
							params : {
								ts : _ts
							},
							success : function(resp) {
								var statResult = Ext.util.JSON.decode(resp.responseText);
								self.store.loadData(statResult);
								var chart = self.newColumnChart();
								self.southPanel.add(chart);
								self.doLayout();
							}
						});
					},
					scope : this
				});
				timer.start();
			},
			failure : function(response) {
				if (response.responseText)
					Ext.Msg.alert('出错了', response.responseText.replace(/\r\n/ig, '<br>'));
			},
			scope : this
		});
	}
});
