UnresolvedQuesPanel = function(_cfg) {
	var self = this;
	var _pageSize = 20;
	var fields = [ {
		type : 'int',
		name : 'times'
	}, {
		type : 'string',
		name : 'question'
	}, {
		type : 'string',
		name : 'normQuestion'
	}, {
		type : 'string',
		name : 'createTime'
	}, {
		type : 'string',
		name : 'updateTime'
	}, {
		type : 'int',
		name : 'type'
	}, {
		type : 'string',
		name : 'location'
	}, {
		type : 'string',
		name : 'platform'
	} ];
	var columns = [ new Ext.grid.RowNumberer(), {
		dataIndex : 'question',
		header : '用户问句',
		align : 'left',
		renderer : function(v) {
			return v.replace(/>/g, '&gt;').replace(/</g, '&lt;');
		}
	}, {
		dataIndex : 'normQuestion',
		header : '格式化问句',
		align : 'left',
		width : 100,
		renderer : function(v) {
			return v.replace(/>/g, '&gt;').replace(/</g, '&lt;');
		}
	}, {
		dataIndex : 'times',
		header : '提问次数',
		width : 45,
		align : 'center'
	}, {
		dataIndex : 'type',
		header : '类型',
		width : 40,
		align : 'center',
		renderer : function(v) {
			if (v == 1)
				return '全数字';
			if (v == 2)
				return '全字母';
			if (v == 4)
				return '字母数字';
			if (v == 8)
				return '含中文';
		}
	}, {
		dataIndex : 'createTime',
		header : '首次提问时间',
		width : 75,
		align : 'center'
	}, {
		dataIndex : 'updateTime',
		header : '末次提问时间',
		width : 75,
		align : 'center'
	} ];

	var _store = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({
			url : 'unresolved-ques!list.action'
		}),
		reader : new JsonReaderEx({
			idProperty : 'id',
			root : 'data',
			fields : fields
		})
	});

	var queryByItem = new Ext.form.ComboBox({
		fieldLabel : '统计项',
		hiddenName : 'queryByItem',
		valueField : 'id',
		displayField : 'text',
		mode : 'local',
		editable : false,
		typeAhead : true,
		loadMask : true,
		allowBlank : false,
		selectOnFocus : false,
		triggerAction : 'all',
		emptyText : '选择统计项',
		width : 88,
		store : new Ext.data.SimpleStore({
			fields : [ "id", "text" ],
			data : [ [ 1, "时间(天)" ], [ 2, "时间(月)" ], [ 3, "时间段" ] ]
		}),
		listeners : {
			'select' : function(tp) {
				self.checkItem();
			}
		}
	});

	this.initTime = function() {
		this.time = new Ext.form.DateField({
			name : 'timeText',
			xtype : 'datefield',
			fieldLabel : '选择日期',
			emptyText : '选择日期',
			format : 'Y-m-d',
			editable : false,
			width : 90
		});
		this.endtime = new Ext.form.DateField({
			name : 'endTimeText',
			xtype : 'datefield',
			fieldLabel : '选择日期',
			emptyText : '选择日期',
			format : 'Y-m-d',
			editable : false,
			width : 90
		});
	};
	this.initToMonth = function() {
		this.toMonth = new DateFieldEx({
			name : 'toMonth',
			xtype : 'datefield',
			fieldLabel : '月份',
			emptyText : '请选择月份',
			format : 'Y-m',
			editable : false,
			width : 90
		});
	};
	this.initTypeCombo = function() {
		this.searchTypeCombo = new Ext.form.ComboBox({
			fieldLabel : '类型',
			valueField : 'id',
			displayField : 'text',
			emptyText : '选择类型',
			editable : false,
			allowBlank : true,
			triggerAction : 'all',
			mode : 'local',
			// value : 0,
			store : new Ext.data.SimpleStore({
				fields : [ "id", "text" ],
				data : [ [ 0, '全部' ], [ 1, "全数字" ], [ 2, "全字母" ], [ 4, "字母数字" ], [ 8, "含中文" ] ]
			}),
			width : 80
		});
	};

	var queryButton = new Ext.Button({
		text : '统计',
		iconCls : 'icon-search',
		handler : function() {
			if (self.queryByItem.getValue() == null || self.queryByItem.getValue() == '') {
				Ext.Msg.alert('错误提示', '请选择统计项');
				return;
			}
			self.countData(self.queryByItem.getValue());
		}
	});

	var exportButton = new Ext.SplitButton({
		text : '导出结果',
		iconCls : 'icon-ontology-export',
		hidden : !Dashboard.u().allow('om.stat.uq.EXP'),
		menu : {
			defaults : {
				hideOnClick : false
			},
			items : [ new Ext.form.ComboBox({
				ref : 'exportNum',
				valueField : 'num',
				displayField : 'num',
				mode : 'local',
				editable : true,
				selectOnFocus : false,
				triggerAction : 'all',
				validator : function(val) {
					val = val.trim();
					if (val) {
						if (/^[0-9]+$/.test(val)) {
							var intVal = parseInt(val);
							if (intVal >= 100 && intVal <= 8000)
								return true;
						}
						return '100到8000间的整数';
					}
					return true;
				},
				width : 100,
				listWidth : 100,
				value : 5000,
				store : new Ext.data.ArrayStore({
					fields : [ 'num' ],
					data : [ [ 100 ], [ 500 ], [ 1000 ], [ 2000 ], [ 5000 ], [ 8000 ] ]
				})
			}) ]
		},
		handler : function() {
			_doExport(0);
		}
	});
	_doExport = function(start) {
		var records = _store.getRange();
		if (records.length) {
			var size = parseInt(exportButton.menu.exportNum.getValue());
			if (size < 50 || size > 8000) {
				Ext.Msg.alert('提示', '导出数量必须是100到8000间的整数');
				return false;
			}
			var params = {
				expstart : start,
				expsize : size
			};
			Ext.apply(params, _store.baseParams);
			self.getEl().mask('正在导出数据...');
			Ext.Ajax.request({
				url : 'unresolved-ques!export.action',
				params : params,
				success : function(resp) {
					self.getEl().unmask();
					var result = Ext.util.JSON.decode(resp.responseText);
					if (!result.data) {
						Ext.Msg.alert('提示', '已无数据可以导出');
						return false;
					} else {
						Ext.Msg.show({
							title : '操作提示',
							buttons : {
								yes : '继续导出',
								no : '从头开始',
								cancel : '关闭'
							},
							width : 300,
							fn : function(result) {
								if (result == 'yes') {
									_doExport(start + size);
								} else if (result == 'no') {
									_doExport(0);
								}
							}
						});
					}
					if (!self.downloadIFrame) {
						self.downloadIFrame = self.getEl().createChild({
							tag : 'iframe',
							style : 'display:none;'
						});
					}
					self.downloadIFrame.dom.src = 'unresolved-ques!getExport.action?&_t=' + new Date().getTime();
				},
				failure : function(resp) {
					self.getEl().unmask();
					Ext.Msg.alert('错误', resp.responseText);
				}
			});
		} else {
			Ext.Msg.alert('提示', '无数据可供导出');
		}
	};

	var fSet = new Ext.form.FieldSet({
		xtype : 'fieldset',
		border : false,
		style : 'padding : 3px;',
		items : [ {
			layout : 'column',
			labelAlign : "left",
			border : false,
			bodyStyle : 'padding-top:5px;background-color:' + sys_bgcolor,
			items : [ {
				border : false,
				items : queryButton
			}, {
				border : false,
				bodyStyle : 'margin-left:2px',
				items : exportButton
			}, {
				border : false,
				layout : 'column',
				html : '&nbsp;&nbsp;统计项:&nbsp;&nbsp;',
				bodyStyle : 'background-color:' + sys_bgcolor + ' margin-top:4px;'
			}, {
				border : false,
				items : queryByItem
			}, {
				border : false,
				layout : 'column',
				html : '&nbsp;&nbsp;统计条件:&nbsp;&nbsp;',
				bodyStyle : 'background-color:' + sys_bgcolor + ' margin-top:4px;'
			} ]
		} ]

	});
	var pagingBar = new Ext.CustomPagingToolbar({
		store : _store,
		displayInfo : true,
		pageSize : _pageSize,
		prependButtons : true,
		beforePageText : '第',
		afterPageText : '页，共{0}页',
		displayMsg : '第{0}到{1}条记录，共{2}条',
		emptyMsg : "没有记录",
		customBtn : {
			text : '总数',
			tooltip : '计算总数',
			handler : function(btn) {
				if (_store.getCount() == 0) {
					this.updateInfo0('无数据');
					return false;
				}
				btn.setDisabled(true);
				var self = this;
				Ext.Ajax.request({
					url : 'unresolved-ques!total.action',
					params : Ext.urlEncode(_store.baseParams),
					success : function(resp) {
						var result = Ext.util.JSON.decode(resp.responseText);
						if (result && result.data)
							self.updateInfo0('总提问次数: ' + result.data);
						btn.setDisabled(false);
					},
					failure : function(resp) {
						Ext.Msg.alert('错误', resp.responseText);
						btn.setDisabled(false);
					}
				});
			}
		}
	});

	var dataPanelCfg = {
		name : 'unresolvedQuesPanel',
		border : false,
		store : _store,
		loadMask : true,
		columns : columns,
		stripeRows : true,
		region : 'center',
		viewConfig : {
			forceFit : true
		},
		bbar : pagingBar
	};
	var dataPanel = new Ext.grid.GridPanel(Ext.applyIf(_cfg || {}, dataPanelCfg));

	var cfg = ({
		layout : 'border',
		border : false,
		items : [ {
			layout : 'form',
			border : false,
			region : 'north',
			height : 38,
			bodyStyle : 'border-bottom: 1px solid ' + sys_bdcolor + ' border-right: 1px solid ' + sys_bdcolor,
			buttonAlign : 'center',
			items : [ {
				layout : 'form',
				border : false,
				bodyStyle : 'background-color:' + sys_bgcolor,
				labelAlign : "center",
				items : [ fSet ]
			} ]
		}, dataPanel ]
	});

	UnresolvedQuesPanel.superclass.constructor.call(this, cfg);

	this.queryByItem = queryByItem;
	this.getPageSize = function() {
		return _pageSize;
	}, this.fSet = fSet;
	this.store = _store;
};
Ext.extend(UnresolvedQuesPanel, Ext.Panel, {
	checkItem : function() {
		var self = this;
		var data = this.queryByItem.getValue();
		self.cleanData(data);
		if (data == 1) {
			this.initTime();
			this.fSet.items.item(0).add(this.time);
			this.time.setValue(new Date());
		}
		if (data == 2) {
			this.initToMonth();
			this.fSet.items.item(0).add(this.toMonth);
			this.toMonth.setValue(new Date());
		}
		if (data == 3) {
			this.initTime();
			this.fSet.items.item(0).add(this.time);
			var bd = new Date();
			bd.setDate(bd.getDate() - 30);
			this.time.setValue(bd);
			this.fSet.items.item(0).add(this.endtime);
			this.endtime.setValue(new Date());
		}
		this.initTypeCombo();
		this.fSet.items.item(0).add(this.searchTypeCombo);
		this.fSet.items.item(0).add(this.getDimChooser());
		this.doLayout();
	},
	cleanData : function(data) {
		if (typeof (this.fSet.items.item(0).get(this.time)) != 'undefined') {
			this.time.getEl().dom.style.display = 'none';
		}
		if (typeof (this.fSet.items.item(0).get(this.endtime)) != 'undefined') {
			this.endtime.getEl().dom.style.display = 'none';
		}
		if (typeof (this.fSet.items.item(0).get(this.toMonth)) != 'undefined') {
			this.toMonth.getEl().dom.style.display = 'none';
		}
		if (typeof (this.fSet.items.item(0).get(this.searchTypeCombo)) != 'undefined') {
			this.searchTypeCombo.getEl().dom.style.display = 'none';
		}
		if (typeof (this.fSet.items.item(0).get(this.channel)) != 'undefined') {
			this.channel.getEl().dom.style.display = 'none';
		}
		if (typeof (this.fSet.items.item(0).get(this.city)) != 'undefined') {
			this.city.getEl().dom.style.display = 'none';
		}
		this.fSet.items.item(0).remove(this.multiCondition);
	},
	getDimChooser : function() {
		var self = this;
		var multiCondition = Dashboard.createDimChooser(150);
		this.multiCondition = multiCondition;
		return multiCondition;
	},
	countData : function(data) {
		var date = new Date();
		var time, etime, month;
		if (data == 1) {
			if (this.time.getRawValue() == null || this.time.getRawValue() == '') {
				Ext.Msg.alert('错误提示', '请选择日期');
				return;
			}
			if (this.time.getValue().getTime() > Date.parseDate(date.format("Y-m-d"), 'Y-m-d').getTime()) {
				Ext.Msg.alert('错误提示', '只能选择今天以前的时间');
				return;
			}
			time = this.time.getRawValue();
		}
		if (data == 2) {
			if (this.toMonth.getValue() == null || this.toMonth.getValue() == '') {
				Ext.Msg.alert('错误提示', '请选择统计的月');
				return;
			} else {
				if (this.toMonth.getValue().getTime() > Date.parseDate(date.format("Y-m"), 'Y-m').getTime()) {
					Ext.Msg.alert('错误提示', '只能统计本月及之前的信息');
					return;
				}
				month = this.toMonth.getRawValue();
			}
		}
		if (data == 3) {
			if (!this.time.getRawValue() || !this.endtime.getRawValue) {
				Ext.Msg.alert('错误提示', '请正确选择时间段');
				return;
			}
			var bt = this.time.getValue().getTime();
			var et = this.endtime.getValue().getTime();
			if (bt > Date.parseDate(date.format("Y-m-d"), 'Y-m-d').getTime() || et > Date.parseDate(date.format("Y-m-d"), 'Y-m-d').getTime()) {
				Ext.Msg.alert('错误提示', '只能选择今天以前的时间');
				return;
			}
			var t_off = et - bt;
			if (t_off < 0) {
				Ext.Msg.alert('错误提示', '开始时间应该小于结束时间');
				return;
			} else if (t_off / (1000 * 3600 * 24) > 30) {
				Ext.Msg.alert('错误提示', '时间段跨度不可超过30天');
				return;
			}
			time = this.time.getRawValue();
			etime = this.endtime.getRawValue();
		}
		var self = this;
		var _type = this.searchTypeCombo.getValue();
		var store = this.store;
		for ( var key in store.baseParams) {
			if (key && key.indexOf('data') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('time') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('month') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('type') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('endtime') != -1) {
				delete store.baseParams[key];
			}
			if (key && key.indexOf('multiCondition') != -1) {
				delete store.baseParams[key];
			}
		}
		var multiCondition = this.multiCondition.getValue();
		if (data)
			store.setBaseParam('data', data);
		if (time)
			store.setBaseParam('time', time);
		if (month)
			store.setBaseParam('month', month);
		if (_type)
			store.setBaseParam('type', _type);
		if (etime)
			store.setBaseParam('endtime', etime);
		if (multiCondition)
			store.setBaseParam('multiCondition', multiCondition);
		this.store.load({
			callback : function(recs) {
				self.getEl().unmask();
			},
			params : {
				start : 0,
				limit : this.getPageSize()
			}
		});
	}
});