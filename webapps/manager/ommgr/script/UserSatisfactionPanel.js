UserSatisfactionPanel = function() {
	var self = this;
	this.FeedbackRecord = Ext.data.Record.create([ 'id', 'userId', 'sessionId', 'grade', 'advice', 'reason', 'createTime', 'platform', 'location',
			'brand', 'custom1', 'custom2', 'custom3' ]);
	var gradeData = [ [ 1, '非常满意' ], [ 2, '满意' ], [ 3, '一般' ], [ 4, '不满意' ], [ 5, '非常不满意' ] ];
	var gradeMap = {};
	for ( var i = 0; i < gradeData.length; i++)
		gradeMap[gradeData[i][0]] = gradeData[i][1];
	var store = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({
			url : 'feedback!listFeedback.action'
		}),
		autoLoad : true,
		reader : new Ext.data.JsonReader({
			idProperty : 'id',
			root : 'data',
			fields : this.FeedbackRecord
		})
	});
	var pagingBar = new Ext.PagingToolbar({
		store : store,
		displayInfo : true,
		pageSize : 20,
		prependButtons : true,
		beforePageText : '第',
		afterPageText : '页，共{0}页',
		displayMsg : '第{0}到{1}条记录，共{2}条',
		emptyMsg : "没有记录"
	});

	var dimData = window._dimChooserInitData;
	var platformTags = [ [ '-1', '全部' ] ];
	if (dimData && dimData.length) {
		dimData = dimData[0];
		if (dimData && dimData.length) {
			var platformDim = null;
			for ( var i = 0; i < dimData.length; i++) {
				if (dimData[i].code == 'platform') {
					platformDim = dimData[i];
					break;
				}
			}
			if (platformDim && platformDim.tags && platformDim.tags.length) {
				for ( var i = 0; i < platformDim.tags.length; i++) {
					platformTags.push([ platformDim.tags[i].tag, platformDim.tags[i].name ]);
				}
			}
		}
	}

	// var platformChooser = new Ext.form.ComboBox({
	// triggerAction : 'all',
	// width : 126,
	// editable : false,
	// value : '-1',
	// mode : 'local',
	// store : new Ext.data.ArrayStore({
	// fields : [ 'tag', 'displayText' ],
	// data : platformTags
	// }),
	// valueField : 'tag',
	// displayField : 'displayText'
	// });
	// this.platformChooser = platformChooser;

	var dimChooser = Dashboard.createDimChooser(175);
	this.dimChooser = dimChooser;

	var _sm = new Ext.grid.RowSelectionModel();
	this.sm = _sm;
	var _columns = [ new Ext.grid.RowNumberer(), {
		width : 100,
		header : '用户名',
		dataIndex : 'userId'
	}, {
		header : '满意度',
		width : 80,
		dataIndex : 'grade',
		renderer : function(v) {
			return gradeMap[v];
		}
	}, {
		header : '原因',
		width : 160,
		dataIndex : 'reason'
	}, {
		header : '建议',
		id : 'adviceCol',
		dataIndex : 'advice'
	}, {
		header : '调查时间',
		width : 130,
		dataIndex : 'createTime'
	}, {
		header : Dashboard.dimensionNames.platform,
		dataIndex : 'platform',
		width : 75
	} ];
	if (Dashboard.dimensionNames.location)
		_columns.push({
			header : Dashboard.dimensionNames.location,
			dataIndex : 'location',
			width : 75,
			renderer : function(val) {
				if (!val || !val.trim())
					return 'N/A';
				return val;
			}
		});
	if (Dashboard.dimensionNames.brand)
		_columns.push({
			header : Dashboard.dimensionNames.brand,
			dataIndex : 'brand',
			width : 75,
			renderer : function(val) {
				if (!val || !val.trim())
					return 'N/A';
				return val;
			}
		});
	if (Dashboard.dimensionNames.custom1)
		_columns.push({
			header : Dashboard.dimensionNames.custom1,
			dataIndex : 'custom1',
			width : 75,
			renderer : function(val) {
				if (!val || !val.trim())
					return 'N/A';
				return val;
			}
		});
	if (Dashboard.dimensionNames.custom2)
		_columns.push({
			header : Dashboard.dimensionNames.custom2,
			dataIndex : 'custom2',
			width : 75,
			renderer : function(val) {
				if (!val || !val.trim())
					return 'N/A';
				return val;
			}
		});
	if (Dashboard.dimensionNames.custom3)
		_columns.push({
			header : Dashboard.dimensionNames.custom3,
			dataIndex : 'custom3',
			width : 75,
			renderer : function(val) {
				if (!val || !val.trim())
					return 'N/A';
				return val;
			}
		});

	var _exportItems = [ new Ext.form.ComboBox({
		ref : 'exportNum',
		valueField : 'num',
		displayField : 'num',
		mode : 'local',
		editable : true,
		selectOnFocus : false,
		triggerAction : 'all',
		validator : function(val) {
			val = val.trim();
			if (val) {
				if (/^[0-9]+$/.test(val)) {
					return true;
				}
				return '请正确输入数字';
			}
			return true;
		},
		width : 100,
		listWidth : 100,
		value : 8000,
		store : new Ext.data.ArrayStore({
			fields : [ 'num' ],
			data : [ [ 100 ], [ 500 ], [ 1000 ], [ 2000 ], [ 5000 ], [ 8000 ] ]
		})
	}), '-', {
		text : '用户名',
		checked : true,
		value : 1
	}, {
		text : '满意度',
		checked : true,
		value : 2
	}, {
		text : '原因',
		checked : true,
		value : 4
	}, {
		text : '建议',
		checked : true,
		value : 8
	}, {
		text : '调查时间',
		checked : true,
		value : 16
	}, {
		text : Dashboard.dimensionNames.platform,
		checked : true,
		value : 32
	}, {
		text : '详细会话',
		checked : false,
		value : 2048
	} ];
	if (Dashboard.dimensionNames.location)
		_exportItems.push({
			text : Dashboard.dimensionNames.location,
			checked : true,
			value : 64
		});
	if (Dashboard.dimensionNames.brand)
		_exportItems.push({
			text : Dashboard.dimensionNames.brand,
			checked : true,
			value : 128
		});
	if (Dashboard.dimensionNames.custom1)
		_exportItems.push({
			text : Dashboard.dimensionNames.custom1,
			checked : true,
			value : 256
		});
	if (Dashboard.dimensionNames.custom2)
		_exportItems.push({
			text : Dashboard.dimensionNames.custom2,
			checked : true,
			value : 512
		});
	if (Dashboard.dimensionNames.custom3)
		_exportItems.push({
			text : Dashboard.dimensionNames.custom3,
			checked : true,
			value : 1024
		});

	var config = {
		id : 'userSatisfactionPanel',
		store : store,
		region : 'center',
		columnLines : true,
		border : false,
		style : 'border-right: 1px solid ' + sys_bdcolor + ' border-bottom: 1px solid ' + sys_bdcolor,
		tbar : [ '满意度:', new Ext.form.ComboBox({
			id : 's_grade',
			triggerAction : 'all',
			width : 100,
			editable : false,
			value : '',
			mode : 'local',
			store : new Ext.data.ArrayStore({
				fields : [ 'grade', 'displayText' ],
				data : [ [ 0, '全部' ] ].concat(gradeData)
			}),
			valueField : 'grade',
			displayField : 'displayText'
		}), '建议:', {
			id : 's_advice',
			xtype : 'textfield',
			width : 100,
			emptyText : '输入关键字'
		}, '维度:', dimChooser, ' ', {
			text : '搜索',
			iconCls : 'icon-search',
			handler : function() {
				self.search();
			}
		}, ' ', {
			text : '全部',
			iconCls : 'icon-search',
			handler : function() {
				self.search(true);
			}
		}, ' ', {
			text : '导出',
			xtype : 'splitbutton',
			ref : 'exportBtn',
			menu : {
				defaults : {
					hideOnClick : false
				},
				items : _exportItems
			},
			iconCls : 'icon-ontology-export',
			text : '导出',
			hidden : !Dashboard.u().allow('om.stat.si.EXP'),
			handler : function() {
				self.exportData(true);
			}
		}, {
			iconCls : 'icon-delete',
			text : '删除',
			handler : function() {
				var rows = self.grid.getSelectionModel().getSelections();
				if (rows.length == 0) {
					Ext.Msg.alert('提示', '请至少选择一条记录！');
					return false;
				}
				Ext.Msg.confirm('提示', '确认删除这' + rows.length + '条记录？', function(btn) {
					if (btn == 'yes') {
						var ids = [];
						Ext.each(rows, function() {
							ids.push(this.id);
						});
						self.getEl().mask('正在删除...');
						Ext.Ajax.request({
							url : 'feedback!deleteFeedback.action',
							params : {
								ids : ids.join(',')
							},
							success : function(resp) {
								self.getEl().unmask();
								Dashboard.setAlert('删除成功');
								store.reload();
							},
							failure : function(resp) {
								self.getEl().unmask();
								Ext.Msg.alert('错误', resp.responseText);
							}
						});
					}
				});
			}
		}, {
			iconCls : 'icon-refresh',
			text : '刷新',
			handler : function() {
				store.reload();
			}
		} ],
		bbar : pagingBar,
		sm : _sm,
		loadMask : true,
		autoExpandColumn : 'adviceCol',
		colModel : new Ext.grid.ColumnModel({
			columns : _columns
		})
	};
	var _chartStore = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({
			url : 'feedback!stat.action'
		}),
		reader : new Ext.data.JsonReader({
			idProperty : 'id',
			root : 'data',
			fields : Ext.data.Record.create([ 'grade', 'count' ])
		}),
		autoLoad : true
	});
	this.charStore = _chartStore;
	var _chartPanel = new Ext.Panel({
		style : 'border-top: 1px solid ' + sys_bdcolor,
		border : false,
		height : 250,
		header : false,
		split : true,
		collapsible : true,
		collapseMode : 'mini',
		region : 'south',
		items : {
			xtype : 'columnchart',
			store : _chartStore,
			yField : 'count',
			url : '../ext/resources/charts.swf',
			xField : 'grade',
			xAxis : new Ext.chart.CategoryAxis({
				title : '满意度类别'
			}),
			yAxis : new Ext.chart.NumericAxis({
				title : '票数'
			}),
			extraStyle : {
				xAxis : {
					labelRotation : 0
				}
			}
		}
	});
	var logSessionWindow = new DetailWindow({
		title : '会话详细信息'
	});
	this.logSessionWindow = logSessionWindow;
	var _grid = new Ext.grid.GridPanel(config);
	_grid.tbarEx = new Ext.Toolbar({
		enableOverflow : true,
		items : [ '用户名:', {
			id : 's_user',
			xtype : 'textfield',
			width : 100,
			emptyText : '输入用户名'
		}, '原因:', {
			id : 's_reason',
			xtype : 'textfield',
			width : 100,
			emptyText : '输入关键字'
		}, '调查时间:从', {
			id : 's_bdate',
			xtype : 'datefield',
			width : 90,
			emptyText : '选择时间',
			format : 'Y-m-d'
		}, '到', {
			id : 's_edate',
			xtype : 'datefield',
			width : 90,
			emptyText : '选择时间',
			format : 'Y-m-d'
		} ]
	});
	this.grid = _grid;
	var cfg = {
		layout : 'border',
		border : false,
		items : [ _grid, _chartPanel ]
	}
	UserSatisfactionPanel.superclass.constructor.call(this, cfg);
	_grid.addListener('render', function() {
		this.tbarEx.render(this.tbar);
	}, _grid);
	_grid.addListener('rowdblclick', function() {
		var sessionid = this.sm.getSelected().data.sessionId;
		var month = this.sm.getSelected().data.createTime;
		if (month) {
			month = month.substring(0, 7).replace('-', '');
			self.queryLogSession(sessionid, month);
		}
	}, this);
}

Ext.extend(UserSatisfactionPanel, Ext.Panel, {
	search : function(showAll) {
		var _userField = this.grid.tbarEx.findById('s_user');
		var _adviceField = this.grid.getTopToolbar().findById('s_advice');
		var _reasonField = this.grid.tbarEx.findById('s_reason');
		var _gradeCombo = this.grid.getTopToolbar().findById('s_grade');
		var _bdateField = this.grid.tbarEx.findById('s_bdate');
		var _edateField = this.grid.tbarEx.findById('s_edate');
		if (showAll) {
			_userField.setValue('');
			_adviceField.setValue('');
			_reasonField.setValue('');
			_gradeCombo.setValue(0);
			_bdateField.setValue('');
			_edateField.setValue('');
			this.dimChooser.setValue({});
		}
		var store = this.grid.getStore();
		for ( var key in store.baseParams) {
			if (key && key.indexOf('feedbackParam.') != -1) {
				delete store.baseParams[key];
				delete this.charStore.baseParams[key];
			}
		}
		var _user = _userField.getValue().trim();
		var _advice = _adviceField.getValue();
		var _reason = _reasonField.getValue();
		var _grade = _gradeCombo.getValue();
		var _bdate = _bdateField.getValue();
		var _edate = _edateField.getValue();
		// var _platform = this.platformChooser.getValue();
		// _platform = _platform == '-1' ? null : _platform;
		var dims = this.dimChooser.getValue();
		if (_user) {
			store.setBaseParam('feedbackParam.userId', _user);
			this.charStore.setBaseParam('feedbackParam.userId', _user);
		}
		if (_advice) {
			store.setBaseParam('feedbackParam.advice', _advice);
			this.charStore.setBaseParam('feedbackParam.advice', _advice);
		}
		if (_reason) {
			store.setBaseParam('feedbackParam.reason', _reason);
			this.charStore.setBaseParam('feedbackParam.reason', _reason);
		}
		if (_grade) {
			store.setBaseParam('feedbackParam.grade', _grade);
			this.charStore.setBaseParam('feedbackParam.grade', _grade);
		}
		if (_bdate) {
			store.setBaseParam('feedbackParam.beginTime', _bdate);
			this.charStore.setBaseParam('feedbackParam.beginTime', _bdate);
		}
		if (_edate) {
			store.setBaseParam('feedbackParam.endTime', _edate);
			this.charStore.setBaseParam('feedbackParam.endTime', _edate);
		}
		if (dims) {
			store.setBaseParam('feedbackParam.dims', dims);
			this.charStore.setBaseParam('feedbackParam.dims', dims);
		}
		store.load();
		this.charStore.load();
	},
	exportData : function(restart) {
		var self = this;
		var exportBtn = this.grid.getTopToolbar().exportBtn;
		var _exportItems = exportBtn.menu.items.items;
		var expNum = exportBtn.menu.exportNum.getValue();
		if (!/^[0-9]+$/.test(expNum)) {
			Ext.Msg.alert('提示', '您填写的导出数量不合法');
			return false;
		}
		var exportcols = 0;
		Ext.each(_exportItems, function(item) {
			if (item.checked)
				exportcols = exportcols | item.value;
		});
		if (exportcols == 0) {
			Ext.Msg.alert('提示', '请至少选择一列进行导出');
			return false;
		}
		var _user = this.grid.tbarEx.findById('s_user').getValue().trim();
		var _advice = this.grid.getTopToolbar().findById('s_advice').getValue();
		var _reason = this.grid.tbarEx.findById('s_reason').getValue();
		var _grade = this.grid.getTopToolbar().findById('s_grade').getValue();
		var _bdate = this.grid.tbarEx.findById('s_bdate').getValue();
		var _edate = this.grid.tbarEx.findById('s_edate').getValue();
		var dims = this.dimChooser.getValue();
		// _platform = _platform == '-1' ? null : _platform;
		var params = {
			'user' : _user,
			'advice' : _advice,
			'reason' : _reason,
			'bdate' : _bdate,
			'edate' : _edate,
			'dims' : dims
		};
		if (_grade)
			params.grade = _grade;
		params['cols'] = exportcols;
		params['num'] = expNum;
		if (restart)
			params['restart'] = 'true';
		Ext.Ajax.request({
			url : 'feedback!exportData.action',
			params : Ext.urlEncode(params),
			success : function(response) {
				var result = Ext.util.JSON.decode(response.responseText);
				if (!result.data || result.data.totalCount == 0) {
					Ext.Msg.alert('提示', '无数据可以导出');
					return false;
				}
				var timer = new ProgressTimer({
					initData : result.data,
					progressId : 'userSatisfactionStatus',
					boxConfig : {
						title : '正在导出满意度调查...',
						buttons : {
							yes : '继续导出',
							no : '从头开始',
							cancel : '关闭'
						},
						fn : function(btn) {
							if (btn == 'yes')
								self.exportData();
							else if (btn == 'no')
								self.exportData(true);
						}
					},
					finish : function() {
						if (!this.downloadIFrame) {
							this.downloadIFrame = this.getEl().createChild({
								tag : 'iframe',
								style : 'display:none;'
							});
						}
						this.downloadIFrame.dom.src = 'feedback!exportExcelData.action?&_t=' + new Date().getTime();
					},
					scope : this
				});
				timer.start();
			},
			failure : function(response) {
				if (response.responseText)
					Ext.Msg.alert('出错了', response.responseText.replace(/\r\n/ig, '<br>'));
			},
			scope : this
		});
	},
	queryLogSession : function(id, month) {
		this.logSessionWindow.show();
		this.logSessionWindow.getPanel().load({
			url : "log-session!queryLogSessionDetail.action",
			params : {
				"id" : id,
				"month" : month
			},
			waitMsg : '正在加载数据...',
			success : function(response) {
			},
			failure : function(response) {
				Ext.Msg.alert("提示", "出错或者超时!");
			}
		});
	}

});
