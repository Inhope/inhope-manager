Dashboard = {
	u : function() {
		return this._usr ? this._usr : this._usr = new LoginUser('om');
	},
	setAlert : function(msg, status, delay) {
		if (!status)
			status = 'ok';
		if (!delay)
			delay = 3;
		if (!this.msgCt) {
			this.msgCt = Ext.DomHelper.insertFirst(document.body, {
				id : 'msg-div'
			}, true);
			this.msgCt.setStyle('position', 'absolute');
			this.msgCt.setStyle('z-index', 9999);
			this.msgCt.setWidth(300);
		}
		this.msgCt.alignTo(document, 't-t');
		var html = [ '<div class="app-msg">', '<div class="x-box-tl"><div class="x-box-tr"><div class="x-box-tc"></div></div></div>',
				'<div class="x-box-ml"><div class="x-box-mr"><div class="x-box-mc"><h3 class="x-icon-text icon-status-' + status + '">', msg,
				'</h3>', '</div></div></div>', '<div class="x-box-bl"><div class="x-box-br"><div class="x-box-bc"></div></div></div>', '</div>' ]
				.join('');
		Ext.DomHelper.append(this.msgCt, {
			'html' : html
		}, true).slideIn('t').pause(delay).ghost("t", {
			remove : true
		});
	},
	navRegistry : [],
	registerNav : function(navClass, sort) {
		this.navRegistry.push({
			'createNav' : function(id) {
				return new navClass({
					'id' : id
				});
			},
			'sort' : sort
		});
	},
	getDaysOfMonth : function(date) {
		if (!date)
			date = new Date();
		date.setMonth(date.getMonth() + 1);
		date.setDate(0);
		return date.getDate();
	},
	createXHR : function() {
		var _xhr = false;
		try {
			_xhr = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				_xhr = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e2) {
				_xhr = false;
			}
		}
		if (!_xhr && window.XMLHttpRequest)
			_xhr = new XMLHttpRequest();
		return _xhr;
	},
	createDimChooser : function(_width, _excludeDims) {
		return new DimensionCheckBoxField({
			width : _width,
			valueTransformer : function(value) {
				if (value == 'ALLDIM')
					return '';
				if (typeof value == 'object')
					return value.join(',');
				return value;
			}
		});
		// return Ext.create({
		// width : _width,
		// name : 'dimension',
		// xtype : 'dimensionbox',
		// clearBtn : true,
		// emptyText : '请选择维度',
		// menuConfig : {
		// labelAlign : 'top',
		// excludeDims : _excludeDims
		// },
		// valueTransformer : function(value) {
		// if (value == 'ALLDIM')
		// return '';
		// var tags = this.tags;
		// var groupTags = {};
		// if (tags)
		// Ext.each(tags, function(t) {
		// var arr = groupTags[t.dimCode];
		// if (!arr) {
		// arr = [];
		// groupTags[t.dimCode] = arr;
		// }
		// arr.push(t.tagId);
		// });
		// return Ext.encode(groupTags);
		// }
		// });
	},
	navToService : function(servId) {
		var node = Dashboard.navPanel.getRootNode().findChild('module', 'serviceDetail', true);
		node.external = true;
		Dashboard.navPanel.fireEvent('click', node);
		var tab = Dashboard.mainPanel.get('serviceDetailTab');
		tab.items.items[0].externalLoad(servId);
		node.external = false;
	},
	nav : function() {
		var module = arguments[0];
		var node = Dashboard.navPanel.getRootNode().findChild('module', module, true);
		node.external = true;
		Dashboard.navPanel.fireEvent('click', node);
		var tab = Dashboard.mainPanel.get(module + 'Tab');
		Dashboard.mainPanel.activate(tab);
		tab.items.items[0].externalLoad.apply(tab.items.items[0], Array.prototype.slice.call(arguments, 1));
		node.external = false;
	},
	getColumnTitle : function(origin) {
		if (Dashboard.miscConfig.titleConf) {
			var replacement = Dashboard.miscConfig.titleConf[origin];
			if (replacement)
				origin = replacement;
		}
		return origin;
	}
};

getTopWindow().OM_Dashboard = getTopWindow().ommgr_Dashboard = Dashboard;

Ext.onReady(function() {
	Ext.QuickTips.init();

	var xhr = Dashboard.createXHR();
	xhr.open('GET', 'special-check.action', false);
	xhr.send();
	var instrHidden = parseInt(xhr.responseText) == 0;

	xhr.open('GET', 'robot-log!listDimensionNames.action', false);
	xhr.send();
	Dashboard.dimensionNames = Ext.decode(xhr.responseText);

	xhr.open('GET', 'robot-log!miscConfig.action', false);
	xhr.send();
	Dashboard.miscConfig = Ext.decode(xhr.responseText);
	Dashboard.miscConfig = Dashboard.miscConfig || {};

	xhr.open('GET', 'ontology-dimension!listAllowed.action', false);
	xhr.send();
	window._dimChooserInitData = Ext.decode(xhr.responseText).data;

	var __enableAudit = http_get('property!get.action?key=kbmgr.enableAudit');

	window.fakeClipboard = {
		_el : Ext.getBody().createChild({
			id : 'fakeClipboard',
			tag : 'textarea',
			style : 'position:absolute;width:0;height:0;left:-10px;'
		}),
		_onpaste : Ext.emptyFn,
		_init : function() {
			var el = this._el;
			this._inited = true;
			el.on("keydown", function(e) {
				var el = this._el;
				if (e.getKey() == 17)
					el.ctrl = true;
				if (!el.ctrl)
					e.stopEvent();
				else if (el.ctrl && e.getKey() == 86) {
					el.ready = true;
				}
			}, this);
			el.on("keyup", function(e) {
				var el = this._el;
				if (el.ready && e.getKey() == 86) {
					el.ready = false;
					this._onpaste(el.dom.value);
				}
				if (e.getKey() == 17)
					el.ctrl = false;
			}, this);
		},
		listen : function(fn) {
			var el = this._el;
			if (!this._inited)
				this._init();
			this._onpaste = fn;
			el.dom.value = '';
			el.dom.focus();
		},
		decodeExcel : function(value) {
			var s = value;
			var data = [], row = [], cell = '', b = false;
			for ( var i = 0; i < s.length; i++) {
				var c = s.charAt(i);
				if (c == '"') {
					if (b) {
						if (i + 1 < s.length && s.charAt(i + 1) == '"') {
							cell += '"';
							i++;
							continue;
						} else {
							b = false;
						}
					} else if (cell.length > 0) {
						cell += c;
					} else {
						b = true;
					}
				} else if (!b && c == '\t') {
					if (cell.length > 0)
						row.push(cell);
					else
						row.push(null);
					cell = '';
				} else if (!b && c == '\r') {
					continue;
				} else if (!b && c == '\n') {
					if (cell.length > 0)
						row.push(cell);
					cell = '';
					data.push(row);
					row = [];
				} else {
					cell += c;
				}
			}
			if (cell.length > 0)
				row.push(cell);
			if (row.length > 0)
				data.push(row);
			return data;
		}
	};

	var mainPanel = new Ext.TabPanel({
		region : 'center',
		activeTab : 0,
		border : false,
		style : 'border-left: 1px solid ' + sys_bdcolor,
		resizeTabs : true,
		tabWidth : 150,
		minTabWidth : 120,
		enableTabScroll : true,
		layoutOnTabChange : true
	});
	Dashboard.mainPanel = mainPanel;

	var navs = Dashboard.u().filterAllowed([ {
		id : '999',
		authName : 'om.log.portal',
		name : '日志统计概览',
		icon : 'icon-portal',
		panelClass : LogPortal
	}, {
		id : '1',
		authName : 'om.log',
		name : '日志管理',
		icon : 'icon-expand'
	}, {
		id : '12',
		authName : 'om.log.ask',
		name : '自动问答明细',
		module : 'askDetail',
		parent : '1',
		icon : 'icon-toolbox',
		panelClass : LogAskDetailPanel
	}, {
		id : '11',
		authName : 'om.log.sess',
		name : '会话日志明细',
		module : 'sessionDetail',
		parent : '1',
		icon : 'icon-toolbox',
		panelClass : LogSessionPanel
	}, {
		id : '18',
		authName : 'om.log.svc',
		name : '服务日志明细',
		module : 'serviceDetail',
		parent : '1',
		icon : 'icon-toolbox',
		panelClass : LogServiceDetailPanel
	}, {
		id : '1x',
		authName : 'om.log.instr',
		name : '指令转发明细',
		module : 'webInstr',
		parent : '1',
		icon : 'icon-toolbox',
		panelClass : LogInstructionPanel,
		hidden : instrHidden
	}, {
		id : '13',
		authName : 'om.log.acs',
		name : '人工客服明细',
		module : 'webLabourDetail',
		parent : '1',
		icon : 'icon-toolbox',
		panelClass : LogAcsDetailPanel
	}, {
		id : '14',
		name : '操作日志明细',
		authName : 'om.log.op',
		module : 'sysSession',
		parent : '1',
		icon : 'icon-toolbox',
		panelClass : SYSLogDetailPanel
	}, {
		id : '15',
		authName : 'om.log.lw',
		name : '用户留言明细',
		icon : 'icon-toolbox',
		module : 'leaveMessage',
		parent : '1',
		panelClass : LeaveMessagePanel
	},
	// {
	// id : '16',
	// authName : 'om.log.nc',
	// name : '征名活动明细',
	// module : 'NickCollectPanel',
	// icon : 'icon-toolbox',
	// parent : '1',
	// panelClass : NickCollectPanel
	// },
	{
		id : '17',
		authName : 'om.log.fv',
		name : '未解决问答明细',
		module : 'LogFaqVotePanel',
		icon : 'icon-toolbox',
		parent : '1',
		panelClass : LogFaqVotePanel
	}, {
		id : __enableAudit ? '19' : '',
		authName : 'om.log.audit',
		name : '审核日志明细',
		module : 'LogAuditPanel',
		icon : 'icon-toolbox',
		parent : '1',
		panelClass : LogAuditPanel
	}, {
		id : '3',
		authName : 'om.stat',
		name : '统计分析管理',
		icon : 'icon-expand'
	}, {
		id : '30',
		authName : 'om.stat.tv',
		name : '访问数据统计(时间)',
		icon : 'icon-toolbox',
		module : 'accData',
		parent : '3',
		panelClass : AccDataPanel
	}, {
		id : '31',
		authName : 'om.stat.dv',
		name : '访问数据统计(维度)',
		icon : 'icon-toolbox',
		module : 'accDimData',
		parent : '3',
		panelClass : AccDimDataPanel
	}, {
		id : '37',
		authName : 'om.stat.svc',
		name : '服务日志统计',
		icon : 'icon-toolbox',
		module : 'servData',
		parent : '3',
		panelClass : StatServiceLogPanel
	}, {
		id : '32',
		authName : 'om.stat.ki',
		name : '知识点调查统计',
		icon : 'icon-toolbox',
		module : 'cusApp',
		parent : '3',
		panelClass : CusAppPanel
	}, {
		id : '33',
		authName : 'om.stat.si',
		name : '满意度调查统计',
		icon : 'icon-toolbox',
		module : 'userSatisfaction',
		parent : '3',
		panelClass : UserSatisfactionPanel
	}, {
		id : '34',
		authName : 'om.stat.kr',
		name : '知识点排名统计',
		icon : 'icon-toolbox',
		module : 'knoOrder',
		parent : '3',
		panelClass : KnoOrdPanel
	}, {
		id : '35',
		authName : 'om.stat.uq',
		name : '未匹配问题统计',
		icon : 'icon-toolbox',
		module : 'unresolvedQues',
		parent : '3',
		panelClass : UnresolvedQuesPanel
	}, {
		id : '36',
		authName : 'om.stat.ir',
		name : '指令转发统计',
		icon : 'icon-toolbox',
		module : 'instrRank',
		parent : '3',
		panelClass : InstructionRankPanel,
		hidden : instrHidden
	},
	// {
	// id : '4',
	// authName : 'om.define',
	// name : '自定义统计',
	// module : 'report',
	// icon : 'icon-expand'
	// },
	// {
	// id : '41',
	// authName : 'om.define.fir',
	// name : '自定义设置',
	// module : 'report-fir',
	// parent : '4',
	// panelClass : DefineReport,
	// icon : 'icon-toolbox'
	// },
	// {
	// id : 'report_define',
	// authName : 'om.define.sec',
	// url : 'report!children.action',
	// name : '生成报表统计',
	// module : 'report-sec',
	// parent : '4',
	// icon : 'icon-expand'
	// },
	{
		id : '_extstat_node',
		url : 'external-stat!list.action',
		authName : 'om.custom',
		name : '其他运维管理',
		icon : 'icon-expand'
	} ]);

	if (isVersionStandard())
		navs.splice(navs.length - 4, 3);
	var rootNode = new Ext.tree.TreeNode({
		id : '0',
		text : 'root'
	});
	var _nodeMap = {};
	for ( var i = 0; i < navs.length; i++) {
		var _item = navs[i];
		if (_item.hidden || !_item.id)
			continue;
		var _node = null;
		if (_item.url) {
			if (_item.panelClass) {
				_node = new Ext.tree.AsyncTreeNode({
					id : _item.id,
					text : _item.name,
					iconCls : _item.icon,
					module : _item.module,
					panelClass : _item.panelClass,
					loader : new Ext.tree.TreeLoader({
						url : _item.url
					})
				});
			} else {
				_node = new Ext.tree.AsyncTreeNode({
					id : _item.id,
					text : _item.name,
					iconCls : _item.icon,
					loader : new Ext.tree.TreeLoader({
						url : _item.url
					})
				});
			}
		} else {
			_node = new Ext.tree.TreeNode({
				id : _item.id,
				text : _item.name,
				iconCls : _item.icon,
				module : _item.module,
				panelClass : _item.panelClass,
				initConfig : _item.initConfig
			});
		}
		_nodeMap[_item.id] = _node;
		_node.authName = _item.authName
		if (!_item.parent)
			rootNode.appendChild(_node);
		else
			_nodeMap[_item.parent].appendChild(_node);
	}

	// if (_nodeMap && _nodeMap['_extstat_node']) {
	// _nodeMap['_extstat_node'].on('expand', function() {
	// if (this.findChild('id', '40') != null)
	// return false;
	// if (!Dashboard.miscConfig.imgtxtStatDisabled) {
	// var _item = {
	// id : '40',
	// authName : 'om.weixin.itr',
	// name : '微信图文点击统计',
	// icon : 'icon-toolbox',
	// module : 'imgTxtRank',
	// parent : '_extstat_node',
	// initConfig : {
	// platform : 'weixin'
	// },
	// panelClass : ImgTxtRankPanel
	// };
	// var _node = new Ext.tree.TreeNode({
	// id : _item.id,
	// text : _item.name,
	// iconCls : _item.icon,
	// module : _item.module,
	// panelClass : _item.panelClass,
	// initConfig : _item.initConfig
	// });
	// this.appendChild(_node);
	// }
	// });
	// }
	var navPanel = new Ext.tree.TreePanel({
		region : 'west',
		width : 200,
		minSize : 175,
		maxSize : 400,
		split : true,
		collapsible : true,
		collapseMode : 'mini',
		border : false,
		style : 'border-right: 1px solid ' + sys_bdcolor,
		title : '控制台',
		autoScroll : true,
		rootVisible : false,
		lines : false,
		root : rootNode
	});
	Dashboard.navPanel = navPanel;
	navPanel.showTab = function(panelClass, tabId, title, iconCls, closable, cfg) {
		if (!panelClass)
			return false;
		var tab = mainPanel.get(tabId);
		if (!tab) {
			var tabPanel = new panelClass(cfg);
			tabPanel.navPanel = this;
			tab = mainPanel.add({
				id : tabId,
				layout : 'fit',
				title : title,
				items : tabPanel,
				iconCls : iconCls,
				closable : closable
			});
			tab.show();
		} else {
			mainPanel.activate(tabId);
		}
		return tab.get(0);
	};
	//
	var clicked = false;
	navPanel.on('expandnode', function(node) {
		if (!clicked && parent.initModule) {
			if (node.authName == parent.initModule) {
				this.fireEvent('click', node);
				clicked = true
				return false
			}
		} else if (!clicked && node.id == '999') {
			this.fireEvent('click', node);
			clicked = true;
		} else if (!clicked && node.id == '1') {
			this.fireEvent('click', node.firstChild);
			if (getTopWindow().om_serv_cb)
				getTopWindow().om_serv_cb.call();
			if (getTopWindow().ommgr_initcb) {
				getTopWindow().ommgr_initcb.call();
				getTopWindow().ommgr_initcb = null;
			}
			clicked = true;
		}
	}, navPanel);
	navPanel.on('afterrender', function() {
		this.getRootNode().expand(true, true);
	}, navPanel);
	navPanel.on('click', function(n) {
		if (!n)
			return false;
		var module = n.attributes.module;
		var panelClass = n.attributes.panelClass;
		var cfg = {};
		if (n.attributes.attachment && n.attributes.attachment.indexOf('external:') == 0) {
			var url = n.attributes.attachment.replace('external:', '');
			cfg.url = url;
			module = n.id;
			panelClass = ExternalStatPanel;
		}
		var initConfig = n.attributes.initConfig;
		if (initConfig)
			Ext.apply(cfg, initConfig);
		var p = this.showTab(panelClass, module + 'Tab', n.text, n.attributes.iconCls, true, cfg);
		if (module == 'askDetail')
			LogNavPanel.logAskDetailPanel = p;
		if (p && p.loadData && !n.external)
			p.loadData();
	}, navPanel);

	var _customForm = new Ext.form.FormPanel({
		frame : false,
		border : false,
		labelWidth : 70,
		autoHeight : true,
		waitMsgTarget : true,
		bodyStyle : 'padding : 3px 10px; background-color:' + sys_bgcolor,
		items : [ {
			xtype : 'textfield',
			fieldLabel : '名称',
			allowBlank : false,
			name : 'name',
			blankText : '请输入名称',
			anchor : '98%'
		}, {
			xtype : 'textfield',
			fieldLabel : '地址',
			allowBlank : false,
			name : 'url',
			blankText : '请输入地址',
			anchor : '98%',
			validator : function(v) {
				v = v.trim();
				if (v && (v.indexOf('http://') == 0 || v.indexOf('https://') == 0))
					return true;
				return '请正确输入地址';
			}
		} ],
		tbar : [ {
			text : '保存',
			iconCls : 'icon-save',
			handler : function() {
				this.disable();
				var btn = this;
				var f = _customForm.getForm();
				if (!f.isValid()) {
					Ext.Msg.alert('提示', '您尚未正确完整的输入表单内容，请检查！');
					btn.enable();
					return false;
				}
				var vals = f.getFieldValues();
				Ext.Ajax.request({
					url : 'external-stat!save.action',
					params : {
						'name' : vals.name,
						'url' : vals.url
					},
					success : function(resp) {
						var oresp = Ext.decode(resp.responseText);
						if (oresp.success) {
							Dashboard.setAlert('保存成功。');
							navPanel.getRootNode().findChild('id', '_extstat_node').reload();
						} else {
							Dashboard.setAlert('保存失败，已存在同名的配置。');
						}
						_customWin.hide();
						btn.enable();
					},
					failure : function(resp) {
						Ext.Msg.alert('错误', resp.responseText);
						btn.enable();
						_customWin.hide();
					}
				});
			}
		} ]
	});
	var _customWin = new Ext.Window({
		width : 425,
		defaults : {
			border : false
		},
		modal : true,
		plain : true,
		shim : true,
		closable : true,
		closeAction : 'hide',
		collapsible : true,
		resizable : false,
		draggable : true,
		animCollapse : true,
		constrainHeader : true,
		listeners : {
			'beforehide' : function(p) {
				p.formPanel.getForm().reset();
			}
		},
		title : '新增管理',
		shadow : false,
		items : [ _customForm ]
	});

	_customWin.formPanel = _customForm;
	var menu = new Ext.menu.Menu({
		items : [ {
			ref : 'refreshBtn',
			text : '刷新',
			iconCls : 'icon-refresh',
			width : 50,
			handler : function() {
				menu.currentNode.reload();
			}
		}, '-', {
			ref : 'newBtn',
			text : '新建',
			iconCls : 'icon-add',
			width : 50,
			handler : function() {
				_customWin.show();
			}
		}, !Dashboard.u().allow('ALL') ? '' : {
			ref : 'designBtn',
			text : '报表设计器',
			iconCls : 'icon-toolbox',
			width : 50,
			handler : function() {
				Dashboard.navPanel.showTab(DefineReport, '41', '报表设计器', 'icon-toolbox', true, {});
			}
		}, {
			ref : 'delBtn',
			text : '删除',
			iconCls : 'icon-delete',
			width : 50,
			handler : function() {
				Ext.Msg.confirm('提示', '确认删除' + menu.currentNode.text + '？', function(btn) {
					if (btn == 'yes') {
						Ext.Ajax.request({
							url : 'external-stat!delete.action',
							params : {
								name : menu.currentNode.text,
								menuId : menu.currentNode.id,
								cls : menu.currentNode.attributes.cls
							},
							success : function(resp) {
								Dashboard.setAlert('删除成功！');
								menu.currentNode.parentNode.reload();
							},
							failure : function(resp) {
								Ext.Msg.alert('错误', resp.responseText);
							}
						});
					}
				});
			}
		} ]
	});
	navPanel.on('contextmenu', function(node, event) {
		if (node.id == 'report_define' || node.parentNode.id == 'report_define') {
			r_menu.currentNode = node;
			node.select();
			if (node.id == 'report_define') {
				r_menu.refreshBtn.setDisabled(false);
				r_menu.delBtn.setDisabled(true);
			} else {
				r_menu.refreshBtn.setDisabled(true);
				r_menu.delBtn.setDisabled(false);
			}
			r_menu.showAt(event.getXY());
		}
		if (node.id != '_extstat_node' && node.parentNode.id != '_extstat_node')
			return false;
		menu.currentNode = node;
		node.select();
		if (node.id == '_extstat_node') {
			menu.refreshBtn.setDisabled(false);
			menu.newBtn.setDisabled(false);
			if (Dashboard.u().allow('ALL'))
				menu.designBtn.setDisabled(false);
			menu.delBtn.setDisabled(true);
		} else {
			menu.refreshBtn.setDisabled(true);
			menu.newBtn.setDisabled(true);
			if (Dashboard.u().allow('ALL'))
				menu.designBtn.setDisabled(true);
			menu.delBtn.setDisabled(false);
		}
		menu.showAt(event.getXY());
	});

	var viewport = new Ext.Viewport({
		layout : 'border',
		items : [ navPanel, mainPanel ]
	});

});
