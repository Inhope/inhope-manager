DataNavTree = function() {
	var self = this;
	this.data = {};

	var loader = new Ext.tree.TreeLoader({
				dataUrl : 'dict-name!list.action'
			});
	var initRoot = new Ext.tree.AsyncTreeNode({
				id : 'root',
				text : '词典类型',
				draggable : false,
				expanded : true,
				iconCls : 'icon-ontology-root',
				loader : loader
			});
	var cfg = {
		title : '字典管理',
		width : 200,
		minSize : 175,
		maxSize : 400,
		xtype : 'treepanel',
		collapsible : true,
		rootVisible : true,
		animate : true,
		lines : true,
		border : false,
		autoScroll : true,
		root : initRoot,
		tbar : new Ext.Toolbar({
					enableOverflow : true,
					height : '20',
					items : Dashboard.u().filterAllowed([{
								blankText : "请输入分类名称",
								xtype : 'textfield',
								width : 120
							}, {
								text : '查找',
								authName : 'om.data.find',
								iconCls : 'icon-search',
								width : 50,
								handler : function() {
									self.searchNode();
								}
							}, '-', {
								text : '新建',
								authName : 'om.data.new',
								iconCls : 'icon-wordclass-root-add',
								width : 50,
								handler : function() {
									self.addNode(self.getRootNode());
								}
							}, '-', {
								text : '刷新',
								iconCls : 'icon-refresh',
								width : 50,
								handler : function() {
									self.refresh(self.getRootNode());
								}
							}])

				}),
		listeners : {
			render : function(tp) {
			},
			loadexception : function(loader, node, response) {// 解决在IE下偶尔有不能加载的bug
				node.loaded = false;
				node.reload.defer(10, node);// 不停的加载，直到true
			}
		}
	}
	DataNavTree.superclass.constructor.call(this, cfg);
	var dictMenu = new Ext.menu.Menu({
		items : Dashboard.u().filterAllowed([{
				text : '刷新',
				iconCls : 'icon-refresh',
				width : 50,
				handler : function() {
					self.refreshNode();
				}
			}, {
				text : '新建子分类',
				authName : 'om.data.0.new',
				id : 'addType',
				iconCls : 'icon-ontology-cate-add',
				width : 50,
				handler : function() {
					self.addNode(dictMenu.currentNode);
				}
			}, {
				text : '修改分类',
				authName : 'om.data.0.edit',
				iconCls : 'icon-ontology-cate-edit',
				width : 50,
				handler : function() {
					self.modifyNode(dictMenu.currentNode);
				}
			}, {
				text : '删除分类',
				authName : 'om.data.0.del',
				iconCls : 'icon-ontology-cate-del',
				width : 50,
				handler : function() {
					self.deleteNode(dictMenu.currentNode);
				}
			}])
	});
	this._deleteObject = function() {
		var selNode = this.getSelectedNode();
		if (!selNode) {
			Ext.Msg.alert('提示', '请先选择需要删除的节点');
			return;
		}
		Ext.Msg.confirm('确定框', '确定删除记录' + selNode.text + '吗?',
				function(result) {
					if (result == 'yes') {
						Ext.Ajax.request({
									url : '',// 待定
									params : {
										"objectId" : selNode.id
									},
									success : function(response) {
										var result = Ext.util.JSON
												.decode(response.responseText);
										Dashboard.setAlert(result.message);
										selNode.parentNode.removeChild(selNode,
												true);
									},
									failure : function(response) {
										Ext.Msg.alert("错误",
												response.responseText);
									}
								});
					}
				});
	}

	this.dictMenu = dictMenu;

	this.on('contextmenu', function(node, event) {
				event.preventDefault();
				node.select();
				if (node.id != 'root') {
					Ext.getCmp('addType').setDisabled(true);
				} else {
					Ext.getCmp('addType').setDisabled(false);
				}
				dictMenu.currentNode = node;
				dictMenu.showAt(event.getXY());

			});

	var _formPanel = new Ext.form.FormPanel({
				frame : false,
				border : false,
				labelWidth : 100,
				// autoHeight : true,
				height : 120,
				waitMsgTarget : true,
				bodyStyle : 'padding : 5px 10px',
				defaults : {
					blankText : '',
					invalidText : '',
					anchor : '92%',
					xtype : 'textfield'
				},
				items : [new Ext.form.Hidden({
									name : 'dictid'
								}), {
							fieldLabel : '英文名称',
							name : 'dictname',
							allowBlank : false
						}, {
							fieldLabel : '类型名称',
							name : 'dicthint',
							allowBlank : true
						}],
				buttons : [{
							text : '保存',
							handler : function() {
								self.saveNode(this, self.getRootNode());
							}
						}, {
							text : '关闭',
							handler : function() {
								self.endEdit();
							}
						}]
			});
	var _formWin = new Ext.Window({
				width : 380,
				title : '字典类型管理',
				defaults : {
					border : false
				},
				modal : true,
				plain : true,
				shim : true,
				closable : false,
				closeAction : 'hide',
				collapsible : true,
				resizable : false,
				draggable : true,
				animCollapse : true,
				constrainHeader : true,
				items : [_formPanel]
			});
	_formWin.formPanal = _formPanel;
	this.formWin = _formWin;

	this.on('click', function(n) {
				if (!n)
					return false;
				var objectPanel = self.showTab(dict.MainPanel, (n.tabId
								? n.tabId
								: 'dict-tab-' + n.id), n.text,
						'icon-wordclass-root', true);
				objectPanel.setDictType(n.id);
				objectPanel.loadDatas();
			})

}

Ext.extend(DataNavTree, Ext.tree.TreePanel, {
	endEdit : function() {
		this.formWin.hide();
		this.formWin.formPanal.getForm().reset();
	},
	saveNode : function(btn, node) {
		// btn.disable();
		if (!this.formWin.formPanal.getForm().isValid())
			return;
		var values = this.formWin.formPanal.getForm().getFieldValues();

		var self = this;
		Ext.Ajax.request({
					url : 'dict-name!save.action',
					params : {
						data : Ext.encode(ux.util.resetEmptyString(values))
					},
					success : function(form, action) {
						if (form.responseText != "") {
							var result = Ext.util.JSON
									.decode(form.responseText);
							if (result != null) {
								Ext.Msg.alert("错误提示", result.message);
								return;
							}
						}
						Dashboard.setAlert("保存成功!");
						self.endEdit();
						self.updateNode(node);
						btn.enable();
					},
					failure : function(response) {
						Ext.Msg.alert("错误", response.responseText);
						btn.enable();
					}
				})
	},

	deleteNode : function(node) {
		var self = this;
		if (!node.leaf) {
			Ext.Msg.alert('警告', '只能删除叶子分类！');
			return;
		}
		Ext.Msg.confirm('提示', '确定要删除&nbsp&nbsp;' + node.text
						+ '&nbsp&nbsp及其相关的所有类别信息吗', function(btn, text) {
					if (btn == 'yes') {
						Ext.Ajax.request({
									url : 'dict-name!delete.action?dictid='
											+ node.id,
									success : function(form, action) {
										node.parentNode.removeChild(node);
										Dashboard.setAlert("删除成功!");
									},
									failure : function(response) {
										Ext.Msg.alert("错误",
												response.responseText);
									}
								});
					}
				});
	},
	formRecordType : Ext.data.Record.create(['dictid', 'dicthint', 'dictname']),
	addNode : function() {
		this.modifyInternal({})
	},
	modifyNode : function(node) {
		if (node.id == 'root') {
			Ext.Msg.alert("禁止操作", "导航节点不能修改");
		} else {
			var d = node.attributes;
			if (typeof(d.id) == "string") {
				d.dictid = d.id
			}
			d.dicthint = d.text;
			d.dictname = d.name;
			this.modifyInternal(d)
		}
	},
	modifyInternal : function(d) {
		this.formWin.show();
		this.formWin.formPanal.getForm().loadRecord(new this.formRecordType(d))
	},
	refresh : function(node) {
		if (node.isExpanded())
			node.reload();
		else
			node.expand();

	},
	getSelectedNode : function() {
		return this.getSelectionModel().getSelectedNode();
	},
	updateNode : function(node) {
		if (node == null) {
			var selNode = this.getSelectedNode();
			if (selNode.id == 'root') {
				selNode.reload();
			} else {
				selNode.parentNode.reload();
			}
		} else {
			node.reload();
		}
	},
	searchNode : function() {
		var dictText = this.getTopToolbar().getComponent(0).getValue().trim();
		if (!dictText) {
			Ext.Msg.alert('提示', '请输入分类名称');
			return;
		}
		var self = this;
		Ext.Ajax.request({
			url : 'dict-name!search.action',
			params : {
				'dictText' : dictText
			},
			success : function(form, action) {
				if (!form.responseText) {
					Ext.Msg.alert('提示', '无此分类,请重新输入');
					return;
				}
				self
						.expandNode(Ext.util.JSON.decode(form.responseText).message);

			},
			failure : function(form, action) {
				Ext.Msg.alert('提示', '查找错误');
			}
		});
	},
	expandNode : function(id) {
		var self = this.getNodeById(id);
		this.getSelectionModel().select(self);
		return;
	},
	refreshNode : function() {
		var node = this.getSelectedNode();
		if (!node)
			this.refresh(this.getRootNode());
		else {
			this.refresh(node);
		}
	}

});

//Dashboard.registerNav(DataNavTree, 3);