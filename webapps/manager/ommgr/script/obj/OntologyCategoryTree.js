OntologyCategoryTree = function(_cfg) {
	_cfg = _cfg || {};
	var self = this;
	this.data = {};
	var cfg = Ext.apply({
				title : '知识库管理',
				width : 200,
				minSize : 175,
				maxSize : 400,
				xtype : 'treepanel',
				collapsible : true,
				rootVisible : false,
				enableDD : true,
				lines : false,
				border : false,
				autoScroll : true,
				containerScroll : true,
				root : {
					lazyLoad : true,
					id : '0',
					text : 'root',
					iconCls : 'icon-ontology-root'
				},
				dataUrl : 'ontology-category!list.action'
			}, _cfg);
	var tbaritems = [{
				text : '刷新',
				iconCls : 'icon-refresh',
				width : 50,
				handler : function() {
					self.refresh(self.getRootNode());
				}
			}];
	cfg.tbar = new Ext.Toolbar({
		height : '20',
		items : Dashboard.u().filterAllowed(tbaritems)
	});
	OntologyCategoryTree.superclass.constructor.call(this, cfg);
	this.on('beforeload', function(node, event) {
				this.getLoader().baseParams['ispub'] = this.isPub(node);
			});
	if (cfg.compact)
		return

	this.on('dblclick', function(n) {alert(1);}, this);
	this.on('load', function(node, refNode) {
				if (node.childNodes) {
					Ext.each(node.childNodes, function(n) {
								if (n.attributes.objectId) {
									n.draggable = true;
								}
							});
				}
			});
};

Ext.extend(OntologyCategoryTree, Ext.tree.TreePanel, {});


