Ext.namespace('brand')
brand.BrandStore = function(_cfg) {

	var loadGridProxy = new Ext.data.HttpProxy({
				url : 'ontology-dimension!getOntologyDimensionByName.action'
	});

	var dataReader = new Ext.data.JsonReader({
				//idProperty : 'brandid',
				root : 'data',
				fields : [{
							name : 'id',
							type : 'string'
						}, {
							name : 'name',
							type : 'string'
						}]
			});

	var cfg = {
		proxy : loadGridProxy,
		reader : dataReader,
		remoteSort : true,
		autoLoad : false,
		writer : new Ext.data.JsonWriter()
	};
	brand.BrandStore.superclass.constructor.call(this, Ext.applyIf(_cfg || {}, cfg));
}

Ext.extend(brand.BrandStore, Ext.data.Store, {
			loadDatas : function() {
				var store = this;
				store.setBaseParam('query', 'brand');
				store.load();
			}
		})
