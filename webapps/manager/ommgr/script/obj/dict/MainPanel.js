Ext.namespace('dict')
dict.ObjectType = Ext.data.Record.create(['id', 'dictId', 'dictName',
		'dictHint', 'itemCode', 'itemValue']);
dict.MainPanel = function(_cfg) {
	var self = this;
	this.data = {};
	this.extraAttrs = {};
	var _pageSize = 20;
	var dictType;

	var _store = new Ext.data.Store({
				proxy : new Ext.data.HttpProxy({
							url : 'dict-content!list.action'
						}),
				reader : new Ext.data.JsonReader({
							idProperty : 'id',
							root : 'data',
							fields : [{
										name : 'id',
										type : 'string'
									}, {
										name : 'itemcode',
										type : 'string'
									}, {
										name : 'itemvalue',
										type : 'string'
									}, {
										name : 'dicthint',
										type : 'string'
									}, {
										name : 'isactive',
										type : 'string'
									}, {
										name : 'creator',
										type : 'string'
									}, {
										name : 'creattime',
										mapping : 'creattime',
										type : 'date',
										dateFormat : 'Y-m-dTH:i:s'
									}, {
										name : 'editor',
										type : 'string'
									}, {
										name : 'edittime',
										mapping : 'edittime',
										type : 'date',
										dateFormat : 'Y-m-dTH:i:s'
									}]
						}),
				writer : new Ext.data.JsonWriter()
			});
	var pagingBar = new Ext.PagingToolbar({
				store : _store,
				displayInfo : true,
				pageSize : _pageSize,
				prependButtons : true,
				beforePageText : '第',
				afterPageText : '页，共{0}页',
				displayMsg : '第{0}到{1}条记录，共{2}条',
				emptyMsg : "没有记录"
			})
	var _sm = new Ext.grid.CheckboxSelectionModel({
				singleSelect : true
			});

	var actionTbarArray = [{
				text : '新建词典内容',
				tooltip : '新建词典内容',
				iconCls : 'icon-add',
				handler : function() {
					self.newCreate();
				}
			}, {
				text : '修改词典内容',
				tooltip : '修改选中词典内容',
				iconCls : 'icon-wordclass-edit',
				handler : function() {
					var records = _sm.getSelections();
					if (records.length == 0) {
						Ext.Msg.alert('友情提示', '请选择您要修改的知识点!');
					} else if (records.length == 1) {
						self.modifyData(records[0]);
					} else {
						Ext.Msg.alert('友情提示', '您每次只能选择一条知识点进行修改!');
					}
				}
			}, {
				text : '删除词典内容',
				tooltip : '删除选中的词典内容',
				iconCls : 'icon-delete',
				handler : function() {
					var records = _sm.getSelections();
					if (records.length == 0) {
						Ext.Msg.alert('友情提示', '请选择要删除的知识点!');
						return;
					}
					Ext.Msg.confirm("友情提示", "您确定要删除所有选中的知识点吗?", function(
									clickType) {
								if (clickType == "yes") {
									var id = records[0].id
									self.deleteRecord(id);
								}
							});
				}
			}, '-', {
				text : '刷新列表',
				tooltip : '刷新词典内容',
				iconCls : 'icon-refresh',
				handler : function() {
					_store.reload();
				}
			}];
	var actionTbar = new Ext.Toolbar({
				items : actionTbarArray
			});

	var dictCode = new Ext.form.TextField({
				name : "searchDictCode",
				width : 100,
				emptyText : "请输入字典项"
			});
	var dictValue = new Ext.form.TextField({
				name : "searchDictValue",
				width : 100,
				emptyText : "请输入字典值"
			});
	var startTime = new Ext.form.DateField({
				ref : 'startTimeText',
				name : 'startTime',
				fieldLabel : '起始时间',
				xtype : 'datefield',
				format : 'Y-m-d',
				editable : false,
				width : 180
			});
	var endTime = new Ext.form.DateField({
				ref : 'endTimeText',
				name : 'endTime',
				fieldLabel : '结束时间',
				xtype : 'datefield',
				format : 'Y-m-d',
				editable : false,
				width : 180
			});
	var cfg = {
		region : 'center',
		border : false,
		frame : false,
		// bodyStyle : 'padding:20px 10px 10px 15px',
		store : _store,
		loadMask : true,
		colModel : new Ext.grid.ColumnModel({
			defaults : {
				sortable : true
			},
			columns : [new Ext.grid.RowNumberer(), _sm, {
						header : 'id',
						dataIndex : 'id',
						hidden : true
					}, {
						header : '字典项',
						dataIndex : 'itemcode'
					}, {
						header : '字典值',
						dataIndex : 'itemvalue',
						sortable : false
					}, {
						header : '所属类型',
						dataIndex : 'dicthint',
						sortable : false
					}, {
						header : '是否有效',
						dataIndex : 'isactive',
						sortable : false,
						renderer : function(value) {
							return value == 1 ? '是' : '否';
						}
					}, {
						header : '创建者',
						dataIndex : 'creator',
						sortable : false
					}, {
						header : '创建时间',
						dataIndex : 'creattime',
						renderer : function(value) {
							return value ? value.dateFormat('Y-m-d H:i:s') : '';
						}
					}, {
						header : '修改者',
						dataIndex : 'editor',
						sortable : false
					}, {
						header : '修改时间',
						dataIndex : 'edittime',
						renderer : function(value) {
							return value
									? value.dateFormat('Y-m-d  H:i:s')
									: '';
						}
					}]
		}),
		tbar : actionTbar,
		listeners : {
			render : function() {
				var searchTbar = new Ext.Toolbar({
							items : ['字典项:', dictCode, '字典值:', dictValue,
									'创建时间:', '从', startTime, '到', endTime, {
										text : '查询',
										iconCls : 'icon-search',
										handler : function() {
											self.searchDatas();
										}
									}]
						});
				searchTbar.render(this.tbar);
			}
		},
		sm : _sm,
		bbar : pagingBar,
		viewConfig : {
			forceFit : true
		}
	}
	dict.MainPanel.superclass.constructor.call(this, Ext.applyIf(_cfg || {},
					cfg));
	this.getPageSize = function() {
		return _pageSize;
	}

	var isActiveRadio1 = new Ext.form.Radio({
				xtype : 'radio',
				name : 'isactive',
				boxLabel : '是',
				inputValue : 1
			})
	var isActiveRadio0 = new Ext.form.Radio({
				xtype : 'radio',
				name : 'isactive',
				boxLabel : '否',
				inputValue : 0
			})
	var _formPanel = new Ext.form.FormPanel({
				labelWidth : 75,
				anchor : '100%',
				waitMsgTarget : true,
				bodyStyle : 'padding : 5px; background-color:' + sys_bgcolor,
				items : [new Ext.form.Hidden({
									name : 'id'
								}), new Ext.form.TextField({
									fieldLabel : '字典项',
									allowBlank : false,
									name : 'itemcode',
									blankText : '',
									invalidText : '',
									anchor : '100%'
								}), new Ext.form.TextField({
									fieldLabel : '字典值',
									allowBlank : false,
									name : 'itemvalue',
									blankText : '',
									invalidText : '',
									anchor : '100%'
								}), {
							fieldLabel : '是否有效',
							layout : 'column',
							items : [{
										layout : 'column',
										items : [isActiveRadio1, isActiveRadio0]
									}]
						}],
				buttons : [{
							text : '保存',
							handler : function() {
								self.saveWord(this);
							}
						}, {
							text : '重置',
							handler : function() {
								_formPanel.form.reset();
							}
						}]
			});
	var _formWin = new Ext.Window({
				width : 300,
				height : 160,
				layout : 'anchor',
				title : '词典内容编辑',
				defaults : {
					border : false
				},
				modal : true,
				plain : true,
				closable : true,
				shim : true,
				closeAction : 'hide',
				collapsible : true,
				resizable : false,
				draggable : true,
				animCollapse : true,
				constrainHeader : true,
				items : [_formPanel]
			});
	_formWin.formPanel = _formPanel;
	this.formWin = _formWin;
	this.dictCode = dictCode;
	this.dictValue = dictValue;
	this.startTime = startTime;
	this.endTime = endTime;
	this.isActiveRadio1 = isActiveRadio1;
	this.isActiveRadio0 = isActiveRadio0;
	this.dictType = dictType;
}
/**
 * @class dict.MainPanel
 * @extends Ext.Panel
 */
Ext.extend(dict.MainPanel, Ext.grid.GridPanel, {
			setDictType : function(dictType) {
				this.dictType = dictType;
			},
			loadDatas : function() {
				var store = this.getStore();
				var store = this.getStore();
				for (var key in store.baseParams) {
					if (key && key.indexOf('dicttype') != -1) {
						delete store.baseParams[key];
					}
				}
				// 绑定查询参数到store,否则点击分页时会出现无参数情况
				if (this.dictType)
					store.setBaseParam('dicttype', this.dictType);
				store.load({
							params : {
								start : 0,
								limit : this.getPageSize(),
								'dicttype' : this.dictType
							}
						});
			},
			searchDatas : function() {

				var store = this.getStore();
				for (var key in store.baseParams) {
					if (key && key.indexOf('dicttype') != -1) {
						delete store.baseParams[key];
					}
					if (key && key.indexOf('itemcode') != -1) {
						delete store.baseParams[key];
					}
					if (key && key.indexOf('itemvalue') != -1) {
						delete store.baseParams[key];
					}
					if (key && key.indexOf('starttime') != -1) {
						delete store.baseParams[key];
					}
					if (key && key.indexOf('endtime') != -1) {
						delete store.baseParams[key];
					}
				}
				// 绑定查询参数到store,否则点击分页时会出现无参数情况
				if (this.dictType)
					store.setBaseParam('dicttype', this.dictType);
				if (this.dictCode.getValue().trim())
					store.setBaseParam('itemcode', this.dictCode.getValue()
									.trim());
				if (this.dictValue.getValue().trim())
					store.setBaseParam('itemvalue', this.dictValue.getValue()
									.trim());
				if (this.startTime.getRawValue())
					store.setBaseParam('starttime', this.startTime
									.getRawValue());
				if (this.endTime.getRawValue())
					store.setBaseParam('endtime', this.endTime.getRawValue());
				store.load({
							params : {
								start : 0,
								limit : this.getPageSize(),
								'dicttype' : this.dictType,
								'itemcode' : this.dictCode.getValue().trim(),
								'itemvalue' : this.dictValue.getValue().trim(),
								'starttime' : this.startTime.getRawValue(),
								'endtime' : this.endTime.getRawValue()
							}
						});
			},
			modifyData : function(object) {
				var formItems = this.formWin.formPanel.items;
				this.formWin.show();
				this.formWin.formPanel.getForm().loadRecord(object);

			},
			newCreate : function() {
				var formItems = this.formWin.formPanel.items;
				formItems.item(0).setValue('');
				formItems.item(1).setValue('');
				formItems.item(2).setValue('');
				this.isActiveRadio1.checked = true;
				this.formWin.show();
			},
			hideWin : function() {
				this.formWin.hide();
			},
			deleteRecord : function(id) {
				var self = this;
				Ext.Ajax.request({
							url : 'dict-content!delete.action',
							params : {
								'id' : id
							},
							success : function(resp) {
								Ext.Msg.alert('提示', '删除成功！');
								self.getStore().reload();
							},
							failure : function(resp) {
								Ext.Msg.alert('错误', resp.responseText);
								btn.enable();
							}
						});
			},
			saveWord : function(btn) {
				if (!this.formWin.formPanel.getForm().isValid()) {
					Ext.Msg.alert('提示', '您尚未正确完整的输入表单内容，请检查！');
					btn.enable();
					return false;
				}
				var formItems = this.formWin.formPanel.items;
				var _idHidden = formItems.item(0);
				var _itemCode = formItems.item(1);
				var _itemValue = formItems.item(2);
				var _isActive = this.formWin.formPanel.form
						.findField("isactive").getGroupValue()
				this.data.id = _idHidden.getValue();
				this.data.itemcode = _itemCode.getValue().trim();
				this.data.itemvalue = _itemValue.getValue().trim();
				this.data.isactive = _isActive;
				this.data.dictid = this.dictType;
				var self = this;
				Ext.Ajax.request({
							url : 'dict-content!save.action',
							params : {
								'data' : Ext.encode(self.data)
							},
							success : function(resp) {
								var result = Ext.util.JSON
										.decode(resp.responseText);
								if (result.message == 'error') {
									Ext.Msg.alert('失败提示', "字典项或者字典值重复");
								} else {
									Ext.Msg.alert('提示', '保存成功！');
									self.hideWin();
									self.getStore().reload();
								}
							},
							failure : function(resp) {
								Ext.Msg.alert('错误', resp.responseText);
								btn.enable();
							}
						});
			}
		})
