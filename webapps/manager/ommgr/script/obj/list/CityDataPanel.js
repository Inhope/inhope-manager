Ext.namespace('obj.citylist')
obj.citylist.MainPanel = Ext.extend(Ext.grid.GridPanel, {
			// init
			initComponent : function() {
				obj.citylist.MainPanel.superclass.initComponent.call(this);
				this.store = new Ext.data.JsonStore({
							idProperty : 'id',
							fields : ['cityid', 'cityname'], // 待确定字段
							url : 'dict-city!list.action',
							root : 'data',
							autoLoad : false
						});
			},
			listObjects : function() {
				this.getStore().load();
			}
		});