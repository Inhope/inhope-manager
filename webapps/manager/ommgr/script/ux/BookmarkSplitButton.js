BookmarkSplitButton = Ext
		.extend(
				Ext.Toolbar.SplitButton,
				{
					_loadMenu : function() {
						var self = this;
						var xhr = Dashboard.createXHR();
						xhr.open('GET', 'log-bookmark!list.action?ts='
								+ new Date().getTime() + '&category='
								+ this.category, false);
						xhr.send();
						var bookmarks = Ext.decode(xhr.responseText);
						var items = [], menu = {};
						for ( var i = 0; i < bookmarks.length; i++) {
							items
									.push({
										iconCls : 'icon-arrow-right',
										idx : i,
										tooltip : '前往该书签',
										bmId : bookmarks[i].id,
										bmName : bookmarks[i].name,
										text : bookmarks[i].name
												+ (bookmarks[i].droppable ? '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img title="点击删除" style="position:absolute;right:0;vertical-align:bottom;" src="images/delete.gif"/>'
														: ''),
										handler : function(el, e) {
											var t = e.getTarget();
											if (t.tagName.toLowerCase() == 'img') {
												self._delete(el.bmId,
														parseInt(el.idx),
														el.bmName);
											} else {
												self._goto(el.bmId, el.bmName);
											}
										}
									});
						}
						if (items.length == 0) {
							return this._getEmptyMenu();
						}
						menu.items = items;
						return menu;
					},
					_getEmptyMenu : function() {
						return {
							items : [ {
								text : '暂无书签'
							} ]
						};
					},
					_goto : function(id, name) {
						var self = this;
						Ext.Msg.confirm('提示,', '确认载入书签"' + name + '"？',
								function(btn) {
									if (btn == 'yes') {
										Ext.Ajax.request({
											url : self.action.url,
											params : {
												id : id
											},
											success : function(r) {
												self.action.cb(r.responseText);
											}
										});

									}
								});
					},
					_delete : function(id, idx, name) {
						var self = this;
						Ext.Msg
								.confirm(
										'提示,',
										'确认删除书签"' + name + '"？',
										function(btn) {
											if (btn == 'yes') {
												Ext.Ajax
														.request({
															url : 'log-bookmark!delete.action',
															params : {
																id : id
															},
															success : function() {
																Dashboard
																		.setAlert('书签已成功删除。');
																self.menu
																		.remove(self.menu
																				.get(idx));
																if (self.menu.items.length == 0)
																	self
																			.setMenu(self
																					._getEmptyMenu());
																self.menu
																		.showAt([
																				0,
																				0 ]);
																self.menu
																		.doLayout(
																				true,
																				true);
																self.menu
																		.hide();
															}
														});

											}
										});
					},
					initComponent : function() {
						this.setMenu(this._loadMenu());
					},
					getMenuClass : function() {
						return 'x-btn-split'
								+ (this.arrowAlign == 'bottom' ? '-bottom' : '')
								+ ' icon-bookmark-btn-arrow';
					},
					handler : function() {
						if (!this.store || this.store.getCount() == 0) {
							Ext.Msg.alert('提示', '无数据可供收藏。');
							return false;
						}
						if (!this._win) {
							var self = this;
							this._form = new Ext.form.FormPanel(
									{
										frame : false,
										border : false,
										labelWidth : 60,
										autoHeight : true,
										waitMsgTarget : true,
										bodyStyle : 'padding : 3px 10px; background-color:' + sys_bgcolor,
										items : [ {
											ref : 'nameField',
											xtype : 'textfield',
											fieldLabel : '书签名称',
											maxLength : 30,
											allowBlank : true,
											name : 'name',
											anchor : '98%'
										} ],
										tbar : [
												{
													text : '收藏',
													iconCls : 'icon-bookmark',
													handler : function(b) {
														var f = self._form;
														var name = f.nameField
																.getValue()
																.trim();
														if (!name) {
															Dashboard
																	.setAlert('请输入书签名称！');
															return false;
														}
														b.disable();
														var saveParams = {
															targetId : self.store
																	.getAt(0).id,
															category : self.category,
															targetCondition : Ext
																	.encode(self.store.baseParams)
														};
														var type = f
																.getTopToolbar().onlyToMe.checked ? 2
																: 1;
														saveParams.type = type;
														saveParams.name = name;
														Ext.Ajax
																.request({
																	url : 'log-bookmark!save.action',
																	params : {
																		data : Ext
																				.encode(saveParams)
																	},
																	success : function(
																			resp) {
																		self
																				.setMenu(self
																						._loadMenu());
																		b
																				.enable();
																		Dashboard
																				.setAlert('书签已成功保存。');
																		self._win
																				.hide();
																	},
																	failure : function() {
																		b
																				.enable();
																	}
																});
													}
												}, '-', {
													xtype : 'checkbox',
													checked : true,
													ref : 'onlyToMe',
													boxLabel : '仅本人可见'
												} ]
									});
							this._win = new Ext.Window({
								defaults : {
									border : false
								},
								modal : true,
								plain : true,
								shim : true,
								closable : true,
								closeAction : 'hide',
								collapsible : true,
								resizable : false,
								draggable : true,
								animCollapse : true,
								constrainHeader : true,
								shadow : false,
								width : 320,
								title : '添加书签',
								items : this._form,
								listeners : {
									'beforehide' : function(p) {
										self._form.getForm().reset();
									}
								}
							});
						}
						this._win.show();
					}
				});