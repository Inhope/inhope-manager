/**
 * var combotree = new
 * Ext.ux.ComboBoxTree({renderTo:'header',rootId:'0',rootText:'root',treeUrl:'ontology-category!list.action'});
 */
Ext.ux.ComboBoxTree = function() {
	this.treeId = Ext.id() + '-tree';
	this.maxHeight = arguments[0].maxHeight || arguments[0].height
			|| this.maxHeight;
	this.tpl = new Ext.Template('<tpl for="."><div style="height:'
			+ this.maxHeight + 'px"><div id="' + this.treeId
			+ '"></div></div></tpl>');
	this.store = new Ext.data.SimpleStore({
				fields : [],
				data : [[]]
			});
	this.selectedClass = '';
	this.mode = 'local';
	this.triggerAction = 'all';
	this.onSelect = Ext.emptyFn;
	this.editable = false;
	this.beforeBlur = Ext.emptyFn;

	if (arguments[0].tree) {
		this.tree = arguments[0].tree;
	} else {
		var root = new Ext.tree.AsyncTreeNode({
					text : arguments[0].rootText,
					id : arguments[0].rootId,
					loader : new Ext.tree.TreeLoader({
								dataUrl : arguments[0].treeUrl,
								clearOnLoad : true
							})
				});

		this.tree = new Ext.tree.TreePanel({
					tbar : [{
								xtype : 'button',
								text : '确定',
								handler : function() {
									alert(1)
								}
							}],
					border : false,
					root : root,
					rootVisible : this.rootVisible,
					listeners : {
						scope : this,
						click : function(node) {
							this.setValue(node);
							this.collapse();
						}
					}
				});
	}

	// all:所有结点都可选中
	// exceptRoot：除根结点，其它结点都可选（默认）
	// folder:只有目录（非叶子和非根结点）可选
	// leaf：只有叶子结点可选
	this.selectNodeModel = arguments[0].selectNodeModel || 'exceptRoot';

	this.addEvents('afterchange');
	Ext.ux.ComboBoxTree.superclass.constructor.apply(this, arguments);
}

Ext.extend(Ext.ux.ComboBoxTree, Ext.form.ComboBox, {
	onViewClick : Ext.emptyFn,
	assertValue : Ext.emptyFn,
	expand : function() {
		Ext.ux.ComboBoxTree.superclass.expand.call(this);
		if (this.tree.rendered) {
			return;
		}

		Ext.apply(this.tree, {
					height : this.maxHeight,
					width : (this.listWidth || this.width - (Ext.isIE ? 3 : 0))
							- 2,
					border : false,
					autoScroll : true
				});
		if (this.tree.xtype) {
			this.tree = Ext.ComponentMgr.create(this.tree, this.tree.xtype);
		}
		this.tree.render(this.treeId);

		var root = this.tree.getRootNode();
		if (!root.isLoaded())
			root.reload();
		this.tree.on('click', function(node) {
					var selModel = this.selectNodeModel;
					var isLeaf = node.isLeaf();

					if ((node == root) && selModel != 'all') {
						return;
					} else if (selModel == 'folder' && isLeaf) {
						return;
					} else if (selModel == 'leaf' && !isLeaf) {
						return;
					}

					var oldNode = this.getNode();
					if (this.fireEvent('beforeselect', this, node, oldNode) !== false) {
						this.setValue(node);
						this.collapse();

						this.fireEvent('select', this, node, oldNode);
						(oldNode !== node) ? this.fireEvent('afterchange',
								this, node, oldNode) : '';
					}
				}, this);
	},

	setValue : function(node) {
		this.node = node;
		var text = node.text;
		this.lastSelectionText = text;
		if (this.hiddenField) {
			this.hiddenField.value = node.id;
		}
		Ext.form.ComboBox.superclass.setValue.call(this, text);
		this.value = node.id;
	},

	getValue : function() {
		return typeof this.value != 'undefined' ? this.value : '';
	},

	getNode : function() {
		return this.node;
	},

	clearValue : function() {
		Ext.ux.ComboBoxTree.superclass.clearValue.call(this);
		this.node = null;
	},

	// private
	destroy : function() {
		Ext.ux.ComboBoxTree.superclass.destroy.call(this);
		Ext.destroy([this.node, this.tree]);
		delete this.node;
	}
});

Ext.reg('combotree', Ext.ux.ComboBoxTree);