var FloatHtmlEditor = Ext.extend(Ext.form.HtmlEditor, {
	onEditorEvent : function(e) {
		if (this.onSpecialKey) {
			this.onSpecialKey(e);
		}
		return FloatHtmlEditor.superclass.onEditorEvent.apply(this, arguments)
	},
	getValue : function() {
		var ret = FloatHtmlEditor.superclass.getValue.apply(this, arguments);
		if (ret) {// fix edit grid panel compare startvalue & nowvalue
			ret = ret.replace(/^(&nbsp;|<br>|\s)*|(&nbsp;|<br>|\s)*$/ig, '')
		}
		if (!ret) {
			ret = '';
		}
		return ret;
	},
	initComponent : function() {
		FloatHtmlEditor.superclass.initComponent.call(this);
		this.on('afterrender', function() {
					this.getToolbar().hide();
				}, this)
		this.on('initialize', function() {
			var toolbarWin = this.toolbarWin = new Ext.Window({
						html : '<div id="' + this.valueEditToolbarId
								+ '" class=""></div>',
						height : 233,
						width : 50,
						closeAction : 'hide',
						resizable : false,
						iconCls : 'icon-toolbox'
					})
			toolbarWin.show();
			var xy = this.getHoverTarget().getEl().getXY();
			toolbarWin.setPagePosition(xy[0] + this.getHoverTarget().getWidth()
							- toolbarWin.getWidth(), xy[1]);
			var el = this.getToolbar().getEl();
			var appentToEl = Ext.getDom(this.valueEditToolbarId);
			appentToEl.className = el.dom.parentNode.className;
			el.appendTo(appentToEl);
			this.getToolbar().show();
			var tbarRawRow = el.query('.x-toolbar-left-row')[0];
			var btnNodes = Ext.Element.fly(tbarRawRow).query('.x-toolbar-cell');
			Ext.each(btnNodes, function(btnNode) {
						var btnEl = Ext.Element.fly(btnNode);
						if (btnEl.query('.xtb-sep').length) {
							btnEl.remove();
						} else {
							var newRow = Ext.Element.fly(tbarRawRow)
									.insertSibling({
												tag : 'tr',
												cls : 'x-toolbar-left-row'
											});
							newRow.appendChild(btnNode);
						}
					})
			toolbarWin.hide();
		}, this)
	},
	// public >>>
	getHoverTarget : Ext.emptyFn,
	toolbarWin : null,
	valueEditToolbarId : Ext.id(),
	// public <<<

	setSize : function(w, h) {
		FloatHtmlEditor.superclass.setSize.apply(this, arguments);
		var t = Ext.Element.fly(this.getEl().parent().query('iframe')[0]);
		t.setHeight(this.height-6);
		//t.dom.parentNode.parentNode.style.top = 0;
		//t.focus();
	},
	focus : function(st, delay){
		var t = Ext.Element.fly(this.getEl().parent().query('iframe')[0]);
		t.focus();
	},
	enableAlignments : false,
	enableFont : false,
	enableLinks : false,
	enableSourceEdit : false
})