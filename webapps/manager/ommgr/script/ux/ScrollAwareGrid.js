
ExcelLikeGrid = Ext.extend(Ext.grid.EditorGridPanel, {
    
	initComponent : function(){
        Ext.grid.EditorGridPanel.superclass.initComponent.call(this);

        this.addEvents(
        	'scrolltopclick',
        	'scrollbottomclick',
        	'scrollleftclick',
        	'scrollrightclick'
        );
        
        
        this.on("mousedown",function(e) {
        	var last = this._lastFire
        	var curr = new Date().getTime()
        	if(last && curr-last<100) return;
        	this._lastFire = curr
			var c = this.getEl().select("div[class=x-grid3-scroller]").first();
			var x = c.getX()+c.dom.clientWidth;
			var y = c.getY()+c.dom.clientHeight;
			var xe = e.getPageX();
			var ye = e.getPageY();
			if(xe>x && xe<x+Ext.getScrollBarWidth()
				&& ye>y-Ext.getScrollBarWidth() && ye<y){
				if(c.dom.scrollTop + c.dom.clientHeight >= c.dom.scrollHeight) {
					this.fireEvent('scrollbottomclick');
				}
			}
		});
	}


});
