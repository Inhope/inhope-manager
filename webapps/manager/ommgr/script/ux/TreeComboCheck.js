var CheckTreeLoader = Ext.extend(Ext.tree.TreeLoader, {
			checkIds : {},
			createNode : function(attr) {
				attr.checked = false;
				if (this.checkIds[attr.id])
					attr.checked = true;
				return CheckTreeLoader.superclass.createNode.apply(this,
						arguments);
			}
		})
// dataUrl: 'ontology-class!list.action'
// fieldLabel : '本体分类',
var TreeComboCheck = Ext.extend(TreeComboBox, {
			emptyText : '请选择...',
			editable : false,
			allowBlank : true,
			minHeight : 250,
			root : {
				id : '0',
				text : 'root',
				iconCls : 'icon-ontology-root'
			},
			initComponent : function() {
				var self = this;
				if (!this.dataUrl)
					throw new Error('dataUrl required');
				this.loader = new CheckTreeLoader({
							dataUrl : this.dataUrl
						});
				this.treeConfig = {
					listeners : {
						afterrender : function(tree) {
							self.expandCheckedNode(tree)
						},
						checkchange : function(node, checked) {
							if (self.reseting)
								return;
							if (checked) {
								self.getValue().push({
									id : node.id,
									name : node.text,
									path : node.getPath('text').replace(
											'/root', '')
								});
							} else {
								Ext.each(self.getValue(), function(cls, index) {
											if (cls.id == node.id) {
												self.getValue()
														.splice(index, 1);
												return false;
											}
										})
							}
							self.updateUIText();
						}
					}
				};
				TreeComboCheck.superclass.initComponent.call(this);
			},
			getValue : function() {
				if (!this.value) {
					this.value = [];
				}
				return this.value;
			},
			updateUIText : function() {
				this.onValueChange(this.value);
				var s = '';
				Ext.each(this.getValue(), function(cls) {
							s += cls.path + ' , ';
						})
				if (s) {
					s = s.substring(0, s.length - 2);
				}
				TreeComboCheck.superclass.setRawValue.call(this, s);
			},
			onValueChange : Ext.emptyFn,// function(values){}
			setValue : function(vs) {
				if (typeof vs == 'string')
					return

				this.value = vs;
				if (vs) {
					this.updateUIText();
					this.loader.checkIds = {};
					this.value = vs;
				} else {
					this.onValueChange();
				}
			},
			reset : function() {
				try {
					this.reseting = true;
					if (this.value && this.tree) {
						Ext.each(this.value, function(cls) {
									var n = this.tree.getNodeById(cls.id);
									if (n) {
										n.getUI().toggleCheck(false)
									}
								}, this)
					}
					if (this.tree) {
						this.tree.collapseAll();
					}
				} finally {
					this.reseting = false;
				}
				this.value = [];
			},
			// expand & check
			doQuery : function() {
				this.expand();
				if (this.tree) {
					this.expandCheckedNode(this.tree)
				}
			},
			expandCheckedNode : function(tree) {
				try {
					this.reseting = true;
					Ext.each(this.getValue(), function(v) {
								var n;
								if (tree) {
									n = tree.getNodeById(v.id)
								}
								if (n) {
									n.getUI().toggleCheck(true);
								} else {
									this.loader.checkIds[v.id] = true
								}
								var arr = v.idPath.split('/');
								if (arr.length > 0)
									arr.length = arr.length - 1;
								tree.expandPath('/0' + arr.join('/'));
							}, this)
				} finally {
					this.reseting = false;
				}
			}
		})