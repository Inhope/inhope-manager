Ext.namespace('ux')
var _ext_panel_init = Ext.Panel.prototype.initComponent;
var _ext_panel_const = Ext.Panel;
Ext.override(Ext.data.Record, {
			_set_method : Ext.data.Record.prototype.set,
			set : function(key, value) {
				if (!key) {
					return;
				}
				return this._set_method.apply(this, arguments)
			}
		})
Ext.override(Ext.Editor, {
			_realign : Ext.Editor.prototype.realign,
			_initComponent : Ext.Editor.prototype.initComponent,
			_setSize : Ext.Editor.prototype.setSize,
			initComponent : function() {
				this.autoSize = true;
				if (this.field && this.field.getCustomEditorSize) {
					this.getCustomEditorSize = this.field.getCustomEditorSize
				}
				this._initComponent.call(this);
			},
			setSize : function(w, h) {
				if (this.getCustomEditorSize) {
					var sz = this.getCustomEditorSize(this);
					if (sz[0])
						w = sz[0];
					if (sz[1])
						h = sz[1]
				}
				this._setSize.apply(this, [w, h]);
			},
			realign : function(autoSize) {
				var old = this.boundEl
				if (old)
					this.boundEl = old.findParent('td', 5, true)
				this._realign.apply(this, arguments)
				this.boundEl = old;
			}
		});
Ext.override(Ext.Panel, {
// split : true,
		// collapsible : true
		// initComponent : function() {
		// if (this.region && this.region != 'center') {
		// if (this.title == '控制台') {
		// Ext.apply(this.initialConfig, {
		//
		// })
		// }
		// }
		// return _ext_panel_init.call(this);
		// }
		})
Ext.override(Ext.tree.TreePanel, {
			// private
			renderRoot : function() {
				if (!this.root.attributes.lazyLoad) {
					this.renderRootInternal();
				}
			},
			renderRootInternal : function() {
				if (this.__rootRendered)
					return false;
				this.__rootRendered = true;
				this.root.render();
				if (!this.rootVisible) {
					this.root.renderChildren();
				}
			},
			renderRootNode : function() {
				this.renderRootInternal();
			}
		})
Ext.form.ComboBox.prototype._doQuery = Ext.form.ComboBox.prototype.doQuery
// Ext.override(Ext.form.ComboBox, {
// doQuery : function() {
// if (this.getStore().autoLoad === false && !this.getStore().__autoLoaded) {
// this.getStore().__autoLoaded = true;
// this.getStore().load();
// }
// return Ext.form.ComboBox.prototype._doQuery.apply(this, arguments)
// }
// })
Function.prototype.dg = Function.prototype.createDelegate

ux.ObjectTextField = Ext.extend(Ext.form.Field, {
			fieldPath : '',
			initComponent : function() {
				if (!this.fieldPath) {
					throw new Error('ObjectTextField.fieldPath required')
				}
			},
			reset : function() {
				this.value = null;
				this.setRawValue(null);
			},
			setValue : function(v) {
				if (v) {
					if (typeof v == 'string') {
						if (!this.value)
							this.value = {};
						this.value[this.fieldPath] = v;
					} else {
						this.value = v;
					}
					this.setRawValue(v[this.fieldPath]);
				} else {
					this.value = v;
				}
			},
			getValue : function() {
				var v = this.getRawValue();
				if (v) {
					if (!this.value)
						this.value = {};
					this.value[this.fieldPath] = v;
				} else {
					this.value = null;
				}
				return this.value;
			}
		})

ux.PostEditableEditorGridPanel = Ext.extend(Ext.grid.EditorGridPanel, {
			postEditValue : function(value, startValue, r, field) {
				if (this.onPostEditValue) {
					this.onPostEditValue(value, startValue, r, field)
				}
				return ux.PostEditableEditorGridPanel.superclass.postEditValue
						.apply(this, arguments);
			}
		})
ux.util = {
	resetEmptyString : function(obj) {
		for (var key in obj) {
			var v = obj[key];
			if (!v) {
				delete obj[key];
			} else if (v.push && v.length) {
				for (var i = 0; i < v.length; i++) {
					this.resetEmptyString(v[i]);
				}
			} else if (key.indexOf('id') != -1 && key.indexOf('-') == 0) {
				delete obj[key]// reset negative id as null
			}
		}
		return obj;
	}
}
// enable grid cell to be selected in chrome/IE
Ext.grid.GridView.prototype.cellTpl = new Ext.Template(Ext.grid.GridView.prototype.cellTpl.html
		.replace('unselectable="on"', '').replace('class="',
				'class="x-selectable '));

Ext.override(Ext.Button,
		{
		   initComponent: function()
		   {
		      Ext.Button.superclass.initComponent.call(this);
		      this.addEvents(
		            "click",
		            "toggle",
		            'mouseover',
		            'mouseout',
		            'menushow',
		            'menuhide',
		            'menutriggerover',
		            'menutriggerout'
		      );
		      if (this.menu)
		      {
		         var m = this.menu;
		         delete this.menu;
		         this.setMenu(m);
		      }
		         
		      if (typeof this.toggleGroup === 'string')
		         this.enableToggle = true;
		   },
		   
		   setMenu: function(menu)
		   {
		      var hasMenu = (this.menu != null);
		      this.menu = Ext.menu.MenuMgr.get(menu);
		      if (this.rendered && !hasMenu)
		      {
		         this.el.child(this.menuClassTarget).addClass('x-btn-with-menu');
		         this.menu.on("show", this.onMenuShow, this);
		         this.menu.on("hide", this.onMenuHide, this);
		      }
		   },
		   
		   clearMenu: function(destroy)
		   {
		      if (this.rendered)
		      {
		         this.el.child(this.menuClassTarget).removeClass('x-btn-with-menu');
		         this.menu.un('show', this.onMenuShow, this);
		         this.menu.un('hide', this.onMenuHide, this);
		      }
		      if (destroy)
		         Ext.destroy(this.menu);
		         
		      this.menu = null;
		   }
		}
		);