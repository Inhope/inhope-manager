<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="res" uri="/WEB-INF/tld/import-res.tld"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="usr" uri="/WEB-INF/tld/login-user.tld"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>语音内容管理系统</title>
	<%@include file="/commons/extjsvable.jsp"%>
	<usr:checkPerm jsExportVar="rs_check_perm_result" permissionNames="rs"
			dump="true" />
	<usr:name jsExportVar="rs_username" />
	<link rel="stylesheet" type="text/css" href="custom.css" />
	<res:import dir="script" include=".*\.js" recursive="true" />
	<link type="text/css" rel="stylesheet" href="../commons/dp.SyntaxHighlighter/Styles/SyntaxHighlighter.css"></link>
	<!-- <script language="javascript" src="../commons/dp.SyntaxHighlighter/Scripts/shCore.js"></script>
	<script language="javascript" src="../commons/dp.SyntaxHighlighter/Scripts/shBrushXml.js"></script> -->
</head>
<body>
</body>
</html>