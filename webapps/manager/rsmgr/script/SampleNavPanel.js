SampleNavPanel = function() {
	var self = this;
	var menu = Dashboard.u().filterAllowed([/*{
				id : '1',
				authName : 'rs.sample',
				name : '内容管理',
				icon : 'icon-expand'
			}, {
				id : '11',
				name : '场景管理',
				module : 'scenarioPanel',
				parent : '1',
				icon : 'icon-toolbox',
				panelClass : ScenarioPanel
			},*/ {
				id : '1',
				authName : 'rs.reco.sample.',
				name : '语音样例管理',
				module : 'samplePanel',
				icon : 'icon-toolbox',
				panelClass : SamplePanel
			}, /*{
				id : '3',
				name : '统计分析',
				icon : 'icon-expand'
			},*/ {
				id : '2',
				authName:'rs.reco.stat',
				name : '识别结果统计',
				icon : 'icon-toolbox',
				module : 'recogStat',
				panelClass : RecognizeStatPanel
			}]);
	
	var rootNode = new Ext.tree.TreeNode({
				id : '0',
				text : 'root'
			});
	var _nodeMap = {};

	rootNode.on('click', function() {
				//rootNode.firstChild.expand();
				var n = rootNode.firstChild;
				n.select();
				var module = n.attributes.module;
				var panelClass = n.attributes.panelClass;
				var p = self.showTab(panelClass, module + 'Tab', n.text,
						n.attributes.iconCls, true);
				if (p && p.loadData)
					p.loadData();
			});

	for (var i = 0; i < menu.length; i++) {
		var _item = menu[i];
		var _node = new Ext.tree.TreeNode({
					id : _item.id,
					text : _item.name,
					iconCls : _item.icon,
					module : _item.module,
					panelClass : _item.panelClass,
					handler : _item.handler
				});
		_nodeMap[_item.id] = _node;
		if (!_item.parent)
			rootNode.appendChild(_node);
		else
			_nodeMap[_item.parent].appendChild(_node);
	}
	var config = {
		title : '语音识别测试',
		border : false,
		autoScroll : true,
		rootVisible : false,
		lines : false,
		root : rootNode
	};
	SampleNavPanel.superclass.constructor.call(this, config);

	this.modulePanels = {};
	this.on('click', function(n) {
				if (!n)
					return false;
				if (n.attributes.handler) {
					n.attributes.handler.call(n, arguments);
					return false;
				}
				var module = n.attributes.module;
				var panelClass = n.attributes.panelClass;
				var p = self.showTab(panelClass, module + 'Tab', n.text,
						n.attributes.iconCls, true);
				if (p && p.loadData)
					p.loadData();
			});
}

Ext.extend(SampleNavPanel, Ext.tree.TreePanel, {
			authName : 'rs.reco'
		});

Dashboard.registerNav(SampleNavPanel, 4);