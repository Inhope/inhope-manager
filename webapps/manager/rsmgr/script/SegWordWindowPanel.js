SegWordWindowPanel = function(cfg) {
	cfg = cfg || {};
	var self = this;
	this.pageSize = 20;
	this.store;
	this.matchIndex = 0;

	var segmentWordTextarea = new Ext.form.TextArea({
		  width : 530,
		  height : 200,
		  readOnly : true,
		  hideLabel : true,
		  style : 'background:none repeat scroll 0 0 #E6E6E6;color:black;'
	  });
	this.segmentWordTextarea = segmentWordTextarea;

	var inputSentence = new Ext.form.TextArea({
		  enableKeyEvents : true,
		  height : 40,
		  columnWidth : .8,
		  emptyText : '输入需要分词的语音语句'
	  });

	var _segWordForm = new Ext.FormPanel({
		  layout : "fit",
		  frame : true,
		  border : 0,
		  autoHeight : true,
		  waitMsgTarget : true,
		  defaults : {
			  bodyStyle : 'padding:10px'
		  },
		  margins : '0 0 0 0',
		  fileUpload : true,
		  items : [{
			    xtype : 'fieldset',
			    title : '输入语句',
			    autoHeight : true,
			    layout : 'column',
			    items : [inputSentence, {
				      xtype : 'button',
				      columnWidth : .1,
				      height : 40,
				      style : 'margin-left:20px;',
				      text : "开始执行",
				      handler : function() {
					      var sentenceValue = inputSentence.getValue();
					      self.segmentWord(sentenceValue);
				      }
			      }]
		    }, {
			    xtype : 'fieldset',
			    title : '导入文件',
			    autoHeight : true,
			    layout : 'column',
			    items : [{
				      name : 'sentenceFile',
				      xtype : "textfield",
				      fieldLabel : '文件',
				      inputType : 'file',
				      columnWidth : .8,
				      allowBlank : false,
				      blankText : '导入文件不能为空'
			      }, {
				      xtype : 'button',
				      columnWidth : .1,
				      style : 'margin-left:20px;',
				      text : "开始导入",
				      handler : function() {
					      var btn = this;
					      var fileForm = _segWordForm.getForm();
					      if (fileForm.isValid()) {
						      fileForm.submit({
							        url : 'speech-dict!importSentenceSegWord.action',
							        success : function(form, action) {
								        btn.enable();
								        var result = action.result.data;
								        var timer = new ProgressTimer({
									          initData : result,
									          progressId : 'importSentenceSegWordStatus',
									          progress : true,
									          progressText : 'percent',
									          boxConfig : {
										          title : '导入句子分析'
									          },
									          finish : function(p, response) {
										          if (p.currentCount == 100) {
											          // 准备下载
											          if (!self.downloadIFrame) {
												          self.downloadIFrame = self.getEl().createChild({
													            tag : 'iframe',
													            style : 'display:none;'
												            })
											          }
											          self.downloadIFrame.dom.src = 'speech-dict!downExportFile.action?type=segWord&_t=' + new Date().getTime();
										          }
									          }
								          });
								        timer.start();
							        },
							        failure : function(form, action) {
								        btn.enable();
								        Ext.MessageBox.hide();
								        Ext.Msg.alert('错误', '导入初始化失败.' + (action.result.message ? '详细错误:' + action.result.message : ''));
							        }
						        });
					      }
				      }
			      }]
		    }, {
			    xtype : 'fieldset',
			    title : '分词结果',
			    items : segmentWordTextarea
		    }]
	  });
	var _cfg = {
		width : 600,
		height : 500,
		title : '语音分词',
		defaults : {// 表示该窗口中所有子元素的特性
			border : false
			// 表示所有子元素都不要边框
		},
		plain : true,// 方角 默认
		modal : false,
		closeAction : 'hide',
		collapsible : true,// 折叠
		closable : true, // 关闭
		resizable : false,// 改变大小
		draggable : true,// 拖动
		minimizable : false,// 最小化
		maximizable : false,// 最大化
		animCollapse : true,
		constrainHeader : true,
		autoHeight : false,
		items : [_segWordForm],
		listeners : {
			show : function() {
				inputSentence.getEl().dom.onkeydown = function() {
					var keyCode = arguments.length > 0 ? arguments[0].which : event.keyCode;
					try {
						if (13 == keyCode) {
							var sentenceValue = inputSentence.getValue();
							self.segmentWord(sentenceValue);
							inputSentence.focus(true, true);
						}
					} catch (exception) {
					}
				};
			}
		}
	}

	SegWordWindowPanel.superclass.constructor.call(this, _cfg);
};

Ext.extend(SegWordWindowPanel, Ext.Window, {
	  showWindow : function() {
		  this.show();
	  },
	  segmentWord : function(sentenceValue) {
		  if (!sentenceValue) {
			  Ext.Msg.alert('提示', '输入的句子不能为空');
			  return;
		  }
		  var self = this;
		  Ext.Ajax.request({
			    url : 'speech-dict!segWord.action',
			    params : {
				    'sentence' : sentenceValue
			    },
			    success : function(form, action) {
				    var resultObj = Ext.util.JSON.decode(form.responseText);
				    if (resultObj.success)
					    self.segmentWordTextarea.setValue(resultObj.message);
				    else
					    Ext.Msg.alert('提示', '分词错误');
			    },
			    failure : function(response) {
				    Ext.Msg.alert('错误', response.responseText);
			    }
		    });
	  }
  });
