SpeechDictNavPanel = function(_cfg) {
	var self = this;
	this.data = {};
	this.DictPanel = '';

	var cfg = {
		title : '词典功能管理',
		width : 200,
		minSize : 175,
		maxSize : 400,
		xtype : 'treepanel',
		collapsible : true,
		rootVisible : false,
		lines : false,
		border : false,
		autoScroll : true,
		root : {
			id : '0',
			text : '语音词典',
			iconCls : 'icon-ontology-root',
			lazyLoad : true,
			children : Dashboard.u().filterAllowed([{
				  id : '1',
				  authName : 'rs.dict.pron',
				  text : "语音发音词典",
				  iconCls : 'icon-wordclass-root',
				  leaf : true
			  }, {
				  id : '2',
				  authName : 'rs.dict.speech_assist',
				  text : "发音辅助工具",
				  iconCls : 'icon-toolbox',
				  leaf : true
			  }, {
				  id : '3',
				  authName : 'rs.dict.speech_test',
				  text : "语音测试工具",
				  iconCls : 'icon-toolbox',
				  leaf : true
			  }, {
				  id : '4',
				  authName : 'rs.dict.eng_abb',
				  text : "常用英文缩写",
				  iconCls : 'icon-wordclass',
				  leaf : true
			  }, {
				  id : '5',
				  authName : 'rs.dict.eng_word',
				  text : "常用英文单词",
				  iconCls : 'icon-wordclass',
				  leaf : true
			  }, {
				  id : '7',
				  authName : 'rs.dict.segment_word',
				  text : "语音分词工具",
				  iconCls : 'icon-statistic',
				  leaf : true
			  }])
		}
	};
	SpeechDictNavPanel.superclass.constructor.call(this, Ext.applyIf(_cfg || {}, cfg));
	this.on('click', function(n) {
		  if (!n)
			  return false;
		  if (n.id == 1) {
			  this.DictPanel = self.showTab(SpeechDictPanel, "speechDictsTab", n.text, 'icon-wordclass-root', true);
			  this.DictPanel.loadData();
		  } else if (n.id == 2) {
			  new SpeechDictToolPanel().showWindow();
		  } else if (n.id == 3) {
			  if (!this.speechTestWindow)
				  this.speechTestWindow = new SpeechTestToolPanel();
			  this.speechTestWindow.showWindow();
		  } else if (n.id == 4) {
			  this.abbrPanel = self.showTab(SpeechAssistWordPanel, "abbrWordsTab", n.text, 'icon-wordclass', true);
			  this.abbrPanel.loadWords(1);
		  } else if (n.id == 5) {
			  this.enWordsPanel = self.showTab(SpeechAssistWordPanel, "enWordsTab", n.text, 'icon-wordclass', true);
			  this.enWordsPanel.loadWords(2);
		  } else if (n.id == 7) {
			  if (!this.segWordWindowPanel)
				  this.segWordWindowPanel = new SegWordWindowPanel();
			  this.segWordWindowPanel.showWindow();
		  }
	  })
	// this.on('afterrender', function(){new
	// SpeechTestToolPanel().showWindow();})
}

Ext.extend(SpeechDictNavPanel, Ext.tree.TreePanel, {
	  authName : 'rs.dict'
  });
Dashboard.registerNav(SpeechDictNavPanel, 3);
