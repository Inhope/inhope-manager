SpeechModelNavPanel = function(_cfg) {
	var self = this;
	this.modelType;
	this.data = {};
	this.modelPanel = '';
	this.importType = '';
	var cfg = {
		title : '语音模型管理',
		width : 200,
		minSize : 175,
		maxSize : 400,
		xtype : 'treepanel',
		collapsible : true,
		rootVisible : false,
		lines : false,
		border : false,
		autoScroll : true,
		listeners : {},
		root : {
			id : '0',
			text : 'root',
			iconCls : 'icon-ontology-root',
			lazyLoad : true,
			children : [{
				  id : '1',
				  text : "语料模型"
			  }]
		},
		dataUrl : 'speech-model-category!list.action',
		tbar : new Ext.Toolbar({
			  enableOverflow : true,
			  height : '20',
			  items : [{
				    emptyText : "请输入分类名称",
				    xtype : 'textfield',
				    width : 120,
				    listeners : {
					    specialkey : function(f, e) {
						    if (e.getKey() == e.ENTER) {
							    self.searchNode(f);
						    }
					    }
				    }
			    }, {
				    text : '搜索',
				    iconCls : 'icon-search',
				    width : 50,
				    handler : function() {
					    self.searchNode();
				    }
			    }, {
				    text : '同步所有模型',
				    iconCls : 'icon-wordclass-root-sync',
				    width : 50,
				    handler : function() {
					    self.syncAll();
				    }
			    }, {
				    text : '模型文件管理',
				    iconCls : 'icon-ontology-sync',
				    width : 50,
				    handler : function() {
					    self.modelFileWin.show();
				    }
			    }, {
				    text : '模型应用',
				    iconCls : 'icon-ok',
				    width : 50,
				    handler : function() {
					    self.applyAll();
				    }
			    }, '-', {
				    text : '刷新',
				    iconCls : 'icon-refresh',
				    width : 50,
				    handler : function() {
					    self.refresh(self.getRootNode());
				    }
			    }]
		  })
	};

	SpeechModelNavPanel.superclass.constructor.call(this, Ext.applyIf(_cfg || {}, cfg));

	var menu = new Ext.menu.Menu({
		  items : [{
			    text : '刷新',
			    iconCls : 'icon-refresh',
			    width : 50,
			    handler : function() {
				    self.refreshNode();
			    }
		    }, {
			    ref : 'addChildSpeechCategory',
			    text : '新增分类',
			    iconCls : 'icon-wordclass-add',
			    width : 50,
			    handler : function() {
				    self.addNode(menu.currentNode, false);
			    }
		    }, {
			    id : 'createSpeechModel',
			    text : '新增模型',
			    iconCls : 'icon-wordclass-add',
			    width : 50,
			    handler : function() {
				    self.addNode(menu.currentNode, true);
			    }
		    }, {
			    id : 'modifySpeechModel',
			    text : '修改模型',
			    iconCls : 'icon-wordclass-edit',
			    width : 50,
			    handler : function() {
				    if (menu.currentNode.id == 1 || menu.currentNode.id == 2) {
					    Ext.Msg.alert('错误', '根节点禁止修改');
					    return;
				    }
				    self.modifyNode(menu.currentNode);
			    }
		    }, {
			    id : 'deleteSpeechModel',
			    text : '删除模型',
			    iconCls : 'icon-wordclass-del',
			    width : 50,
			    handler : function() {
				    self.deleteNode(menu.currentNode);
			    }
		    }, '-', {
			    id : 'importSpeechModel',
			    text : '导入模型数据',
			    iconCls : 'icon-ontology-import',
			    width : 50,
			    handler : function() {
				    self.importData(menu.currentNode);
			    }
		    }, {
			    text : '导出模型数据',
			    iconCls : 'icon-ontology-export',
			    width : 50,
			    handler : function() {
				    self.exportSpeechModelData(menu.currentNode);
			    }
		    }, '-', {
			    text : '同步当前模型',
			    iconCls : 'icon-wordclass-root-sync',
			    width : 50,
			    handler : function() {
				    self.syncCurrent(menu.currentNode);
			    }
		    }, {
			    ref : 'synAllSpeechModel',
			    text : '同步所有模型',
			    iconCls : 'icon-wordclass-root-sync',
			    width : 50,
			    handler : function() {
				    self.syncAll();
			    }
		    }, {
			    text : '应用模型',
			    iconCls : 'icon-ok',
			    width : 50,
			    handler : function() {
				    self.applyCurrent(menu.currentNode);
			    }
		    }]
	  });

	this.menu = menu;

	this.on('contextmenu', function(node, event) {
		  if (node.attributes.id.length != 1) {
			  if (node.attributes.isModel == '1') {
				  Ext.getCmp('modifySpeechModel').setText("编辑模型");
				  Ext.getCmp('deleteSpeechModel').setText("删除模型");
				  self.menu.addChildSpeechCategory.hide();
				  Ext.getCmp('createSpeechModel').hide();
				  Ext.getCmp('importSpeechModel').hide();
			  } else {
				  Ext.getCmp('modifySpeechModel').setText("编辑分类");
				  Ext.getCmp('deleteSpeechModel').setText("删除分类");
				  self.menu.addChildSpeechCategory.show();
				  Ext.getCmp('createSpeechModel').show();
				  Ext.getCmp('importSpeechModel').show();
			  }
			  Ext.getCmp('modifySpeechModel').show();
			  Ext.getCmp('deleteSpeechModel').show();
			  self.menu.synAllSpeechModel.hide();
		  } else {
			  self.menu.synAllSpeechModel.show();
			  self.menu.addChildSpeechCategory.show();
			  Ext.getCmp('createSpeechModel').show();
			  Ext.getCmp('modifySpeechModel').hide();
			  Ext.getCmp('deleteSpeechModel').hide();
			  Ext.getCmp('importSpeechModel').show();
		  }

		  event.preventDefault();
		  menu.currentNode = node;
		  node.select();
		  if (node.id != 3) {
			  menu.showAt(event.getXY());
		  }
	  });
	var modelFileId = new Ext.form.TextField({
		  fieldLabel : '模型Id',
		  allowBlank : false,
		  name : 'modelId',
		  blankText : '请填写模型ID',
		  anchor : '95%'
	  });
	var modelFileContent = new Ext.form.TextArea({
		  name : "modelFileContent",
		  anchor : '100% 88%',
		  allowBlank : false,
		  blankText : '模型文件内容不能为空',
		  style : 'background:none repeat scroll 0 0 #E6E6E6;color:black;'
	  });
	this.modelFileId = modelFileId;
	this.modelFileContent = modelFileContent;
	var modelFileForm = new Ext.form.FormPanel({
		  region : 'center',
		  frame : false,
		  border : false,
		  labelWidth : 75,
		  height : '100%',
		  waitMsgTarget : true,
		  bodyStyle : 'padding : 3px 10px; background-color:' + sys_bgcolor,
		  items : [modelFileId, {
			    xtype : 'label',
			    fieldLabel : '模型文件内容'
		    }, modelFileContent],
		  tbar : [{
			    text : '保存',
			    iconCls : 'icon-wordclass-add',
			    handler : function() {
				    self.saveModelFile(this);
			    }
		    }, {
			    text : '模型预览',
			    iconCls : 'icon-statistic',
			    handler : function() {
				    self.openPreview();
			    }
		    }]
	  });
	this.modelFileForm = modelFileForm;

	var _modelFileStore = new Ext.data.Store({
		  autoLoad : true,
		  proxy : new Ext.data.HttpProxy({
			    url : 'speech-model!getModelFileId.action'
		    }),
		  reader : new Ext.data.JsonReader({
			    idProperty : 'modelId',
			    root : 'data',
			    fields : [{
				      name : 'modelId',
				      type : 'string'
			      }, {
				      name : 'modelFileName',
				      type : 'string'
			      }]
		    }),
		  writer : new Ext.data.JsonWriter()
	  });

	var _sm = new Ext.grid.CheckboxSelectionModel({});

	var modelFileGrid = new Ext.grid.GridPanel({
		  region : 'west',
		  border : false,
		  style : 'border-right: 1px solid ' + sys_bdcolor,
		  frame : false,
		  width : '30%',
		  store : _modelFileStore,
		  columnLines : true,
		  loadMask : true,
		  colModel : new Ext.grid.ColumnModel({
			    columns : [{
				      header : "模型Id",
				      dataIndex : 'modelId',
				      menuDisabled : true,
				      width : 3
			      }, {
				      header : '模型文件',
				      dataIndex : 'modelFileName',
				      menuDisabled : true,
				      width : 7
			      }]
		    }),
		  tbar : [{
			    text : '新增',
			    xtype : 'button',
			    iconCls : 'icon-table-add',
			    handler : function() {
				    modelFileGrid.getSelectionModel().clearSelections();
				    self.modelFileId.setValue('');
				    self.modelFileContent.setValue('');
				    self.isAddFile = true;
				    self.previewFileName = "";
			    }
		    }, {
			    text : '同步所有文件',
			    iconCls : 'icon-ontology-export',
			    handler : function() {
				    Ext.Ajax.request({
					      url : 'speech-model!syncAllModelFile.action',
					      success : function(form, action) {
						      var objRes = Ext.decode(form.responseText);
						      if (objRes.success) {
							      Dashboard.setAlert("同步成功");
						      } else {
							      Ext.Msg.alert("错误", objRes.message);
						      }
					      },
					      failure : function(response) {
						      Ext.Msg.alert("错误", response.responseText);
					      }
				      })
			    }
		    }, {
			    text : '删除',
			    iconCls : 'icon-table-delete',
			    handler : function() {
				    var res = modelFileGrid.getSelectionModel().getSelections();
				    if (!res || res.length < 1) {
					    Ext.Msg.alert('', '请选择模型文件');
					    return;
				    } else {
					    Ext.Ajax.request({
						      url : 'speech-model!deleteModelFile.action',
						      params : {
							      modelId : res[0].data.modelId
						      },
						      success : function(form, action) {
							      var objRes = Ext.decode(form.responseText);
							      if (objRes.success) {
								      self.modelFileGrid.getStore().reload();
								      modelFileGrid.getSelectionModel().clearSelections();
								      Dashboard.setAlert("删除成功");
								      self.modelFileId.setValue('');
								      self.modelFileContent.setValue('');
								      self.isAddFile = true;
								      self.previewFileName = "";
							      } else {
								      Ext.Msg.alert("错误", objRes.message);
							      }
						      },
						      failure : function(response) {
							      Ext.Msg.alert("错误", response.responseText);
						      }
					      })
				    }
			    },
			    scope : this
		    }],
		  sm : new Ext.grid.RowSelectionModel({
			    singleSelect : true,
			    listeners : {
				    'rowselect' : function(sm, rowIndex, record) {
					    if (typeof record.get("modelId") !== 'undefined') {
						    self.isAddFile = false;
						    self.previewFileName = record.get("modelFileName");
						    Ext.Ajax.request({
							      url : 'speech-model!getMoelFileInfo.action',
							      params : {
								      modelId : record.get("modelId")
							      },
							      success : function(form, action) {
								      var objRes = Ext.decode(form.responseText);
								      if (objRes.success) {
									      var data = objRes.data;
									      self.modelFileId.setValue(data[0]);
									      self.modelFileContent.setValue(data[1]);
								      } else {
									      Ext.Msg.alert("错误", objRes.message);
								      }
							      },
							      failure : function(response) {
								      Ext.Msg.alert("错误", response.responseText);
							      }
						      })
					    }
				    }
			    }
		    }),
		  viewConfig : {
			  forceFit : true,
			  scrollOffset : 0
		  }
	  });
	this.modelFileGrid = modelFileGrid;

	this.isAddFile = true;
	this.previewFileName = "";

	var modelFileWin = new Ext.Window({
		  width : 800,
		  height : 520,
		  title : '模型文件管理',
		  defaults : {
			  border : false
		  },
		  modal : false,
		  shim : true,
		  closable : true,
		  closeAction : 'hide',
		  collapsible : true,
		  resizable : true,
		  draggable : true,
		  animCollapse : true,
		  maximizable : true,
		  constrainHeader : true,
		  layout : 'border',
		  items : [modelFileGrid, modelFileForm]
	  });
	this.modelFileWin = modelFileWin;

	var _treeCombo = new TreeComboBox({
		  fieldLabel : '父级分类',
		  emptyText : '请选择父级分类',
		  editable : false,
		  anchor : '95%',
		  allowBlank : false,
		  minHeight : 250,
		  name : 'parentCategoryId',
		  root : {
			  id : '0',
			  text : 'root',
			  iconCls : 'icon-ontology-root',
			  children : [{
				    id : '1',
				    text : "语料模型",
				    'isModel' : 0
			    }]
		  },
		  loader : new Ext.tree.TreeLoader({
			    dataUrl : 'speech-model-category!list.action'
		    })
	  });
	this.treeCombo = _treeCombo;
	this.frequency = new Ext.form.NumberField({
		  fieldLabel : '权重',
		  name : 'frequency',
		  blankText : '请输入重复值',
		  anchor : '95%'
	  });
	this.modelId = new Ext.form.TextField({
		  fieldLabel : '模型Id',
		  allowBlank : true,
		  name : 'modelId',
		  blankText : '请填写模型ID',
		  anchor : '95%'
	  });
	var _formPanel = new Ext.form.FormPanel({
		  frame : false,
		  border : false,
		  labelWidth : 75,
		  autoHeight : true,
		  waitMsgTarget : true,
		  bodyStyle : 'padding : 3px 10px; background-color:' + sys_bgcolor,
		  items : [new Ext.form.Hidden({
			      name : 'categoryId'
		      }), new Ext.form.Hidden({
			      name : 'bh'
		      }), new Ext.form.Hidden({
			      name : 'isModel'
		      }), _treeCombo, new Ext.form.TextField({
			      fieldLabel : '名称',
			      allowBlank : false,
			      name : 'name',
			      blankText : '请输入类型名称',
			      anchor : '95%'
		      }), self.modelId, self.frequency],
		  tbar : [{
			    text : '保存',
			    iconCls : 'icon-wordclass-add',
			    handler : function() {
				    self.saveNode(this);
			    }
		    }]
	  });
	var _formWin = new Ext.Window({
		  width : 495,
		  title : '类型管理',
		  defaults : {
			  border : false
		  },
		  modal : true,
		  plain : true,
		  shim : true,
		  closable : true,
		  closeAction : 'hide',
		  collapsible : true,
		  resizable : false,
		  draggable : true,
		  animCollapse : true,
		  constrainHeader : true,
		  listeners : {
			  'beforehide' : function(p) {
				  p.formPanel.getForm().reset();
			  }
		  },
		  items : [_formPanel],
		  showPubSelector : function(show) {
			  if (show)
				  this.pubSchemaSelector.show()
			  else
				  this.pubSchemaSelector.hide()
		  }.dg(this)
	  });
	var _fileForm = new Ext.FormPanel({
		  layout : "fit",
		  frame : true,
		  border : false,
		  autoHeight : true,
		  waitMsgTarget : true,
		  defaults : {
			  bodyStyle : 'padding:10px'
		  },
		  margins : '0 0 0 0',
		  labelAlign : "left",
		  labelWidth : 50,
		  fileUpload : true,
		  items : [{
			    xtype : 'fieldset',
			    title : '选择文件',
			    autoHeight : true,
			    items : [{
				      ref : '../speechModelUploadFile',
				      name : 'uploadFile',
				      xtype : "textfield",
				      fieldLabel : '文件',
				      inputType : 'file',
				      anchor : '96%'
			      }]
		    }, {
			    xtype : 'fieldset',
			    title : '设置参数',
			    autoHeight : true,
			    items : [{
				      name : 'cleanTypeAndData',
				      xtype : 'checkbox',
				      hideLabel : true,
				      boxLabel : '是否清空子分类及相关词类名',
				      inputValue : true,
				      checked : true
			      }]
		    }]
	  });
	var _importPanel = new Ext.Panel({
		  layout : "fit",
		  layoutConfig : {
			  animate : true
		  },
		  items : [_fileForm],
		  buttons : [{
			    ref : "btnImportModel",
			    text : "开始导入",
			    handler : function() {
				    if (!_fileForm.speechModelUploadFile.getValue()) {
					    Ext.Msg.alert("提示", "请选择要上传的文件");
				    } else {
					    this.disable();
					    var btn = this;
					    // 开始导入
					    var keyclassForm = self.fileForm.getForm();
					    var typePath = self.modelType.id.length != 1 ? self.modelType.getPath("text") : '语音模型导入';
					    if (keyclassForm.isValid()) {
						    keyclassForm.submit({
							      params : {
								      'speechModelCategoryId' : (self.modelType == null || self.modelType.id.length == 1) ? null : self.modelType.id,
								      'weighing' : self.modelType.attributes.weighing,
								      'importType' : self.importType,
								      'typepath' : typePath
							      },
							      url : 'speech-model-category!importSpeechModel.action',
							      success : function(form, action) {
								      btn.enable();
								      var result = action.result.data;
								      var timer = new ProgressTimer({
									        initData : result,
									        progressId : 'importSpeechModelStatus',
									        boxConfig : {
										        title : '正在导入语音模型...'
									        },
									        finish : function(p, response) {
										        if (p.currentCount == -2) {
											        // 下载验证失败的信息
											        if (!self.downloadIFrame) {
												        self.downloadIFrame = self.getEl().createChild({
													          tag : 'iframe',
													          style : 'display:none;'
												          })
											        }
											        self.downloadIFrame.dom.src = "speech-model-category!downExportFile.action?type="
											        self.importType + "&_t=" + new Date().getTime();
										        }
										        self.refresh(self.modelType);
									        }
								        });
								      timer.start();
							      },
							      failure : function(form, action) {
								      btn.enable();
								      Ext.MessageBox.hide();
								      Ext.Msg.alert('错误', '导入初始化失败。' + (action.result.message ? "详细错误:" + action.result.message : ''));
							      }
						      });
					    }
				    }
			    }
		    }, {
			    text : "关闭",
			    handler : function() {
				    self.importKeywordWin.hide();
			    }
		    }]
	  });

	this.importPanel = _importPanel;

	var importKeywordWin = new Ext.Window({
		  width : 400,
		  title : '语音模型导入',
		  defaults : {// 表示该窗口中所有子元素的特性
			  border : false
			  // 表示所有子元素都不要边框
		  },
		  plain : true,// 方角 默认
		  modal : true,
		  plain : true,
		  shim : true,
		  closeAction : 'hide',
		  collapsible : true,// 折叠
		  closable : true, // 关闭
		  resizable : false,// 改变大小
		  draggable : true,// 拖动
		  minimizable : false,// 最小化
		  maximizable : false,// 最大化
		  animCollapse : true,
		  constrainHeader : true,
		  autoHeight : false,
		  items : [_importPanel]
	  });
	this.fileForm = _fileForm;
	this.importKeywordWin = importKeywordWin;
	_formWin.formPanel = _formPanel;
	this.formWin = _formWin;

	this.on('click', function(n) {
		  this.modelType = n;
		  if (!n)
			  return false;
		  this.modelPanel = self.showTab(SpeechModelPanel, (n.attributes.isModel != 1 ? "speechModelTab" : 'speech-model-tab' + n.id), n.text, (n.attributes.isModel == 1
		      ? 'icon-wordclass'
		      : 'icon-speechcagory'), true);
		  this.modelPanel.countModel(n.id);
		  this.modelPanel.loadModels(n.id, n.attributes.isModel);
	  })
}

Ext.extend(SpeechModelNavPanel, Ext.tree.TreePanel, {
	  showModelTab : function(tabId, path) {
		  var self = this;
		  this.expandPath('/0/' + path, 'id', function() {
			    var node = self.getRootNode().findChild('id', tabId, true);
			    self.fireEvent('click', node);
		    });
	  },
	  endEdit : function() {
		  this.formWin.hide();
		  this.formWin.formPanel.getForm().reset();
	  },
	  isRoot : function(node) {
		  return node.id.length == 1;
	  },
	  saveNode : function(btn) {
		  btn.disable();
		  var f = this.formWin.formPanel.getForm();
		  if (!f.isValid()) {
			  Ext.Msg.alert('提示', '您尚未正确完整的输入表单内容，请检查！');
			  btn.enable();
			  return false;
		  }
		  this.data = f.getValues();
		  if (this.data.isModel == '1' && !this.data.modelId) {
			  Ext.Msg.alert('', '请填写模型ID');
			  btn.enable();
			  return
		  }
		  this.data.parentCategoryId = this.treeCombo.getValue();
		  if (this.data.parentCategoryId.length == 1) {
			  this.data.parentCategoryId = null;
		  }
		  if (!this.data.categoryId)
			  this.data.categoryId = null;
		  var self = this;
		  Ext.Ajax.request({
			    url : 'speech-model-category!save.action',
			    params : {
				    data : Ext.encode(self.data)
			    },
			    success : function(form, action) {
				    Dashboard.setAlert("保存成功");
				    self.endEdit();
				    self.updateNode(Ext.decode(form.responseText));
				    btn.enable();
			    },
			    failure : function(response) {
				    Ext.Msg.alert("错误", response.responseText);
				    btn.enable();
			    }
		    })
	  },
	  addNode : function(node, isModel) {
		  var rec = new Ext.data.Record(Ext.apply({}, node.attributes));
		  if (isModel) {
			  this.frequency.show();
			  this.modelId.show();
			  rec.set('frequency', rec.get('weighing') == 0 ? 1 : rec.get('weighing'));
			  rec.set('isModel', isModel ? "1" : "0");
		  } else {
			  this.frequency.hide();
			  this.modelId.hide();
			  rec.set('frequency', "0");
			  rec.set('isModel', isModel ? "1" : "0");
		  }
		  this.treeCombo.reload();
		  var fp = this.formWin.formPanel;
		  fp.getForm().reset();
		  fp.getForm().loadRecord(rec);
		  this.formWin.show();
		  this.treeCombo.setValueEx(node);
	  },
	  modifyNode : function(node) {
		  var isModel = node.attributes.isModel;
		  var fp = this.formWin.formPanel;
		  fp.getForm().reset();
		  var rec = new Ext.data.Record(Ext.apply({}, node.attributes));
		  rec.set('name', rec.get('text'));
		  rec.set('categoryId', rec.get('id'));
		  if (isModel == '1') {
			  this.frequency.show();
			  this.modelId.show();
			  this.modelId.setValue('');
			  rec.set('frequency', rec.get('weighing'));
			  rec.set('isModel', isModel);
		  } else {
			  this.frequency.hide();
			  this.modelId.hide();
			  rec.set('frequency', "0");
			  rec.set('isModel', 0);
		  }
		  this.treeCombo.reload();
		  fp.getForm().loadRecord(rec);
		  this.formWin.show();
		  this.treeCombo.setValueEx(node.parentNode);
	  },
	  deleteNode : function(node) {
		  var self = this;
		  var modelPanel = this.modelPanel
		  if (node.id.length == 1) {
			  Ext.Msg.alert('提示', '根节点不能删除！');
			  return;
		  }
		  Ext.Msg.confirm('', '确定要删除"' + node.getPath('text').replace('/root', '') + '"及以下所有分类吗？', function(btn, text) {
			    if (btn == 'yes') {
				    Ext.Ajax.request({
					      url : 'speech-model-category!del.action',
					      params : {
						      'categoryId' : node.id
					      },
					      success : function(response) {
						      var node4Sel = node.nextSibling ? node.nextSibling : node.previousSibling;
						      if (!node4Sel) {
							      node4Sel = node.parentNode;
							      node4Sel.leaf = true;
						      }
						      node4Sel.select();
						      if (typeof(modelPanel) != 'undefined' && modelPanel != '') {
							      modelPanel.loadModels(node.parentNode.id, node.attributes.isModel)
						      }
						      self.closeTab(node.attributes.isModel != 1 ? "speechModelTab" : 'speech-model-tab' + node.id);
						      node.parentNode.removeChild(node);
					      },
					      failure : function(response) {
						      Ext.Msg.alert("错误", response.responseText);
					      }
				      });
			    }
		    });
	  },
	  refresh : function(node) {
		  if (node.isExpanded() && !node.attributes.leaf)
			  node.reload();
		  else
			  node.expand();
	  },
	  getSelectedNode : function() {
		  return this.getSelectionModel().getSelectedNode();
	  },
	  updateNode : function(data) {
		  var parentCateId = this.data.parentCategoryId;
		  var parentNode;
		  if (parentCateId == null)
			  parentNode = this.getRootNode().childNodes[0];
		  else
			  parentNode = this.getNodeById(parentCateId);
		  if (!data.saveOrUpdate) {
			  var selNode = this.getSelectedNode();
			  selNode.setText(data.text);
			  selNode.attributes.weighing = data.weighing;
			  selNode.attributes.isModel = data.isModel;
			  if ('1' == data.isModel)
				  selNode.attributes.modelId = data.modelId;
			  if (selNode.parentNode.id != parentNode.id) {
				  var parentBefore = selNode.parentNode;
				  if (parentNode) {
					  selNode.leaf = data.leaf;
					  // this.setNodeIconCls(selNode, data.iconCls);
					  selNode.attributes.bh = data.bh;
					  parentNode.leaf = false;
					  parentNode.appendChild(selNode);
				  } else {
					  parentBefore.removeChild(selNode);
				  }
				  if (parentBefore.childNodes.length == 0)
					  parentBefore.leaf = true;
			  }
		  } else {
			  if (parentNode) {
				  var node = new Ext.tree.TreeNode({
					    id : data.id,
					    iconCls : data.iconCls,
					    cls : data.cls,
					    leaf : true,
					    text : data.text,
					    bh : data.bh,
					    isModel : data.isModel,
					    weighing : data.weighing,
					    modelId : '1' == data.isModel ? data.modelId : ''
				    });
				  if (parentNode.leaf)
					  parentNode.leaf = false
				  parentNode.appendChild(node);
				  parentNode.expand();
				  node.select();
			  }
		  }
	  },
	  setNodeIconCls : function(node, clsName) {
		  var iel = node.getUI().getIconEl();
		  if (iel) {
			  var el = Ext.get(iel);
			  if (el) {
				  if (clsName.indexOf('-root') > 0)
					  el.removeClass('icon-wordclass-category');
				  else
					  el.removeClass('icon-wordclass-root');
				  el.addClass(clsName);
			  }
		  }
	  },

	  searchNode : function(focusTarget) {
		  var categoryName = categoryName = this.getTopToolbar().getComponent(0).getValue();
		  if (!categoryName || !(categoryName = categoryName.trim())) {
			  Ext.Msg.alert('', '请输入分类名称');
			  return;
		  }
		  var self = this;
		  Ext.Ajax.request({
			    url : 'speech-model-category-search!search.action',
			    params : {
				    'categoryName' : categoryName
			    },
			    success : function(form, action) {
				    if (!form.responseText) {
					    Dashboard.setAlert("没有查找到更多的\"" + categoryName + "\",请修改后重新查找");
					    return;
				    }
				    var obj = Ext.util.JSON.decode(form.responseText);
				    var idPath = obj.pathId;
				    if (idPath) {
					    var targetId = idPath.pop();
					    self.expandPath('/0/1/' + idPath.join('/'), '', function(suc, n) {
						      if (targetId) {
							      self.getNodeById(targetId).select();
							      if (focusTarget)
								      focusTarget.focus()
						      }
					      });
				    }
			    },
			    failure : function(form, action) {
				    Ext.Msg.alert('错误', '查找错误');
			    }
		    });
	  },

	  expandNode : function(node, bh) {
		  var self = this;
		  if (node.attributes.bh == bh || node.leaf) {
			  this.getSelectionModel().select(node);
			  return;
		  }
		  node.expand(false, true, function() {
			    var childs = node.childNodes;
			    for (var i = 0; i < childs.length; i++) {
				    if (bh.indexOf(childs[i].attributes.bh) != -1)
					    self.expandNode(childs[i], bh);
			    }
		    });
	  },
	  refreshNode : function() {
		  var node = this.getSelectedNode();
		  if (!node)
			  this.refresh(this.getRootNode());
		  else {
			  this.refresh(node);
		  }
	  },
	  importData : function(node) {
		  var self = this;
		  this.modelType = node;
		  self.importKeywordWin.show();
	  },
	  exportSpeechModelData : function(typeNode) {
		  var self = this;
		  var fileType = '';
		  Ext.Msg.show({
			    title : '导出文件类型',
			    msg : '请选择要导出文件格式',
			    buttons : {
				    yes : 'Excel07',
				    ok : 'Excel03',
				    cancel : true
			    },
			    fn : function(choice) {
				    if (choice == 'cancel')
					    return
				    choice == 'ok' ? fileType = '03' : '';
				    choice == 'yes' ? fileType = '07' : '';

				    Ext.Ajax.request({
					      url : 'speech-model-category!exportSpeechModel.action',
					      params : {
						      'categoryId' : typeNode.id.length == 1 ? null : typeNode.id,
						      'typePath' : (typeof(typeNode) == 'undefined' ? '' : typeNode.getPath("text")),
						      'filetype' : fileType
					      },
					      success : function(response) {
						      var result = Ext.util.JSON.decode(response.responseText);
						      if (result.success) {
							      var timer = new ProgressTimer({
								        initData : result.data,
								        progressId : 'exportSpeechModelStatus',
								        boxConfig : {
									        title : '正在导出语音模型...'
								        },
								        finish : function() {
									        if (!self.downloadIFrame) {
										        self.downloadIFrame = self.getEl().createChild({
											          tag : 'iframe',
											          style : 'display:none;'
										          })
									        }
									        self.downloadIFrame.dom.src = 'speech-model-category!downExportFile.action?_t=' + new Date().getTime();
								        }
							        });
							      timer.start();
						      } else {
							      Ext.Msg.alert('错误', '导出失败:' + result.message)
						      }
					      },
					      failure : function(response) {
						      Ext.Msg.alert('错误', '导出失败')
					      },
					      scope : this
				      });
			    }
		    });

	  },
	  syncAll : function() {
		  Ext.Ajax.request({
			    url : 'speech-model!syncAll.action',
			    success : function(resp) {
				    var timer = new ProgressTimer({
					      initData : Ext.decode(resp.responseText),
					      progressId : 'syncSpeechModel',
					      boxConfig : {
						      title : '同步所有模型'
					      }
				      });
				    timer.start();
			    },
			    failure : function(resp) {
				    Ext.Msg.alert('错误', resp.responseText);
			    }
		    });
	  },
	  syncCurrent : function(typeNode) {
		  Ext.Ajax.request({
			    params : {
				    'categoryId' : typeNode.id.length == 1 ? null : typeNode.id
			    },
			    url : 'speech-model!syncCurrent.action',
			    success : function(resp) {
				    var timer = new ProgressTimer({
					      initData : Ext.decode(resp.responseText),
					      progressId : 'syncSpeechModel',
					      boxConfig : {
						      title : '同步当前模型'
					      }
				      });
				    timer.start();
			    },
			    failure : function(resp) {
				    Ext.Msg.alert('错误', resp.responseText);
			    }
		    });
	  },
	  applyAll : function() {
		  Ext.Ajax.request({
			    url : 'speech-model!applyAll.action',
			    success : function(resp) {
				    var result = Ext.util.JSON.decode(resp.responseText);
				    if (result.success)
					    Dashboard.setAlert("应用成功");
				    else
					    Ext.Msg.alert("错误", result.message);
			    },
			    failure : function(resp) {
				    var fail = Ext.util.JSON.decode(resp.responseText);
				    Ext.Msg.alert('错误', fail.message);
			    }
		    });
	  },
	  applyCurrent : function(typeNode) {
		  Ext.Ajax.request({
			    params : {
				    'categoryId' : typeNode.id.length == 1 ? null : typeNode.id
			    },
			    url : 'speech-model!applyCurrent.action',
			    success : function(resp) {
				    var result = Ext.util.JSON.decode(resp.responseText);
				    if (result.success)
					    Dashboard.setAlert("应用成功");
				    else
					    Ext.Msg.alert("错误", result.message);
			    },
			    failure : function(resp) {
				    var fail = Ext.util.JSON.decode(resp.responseText);
				    Ext.Msg.alert('错误', fail.message);
			    }
		    });
	  },
	  saveModelFile : function(btn) {
		  var self = this;
		  var modelFForm = self.modelFileForm.getForm();
		  if (modelFForm.isValid()) {
			  var modelId = self.modelFileId.getValue();
			  var modelContent = self.modelFileContent.getValue();
			  if (modelId && modelContent) {
				  Ext.Ajax.request({
					    params : {
						    'modelId' : modelId,
						    'modelContent' : modelContent,
						    'isAddFile' : self.isAddFile
					    },
					    url : 'speech-model!saveModelFile.action',
					    success : function(resp) {
						    var result = Ext.util.JSON.decode(resp.responseText);
						    if (result.success) {
							    self.modelFileGrid.getStore().reload();
							    Dashboard.setAlert("保存成功");
						    } else
							    Ext.Msg.alert("错误", result.message);
					    },
					    failure : function(resp) {
						    var fail = Ext.util.JSON.decode(resp.responseText);
						    Ext.Msg.alert('错误', fail.message);
					    }
				    });
			  } else {
				  Ext.Msg.alert('错误', '模型文件编辑id或内容不能为空');
			  }
		  }
	  },
	  openPreview : function() {
		  var self = this;
		  if (self.previewFileName) {
			  window.open('filepath/' + self.previewFileName, 'self.previewFileName',
			    'height="100%", width="100%", top=0,left=0, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, status=no');
		  } else
			  Ext.Msg.alert('提示', '请选择预览的文件');
	  },
	  authName : 'rs.model'
  });

Dashboard.registerNav(SpeechModelNavPanel, 2);
