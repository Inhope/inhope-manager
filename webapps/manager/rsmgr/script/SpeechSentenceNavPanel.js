SpeechSentenceNavPanel = function(_cfg) {
	var self = this;
	this.sentenceclassType;
	this.data = {};
	this.sentencePanel = '';
	this.importType = '';

	var isReleaseDefaultVal = http_get("speech-sentence!getDefaultReleaseValue.action");

	var cfg = {
		title : '训练语料管理',
		width : 200,
		minSize : 175,
		maxSize : 400,
		xtype : 'treepanel',
		collapsible : true,
		rootVisible : false,
		lines : false,
		border : false,
		autoScroll : true,
		listeners : {},
		root : {
			id : '0',
			text : 'root',
			iconCls : 'icon-ontology-root',
			lazyLoad : true,
			children : [{
				  id : '1',
				  text : "语音语料"
			  }]

		},
		dataUrl : 'speech-sentence-category!list.action',
		tbar : new Ext.Toolbar({
			  enableOverflow : true,
			  height : '20',
			  items : [{
				    blankText : "请输入分类名称",
				    xtype : 'textfield',
				    width : 120,
				    listeners : {
					    specialkey : function(f, e) {
						    if (e.getKey() == e.ENTER) {
							    self.searchNode(f);
						    }
					    }
				    }
			    }, {
				    text : '查找',
				    iconCls : 'icon-search',
				    width : 50,
				    handler : function() {
					    self.searchNode();
				    }
			    }, {
				    text : '校验语料',
				    iconCls : 'icon-status-notice',
				    width : 50,
				    handler : function() {
					    self.checkSentence();
				    }
			    }, {
				    text : '同步所有分类',
				    iconCls : 'icon-wordclass-root-sync',
				    width : 50,
				    handler : function() {
					    self.syncWin.show();
					    // self.SyncAllAI();
				    }
			    }, {
				    text : '语音训练',
				    iconCls : 'icon-ontology-sync',
				    width : 50,
				    handler : function() {
					    self.training();
				    }
			    }, {
				    text : '语音应用',
				    iconCls : 'icon-ok',
				    xtype : 'splitbutton',
				    width : 50,
				    menu : {
					    items : [{
						      text : '发布到语音云平台',
						      checked : (isReleaseDefaultVal == 'false') ? false : true,
						      ref : '../../releaseCloudCB',
						      disabled : ('admin' != Dashboard.u().getUsername() && !Dashboard.u().isSupervisor()),
						      checkHandler : function(btn, checked) {
							      this.releaseCloud = checked
						      }.dg(this)
					      }]
				    },
				    handler : function() {
					    self.apply(this.releaseCloud);
				    }.dg(this)
			    }, '-', {
				    text : '刷新',
				    iconCls : 'icon-refresh',
				    width : 50,
				    handler : function() {
					    self.refresh(self.getRootNode());
				    }
			    }]

		  })
	};

	SpeechSentenceNavPanel.superclass.constructor.call(this, Ext.applyIf(_cfg || {}, cfg));

	var menu = new Ext.menu.Menu({
		  items : [{
			    text : '刷新',
			    iconCls : 'icon-refresh',
			    width : 50,
			    handler : function() {
				    self.refreshNode();
			    }
		    }, {
			    id : 'addChildSpeechSentenceCategory',
			    text : '新建分类',
			    iconCls : 'icon-wordclass-add',
			    width : 50,
			    handler : function() {
				    self.addNode(menu.currentNode, false);
			    }
		    }, {
			    id : 'createSpeechSentenceclass',
			    text : '新建场景名称',
			    iconCls : 'icon-wordclass-add',
			    width : 50,
			    handler : function() {
				    self.addNode(menu.currentNode, true);
			    }
		    }, {
			    id : 'modifySpeechSentenceclass',
			    text : '修改分类',
			    iconCls : 'icon-wordclass-edit',
			    width : 50,
			    handler : function() {
				    if (menu.currentNode.id == 1) {
					    Ext.Msg.alert('错误提示', '根节点禁止修改');
					    return;
				    }
				    self.modifyNode(menu.currentNode);
			    }
		    }, {
			    id : 'deleteSpeechSentenceclass',
			    text : '删除分类',
			    iconCls : 'icon-wordclass-del',
			    width : 50,
			    handler : function() {
				    self.deleteNode(menu.currentNode);
			    }
		    }, '-', {
			    id : 'importSpeechSentenceclass',
			    text : '导入数据',
			    iconCls : 'icon-ontology-import',
			    width : 50,
			    handler : function() {
				    self.importData(menu.currentNode);
			    }
		    }, {
			    text : '导出数据',
			    iconCls : 'icon-ontology-export',
			    width : 50,
			    handler : function() {
				    self.exportSpeechSentenceclassData(menu.currentNode);
			    }
		    }, '-', {
			    id : 'synAllSpeechSentenceclass',
			    text : '同步所有分类',
			    iconCls : 'icon-wordclass-root-sync',
			    width : 50,
			    handler : function() {
				    self.SyncAllAI();
			    }
		    }]
	  });

	this.menu = menu;

	this.on('contextmenu', function(node, event) {
		  if (node.attributes.id.length != 1) {
			  if (node.attributes.isSentenceclass == 1) {
				  Ext.getCmp('modifySpeechSentenceclass').setText('修改场景名称');
				  Ext.getCmp('deleteSpeechSentenceclass').setText('删除场景名称');
				  Ext.getCmp('addChildSpeechSentenceCategory').hide();
				  Ext.getCmp('createSpeechSentenceclass').hide();
				  Ext.getCmp('importSpeechSentenceclass').hide();
			  } else {
				  Ext.getCmp('modifySpeechSentenceclass').setText('修改分类');
				  Ext.getCmp('deleteSpeechSentenceclass').setText('删除分类');
				  Ext.getCmp('addChildSpeechSentenceCategory').show();
				  Ext.getCmp('createSpeechSentenceclass').show();
				  Ext.getCmp('importSpeechSentenceclass').show();
			  }
			  Ext.getCmp('modifySpeechSentenceclass').show();
			  Ext.getCmp('deleteSpeechSentenceclass').show();
			  Ext.getCmp('synAllSpeechSentenceclass').hide();
		  } else {
			  Ext.getCmp('synAllSpeechSentenceclass').show();
			  Ext.getCmp('addChildSpeechSentenceCategory').show();
			  Ext.getCmp('createSpeechSentenceclass').show();
			  Ext.getCmp('modifySpeechSentenceclass').hide();
			  Ext.getCmp('deleteSpeechSentenceclass').hide();
			  Ext.getCmp('importSpeechSentenceclass').show();
		  }

		  event.preventDefault();
		  menu.currentNode = node;
		  node.select();
		  menu.showAt(event.getXY());
	  });

	this.on('beforeload', function(node, event) {
	  });

	var _treeCombo = new TreeComboBox({
		  fieldLabel : '父级语料',
		  emptyText : '请选择父级语料...',
		  editable : false,
		  anchor : '95%',
		  allowBlank : false,
		  minHeight : 250,
		  name : 'parentCategoryId',
		  root : {
			  id : '0',
			  text : 'root',
			  iconCls : 'icon-ontology-root',
			  children : [{
				    id : '1',
				    text : "语音语料"
			    }]
		  },
		  loader : new Ext.tree.TreeLoader({
			    dataUrl : 'speech-sentence-category!list.action'
		    })
	  });
	this.treeCombo = _treeCombo;
	this.weighing = new Ext.form.NumberField({
		  fieldLabel : '重复值',
		  name : 'weighing',
		  blankText : '请输入重复值',
		  anchor : '95%'
	  })
	var _formPanel = new Ext.form.FormPanel({
		  frame : false,
		  border : false,
		  labelWidth : 75,
		  autoHeight : true,
		  waitMsgTarget : true,
		  bodyStyle : 'padding : 3px 10px; background-color:' + sys_bgcolor,
		  items : [new Ext.form.Hidden({
			      name : 'categoryId'
		      }), new Ext.form.Hidden({
			      name : 'bh'
		      }), new Ext.form.Hidden({
			      name : 'isSentenceclass'
		      }), _treeCombo, new Ext.form.TextField({
			      fieldLabel : '类型名称',
			      allowBlank : false,
			      name : 'name',
			      blankText : '请输入类型名称',
			      anchor : '95%'
		      }), self.weighing],
		  tbar : [{
			    text : '保存',
			    iconCls : 'icon-wordclass-add',
			    handler : function() {
				    self.saveNode(this);
			    }
		    }]
	  });
	var syncStore = new Ext.data.ArrayStore({
		  fields : ['key', 'value'],
		  idIndex : 0
	  });
	var seleMode = new Ext.grid.RowSelectionModel();
	this.seleMode = seleMode;
	var syncPramaterPanel = new Ext.grid.EditorGridPanel({
		  height : 400,
		  flex : 3,
		  style : 'border-right: 1px solid ' + sys_bdcolor,
		  border : false,
		  clicksToEdit : 1,
		  frame : false,
		  columnLines : true,
		  selModel : this.seleMode,
		  pasteable : true,
		  colModel : new Ext.grid.ColumnModel({
			    columns : [new Ext.form.Hidden({
				        dataIndex : 'isEdit',
				        width : 0,
				        hidden : true
			        }), {
				      header : 'Key',
				      dataIndex : 'key',
				      width : 5,
				      editor : new Ext.form.TextField()
			      }, {
				      header : 'Value',
				      dataIndex : 'value',
				      width : 5,
				      editor : new Ext.form.TextField()
			      }]
		    }),
		  store : syncStore,
		  viewConfig : {
			  forceFit : true
		  },
		  listeners : {
			  afteredit : function(e) {
				  e.record.set('isEdit', 1);
			  }
		  }
	  });
	this.syncPramaterPanel = syncPramaterPanel;
	var syncWin = new Ext.Window({
		  width : 495,
		  height : 400,
		  title : '同步语料',
		  defaults : {
			  border : false
		  },
		  modal : true,
		  plain : true,
		  shim : true,
		  closable : true,
		  closeAction : 'hide',
		  collapsible : true,
		  resizable : true,
		  draggable : true,
		  animCollapse : true,
		  constrainHeader : true,
		  items : [syncPramaterPanel],
		  showPubSelector : function(show) {
			  if (show)
				  this.pubSchemaSelector.show()
			  else
				  this.pubSchemaSelector.hide()
		  }.dg(this),
		  tbar : [{
			    text : '删除',
			    iconCls : 'icon-table-delete',
			    handler : function() {
				    var self = this;
				    var selections = self.seleMode.getSelections();
				    if (selections.length == 0) {
					    Ext.Msg.alert('提示', '请先选择要删除的记录！');
					    return false;
				    }
				    Ext.each(selections, function(item) {
					      syncStore.remove(item);
				      });
			    },
			    scope : this
		    }, {
			    text : '新增',
			    xtype : 'button',
			    iconCls : 'icon-table-add',
			    handler : function() {
				    var s = syncPramaterPanel.getStore();
				    s.add(new s.recordType({}, Ext.id()));
			    }
		    }, {
			    text : '开始同步',
			    iconCls : 'icon-ontology-export',
			    handler : function() {
				    self.SyncAllAI();
			    }
		    }]
	  });
	this.syncWin = syncWin;
	var _fileForm = new Ext.FormPanel({
		  layout : "fit",
		  frame : true,
		  border : false,
		  autoHeight : true,
		  waitMsgTarget : true,
		  defaults : {
			  bodyStyle : 'padding:10px'
		  },
		  margins : '0 0 0 0',
		  labelAlign : "left",
		  labelWidth : 50,
		  fileUpload : true,
		  items : [{
			    xtype : 'fieldset',
			    title : '选择文件',
			    autoHeight : true,
			    items : [{
				      id : 'speechSentenceUploadFileId',
				      name : 'uploadFile',
				      xtype : "textfield",
				      fieldLabel : '文件',
				      inputType : 'file',
				      anchor : '96%'
			      }]
		    }, {
			    xtype : 'fieldset',
			    title : '设置参数',
			    autoHeight : true,
			    items : [{
				      name : 'cleanTypeAndData',
				      xtype : 'checkbox',
				      hideLabel : true,
				      boxLabel : '是否清空子分类及相关语料',
				      inputValue : true,
				      checked : true
			      }]
		    }]
	  });
	var _formWin = new Ext.Window({
		  width : 495,
		  title : '类别编辑',
		  defaults : {
			  border : false
		  },
		  modal : true,
		  plain : true,
		  shim : true,
		  closable : true,
		  closeAction : 'hide',
		  collapsible : true,
		  resizable : false,
		  draggable : true,
		  animCollapse : true,
		  constrainHeader : true,
		  listeners : {
			  'beforehide' : function(p) {
				  p.formPanel.getForm().reset();
			  }
		  },
		  items : [_formPanel],
		  showPubSelector : function(show) {
			  if (show)
				  this.pubSchemaSelector.show()
			  else
				  this.pubSchemaSelector.hide()
		  }.dg(this)
	  });
	var _fileForm = new Ext.FormPanel({
		  layout : "fit",
		  frame : true,
		  border : false,
		  autoHeight : true,
		  waitMsgTarget : true,
		  defaults : {
			  bodyStyle : 'padding:10px'
		  },
		  margins : '0 0 0 0',
		  labelAlign : "left",
		  labelWidth : 50,
		  fileUpload : true,
		  items : [{
			    xtype : 'fieldset',
			    title : '选择文件',
			    autoHeight : true,
			    items : [{
				      id : 'speechSentenceUploadFileId',
				      name : 'uploadFile',
				      xtype : "textfield",
				      fieldLabel : '文件',
				      inputType : 'file',
				      anchor : '96%'
			      }]
		    }, {
			    xtype : 'fieldset',
			    title : '设置参数',
			    autoHeight : true,
			    items : [{
				      name : 'cleanTypeAndData',
				      xtype : 'checkbox',
				      hideLabel : true,
				      boxLabel : '是否清空子分类及相关语料',
				      inputValue : true,
				      checked : true
			      }, {
				      name : 'addReplySentence',
				      xtype : 'checkbox',
				      hideLabel : true,
				      boxLabel : '是否叠加导入重复语料',
				      inputValue : true,
				      checked : false
			      }]
		    }]
	  });
	var trainExceptionTextarea = new Ext.form.TextArea({
		  name : "trainException",
		  width : 800,
		  height : 500,
		  readOnly : true,
		  style : 'background:none repeat scroll 0 0 #E6E6E6;color:black;'
	  });
	var trainExceptionWindow = new Ext.Window({
		  title : "训练消息提示",
		  closable : true,
		  autoWidth : true,
		  autoHeight : true,
		  closeAction : 'hide',
		  plain : true,
		  items : [trainExceptionTextarea],
		  listeners : {
			  'show' : function() {
				  this.center(); // 屏幕居中
			  }
		  }
	  });
	this.trainExceptionTextarea = trainExceptionTextarea;
	this.trainExceptionWindow = trainExceptionWindow;

	var exceptionTextarea = new Ext.form.TextArea({
		  name : "exception",
		  width : 800,
		  height : 500,
		  readOnly : true,
		  style : 'background:none repeat scroll 0 0 #E6E6E6;color:black;'
	  });
	var exceptionWindow = new Ext.Window({
		  title : "验证失败",
		  closable : true,
		  autoWidth : true,
		  autoHeight : true,
		  closeAction : 'hide',
		  plain : true,
		  items : [exceptionTextarea],
		  listeners : {
			  'show' : function() {
				  this.center(); // 屏幕居中
			  }
		  }
	  });
	this.exceptionTextarea = exceptionTextarea;
	this.exceptionWindow = exceptionWindow;
	var _importPanel = new Ext.Panel({
		  layout : "fit",
		  layoutConfig : {
			  animate : true
		  },
		  items : [_fileForm],
		  buttons : [{
			    id : "btn_import_speechsentenceclass",
			    text : "开始导入",
			    handler : function() {
				    if (!Ext.getCmp("speechSentenceUploadFileId").getValue()) {
					    Ext.Msg.alert("错误提示", "请选择你要上传的文件");
				    } else {
					    this.disable();
					    var btn = this;
					    // 开始导入
					    var keyclassForm = self.fileForm.getForm();
					    var typePath = self.sentenceclassType.id.length != 1 ? self.sentenceclassType.getPath("text") : "语音语料导入";
					    if (keyclassForm.isValid()) {
						    keyclassForm.submit({
							      params : {
								      'speechsentenceCategoryId' : (self.sentenceclassType == null || self.sentenceclassType.id.length == 1) ? null : self.sentenceclassType.id,
								      'importType' : self.importType,
								      'typepath' : typePath
							      },
							      url : 'speech-sentence-category!importSpeechSentenceclass.action',
							      success : function(form, action) {
								      btn.enable();
								      var result = action.result.data;
								      var timer = new ProgressTimer({
									        initData : result,
									        progressId : 'importSentenceclassStatus',
									        boxConfig : {
										        title : '正在导入语音语料...'
									        },
									        finish : function(p, response) {
										        if (p.currentCount == -2) {
											        Ext.Msg.hide();
											        var message = Ext.decode(response.responseText).message
											        exceptionTextarea.setValue(message);
											        exceptionWindow.setTitle('验证失败');
											        exceptionWindow.show();
										        } else if (p.currentCount == -3) {
											        // 下载验证失败的信息
											        if (!self.downloadIFrame) {
												        self.downloadIFrame = self.getEl().createChild({
													          tag : 'iframe',
													          style : 'display:none;'
												          })
											        }
											        self.downloadIFrame.dom.src = "speech-sentence-category!downExportFile.action?type="
											        self.importType + "&_t=" + new Date().getTime();
										        }
										        self.refresh(self.sentenceclassType);
									        }
								        });
								      timer.start();
							      },
							      failure : function(form, action) {
								      btn.enable();
								      Ext.MessageBox.hide();
								      Ext.Msg.alert('错误', '导入初始化失败.' + (action.result.message ? '详细错误:' + action.result.message : ''));
							      }
						      });
					    }
				    }
			    }
		    }, {
			    text : "关闭",
			    handler : function() {
				    self.importSentenceWin.hide();
			    }
		    }]
	  });

	var importSentenceWin = new Ext.Window({
		  width : 450,
		  title : '语音语料导入',
		  defaults : {// 表示该窗口中所有子元素的特性
			  border : false
			  // 表示所有子元素都不要边框
		  },
		  plain : true,// 方角 默认
		  modal : true,
		  plain : true,
		  shim : true,
		  closeAction : 'hide',
		  collapsible : true,// 折叠
		  closable : true, // 关闭
		  resizable : false,// 改变大小
		  draggable : true,// 拖动
		  minimizable : false,// 最小化
		  maximizable : false,// 最大化
		  animCollapse : true,
		  constrainHeader : true,
		  autoHeight : false,
		  items : [_importPanel]
	  });
	this.fileForm = _fileForm;
	this.importSentenceWin = importSentenceWin;
	_formWin.formPanel = _formPanel;
	this.formWin = _formWin;

	this.on('click', function(n) {
		  this.sentenceclassType = n;
		  if (!n)
			  return false;
		  this.sentencePanel = self.showTab(SpeechSentencePanel, (n.attributes.isSentenceclass != 1 ? "speechSentencesTab" : 'speech-sentence-tab-' + n.id), n.text,
		    (n.attributes.isSentenceclass == 1 ? 'icon-wordclass' : 'icon-speechcagory'), true);
		  this.sentencePanel.countSentencesclass(n.id);
		  this.sentencePanel.loadSentences(n.id, n.attributes.isSentenceclass);
		  this.sentencePanel.navPanel = this;
	  })
}

Ext.extend(SpeechSentenceNavPanel, Ext.tree.TreePanel, {
	  showSentenceTab : function(tabId, path) {
		  var self = this;
		  this.expandPath('/0/1' + path, 'id', function() {
			    var node = self.getRootNode().findChild('id', tabId, true);
			    self.fireEvent('click', node);
		    });
	  },
	  endEdit : function() {
		  this.formWin.hide();
		  this.formWin.formPanel.getForm().reset();
	  },
	  isRoot : function(node) {
		  return node.id.length == 1;
	  },
	  saveNode : function(btn) {
		  btn.disable();
		  var f = this.formWin.formPanel.getForm();
		  if (!f.isValid()) {
			  Ext.Msg.alert('提示', '您尚未正确完整的输入表单内容，请检查！');
			  btn.enable();
			  return false;
		  }
		  this.data = f.getValues();
		  this.data.parentCategoryId = this.treeCombo.getValue();
		  if (this.data.parentCategoryId.length == 1) {
			  this.data.parentCategoryId = null;
		  }
		  if (!this.data.categoryId)
			  this.data.categoryId = null;
		  var self = this;
		  Ext.Ajax.request({
			    url : 'speech-sentence-category!save.action',
			    params : {
				    data : Ext.encode(self.data)
			    },
			    success : function(form, action) {
				    Dashboard.setAlert("保存成功!");
				    self.endEdit();
				    self.updateNode(Ext.decode(form.responseText));
				    btn.enable();
			    },
			    failure : function(response) {
				    Ext.Msg.alert("错误", response.responseText);
				    btn.enable();
			    }
		    })
	  },
	  _setPubDataUrl : function() {
		  if (!this.treeComboExpandL) {
			  this.treeCombo.on('expand', this.treeComboExpandL = function() {
				    if (this.treeCombo.loader.dataUrl != this.lastDataUrl) {
					    this.treeCombo.reload();
				    }
			    }, this)
		  }
		  this.lastDataUrl = this.treeCombo.loader.dataUrl
	  },
	  addNode : function(node, isSentenceclass) {
		  var rec = new Ext.data.Record(Ext.apply({}, node.attributes));
		  if (isSentenceclass) {
			  this.weighing.show();
			  rec.set('isSentenceclass', isSentenceclass ? "1" : "0");
		  } else {
			  this.weighing.hide();
			  rec.set('isSentenceclass', "0");
		  }
		  var fp = this.formWin.formPanel;
		  fp.getForm().loadRecord(rec);
		  this.formWin.show();
		  // if (this.isRoot(node))
		  // this.treeCombo.setReadOnly(true);
		  // else
		  // this.treeCombo.setReadOnly(false);
		  this.treeCombo.setValueEx(node);
		  var _chkGroup = this.formWin.formPanel.capChkGroup;
		  var _chkVals = {};
		  this._setPubDataUrl();
		  // _chkGroup.setValue(_chkVals);
	  },
	  modifyNode : function(node) {
		  this.formWin.show();
		  var isSentenceclass = node.attributes.isSentenceclass;
		  var fp = this.formWin.formPanel;
		  var rec = new Ext.data.Record(Ext.apply({}, node.attributes));
		  rec.set('name', rec.get('text'));
		  rec.set('categoryId', rec.get('id'));
		  if (isSentenceclass == '1') {
			  this.weighing.show();
			  rec.set('weighing', rec.get('weighing'));
		  } else {
			  this.weighing.hide();
			  rec.set('weighing', "0");
		  }
		  fp.getForm().loadRecord(rec);
		  // if (this.isRoot(node.parentNode))
		  // this.treeCombo.setReadOnly(true);
		  // else
		  // this.treeCombo.setReadOnly(false);
		  this.treeCombo.setValueEx(node.parentNode);
		  this._setPubDataUrl();
	  },
	  deleteNode : function(node) {
		  var self = this;
		  var sentencePanel = this.sentencePanel
		  if (node.id.length == 1) {
			  Ext.Msg.alert('警告', '根节点不能删除！');
			  return;
		  }
		  Ext.Msg.confirm('提示', '确定要删除&nbsp&nbsp' + node.getPath('text').replace('/root', '') + '&nbsp&nbsp及以下所有分类吗', function(btn, text) {
			    if (btn == 'yes') {
				    Ext.Ajax.request({
					      url : 'speech-sentence-category!del.action',
					      params : {
						      categoryId : node.id
					      },
					      success : function(response) {
						      var node4Sel = node.nextSibling ? node.nextSibling : node.previousSibling;
						      if (!node4Sel) {
							      node4Sel = node.parentNode;
							      node4Sel.leaf = true;
						      }
						      node4Sel.select();
						      if (typeof(sentencePanel) != 'undefined' && sentencePanel != '') {
							      sentencePanel.loadSentences(node.parentNode.id, node.attributes.isSentenceclass)
						      }
						      self.closeTab(node.attributes.isSentenceclass != 1 ? "speechSentencesTab" : 'speech-sentence-tab-' + node.id);
						      node.parentNode.removeChild(node);
					      },
					      failure : function(response) {
						      Ext.Msg.alert("错误", response.responseText);
					      }
				      });
			    }
		    });
	  },
	  refresh : function(node) {
		  if (node.isExpanded() && !node.attributes.leaf)
			  node.reload();
		  else
			  node.expand();
	  },
	  getSelectedNode : function() {
		  return this.getSelectionModel().getSelectedNode();
	  },
	  updateNode : function(data) {
		  var parentCateId = this.data.parentCategoryId;
		  var parentNode;
		  if (parentCateId == null) {
			  parentNode = this.getRootNode().childNodes[0];
		  } else {
			  parentNode = this.getNodeById(parentCateId);
		  }
		  if (!data.saveOrUpdate) {
			  var selNode = this.getSelectedNode();
			  selNode.setText(data.text);
			  selNode.attributes.isSentenceclass = data.isSentenceclass;
			  selNode.attributes.weighing = data.weighing;
			  if (selNode.parentNode.id != parentNode.id) {
				  var parentBefore = selNode.parentNode;
				  if (parentNode) {
					  selNode.leaf = data.leaf;
					  // this.setNodeIconCls(selNode, data.iconCls);
					  selNode.attributes.bh = data.bh;
					  selNode.attributes.isSentenceclass = data.isSentenceclass;
					  parentNode.leaf = false;
					  parentNode.appendChild(selNode);
				  } else {
					  parentBefore.removeChild(selNode);
				  }
				  if (parentBefore.childNodes.length == 0)
					  parentBefore.leaf = true;
			  }
		  } else {
			  if (parentNode) {
				  var node = new Ext.tree.TreeNode({
					    id : data.id,
					    iconCls : data.iconCls,
					    cls : data.cls,
					    leaf : true,
					    text : data.text,
					    bh : data.bh,
					    isSentenceclass : data.isSentenceclass,
					    weighing : data.weighing
				    });
				  if (parentNode.leaf)
					  parentNode.leaf = false
				  parentNode.appendChild(node);
				  parentNode.expand();
				  node.select();
			  }
		  }
	  },
	  setNodeIconCls : function(node, clsName) {
		  var iel = node.getUI().getIconEl();
		  if (iel) {
			  var el = Ext.get(iel);
			  if (el) {
				  if (clsName.indexOf('-root') > 0)
					  el.removeClass('icon-wordclass-category');
				  else
					  el.removeClass('icon-wordclass-root');
				  el.addClass(clsName);
			  }
		  }
	  },

	  searchNode : function(focusTarget) {
		  var categoryName = categoryName = this.getTopToolbar().getComponent(0).getValue();
		  if (!categoryName || !(categoryName = categoryName.trim())) {
			  Ext.Msg.alert('提示', '请输入分类名称');
			  return;
		  }
		  var self = this;
		  Ext.Ajax.request({
			    url : 'speech-sentence-category-search!search.action',
			    params : {
				    'categoryName' : categoryName
			    },
			    success : function(form, action) {
				    if (!form.responseText) {
					    Dashboard.setAlert('没有查找到更多的"' + categoryName + '"，请修改后重新查找');
					    return;
				    }
				    var obj = Ext.util.JSON.decode(form.responseText);
				    var idPath = obj.pathId;
				    if (idPath) {
					    var targetId = idPath.pop();
					    self.expandPath('/0/' + '1/' + idPath.join('/'), 'id', function(suc, n) {
						      self.getNodeById(targetId).select();
						      if (focusTarget)
							      focusTarget.focus()
					      });
				    }
			    },
			    failure : function(form, action) {
				    Ext.Msg.alert('提示', '查找错误');
			    }
		    });
	  },

	  expandNode : function(node, bh) {
		  var self = this;
		  if (node.attributes.bh == bh || node.leaf) {
			  this.getSelectionModel().select(node);
			  return;
		  }
		  node.expand(false, true, function() {
			    var childs = node.childNodes;
			    for (var i = 0; i < childs.length; i++) {
				    if (bh.indexOf(childs[i].attributes.bh) != -1)
					    self.expandNode(childs[i], bh);
			    }
		    });
	  },
	  refreshNode : function() {
		  var node = this.getSelectedNode();
		  if (!node)
			  this.refresh(this.getRootNode());
		  else {
			  this.refresh(node);
		  }
	  },
	  importData : function(node) {
		  var self = this;
		  this.sentenceclassType = node;
		  Ext.Msg.show({
			    title : '导入文件类型',
			    msg : '您选择要导入文件格式',
			    buttons : {
				    // no : 'ZIP压缩包',
				    yes : 'Excel',
				    cancel : true
			    },
			    fn : function(choice) {
				    if (choice == 'cancel')
					    return
				    choice == 'yes' ? self.importType = 'excel' : '';
				    // choice == 'no' ? self.importType = 'zip' : '';
				    self.importSentenceWin.show();
			    }
		    });
		  // 恢复导入按钮
		  Ext.getCmp("btn_import_speechsentenceclass").setDisabled(false);

	  },
	  exportSpeechSentenceclassData : function(typeNode) {
		  var self = this;
		  var fileType = '';
		  Ext.Msg.show({
			    title : '导出文件类型',
			    msg : '您选择要导出的文件类型',
			    buttons : {
				    no : 'ZIP',
				    yes : 'Excel07',
				    ok : 'Excel03',
				    cancel : true
			    },
			    fn : function(choice) {
				    if (choice == 'cancel')
					    return
				    choice == 'ok' ? fileType = '03' : '';
				    choice == 'yes' ? fileType = '07' : '';
				    choice == 'no' ? fileType = 'zip' : '';
				    Ext.Ajax.request({
					      url : 'speech-sentence-category!exportSpeechSentenceclass.action',
					      params : {
						      'sentenceclassCategoryId' : typeNode.id.length == 1 ? null : typeNode.id,
						      'typePath' : (typeof(typeNode) == 'undefined' ? '' : typeNode.getPath("text")),
						      'filetype' : fileType
					      },
					      success : function(response) {
						      var params = Ext.urlEncode({
							        'type' : fileType == 'zip' ? 'zip' : 'excel'
						        })
						      var result = Ext.util.JSON.decode(response.responseText);
						      if (result.success) {
							      var timer = new ProgressTimer({
								        initData : result.data,
								        progressId : 'exportSpeechSentenceclassStatus',
								        boxConfig : {
									        title : '正在导出语音语料...'
								        },
								        finish : function() {
									        if (!self.downloadIFrame) {
										        self.downloadIFrame = self.getEl().createChild({
											          tag : 'iframe',
											          style : 'display:none;'
										          })
									        }
									        self.downloadIFrame.dom.src = 'speech-sentence-category!downExportFile.action?' + params + '&_t=' + new Date().getTime();
								        }
							        });
							      timer.start();
						      } else {
							      Ext.Msg.alert('错误', '导出失败:' + result.message)
						      }
					      },
					      failure : function(response) {
						      Ext.Msg.alert('错误', '导出失败')
					      },
					      scope : this
				      });
			    }
		    });
	  },
	  checkSentence : function() {
		  var self = this;
		  Ext.Ajax.request({
			    url : 'speech-sentence!checkSentence.action',
			    success : function(resp) {
				    var timer = new ProgressTimer({
					      initData : Ext.decode(resp.responseText),
					      progressId : 'checkSentenceclassStatus',
					      boxConfig : {
						      title : '检查所有语料'
					      },
					      finish : function(p, response) {
						      if (p.currentCount == -1) {
							      Ext.Msg.hide();
							      var message = Ext.decode(response.responseText).message
							      self.exceptionTextarea.setValue(message);
							      self.exceptionWindow.setTitle('校验特殊语料结果');
							      self.exceptionWindow.show();
						      }
					      }
				      });
				    timer.start();
			    },
			    failure : function(resp) {
				    Ext.Msg.alert('错误', resp.responseText);
			    }
		    });
	  },
	  SyncAllAI : function() {
		  var resData = [];
		  var res = 0;
		  var isEmpty = false;
		  Ext.each(this.syncPramaterPanel.getStore().getRange(), function(record) {
			    res++;
			    var key = record.data["key"];
			    var value = record.data["value"];
			    var isEdit = record.data["isEdit"];
			    if ((!key || !value) && isEdit == 1) {
				    Ext.Msg.alert("错误", "第" + res + "行 " + "key/value为空");
				    isEmpty = true;
				    return;
			    }
			    if (isEdit == 1) {
				    resData.push(record.data);
			    }
		    });
		  if (isEmpty) {
			  return;
		  }
		  resData = Ext.encode(resData);
		  Ext.Ajax.request({
			    url : 'speech-sentence!syncAllAI.action',
			    params : {
				    data : resData
			    },
			    success : function(resp) {
				    var timer = new ProgressTimer({
					      initData : Ext.decode(resp.responseText),
					      progressId : 'syncSpeechSentence',
					      boxConfig : {
						      title : '同步所有语料'
					      }
				      });
				    timer.start();
			    },
			    failure : function(resp) {
				    Ext.Msg.alert('错误', resp.responseText);
			    }
		    });
	  },
	  training : function() {
		  var self = this;
		  Ext.Ajax.request({
			    url : 'speech-sentence!training.action',
			    success : function(resp) {
				    Ext.Ajax.request({
					      url : 'speech-sentence!getTrainProgress.action',
					      success : function(resp) {
						      var timer = new ProgressTimer({
							        initData : Ext.decode(resp.responseText),
							        progressId : 'trainingSpeechSentence',
							        progress : false,
							        boxConfig : {
								        title : '训练语料'
							        },
							        finish : function(p, response) {
								        if (p.currentCount == -1) {
									        Ext.Msg.hide();
									        var message = Ext.decode(response.responseText).message;
									        self.trainExceptionTextarea.setValue(message);
									        self.trainExceptionWindow.show();
								        }
							        }
						        });
						      timer.start();
					      },
					      failure : function(resp) {
						      Ext.Msg.alert('错误', resp.responseText);
					      }
				      });
			    },
			    failure : function(resp) {
				    Ext.Msg.alert('错误', resp.responseText);
			    }
		    });
	  },
	  apply : function(isReleaseCloud) {
		  var self = this;
		  Ext.Ajax.request({
			    params : {
				    'isReleaseCloud' : isReleaseCloud
			    },
			    url : 'speech-sentence!apply.action',
			    success : function(resp) {
				    var result = Ext.util.JSON.decode(resp.responseText);
				    if (result.success)
					    Dashboard.setAlert("应用成功");
				    else
					    Ext.Msg.alert("错误", result.message);
			    },
			    failure : function(resp) {
				    var fail = Ext.util.JSON.decode(resp.responseText);
				    Ext.Msg.alert('错误', fail.message);
			    }
		    });
	  },
	  authName : 'rs.sentence'
  });

Dashboard.registerNav(SpeechSentenceNavPanel, 1);
