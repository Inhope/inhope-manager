SpeechWordNavPanel = function(_cfg) {
	var self = this;
	this.wordclassType;
	this.data = {};
	this.isMini = null;
	this.wordPanel = '';
	this.importType = '';
	var cfg = {
		title : '语音词类管理',
		width : 200,
		minSize : 175,
		maxSize : 400,
		xtype : 'treepanel',
		collapsible : true,
		rootVisible : false,
		lines : false,
		border : false,
		autoScroll : true,
		listeners : {},
		root : {
			id : '0',
			text : 'root',
			iconCls : 'icon-ontology-root',
			lazyLoad : true,
			children : Dashboard.u().filterAllowed([{
						id : '1',
						authName : 'rs.word.speech',
						text : "语音词类",
						'isMini' : 0,
						'isWordclass' : 0
					}, {
						id : '2',
						authName : 'rs.word.mini',
						text : "迷你词类",
						'isMini' : 1,
						'isWordclass' : 0
					}, {
						id : '3',
						text : "系统词类",
						iconCls : 'icon-system-wordclass',
						leaf : true,
						'isMini' : -1
					}])

		},
		dataUrl : 'speech-word-category!list.action',
		tbar : new Ext.Toolbar({
					enableOverflow : true,
					height : '20',
					items : [{
								blankText : "请输入分类名称",
								xtype : 'textfield',
								width : 120,
								listeners : {
									specialkey : function(f, e) {
										if (e.getKey() == e.ENTER) {
											self.searchNode(f);
										}
									}
								}
							}, {
								text : '查找',
								iconCls : 'icon-search',
								width : 50,
								handler : function() {
									self.searchNode();
								}
							}, {
								text : '同步语音词类',
								iconCls : 'icon-wordclass-root-sync',
								width : 50,
								handler : function() {
									self.SyncAllAI();
								}
							}, '-', {
								text : '刷新',
								iconCls : 'icon-refresh',
								width : 50,
								handler : function() {
									self.refresh(self.getRootNode());
								}
							}]

				})
	};

	SpeechWordNavPanel.superclass.constructor.call(this, Ext.applyIf(
					_cfg || {}, cfg));

	var menu = new Ext.menu.Menu({
				items : [{
							text : '刷新',
							iconCls : 'icon-refresh',
							width : 50,
							handler : function() {
								self.refreshNode();
							}
						}, {
							id : 'addChildSpeechCategory',
							text : '新建分类',
							iconCls : 'icon-wordclass-add',
							width : 50,
							handler : function() {
								self.addNode(menu.currentNode, false);
							}
						}, {
							id : 'createSpeechWordclass',
							text : '新建词类',
							iconCls : 'icon-wordclass-add',
							width : 50,
							handler : function() {
								self.addNode(menu.currentNode, true);
							}
						}, {
							id : 'modifySpeechWordclass',
							text : '修改分类',
							iconCls : 'icon-wordclass-edit',
							width : 50,
							handler : function() {
								if (menu.currentNode.id == 1
										|| menu.currentNode.id == 2) {
									Ext.Msg.alert('错误提示', '根节点禁止修改');
									return;
								}
								self.modifyNode(menu.currentNode);
							}
						}, {
							id : 'deleteSpeechWordclass',
							text : '删除分类',
							iconCls : 'icon-wordclass-del',
							width : 50,
							handler : function() {
								self.deleteNode(menu.currentNode);
							}
						}, '-', {
							id : 'importSpeechWordclass',
							text : '导入数据',
							iconCls : 'icon-ontology-import',
							width : 50,
							handler : function() {
								self.importData(menu.currentNode);
							}
						}, {
							text : '导出数据',
							iconCls : 'icon-ontology-export',
							width : 50,
							handler : function() {
								self
										.exportSpeechWordclassData(menu.currentNode);
							}
						}, '-',
						// {
						// id : 'synCurrentSpeechWord',
						// text : '同步当前分类',
						// iconCls : 'icon-wordclass-root-sync',
						// width : 50,
						// handler : function() {
						// self.SyncAI(menu.currentNode);
						// }
						// },
						{
							id : 'synAllSpeechWordclass',
							text : '同步语音分类',
							iconCls : 'icon-wordclass-root-sync',
							width : 50,
							handler : function() {
								self.SyncAllAI();
							}
						}]
			});

	this.menu = menu;

	this.on('contextmenu', function(node, event) {
				if (node.attributes.id.length != 1) {
					if (node.attributes.isWordclass == 1) {
						Ext.getCmp('modifySpeechWordclass').setText("修改词类");
						Ext.getCmp('deleteSpeechWordclass').setText("删除词类");
						Ext.getCmp('addChildSpeechCategory').hide();
						Ext.getCmp('createSpeechWordclass').hide();
						Ext.getCmp('importSpeechWordclass').hide();
					} else {
						Ext.getCmp('modifySpeechWordclass').setText("修改分类");
						Ext.getCmp('deleteSpeechWordclass').setText("删除分类");
						Ext.getCmp('addChildSpeechCategory').show();
						Ext.getCmp('createSpeechWordclass').show();
						Ext.getCmp('importSpeechWordclass').show();
					}
					Ext.getCmp('modifySpeechWordclass').show();
					Ext.getCmp('deleteSpeechWordclass').show();
					Ext.getCmp('synAllSpeechWordclass').hide();
				} else {
					Ext.getCmp('synAllSpeechWordclass').show();
					Ext.getCmp('addChildSpeechCategory').show();
					Ext.getCmp('createSpeechWordclass').show();
					Ext.getCmp('modifySpeechWordclass').hide();
					Ext.getCmp('deleteSpeechWordclass').hide();
					Ext.getCmp('importSpeechWordclass').show();
				}

				event.preventDefault();
				menu.currentNode = node;
				node.select();
				if (node.id != 3) {
					menu.showAt(event.getXY());
				}
			});

	this.on('beforeload', function(node, event) {
				this.getLoader().baseParams['isMini'] = node.attributes.isMini;
			});

	var _treeCombo = new TreeComboBox({
				fieldLabel : '父级词类',
				emptyText : '请选择父级词类...',
				editable : false,
				anchor : '95%',
				allowBlank : false,
				minHeight : 250,
				name : 'parentCategoryId',
				root : {
					id : '0',
					text : 'root',
					iconCls : 'icon-ontology-root',
					children : [{
								id : '1',
								text : "语音词类",
								'isMini' : 0,
								'isWordclass' : 0
							}, {
								id : '2',
								text : "迷你词类",
								'isMini' : 1,
								'isWordclass' : 0
							}]
				},
				loader : new Ext.tree.TreeLoader({
							dataUrl : 'speech-word-category!list.action'
						})
			});
	this.treeCombo = _treeCombo;
	this.weighing = new Ext.form.NumberField({
				fieldLabel : '默认权重',
				name : 'weighing',
				blankText : '请输入权重值',
				anchor : '95%'
			});
	var _formPanel = new Ext.form.FormPanel({
				frame : false,
				border : false,
				labelWidth : 75,
				autoHeight : true,
				waitMsgTarget : true,
				bodyStyle : 'padding : 3px 10px; background-color:' + sys_bgcolor,
				items : [new Ext.form.Hidden({
									name : 'categoryId'
								}), new Ext.form.Hidden({
									name : 'bh'
								}), new Ext.form.Hidden({
									name : 'isMini'
								}), new Ext.form.Hidden({
									name : 'isWordclass'
								}), _treeCombo, new Ext.form.TextField({
									fieldLabel : '类型名称',
									allowBlank : false,
									name : 'name',
									blankText : '请输入类型名称',
									anchor : '95%'
								}), self.weighing],
				tbar : [{
							text : '保存',
							iconCls : 'icon-wordclass-add',
							handler : function() {
								self.saveNode(this);
							}
						}]
			});
	var _formWin = new Ext.Window({
				width : 495,
				title : '类型管理',
				defaults : {
					border : false
				},
				modal : true,
				plain : true,
				shim : true,
				closable : true,
				closeAction : 'hide',
				collapsible : true,
				resizable : false,
				draggable : true,
				animCollapse : true,
				constrainHeader : true,
				listeners : {
					'beforehide' : function(p) {
						p.formPanel.getForm().reset();
					}
				},
				items : [_formPanel],
				showPubSelector : function(show) {
					if (show)
						this.pubSchemaSelector.show()
					else
						this.pubSchemaSelector.hide()
				}.dg(this)
			});
	var _fileForm = new Ext.FormPanel({
				layout : "fit",
				frame : true,
				border : false,
				autoHeight : true,
				waitMsgTarget : true,
				defaults : {
					bodyStyle : 'padding:10px'
				},
				margins : '0 0 0 0',
				labelAlign : "left",
				labelWidth : 50,
				fileUpload : true,
				items : [{
							xtype : 'fieldset',
							title : '选择文件',
							autoHeight : true,
							items : [{
										id : 'speechWordUploadFileId',
										name : 'uploadFile',
										xtype : "textfield",
										fieldLabel : '文件',
										inputType : 'file',
										anchor : '96%'
									}]
						}, {
							xtype : 'fieldset',
							title : '设置参数',
							autoHeight : true,
							items : [{
										name : 'cleanTypeAndData',
										xtype : 'checkbox',
										hideLabel : true,
										boxLabel : '是否清空子分类及相关词类名',
										inputValue : true,
										checked : true
									}]
						}]
			});
	var exceptionTextarea = new Ext.form.TextArea({
				name : "exception",
				width : 800,
				height : 500,
				readOnly : true,
				style : 'background:none repeat scroll 0 0 #E6E6E6;color:black;'
			});
	var exceptionWindow = new Ext.Window({
				title : "验证失败",
				closable : true,
				autoWidth : true,
				autoHeight : true,
				closeAction : 'hide',
				plain : true,
				items : [exceptionTextarea],
				listeners : {
					'show' : function() {
						this.center(); // 屏幕居中
					}
				}
			});
	var _importPanel = new Ext.Panel({
		layout : "fit",
		layoutConfig : {
			animate : true
		},
		items : [_fileForm],
		buttons : [{
			id : "btn_import_speechwordclass",
			text : "开始导入",
			handler : function() {
				if (!Ext.getCmp("speechWordUploadFileId").getValue()) {
					Ext.Msg.alert("错误提示", "请选择你要上传的文件");
				} else {
					this.disable();
					var btn = this;
					// 开始导入
					var keyclassForm = self.fileForm.getForm();
					var typePath = self.wordclassType.id.length != 1
							? self.wordclassType.getPath("text")
							: "语音词类导入";
					if (keyclassForm.isValid()) {
						keyclassForm.submit({
							params : {
								'speechwordCategoryId' : (self.wordclassType == null || self.wordclassType.id.length == 1)
										? null
										: self.wordclassType.id,
								'isMini' : self.isMini,
								'weighing' : self.wordclassType.attributes.weighing,
								'importType' : self.importType,
								'typepath' : typePath
							},
							url : 'speech-word-category!importSpeechWordclass.action',
							success : function(form, action) {
								btn.enable();
								var result = action.result.data;
								var timer = new ProgressTimer({
									initData : result,
									progressId : 'importSpeechWordclassStatus',
									boxConfig : {
										title : '正在导入语音词类...'
									},
									finish : function(p, response) {
										if (p.currentCount == -2) {
											Ext.Msg.hide();
											var message = Ext
													.decode(response.responseText).message
											exceptionTextarea.setValue(message);
											exceptionWindow.show();
											// Ext.MessageBox.minWidth = 300;
											// Ext.Msg.alert("验证异常", message);
										} else if (p.currentCount == -3) {
											// 下载验证失败的信息
											if (!self.downloadIFrame) {
												self.downloadIFrame = self
														.getEl().createChild({
															tag : 'iframe',
															style : 'display:none;'
														})
											}
											self.downloadIFrame.dom.src = "speech-word-category!downExportFile.action?type="
											self.importType + "&_t="
													+ new Date().getTime();
										}
										self.refresh(self.wordclassType);
									}
								});
								timer.start();
							},
							failure : function(form, action) {
								btn.enable();
								Ext.MessageBox.hide();
								Ext.Msg
										.alert(
												'错误',
												'导入初始化失败.'
														+ (action.result.message
																? '详细错误:'
																		+ action.result.message
																: ''));
							}
						});
					}
				}
			}
		}, {
			text : "关闭",
			handler : function() {
				self.importKeywordWin.hide();
			}
		}]
	});

	var importKeywordWin = new Ext.Window({
				width : 400,
				title : '语音词类导入',
				defaults : {// 表示该窗口中所有子元素的特性
					border : false
					// 表示所有子元素都不要边框
				},
				plain : true,// 方角 默认
				modal : true,
				plain : true,
				shim : true,
				closeAction : 'hide',
				collapsible : true,// 折叠
				closable : true, // 关闭
				resizable : false,// 改变大小
				draggable : true,// 拖动
				minimizable : false,// 最小化
				maximizable : false,// 最大化
				animCollapse : true,
				constrainHeader : true,
				autoHeight : false,
				items : [_importPanel]
			});
	this.fileForm = _fileForm;
	this.importKeywordWin = importKeywordWin;
	_formWin.formPanel = _formPanel;
	this.formWin = _formWin;

	this.on('click', function(n) {
				if (n.id != 3) {
					this.wordclassType = n;
					if (!n)
						return false;
					this.isMini = n.attributes.isMini;
					this.wordPanel = self.showTab(SpeechWordPanel,
							(n.attributes.isWordclass != 1
									? "speechWordclassTab"
									: 'speech-word-tab' + n.id), n.text,
							(n.attributes.isWordclass == 1
									? 'icon-wordclass'
									: 'icon-speechcagory'), true);
					this.wordPanel.countWordclass(n.attributes.isMini, n.id);
					this.wordPanel.loadWords(n.id, n.attributes.isMini,
							n.attributes.isWordclass);
				} else {
					var systemWordPanel = self.showTab(
							SpeechSystemWordclassPanel, 'speech-word-tab'
									+ n.id, n.text, 'icon-system-wordclass',
							true);
					systemWordPanel.loadWords();
				}
			})
}

Ext.extend(SpeechWordNavPanel, Ext.tree.TreePanel, {
	showWordTab : function(tabId, path) {
		var self = this;
		this.expandPath('/0/' + path, 'id', function() {
					var node = self.getRootNode().findChild('id', tabId, true);
					self.fireEvent('click', node);
				});
	},
	endEdit : function() {
		this.formWin.hide();
		this.formWin.formPanel.getForm().reset();
	},
	isRoot : function(node) {
		return node.id.length == 1;
	},
	saveNode : function(btn) {
		btn.disable();
		var f = this.formWin.formPanel.getForm();
		if (!f.isValid()) {
			Ext.Msg.alert('提示', '您尚未正确完整的输入表单内容，请检查！');
			btn.enable();
			return false;
		}
		this.data = f.getValues();
		this.data.parentCategoryId = this.treeCombo.getValue();
		var isMini = this.data.isMini;
		if (this.data.parentCategoryId.length == 1) {
			this.data.parentCategoryId = null;
		}
		if (!this.data.categoryId)
			this.data.categoryId = null;
		var self = this;
		Ext.Ajax.request({
					url : 'speech-word-category!save.action',
					params : {
						data : Ext.encode(self.data)
					},
					success : function(form, action) {
						Dashboard.setAlert("保存成功!");
						self.endEdit();
						self.updateNode(Ext.decode(form.responseText), isMini);
						btn.enable();
					},
					failure : function(response) {
						Ext.Msg.alert("错误", response.responseText);
						btn.enable();
					}
				})
	},
	_setMiniDataUrl : function() {
		if (!this.treeComboExpandL) {
			this.treeCombo.on('expand', this.treeComboExpandL = function() {
						if (this.treeCombo.loader.dataUrl != this.lastDataUrl) {
							this.treeCombo.reload();
						}
					}, this)
		}
		this.lastDataUrl = this.treeCombo.loader.dataUrl
		if (this.treeCombo.loader.dataUrl.indexOf('isMini') == -1)
			this.treeCombo.loader.dataUrl += '?isMini=' + this.isMini
	},
	addNode : function(node, isWordclass) {
		this.isMini = node.attributes.isMini;
		var rec = new Ext.data.Record(Ext.apply({}, node.attributes));
		if (isWordclass) {
			this.weighing.show();
			rec.set('weighing', rec.get('weighing') == 0 ? 1 : rec
							.get('weighing'));
			rec.set('isWordclass', isWordclass ? "1" : "0");
		} else {
			this.weighing.hide();
			rec.set('weighing', "0");
			rec.set('isWordclass', isWordclass ? "1" : "0");
		}
		this.treeCombo.loader.dataUrl = this.treeCombo.loader.dataUrl
				.split("?isMini=")[0] += '?isMini=' + this.isMini;
		if (this.isMini != 1) {
			this.treeCombo.root.children[0].hidden = false;
			this.treeCombo.root.children[1].hidden = true;
		} else {
			this.treeCombo.root.children[0].hidden = true;
			this.treeCombo.root.children[1].hidden = false;
		}
		this.treeCombo.reload();
		rec.set('isMini', this.isMini);
		var fp = this.formWin.formPanel;
		fp.getForm().loadRecord(rec);
		this.formWin.show();
		// if (this.isRoot(node))
		// this.treeCombo.setReadOnly(true);
		// else
		// this.treeCombo.setReadOnly(false);
		this.treeCombo.setValueEx(node);
		this._setMiniDataUrl();
	},
	modifyNode : function(node) {
		this.isMini = node.attributes.isMini;
		var isWordclass = node.attributes.isWordclass;
		this.formWin.show();
		var fp = this.formWin.formPanel;
		var rec = new Ext.data.Record(Ext.apply({}, node.attributes));
		rec.set('name', rec.get('text'));
		rec.set('categoryId', rec.get('id'));
		if (isWordclass == '1') {
			this.weighing.show();
			rec.set('weighing', rec.get('weighing'));
			rec.set('isWordclass', rec.get('isWordclass'));
		} else {
			this.weighing.hide();
			rec.set('weighing', "0");
			rec.set('isWordclass', 0);
		}
		this.treeCombo.loader.dataUrl = this.treeCombo.loader.dataUrl
				.split("?isMini=")[0] += '?isMini=' + this.isMini;
		if (this.isMini != 1) {
			this.treeCombo.root.children[0].hidden = false;
			this.treeCombo.root.children[1].hidden = true;
		} else {
			this.treeCombo.root.children[0].hidden = true;
			this.treeCombo.root.children[1].hidden = false;
		}
		this.treeCombo.reload();
		rec.set('isMini', this.isMini);
		fp.getForm().loadRecord(rec);
		// if (this.isRoot(node.parentNode))
		// this.treeCombo.setReadOnly(true);
		// else
		// this.treeCombo.setReadOnly(false);
		this.treeCombo.setValueEx(node.parentNode);
		this._setMiniDataUrl();
	},
	deleteNode : function(node) {
		var self = this;
		var wordPanel = this.wordPanel
		if (node.id.length == 1) {
			Ext.Msg.alert('警告', '根节点不能删除！');
			return;
		}
		Ext.Msg.confirm('提示', '确定要删除&nbsp&nbsp'
						+ node.getPath('text').replace('/root', '')
						+ '&nbsp&nbsp及以下所有分类吗', function(btn, text) {
					if (btn == 'yes') {
						Ext.Ajax.request({
									url : 'speech-word-category!del.action',
									params : {
										'isMini' : node.attributes.isMini,
										'categoryId' : node.id
									},
									success : function(response) {
										var node4Sel = node.nextSibling
												? node.nextSibling
												: node.previousSibling;
										if (!node4Sel) {
											node4Sel = node.parentNode;
											node4Sel.leaf = true;
										}
										node4Sel.select();
										if (typeof(wordPanel) != 'undefined'
												&& wordPanel != '') {
											wordPanel
													.loadWords(
															node.parentNode.id,
															node.attributes.isMini,
															node.attributes.isWordclass)
										}
										self
												.closeTab(node.attributes.isWordclass != 1
														? "speechWordclassTab"
														: 'speech-word-tab'
																+ node.id);
										node.parentNode.removeChild(node);
									},
									failure : function(response) {
										Ext.Msg.alert("错误",
												response.responseText);
									}
								});
					}
				});
	},
	refresh : function(node) {
		this.isMini = null;
		this.isMini = node.attributes.isMini;
		if (node.isExpanded() && !node.attributes.leaf)
			node.reload();
		else
			node.expand();
	},
	getSelectedNode : function() {
		return this.getSelectionModel().getSelectedNode();
	},
	updateNode : function(data, isMini) {
		var parentCateId = this.data.parentCategoryId;
		var parentNode;
		if (parentCateId == null) {
			if (isMini == '0') {
				parentNode = this.getRootNode().childNodes[0];
			} else if (isMini == '1') {
				parentNode = this.getRootNode().childNodes[1];
			}
		} else {
			parentNode = this.getNodeById(parentCateId);
		}
		if (!data.saveOrUpdate) {
			var selNode = this.getSelectedNode();
			selNode.setText(data.text);
			selNode.attributes.weighing = data.weighing;
			selNode.attributes.isWordclass = data.isWordclass;
			if (selNode.parentNode.id != parentNode.id) {
				var parentBefore = selNode.parentNode;
				if (parentNode) {
					selNode.leaf = data.leaf;
					// this.setNodeIconCls(selNode, data.iconCls);
					selNode.attributes.bh = data.bh;
					parentNode.leaf = false;
					parentNode.appendChild(selNode);
				} else {
					parentBefore.removeChild(selNode);
				}
				if (parentBefore.childNodes.length == 0)
					parentBefore.leaf = true;
			}
		} else {
			if (parentNode) {
				var node = new Ext.tree.TreeNode({
							id : data.id,
							iconCls : data.iconCls,
							cls : data.cls,
							leaf : true,
							text : data.text,
							bh : data.bh,
							isMini : data.isMini,
							isWordclass : data.isWordclass,
							weighing : data.weighing
						});
				if (parentNode.leaf)
					parentNode.leaf = false
				parentNode.appendChild(node);
				parentNode.expand();
				node.select();
			}
		}
	},
	setNodeIconCls : function(node, clsName) {
		var iel = node.getUI().getIconEl();
		if (iel) {
			var el = Ext.get(iel);
			if (el) {
				if (clsName.indexOf('-root') > 0)
					el.removeClass('icon-wordclass-category');
				else
					el.removeClass('icon-wordclass-root');
				el.addClass(clsName);
			}
		}
	},

	searchNode : function(focusTarget) {
		var categoryName = categoryName = this.getTopToolbar().getComponent(0)
				.getValue();
		if (!categoryName || !(categoryName = categoryName.trim())) {
			Ext.Msg.alert('提示', '请输入分类名称');
			return;
		}
		var self = this;
		Ext.Ajax.request({
					url : 'speech-word-category-search!search.action',
					params : {
						'categoryName' : categoryName
					},
					success : function(form, action) {
						if (!form.responseText) {
							Dashboard.setAlert('没有查找到更多的"' + categoryName
									+ '"，请修改后重新查找');
							return;
						}
						var obj = Ext.util.JSON.decode(form.responseText);
						var idPath = obj.pathId;
						var isMini = obj.isMini;
						if (idPath) {
							var targetId = idPath.pop();
							self
									.expandPath(
											'/0/'
													+ (isMini == 0
															? '1/'
															: (isMini == 1
																	? '2/'
																	: ''))
													+ idPath.join('/'), '',
											function(suc, n) {
												if (targetId) {
													self.getNodeById(targetId)
															.select();
													if (focusTarget)
														focusTarget.focus()
												}
											});
						}
					},
					failure : function(form, action) {
						Ext.Msg.alert('提示', '查找错误');
					}
				});
	},

	expandNode : function(node, bh, isMini) {
		var self = this;
		if (node.attributes.bh == bh || node.leaf) {
			this.getSelectionModel().select(node);
			return;
		}
		node.expand(false, true, function() {
					var childs = node.childNodes;
					for (var i = 0; i < childs.length; i++) {
						if (bh.indexOf(childs[i].attributes.bh) != -1)
							self.expandNode(childs[i], bh);
					}
				});
	},
	refreshNode : function() {
		var node = this.getSelectedNode();
		this.isMini = null;
		this.isMini = node.attributes.isMini;
		if (!node)
			this.refresh(this.getRootNode());
		else {
			this.refresh(node);
		}
	},
	importData : function(node) {
		var self = this;
		this.wordclassType = node;
		this.isMini = node.attributes.isMini;
		this.allpub = node.id == '1';
		Ext.Msg.show({
					title : '导入文件类型',
					msg : '您选择要导入文件格式',
					buttons : {
						no : 'ZIP压缩包',
						yes : 'Excel',
						cancel : true
					},
					fn : function(choice) {
						if (choice == 'cancel')
							return
						choice == 'yes' ? self.importType = 'excel' : '';
						choice == 'no' ? self.importType = 'zip' : '';
						self.importKeywordWin.show();
					}
				});
		// 恢复导入按钮
		Ext.getCmp("btn_import_speechwordclass").setDisabled(false);

	},
	exportSpeechWordclassData : function(typeNode) {
		this.isMini = typeNode.attributes.isMini;
		var self = this;
		var fileType = '';
		Ext.Msg.show({
			title : '导出文件类型',
			msg : '您选择要导出的文件类型',
			buttons : {
				no : 'ZIP',
				yes : 'Excel07',
				ok : 'Excel03',
				cancel : true
			},
			fn : function(choice) {
				if (choice == 'cancel')
					return
				choice == 'ok' ? fileType = '03' : '';
				choice == 'yes' ? fileType = '07' : '';
				choice == 'no' ? fileType = 'zip' : '';

				Ext.Ajax.request({
					url : 'speech-word-category!exportSpeechWordclass.action',
					params : {
						'wordclassCategoryId' : typeNode.id.length == 1
								? null
								: typeNode.id,
						'typePath' : (typeof(typeNode) == 'undefined'
								? ''
								: typeNode.getPath("text")),
						'isMini' : self.isMini,
						'filetype' : fileType
					},
					success : function(response) {
						// var fileName = Ext.util.JSON
						// .decode(response.responseText).message;
						var params = Ext.urlEncode({
									'type' : fileType == 'zip'
											? 'zip'
											: 'excel'
								})
						var result = Ext.util.JSON
								.decode(response.responseText);
						if (result.success) {
							var timer = new ProgressTimer({
								initData : result.data,
								progressId : 'exportSpeechWordclassStatus',
								boxConfig : {
									title : '正在导出语音词类...'
								},
								finish : function() {
									if (!self.downloadIFrame) {
										self.downloadIFrame = self.getEl()
												.createChild({
															tag : 'iframe',
															style : 'display:none;'
														})
									}
									self.downloadIFrame.dom.src = 'speech-word-category!downExportFile.action?'
											+ params
											+ '&_t='
											+ new Date().getTime();
								}
							});
							timer.start();
						} else {
							Ext.Msg.alert('错误', '导出失败:' + result.message)
						}
					},
					failure : function(response) {
						Ext.Msg.alert('错误', '导出失败')
					},
					scope : this
				});
			}
		});

	},
	SyncAllAI : function() {
		Ext.Ajax.request({
					url : 'speech-word!syncAllAI.action',
					success : function(resp) {
						var timer = new ProgressTimer({
									initData : Ext.decode(resp.responseText),
									progressId : 'syncSpeechWord',
									boxConfig : {
										title : '同步所有词类'
									}
								});
						timer.start();
					},
					failure : function(resp) {
						Ext.Msg.alert('错误', resp.responseText);
					}
				});
	},
	// SyncAI : function(node) {
	// if (node.id.length == 1) {
	// Ext.Ajax.request({
	// url : 'speech-word!syncAllAI.action',
	// params : {
	// isMini : node.attributes.isMini
	// },
	// success : function(resp) {
	// var timer = new ProgressTimer({
	// initData : Ext
	// .decode(resp.responseText),
	// progressId : 'syncWord',
	// boxConfig : {
	// title : '同步词类'
	// }
	// });
	// timer.start();
	// },
	// failure : function(resp) {
	// Ext.Msg.alert('错误', resp.responseText);
	// }
	// });
	// } else {
	// Ext.Ajax.request({
	// url : 'speech-word!syncAI.action',
	// params : {
	// 'isMini' : node.attributes.isMini,
	// 'syncCategoryId' : node.attributes.id,
	// 'bh' : node.attributes.bh
	// },
	// success : function(resp) {
	// var timer = new ProgressTimer({
	// initData : Ext
	// .decode(resp.responseText),
	// progressId : 'syncSpeechWord',
	// boxConfig : {
	// title : '同步词类'
	// }
	// });
	// timer.start();
	// },
	// failure : function(resp) {
	// Ext.Msg.alert('错误', resp.responseText);
	// }
	// });
	// }
	// },
	authName : 'rs.word'
});

Dashboard.registerNav(SpeechWordNavPanel, 2);
