Ext.QuickTips.init();
Ext.form.Field.prototype.msgTarget = "side";
Ext.isFloat = function(v) {
	return /^\d+(\.\d+)?$/.test(v);
}
Ext.isInt = function(v) {
	return /^[1-9]+(\d+)?$/.test(v);
}
Dashboard = {
	u : function() {
		return this._usr ? this._usr : this._usr = new LoginUser('rs')
	},
	setAlert : function(msg, status, delay) {
		if (!status)
			status = 'ok'
		if (!delay)
			delay = 3
		if (!this.msgCt) {
			this.msgCt = Ext.DomHelper.insertFirst(document.body, {
				  id : 'msg-div'
			  }, true);
			this.msgCt.setStyle('position', 'absolute');
			this.msgCt.setStyle('z-index', 9999);
			this.msgCt.setWidth(300);
		}
		this.msgCt.alignTo(document, 't-t');
		var html = ['<div class="app-msg">', '<div class="x-box-tl"><div class="x-box-tr"><div class="x-box-tc"></div></div></div>',
		  '<div class="x-box-ml"><div class="x-box-mr"><div class="x-box-mc"><h3 class="x-icon-text icon-status-' + status + '">', msg, '</h3>', '</div></div></div>',
		  '<div class="x-box-bl"><div class="x-box-br"><div class="x-box-bc"></div></div></div>', '</div>'].join('');
		Ext.DomHelper.append(this.msgCt, {
			  'html' : html
		  }, true).slideIn('t').pause(delay).ghost("t", {
			  remove : true
		  });
	},
	navRegistry : [],
	registerNav : function(navClass, sort) {
		this.navRegistry.push({
			  cls : navClass,
			  'createNav' : function(id) {
				  return new navClass({
					    'id' : id
				    });
			  },
			  'sort' : sort
		  });
	},
	getDaysOfMonth : function(date) {
		if (!date)
			date = new Date();
		date.setMonth(date.getMonth() + 1);
		date.setDate(0);
		return date.getDate();
	},
	createXHR : function() {
		var _xhr = false;
		try {
			_xhr = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				_xhr = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e2) {
				_xhr = false;
			}
		}
		if (!_xhr && window.XMLHttpRequest)
			_xhr = new XMLHttpRequest();
		return _xhr;
	},
	newTemplateRenderer : function() {
		return function(value) {
			if (!value)
				return;
			var x = '';
			var istmplt = false
			for (var i = 0; i < value.length; i++) {
				var w = value.charAt(i)
				var color = null;
				if (w == '<') {
					w = '&lt;';
					color = 'blue';
				} else if (w == '>') {
					w = '&gt;';
					color = 'blue';
				} else if (w == '(') {
					color = '#c71585';
				} else if (w == ')') {
					color = '#c71585'
				} else if (w == '*') {
					w = "<span style='font-size:13pt'>*</span>"
					color = 'red';
				} else if (w == '?') {
					color = 'red'
				} else if (w == '|') {
					color = 'red'
				} else if (w == ']') {
					color = 'green'
				} else if (w == '[') {
					color = 'green'
				} else if (w == '@' || w == '#' || w == '&') {
					color = "blue";
					while (value.charAt(i + 1) != "|" && value.charAt(i + 1) != "]") {
						w += value.charAt(i + 1);
						if (i < value.length - 1)
							i++;
						else
							break;
					}
				}

				if (color) {
					x += "<div style='display:inline;font-weight:bold;color:" + color + ";'>" + w + "</div>";
					istmplt = true
				} else
					x += w;
			}
			return istmplt ? ("<div style='display:inline;background-color:#ffff99'>" + x + "</div>") : x;
		}
	}
};

Ext.onReady(function() {
	Ext.QuickTips.init();

	var xhr = Dashboard.createXHR();
	xhr.open('GET', 'property!get0.action?key=speech.addr', false);
	xhr.send();
	var speechAddr = xhr.responseText;
	if (speechAddr) {
		var idx = speechAddr.indexOf(':');
		if (idx != -1)
			speechAddr = speechAddr.substring(0, idx);
		Dashboard.rtmpAddr = speechAddr;
	}
	var xhr = Dashboard.createXHR();
	xhr.open('GET', 'property!get0.action?key=robot.project_id', false);
	xhr.send();
	var projectId = xhr.responseText;
	if (projectId)
		Dashboard.projectId = projectId + ".gram";

	window.fakeClipboard = {
		_el : Ext.getBody().createChild({
			  id : 'fakeClipboard',
			  tag : 'textarea',
			  style : 'position:absolute;width:0;height:0;left:-10px;'
		  }),
		_onpaste : Ext.emptyFn,
		_init : function() {
			var el = this._el;
			this._inited = true;
			el.on("keydown", function(e) {
				  var el = this._el;
				  if (e.getKey() == 17)
					  el.ctrl = true;
				  if (!el.ctrl)
					  e.stopEvent();
				  else if (el.ctrl && e.getKey() == 86) {
					  el.ready = true
				  }
			  }, this);
			el.on("keyup", function(e) {
				  var el = this._el;
				  if (el.ready && e.getKey() == 86) {
					  el.ready = false
					  this._onpaste(el.dom.value)
				  }
				  if (e.getKey() == 17)
					  el.ctrl = false;
			  }, this);
		},
		listen : function(fn) {
			var el = this._el;
			if (!this._inited)
				this._init();
			this._onpaste = fn;
			el.dom.value = '';
			el.dom.focus();
		},
		decodeExcel : function(value) {
			var s = value;
			var data = [], row = [], cell = '', b = false;
			for (var i = 0; i < s.length; i++) {
				var c = s.charAt(i);
				if (c == '"') {
					if (b) {
						if (i + 1 < s.length && s.charAt(i + 1) == '"') {
							cell += '"';
							i++;
							continue;
						} else {
							b = false;
						}
					} else if (cell.length > 0) {
						cell += c
					} else {
						b = true
					}
				} else if (!b && c == '\t') {
					if (cell.length > 0)
						row.push(cell);
					else
						row.push(null);
					cell = '';
				} else if (!b && c == '\r') {
					continue;
				} else if (!b && c == '\n') {
					if (cell.length > 0)
						row.push(cell);
					cell = '';
					data.push(row);
					row = [];
				} else {
					cell += c;
				}
			}
			if (cell.length > 0)
				row.push(cell);
			if (row.length > 0)
				data.push(row);
			return data;
		}
	};

	var r = Dashboard.navRegistry.sort(function(nav1, nav2) {
		  return nav1.sort - nav2.sort;
	  });

	Dashboard.u().filterAllowed(r, 'cls.prototype.authName')

	var mainPanel = new Ext.TabPanel({
		  region : 'center',
		  activeTab : 0,
		  border : false,
		  style : 'border-left: 1px solid ' + sys_bdcolor,
		  resizeTabs : true,
		  tabWidth : 150,
		  minTabWidth : 120,
		  enableTabScroll : true,
		  layoutOnTabChange : true
	  });

	var tabCtxMenu = new Ext.menu.Menu({
		  items : [{
			    text : '关闭其他标签页',
			    iconCls : 'icon-status-cancel',
			    width : 50,
			    handler : function() {
				    var atab = this.getActiveTab();
				    var c = this.items.getCount();
				    for (var i = c - 1; i >= 0; i--) {
					    if (atab != this.items.get(i)) {
						    mainPanel.remove(this.items.get(i).id);
					    }
				    }
			    }.dg(mainPanel)
		    }]
	  });
	mainPanel.on('contextmenu', function(tabp, targetp, e) {
		  tabCtxMenu.showAt(e.getXY())
	  })

	var navItems = [];
	var tabRegistry = {};
	for (var i = 0; i < r.length; i++) {
		var nav = r[i].createNav();
		nav.moduleIndex = i;
		nav.showTab = function(panelClass, tabId, title, iconCls, closable) {
			if (!panelClass)
				return false;
			var tab = mainPanel.get(tabId);
			if (!tab) {
				var tabPanel = new panelClass();
				tabPanel.navPanel = this;
				tabPanel.tabId = tabId;
				tabPanel.tab = tab = mainPanel.add({
					  id : tabId,
					  layout : 'fit',
					  title : title,
					  items : tabPanel,
					  iconCls : iconCls,
					  closable : closable
				  });
				tabPanel.addEvents('tabactive', 'tabinactive')
				tabPanel.close = function() {
					nav.closeTab(this.tabId)
				}
				tab.on('close', function() {
					  return tabPanel.fireEvent("close", tabPanel);
				  });
				tab.on('beforeclose', function() {
					  return tabPanel.fireEvent("beforeclose", tabPanel);
				  });
				tabRegistry[tabId] = this.moduleIndex;
				tab.show();
			} else {
				mainPanel.activate(tabId);
				if (title)
					tab.setTitle(title)
				if (iconCls)
					tab.setIconClass(iconCls)
			}
			return tab.get(0);
		};
		nav.closeTab = function(tabId) {
			mainPanel.remove(tabId)
		};

		if (i == 0) {
			nav.on('afterrender', function() {
				  var f = function(node) {
					  if (node.childNodes && node.childNodes.length) {
						  node.childNodes[0].expand();
						  (function() {
							  node.childNodes[0].fireEvent('click', node.childNodes[0]);
						  }).defer(100);
					  }
					  this.removeListener('load', f)
				  }
				  this.on('load', f, this)
				  this.renderRootNode();
			  })
		} else {
			nav.on('expand', function() {
				  this.renderRootNode();
			  })
		}
		navItems.push(nav);
	}

	var navPanel = new Ext.Panel({
		  region : 'west',
		  width : 200,
		  minSize : 175,
		  maxSize : 400,
		  split : true,
		  collapsible : true,
		  collapseMode : 'mini',
		  border : false,
		  style : 'border-right: 1px solid ' + sys_bdcolor,
		  layout : 'accordion',
		  title : '控制台',
		  items : navItems
	  });

	var viewport = new Ext.Viewport({
		  layout : 'border',
		  items : [navPanel, mainPanel]
	  });

	mainPanel.on('tabchange', function(tabPanel, tab) {
		  if (!tab)
			  return;
		  var navIndex = tabRegistry[tab.id];
		  if (typeof navIndex == 'undefined')
			  return;
		  navPanel.layout.setActiveItem(navIndex);
	  });
	mainPanel.on('beforetabchange', function(tabPanel, newtab, currenttab) {
		  if (currenttab)
			  currenttab.get(0).fireEvent('tabinactive')
		  if (newtab)
			  newtab.get(0).fireEvent('tabactive')
	  });

	 // var navs = [ {
	 // id : '1',
	 // authName : 'om.log',
	 // name : '内容管理',
	 // icon : 'icon-expand'
	 // }, {
	 // id : '11',
	 // name : '场景管理',
	 // module : 'scenarioPanel',
	 // parent : '1',
	 // icon : 'icon-toolbox',
	 // panelClass : ScenarioPanel
	 // }, {
	 // id : '12',
	 // name : '样例管理',
	 // module : 'samplePanel',
	 // parent : '1',
	 // icon : 'icon-toolbox',
	 // panelClass : SamplePanel
	 // }, {
	 // id : '3',
	 // name : '统计分析',
	 // icon : 'icon-expand'
	 // }, {
	 // id : '30',
	 // name : '识别结果统计',
	 // icon : 'icon-toolbox',
	 // module : 'recogStat',
	 // parent : '3',
	 // panelClass : RecognizeStatPanel
	 // } ];
	 // var rootNode = new Ext.tree.TreeNode({
	 // id : '0',
	 // text : 'root'
	 // });
	 // var _nodeMap = {};
	 // for ( var i = 0; i < navs.length; i++) {
	 // var _item = navs[i];
	 // if (_item.hidden)
	 // continue;
	 // var _node = new Ext.tree.TreeNode({
	 // id : _item.id,
	 // text : _item.name,
	 // iconCls : _item.icon,
	 // module : _item.module,
	 // panelClass : _item.panelClass
	 // });
	 // _nodeMap[_item.id] = _node;
	 // if (!_item.parent)
	 // rootNode.appendChild(_node);
	 // else
	 // _nodeMap[_item.parent].appendChild(_node);
	 // }
	 // var navPanel = new Ext.tree.TreePanel({
	 // region : 'west',
	 // width : 200,
	 // minSize : 175,
	 // maxSize : 400,
	 // split : true,
	 // collapsible : true,
	 // collapseMode : 'mini',
	 // border : false,
	 // style : 'border-right: 1px solid ' + sys_bdcolor,
	 // title : '控制台',
	 // autoScroll : true,
	 // rootVisible : false,
	 // lines : false,
	 // root : rootNode
	 // });
	 // navPanel.showTab = function(panelClass, tabId, title, iconCls,
	 // closable)
	 // {
	 // if (!panelClass)
	 // return false;
	 // var tab = mainPanel.get(tabId);
	 // if (!tab) {
	 // var tabPanel = new panelClass();
	 // tabPanel.navPanel = this;
	 // tab = mainPanel.add({
	 // id : tabId,
	 // layout : 'fit',
	 // title : title,
	 // items : tabPanel,
	 // iconCls : iconCls,
	 // closable : closable
	 // });
	 // tab.show();
	 // } else {
	 // mainPanel.activate(tabId);
	 // }
	 // return tab.get(0);
	 // }
	 // navPanel.on('expandnode', function(node) {
	 // if (node.id == '1') {
	 // this.fireEvent('click', node.firstChild);
	 // }
	 // }, navPanel);
	 // navPanel.on('afterrender', function() {
	 // this.getRootNode().expand(true, true);
	 // }, navPanel);
	 // navPanel.on('click', function(n) {
	 // if (!n)
	 // return false;
	 // var module = n.attributes.module;
	 // var panelClass = n.attributes.panelClass;
	 // var p = this.showTab(panelClass, module + 'Tab', n.text,
	 // n.attributes.iconCls, true);
	 // if (p && p.loadData)
	 // p.loadData();
	 // }, navPanel);
	 //
	 // var viewport = new Ext.Viewport({
	 // layout : 'border',
	 // items : [ navPanel, mainPanel ]
	 // });

 });