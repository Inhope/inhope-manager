RecognizeStatPanel = function(_cfg) {
	var self = this;
	this._cduration = 0;
	this._count = 0;

	var categoryCombo = new Ext.form.ComboBox({
		hiddenName : 'category',
		valueField : 'name',
		displayField : 'title',
		loadMask : true,
		selectOnFocus : false,
		editable : false,
		triggerAction : 'all',
		width : 100,
		store : new RecognizeStore({
			type : 'category'
		})
	});
	var regStatusCombo = new Ext.form.ComboBox({
		hiddenName : 'status',
		valueField : 'name',
		displayField : 'title',
		mode : 'local',
		editable : false,
		value : -1,
		triggerAction : 'all',
		forceSelection : true,
		width : 100,
		store : new Ext.data.JsonStore({
			fields : [ 'name', 'title' ],
			data : [ {
				name : -1,
				title : '全部'
			}, {
				name : 100,
				title : '识别成功'
			}, {
				name : 0,
				title : '识别失败'
			} ]
		})
	});

	var _gridReader = new Ext.data.JsonReader({
		idProperty : 'id',
		root : 'data',
		fields : [ {
			name : 'id',
			type : "string"
		}, {
			name : 'engine',
			type : "string"
		}, {
			name : 'resu',
			type : "string"
		}, {
			name : 'confidence',
			type : "string",
			convert : function(v, r) {
				if (r.confidence == 100) {
					return '<p style="color:#0000FF">成功</p>';
				} else {
					return '<p style="color:#FF0000">失败</p>';
				}
			}
		}, {
			name : 'categoryName',
			type : "string"
		}, {
			name : 'content',
			type : "string"
		}, {
			name : 'path',
			type : "string"
		}, {
			name : 'createTime',
			type : "string"
		}, {
			name : 'duration',
			type : "int"
		} ]
	});

	var _chartReader = new Ext.data.JsonReader({
		idProperty : 'id',
		totalProperty : 'total',
		root : 'data',
		fields : [ {
			name : 'categoryName',
			type : "string"
		}, {
			name : 'scale',
			type : "int"
		} ]
	});
	this._gridStore = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({
			url : 'recognize-stat!detail.action'
		}),
		autoLoad : true,
		params : {
			category : categoryCombo.getValue(),
			status : regStatusCombo.getValue()
		},
		reader : _gridReader
	});

	this._chartStore = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({
			url : 'recognize-stat!stat.action'
		}),
		autoLoad : true,
		reader : _chartReader
	});

	var config = {
		id : 'recog',
		store : this._gridStore,
		region : 'center',
		columnLines : true,
		border : false,
		style : 'border-right: 1px solid ' + sys_bdcolor + ' border-bottom: 1px solid ' + sys_bdcolor,
		// sm : _sm,
		loadMask : true,
		colModel : new Ext.grid.ColumnModel({
			columns : [ new Ext.grid.RowNumberer(), {
				header : '编号',
				hidden : true,
				width : 50,
				dataIndex : 'id'
			}, {
				header : '样例语句',
				width : 200,
				dataIndex : 'content'
			}, {
				header : '识别结果',
				width : 200,
				dataIndex : 'resu'
			}, {
				header : '是否成功',
				width : 50,
				dataIndex : 'confidence'
			}, {
				header : '样例类别',
				width : 100,
				dataIndex : 'categoryName',
				sortable : true
			}, {
				header : '识别耗时(ms)',
				width : 80,
				dataIndex : 'duration'
			}, {
				header : '识别时间',
				width : 100,
				renderer : Ext.util.Format.dateRenderer('m/d/Y'),
				dataIndex : 'createTime'
			} ]
		}),
		bbar : new Ext.PagingToolbar({
			pageSize : 20,
			store : this._gridStore,
			displayInfo : true,
			beforePageText : '第',
			afterPageText : '页，共{0}页',
			displayMsg : '显示第{0}条到{1}条记录，共{2}条',
			emptyMsg : "没有数据",
			items : []
		}),
		viewConfig : {
			forceFit : true
		}
	};

	var _grid = new Ext.grid.GridPanel(config);
	this.grid = _grid;

	this.categoryCombo = categoryCombo;
	this.regStatusCombo = regStatusCombo;

	var cfg = {
		layout : 'border',
		border : false,
		tbar : [
				'样例类别:',
				categoryCombo,
				' 识别状态:',
				regStatusCombo,
				{
					text : '搜索',
					iconCls : 'icon-search',
					handler : function() {
						self._gridStore.baseParams['category'] = categoryCombo
								.getValue();
						self._gridStore.baseParams['status'] = regStatusCombo
								.getValue();
						self.amount();
						self._gridStore.reload({
							params : {
								category : categoryCombo.getValue(),
								status : regStatusCombo.getValue()
							}
						});
					}
				} ],
		items : [ _grid, {
			region : 'south',
			header : false,
			ref : 'southPanel',
			split : true,
			collapsible : true,
			collapseMode : 'mini',
			frame : false,
			border : false,
			height : 250,
			items : {
				xtype : 'columnchart',
				id : 'statChart',
				region : 'south',
				header : false,
				ref : 'southPanel',
				store : this._chartStore,
				yField : 'scale',
				canDropLabels : "true",
				url : '../ext/resources/charts.swf',
				xField : 'categoryName',
				xAxis : new Ext.chart.CategoryAxis({
					title : '样例类别',
					size : 8
				}),
				yAxis : new Ext.chart.NumericAxis({
					title : '正确识别数'
				}),
				extraStyle : {
					xAxis : {
						labelRotation : 0
					}
				},
				chartStyle : {
					font : { // X轴Y轴字体设置。
						name : 'Tahoma',
						color : 0x444444,
						size : 8
					}
				}
			},
			bbar : [ '->', {
				xtype : 'tbtext',
				text : '正在统计...'
			} ]
		} ]
	};
	RecognizeStatPanel.superclass.constructor.call(this, cfg);
}
Ext.extend(RecognizeStatPanel, Ext.Panel, {
	loadData : function() {
		this.amount();
	},
	amount : function() {
		Ext.Ajax.request({
			url : 'recognize-stat!amount.action',
			params : {
				'category' : this._gridStore.baseParams['category'],
				'status' : this._gridStore.baseParams['status']
			},
			success : function(response) {
				var result = Ext.util.JSON.decode(response.responseText);
				this.southPanel.getBottomToolbar().items.get(1).setText(
						result.message);
			},
			failure : function(response) {
				if (response.responseText)
					this.southPanel.getBottomToolbar().items.get(1).setText(
							"统计失败");
			},
			scope : this
		});
	},
	clear : function() {
		this.getEl().mask('清除中，请稍等...');
		Ext.Ajax.request({
			url : 'recognize-stat!delete.action',
			success : function(resp) {
				this.getEl().unmask();
				Dashboard.setAlert('清除成功！');
				this.reload();
			},
			failure : function(resp) {
				this.getEl().unmask();
				Ext.Msg.alert('错误', resp.responseText);
			},
			scope : this
		});

	},
	reload : function() {
		this.store.reload({
			params : {
				category : categoryCombo.getValue(),
				status : regStatusCombo.getValue()
			},
			scope : this
		});
	}
});
