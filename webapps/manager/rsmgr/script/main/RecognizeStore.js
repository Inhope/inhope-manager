RecognizeStore = function(_cfg) {

	this.type = _cfg.type;

	var loadGridProxy = new Ext.data.HttpProxy({
		url : 'recognize-stat!listTags.action?type=' + this.type
	});

	var dataReader = new Ext.data.JsonReader({
		root : 'data',
		fields : [ {
			name : 'name',
			type : 'string'
		}, {
			name : 'title',
			type : 'string'
		} ]
	});

	var cfg = {
		proxy : loadGridProxy,
		reader : dataReader,
		remoteSort : true,
		autoLoad : false,
		writer : new Ext.data.JsonWriter()
	};

	RecognizeStore.superclass.constructor.call(this, Ext
			.applyIf(_cfg || {}, cfg));
}

Ext.extend(RecognizeStore, Ext.data.Store, {
	loadDatas : function() {
		var store = this;
		store.setBaseParam('name', this.name);
		store.load();
	}
})
