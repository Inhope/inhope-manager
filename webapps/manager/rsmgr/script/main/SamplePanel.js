SamplePanel = function() {
	var self = this;
	self.selectRow = 0;
	this.data = {
		items : []
	};
	this.CategoryRecord = Ext.data.Record.create([ 'id', 'name' ]);
	this.categoryProxy = new LocalProxy({});
	var _categoryGridPanel = this.categoryGridPanel = new Ext.grid.EditorGridPanel(
			{
				id : 'categoryGridPanel',
				region : 'west',
				width : 310,
				split : true,
				columnLines : true,
				collapsible : true,
				collapseMode : 'mini',
				style : 'border-right: 1px solid ' + sys_bdcolor,
				border : false,
				title : '样例分类',
				subgridLoader : {
					load : self.loadItems,
					scope : self
				},
				store : new Ext.data.Store({
					proxy : self.categoryProxy,
					reader : new Ext.data.JsonReader({
						idProperty : 'id',
						root : 'data',
						fields : self.CategoryRecord
					})
				}),
				tbar : [
						{
							text : '新增',
							iconCls : 'icon-table-add',
							handler : function() {
								var category = _categoryGridPanel.getStore().recordType;
								var c = new category({
									name : ''
								});
								_categoryGridPanel.stopEditing();
								_categoryGridPanel.getStore().insert(0, c);
								_categoryGridPanel.startEditing(0, 0);
							}
						}, {
							text : '刷新',
							iconCls : 'icon-refresh',
							handler : function() {
								self.loadData();
							}
						}, {
							text : '导入',
							iconCls : 'icon-ontology-import',
							handler : function() {
								self.importData();
							}
						}, {
							text : '导出',
							iconCls : 'icon-ontology-export',
							handler : function() {
								self.exportData();
							}
						}, {
							text : '删除',
							iconCls : 'icon-table-delete',
							handler : function() {
								self.deleteRes(1);
							}
						} ],
				sm : new Ext.grid.RowSelectionModel({
					singleSelect : true
				}),
				loadMask : true,
				autoExpandColumn : 'nameCol',
				colModel : new Ext.grid.ColumnModel({
					columns : [ new Ext.grid.RowNumberer(), {
						header : '编号',
						id : 'idCol',
						dataIndex : 'id',
						hidden : true
					}, {
						header : '样例分类',
						id : 'nameCol',
						dataIndex : 'name',
						editor : new Ext.form.TextField({
							allowBlank : false
						})
					} ]
				}),
				listeners : {
					afteredit : function(e) {
						// self.save(self.data.data[e.row]);
						self.editCategory(e.record.get("id"), e.record
								.get("name"))
					}
				}
			});

	_categoryGridPanel.getStore().on('load', function() {
		this.getSelectionModel().selectRow(self.selectRow);
	}, _categoryGridPanel)
	_categoryGridPanel.getSelectionModel().on('rowselect',
			function(sm, rowIdx, rec) {
				var sel = sm.getSelected();
				self.selectRow = rowIdx;
				// var currId = sel.get('id');
				self.loadItems(rec);
			});

	this.ItemRecord = Ext.data.Record.create([ 'id', 'content', 'priority' ]);
	this.itemProxy = new LocalProxy({});
	var _itemPanel = this.itemGridPanel = new Ext.grid.EditorGridPanel(
			{
				id : 'itemPanel',
				region : 'center',
				title : '样例分类详情',
				columnLines : true,
				style : 'border-left: 1px solid ' + sys_bdcolor + ' border-right: 1px solid ' + sys_bdcolor,
				border : false,
				store : new Ext.data.Store({
					proxy : self.itemProxy,
					reader : new Ext.data.JsonReader({
						idProperty : 'id',
						root : 'items',
						fields : self.ItemRecord
					}),
					writer : new Ext.data.JsonWriter()
				}),
				tbar : [ {
					text : '新增',
					iconCls : 'icon-table-add',
					handler : function() {
						var s = _itemPanel.getStore();
						var r = new s.recordType({}, Ext.id());
						s.insert(0, r);
					}
				},
				// {
				// iconCls : 'icon-refresh',
				// text : '刷新',
				// handler : function() {
				// self.itemGridPanel.getStore().reload();
				// }
				// },
				{
					text : '删除',
					iconCls : 'icon-table-delete',
					handler : function() {
						self.deleteRes(2);
					}
				} ],
				sm : new Ext.grid.RowSelectionModel({
				// singleSelect : true
				}),
				loadMask : true,
				autoExpandColumn : 'content',
				colModel : new Ext.grid.ColumnModel(
						{
							columns : [
									new Ext.grid.RowNumberer(),
									{
										header : '样例ID',
										dataIndex : 'id',
										hidden : true
									},
									{
										header : '样例语句',
										id : 'content',
										dataIndex : 'content',
										editor : new Ext.form.TextField({
											allowBlank : false
										})
									},
									{
										width : 80,
										header : '操作',
										dataIndex : 'operations',
										renderer : function(v, meta, rec) {
											var ret = "<a class='_uploadSample' href='javascript:void(0);' onclick='javscript:return false;'>上传语音</a>";
											return ret;
										}
									} ]
						}),
				listeners : {
					afteredit : function(e) {
						self.editItem(_categoryGridPanel.getSelectionModel()
								.getSelected().get("id"), e.record.get("id"),
								e.record.get("content"))
					}
				}
			});
	_itemPanel.on('cellclick', function(grid, row, col, e) {
		var btn = e.getTarget('._uploadSample');
		if (btn) {
			var rec = this.getStore().getAt(row);
			self.uploadDataWin.show();
			self.uploadDataWin.form.getForm().loadRecord(rec);
		}
	}, _itemPanel);
	var _uploadDataForm = new Ext.form.FormPanel({
		fileUpload : true,
		frame : false,
		border : false,
		labelWidth : 70,
		autoHeight : true,
		waitMsgTarget : true,
		bodyStyle : 'padding : 3px 10px; background-color:' + sys_bgcolor,
		items : [ new Ext.form.Hidden({
			name : 'id'
		}), {
			xtype : 'fileuploadfield',
			emptyText : '请选择wav文件或zip压缩包',
			allowBlank : false,
			fieldLabel : '选择文件',
			name : 'data',
			anchor : '98%',
			buttonText : '',
			buttonCfg : {
				iconCls : 'icon-upload'
			},
			validator : function(v) {
				if (v) {
					if (/zip$|wav$/i.test(v))
						return true;
					else
						return '文件格式不正确';
				}
				return false;
			}
		} ],
		tbar : [ {
			text : '上传',
			iconCls : 'icon-ontology-import',
			handler : function() {
				var btn = this;
				btn.disable();
				if (_uploadDataForm.getForm().isValid()) {
					_uploadDataForm.getForm().submit({
						url : 'sample-data!save.action',
						waitMsg : '正在上传...',
						success : function(form, act) {
							Dashboard.setAlert('上传成功！');
							btn.enable();
							_uploadDataWin.hide();
						},
						failure : function(form, act) {
							Ext.Msg.alert('错误', act.result.message);
							btn.enable();
						}
					});
				} else {
					Ext.Msg.alert('错误', '您尚未正确完整的输入表单内容，请检查！', function() {
						btn.enable();
					});
				}
			}
		} ]
	});
	var _uploadDataWin = new Ext.Window({
		title : '上传采样语音',
		width : 420,
		items : [ _uploadDataForm ],
		listeners : {
			'beforehide' : function(p) {
				p.form.getForm().reset();
			}
		},
		defaults : {
			border : false
		},
		modal : true,
		plain : true,
		shim : true,
		closable : true,
		closeAction : 'hide',
		collapsible : true,
		resizable : false,
		draggable : true,
		animCollapse : true,
		constrainHeader : true,
		shadow : false
	});
	this.uploadDataWin = _uploadDataWin;
	_uploadDataWin.form = _uploadDataForm;
	this.uploadDataForm = _uploadDataForm;

	var _fileForm = new Ext.FormPanel({
		layout : "fit",
		frame : true,
		border : false,
		autoHeight : true,
		waitMsgTarget : true,
		defaults : {
			bodyStyle : 'padding:10px'
		},
		margins : '0 0 0 0',
		labelAlign : "left",
		labelWidth : 50,
		fileUpload : true,
		items : [ {
			xtype : 'fieldset',
			title : '选择文件',
			autoHeight : true,
			items : [ {
				id : 'sampleUploadFileId',
				name : 'uploadFile',
				xtype : "textfield",
				fieldLabel : '文件',
				inputType : 'file',
				anchor : '96%'
			} ]
		}, {
			xtype : 'fieldset',
			title : '设置参数',
			autoHeight : true,
			items : [ {
				name : 'cleanOrAdd',
				xtype : 'checkbox',
				hideLabel : true,
				boxLabel : '是否清空所有',
				inputValue : true,
				checked : true
			} ]
		} ]
	});
	this.fileForm = _fileForm;

	var _importPanel = new Ext.Panel(
			{
				layout : "fit",
				layoutConfig : {
					animate : true
				},
				items : [ _fileForm ],
				buttons : [
						{
							id : "btn_import_wordclass",
							text : "开始导入",
							handler : function() {
								if (!Ext.getCmp("sampleUploadFileId")
										.getValue()) {
									Ext.Msg.alert("错误提示", "请选择你要上传的文件");
									return;
								} else {
									this.disable();
									var btn = this;
									// 开始导入
									var sampleForm = self.fileForm.getForm();
									if (sampleForm.isValid()) {
										sampleForm
												.submit({
													params : {

													},
													url : 'sample!importSample.action',
													success : function(form,
															action) {
														btn.enable();
														var result = action.result.data;
														var timer = new ProgressTimer(
																{
																	initData : result,
																	progressId : 'importSampleStatus',
																	boxConfig : {
																		title : '正在导入样例...'
																	},
																	finish : function(
																			p,
																			response) {
																		if (p.currentCount == -2) {
																			// 下载验证失败的信息
																			if (!self.downloadIFrame) {
																				self.downloadIFrame = self
																						.getEl()
																						.createChild(
																								{
																									tag : 'iframe',
																									style : 'display:none;'
																								})
																			}
																			self.downloadIFrame.dom.src = "sample!downExportFile.action?_t="
																					+ new Date()
																							.getTime();
																		}
																		self
																				.loadData();
																	}
																});
														timer.start();
													},
													failure : function(form,
															action) {
														btn.enable();
														Ext.MessageBox.hide();
														Ext.Msg
																.alert(
																		'错误',
																		'导入初始化失败.'
																				+ (action.result.message ? '详细错误:'
																						+ action.result.message
																						: ''));
													}
												});
									}
								}
							}
						}, {
							text : "关闭",
							handler : function() {
								self.importSampleWin.hide();
							}
						} ]
			});

	var importSampleWin = new Ext.Window({
		width : 520,
		title : '样例导入',
		defaults : {// 表示该窗口中所有子元素的特性
			border : false
		// 表示所有子元素都不要边框
		},

		plain : true,// 方角 默认

		modal : true,
		plain : true,
		shim : true,

		closeAction : 'hide',
		collapsible : true,// 折叠
		closable : true, // 关闭
		resizable : false,// 改变大小
		draggable : true,// 拖动
		minimizable : false,// 最小化
		maximizable : false,// 最大化

		animCollapse : true,
		constrainHeader : true,
		autoHeight : false,
		items : [ _importPanel ]
	});

	this.importSampleWin = importSampleWin;

	var config = {
		id : 'samplePanel',
		layout : 'border',
		border : false,
		items : [ _categoryGridPanel, _itemPanel ]
	};
	SamplePanel.superclass.constructor.call(this, config);
	self.loadData();
}

Ext
		.extend(
				SamplePanel,
				Ext.Panel,
				{
					loadData : function() {
						Ext.Ajax.request({
							url : 'sample!list.action',
							success : function(form, action) {
								this.data = Ext.util.JSON
										.decode(form.responseText);
								this._refresh();
							},
							failure : function(form, action) {
								Ext.Msg.alert('提示', '获取场景数据失败');
							},
							scope : this
						});
					},
					loadItems : function(record) {
						if (!record.data.items) {
							record.data.items = [];
						}
						this.itemProxy.data = record.data;
						this.itemGridPanel.getStore().reload();
						// this.itemGridPanel.batchAdd(20);
					},
					_refresh : function() {
						this.categoryProxy.data = this.data;
						var grid = this.categoryGridPanel;
						grid.getStore().load();
						// grid.batchAdd(30);
						// var i = grid.defaultSelected;
						// if (!i)
						// i = 0;
						// grid.getSelectionModel().selectRow(i);
					},
					editCategory : function(id, name) {
						this.getEl().mask('保存...');
						Ext.Ajax.request({
							url : 'sample!editCategory.action',
							params : {
								id : id,
								name : name
							},
							success : function(form, action) {
								this.getEl().unmask();
								Dashboard.setAlert("保存成功!");
								var data = Ext.util.JSON
										.decode(form.responseText);
								// alert(data)
								// this.data = data;
								// this._refresh();
								this.loadData();
							},
							failure : function(response) {
								this.getEl().unmask();
								Ext.Msg.alert("错误", response.responseText);
							},
							scope : this
						});
					},
					editItem : function(cid, id, content) {
						this.getEl().mask('保存...');
						Ext.Ajax.request({
							url : 'sample!editItem.action',
							params : {
								cid : cid,
								id : id,
								content : content
							},
							success : function(form, action) {
								this.getEl().unmask();
								Dashboard.setAlert("保存成功!");
								var data = Ext.util.JSON
										.decode(form.responseText);
								// this._refresh();
								this.loadData();
							},
							failure : function(response) {
								this.getEl().unmask();
								Ext.Msg.alert("错误", response.responseText);
							},
							scope : this
						});
					},
					importData : function() {
						this.importSampleWin.show();
					},
					exportData : function() {
						var self = this;
						// 开始下载
						Ext.Ajax
								.request({
									url : 'sample!exportSample.action',
									success : function(response) {
										var fileName = Ext.util.JSON
												.decode(response.responseText).message;
										var params = Ext.urlEncode({
											'filename' : fileName
										})
										var result = Ext.util.JSON
												.decode(response.responseText);
										if (result.success) {
											var timer = new ProgressTimer(
													{
														initData : result.data,
														progressId : 'exportSampleStatus',
														boxConfig : {
															title : '正在导出样例...'
														},
														finish : function() {
															if (!self.downloadIFrame) {
																self.downloadIFrame = self
																		.getEl()
																		.createChild(
																				{
																					tag : 'iframe',
																					style : 'display:none;'
																				})
															}
															self.downloadIFrame.dom.src = 'sample!downExportFile.action?'
																	+ params
																	+ '&_t='
																	+ new Date()
																			.getTime();
														}
													});
											timer.start();
										} else {
											Ext.Msg.alert('错误', '导出失败:'
													+ result.message)
										}
									},
									failure : function(response) {
										Ext.Msg.alert('错误', '导出失败')
									},
									scope : this
								});
					},

					deleteRes : function(type) {
						var rows;
						var tempGrid;
						var url = "";
						if (type == 1) {
							url = "sample!deleteCategory.action";
							tempGrid = this.categoryGridPanel;
						} else if (type == 2) {
							url = "sample!deleteItem.action";
							tempGrid = this.itemGridPanel;
						}
						rows = tempGrid.selModel.getSelections();
						if (rows.length == 0) {
							Ext.Msg.alert('提示', '请先选择要删除的记录！');
							return false;
						}
						var self = this;
						Ext.Msg.confirm('确定框', '确定要删除这' + rows.length + '记录吗？',
								function(sel) {
									if (sel == 'yes') {
										var ids = [];
										Ext.each(rows, function(_item) {
											ids.push("'" + _item.get("id")
													+ "'")
										})
										Ext.Ajax.request({
											url : url,
											params : {
												ids : ids.join(',')
											},
											success : function(resp) {
												Dashboard.setAlert('删除成功！')
												// tempGrid.getStore().reload();
												self.loadData();
											},
											failure : function(resp) {
												Ext.Msg.alert('错误',
														resp.responseText)
											}
										});
									}
								});
					}
				});
