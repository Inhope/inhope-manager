ScenarioPanel = function(cfg) {
	cfg = cfg || {};
	var self = this;
	this.data = {
		tags : []
	};
	this.ScenarioRecord = Ext.data.Record.create([ 'id', 'title', 'name',
			'render' ]);
	this.scenarioProxy = new LocalProxy({});
	var scenarioEditGrid = this.scenarioEditGrid = new ExcelLikeGrid({
		flex : 5,
		style : 'border-right: 1px solid ' + sys_bdcolor,
		border : false,
		clicksToEdit : 1,
		frame : false,
		columnLines : true,
		selModel : new Ext.grid.RowSelectionModel(),
		subgridLoader : {
			load : self.loadTags,
			scope : self
		},
		pasteable : true,
		columns : [ new Ext.grid.RowNumberer(), {
			header : '场景代码',
			dataIndex : 'name',
			editor : new Ext.form.TextField()
		}, {
			header : '场景名称',
			dataIndex : 'title',
			editor : new Ext.form.TextField()
		}, {
			header : '渲染方式',
			dataIndex : 'render',
			editor : new Ext.form.TextField()
		}, new DeleteButtoner() ],
		store : new Ext.data.Store({
			proxy : self.scenarioProxy,
			reader : new Ext.data.JsonReader({
				idProperty : 'id',
				root : 'data',
				fields : self.ScenarioRecord
			}),
			writer : new Ext.data.JsonWriter()
		}),
		viewConfig : {
			forceFit : true
		}
	});

	this.TagRecord = Ext.data.Record
			.create([ 'id', 'title', 'name', 'priority' ]);
	this.tagProxy = new LocalProxy({});
	var tagEditGrid = this.tagEditGrid = new ExcelLikeGrid({
		anchor : '100% -60',
		border : true,
		clicksToEdit : 1,
		frame : false,
		columnLines : true,
		selModel : new Ext.grid.RowSelectionModel(),
		pasteable : true,
		columns : [ new Ext.grid.RowNumberer(), {
			header : 'TagID',
			dataIndex : 'name',
			editor : new Ext.form.TextField()
		}, {
			header : 'Tag名称',
			dataIndex : 'title',
			editor : new Ext.form.TextField()
		}, new DeleteButtoner() ],
		store : new Ext.data.Store({
			proxy : self.tagProxy,
			reader : new Ext.data.JsonReader({
				idProperty : 'id',
				root : 'tags',
				fields : self.TagRecord
			}),
			writer : new Ext.data.JsonWriter()
		}),
		viewConfig : {
			forceFit : true
		}
	});

	var tagForm = this.tagForm = new Ext.form.FormPanel({
		flex : 3,
		margins : '0 0 0 5',
		padding : 8,
		style : 'border-left: 1px solid ' + sys_bdcolor,
		border : false,
		frame : false,
		labelAlign : 'top',
		bodyStyle : 'background-color:' + sys_bgcolor,
		items : [ {
			xtype : 'textfield',
			anchor : '100%',
			fieldLabel : '场景名称',
			ref : 'scenarioNameField',
			listeners : {
				blur : function() {
					var r = scenarioEditGrid.getSelectionModel().getSelected();
					if (r)
						r.set("name", this.getValue())
				}
			}
		}, tagEditGrid ]
	});

	ScenarioPanel.superclass.constructor.call(this, {
		border : false,
		frame : false,
		layout : {
			type : 'hbox',
			align : 'stretch'
		},
		items : [ scenarioEditGrid, tagForm ],
		buttonAlign : 'center',
		tbar : [ {
			text : '保存',
			iconCls : 'icon-table-save',
			handler : function() {
				self.save();
			}
		}, {
			text : '刷新',
			iconCls : 'icon-refresh',
			handler : function() {
				self.loadData();
			}
		} ]
	});

	scenarioEditGrid.on("scrollbottomclick", function(e) {
		scenarioEditGrid.batchAdd(28);
	});
	tagEditGrid.on("scrollbottomclick", function(e) {
		tagEditGrid.batchAdd(18);
	});

	scenarioEditGrid.on("shiftright", function(e) {
		var g = tagEditGrid
		var col = g.findMostLeftEditCol()
		g.getSelectionModel().selectRow(0)
		this.stopEditing()
		g.startEditing(0, col)
	});
	tagEditGrid.on("shiftleft", function(e) {
		var g = scenarioEditGrid
		var col = g.findMostRightEditCol()
		var r = g.getSelectionModel().getSelected()
		var row = g.getStore().indexOf(r)
		this.stopEditing()
		g.startEditing(row, col)
	});

};

Ext.extend(ScenarioPanel, Ext.Panel, {
	loadData : function() {
		Ext.Ajax.request({
			url : 'scenario!list.action',
			success : function(form, action) {
				this.data = Ext.util.JSON.decode(form.responseText);
				this._refresh();
			},
			failure : function(form, action) {
				Ext.Msg.alert('提示', '获取场景数据失败');
			},
			scope : this
		});
	},
	loadTags : function(record) {
		this.tagForm.scenarioNameField.setValue(record.data.name)
		if (!record.data.tags) {
			record.data.tags = [];
		}
		this.tagProxy.data = record.data
		this.tagEditGrid.getStore().reload();
		this.tagEditGrid.batchAdd(20);
	},
	_refresh : function() {
		this.scenarioProxy.data = this.data;
		var grid = this.scenarioEditGrid;
		grid.getStore().load();
		grid.batchAdd(30);
		var i = grid.defaultSelected
		if (!i)
			i = 0;
		grid.getSelectionModel().selectRow(i);
	},
	save : function() {
		this.getEl().mask('保存...')
		Ext.Ajax.request({
			url : 'scenario!save.action',
			params : {
				data : Ext.encode(this.data.data)
			},
			success : function(form, action) {
				this.getEl().unmask()
				Dashboard.setAlert("保存成功!");
				var data = Ext.util.JSON.decode(form.responseText);
				this.data = data;
				this._refresh();
			},
			failure : function(response) {
				this.getEl().unmask()
				Ext.Msg.alert("错误", response.responseText);
			},
			scope : this
		});
	}
});
