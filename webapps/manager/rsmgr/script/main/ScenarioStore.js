ScenarioStore = function(_cfg) {

	this.name = _cfg.name;

	var loadGridProxy = new Ext.data.HttpProxy({
		url : 'scenario!listTags.action?name=' + this.name
	});

	var dataReader = new Ext.data.JsonReader({
		root : 'data',
		fields : [ {
			name : 'name',
			type : 'string'
		}, {
			name : 'title',
			type : 'string'
		} ]
	});

	var cfg = {
		proxy : loadGridProxy,
		reader : dataReader,
		remoteSort : true,
		autoLoad : false,
		writer : new Ext.data.JsonWriter()
	};

	ScenarioStore.superclass.constructor.call(this, Ext
			.applyIf(_cfg || {}, cfg));
}

Ext.extend(ScenarioStore, Ext.data.Store, {
	loadDatas : function() {
		var store = this;
		store.setBaseParam('name', this.name);
		store.load();
	}
})
