SpeechAssistWordPanel = function(cfg) {
	cfg = cfg || {};
	var self = this;
	this.pageSize = 20;
	this.store;
	this.matchIndex = 0;
	this.SpeechAssistWordRecord = Ext.data.Record.create(['id', 'isEdit',
			'name', 'time']);
	this.SpeechAssistWordProxy = new Ext.data.HttpProxy({
				url : 'speech-assist-word!list.action'
			});
	var _store = new Ext.data.Store({
				proxy : self.SpeechAssistWordProxy,
				reader : new Ext.data.JsonReader({
							totalProperty : 'total',
							idProperty : 'id',
							root : 'data',
							fields : self.SpeechAssistWordRecord
						})
			});
	var seleMode = new Ext.grid.RowSelectionModel();
	this.seleMode = seleMode;
	var speechAssistWordEditGrid = this.speechAssistWordEditGrid = new Ext.grid.EditorGridPanel(
			{
				flex : 4,
				style : 'border-right: 1px solid ' + sys_bdcolor,
				border : false,
				clicksToEdit : 1,
				frame : false,
				columnLines : true,
				selModel : this.seleMode,
				pasteable : true,
				colModel : new Ext.grid.ColumnModel({
							defaults : {
								sortable : true
							},
							columns : [new Ext.grid.RowNumberer(),
									new Ext.form.Hidden({
												dataIndex : 'isEdit',
												width : 0,
												hidden : true
											}), {
										header : '内容',
										dataIndex : 'name',
										width : 7,
										editor : new Ext.form.TextField()
									}, {
										header : '时间',
										dataIndex : 'time',
										width : 3,
										readOnly : true
									}]
						}),
				store : _store,
				bbar : new Ext.PagingToolbar({
							store : _store,
							displayInfo : true,
							pageSize : this.pageSize,
							beforePageText : '第',
							afterPageText : '页，共{0}页',
							displayMsg : '第{0}到{1}条记录，共{2}条',
							emptyMsg : "没有记录"
						}),
				viewConfig : {
					forceFit : true
				},
				listeners : {
					afteredit : function(e) {
						e.record.set('isEdit', 1);
					}
				},
				scope : this
			});
	_store.baseParams['limit'] = this.pageSize;
	var tbarEx = new Ext.Toolbar({
				items : [{
							text : '搜索',
							ref : 'searchBtn',
							xtype : 'splitbutton',
							iconCls : 'icon-search',
							handler : function() {
								self.searchWord();
							},
							menu : {
								items : [{
											text : '精确查询',
											checked : false,
											name : 'precise',
											ref : '../precise'
										}]
							}
						}, '-', '内容:', {
							ref : 'searchSpeechAssistWordname',
							xtype : 'textfield',
							emptyText : '输入内容搜索...',
							listeners : {
								specialkey : function(field, e) {
									if (e.getKey() == Ext.EventObject.ENTER) {
										self.searchWord();
									}
								}
							}
						}, '编辑时间:从', {
							ref : 'searchSpeechAssistWordBdate',
							xtype : 'datefield',
							emptyText : '开始时间',
							width : 150,
							format : 'Y-m-d',
							listeners : {
								specialkey : function(field, e) {
									if (e.getKey() == Ext.EventObject.ENTER) {
										self.searchWord();
									}
								}
							}
						}, '到', {
							ref : 'searchSpeechAssistWordEdate',
							xtype : 'datefield',
							width : 150,
							emptyText : '结束时间',
							format : 'Y-m-d',
							listeners : {
								specialkey : function(field, e) {
									if (e.getKey() == Ext.EventObject.ENTER) {
										self.searchWord();
									}
								}
							}
						}]
			});
	this.tbarEx = tbarEx;
	SpeechAssistWordPanel.superclass.constructor.call(this, {
				border : false,
				frame : false,
				layout : {
					type : 'hbox',
					align : 'stretch'
				},
				items : [speechAssistWordEditGrid],
				buttonAlign : 'center',
				tbar : [{
							text : '删除',
							iconCls : 'icon-table-delete',
							handler : function() {
								self.deleteRes();
							}
						}, {
							text : '新增',
							ref : 'addSpeechAssistWordRow',
							xtype : 'splitbutton',
							iconCls : 'icon-table-add',
							menu : {
								items : [{
											xtype : 'textfield',
											name : 'addRowSize',
											fieldLabel : '数量',
											width : 60,
											value : '1',
											ref : '../addRowSize'
										}]
							},
							handler : function() {
								var addSize = this.addRowSize.getValue()
								if (addSize > 20) {
									Ext.Msg.alert("提示", "新增数量不能超过 20");
									return;
								}
								self.addRec(addSize);
							}
						}, {
							text : '刷新',
							iconCls : 'icon-refresh',
							handler : function() {
								self.refresh();
							}
						}, {
							text : '导入数据',
							iconCls : 'icon-ontology-import',
							width : 50,
							handler : function() {
								self.importAssistWordWin.show();
							}
						}, {
							text : '导出数据',
							iconCls : 'icon-ontology-export',
							width : 50,
							handler : function() {
								self.exportData();
							}
						}, {
							text : '同步',
							iconCls : 'icon-wordclass-root-sync',
							xtype : 'splitbutton',
							menu : {
								items : [{
											// xtype : 'checkbox',
											text : '增量',
											ref : '../addSync',
											checked : false

										}]
							},
							handler : function() {
								var addSync = this.addSync.checked;
								self.syncAll(addSync);
							}
						},{
							text : '保存',
							ref : 'save',
							iconCls : 'icon-table-save',
							handler : function() {
								self.allSave();
							}
						}],
				listeners : {
					'render' : function() {
						tbarEx.render(this.tbar)
					}
				}
			});
	var _fileForm = new Ext.FormPanel({
				layout : "fit",
				frame : true,
				border : false,
				autoHeight : true,
				waitMsgTarget : true,
				defaults : {
					bodyStyle : 'padding:10px'
				},
				margins : '0 0 0 0',
				labelAlign : "left",
				labelWidth : 50,
				fileUpload : true,
				items : [{
							xtype : 'fieldset',
							title : '选择文件',
							autoHeight : true,
							items : [{
										name : 'uploadFile',
										xtype : "textfield",
										ref : '../uploadFile',
										fieldLabel : '文件',
										inputType : 'file',
										anchor : '96%'
									}]
						}, {
							xtype : 'fieldset',
							title : '设置参数',
							autoHeight : true,
							items : [{
										name : 'cleanContent',
										xtype : 'checkbox',
										ref : '../cleanImport',
										hideLabel : true,
										boxLabel : '是否清空内容',
										inputValue : true,
										checked : true
									}]
						}]
			});
	this.fileForm = _fileForm;
	var _importPanel = new Ext.Panel({
		layout : "fit",
		layoutConfig : {
			animate : true
		},
		items : [_fileForm],
		buttons : [{
			text : "开始导入",
			handler : function() {
				if (!self.fileForm.uploadFile.getValue()) {
					Ext.Msg.alert("错误提示", "请选择你要上传的文件");
				} else {
					var dirs = self.fileForm.uploadFile.getValue().split('.');
					if ('txt' !== dirs[dirs.length - 1]) {
						Ext.Msg.alert('错误提示', '文件格式只能是txt文本类型');
						return;
					}
					this.disable();
					var btn = this;
					// 开始导入
					var keyclassForm = self.fileForm.getForm();
					if (keyclassForm.isValid()) {
						keyclassForm.submit({
							params : {
								'type' : self.type,
								'cleanImport' : self.fileForm.cleanImport.checked
							},
							url : 'speech-assist-word!importData.action',
							success : function(form, action) {
								btn.enable();
								var result = action.result.data;
								var timer = new ProgressTimer({
									initData : result,
									progressId : 'importSpeechAssistWordStatus',
									boxConfig : {
										title : 1 == self.type
												? '正在导入英语缩写词...'
												: '正在导入英文单词...'
									},
									finish : function(p, response) {
										self.refresh();
									}
								});
								timer.start();
							},
							failure : function(form, action) {
								btn.enable();
								Ext.Msg.alert('错误', '导入初始化失败.'
												+ action.result.message);
							}
						});
					}
				}
			}
		}, {
			text : "关闭",
			handler : function() {
				self.importAssistWordWin.hide();
			}
		}]
	});

	var importAssistWordWin = new Ext.Window({
				width : 400,
				title : '语音词类导入',
				defaults : {// 表示该窗口中所有子元素的特性
					border : false
					// 表示所有子元素都不要边框
				},
				plain : true,// 方角 默认
				modal : true,
				plain : true,
				shim : true,
				closeAction : 'hide',
				collapsible : true,// 折叠
				closable : true, // 关闭
				resizable : false,// 改变大小
				draggable : true,// 拖动
				minimizable : false,// 最小化
				maximizable : false,// 最大化
				animCollapse : true,
				constrainHeader : true,
				autoHeight : false,
				items : [_importPanel]
			});
	this.importAssistWordWin = importAssistWordWin
	this.type;
};

Ext.extend(SpeechAssistWordPanel, Ext.Panel, {
	loadWords : function(type) {
		this.type = type;
		var store = this.speechAssistWordEditGrid.getStore();
		for (var key in store.baseParams) {
			if (key && key.indexOf('queryParam.') != -1) {
				delete store.baseParams[key];
			} else {
				this.mask = false;
			}
		}
		store.setBaseParam('type', type);
		store.load({
					params : {
						start : 0,
						limit : this.pageSize
					}
				});
		this.store = store;
	},
	refresh : function() {
		this.loadWords(this.type);
	},
	deleteRes : function() {
		var self = this
		var rows = this.seleMode.getSelections();
		if (rows.length == 0) {
			Ext.Msg.alert('提示', '请先选择要删除的记录！');
			return false;
		}
		var self = this;
		Ext.Msg.confirm('确定框', '确定要删除这' + rows.length + '记录吗？', function(sel) {
					if (sel == 'yes') {
						var ids = [];
						Ext.each(rows, function(_item) {
									ids.push(_item.id)
								})
						Ext.Ajax.request({
									url : 'speech-assist-word!deleteByIds.action',
									params : {
										ids : ids.join(','),
										type : self.type
									},
									success : function(resp) {
										Dashboard.setAlert('删除成功！')
										self.speechAssistWordEditGrid
												.getStore().reload();
									},
									failure : function(resp) {
										Ext.Msg.alert('错误', resp.responseText)
									}
								});
					}
				});
	},
	addRec : function(size) {
		var s = this.speechAssistWordEditGrid.getStore();
		var c = s.getTotalCount();
		for (var i = 0; i < size; i++) {
			s.add(new s.recordType({}, Ext.id()));
		}
	},
	allSave : function() {
		var self = this;
		var res = 0;
		var isEmpty = false;
		var resData = [];
		Ext.each(this.speechAssistWordEditGrid.getStore().getRange(), function(
						record) {
					res++;
					var name = record.data["name"];
					var isEdit = record.data["isEdit"];
					record.data.type = self.type;
					if (isEdit == 1) {
						resData.push(record.data);
					}
				});

		if (isEmpty) {
			return;
		}
		resData = Ext.encode(resData);
		if (resData.length <= 0) {
			Ext.Msg.alert("提示", "修改或新增的数据为空");
			return;
		}
		this.getEl().mask('保存...');
		Ext.Ajax.request({
					url : 'speech-assist-word!save.action',
					params : {
						data : resData,
						type : self.type
					},
					success : function(form, action) {
						this.getEl().unmask()
						var obj = Ext.util.JSON.decode(form.responseText);
						if (!obj.success) {
							Ext.Msg.alert("错误", obj.message);
							return;
						}
						Dashboard.setAlert("保存成功!");
						self.speechAssistWordEditGrid.getStore().reload();
					},
					failure : function(response) {
						this.getEl().unmask()
						Ext.Msg.alert("错误", response.responseText);
					},
					scope : this
				});
	},
	searchWord : function() {
		var self = this;
		var _name = this.tbarEx.searchSpeechAssistWordname.getValue().trim();
		var _bdate = this.tbarEx.searchSpeechAssistWordBdate.getValue();
		var _edate = this.tbarEx.searchSpeechAssistWordEdate.getValue();
		var store = self.speechAssistWordEditGrid.getStore();
		for (var key in store.baseParams) {
			if (key && key.indexOf('queryParam.') != -1)
				delete store.baseParams[key];
		}
		store.setBaseParam('type', self.type)
		if (_name)
			store.setBaseParam('queryParam.name', _name);
		if (_bdate)
			store.setBaseParam('queryParam.beginDate', _bdate);
		if (_edate)
			store.setBaseParam('queryParam.endDate', _edate);
		if (this.tbarEx.searchBtn.precise.checked)
			store.setBaseParam('queryParam.preciseSearch', true)
		store.load();
	},
	exportData : function() {
		var self = this;
		Ext.Ajax.request({
			url : 'speech-assist-word!exportData.action',
			params : {
				'type' : self.type
			},
			success : function(response) {
				var result = Ext.util.JSON.decode(response.responseText);
				if (result.success) {
					var timer = new ProgressTimer({
						initData : result.data,
						progressId : 'exportSpeechAssistWordStatus',
						boxConfig : {
							title : 1 == self.type
									? '正在导出英语缩写词...'
									: '正在导出英文单词...'
						},
						finish : function() {
							if (!self.downloadIFrame) {
								self.downloadIFrame = self.getEl().createChild(
										{
											tag : 'iframe',
											style : 'display:none;'
										})
							}
							self.downloadIFrame.dom.src = 'speech-assist-word!downExportFile.action?'
									+ '_t=' + new Date().getTime();
						}
					});
					timer.start();
				} else {
					Ext.Msg.alert('错误', '导出失败:' + result.message)
				}
			},
			failure : function(response) {
				Ext.Msg.alert('错误', '导出失败')
			},
			scope : this

		});

	},
	syncAll : function(addSync) {
		var self = this;
		Ext.Ajax.request({
					url : 'speech-assist-word!syncWords.action',
					params : {
						type : self.type,
						addSync : addSync
					},
					success : function(form, action) {
						this.getEl().unmask()
						var obj = Ext.util.JSON.decode(form.responseText);
						if (!obj.success) {
							Ext.Msg.alert("错误", obj.message);
							return;
						}
						Dashboard.setAlert("同步成功!");
					},
					failure : function(response) {
						this.getEl().unmask()
						Ext.Msg.alert("错误", response.responseText);
					},
					scope : this
				});
	}
});
