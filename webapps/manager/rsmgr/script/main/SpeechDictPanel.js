SpeechDictPanel = function(cfg) {
	cfg = cfg || {};
	var self = this;
	this.data = {
		items : []
	};
	this.DictRecord = Ext.data.Record.create(['id', 'name']);
	this.dictProxy = new LocalProxy({});
	var categoryEditGrid = this.categoryEditGrid = new ExcelLikeGrid({
				flex : 5,
				style : 'border-right: 1px solid ' + sys_bdcolor,
				border : false,
				clicksToEdit : 1,
				frame : false,
				columnLines : true,
				selModel : new Ext.grid.RowSelectionModel(),
				subgridLoader : {
					load : self.loadItems,
					scope : self
				},
				pasteable : true,
				columns : [new Ext.grid.RowNumberer(), {
							header : '词典内容',
							dataIndex : 'name',
							editor : new Ext.form.TextField()
						}, new DeleteButtoner()],
				store : new Ext.data.Store({
							proxy : self.dictProxy,
							reader : new Ext.data.JsonReader({
										idProperty : 'id',
										root : 'data',
										fields : self.DictRecord
									}),
							writer : new Ext.data.JsonWriter()
						}),
				viewConfig : {
					forceFit : true
				}
			});

	this.ItemRecord = Ext.data.Record.create(['id', 'content', 'priority']);
	this.itemProxy = new LocalProxy({});
	var rowNumber = new Ext.grid.RowNumberer()
	var itemEditGrid = this.itemEditGrid = new ExcelLikeGrid({
				anchor : '100% -60',
				border : true,
				clicksToEdit : 1,
				frame : false,
				columnLines : true,
				selModel : new Ext.grid.RowSelectionModel(),
				pasteable : true,
				columns : [rowNumber, {
							header : '顺序',
							dataIndex : 'priority',
							hidden : true,
							editor : new Ext.form.TextField({
										disabled : true
									}),
							width : 20
						}, {
							header : '发音内容',
							dataIndex : 'content',
							editor : new Ext.form.TextField(),
							width : 80
						}, new DeleteButtoner()],
				store : new Ext.data.Store({
							proxy : self.itemProxy,
							reader : new Ext.data.JsonReader({
										idProperty : 'id',
										root : 'items',
										fields : self.ItemRecord
									}),
							writer : new Ext.data.JsonWriter(),
							listeners : {
								update : function(store, r, operation) {
									if (r.get("content").trim()
											&& r.get("priority") == null) {
										r.set('priority', store.indexOf(r) + 1);
									}
								}
							}
						}),
				viewConfig : {
					forceFit : true
				}
			});

	var itemForm = this.itemForm = new Ext.form.FormPanel({
				flex : 5,
				margins : '0 0 0 5',
				padding : 8,
				style : 'border-left: 1px solid ' + sys_bdcolor,
				border : false,
				frame : false,
				labelAlign : 'top',
				bodyStyle : 'background-color:' + sys_bgcolor,
				items : [{
					xtype : 'textfield',
					anchor : '100%',
					fieldLabel : '词典内容',
					ref : 'categoryNameField',
					listeners : {
						blur : function() {
							var r = categoryEditGrid.getSelectionModel()
									.getSelected();
							if (r)
								r.set("name", this.getValue());
						}
					}
				}, itemEditGrid]
			});

	var _fileForm = new Ext.FormPanel({
				layout : "fit",
				frame : true,
				border : false,
				autoHeight : true,
				waitMsgTarget : true,
				defaults : {
					bodyStyle : 'padding:10px'
				},
				margins : '0 0 0 0',
				labelAlign : "left",
				labelWidth : 50,
				fileUpload : true,
				items : [{
							xtype : 'fieldset',
							title : '选择文件',
							autoHeight : true,
							items : [{
										id : 'dictUploadFileId',
										name : 'uploadFile',
										xtype : "textfield",
										fieldLabel : '文件',
										inputType : 'file',
										anchor : '96%'
									}]
						}, {
							xtype : 'fieldset',
							title : '设置参数',
							autoHeight : true,
							items : [{
										name : 'cleanOrAdd',
										xtype : 'checkbox',
										hideLabel : true,
										boxLabel : '是否清空所有',
										inputValue : true,
										checked : true
									}]
						}]
			});
	this.fileForm = _fileForm;

	var _importPanel = new Ext.Panel({
		layout : "fit",
		layoutConfig : {
			animate : true
		},
		items : [_fileForm],
		buttons : [{
			text : "开始导入",
			handler : function() {
				if (!Ext.getCmp("dictUploadFileId").getValue()) {
					Ext.Msg.alert("错误提示", "请选择你要上传的文件");
					return;
				} else {
					this.disable();
					var btn = this;
					// 开始导入
					var dictForm = self.fileForm.getForm();
					if (dictForm.isValid()) {
						dictForm.submit({
							url : 'speech-dict!importSpeechDict.action',
							success : function(form, action) {
								btn.enable();
								var result = action.result.data;
								var timer = new ProgressTimer({
									initData : result,
									progressId : 'importSpeechDictStatus',
									boxConfig : {
										title : '正在导入词典...'
									},
									finish : function(p, response) {
										if (p.currentCount == -2) {
											// 下载验证失败的信息
											if (!self.downloadIFrame) {
												self.downloadIFrame = self
														.getEl().createChild({
															tag : 'iframe',
															style : 'display:none;'
														})
											}
											self.downloadIFrame.dom.src = "speech-dict!downExportFile.action?_t="
													+ new Date().getTime();
										}
										self.loadData();
									}
								});
								timer.start();
							},
							failure : function(form, action) {
								btn.enable();
								Ext.MessageBox.hide();
								Ext.Msg
										.alert(
												'错误',
												'导入初始化失败.'
														+ (action.result.message
																? '详细错误:'
																		+ action.result.message
																: ''));
							}
						});
					}
				}
			}
		}, {
			text : "关闭",
			handler : function() {
				self.importDictWin.hide();
			}
		}]
	});

	var importDictWin = new Ext.Window({
				width : 520,
				title : '词典导入',
				defaults : {// 表示该窗口中所有子元素的特性
					border : false
					// 表示所有子元素都不要边框
				},
				modal : true,
				plain : true,
				shim : true,
				closeAction : 'hide',
				collapsible : true,// 折叠
				closable : true, // 关闭
				resizable : false,// 改变大小
				draggable : true,// 拖动
				minimizable : false,// 最小化
				maximizable : false,// 最大化
				animCollapse : true,
				constrainHeader : true,
				autoHeight : false,
				items : [_importPanel]
			});

	this.importDictWin = importDictWin;
	var dictTextField = new Ext.form.TextField({
				fieldLabel : "词典",
				emptyText : '输入查询的词典内容',
				width : 150
			});
	SpeechDictPanel.superclass.constructor.call(this, {
				border : false,
				frame : false,
				layout : {
					type : 'hbox',
					align : 'stretch'
				},
				items : [categoryEditGrid, itemForm],
				buttonAlign : 'center',
				tbar : [{
							text : '刷新',
							iconCls : 'icon-refresh',
							handler : function() {
								self.loadData();
							}
						}, {
							text : '导入',
							iconCls : 'icon-ontology-import',
							handler : function() {
								self.importData();
							}
						}, {
							text : '导出',
							iconCls : 'icon-ontology-export',
							handler : function() {
								self.exportData();
							}
						}, {
							text : '同步',
							iconCls : 'icon-wordclass-root-sync',
							handler : function() {
								self.SyncAllAI();
							}
						},{
							text : '保存',
							iconCls : 'icon-table-save',
							handler : function() {
								self.save();
							}
						}, 
//						{
//							text : '辅助工具',
//							iconCls : 'icon-toolbox',
//							handler : function() {
//								_toolForm.content.setValue();
//								_toolForm.voice.setValue();
//								toolWindow.show();
//							}
//						},
						'-', dictTextField, {
							text : '搜索',
							iconCls : 'icon-search',
							handler : function() {
								self.loadData(dictTextField.getValue())
							}
						}]
			});

	categoryEditGrid.on("scrollbottomclick", function(e) {
				categoryEditGrid.batchAdd(28);
			});
	itemEditGrid.on("scrollbottomclick", function(e) {
				itemEditGrid.batchAdd(18);
			});

	categoryEditGrid.on("shiftright", function(e) {
				var g = itemEditGrid;
				var col = g.findMostLeftEditCol();
				g.getSelectionModel().selectRow(0);
				this.stopEditing();
				g.startEditing(0, col);
			});
	itemEditGrid.on("shiftleft", function(e) {
				var g = categoryEditGrid;
				var col = g.findMostRightEditCol();
				var r = g.getSelectionModel().getSelected();
				var row = g.getStore().indexOf(r);
				this.stopEditing();
				g.startEditing(row, col);
			});

};

Ext.extend(SpeechDictPanel, Ext.Panel, {
	loadData : function(dictname) {
		Ext.Ajax.request({
					url : 'speech-dict!list.action',
					params : {
						'dictname' : dictname
					},
					success : function(form, action) {
						this.data = Ext.util.JSON.decode(form.responseText);
						this._refresh();
					},
					failure : function(form, action) {
						Ext.Msg.alert('提示', '获取词典数据失败');
					},
					scope : this
				});
	},
	loadItems : function(record) {
		this.itemForm.categoryNameField.setValue(record.data.name);
		if (!record.data.items) {
			record.data.items = [];
		}
		this.itemProxy.data = record.data;
		this.itemEditGrid.getStore().reload();
		this.itemEditGrid.batchAdd(10);
	},
	_refresh : function() {
		this.dictProxy.data = this.data;
		var grid = this.categoryEditGrid;
		grid.getStore().load();
		grid.batchAdd(30);
		var i = grid.defaultSelected;
		if (!i)
			i = 0;
		grid.getSelectionModel().selectRow(i);
	},
	save : function() {
		this.getEl().mask('保存...');
		Ext.Ajax.request({
					url : 'speech-dict!save.action',
					params : {
						data : Ext.encode(this.data.data)
					},
					success : function(form, action) {
						this.getEl().unmask();
						Dashboard.setAlert("保存成功!");
						var data = Ext.util.JSON.decode(form.responseText);
						this.data = data;
						this._refresh();
					},
					failure : function(response) {
						this.getEl().unmask();
						Ext.Msg.alert("错误", response.responseText);
					},
					scope : this
				});
	},
	importData : function() {
		this.importDictWin.show();
	},
	exportData : function() {
		var self = this;
		// 开始下载
		Ext.Ajax.request({
			url : 'speech-dict!exportSpeechDict.action',
			success : function(response) {
				var fileName = Ext.util.JSON.decode(response.responseText).message;
				var params = Ext.urlEncode({
							'filename' : fileName
						})
				var result = Ext.util.JSON.decode(response.responseText);
				if (result.success) {
					var timer = new ProgressTimer({
						initData : result.data,
						progressId : 'exportSpeechDictStatus',
						boxConfig : {
							title : '正在导出词典...'
						},
						finish : function() {
							if (!self.downloadIFrame) {
								self.downloadIFrame = self.getEl().createChild(
										{
											tag : 'iframe',
											style : 'display:none;'
										})
							}
							self.downloadIFrame.dom.src = 'speech-dict!downExportFile.action?'
									+ params + '&_t=' + new Date().getTime();
						}
					});
					timer.start();
				} else {
					Ext.Msg.alert('错误', '导出失败:' + result.message)
				}
			},
			failure : function(response) {
				Ext.Msg.alert('错误', '导出失败')
			},
			scope : this
		});
	},
	SyncAllAI : function() {
		Ext.Ajax.request({
					url : 'speech-dict!syncAllAI.action',
					success : function(resp) {
						var timer = new ProgressTimer({
									initData : Ext.decode(resp.responseText),
									progressId : 'syncSpeechDict',
									boxConfig : {
										title : '同步所有词典'
									}
								});
						timer.start();
					},
					failure : function(resp) {
						Ext.Msg.alert('错误', resp.responseText);
					}
				});
	}
});
