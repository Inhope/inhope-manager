SpeechDictToolPanel = function() {
	var _toolForm = new Ext.FormPanel({
		layout : "form",
		border : false,
		frame : true,
		// autoHeight : true,
		defaults : {
			bodyStyle : 'padding:10px,margins :30 10 20 0'
		},
		labelWidth : 60,
		buttonAlign : 'center',
		items : [new Ext.form.TextField({
							ref : 'content',
							name : 'content',
							width : 240,
							fieldLabel : "内容",
							allowBlank : false,
							emptyText : '输入查询的词典'
						}), new Ext.form.TextField({
							ref : 'voice',
							name : 'voice',
							width : 240,
							fieldLabel : "发音",
							readOnly : true,
							emptyText : '显示查询结果'
						}), {
					text : "查询",
					xtype : 'button',
					style : 'padding-top:8px;padding-right:180px;float:right;',
					handler : function() {
						var name = _toolForm.content.getValue();
						_toolForm.voice.setValue();
						if (name) {
							_toolForm.getEl().mask("查询中...");
							Ext.Ajax.request({
								url : 'speech-dict!searchVoice.action',
								params : {
									'name' : name
								},
								success : function(form, action) {
									_toolForm.getEl().unmask();
									result = Ext.util.JSON
											.decode(form.responseText).message;
									if (typeof(result) != 'undefined' && result) {
										_toolForm.voice.setValue(result);
									} else {
										_toolForm.voice.setValue('error')
									}
								},
								failure : function(form, action) {
									_toolForm.getEl().unmask();
									Ext.Msg.alert('提示', '查询失败');
								},
								scope : this
							});
						} else {
							Ext.Msg.alert("错误", "内容不能为空!");
						}
					}
				}]
	});
	this.toolForm = _toolForm;
	var cfg = {
		border : false,
		width : 350,
		title : '词典语音查询',
		defaults : {
			border : false
		},
		modal : false,
		plain : true,
		// shim : true,
		closeAction : 'hide',
		collapsible : true,
		closable : true,
		resizable : false,
		draggable : true,
		minimizable : false,
		maximizable : false,
		animCollapse : true,
		constrainHeader : true,
		autoHeight : false,
		items : [_toolForm]
	};

	SpeechDictToolPanel.superclass.constructor.call(this, cfg);
}
Ext.extend(SpeechDictToolPanel, Ext.Window, {
			showWindow : function() {
				this.toolForm.content.setValue();
				this.toolForm.voice.setValue();
				this.show()
			}
		});