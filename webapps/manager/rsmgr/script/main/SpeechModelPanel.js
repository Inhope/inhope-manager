SpeechModelPanel = function(cfg) {
	cfg = cfg || {};
	var self = this;
	this.pageSize = 20;
	this.store;
	this.matchIndex = 0;
	this.SpeechModelRecord = Ext.data.Record.create(['id', 'isEdit', 'name', 'ruleType', 'frequency', 'time', 'isEnable', 'categoryid', 'categoryName', 'path']);
	this.SpeechModelProxy = new Ext.data.HttpProxy({
		  url : 'speech-model!list.action'
	  });
	var _store = new Ext.data.Store({
		  proxy : self.SpeechModelProxy,
		  reader : new Ext.data.JsonReader({
			    totalProperty : 'total',
			    idProperty : 'id',
			    root : 'data',
			    fields : self.SpeechModelRecord
		    })
	  });
	var ruleStoreCondition = new Ext.data.SimpleStore({
		  fields : ["key", "value"],
		  data : [['', '全部'], ['0', '一般类型'], ['1', '规则类型']]
	  });
	var ruleCombo = new Ext.form.ComboBox({
		  triggerAction : 'all',
		  fieldLabel : '类型',
		  hiddenName : 'ruleType',
		  editable : false,
		  width : 100,
		  mode : 'local',
		  store : ruleStoreCondition,
		  displayField : 'value',
		  valueField : 'key'
	  });
	this.ruleCombo = ruleCombo;
	var ruleStore = new Ext.data.SimpleStore({
		  fields : ["key", "value"],
		  data : [['0', '一般类型'], ['1', '规则类型']]
	  });
	var seleMode = new Ext.grid.RowSelectionModel();
	this.seleMode = seleMode;
	this.pbar = new Ext.PagingToolbar({
		  store : _store,
		  displayInfo : true,
		  pageSize : this.pageSize,
		  beforePageText : '第',
		  afterPageText : '页，共{0}页',
		  displayMsg : '第{0}到第{1}条记录，共{2}条记录',
		  emptyMsg : '没有记录'
	  });

	this.pbar.add('-', {
		  xtype : 'label',
		  ref : 'statLabel',
		  text : '模型'
	  }, '&nbsp;&nbsp;&nbsp;');
	var speechModelEditGrid = this.speechModelEditGrid = new Ext.grid.EditorGridPanel({
		  // flex : 4,
		  style : 'border-right: 1px solid ' + sys_bdcolor,
		  border : false,
		  clicksToEdit : 1,
		  frame : false,
		  // columnLines : true,
		  selModel : this.seleMode,
		  pasteable : true,
		  colModel : new Ext.grid.ColumnModel({
			    defaults : {
				    sortable : true
			    },
			    columns : [new Ext.grid.RowNumberer(), {
				      dataIndex : 'isEdit',
				      hidden : true,
				      hideable : false
			      }, {
				      dataIndex : 'isEnable',
				      hidden : true,
				      hideable : false
			      }, {
				      header : '语料',
				      dataIndex : 'name',
				      width : 7,
				      editor : new Ext.form.TextField({
					        width : '100%'
				        }),
				      renderer : Dashboard.newTemplateRenderer()
			      }, {
				      header : '类型',
				      dataIndex : 'ruleType',
				      width : 1,
				      editor : new Ext.form.ComboBox({
					        triggerAction : 'all',
					        fieldLabel : '类型',
					        hiddenName : 'ruleType',
					        emptyText : '选择类型',
					        editable : false,
					        width : 100,
					        mode : 'local',
					        store : ruleStore,
					        displayField : 'value',
					        valueField : 'key'
				        }),
				      renderer : function(v, meta, r) {
					      var index = ruleStore.find('key', v);
					      if (index != -1) {
						      return ruleStore.getAt(index).data.value
					      }
					      return v;
				      }
			      }, {
				      header : '权重',
				      dataIndex : 'frequency',
				      width : 0.5,
				      editor : new Ext.form.NumberField({
					        decimalPrecision : 2
				        }),
				      validator : function(v) {
					      if (!Ext.isNumber(v)) {
						      return '权重只能是小数、整数';
					      }
					      return true;
				      }
			      }, {
				      header : '类别',
				      dataIndex : 'categoryName',
				      width : 1
			      }, {
				      header : '时间',
				      dataIndex : 'time',
				      width : 1.5
			      }]
		    }),
		  store : _store,
		  bbar : this.pbar,
		  viewConfig : {
			  forceFit : true,
			  scrollOffset : 0,
			  enableRowBody : true,
			  getRowClass : function(record, rowIndex, rowParams, store) {
				  // 禁用数据显示灰色
				  var cls = "white-row";
				  if (record.data.isEnable != 1) {
					  cls = 'disabled-row';
				  }
				  return cls;
			  }
		  },
		  listeners : {
			  afteredit : function(e) {
				  e.record.set('isEdit', 1);
				  var value = e.record.get("name");
				  if (typeof(value) !== "undefined") {
					  var startTag = "[";
					  var endTag = "]";
					  var match = true;
					  var index = 0;
					  for (i = 0; i <= value.length - 1; i++) {
						  var s = value[i];
						  if (startTag == s) {
							  index = i;
							  if (!match) {
								  break;
							  }
							  match = false;
						  } else if (s == endTag && !match && (index + 1 != i)) {
							  match = true;
						  } else if (s == endTag && match) {
							  match = false;
							  break;
						  }
					  }
					  if (match) {
						  self.matchIndex = self.matchIndex != 0 ? self.matchIndex - 1 : 0;
						  if (self.matchIndex == 0) {
							  self.getTopToolbar().save.enable();
						  }
					  } else {
						  self.matchIndex += 1;
						  self.getTopToolbar().save.disable();
						  Ext.Msg.alert("错误", "模型语句内容格式错误");
						  return;
					  }
				  }
			  }
		  },
		  scope : this
	  });
	var copyContent = "";
	var contextmenu = new Ext.menu.Menu({
		  // plain : true,
		  name : 'rightClick',
		  items : [{
			    text : '所属类别',
			    ref : 'categoryDir',
			    iconCls : 'icon-speechcagory',
			    width : 45,
			    handler : function() {
				    var selections = self.speechModelEditGrid.getSelectionModel().getSelections();
				    if (selections.length == 0) {
					    Ext.Msg.alert('提示', "先选择行");
					    return;
				    }
				    var record = selections[0];
				    var path = record.get('path');
				    self.navPanel.showModelTab(record.get("categoryid"), path);
			    }
		    }, {
			    text : '复制',
			    ref : 'speechModelCopy',
			    iconCls : 'icon-copy',
			    width : 45,
			    handler : function() {
				    var selections = self.speechModelEditGrid.getSelectionModel().getSelections();
				    if (selections.length == 0) {
					    Ext.Msg.alert('提示', "先选择行");
					    return;
				    }
				    var record = selections[0];
				    copyContent = record.get("name");
			    }
		    }, {
			    text : '粘贴',
			    iconCls : 'icon-paste',
			    ref : 'speechModelPaste',
			    width : 45,
			    handler : function() {
				    var selections = self.speechModelEditGrid.getSelectionModel().getSelections();
				    if (selections.length == 0) {
					    Ext.Msg.alert('提示', "先选择行");
					    return;
				    }
				    if (copyContent) {
					    selections[0].set("name", copyContent);
				    }
			    }
		    }, '-', {
			    text : '禁用',
			    iconCls : 'icon-disable',
			    ref : 'speechModelDisabled',
			    width : 45,
			    handler : function() {
				    var selections = self.speechModelEditGrid.getSelectionModel().getSelections();
				    if (selections.length == 0) {
					    Ext.Msg.alert('', "先选择行");
					    return;
				    }
				    self.isEnable(selections[0], false);
			    }
		    }, {
			    text : '启用',
			    iconCls : 'icon-enable',
			    ref : 'speechModelEnable',
			    width : 45,
			    handler : function() {
				    var selections = self.speechModelEditGrid.getSelectionModel().getSelections();
				    if (selections.length == 0) {
					    Ext.Msg.alert('', "先选择行");
					    return;
				    }
				    self.isEnable(selections[0], true);
			    }
		    }]

	  });
	this.menu = contextmenu;
	_store.baseParams['limit'] = this.pageSize;
	var tbarEx = new Ext.Toolbar({
		  items : [{
			    text : '搜索',
			    ref : 'searchBtn',
			    xtype : 'splitbutton',
			    iconCls : 'icon-search',
			    handler : function() {
				    self.searchModel();
			    },
			    menu : {
				    items : [{
					      text : '精确查询',
					      checked : false,
					      name : 'precise',
					      ref : '../precise'
				      }]
			    }
		    }, '-', '语料:', {
			    ref : 'searchSpeechModelname',
			    xtype : 'textfield',
			    emptyText : '请输入语料搜索',
			    listeners : {
				    specialkey : function(field, e) {
					    if (e.getKey() == Ext.EventObject.ENTER) {
						    self.searchModel();
					    }
				    }
			    }
		    }, '类型:', ruleCombo, '编辑时间:从', {
			    ref : 'searchSpeechmodelBdate',
			    xtype : 'datefield',
			    emptyText : '开始时间',
			    width : 150,
			    format : 'Y-m-d',
			    listeners : {
				    specialkey : function(field, e) {
					    if (e.getKey() == Ext.EventObject.ENTER) {
						    self.searchModel();
					    }
				    }
			    }
		    }, '到', {
			    ref : 'searchSpeechmodelEdate',
			    xtype : 'datefield',
			    width : 150,
			    emptyText : '结束时间',
			    format : 'Y-m-d',
			    listeners : {
				    specialkey : function(field, e) {
					    if (e.getKey() == Ext.EventObject.ENTER) {
						    self.searchModel();
					    }
				    }
			    }
		    }]
	  });
	this.tbarEx = tbarEx;
	SpeechModelPanel.superclass.constructor.call(this, {
		  border : false,
		  frame : false,
		  layout : {
			  type : 'hbox',
			  align : 'stretch'
		  },
		  items : [speechModelEditGrid],
		  buttonAlign : 'center',
		  tbar : [{
			    text : '删除',
			    iconCls : 'icon-table-delete',
			    handler : function() {
				    self.deleteRes();
			    }
		    }, {
			    text : '新增',
			    ref : 'addSpeechmodelRow',
			    xtype : 'splitbutton',
			    iconCls : 'icon-table-add',
			    menu : {
				    items : [{
					      xtype : 'textfield',
					      name : 'addRowSize',
					      fieldLabel : '数量',
					      width : 60,
					      value : '1',
					      ref : '../addRowSize'
				      }]
			    },
			    handler : function() {
				    var addSize = this.addRowSize.getValue()
				    if (addSize > 20) {
					    Ext.Msg.alert("提示", "新增数量不能超过20");
					    return;
				    }
				    self.addRec(addSize);
			    }
		    }, {
			    text : '刷新',
			    iconCls : 'icon-refresh',
			    handler : function() {
				    self.refresh();
			    }
		    }, {
			    text : '保存',
			    ref : 'save',
			    iconCls : 'icon-table-save',
			    handler : function() {
				    self.allSave();
			    }
		    }],
		  listeners : {
			  'render' : function() {
				  tbarEx.render(this.tbar)
			  }
		  }
	  });

	speechModelEditGrid.on("shiftright", function(e) {
		  var col = g.findMostLeftEditCol();
		  g.getSelectionModel().selectRow(0);
		  this.stopEditing();
		  g.startEditing(0, col);
	  });
	speechModelEditGrid.on("rowcontextmenu", function(grid, rowIndex, e) {
		  e.preventDefault();// 阻止事件的传递
		  grid.getSelectionModel().selectRow(rowIndex);
		  var selection = self.speechModelEditGrid.getSelectionModel().getSelections();
		  var row = selection[0];
		  self.menu.categoryDir.show();
		  self.menu.speechModelCopy.show();
		  self.menu.speechModelPaste.show();
		  self.menu.speechModelEnable.show();
		  self.menu.speechModelDisabled.show();
		  if (!row.get("name")) {
			  self.menu.speechModelCopy.hide();
			  self.menu.speechModelEnable.hide();
		  }
		  if (row.get("isEnable") == 1) {
			  self.menu.speechModelEnable.hide();
		  } else {
			  self.menu.speechModelDisabled.hide();
		  }
		  if (copyContent == "") {
			  self.menu.speechModelPaste.hide();
		  }
		  if (self.isModel == 1) {
			  self.menu.categoryDir.hide();
		  }
		  contextmenu.showAt(e.getXY());// 菜单的生成位置
	  });
	this.isModel;
	this.categoryId = '';
};

Ext.extend(SpeechModelPanel, Ext.Panel, {
	  loadModels : function(categoryId, isModel) {
		  this.isModel = isModel;
		  this.categoryId = categoryId;
		  if (typeof(isModel) !== "undefined" && isModel == 1) {
			  if (typeof(this.getTopToolbar().addSpeechmodelRow) != "undefined")
				  this.getTopToolbar().addSpeechmodelRow.enable();
		  } else if (typeof(this.getTopToolbar().addSpeechmodelRow) != "undefined") {
			  this.getTopToolbar().addSpeechmodelRow.disable();
		  }
		  var store = this.speechModelEditGrid.getStore();
		  if (store) {
			  for (var key in store.baseParams) {
				  if (key && key.indexOf('queryParam.') != -1) {
					  delete store.baseParams[key];
				  } else {
					  this.mask = false;
				  }
			  }
			  store.baseParams['categoryId'] = categoryId;
			  store.load({
				    params : {
					    start : 0,
					    limit : this.pageSize
				    }
			    });
			  this.store = store;
		  }
	  },
	  countModel : function(categoryId) {
		  var self = this;
		  categoryId = categoryId.length == 1 ? null : categoryId
		  Ext.Ajax.request({
			    url : 'speech-model!countModel.action',
			    params : {
				    'categoryId' : categoryId
			    },
			    success : function(resp) {
				    var obj = Ext.util.JSON.decode(resp.responseText);
				    var count = obj.message;
				    if (obj.success) {
					    self.pbar.statLabel.setText('模型' + (count == 0 ? '' : count) + ' 个');
				    }
			    },
			    failure : function(resp) {
				    Ext.Msg.alert('错误', resp.responseText)
			    }
		    });
	  },
	  refresh : function() {
		  this.loadModels(this.categoryId, this.isModel);
	  },
	  deleteRes : function() {
		  var rows = this.seleMode.getSelections();
		  if (rows.length == 0) {
			  Ext.Msg.alert('提示', '请先选择要删除的记录！');
			  return false;
		  }
		  var self = this;
		  Ext.Msg.confirm('', '确认删除这' + rows.length + '条记录？', function(sel) {
			    if (sel == 'yes') {
				    var ids = [];
				    Ext.each(rows, function(_item) {
					      ids.push(_item.id)
				      })
				    Ext.Ajax.request({
					      url : 'speech-model!deleteByIds.action',
					      params : {
						      ids : ids.join(',')
					      },
					      success : function(resp) {
						      Dashboard.setAlert('删除成功')
						      self.speechModelEditGrid.getStore().reload();
					      },
					      failure : function(resp) {
						      Ext.Msg.alert('错误', resp.responseText)
					      }
				      });
			    }
		    });
	  },
	  addRec : function(size) {
		  if (this.isModel != 1) {
			  Ext.Msg.alert("错误", "非模型类别不能新增");
			  return;
		  }
		  var s = this.speechModelEditGrid.getStore();
		  var c = s.getTotalCount();
		  for (var i = 0; i < size; i++) {
			  s.add(new s.recordType({}, Ext.id()));
		  }
	  },
	  allSave : function() {
		  var self = this;
		  var res = 0;
		  var isEmpty = false;
		  var resData = [];
		  var deleteIds = [];
		  Ext.each(this.speechModelEditGrid.getStore().getRange(), function(record) {
			    res++;
			    var id = record.data["id"];
			    var name = record.data["name"];
			    var isEdit = record.data["isEdit"];
			    var categoryid = record.data["categoryid"];
			    if ((self.categoryId.length == 1 && typeof(categoryid) == 'undefined') && isEdit == 1 && self.isModel != 1) {
				    Ext.Msg.alert("错误", '第' + res + '行不能新增在非模型类别下');
				    isEmpty = true;
				    return false;
			    }
			    if (!name && isEdit == 1 && id) {
				    deleteIds.push(id);
				    return;
			    }
			    if (isEdit == 1 && name) {
				    resData.push(record.data);
			    }
		    });

		  if (isEmpty) {
			  return;
		  }
		  if (deleteIds.length > 0) {
			  Ext.Msg.confirm('', '确定要保存原有的数据为空(删除操作)的记录吗？', function(sel) {
				    if (sel == 'yes') {
					    resDataObj = Ext.encode(resData);
					    self.getEl().mask('保存中...');
					    Ext.Ajax.request({
						      url : 'speech-model!save.action',
						      params : {
							      data : resDataObj,
							      deleteIds : deleteIds.join(','),
							      categoryId : self.categoryId
						      },
						      success : function(form, action) {
							      self.getEl().unmask()
							      var obj = Ext.util.JSON.decode(form.responseText);
							      if (!obj.success) {
								      Ext.Msg.alert("错误", obj.message);
								      return;
							      }
							      Dashboard.setAlert("保存成功");
							      self.speechModelEditGrid.getStore().reload();
						      },
						      failure : function(response) {
							      self.getEl().unmask()
							      Ext.Msg.alert("错误", response.responseText);
						      },
						      scope : this
					      });
				    }
			    });
		  } else {
			  if (resData.length <= 0) {
				  Ext.Msg.alert("提示", "新增或修改的数据为空");
				  return;
			  }
			  resData = Ext.encode(resData);
			  self.getEl().mask('保存中...');
			  Ext.Ajax.request({
				    url : 'speech-model!save.action',
				    params : {
					    data : resData,
					    categoryId : this.categoryId
				    },
				    success : function(form, action) {
					    this.getEl().unmask()
					    var obj = Ext.util.JSON.decode(form.responseText);
					    if (!obj.success) {
						    Ext.Msg.alert("错误", obj.message);
						    return;
					    }
					    Dashboard.setAlert("保存成功");
					    self.speechModelEditGrid.getStore().reload();
				    },
				    failure : function(response) {
					    this.getEl().unmask()
					    Ext.Msg.alert("错误", response.responseText);
				    },
				    scope : this
			    });
		  }
	  },
	  isEnable : function(select, enable) {
		  var self = this;
		  var mData = [];
		  Ext.Msg.show({
			    title : '',
			    msg : '您确定要' + (enable ? 'enable' : 'disable') + '该语料？',
			    buttons : {
				    yes : '是',
				    no : '否'
			    },
			    fn : function(choice) {
				    if ('no' == choice) {
					    return
				    } else {
					    var obj = {};
					    obj = select.data;
					    if (enable) {
						    obj.isEnable = 1;
					    } else {
						    obj.isEnable = 0;
					    }
					    mData.push(obj);
					    Ext.Ajax.request({
						      url : 'speech-model!save.action',
						      params : {
							      'data' : Ext.encode(mData),
							      'categoryId' : self.categoryId
						      },
						      success : function(response) {
							      var result = Ext.util.JSON.decode(response.responseText);
							      if (result.success) {
								      Dashboard.setAlert((enable ? '启用' : '禁用') + "成功");
								      self.speechModelEditGrid.getStore().reload();
							      }
						      },
						      failure : function(response) {
							      Ext.Msg.alert('错误', (enable ? '启用' : '禁用') + "失败")
						      },
						      scope : this
					      });
				    }
			    }
		    });
	  },
	  searchModel : function() {
		  var _name = this.tbarEx.searchSpeechModelname.getValue().trim();
		  var _ruleType = this.ruleCombo.getValue();
		  var _bdate = this.tbarEx.searchSpeechmodelBdate.getValue();
		  var _edate = this.tbarEx.searchSpeechmodelEdate.getValue();
		  var self = this;
		  var store = self.speechModelEditGrid.getStore();
		  for (var key in store.baseParams) {
			  if (key && key.indexOf('queryParam.') != -1)
				  delete store.baseParams[key];
		  }
		  if (_name)
			  store.setBaseParam('queryParam.name', _name);
		  if (_ruleType)
			  store.setBaseParam('queryParam.ruleType', _ruleType)
		  if (_bdate)
			  store.setBaseParam('queryParam.beginDate', _bdate);
		  if (_edate)
			  store.setBaseParam('queryParam.endDate', _edate);
		  if (this.tbarEx.searchBtn.precise.checked)
			  store.setBaseParam('queryParam.preciseSearch', true)
		  store.load();
	  }
  });
