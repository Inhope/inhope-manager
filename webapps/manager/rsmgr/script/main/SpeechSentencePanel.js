SpeechSentencePanel = function(cfg) {
	cfg = cfg || {};
	var self = this;
	this.pageSize = 20;
	this.store;
	this.categoryId = '';
	this.isSentenceclass = '';
	this.matchIndex = 0;
	this.SpeechSentenceRecord = Ext.data.Record.create(['id', 'isEdit', 'name', 'frequency', 'time', 'isEnable', 'complexRate', 'categoryid', 'categoryName', 'path']);
	this.SpeechSentenceProxy = new Ext.data.HttpProxy({
		  url : 'speech-sentence!list.action'
	  });
	var _store = new Ext.data.Store({
		  proxy : self.SpeechSentenceProxy,
		  reader : new Ext.data.JsonReader({
			    totalProperty : 'total',
			    idProperty : 'id',
			    root : 'data',
			    fields : self.SpeechSentenceRecord
		    }),
		  listeners : {
			  'load' : function() {
				  self.countComplexCount(self.categoryId);
			  }
		  }
	  });
	var seleMode = new Ext.grid.RowSelectionModel();
	this.seleMode = seleMode;

	this.pbar = new Ext.PagingToolbar({
		  store : _store,
		  displayInfo : true,
		  pageSize : this.pageSize,
		  beforePageText : '第',
		  afterPageText : '页，共{0}页',
		  displayMsg : '第{0}到{1}条记录，共{2}条',
		  emptyMsg : "没有记录"
	  });
	this.pbar.add('-', {
		  xtype : 'label',
		  ref : 'complexRateLabel',
		  text : '总复杂度'
	  }, '&nbsp;');
	this.pbar.add('-', {
		  xtype : 'label',
		  ref : 'statLabel',
		  text : '场景'
	  }, '&nbsp;&nbsp;&nbsp;');
	var speechSentenceEditGrid = this.speechSentenceEditGrid = new Ext.grid.EditorGridPanel({
		  flex : 4,
		  style : 'border-right: 1px solid ' + sys_bdcolor,
		  border : false,
		  clicksToEdit : 1,
		  frame : false,
		  columnLines : true,
		  selModel : this.seleMode,
		  pasteable : true,
		  columns : [new Ext.grid.RowNumberer(), new Ext.form.Hidden({
			      dataIndex : 'isEnable',
			      width : 0,
			      hidden : true
		      }), {
			    header : '句子',
			    dataIndex : 'name',
			    width : 7,
			    editor : new Ext.form.TextField(),
			    renderer : Dashboard.newTemplateRenderer()
		    }, {
			    header : '重复值',
			    dataIndex : 'frequency',
			    width : 0.5,
			    editor : new Ext.form.NumberField({
				      decimalPrecision : 0
			      }),
			    validator : function(v) {
				    if (!Ext.isNumber(v)) {
					    return '重复次数只能是整数';
				    }
				    return true;
			    }
		    }, {
			    header : '类别',
			    dataIndex : 'categoryName',
			    width : 1,
			    hidden : true
		    }, {
			    header : '复杂度',
			    dataIndex : 'complexRate',
			    width : 1
		    }, {
			    header : '时间',
			    dataIndex : 'time',
			    width : 1.5,
			    editor : new Ext.form.TextField()
		    }],
		  store : _store,
		  bbar : this.pbar,
		  viewConfig : {
			  forceFit : true,
			  enableRowBody : true,
			  getRowClass : function(record, rowIndex, rowParams, store) {
				  // 禁用数据显示灰色
				  var cls = "white-row";
				  if (record.data.isEnable != 1) {
					  cls = 'disabled-row';
				  }
				  return cls;
			  }
		  },
		  listeners : {
			  afteredit : function(e) {
				  e.record.set('isEdit', 1);
				  var value = e.record.get("name");
				  if (typeof(value) !== "undefined") {
					  var startTag = "[";
					  var endTag = "]";
					  var match = true;
					  var index = 0;
					  for (i = 0; i <= value.length - 1; i++) {
						  var s = value[i];
						  if (startTag == s) {
							  index = i;
							  if (!match) {
								  break;
							  }
							  match = false;
						  } else if (s == endTag && !match && (index + 1 != i)) {
							  match = true;
						  } else if (s == endTag && match) {
							  match = false;
							  break;
						  }
					  }
					  if (match) {
						  self.matchIndex = self.matchIndex != 0 ? self.matchIndex - 1 : 0;
						  if (self.matchIndex == 0) {
							  self.getTopToolbar().save.enable();
						  }
					  } else {
						  self.matchIndex += 1;
						  self.getTopToolbar().save.disable();
						  Ext.Msg.alert("异常", "语料内容格式错误");
						  return;
					  }
				  }
			  }
		  },
		  scope : this
	  });
	var copyContent = "";
	var contextmenu = new Ext.menu.Menu({
		  // plain : true,
		  name : 'rightClick',
		  ref : 'rightMenu',
		  items : [{
			    text : '所属类别',
			    ref : 'categoryDir',
			    iconCls : 'icon-speechcagory',
			    width : 45,
			    handler : function() {
				    var selections = self.speechSentenceEditGrid.getSelectionModel().getSelections();
				    if (selections.length == 0) {
					    Ext.Msg.alert('提示', "先选择行");
					    return;
				    }
				    var record = selections[0];
				    var path = record.get('path');
				    self.navPanel.showSentenceTab(record.get("categoryid"), path);
			    }
		    }, {
			    text : '复制',
			    ref : 'speechSentenceCopy',
			    iconCls : 'icon-copy',
			    width : 45,
			    handler : function() {
				    var selections = self.speechSentenceEditGrid.getSelectionModel().getSelections();
				    if (selections.length == 0) {
					    Ext.Msg.alert('提示', "先选择行");
					    return;
				    }
				    var record = selections[0];
				    copyContent = record.get("name");
			    }
		    }, {
			    text : '粘贴',
			    iconCls : 'icon-paste',
			    ref : 'speechSentencePaste',
			    width : 45,
			    handler : function() {
				    var selections = self.speechSentenceEditGrid.getSelectionModel().getSelections();
				    if (selections.length == 0) {
					    Ext.Msg.alert('提示', "先选择行");
					    return;
				    }
				    if (copyContent) {
					    selections[0].set("name", copyContent);
				    }
			    }
		    }, '-', {
			    text : '禁用',
			    iconCls : 'icon-disable',
			    ref : 'speechSentenceDisabled',
			    width : 45,
			    handler : function() {
				    var selections = self.speechSentenceEditGrid.getSelectionModel().getSelections();
				    if (selections.length == 0) {
					    Ext.Msg.alert('提示', "先选择行");
					    return;
				    }
				    self.isEnable(selections[0], false);
			    }
		    }, {
			    text : '激活',
			    iconCls : 'icon-enable',
			    ref : 'speechSentenceEnable',
			    width : 45,
			    handler : function() {
				    var selections = self.speechSentenceEditGrid.getSelectionModel().getSelections();
				    if (selections.length == 0) {
					    Ext.Msg.alert('提示', "先选择行");
					    return;
				    }
				    self.isEnable(selections[0], true);
			    }
		    }]

	  });
	this.menu = contextmenu;
	_store.baseParams['limit'] = this.pageSize;

	var replaceContentForm = new Ext.form.FormPanel({
		  layout : "form",
		  frame : true,
		  border : false,
		  autoHeight : true,
		  waitMsgTarget : true,
		  margins : '0 0 0 0',
		  labelAlign : "left",
		  formBind : true,
		  labelWidth : 70,
		  items : [{
			    ref : 'sourceContent',
			    xtype : 'textfield',
			    fieldLabel : '待替换内容',
			    anchor : '96%'
		    }, {
			    ref : 'replaceContent',
			    xtype : 'textfield',
			    fieldLabel : '替换内容',
			    anchor : '96%'
		    }],
		  buttons : [new Ext.Button({
			    text : '替换',
			    width : 80,
			    iconCls : 'icon-table-edit',
			    handler : function() {
				    self.replaceContent();
			    }
		    })]
	  });
	this.replaceContentForm = replaceContentForm;
	var replaceContentWin = new Ext.Window({
		  width : 495,
		  title : '语料替换',
		  border : false,
		  modal : true,
		  plain : true,
		  shim : true,
		  closable : true,
		  closeAction : 'hide',
		  collapsible : true,
		  resizable : false,
		  draggable : true,
		  animCollapse : true,
		  constrainHeader : true,
		  listeners : {
			  'beforehide' : function(p) {
				  self.replaceContentForm.getForm().reset();
			  }
		  },
		  items : replaceContentForm
	  });
	this.replaceContentWin = replaceContentWin;

	var tbarEx = new Ext.Toolbar({
		  items : [{
			    text : '搜索',
			    ref : 'searchBtn',
			    xtype : 'splitbutton',
			    iconCls : 'icon-search',
			    handler : function() {
				    self.searchSentence();
			    },
			    menu : {
				    items : [{
					      text : '精确查询',
					      checked : false,
					      name : 'precise',
					      ref : '../precise'
				      }]
			    }
		    }, '-', '句子:', {
			    ref : 'searchSpeechsentencename',
			    xtype : 'textfield',
			    emptyText : '输入语料名搜索...',
			    listeners : {
				    specialkey : function(field, e) {
					    if (e.getKey() == Ext.EventObject.ENTER) {
						    self.searchSentence();
					    }
				    }
			    }
		    }, '编辑时间:从', {
			    ref : 'searchSpeechsentenceBdate',
			    xtype : 'datefield',
			    emptyText : '开始时间',
			    width : 150,
			    format : 'Y-m-d',
			    listeners : {
				    specialkey : function(field, e) {
					    if (e.getKey() == Ext.EventObject.ENTER) {
						    self.searchSentence();
					    }
				    }
			    }
		    }, '到', {
			    ref : 'searchSpeechsentenceEdate',
			    xtype : 'datefield',
			    width : 150,
			    emptyText : '结束时间',
			    format : 'Y-m-d',
			    listeners : {
				    specialkey : function(field, e) {
					    if (e.getKey() == Ext.EventObject.ENTER) {
						    self.searchSentence();
					    }
				    }
			    }
		    }]
	  });
	this.tbarEx = tbarEx;
	SpeechSentencePanel.superclass.constructor.call(this, {
		  border : false,
		  frame : false,
		  layout : {
			  type : 'hbox',
			  align : 'stretch'
		  },
		  items : [speechSentenceEditGrid],
		  buttonAlign : 'center',
		  tbar : [{
			    text : '删除',
			    iconCls : 'icon-table-delete',
			    handler : function() {
				    self.deleteRes();
			    }
		    }, {
			    text : '新增',
			    xtype : 'splitbutton',
			    ref : 'addSpeechSentenceRow',
			    iconCls : 'icon-table-add',
			    menu : {
				    items : [{
					      xtype : 'textfield',
					      name : 'addRowSize',
					      width : 60,
					      value : '1',
					      ref : '../addRowSize'
				      }]
			    },
			    handler : function() {
				    var addSize = this.addRowSize.getValue()
				    if (addSize > 20) {
					    Ext.Msg.alert("提示", "新增数量不能超过 20");
					    return;
				    }
				    self.addRec(addSize);
			    }
		    }, {
			    text : '刷新',
			    iconCls : 'icon-refresh',
			    handler : function() {
				    self.refresh();
			    }
		    }, {
			    text : '替换',
			    ref : 'save',
			    iconCls : 'icon-grid',
			    handler : function() {
				    self.replaceContentWin.show();
			    }
		    }, {
			    text : '保存',
			    ref : 'save',
			    iconCls : 'icon-table-save',
			    handler : function() {
				    self.allSave();
			    }
		    }],
		  listeners : {
			  'render' : function() {
				  tbarEx.render(this.tbar)
			  }
		  }
	  });

	speechSentenceEditGrid.on("shiftright", function(e) {
		  var col = g.findMostLeftEditCol();
		  g.getSelectionModel().selectRow(0);
		  this.stopEditing();
		  g.startEditing(0, col);
	  });
	speechSentenceEditGrid.on("rowcontextmenu", function(grid, rowIndex, e) {

		  e.preventDefault();// 阻止事件的传递
		  grid.getSelectionModel().selectRow(rowIndex);
		  var selection = self.speechSentenceEditGrid.getSelectionModel().getSelections();
		  var row = selection[0];
		  self.menu.categoryDir.show();
		  self.menu.speechSentenceCopy.show();
		  self.menu.speechSentencePaste.show();
		  self.menu.speechSentenceEnable.show();
		  self.menu.speechSentenceDisabled.show();
		  if (!row.get("name")) {
			  self.menu.speechSentenceCopy.hide();
			  self.menu.speechSentenceEnable.hide();
		  }
		  if (row.get("isEnable") == 1) {
			  self.menu.speechSentenceEnable.hide();
		  } else {
			  self.menu.speechSentenceDisabled.hide();
		  }
		  if (copyContent == "") {
			  self.menu.speechSentencePaste.hide();
		  }
		  if (self.isSentenceclass == 1) {
			  self.menu.categoryDir.hide();
		  }
		  contextmenu.showAt(e.getXY());// 菜单的生成位置
	  });
	this.SpeechSentencePanel = SpeechSentencePanel;
};

Ext.extend(SpeechSentencePanel, Ext.Panel, {
	  loadSentences : function(categoryId, isSentenceclass) {
		  this.categoryId = categoryId;
		  this.isSentenceclass = isSentenceclass
		  if (typeof(isSentenceclass) !== "undefined" && isSentenceclass == 1) {
			  if (typeof(this.getTopToolbar().addSpeechSentenceRow) != "undefined")
				  this.getTopToolbar().addSpeechSentenceRow.enable();
		  } else {
			  if (typeof(this.getTopToolbar().addSpeechSentenceRow) != "undefined")
				  this.getTopToolbar().addSpeechSentenceRow.disable();
		  }
		  var store = this.speechSentenceEditGrid.getStore();
		  if (store) {
			  for (var key in store.baseParams) {
				  if (key && key.indexOf('queryParam.') != -1) {
					  delete store.baseParams[key];
				  } else {
					  this.mask = false;
				  }
			  }
			  store.baseParams['categoryId'] = categoryId;
			  store.load({
				    params : {
					    start : 0,
					    limit : this.pageSize
				    }
			    });
			  this.store = store;
		  }
	  },
	  countComplexCount : function(categoryId) {
		  var self = this;
		  categoryId = categoryId.length == 1 ? null : categoryId
		  var name = this.tbarEx.searchSpeechsentencename.getValue().trim();
		  var bdate = this.tbarEx.searchSpeechsentenceBdate.getValue();
		  var edate = this.tbarEx.searchSpeechsentenceEdate.getValue();
		  var preciseSearch = this.tbarEx.searchBtn.precise.checked;
		  Ext.Ajax.request({
			    url : 'speech-sentence!countComplexRate.action',
			    params : {
				    'categoryId' : categoryId,
				    'name' : name,
				    'beginDate' : bdate,
				    'endDate' : edate,
				    'preciseSearch' : preciseSearch
			    },
			    success : function(resp) {
				    var obj = Ext.util.JSON.decode(resp.responseText);
				    var count = obj.data;
				    if (obj.success) {
					    self.pbar.complexRateLabel.setText('总复杂度 ' + (count == 0 ? '' : count) + '');
				    }
			    },
			    failure : function(resp) {
				    Ext.Msg.alert('错误', resp.responseText)
			    }
		    });
	  },
	  countSentencesclass : function(categoryId) {
		  var self = this;
		  categoryId = categoryId.length == 1 ? null : categoryId
		  Ext.Ajax.request({
			    url : 'speech-sentence!countSentencesclass.action',
			    params : {
				    'categoryId' : categoryId
			    },
			    success : function(resp) {
				    var obj = Ext.util.JSON.decode(resp.responseText);
				    var count = obj.data;
				    if (obj.success) {
					    self.pbar.statLabel.setText('场景 ' + (count == 0 ? '' : count) + ' 个');
				    }
			    },
			    failure : function(resp) {
				    Ext.Msg.alert('错误', resp.responseText)
			    }
		    });
	  },
	  refresh : function() {
		  this.loadSentences(this.categoryId);
	  },
	  deleteRes : function() {
		  var rows = this.seleMode.getSelections();
		  if (rows.length == 0) {
			  Ext.Msg.alert('提示', '请先选择要删除的记录！');
			  return false;
		  }
		  var self = this;
		  Ext.Msg.confirm('确定框', '确定要删除这' + rows.length + '记录吗？', function(sel) {
			    if (sel == 'yes') {
				    var ids = [];
				    Ext.each(rows, function(_item) {
					      ids.push(_item.id)
				      })
				    Ext.Ajax.request({
					      url : 'speech-sentence!deleteByIds.action',
					      params : {
						      ids : ids.join(',')
					      },
					      success : function(resp) {
						      Dashboard.setAlert('删除成功！')
						      self.speechSentenceEditGrid.getStore().reload();
					      },
					      failure : function(resp) {
						      Ext.Msg.alert('错误', resp.responseText)
					      }
				      });
			    }
		    });
	  },
	  addRec : function(size) {
		  if (this.categoryId.length == 1 && this.categoryId == 1) {
			  Ext.Msg.alert("错误", "根目录不能直接新增");
			  return;
		  }
		  var s = this.speechSentenceEditGrid.getStore();
		  var c = s.getTotalCount();
		  for (var i = 0; i < size; i++) {
			  s.add(new s.recordType({}, Ext.id()));
		  }
	  },
	  allSave : function() {
		  var self = this;
		  var res = 0;
		  var isEmpty = false;
		  var deleteIds = [];
		  var resData = [];
		  Ext.each(this.speechSentenceEditGrid.getStore().getRange(), function(record) {
			    res++;
			    var id = record.data["id"];
			    var name = record.data["name"];
			    var isEdit = record.data["isEdit"];
			    var categoryid = record.data["categoryid"];
			    if ((self.categoryId.length == 1 && typeof(categoryid) == 'undefined') && isEdit == 1 && self.isSentenceclass != 1) {
				    Ext.Msg.alert("错误", "第" + res + "行不能保存在 非语音类别 下");
				    isEmpty = true;
				    return false;
			    }

			    if (!name && isEdit == 1 && id) {
				    deleteIds.push(id);
				    return;
			    }
			    if (isEdit == 1 && name) {
				    resData.push(record.data);
			    }
		    });

		  if (isEmpty) {
			  return;
		  }
		  if (deleteIds.length > 0) {
			  Ext.Msg.confirm('确定框', '确定要保存原有的数据为空(删除操作)的记录吗？', function(sel) {
				    if (sel == 'yes') {
					    self.getEl().mask('保存...');
					    resData = Ext.encode(resData);
					    Ext.Ajax.request({
						      url : 'speech-sentence!save.action',
						      params : {
							      data : resData,
							      deleteIds : deleteIds.join(','),
							      categoryId : self.categoryId
						      },
						      success : function(form, action) {
							      self.getEl().unmask()
							      var obj = Ext.util.JSON.decode(form.responseText);
							      if (!obj.success) {
								      Ext.Msg.alert("错误", obj.message);
								      return;
							      }
							      Dashboard.setAlert("保存成功!");
							      self.speechSentenceEditGrid.getStore().reload();
						      },
						      failure : function(response) {
							      self.getEl().unmask()
							      Ext.Msg.alert("错误", response.responseText);
						      },
						      scope : this
					      });
				    }
			    });
		  } else {
			  if (resData.length <= 0) {
				  Ext.Msg.alert("提示", "修改或新增的数据为空");
				  return;
			  }
			  resData = Ext.encode(resData);
			  Ext.Ajax.request({
				    url : 'speech-sentence!save.action',
				    params : {
					    data : resData,
					    categoryId : this.categoryId
				    },
				    success : function(form, action) {
					    self.getEl().unmask()
					    var obj = Ext.util.JSON.decode(form.responseText);
					    if (!obj.success) {
						    Ext.Msg.alert("错误", obj.message);
						    return;
					    }
					    Dashboard.setAlert("保存成功!");
					    self.speechSentenceEditGrid.getStore().reload();
				    },
				    failure : function(response) {
					    self.getEl().unmask()
					    Ext.Msg.alert("错误", response.responseText);
				    },
				    scope : this
			    });
		  }
	  },
	  isEnable : function(select, enable) {
		  var self = this;
		  var mData = [];
		  Ext.Msg.show({
			    title : '提示',
			    msg : '您确定要 ' + (enable ? '激活' : '禁用') + ' 该句子',
			    buttons : {
				    yes : '是',
				    no : '否'
			    },
			    fn : function(choice) {
				    if ('no' == choice) {
					    return
				    } else {
					    var obj = {};
					    obj = select.data;
					    if (enable) {
						    obj.isEnable = 1;
					    } else {
						    obj.isEnable = 0;
					    }
					    mData.push(obj);
					    Ext.Ajax.request({
						      url : 'speech-sentence!save.action',
						      params : {
							      'data' : Ext.encode(mData),
							      'categoryId' : self.categoryId
						      },
						      success : function(response) {
							      var result = Ext.util.JSON.decode(response.responseText);
							      if (result.success) {
								      Dashboard.setAlert((enable ? '激活' : '禁用') + " 成功!");
								      self.speechSentenceEditGrid.getStore().reload();
							      }
						      },
						      failure : function(response) {
							      Ext.Msg.alert('错误', (enable ? '激活' : '禁用') + '失败')
						      },
						      scope : this
					      });
				    }
			    }
		    });
	  },
	  searchSentence : function() {
		  var _name = this.tbarEx.searchSpeechsentencename.getValue().trim();
		  var _bdate = this.tbarEx.searchSpeechsentenceBdate.getValue();
		  var _edate = this.tbarEx.searchSpeechsentenceEdate.getValue();
		  var self = this;
		  var store = self.speechSentenceEditGrid.getStore();
		  for (var key in store.baseParams) {
			  if (key && key.indexOf('queryParam.') != -1)
				  delete store.baseParams[key];
		  }
		  if (_name)
			  store.setBaseParam('queryParam.name', _name);
		  if (_bdate)
			  store.setBaseParam('queryParam.beginDate', _bdate);
		  if (_edate)
			  store.setBaseParam('queryParam.endDate', _edate);
		  if (this.tbarEx.searchBtn.precise.checked)
			  store.setBaseParam('queryParam.preciseSearch', true)
		  store.load();
	  },
	  replaceContent : function() {
		  var self = this;
		  var sourceCon = this.replaceContentForm.sourceContent.getValue();
		  var replaceCon = this.replaceContentForm.replaceContent.getValue();
		  if (!sourceCon) {
			  Ext.Msg.alert('提示', '待替换内容不能为空');
			  return;
		  }
		  Ext.Msg.confirm('提示', '确定要替换内容"' + sourceCon + '"为' + (replaceCon ? '"' + replaceCon + '"' : '空') + '吗？', function(btn, text) {
			    if (btn == 'yes') {
				    Ext.Ajax.request({
					      url : 'speech-sentence!replaceContent.action',
					      params : {
						      'sourceContent' : sourceCon,
						      'replaceContent' : replaceCon,
						      'categoryId' : self.categoryId
					      },
					      success : function(response) {
						      var result = Ext.util.JSON.decode(response.responseText);
						      if (result.success) {
							      if (result.data > 0) {
								      Dashboard.setAlert("成功替换" + result.data + "条数据！");
								      self.speechSentenceEditGrid.getStore().reload();
							      } else {
								      Ext.Msg.alert('提示', "没有数据被替换！");
							      }
						      } else
							      Ext.Msg.alert('异常', result.message);
					      },
					      failure : function(response) {
						      Ext.Msg.alert('替换内容失败', response.responseText)
					      },
					      scope : this
				      });
			    }
		    });
	  }
  });
