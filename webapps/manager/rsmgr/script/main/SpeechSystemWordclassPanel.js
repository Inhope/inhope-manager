SpeechSystemWordclassPanel = function(_cfg) {
	var self = this;
	this.store;
	var fields = [{
				type : 'string',
				name : 'name1'
			}, {
				type : 'string',
				name : 'name2'
			}, {
				type : 'string',
				name : 'name3'
			}, {
				type : 'string',
				name : 'name4'
			}];
	var columns = [new Ext.grid.RowNumberer(), {
				header : '系统词类',
				dataIndex : 'name1'
			}, {
				header : '系统词类',
				dataIndex : 'name2'
			}, {
				header : '系统词类',
				dataIndex : 'name3'
			}, {
				header : '系统词类',
				dataIndex : 'name4'
			}];

	var _store = new Ext.data.Store({
				proxy : new Ext.data.HttpProxy({
							url : 'speech-word!systemWordlist.action'
						}),
				reader : new JsonReaderEx({
							idProperty : 'id',
							root : 'data',
							fields : fields
						}),
				writer : new Ext.data.JsonWriter()
			});
	this.store = _store;
	var dataPanelCfg = {
		name : 'SpeechSystemWordclassPanel',
		border : true,
		store : _store,
		loadMask : true,
		columnLines : true,
		columns : columns,
		hideHeaders : true,
		region : 'center',
		viewConfig : {
			forceFit : true
		}
	}
	var dataPanel = new Ext.grid.GridPanel(Ext
			.applyIf(_cfg || {}, dataPanelCfg));

	var cfg = ({
		layout : 'border',
		border : true,
		items : [dataPanel]
	});

	SpeechSystemWordclassPanel.superclass.constructor.call(this, cfg);
}
Ext.extend(SpeechSystemWordclassPanel, Ext.Panel, {
			loadWords : function() {
				this.store.load()
			}
		});