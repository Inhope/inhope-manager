SpeechTestToolPanel = function() {
	var _this = this;
	var _toolForm = new Ext.Panel(
			{
				layout : "fit",
				region : 'center',
				border : false,
				frame : false,
				items : [ new Ext.Panel(
						{
							ref : 'recogResult',
							border : false,
							bodyStyle : 'background:#dfe8f6;',
							html : '<textarea id="__speechTestResult" style="width:100%;height:100%"></textarea>'
						}) ]
			});
	this.toolForm = _toolForm;

	var pbar = new Ext.ProgressBar({
		width : 430,
		text : '点击按钮录音'
	});

	var cfg = {
		tbar : [ pbar, ' ', {
			text : '录音',
			ref : 'recBtn',
			disabled : true,
			enableToggle : true,
			iconCls : 'icon-record',
			listeners : {
				'toggle' : function(btn, pressed) {
					if (!pressed) {
						pbar.reset();
						pbar.updateText('正在提交语音数据...');
						window.stopRecord();
					} else {
						window.startRecord();
						Ext.getDom('__speechTestResult').value = '';
					}
				}
			}
		} ],
		border : false,
		width : 500,
		height : 300,
		title : '语音测试工具',
		plain : true,
		modal : false,
		shim : true,
		closeAction : 'hide',
		layout : 'fit',
		collapsible : true,
		closable : true,
		resizable : false,
		draggable : true,
		minimizable : false,
		maximizable : false,
		animCollapse : true,
		constrainHeader : true,
		items : [ _toolForm ]
	};

	SpeechTestToolPanel.superclass.constructor.call(this, cfg);
	this.on('show', function() {
		if (window.recEnabled)
			this.getTopToolbar().recBtn.enable();
	});
	this.on('hide', function() {
		pbar.reset();
		Ext.getDom('__speechTestResult').value = '';
	});

	initMic = function() {
		var micHtml = '<a href="#">'
				+ '<embed id="_mic_flash" style="width:220px;height:140px;cursor:pointer;" type="application/x-shockwave-flash" src="mic.swf?ts='
				+ new Date().getTime()
				+ '" allowScriptAccess="always"></embed>' + '</a>';
		if (document.all)
			micHtml = '<a href="#">'
					+ '<object id="_mic_flash" style="width:220px;height:140px;cursor:pointer;" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" allowScriptAccess="always" ><param name="movie" value="mic.swf?ts='
					+ new Date().getTime()
					+ '"/><param name="allowScriptAccess" value="always" /></object>'
					+ '</a>';
		window.onMicReady = function() {
			checkMic();
		};
		var div = document.createElement('div');
		div.setAttribute('id', '_mic_div');
		div.innerHTML = micHtml;
		div.style.position = 'absolute';
		div.style.right = '2000px';
		div.style.bottom = '2000px';
		document.body.appendChild(div);
		var mic0 = document.getElementById('_mic_flash');
		function micReady() {
			var div = document.getElementById('_mic_div');
			div.style.right = '0px';
			div.style.bottom = '0px';
		}
		var firstTime = true;
		function checkMic() {
			if (!firstTime) {
				micReady();
				return;
			}
			var ret = mic0.initMic();
			if (ret == 'oldVersion')
				alert('你的Flash版本过低，请先更新到最新版本再尝试语音功能。');
			else if (ret == 'noMic')
				alert('没有在您的机器上找到麦克风设备');
			else if (ret == 'muted') {
				setTimeout(function() {
					micReady();
				}, 500);
			} else
				micReady();
			firstTime = false;
		}
		window.startRecord = function() {
			mic0.startRecord("rtmp://" + Dashboard.rtmpAddr + "/recog", Dashboard.projectId);
		};
		window.recordStarted = function() {
			pbar.wait({
				interval : 100,
				duration : 5000,
				increment : 50,
				text : '正在采集...',
				fn : function() {
					_this.getTopToolbar().recBtn.toggle();
				}
			});
		};
		window.stopRecord = function() {
			setTimeout(function() {
				mic0.stopRecord();
			}, 500);

		};
		window.onQuestionReceived = function(result) {
			Ext.getDom('__speechTestResult').value = result;
			pbar.reset();
			pbar.updateText('点击按钮录音');
		};
		window.playbackComplete = function() {
			mic0.reset();
		};
		window.onSilenceDetected = function() {
			mic0.loading();
			mic0.stopRecord();
		};
		window.muteStateChange = function(muted) {
			var div = document.getElementById('_mic_div');
			if (muted) {
			} else {
				_this.getTopToolbar().recBtn.enable();
				window.recEnabled = true;
				div.style.right = '2000px';
				div.style.bottom = '2000px';
			}
		};
		window.dataDetect = function(msg) {
			if (window.console)
				console.log(msg);
		};
	};
	if (!document.getElementById('_mic_flash'))
		initMic();
};
Ext.extend(SpeechTestToolPanel, Ext.Window, {
	showWindow : function() {
		this.show();
	}
});