SpeechWordPanel = function(cfg) {
	cfg = cfg || {};
	var self = this;
	this.pageSize = 20;
	this.store;
	this.matchIndex = 0;
	this.SpeechWordRecord = Ext.data.Record.create(['id', 'isEdit', 'name', 'ruleType', 'weighing', 'time', 'isEnable', 'categoryid', 'categoryName', 'path']);
	this.SpeechWordProxy = new Ext.data.HttpProxy({
		  url : 'speech-word!list.action'
	  });
	var _store = new Ext.data.Store({
		  proxy : self.SpeechWordProxy,
		  reader : new Ext.data.JsonReader({
			    totalProperty : 'total',
			    idProperty : 'id',
			    root : 'data',
			    fields : self.SpeechWordRecord
		    })
	  });
	var ruleStoreCondition = new Ext.data.SimpleStore({
		  fields : ["key", "value"],
		  data : [['', '全部'], ['0', '普通类型'], ['1', '规则类型']]
	  });
	var ruleCombo = new Ext.form.ComboBox({
		  triggerAction : 'all',
		  fieldLabel : '类型',
		  hiddenName : 'ruleType',
		  editable : false,
		  width : 100,
		  mode : 'local',
		  store : ruleStoreCondition,
		  displayField : 'value',
		  valueField : 'key'
	  });
	this.ruleCombo = ruleCombo;
	var ruleStore = new Ext.data.SimpleStore({
		  fields : ["key", "value"],
		  data : [['0', '普通类型'], ['1', '规则类型']]
	  });
	var seleMode = new Ext.grid.RowSelectionModel();
	this.seleMode = seleMode;
	this.pbar = new Ext.PagingToolbar({
		  store : _store,
		  displayInfo : true,
		  pageSize : this.pageSize,
		  beforePageText : '第',
		  afterPageText : '页，共{0}页',
		  displayMsg : '第{0}到{1}条记录，共{2}条',
		  emptyMsg : "没有记录"
	  });

	this.pbar.add('-', {
		  xtype : 'label',
		  ref : 'statLabel',
		  text : '词类'
	  }, '&nbsp;&nbsp;&nbsp;');
	var speechWordEditGrid = this.speechWordEditGrid = new Ext.grid.EditorGridPanel({
		  flex : 4,
		  style : 'border-right: 1px solid ' + sys_bdcolor,
		  border : false,
		  clicksToEdit : 1,
		  frame : false,
		  columnLines : true,
		  selModel : this.seleMode,
		  pasteable : true,
		  colModel : new Ext.grid.ColumnModel({
			    defaults : {
				    sortable : true
			    },
			    columns : [new Ext.grid.RowNumberer(), new Ext.form.Hidden({
				        dataIndex : 'isEdit',
				        width : 0,
				        hidden : true
			        }), new Ext.form.Hidden({
				        dataIndex : 'isEnable',
				        width : 0,
				        hidden : true
			        }), {
				      header : '词语',
				      dataIndex : 'name',
				      width : 7,
				      editor : new Ext.form.TextField(),
				      renderer : Dashboard.newTemplateRenderer()
			      }, {
				      header : '类型',
				      dataIndex : 'ruleType',
				      width : 1,
				      editor : new Ext.form.ComboBox({
					        triggerAction : 'all',
					        fieldLabel : '类型',
					        hiddenName : 'ruleType',
					        emptyText : '选择类型',
					        editable : false,
					        width : 100,
					        mode : 'local',
					        store : ruleStore,
					        displayField : 'value',
					        valueField : 'key'
				        }),
				      renderer : function(v, meta, r) {
					      var index = ruleStore.find('key', v);
					      if (index != -1) {
						      return ruleStore.getAt(index).data.value
					      }
					      return v;
				      }
			      }, {
				      header : '权重',
				      dataIndex : 'weighing',
				      width : 0.5,
				      editor : new Ext.form.NumberField({
					        decimalPrecision : 2
				        }),
				      validator : function(v) {
					      if (!Ext.isNumber(v)) {
						      return '权重值只能是小数、整数';
					      }
					      return true;
				      }
			      }, {
				      header : '类别',
				      dataIndex : 'categoryName',
				      width : 1
			      }, {
				      header : '时间',
				      dataIndex : 'time',
				      width : 1.5,
				      editor : new Ext.form.TextField()
			      }]
		    }),
		  store : _store,
		  bbar : this.pbar,
		  viewConfig : {
			  forceFit : true,
			  enableRowBody : true,
			  getRowClass : function(record, rowIndex, rowParams, store) {
				  // 禁用数据显示灰色
				  var cls = "white-row";
				  if (record.data.isEnable != 1) {
					  cls = 'disabled-row';
				  }
				  return cls;
			  }
		  },
		  listeners : {
			  afteredit : function(e) {
				  e.record.set('isEdit', 1);
				  var value = e.record.get("name");
				  if (typeof(value) !== "undefined") {
					  var startTag = "[";
					  var endTag = "]";
					  var match = true;
					  var index = 0;
					  for (i = 0; i <= value.length - 1; i++) {
						  var s = value[i];
						  if (startTag == s) {
							  index = i;
							  if (!match) {
								  break;
							  }
							  match = false;
						  } else if (s == endTag && !match && (index + 1 != i)) {
							  match = true;
						  } else if (s == endTag && match) {
							  match = false;
							  break;
						  }
					  }
					  if (match) {
						  self.matchIndex = self.matchIndex != 0 ? self.matchIndex - 1 : 0;
						  if (self.matchIndex == 0) {
							  self.getTopToolbar().save.enable();
						  }
					  } else {
						  self.matchIndex += 1;
						  self.getTopToolbar().save.disable();
						  Ext.Msg.alert("异常", "词语内容格式错误");
						  return;
					  }
				  }
			  }
		  },
		  scope : this
	  });
	var copyContent = "";
	var contextmenu = new Ext.menu.Menu({
		  // plain : true,
		  name : 'rightClick',
		  items : [{
			    text : '所属类别',
			    ref : 'categoryDir',
			    iconCls : 'icon-speechcagory',
			    width : 45,
			    handler : function() {
				    var selections = self.speechWordEditGrid.getSelectionModel().getSelections();
				    if (selections.length == 0) {
					    Ext.Msg.alert('提示', "先选择行");
					    return;
				    }
				    var record = selections[0];
				    var path = record.get('path');
				    self.navPanel.showWordTab(record.get("categoryid"), path);
			    }
		    }, {
			    text : '复制',
			    ref : 'speechWordCopy',
			    iconCls : 'icon-copy',
			    width : 45,
			    handler : function() {
				    var selections = self.speechWordEditGrid.getSelectionModel().getSelections();
				    if (selections.length == 0) {
					    Ext.Msg.alert('提示', "先选择行");
					    return;
				    }
				    var record = selections[0];
				    copyContent = record.get("name");
			    }
		    }, {
			    text : '粘贴',
			    iconCls : 'icon-paste',
			    ref : 'speechWordPaste',
			    width : 45,
			    handler : function() {
				    var selections = self.speechWordEditGrid.getSelectionModel().getSelections();
				    if (selections.length == 0) {
					    Ext.Msg.alert('提示', "先选择行");
					    return;
				    }
				    if (copyContent) {
					    selections[0].set("name", copyContent);
				    }
			    }
		    }, '-', {
			    text : '禁用',
			    iconCls : 'icon-disable',
			    ref : 'speechWordDisabled',
			    width : 45,
			    handler : function() {
				    var selections = self.speechWordEditGrid.getSelectionModel().getSelections();
				    if (selections.length == 0) {
					    Ext.Msg.alert('提示', "先选择行");
					    return;
				    }
				    self.isEnable(selections[0], false);
			    }
		    }, {
			    text : '激活',
			    iconCls : 'icon-enable',
			    ref : 'speechWordEnable',
			    width : 45,
			    handler : function() {
				    var selections = self.speechWordEditGrid.getSelectionModel().getSelections();
				    if (selections.length == 0) {
					    Ext.Msg.alert('提示', "先选择行");
					    return;
				    }
				    self.isEnable(selections[0], true);
			    }
		    }]

	  });
	this.menu = contextmenu;
	_store.baseParams['limit'] = this.pageSize;
	var tbarEx = new Ext.Toolbar({
		  items : [{
			    text : '搜索',
			    ref : 'searchBtn',
			    xtype : 'splitbutton',
			    iconCls : 'icon-search',
			    handler : function() {
				    self.searchWord();
			    },
			    menu : {
				    items : [{
					      text : '精确查询',
					      checked : false,
					      name : 'precise',
					      ref : '../precise'
				      }]
			    }
		    }, '-', '词语:', {
			    ref : 'searchSpeechwordname',
			    xtype : 'textfield',
			    emptyText : '输入词类名搜索...',
			    listeners : {
				    specialkey : function(field, e) {
					    if (e.getKey() == Ext.EventObject.ENTER) {
						    self.searchWord();
					    }
				    }
			    }
		    }, '类型:', ruleCombo, '编辑时间:从', {
			    ref : 'searchSpeechwordBdate',
			    xtype : 'datefield',
			    emptyText : '开始时间',
			    width : 150,
			    format : 'Y-m-d',
			    listeners : {
				    specialkey : function(field, e) {
					    if (e.getKey() == Ext.EventObject.ENTER) {
						    self.searchWord();
					    }
				    }
			    }
		    }, '到', {
			    ref : 'searchSpeechwordEdate',
			    xtype : 'datefield',
			    width : 150,
			    emptyText : '结束时间',
			    format : 'Y-m-d',
			    listeners : {
				    specialkey : function(field, e) {
					    if (e.getKey() == Ext.EventObject.ENTER) {
						    self.searchWord();
					    }
				    }
			    }
		    }]
	  });
	this.tbarEx = tbarEx;
	SpeechWordPanel.superclass.constructor.call(this, {
		  border : false,
		  frame : false,
		  layout : {
			  type : 'hbox',
			  align : 'stretch'
		  },
		  items : [speechWordEditGrid],
		  buttonAlign : 'center',
		  tbar : [{
			    text : '删除',
			    iconCls : 'icon-table-delete',
			    handler : function() {
				    self.deleteRes();
			    }
		    }, {
			    text : '新增',
			    ref : 'addSpeechwordRow',
			    xtype : 'splitbutton',
			    iconCls : 'icon-table-add',
			    menu : {
				    items : [{
					      xtype : 'textfield',
					      name : 'addRowSize',
					      fieldLabel : '数量',
					      width : 60,
					      value : '1',
					      ref : '../addRowSize'
				      }]
			    },
			    handler : function() {
				    var addSize = this.addRowSize.getValue()
				    if (addSize > 20) {
					    Ext.Msg.alert("提示", "新增数量不能超过 20");
					    return;
				    }
				    self.addRec(addSize);
			    }
		    }, {
			    text : '刷新',
			    iconCls : 'icon-refresh',
			    handler : function() {
				    // self.loadWords(this.categoryId, this.isMini,
				    // this.isWordclass);
				    self.refresh();
			    }
		    }, {
			    text : '保存',
			    ref : 'save',
			    iconCls : 'icon-table-save',
			    handler : function() {
				    self.allSave();
			    }
		    }],
		  listeners : {
			  'render' : function() {
				  tbarEx.render(this.tbar)
			  }
		  }
	  });

	// speechWordEditGrid.on("scrollbottomclick", function(e) {
	// speechWordEditGrid.batchAdd(1);
	// });

	speechWordEditGrid.on("shiftright", function(e) {
		  var col = g.findMostLeftEditCol();
		  g.getSelectionModel().selectRow(0);
		  this.stopEditing();
		  g.startEditing(0, col);
	  });
	speechWordEditGrid.on("rowcontextmenu", function(grid, rowIndex, e) {
		  e.preventDefault();// 阻止事件的传递
		  grid.getSelectionModel().selectRow(rowIndex);
		  var selection = self.speechWordEditGrid.getSelectionModel().getSelections();
		  var row = selection[0];
		  self.menu.categoryDir.show();
		  self.menu.speechWordCopy.show();
		  self.menu.speechWordPaste.show();
		  self.menu.speechWordEnable.show();
		  self.menu.speechWordDisabled.show();
		  if (!row.get("name")) {
			  self.menu.speechWordCopy.hide();
			  self.menu.speechWordEnable.hide();
		  }
		  if (row.get("isEnable") == 1) {
			  self.menu.speechWordEnable.hide();
		  } else {
			  self.menu.speechWordDisabled.hide();
		  }
		  if (copyContent == "") {
			  self.menu.speechWordPaste.hide();
		  }
		  if (self.isWordclass == 1) {
			  self.menu.categoryDir.hide();
		  }
		  contextmenu.showAt(e.getXY());// 菜单的生成位置
	  });
	this.isWordclass;
	this.categoryId = '';
	this.isMini;
};

Ext.extend(SpeechWordPanel, Ext.Panel, {
	  loadWords : function(categoryId, isMini, isWordclass) {
		  this.isWordclass = isWordclass;
		  this.categoryId = categoryId;
		  this.isMini = isMini;
		  if (this.speechWordEditGrid.colModel != null) {
			  if (1 == isMini) {
				  this.speechWordEditGrid.colModel.setHidden(4, true);
				  this.speechWordEditGrid.colModel.setHidden(5, true);
			  } else {
				  this.speechWordEditGrid.colModel.setHidden(4, false);
				  this.speechWordEditGrid.colModel.setHidden(5, false);
			  }
		  }
		  if (typeof(isWordclass) !== "undefined" && isWordclass == 1) {
			  if (typeof(this.getTopToolbar().addSpeechwordRow) != "undefined")
				  this.getTopToolbar().addSpeechwordRow.enable();
		  } else {
			  if (typeof(this.getTopToolbar().addSpeechwordRow) != "undefined")
				  this.getTopToolbar().addSpeechwordRow.disable();
		  }
		  var store = this.speechWordEditGrid.getStore();
		  if (store) {
			  for (var key in store.baseParams) {
				  if (key && key.indexOf('queryParam.') != -1) {
					  delete store.baseParams[key];
				  } else {
					  this.mask = false;
				  }
			  }
			  store.baseParams['categoryId'] = categoryId;
			  store.baseParams['isMini'] = isMini;
			  store.load({
				    params : {
					    start : 0,
					    limit : this.pageSize
				    }
			    });
			  this.store = store;
		  }
	  },
	  countWordclass : function(isMini, categoryId) {
		  var self = this;
		  categoryId = categoryId.length == 1 ? null : categoryId
		  Ext.Ajax.request({
			    url : 'speech-word!countWordclass.action',
			    params : {
				    'isMini' : isMini,
				    'categoryId' : categoryId
			    },
			    success : function(resp) {
				    var obj = Ext.util.JSON.decode(resp.responseText);
				    var count = obj.message;
				    if (obj.success) {
					    self.pbar.statLabel.setText('词类 ' + (count == 0 ? '' : count) + ' 个');
				    }
			    },
			    failure : function(resp) {
				    Ext.Msg.alert('错误', resp.responseText)
			    }
		    });
	  },
	  refresh : function() {
		  this.loadWords(this.categoryId, this.isMini, this.isWordclass);
	  },
	  deleteRes : function() {
		  var rows = this.seleMode.getSelections();
		  if (rows.length == 0) {
			  Ext.Msg.alert('提示', '请先选择要删除的记录！');
			  return false;
		  }
		  var self = this;
		  Ext.Msg.confirm('确定框', '确定要删除这' + rows.length + '记录吗？', function(sel) {
			    if (sel == 'yes') {
				    var ids = [];
				    Ext.each(rows, function(_item) {
					      ids.push(_item.id)
				      })
				    Ext.Ajax.request({
					      url : 'speech-word!deleteByIds.action',
					      params : {
						      ids : ids.join(',')
					      },
					      success : function(resp) {
						      Dashboard.setAlert('删除成功！')
						      self.speechWordEditGrid.getStore().reload();
					      },
					      failure : function(resp) {
						      Ext.Msg.alert('错误', resp.responseText)
					      }
				      });
			    }
		    });
	  },
	  addRec : function(size) {
		  if (this.isWordclass != 1) {
			  Ext.Msg.alert("错误", "非词类类别不能新增");
			  return;
		  }
		  var s = this.speechWordEditGrid.getStore();
		  var c = s.getTotalCount();
		  for (var i = 0; i < size; i++) {
			  s.add(new s.recordType({}, Ext.id()));
		  }
	  },
	  allSave : function() {
		  var self = this;
		  var res = 0;
		  var isEmpty = false;
		  var resData = [];
		  var deleteIds = [];
		  Ext.each(this.speechWordEditGrid.getStore().getRange(), function(record) {
			    res++;
			    var id = record.data["id"];
			    var name = record.data["name"];
			    var isEdit = record.data["isEdit"];
			    var categoryid = record.data["categoryid"];
			    if ((self.categoryId.length == 1 && typeof(categoryid) == 'undefined') && isEdit == 1 && self.isWordclass != 1) {
				    Ext.Msg.alert("错误", "第" + res + "行不能新增在非 词类类别 下");
				    isEmpty = true;
				    return false;
			    }
			    if (self.isMini == 1) {
				    if (name.indexOf('[') > -1 || name.indexOf(']') > -1 || name.indexOf("~") > -1 || name.indexOf('@') > -1 || name.indexOf('?') > -1 || name.indexOf('？') > -1) {
					    Ext.Msg.alert("错误", "第" + res + "行 " + "词语包含非法字符");
					    isEmpty = true;
					    return false;
				    }
				    if (!name && isEdit == 1 && id) {
					    deleteIds.push(id);
					    return;
				    }
			    } else if (!name && isEdit == 1 && id) {
				    deleteIds.push(id);
				    return;
			    }
			    if (isEdit == 1 && name) {
				    resData.push(record.data);
			    }
		    });

		  if (isEmpty) {
			  return;
		  }
		  if (deleteIds.length > 0) {
			  Ext.Msg.confirm('确定框', '确定要保存原有的数据为空(删除操作)的记录吗？', function(sel) {
				    if (sel == 'yes') {
					    resDataObj = Ext.encode(resData);
					    self.getEl().mask('保存...');
					    Ext.Ajax.request({
						      url : 'speech-word!save.action',
						      params : {
							      data : resDataObj,
							      deleteIds : deleteIds.join(','),
							      categoryId : self.categoryId
						      },
						      success : function(form, action) {
							      self.getEl().unmask()
							      var obj = Ext.util.JSON.decode(form.responseText);
							      if (!obj.success) {
								      Ext.Msg.alert("错误", obj.message);
								      return;
							      }
							      Dashboard.setAlert("保存成功!");
							      self.speechWordEditGrid.getStore().reload();
						      },
						      failure : function(response) {
							      self.getEl().unmask()
							      Ext.Msg.alert("错误", response.responseText);
						      },
						      scope : this
					      });
				    }
			    });
		  } else {
			  if (resData.length <= 0) {
				  Ext.Msg.alert("提示", "修改或新增的数据为空");
				  return;
			  }
			  resData = Ext.encode(resData);
			  self.getEl().mask('保存...');
			  Ext.Ajax.request({
				    url : 'speech-word!save.action',
				    params : {
					    data : resData,
					    categoryId : this.categoryId
				    },
				    success : function(form, action) {
					    this.getEl().unmask()
					    var obj = Ext.util.JSON.decode(form.responseText);
					    if (!obj.success) {
						    Ext.Msg.alert("错误", obj.message);
						    return;
					    }
					    Dashboard.setAlert("保存成功!");
					    self.speechWordEditGrid.getStore().reload();
				    },
				    failure : function(response) {
					    this.getEl().unmask()
					    Ext.Msg.alert("错误", response.responseText);
				    },
				    scope : this
			    });
		  }
	  },
	  isEnable : function(select, enable) {
		  var self = this;
		  var mData = [];
		  Ext.Msg.show({
			    title : '提示',
			    msg : '您确定要 ' + (enable ? '激活' : '禁用') + ' 该词语',
			    buttons : {
				    yes : '是',
				    no : '否'
			    },
			    fn : function(choice) {
				    if ('no' == choice) {
					    return
				    } else {
					    var obj = {};
					    obj = select.data;
					    if (enable) {
						    obj.isEnable = 1;
					    } else {
						    obj.isEnable = 0;
					    }
					    mData.push(obj);
					    Ext.Ajax.request({
						      url : 'speech-word!save.action',
						      params : {
							      'data' : Ext.encode(mData),
							      'categoryId' : self.categoryId
						      },
						      success : function(response) {
							      var result = Ext.util.JSON.decode(response.responseText);
							      if (result.success) {
								      Dashboard.setAlert((enable ? '激活' : '禁用') + " 成功!");
								      self.speechWordEditGrid.getStore().reload();
							      }
						      },
						      failure : function(response) {
							      Ext.Msg.alert('错误', (enable ? '激活' : '禁用') + '失败')
						      },
						      scope : this
					      });
				    }
			    }
		    });
	  },
	  searchWord : function() {
		  var _name = this.tbarEx.searchSpeechwordname.getValue().trim();
		  var _ruleType = this.ruleCombo.getValue();
		  var _bdate = this.tbarEx.searchSpeechwordBdate.getValue();
		  var _edate = this.tbarEx.searchSpeechwordEdate.getValue();
		  var self = this;
		  var store = self.speechWordEditGrid.getStore();
		  for (var key in store.baseParams) {
			  if (key && key.indexOf('queryParam.') != -1)
				  delete store.baseParams[key];
		  }
		  if (_name)
			  store.setBaseParam('queryParam.name', _name);
		  if (_ruleType)
			  store.setBaseParam('queryParam.ruleType', _ruleType)
		  if (_bdate)
			  store.setBaseParam('queryParam.beginDate', _bdate);
		  if (_edate)
			  store.setBaseParam('queryParam.endDate', _edate);
		  if (this.tbarEx.searchBtn.precise.checked)
			  store.setBaseParam('queryParam.preciseSearch', true)
		  store.load();
	  }
  });
