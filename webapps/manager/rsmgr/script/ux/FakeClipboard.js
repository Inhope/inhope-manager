FakeClipboard = Ext.extend(Object, {

			constructor : function(boundEl) {
				this._el = boundEl;
				FakeClipboard.superclass.constructor.call(this, boundEl)
			},

			_onpaste : Ext.emptyFn,
			_init : function() {
				var el = this._el;
				this._inited = true;
				el.on("keydown", function(e) {
//					console.log(e.ctrlKey, e.getKey())
							if (e.ctrlKey && e.getKey() == 86) {
				(function		(el) {
									this._onpaste(el.dom.value)
									el.dom.value = '';
								}).defer(10, this, [el])
							}
						}, this);
			},
			listen : function(fn) {
				var el = this._el;
				if (!this._inited)
					this._init();
				this._onpaste = fn;
				el.focus();
			},
			decodeExcel : function(value) {
				var s = value;
				var data = [], row = [], cell = '', b = false;
				for (var i = 0; i < s.length; i++) {
					var c = s.charAt(i);
					if (c == '"') {
						if (b) {
							if (i + 1 < s.length && s.charAt(i + 1) == '"') {
								cell += '"';
								i++;
								continue;
							} else {
								b = false;
							}
						} else if (cell.length > 0) {
							cell += c
						} else {
							b = true
						}
					} else if (!b && c == '\t') {
						if (cell.length > 0)
							row.push(cell);
						else
							row.push(null);
						cell = '';
					} else if (!b && c == '\r') {
						continue;
					} else if (!b && c == '\n') {
						if (cell.length > 0)
							row.push(cell);
						cell = '';
						data.push(row);
						row = [];
					} else {
						cell += c;
					}
				}
				if (cell.length > 0)
					row.push(cell);
				if (row.length > 0)
					data.push(row);
				return data;
			}
		});