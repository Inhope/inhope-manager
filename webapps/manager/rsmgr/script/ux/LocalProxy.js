LocalProxy = function(data) {
	var api = {};
	api[Ext.data.Api.actions.read] = true;
	api[Ext.data.Api.actions.create] = true;
	api[Ext.data.Api.actions.update] = true;
	api[Ext.data.Api.actions.destroy] = true;
	LocalProxy.superclass.constructor.call(this, {
				api : api
			});
	this.setData(data);
};

Ext.extend(LocalProxy, Ext.data.DataProxy, {

			setData : function(data) {
				this.data = data;
				this.isModified = false;
			},
			doRequest : function(action, rs, params, reader, callback, scope,
					arg) {

				var root = this.data[reader.meta.root];
				var total = this.data[reader.meta.totalProperty];
				var idProperty = reader.meta.idProperty.value;

				if (action == Ext.data.Api.actions.read) {
					var records = []
					if (root) {
						for (var i = 0; i < root.length; i++) {
							var id = root[i][idProperty]
							if (!id)
								id = 'ER-' + i
							records.push(new reader.recordType(root[i], id));
						}
						var result = {
							success : true,
							records : records,
							totalRecords : total ? total : records.length
						};
						callback.call(scope, result, arg, true);
					}
				} else if (action == Ext.data.Api.actions.create
						|| action == Ext.data.Api.actions.update) {
					Ext.each(rs, function(record, index, rs) {
								var b = true
								for (var i = 0; i < root.length; i++) {
									if (root[i] == record.data)
										b = false;
								}
								if (b)
									root.push(record.data);
							}, this);
					this.isModified = true;
					callback.call(scope, {}, arg, true);
				} else if (action == Ext.data.Api.actions.destroy) {
					if (root) {
						Ext.each(rs, function(record, index, rs) {
									for (var i = 0; i < root.length; i++) {
										if (root[i] == record.data) {
											root.splice(i, 1)
											i--
										}
									}
								}, this);
						this.isModified = true;
					}
					callback.call(scope, {}, arg, true);
				}
			}
		});