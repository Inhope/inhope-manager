<%@ page language="java" import="java.util.*" pageEncoding="utf-8" contentType="text/html; charset=utf-8" %>
<jsp:directive.page import="org.apache.commons.codec.digest.DigestUtils"/>
<%
	request.setAttribute("username","admin");
	request.setAttribute("password",new String(DigestUtils.shaHex("admin")));
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title>Test for LOGIN features</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">
		<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<style>
form div div{
	float: left;width: 250px;font-weight: bold
}
input[type=text],textarea{
	width:100%
}

</style>
	</head>

	<body>
	<div style="background: red">${param.message}</div>
		<form action="authmgr/auth!login.action">
			<div><div>- username:</div><input type="text" name="username" value="${username}"></div>
			<div><div>- password:</div><input type="text" name="password" value="${password}"></div>
			<div><div>- kb.obj.list.defaultquery:</div><textarea type="text" name="kb.obj.list.defaultquery">
{editor:"admin",startEditTime:"2011-07-25",endEditTime:"2011-07-25"}
			</textarea></div>
			<div><div>- kb.obj.list.onlymyown:</div><input type="text" name="kb.obj.list.onlymyown" value='true'></div>
			<div><div>- pwd_hash:</div><input type="text" name="pwd_hash" value="sha1"></div>
			<div><div>- failure_url:</div><input type="text" name="failure_url" value="/manager/test-login-features.jsp"></div>
			<div><div>- success_url:</div><input type="text" name="success_url" value=""></div>
			<input type="submit" value="登陆">
		</form>
		<input type="button" onclick="popup()" value="弹出窗口登陆"></button>
		<div id="debugText"></div>
		<script>
		function popup(){
			var url = "wrapper.html?contentUrl="
			var fm = document.forms[0];
			var paramString = [];
			for(k in fm){
				if (k && fm[k] && fm[k].name && fm[k].tagName){
					paramString.push(fm[k].name+'='+encodeURIComponent(fm[k].value))
				}
			}
			paramString = paramString.join('&')
			document.getElementById('debugText').innerHTML = '<b>PARAMETERS = </b>'+paramString;
			var urlTpl = "authmgr/auth!login.action?"+paramString
			document.getElementById('debugText').innerHTML += '<br><b>URL = </b>'+urlTpl;
			var win = showModalDialog(url+encodeURIComponent(urlTpl),window,'scroll:yes;status:no;dialogWidth:800px;dialogHeight:400px');
		}
		</script>
	</body>
</html>
