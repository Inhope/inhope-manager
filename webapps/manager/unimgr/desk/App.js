var moduleConfig = [ {
	id : 'wordmgr',
	iconCls : 'desktop-module-wordmgr',
	icon : 'top-module-ommgr',
	module : 'wordmgr',
	url : 'kbmgr/wordclass.jsp',
	name : '词类管理'
}, {
	id : 'smtmgr',
	iconCls : 'desktop-module-abs',
	icon : 'top-module-abs',
	module : 'smtmgr',
	url : 'kbmgr/ontology.jsp?classtype=3',
	name : '抽象语义'
}, {
	id : 'ontologymgr',
	iconCls : 'desktop-module-ontologymgr',
	icon : 'top-module-rsmgr',
	module : 'ontologymgr',
	url : 'kbmgr/ontology.jsp?classtype=0',
	name : '本体管理'
}, {
	id : 'kbmgr',
	iconCls : 'desktop-module-kbmgr',
	icon : 'top-module-kbmgr',
	module : 'kbmgr',
	url : 'kbmgr/knowledge.jsp',
	name : '知识管理'
}, {
	id : 'absmgr',
	iconCls : 'desktop-module-auditmgr',
	icon : 'top-module-botmgr',
	module : 'absmgr',
//	url : 'kbmgr/task.jsp',
	name : '语义任务',
	access : true
}, {
	id : 'spomgr',
	name : '语义标注',
	module : 'spomgr',
	iconCls : 'desktop-module-spomgr',
	icon : 'top-module-mktmgr',
	access : true
}, {
	id : 'auditmgr',
	iconCls : 'desktop-module-auditmgr',
	icon : 'top-module-botmgr',
	module : 'auditmgr',
	url : 'kbmgr/audit.jsp',
	name : '审核管理'
}, {
	id : 'chatmgr',
	iconCls : 'desktop-module-spomgr',
	icon : 'top-module-spomgr',
	name : '通用聊天',
	module : 'chatmgr',
	access : true
//	url : 'kbmgr/chat.jsp',
//	perm : 'chat.mgr'
}, {
	id : 'deepmgr',
	iconCls : 'desktop-module-spomgr',
	icon : 'top-module-spomgr',
	module : 'deepmgr',
	name : '深度学习',
	access : true
}, {
	id : 'grammarmgr',
	iconCls : 'desktop-module-wordmgr',
	icon : 'top-module-ommgr',
	module : 'grammarmgr',
	name : '语法词典',
	access : true
}, {
	id : 'authmgr',
	iconCls : 'desktop-module-authmgr',
	icon : 'top-module-authmgr',
	module : 'authmgr',
	name : '系统管理',
	perm : 'auth'
}];
		
var loginForm = new Ext.form.FormPanel({
	frame : false,
	border : false,
	waitMsgTarget : true,
	bodyStyle : 'padding : 30px 20px;',
	defaults : {
		labelSeparator : " ：",
		allowBlank : false,
		msgTarget : 'side',
		anchor : '100%'
	},
	items : [{
		xtype : 'textfield',
		fieldLabel : '用户名',
		id: 'username',
		name : 'username',
		listeners : {
			specialkey : function(field, event) {
				if (event.getKey() == event.ENTER) {
					login();
				}
			}
		}
	},{
		xtype : 'textfield',
		fieldLabel : '密码',
		name : 'password',
		inputType: 'password',
		listeners : {
			specialkey : function(field, event) {
				if (event.getKey() == event.ENTER) {
					login();
				}
			}
		}
	}, {
		id : 'randCode',  
		xtype : 'textfield',
		fieldLabel : '验证码',
		name : 'captcha',
		listeners : {
			specialkey : function(field, event) {
				if (event.getKey() == event.ENTER) {
					login();
				}
			}
		}
	}],
	buttons : [{
		text : '登录',
		handler : function() {
			login();
		}
	},{
		text : '重置',
		handler : function() {
			loginForm.form.reset();
		}
	}]
});

var loginWindow = new Ext.Window({
	title : '小i领域语义库统一管理平台',
	iconCls: 'login-title-icon',
	closable : false,
	closeAction : 'hide',
	width : 400,
	minWidth : 350,
	modal : true,
	items : loginForm,
	listeners : {
		'show' : function(w) {
			loginWindow.child('form').child('#username').focus(true, true);
		}
	}
});

var usr_map = {};

login = function(probe) {
	loginForm.el.mask('登录验证中，请稍等...', 'x-mask-loading');
	if (loginForm.form.isValid()) {
		var _form = loginForm.getForm().getValues();
		var password = _form.password;
		var username = _form.username;
		Ext.Ajax.request({
				url :  '../app/g/authmgr/auth!login.action',
				params : {
					username : username,
					password : username == 'INCE.SUPERVISOR'?password:CryptoJS.SHA1(password).toString(),
					captcha : _form.captcha
				},
				success : authCallback,
				failure : markInvalid
			});
	} else {
		loginForm.el.unmask();
	}
};

islogin = function(response, options) {
	if (isCaptchaEnabled == 'true') {
		Ext.getCmp('randCode').show();
		Ext.getCmp('randCode').enable();
	} else {
		Ext.getCmp('randCode').hide();
		Ext.getCmp('randCode').disable();
	}
	var result = Ext.decode(response.responseText);
	if (!result.success) {
		loginWindow.show();
		if (isCaptchaEnabled == 'true') {
			var bd = Ext.getDom('randCode');
			var bd2 = Ext.get(bd.parentNode);
			bd2.createChild([{
				tag : 'img',
				src : 'auth!captcha.action',
				onClick : "this.src='auth!captcha.action?id='+Math.random()",
				align : 'absbottom',
				style : 'cursor : pointer;'
			}]);
		}
	} else {
		getloginUsr();
		menustore.load({
			callback : function(records, options, success) {
				defaultDm(records);
			}
		});
	}
};

var defaultDm = function(records) {
	if (records.length > 0) {
		for (var i = 0; i < records.length; i++) {
			if (records[i].raw.allow == 1) {
				currentDm.setText(records[i].get('text'));
				currentDm.val = records[i].get('id');
				return;
			} else {
				var m = records[i].raw.menu.items[0];
				currentDm.setText(m.text);
				currentDm.val = m.id;
				return;
			}
			
		}
	}
}

authCallback = function(response, options) {
	var result = Ext.decode(response.responseText);
	if (result.success) {
		loginWindow.hide();
		getloginUsr();
		menustore.load({
			callback : function(records, options, success) {
				defaultDm(records);
			}
		});
	} else {
		if (result.message != 'probe')
			markInvalid(result.message, result.data);
		loginForm.el.unmask();
	}
};

markInvalid = function(response, field) {
	loginForm.el.unmask();
	Ext.Msg.alert("错误", response);
};

login0 = function(probe) {
	Ext.Ajax.request({
		url :  '../app/g/authmgr/auth!login.action',
		params : {
			probe : probe
		},
		success : islogin,
		failure : markInvalid
	});
};

Ext.define('Menu', {
        extend: 'Ext.data.Model',
        fields: [ 'id', 'text', 'iconCls']
    });
var menustore = Ext.create('Ext.data.Store', {
        model: 'Menu',
        autoLoad : false,
        proxy: {
            type: 'ajax',
            url: '../app/g/authmgr/auth!listAllowedDomain.action',
            reader: {
                type: 'json',
                root: 'root'
            }
        }
    });
var menu = Ext.create('DomainSetting', {
			title : '领域库选择：',
			width : 200,
			height : 300,
			store : menustore,
			menuItem : function(mu) {
				if (mu.allow == 0) {
					Ext.Msg.alert('提示', '当前用户无权限选择[<font color="red">' + mu.text + '</font>]领域库！');
					return;
				}
				this.up('button').setText(mu.text);
				this.up('button').val = mu.id;
				myDesktopApp.onClose();
			}
		});

var currentUsr = new Ext.Button({
	iconCls : 'user',
	text : '当前用户：',
	handler : function() {
		myDesktopApp.onLogout();
	}
});

var currentDm = new Ext.Button({
	text : '当前领域库',
	iconCls : 'top-module-kbmgr',
	val : '',
	menu : menu
});


getloginUsr = function() {
	var currenUser = http_get('../app/g/authmgr/auth!getLoginUsername.action');
	currentUsr.setText('当前用户：' + currenUser);
	usr_map[currenUser] = [];
}

		
var menuBtn = [new MyDesktop.WordTool(), new MyDesktop.SentenceTool()];
Ext.each(moduleConfig, function(m, i) {
	var p = Ext.extend(Ext.ux.desktop.Module, {
		id : m.id,
		init : function() {
			this.launcher = {
				text : m.name,
				iconCls: m.icon,
				handler : this.createWindow,
				scope : this
			}
		},
	
	createWindow : function() {
		var me = this;
		var domain = currentDm.val;
		if (domain == '') {
				Ext.MessageBox.alert('提示：', '<font color="red">请在右下角选择领域库！</font>');
				return;
			};
		if(usr_map[currentUsr.getText().substring(5)].indexOf(domain) == 0) {
			showWindow(me, m, domain);
		} else {
			Ext.Ajax.request({
				url : '../app/g/authmgr/auth!signSSO.action?path=' + domain,
				success : function(response, options) {
					var ssoparam = Ext.decode(response.responseText);
					if(ssoparam) {
						Ext.Ajax.request({
							url : '../app/' + domain + '/authmgr/auth!login.action?' + ssoparam,
							success : function(response, options) {
								var result = Ext.decode(response.responseText);
								if (result.success) {
									usr_map[currentUsr.getText().substring(5)].push(domain);
									showWindow(me, m, domain);
								} else {
									Ext.Msg.alert('提示',  result.message);
								}
							},
							failure : function(response, options) {
								Ext.Msg.alert('提示', '登陆到领域' + domain + '失败');
								return;
							}
						});
					} else {
						Ext.Msg.alert('提示', '登陆到领域' + domain + '失败');
					}
				
				}
			});
		}
	}});
	menuBtn.push(new p());
});

var showWindow = function(me, m, domain) {
	if(m.perm != undefined) {
		var isAllow = http_get('../app/g/kbmgr/task-base!isAllow.action?perm='+m.perm);
		if(isAllow == 0) {
			Ext.Msg.alert('提示', '当前用户无权限操作[' + m.name + ']模块！');
			return ;
		}
	}
	var desktop = me.app.getDesktop();
	var win = desktop.getWindow(m.id);
	var tempUri = '../app/' + domain + '/' + (m.url ? m.url : m.id + '/');
	if (m.access) {
		var accessUrl = http_get('../app/g/access-sys!getAccessUrl.action?moduleString=' + m.id);
		tempUri = accessUrl;
	}
	if (!win) {
		win = desktop.createWindow({
			id : m.id,
			layout : 'fit',
			constrain : false,
			title : m.name == '语法词典' ? '现代汉语语法信息词典' : m.name,
			iconCls : m.icon,
			width : '80%',
			height : 500,
			border : false,
			items : [{
				xtype : "component",
				frame: false,
				border: true,
				autoEl : {
					tag : "iframe",
					src : tempUri
				},
				listeners : {
					load : {
						element : 'el',
						fn : function() {
							this.parent().unmask();
						}
					},
					render : function() {
						this.up('panel').body.mask('正在加载: ' + m.name + ' 请稍后...');
					}
				}
			}],
			shim : false,
			animCollapse : false,
			constrainHeader : true,
			listeners : {
				close : function(w) {
					w.restore();
				},
				maximize : function(w) {
					w.setPosition(document.body.scrollLeft - 4, document.body.scrollTop - 4);
				}
			}
			});
		}
	win.show();
}


Ext.define('MyDesktop.App', {
	extend : 'Ext.ux.desktop.App',
	requires : ['Ext.window.MessageBox',
			'Ext.ux.desktop.ShortcutModel',
			'MyDesktop.Settings'],
	init : function() {
		this.callParent();
		login0(true);
	},
	getModules : function() {
		return menuBtn;
	},

	getDesktopConfig : function() {
		var me = this, ret = me.callParent();
		return Ext.apply(ret, {
			contextMenuItems : [{
				text : '设置背景',
				handler : me.onSettings,
				scope : me
			}],
			shortcuts : Ext.create('Ext.data.Store', {
				model : 'Ext.ux.desktop.ShortcutModel',
				data : moduleConfig
			}),
			wallpaper : './desk/wallpapers/default.png',
			wallpaperStretch : true
		});
	},

	getStartConfig : function() {
		var me = this, ret = me.callParent();
		return Ext.apply(ret, {
			title : '我的系统',
			iconCls : 'user',
			height : 300,
			toolConfig : {
				width : 100,
				items : [{
					text : '设置',
					iconCls : 'settings',
					handler : me.onSettings,
					scope : me
				},{
					text : '导出领域库',
					iconCls : 'top-module-kbmgr',
					handler : me.exportDomainData,
					scope : me
				}, '-', {
					text : '退出',
					iconCls : 'logout',
					handler : me.onLogout,
					scope : me
				}]
			}
		});
	},

	getTaskbarConfig : function() {
		var ret = this.callParent();
		return Ext.apply(ret, {
			quickStart : [{
				name : '分词工具',
				iconCls : 'top-module-matmgr',
				module : 'tools-word'
			}, {
				name : '语义工具',
				module : 'tools-sentence',
				iconCls : 'top-module-segment'
			}],
			trayItems : [currentUsr, currentDm,{
										xtype : 'trayclock',
										flex : 1
									}]
		});
	},

	onLogout : function() {
		Ext.Msg.confirm('退出', '您确定退出系统?', function(res) {
			if (res == 'yes') {
						window.location.href = '../app/g/authmgr/auth!logout.action';
					}
		});
	},
	
	onSettings : function() {
		var dlg = new MyDesktop.Settings({
			desktop : this.desktop
		});
		dlg.show();
	},
	
	exportDomainData : function() {
		var dlg = new MyDesktop.ExportDomainDBTool({
			desktop : this.desktop
		});
		dlg.createWindow().show();
	},
	
	onClose : function() {
		var tempConfig = [{
					id : 'desktopWord_'
				}, {
					id : 'desktopRecommend_'
				}, {
					id : 'exportDomainDBTool_'
				}];
		var desktop = myDesktopApp.getDesktop();
		Ext.each(moduleConfig, function(m, i) {
					var win = desktop.getWindow(m.id);
					if (win) {
						win.close();
					}
				});
		Ext.each(tempConfig, function(m, i) {
					var win = Ext.getCmp(m.id);
					if (win) {
						win.close();
					}
				});
	}
});
