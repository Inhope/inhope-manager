Ext.define("DomainSetting", {
	extend : "Ext.menu.Menu",
	alias : 'widget.dynamicmenu',
	loaded : false,
	loadMsg : '加载中...',
	initComponent : function() {
		var me = this;
		me.callParent(arguments);
		me.store = Ext.getStore(this.store);
		me.on('show', me.onMenuLoad, me);
		listeners = {
			scope : me,
			load : me.onLoad,
			beforeload : me.onBeforeLoad
		};
		me.mon(me.store, listeners);
	},
	onMenuLoad : function() {
		var me = this;
		me.store.load();
	},
	onBeforeLoad : function(store) {
		this.updateMenuItems(false);
	},
	onLoad : function(store, records) {
		this.updateMenuItems(true, records);
	},
	updateMenuItems : function(loadedState, records) {
		var me = this;
		me.removeAll();
		if (loadedState) {
			me.setLoading(false, false);
			Ext.Array.each(records, function(item, index, array) {
				if(item.raw.menu) {
					Ext.Array.each(item.raw.menu.items, function(ite, inde, arra) {
						ite.handler = function(recor) {
							me.menuItem(recor);
						};
					});
				}
				item.raw.handler = function(record) {
					me.menuItem(record);
				};
				me.add(item.raw);
			});
		} else {
			me.setLoading(me.loadMsg, false);
		}
		me.loaded = loadedState;
	},
	menuItem : function(item) {
		//console.info(item);
	}
});