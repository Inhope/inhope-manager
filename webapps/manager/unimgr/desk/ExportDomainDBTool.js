Ext.define('MyDesktop.ExportDomainDBTool', {
	  extend : 'Ext.ux.desktop.Module',

	  id : 'tools-export-domain-db-tool',

	  init : function() {
		  this.launcher = {
			  text : '导出',
			  iconCls : 'top-module-kbmgr'
		  }
	  },

	  createWindow : function() {
		  var win = Ext.getCmp('exportDomainDBTool_');
		  if (!win) {
			  win = new ExportDomainDBWin();
		  }
		  return win;
	  }
  });

Ext.define('ExportDomainDBWin', {
	  extend : 'Ext.Window',
	  title : '领域知识库导出工具',
	  initComponent : function() {
		  var form = Ext.create('Ext.form.Panel', {
			    width : 400,
			    bodyPadding : 10,
			    frame : true,
			    renderTo : Ext.getBody(),
			    items : [{
				      xtype : 'filefield',
				      name : 'file',
				      fieldLabel : '引擎许可证',
				      labelWidth : 50,
				      msgTarget : 'side',
				      allowBlank : false,
				      anchor : '100%',
				      buttonText : '请选择文件...'
			      }],

			    buttons : [{
				      text : '上传',
				      handler : function() {
					      var form = this.up('form').getForm();
					      if (form.isValid()) {
						      form.submit({
							        url : '../app/g/kbmgr/domain-db!export.action',
							        params:{
							        	domain:currentDm.val
							        },
							        waitMsg : '正在上传...',
							        success : function(fp, action) {
								        this.pgTimer = new ProgressTimer({
									          initData : action.result.data,
									          progressText : 'percent',
									          progressId : 'exportDomainDB',
									          boxConfig : {
										          title : '领域库导出'
									          },
									          finish : function(p, response) {
												  if (p.currentCount == -3) {
													  return true;
												  } else if (p.currentCount == -2) {
													  if (this.pgTimer.msgbox)
														  this.pgTimer.msgbox.hide()
													  Ext.Msg.updateProgress(0, '', p.message);
													  if (!this.downloadIFrame) {
														  this.downloadIFrame = Ext.fly(document.body).createChild({
															    tag : 'iframe',
															    style : 'display:none;'
														    })
													  }
													  this.downloadIFrame.dom.src = p.exception
												  } else if (p.currentCount == -1) {
													   Ext.Msg.alert('导入失败', p.message);
												  } else if (p.currentCount == 100) {
													  this.fireEvent('importSuccess', [this.cateId]);
												  }
												  delete this.pgTimer;
											  },
									          scope : this
								          });
								        this.pgTimer.start();
							        }
						        });
					      }
				      }
			      }]
		    });
		  this.items = [form];
		  this.callParent()
	  }
  })

  
function ProgressTimer(cfg) {
	var self = this;
	this.progressId = cfg.progressId;
	this.progress = cfg.progress;
	if (!this.progressId) {
		throw new Error('progressId is required!');
	}
	if (!cfg.initData || cfg.initData.active == null) {
		throw new Error('initData.active is required!');
	}
	this.active = cfg.initData.active;
	var progressText = cfg.progressText == 'percent' ? function(c, t) {
		return (t <= 0 ? 0 : parseInt(c * 100 / t)) + '%';
	} : function(c, t) {
		return c + "/" + t;
	};
	var cb_finish = cfg.finish;
	var cb_inactive = cfg.inactive;
	var cb_error = cfg.error;
	var cb_poll = cfg.poll;
	var cb_scope = cfg.scope || this;
	var ok = this.ok = cfg.ok;

	cfg.boxConfig = cfg.boxConfig || {};
	this.updateButtonVisibility = function(progress) {
		if (!this.msgbox)
			return
		// this.msgbox.getDialog().getFooterToolbar().hide()
		Ext.each(this.msgbox.msgButtons, function(btn) {
					if (progress && btn.text != '取消' && progress.finished)
						btn.enable();
					else if (progress && btn.text == '取消' && !progress.finished
							&& progress.cancelable)
						btn.enable();
					else
						btn.disable();
				});
	};
	this.createMsgbox = function(config) {
		config = Ext.apply(config || {}, cfg.boxConfig);
		var m = Ext.Msg.show(Ext.apply({
			title : '导入',
			progress : typeof(self.progress) == 'undefined'
					? true
					: self.progress,
			msg : '同步中...',
			buttons : Ext.MessageBox.OKCANCEL,
			buttonText : {
				ok : '确定',
				cancel : '取消'
			},
			width : 300,
			animEl : 'dialog',
			fn : function(btn) {
				if (btn == 'cancel') {
					Ext.Ajax.request({
						url : '../app/g/progress-timer!cancelProgress.action?progressId='
								+ self.progressId,
						success : function() {
						},
						failure : function() {
						}
					});
					//					m.show();
				};
				if(btn == 'ok') {//modify by baoy since 2015-03-12
					if(ok != undefined && ok != '' && ok != null) {
						window.location.href = ok;
					}
				}
			}
		}, config));
		if (!window.__before_show_bind) {
			window.__before_show_bind = true;
			m.on('beforeshow', function() {
						Ext.each(m.buttons, function(btn) {
									btn.enable();
								});
					});
		}
		return m;
	};
	this.msgbox = this.createMsgbox();
	this.updateButtonVisibility();
	var _requestImportProgress = function() {
		Ext.Ajax.request({
			url : '../app/g/progress-timer!getProgress.action?progressId='
					+ self.progressId,
			success : function(resp) {
				if (!this.msgbox)
					return;
				var progress = Ext.JSON.decode(resp.responseText);
				var value = progress.totalCount
						? (progress.currentCount / progress.totalCount)
						: 0;
				var text = progressText(progress.currentCount,
						progress.totalCount);
				this.msgbox.updateProgress(value, text, progress.message);
				if (progress.finished) {
					if (progress.data) {
						this.createMsgbox({
									width : 600
								});
						this.msgbox.updateProgress(1, progressText(
										progress.totalCount,
										progress.totalCount), progress.message);
//						addMsgBoxDetail(progress.data);
					} else {
						this.msgbox.updateProgress(1, progressText(
										progress.totalCount,
										progress.totalCount), progress.message);
					}
					clearTimeout(this.importTimer);
					if (typeof cb_finish == 'function') {
						cb_finish.apply(cb_scope, [progress, resp]);
					}
					this.updateButtonVisibility(progress);
				} else {
					if (typeof cb_poll == 'function')
						cb_poll.apply(cb_scope, [progress, resp]);
					this.beginSchedule();
					this.updateButtonVisibility(progress);
				}
			},
			failure : function(resp) {
				if (!this.msgbox)
					return;
				if (typeof cb_error == 'function')
					cb_error.call(cb_scope, resp);
				else
					clearTimeout(this.importTimer);
				this.updateButtonVisibility(progress);
			},
			scope : self
		});
	};
	this.pause = function() {
		if (this.importTimer)
			clearTimeout(this.importTimer);
		if (this.msgbox) {
			this.msgbox.hide();
			delete this.msgbox;
		}
	};
	this.restart = function(delay) {
		this.msgbox = this.createMsgbox();
		if (delay)
			_requestImportProgress.defer(delay, this);
		else
			this.beginSchedule();
	};
	this.beginSchedule = function() {
		if (this.importTimer)
			clearTimeout(this.importTimer);
		this.importTimer = window.setTimeout(function() {
					_requestImportProgress();
				}, 1000);
	};

	this.start = function() {
		if (!this.active)
			if (typeof cb_inactive == 'function')
				cb_inactive.call(cb_scope);
			else
				Ext.Msg.alert('提示', '当前有其他互斥操作正在进行，请稍后再试！');
		else {
			this.msgbox.updateProgress(0, progressText(0, 0), '');
			this.beginSchedule();
		}
		return this.active;
	};
}

ProgressTimer.createTimer = function(config, cb) {
	if (!(config instanceof Array)) {
		config = [config];
	}
	function poll() {
		if (!config.length)
			return
		var c = config.shift();
		if (c.initData) {
			if (cb) {
				var tm = new ProgressTimer(c);
				if (typeof cb == 'function')
					cb(tm);
				else
					tm.start();
			}
			return
		}
		Ext.Ajax.request({
					url : '../app/g/progress-timer!getProgress.action?progressId='
							+ c.progressId,
					success : function(resp) {
						var progress = Ext.JSON.decode(resp.responseText);
						if (!progress)
							return;
						if (!progress.finished && cb) {
							c.initData = progress;
							var tm = new ProgressTimer(c);
							if (typeof cb == 'function') {
								cb(tm);
							} else {
								tm.start();
							}
						} else
							poll();
					}
				});
	}
	poll();
};