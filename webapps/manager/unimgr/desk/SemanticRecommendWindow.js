SemanticRecommendWindow = function(cfg) {

	var me = this;
	
	this.recForm = new Ext.FormPanel({
		split : true,
		region : 'south',
		defaults : {
			border : false,
			msgTarget : 'side',
			labelAlign : 'right',
			labelSeparator : " ： ",
			labelWidth : 80
		},
		frame : false,
		border : false,
		bodyStyle : 'padding : 2px;',
		fit : true,
		items : [{
			anchor : '100%',
			emptyText : '支持回车Enter',
			xtype : 'textfield',
			fieldLabel : '用户问题',
			allowBlank : false,
			name : 'userQuestion',
			enableKeyEvents : true,
			listeners : {
				specialkey : function(field, e) {
					if (e.getKey() == e.ENTER) {
						me.doSubmit();
					}
				}
			}
		}]
	});
	
	this.answer = new Ext.form.field.TextArea({
		split : true,
		region : 'west',
		title : '定位抽象语义',
		width : 355,
		xtype : 'textarea',
		readOnly : true,
		hideLabel : true
	})
	
	this.answer2 = new Ext.form.field.TextArea({
		region : 'center',
		title : '定位抽象语义规则',
		xtype : 'textarea',
		readOnly : true,
		hideLabel : true
	})
	
	this.recPanel = new Ext.Panel({
				layout : 'border',
				border : false,
				items : [this.recForm
						, this.answer, this.answer2],
				buttons : [{
							id : 'recommendQuestion',
							text : '提交',
							handler : function(button, b) {
								button.disable();
								me.answer.setValue('');
								me.answer2.setValue('');
								me.doSubmit();
								button.enable();
							}
						}]
			});

	var _cfg = {
		id : 'desktopRecommend_',
		iconCls : 'top-module-segment',
		width : 700,
		height : 400,
		layout : 'fit',
		border : false,
		plain : true,
		shim : true,
		closable : true,
		closeAction : 'hide',
		collapsible : true,
		resizable : false,
		draggable : true,
		animCollapse : true,
		constrainHeader : true,
		title : '推荐工具',
		layout : 'fit',
		items : this.recPanel

	}

	SemanticRecommendWindow.superclass.constructor.call(this, Ext.applyIf(_cfg || {}, cfg));

}
Ext.extend(SemanticRecommendWindow, Ext.Window, {
	doSubmit : function() {
		var me = this;
		var form = me.recForm.getForm();
		if (form.isValid()) {
			form.submit({
					url : '../app/g/kbmgr/wordclass!recommendQuestion.action',
					params : {
						domain : currentDm.val
					},
					method : 'POST',
					success : function(form, action) {
						var data = Ext.decode(action.response.responseText);
						if (data.success) {
							var formpanel = me.recPanel;
							me.answer.setValue(data.data.first);
							me.answer2.setValue(data.data.second);
						}
					},
					failure : function(form, action) {
						Ext.Msg.alert("错误", "推荐失败!");
					}
				});
		}
	}
});
