Ext.define('MyDesktop.SentenceTool', {
    extend: 'Ext.ux.desktop.Module',

    id:'tools-sentence',

    init : function(){
        this.launcher = {
            text: '语义工具',
            iconCls:'top-module-segment'
        }
    },

    createWindow : function(){
    	var desktop = this.app.getDesktop();
    	var win = Ext.getCmp('desktopRecommend_');
        if(!win) {
        	win = new SemanticRecommendWindow();
        }
        win.answer.setValue('');
        win.answer2.setValue('');
        return win;
    }
});
