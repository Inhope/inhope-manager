WordSegmentWindow = function(cfg) {

	var me = this;
	this.textArea0 = new Ext.form.TextArea({
			columnWidth : .35,
			itemId : 'inputContent1',
			emptyText : '请输入句子'
		});
	this.textArea1 = new Ext.form.TextArea({
		columnWidth : .5,
		style : "margin-left:5px;",
		itemId : 'outputContent1',
		emptyText : '显示查询分词结果',
		readOnly : true
	});
	
	this.textArea2 = new Ext.form.TextField({
		columnWidth : .35,
		itemId : 'inputContent2',
		emptyText : '请输入词语'
	});
	
	this.textArea3 = new Ext.form.TextField({
		columnWidth : .35,
		style : "margin-left:5px;",
		itemId : 'outputContent2',
		emptyText : '显示查询结果',
		readOnly : true
	});
	
	this.textArea4 = new Ext.form.TextField({
		columnWidth : .35,
		itemId : 'inputContent3',
		emptyText : '请输入词语'
	});
	
	this.textArea5 = new Ext.form.TextField({
		columnWidth : .35,
		style : "margin-left:5px;",
		itemId : 'outputContent3',
		emptyText : '显示查询结果',
		readOnly : true
	});
	
	this.textFile6 = new Ext.form.field.File({
		columnWidth : .85,
		allowBlank : false,
		editable : false,
		emptyText : '选择文件',
		name : 'file',
		regex : /^.*?\.(xls|xlsx)$/,
		regexText : '只能上传.xls,xlsx文件',
		buttonText : '选择文件',
		buttonConfig : {
			iconCls : 'icon-search'
		}
	});
	
	var toolFormPanel = this.toolFormPanel = new Ext.FormPanel({
				frame : false,
				border : false,
				bodyStyle : 'padding : 3px 10px; background-color:' + sys_bgcolor,
				layout : 'form',
				autoHeight : true,
				items : [{
							layout : 'column',
							border : 0,
							frame : true,
							bodyStyle : 'padding:5px 0 5px 5px',
							items : [this.textArea0, this.textArea1, {
										columnWidth : .15,
										style : "margin-left:5px;",
										text : '分词',
										height : 62,
										xtype : 'button',
										handler : function() {
											me.searchSegment();
										}
									}]
						}, {
							layout : 'column',
							border : 0,
							frame : true,
							bodyStyle : 'padding:5px 0 5px 5px',
							items : [this.textArea2, this.textArea3, {
										columnWidth : .2,
										style : "margin-left:5px;",
										text : '分词引擎查询',
										xtype : 'button',
										iconCls : 'icon-search',
										handler : function() {
											me.searchSegWord();
										}
									}, {
//										columnWidth : .1,
//										itemId : 'deleteWord',
//										style : "margin-left:5px;",
//										text : '删除',
//										xtype : 'button',
//										hidden : false,
//										disabled : true,
//										iconCls : 'icon-delete',
//										handler : function() {
//											me.removeSegWord();
//										}
//									}, {
										columnWidth : .1,
										itemId : 'addWord',
										style : "margin-left:5px;",
										text : '新增',
										xtype : 'button',
										hidden : true,
										iconCls : 'icon-add',
										handler : function() {
											me.addSegWord();
										}
									}]
						}, {
							layout : 'column',
							border : 0,
							frame : true,
							bodyStyle : 'padding:5px 0 5px 5px',
							items : [this.textArea4 , this.textArea5, {
										columnWidth : .15,
										style : "margin-left:5px;",
										text : '互信息',
										xtype : 'button',
										iconCls : 'icon-search',
										handler : function() {
											me.searchVector();
										}
									}, {
										columnWidth : .15,
										style : "margin-left:5px;",
										text : '自定义',
										xtype : 'button',
										iconCls : 'icon-defined',
										handler : function() {
											me.defined();
										}
									}]
						}, {
							layout : 'column',
							border : 0,
							frame : true,
							bodyStyle : 'padding:5px 0 5px 5px',
							items : [this.textFile6, {
										columnWidth : .15,
										style : "margin-left:5px;",
										text : '开始分词',
										xtype : 'button',
										iconCls : 'icon-excel',
										handler : function() {
											me.submitTaskNode();
										}
									}]
						}]
			});

	var _cfg = {
		id : 'desktopWord_',
		width : 600,
		title : '词类工具',
		iconCls : 'top-module-matmgr',
		closable : true,
		closeAction : 'hide',
		resizable : false,
		draggable : true,
		animCollapse : true,
		constrainHeader : true,
		layout : 'fit',
		items : toolFormPanel
	}

	WordSegmentWindow.superclass.constructor.call(this, Ext.applyIf(_cfg || {}, cfg));

}
Ext.extend(WordSegmentWindow, Ext.Window, {
			searchSegment : function() {
				var me = this;
				var content = me.textArea0.getValue();
				if (content.trim()) {
					Ext.Ajax.request({
								url : '../app/g/kbmgr/wordclass!searchSegment.action',
								params : {
									content : content.trim(),
									domain : currentDm.val
								},
								success : function(form, action) {
									var obj = Ext.decode(form.responseText);
									me.textArea1.setValue(obj.message);
								},
								failure : function(response) {
									Ext.Msg.alert("错误", response.responseText);
								}
							});
				} else {
					Ext.Msg.alert('提示', '输入内容不能为空');
				}
			},
			searchSegWord : function() {
				var me = this;
				var word = me.textArea2.getValue();
				if (word) {
					Ext.Ajax.request({
								url : '../app/g/kbmgr/wordclass!searchSegWord.action',
								params : {
									word : word.trim(),
									domain : currentDm.val
								},
								success : function(response) {
									var obj = Ext.util.JSON.decode(response.responseText);
									if (obj) {
										me.textArea3.setValue(obj);
//										me.toolFormPanel.query('#deleteWord')[0].setDisabled(false);
//										me.toolFormPanel.query('#deleteWord')[0].setVisible(true);
//										me.toolFormPanel.query('#addWord')[0].setVisible(false);
									} else {
										me.textArea3.setValue('未找到结果');
//										me.toolFormPanel.query('#deleteWord')[0].setDisabled(true);
//										me.toolFormPanel.query('#deleteWord')[0].setVisible(false);
//										me.toolFormPanel.query('#addWord')[0].setVisible(true);
									}
								},
								failure : function(response) {
									Ext.Msg.alert("错误", response.responseText);
								}
							});
				} else {
					Ext.Msg.alert('提示', '输入内容不能为空');
				}
			},
			searchVector : function() {
				var me = this;
				var word = me.textArea4.getValue();
				if (word) {
					Ext.Ajax.request({
								url : '../app/g/kbmgr/wordclass!searchVector.action',
								params : {
									word : word.trim(),
									domain : currentDm.val
								},
								success : function(form, action) {
									var obj = Ext.util.JSON.decode(form.responseText);
									if (obj.message) {
										me.textArea5.setValue(obj.message);
									}
								},
								failure : function(response) {
									Ext.Msg.alert("错误", response.responseText);
								}
							});
				} else {
					Ext.Msg.alert('提示', '查询内容不能为空');
				}
			},
			defined : function() {
				var me = this;
				Ext.Ajax.request({
							url : '../app/g/kbmgr/wordclass!defined.action',
							params : {
								domain : currentDm.val
							},
							success : function(form, action) {
								var obj = Ext.util.JSON.decode(form.responseText);
								if (obj.message) {
									me.textArea5.setValue(obj.message);
								}
							},
							failure : function(response) {
								Ext.Msg.alert("错误", response.responseText);
							}
						});
			},
			submitTaskNode : function() {
				var self = this;
				var formpanel = self.toolFormPanel;
				var form = formpanel.getForm();
				if (form.isValid()) {
					form.submit({
						url : '../app/g/kbmgr/wordclass!batchSegment.action',
						method : 'POST',
						params : {
							domain : currentDm.val
						},
						success : function(fp, action) {
					    		this.pgTimer = new ProgressTimer({
						          initData : action.result.data,
						          progressText : 'percent',
						          progressId : 'exportBatchSegment',
						          boxConfig : {
						          	msg : '分词中...',
							          title : '分词'
						          },
						          finish : function(p, response) {
									if (!this.downloadIFrame) {
										this.downloadIFrame = self.getEl().createChild({
											tag : 'iframe',
											style : 'display:none;'
										});
									}
									this.downloadIFrame.dom.src = 'wordclass!exportSegmentResult.action?&_t' + new Date().getTime();
						          },
						          scope : this
					          });
					        this.pgTimer.start();
				        },
						failure : function(form, action) {
							var data = Ext.JSON.decode(action.response.responseText);
							Ext.MessageBox.alert(data.message);
						}
					});
				}
			}
		});
