Ext.define('MyDesktop.WordTool', {
			extend : 'Ext.ux.desktop.Module',

			id : 'tools-word',

			init : function() {
				this.launcher = {
					text : '分词工具',
					iconCls : 'top-module-matmgr'
				}
			},

			createWindow : function() {
				var desktop = this.app.getDesktop();
				var win = Ext.getCmp('desktopWord_');
				if (!win) {
					win = new WordSegmentWindow();
				}
				return win;
			}
});
