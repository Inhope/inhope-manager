function getxhr() {
	var _xhr = false;
	try {
		_xhr = new ActiveXObject("Msxml2.XMLHTTP");
	} catch (e) {
		try {
			_xhr = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (e2) {
			_xhr = false;
		}
	}
	if (!_xhr && window.XMLHttpRequest)
		_xhr = new XMLHttpRequest();
	return _xhr;
	};
function http_get(url) {
	var xhr = getxhr();
	xhr.open('GET', url, false);
	xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
	xhr.send();
	return xhr.responseText;
};

var __error_alert_1 = function(conn, response, options) {
	if (response.status == 403) {
		if (options.failure)
			delete options.failure
		var ec = response.getResponseHeader('error_code');
		if (ec == '1') {// logged in but target resource restricted
			Ext.Msg.alert('错误', '请求的资源未授权给当前登陆用户,请联系管理员');
		} else {
			var redirect_url = response.getResponseHeader('redirect_url');
			if (redirect_url){
				Ext.Msg.alert('错误', '您还没有登陆，或者登陆已经超时。正在为您重定向到登陆页面...');
				location = redirect_url
			}else{
				Ext.Msg.alert('错误', '您还没有登陆，或者登陆已经超时。请重新登陆！');
			}
		}
	} else if (options && !options.failure)
		Ext.Msg.alert('错误', '服务器发生错误:' + (response.responseText ? response.responseText : '<br>'));
};


Ext.Ajax.on('requestexception', __error_alert_1);