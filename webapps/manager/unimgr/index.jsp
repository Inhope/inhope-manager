<%@page import="com.incesoft.xmas.auth.support.SSOLoginUser"%>
<%@page import="com.incesoft.xmas.auth.support.AuthContext"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="com.eastrobot.commonsapi.ibotcluster.DoInClusterCallback"%>
<%@page import="com.incesoft.xmas.commons.Simp2TranUtils"%>
<%@page import="java.util.Date"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="com.incesoft.xmas.commons.DateUtils"%>
<%@page import="com.incesoft.xmas.authmgr.web.AuthTool"%>
<%@page import="com.incesoft.xmas.authmgr.web.AuthAction"%>
<%@page
	import="com.eastrobot.commonsapi.ibotcluster.ClusterConfigServiceHelper"%>
<%@page import="com.incesoft.xmas.commons.LicenceHolder"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
if (ClusterConfigServiceHelper.isClusterEnabled() && ClusterConfigServiceHelper.getAppCFG() == null){
	response.sendRedirect("../clustermgr/");
	return;	
}
%>
<jsp:directive.page
	import="org.springframework.web.context.support.WebApplicationContextUtils" />
<jsp:directive.page
	import="com.incesoft.xmas.commons.PropertyPlaceholderRecorder" />
<jsp:directive.page
	import="org.springframework.beans.factory.config.PropertyPlaceholderConfigurer" />
<%@taglib prefix="res" uri="/WEB-INF/tld/import-res.tld"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="usr" uri="/WEB-INF/tld/login-user.tld"%>
<%@taglib prefix="mdl" uri="/WEB-INF/tld/mdl-ft.tld"%>
<%
	ApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(application);
	PropertyPlaceholderRecorder placeHolder = (PropertyPlaceholderRecorder) ctx.getBean(PropertyPlaceholderRecorder.class);
	pageContext.setAttribute("projectName", placeHolder.getProperty("projectName"));
	pageContext.setAttribute("systemName", placeHolder.getProperty("systemName"));
	LicenceHolder licenceholder = ctx.getBean(LicenceHolder.class);
	if (licenceholder != null && licenceholder.getLicence() != null) {
		if (licenceholder.getLicence().getExpiredTime() > 0){
	long dayLeft = (licenceholder.getLicence().getExpiredTime() - System.currentTimeMillis())/24/3600/1000;
	String expiredTime = DateUtils.dateToString(new Date(licenceholder.getLicence().getExpiredTime()), "yyyy-MM-dd");
	request.setAttribute("licenceExpiredTime",expiredTime);
	String[] supportedPlatforms=licenceholder.getLicence().getSupportedPlatforms();
	if(supportedPlatforms!=null && supportedPlatforms.length>0){
	   String platform="";
	   for(String platForms : supportedPlatforms){
		   platform+=platForms+",";
	   }
	   request.setAttribute("licenceSupportedPlatforms",platform.substring(0, platform.length()-1));
	}
	int maxDocCount=licenceholder.getLicence().getMaxDocCount();
	request.setAttribute("licenceMaxDocCount",maxDocCount);
	if (dayLeft > 0 && dayLeft < 30)
		request.setAttribute("licenceExpireInXDay","[引擎许可证将在"+dayLeft+"天后过期]");
	else if (dayLeft <= 0)
		request.setAttribute("licenceExpireInXDay","[引擎许可证已经过期]");
		}
		request.setAttribute("rsmgr_disable", !licenceholder.getLicence().isSpeechEnabled());
	}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><c:if test="${not empty projectName}">${projectName } - </c:if>小i智能机器人统一管理平台</title>
<base target="_self" />
<link rel="stylesheet" type="text/css" href="custom.css" />
<link rel="shortcut icon" type="image/x-icon" href="../favicon.ico" />
<%@include file="/commons/extjsvable.jsp"%>

<link rel="stylesheet" type="text/css" href="<c:url value='/ext/vtab/ext-vtab.css'/>" />
<script type="text/javascript"
	src="<c:url value='/ext/vtab/ext-vtab.js'/>"></script>
<usr:checkPerm jsExportVar="_check_perm_result"
	permissionNames="auth,om,mkt,kb,bot,rs,mat,acs" />
<script>
	var rsmgr_disable = eval('${rsmgr_disable}');
</script>
<mdl:dump name="uni" jsExportVar="uni_mdl_ft" />
<script type="text/javascript">var initModule = '<%=request.getParameter("initModule")!=null ?request.getParameter("initModule"):""%>';
var is_sso_user = <%=AuthContext.getContext().getLoginUser() instanceof SSOLoginUser%>
</script>
<script type="text/javascript" src="script/top.layout.js"></script>
<%
String head_bg = "head_bg";
String head_left_bg = "head_left_bg";
String head_right_bg = "head_left_bg";
String head_logo = "head_logo";
String vtab_css = "ext-vtab";
%>
<% if (LabsUtil.getExtTheme() != null) { 
	head_bg += "_" + LabsUtil.getExtTheme();
	head_left_bg += "_" + LabsUtil.getExtTheme();
	head_right_bg += "_" + LabsUtil.getExtTheme();
	head_logo += "_" + LabsUtil.getExtTheme();
	vtab_css += "-" + LabsUtil.getExtTheme();
}
pageContext.setAttribute("head_bg", head_bg);
pageContext.setAttribute("head_left_bg", head_left_bg);
pageContext.setAttribute("head_right_bg", head_right_bg);
pageContext.setAttribute("vtab_css", vtab_css);
%>
<link rel="stylesheet" type="text/css" href='<c:url value="/ext/vtab/${vtab_css}.css"/>'/>
<style>
#header {
	background: url("images/${head_bg}.jpg") repeat-x scroll 0 0 #1E4176;
	border: 0 none;
	padding: 0 3px;
	margin: 0;
}

#header .api-title {
	background: url(images/${head_left_bg}.jpg) no-repeat left top;
	color: white;
	height: 31px;
	/*margin: 0 0 0 5px;*/
	padding: 6px 0 0 100px;
}

a.blue_link {
	font-size: 12px;
	text-decoration: none;
	color: #0559c2;
	font-family: 宋体;
	font-weight: normal
}

a.blue_link:hover,a.blue_link:active {
	font-size: 12px;
	text-decoration: underline;
}
.subfloat a,.subfloat span{
	float:right;
	margin-right:5px;
}
</style>
</head>
<%
pageContext.setAttribute("utfVersion", Simp2TranUtils.UI_SIMP2TRAN_ENABLED);
if (Simp2TranUtils.UI_SIMP2TRAN_ENABLED) {
	head_logo += "_utf";
}
pageContext.setAttribute("head_logo", head_logo);
%>
<body>
	<div id="header">
		<div class="api-title" style="position: relative">
			<div
				style="background: url('images/${head_logo}.jpg') no-repeat; position: absolute; left: 0; top: 0; width: 135px; height: 31px;"></div>
			<div
				style="color: #fff; font-family: '宋体'; font-size: 16px; font-weight: bold; margin: 2px 0 0 35px;"><%=((PropertyPlaceholderRecorder) WebApplicationContextUtils.getWebApplicationContext(application).getBean(PropertyPlaceholderRecorder.class)).getProperty("systemName")%>
				<span
					style="color: #fff0ad; font-family: '宋体'; font-size: 12px; font-weight: normal; margin: 0 0 0 0;"><%=((PropertyPlaceholderRecorder) WebApplicationContextUtils.getWebApplicationContext(application).getBean(PropertyPlaceholderRecorder.class)).getProperty("projectName")%></span>
			</div>
			<div class="hoverShowInfo"
				style="background: url(images/head_right_bg.jpg) no-repeat right bottom;position: absolute;right:0;top:0;width: 400px; height: 36px;">
				&nbsp;
			</div>
			<div id="rt_info_div" style="position: absolute; right: 20px; top: 9px;width:100%" class="subfloat">
				<!-- logout -->
				<span> <%
	if (ClusterConfigServiceHelper.isClusterEnabled() && AuthTool.getAppSessionAttribute(request, ClusterConfigServiceHelper.getAppURI(request), true).get("cluster") != null){
%> <a target="_blank"
					style="color: #cde2ff; font-family: '宋体'; font-size: 12px;"
					href="<c:url value="/clustermgr/login.jsp"/>">登陆应用平台</a> <%
	}
%> <a style="color: #cde2ff; font-family: '宋体'; font-size: 12px;"
					href="../authmgr/auth!logout.action">注销</a> </span>
				<span style="color: #fff0ad; font-family: '宋体'; font-size: 14px;"><usr:name />
				</span>
				
				<span onmouseout="hiddenTip();" onmouseover="showTip();"
					style="color: #fff000; font-family: '宋体';cursor:help;font-size: 12px;font-weight: bold;">许可证详情
				</span> <span
					style="color: #fff000; font-family: '宋体'; font-size: 12px;font-weight: bold;">${licenceExpireInXDay}
				</span> <a href="javascript:toggleNotificationDetail()">
					<div id="notification_detail"
						style="background-color:#FFFFA0;font-weight:bold;border:1px solid grey;position:absolute;top:20px;color:red;display:none;z-index:100000;overflow:auto;height:200px;width:400px"></div>
					<Marquee id="notification_compact" scrollamount="2"
						style="position:relative;top:5px;border-bottom:1px dotted #fff0ad;color: red; font-family: '宋体'; font-size: 12px;width:400px;height: 15px;line-height: 15px;display:none"></Marquee>
				</a> 
			</div>
		</div>
	</div>
	<div id="divLicenceInfoTip"
		style="display:none;pointer-events: none;position:relative;padding:8px 10px 5px 10px;height:20px;z-index:1000;float:right;margin-right:150px;filter:alpha(opacity=80);opacity:0.8;background-color:#fff0cb;border:1px solid #ffbe41;color:#222;font-weight:bold;">
		许可证有效期:${licenceExpiredTime}|已开通渠道:${licenceSupportedPlatforms}|允许知识量:${licenceMaxDocCount}|本体领域类型:金融
	</div>
	<script>
		if (window.uni_mdl_ft && uni_mdl_ft['uni.hideHeader'])
			document.getElementById('header').style.display = 'none';
		Ext.Ajax.request({
			url : 'sys-notification!list.action',
			success : function(response) {
				var result = Ext.util.JSON.decode(response.responseText);
				if (result.data && result.data.length) {
					window.__sysnotificationids = []
					var compact = ''
					var detail = ''
					Ext.each(result.data, function(n, i, total) {
						if (!n.type)
							window.__sysnotificationids.push(n.id)
						compact += n.content + '[' + n.createTime + ']' + ' | ';
						detail += n.content + '[' + n.createTime + ']' + '<br>';
					})
					if (compact) {
						compact = compact.replace(/\|\s*$/, '')
						Ext.fly('notification_compact').show()
						Ext.fly('notification_compact').dom.innerHTML = (compact)
						Ext.fly('notification_detail').dom.innerHTML = (detail)
					}
				}
			}
		});
		function toggleNotificationDetail() {
			if (window.__sysnotificationids && window.__sysnotificationids.length) {
				Ext.Ajax.request({
					url : 'sys-notification!updateFlag.action',
					params : {
						flag : 1,
						id : window.__sysnotificationids.join(',')
					},
					success : function(response) {
					}
				});
				window.__sysnotificationids = []
			}
			Ext.fly('notification_detail').toggle()
		}

		function showTip() {
			document.getElementById('divLicenceInfoTip').style.display = 'block';
		}
		function hiddenTip() {
			document.getElementById('divLicenceInfoTip').style.display = 'none';
		}

		Ext.Ajax.request({
			url : ('../progress-timer!getProgress.action') + '?progressId=autoImportKnowDataStatus',
			success : function(resp) {
				var objRes=Ext.util.JSON.decode(resp.responseText);
				if (objRes && !objRes.finished) {
					if (!this.msgbox){
						var timer = new ProgressTimer({
							initData : {active:true},
							progressId : 'autoImportKnowDataStatus',
							boxConfig : {
								title : '正在初始化领域语义库...'
							}
						});
				      timer.start();
					}
				}
			},
			failure : function(resp) {
				if (!this.msgbox)
					return;
				if (typeof cb_error == 'function')
					cb_error.call(cb_scope, resp);
				else
					clearTimeout(this.importTimer);
				this.updateButtonVisibility(progress);
			},
			scope : self
		});
		var orientationObjectId = '<%=URLEncoder.encode(request.getParameter("objectId")!=null ?request.getParameter("objectId"):"")%>';
	    var orientationValueId = '<%=URLEncoder.encode(request.getParameter("valueId")!=null ?request.getParameter("valueId"):"")%>';
	    var navToKB = function(){};
	</script>
</body>
</html>
