<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.incesoft.xmas.authmgr.web.AuthAction"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><c:if test="${not empty projectName}">${projectName } - </c:if>小i领域语义库统一管理平台</title>
<base target="_self" />
<link rel="stylesheet" type="text/css" href="custom.css" />
<link rel="shortcut icon" type="image/x-icon" href="../favicon.ico" />
<script>
	var isCaptchaEnabled = "<%=AuthAction.isCaptchaEnabled()%>";
</script>

<script type="text/javascript" src="<c:url value="/unimgr/desk/js/ext-all.js"/>"></script>
<script type="text/javascript" src="<c:url value="/unimgr/desk/js/ext-lang-zh_CN.js"/>"></script>
<script type="text/javascript" src='<c:url value="/commons/scripts/sha1.js"/>'></script>
<link rel="stylesheet" type="text/css" href="<c:url value="/unimgr/desk/resources/ext-theme-classic/ext-theme-classic-all.css"/>" />
<link rel="stylesheet" type="text/css" href="<c:url value="/unimgr/desk/css/desktop.css"/>" />
<script type="text/javascript" src="<c:url value="/unimgr/desk/js/Module.js"/>"></script>
<script type="text/javascript" src="<c:url value="/unimgr/desk/timeout.js"/>"></script>
<script type="text/javascript" src="<c:url value="/unimgr/desk/DomainSetting.js"/>"></script>
<script type="text/javascript" src="<c:url value="/unimgr/desk/SemanticRecommendWindow.js"/>"></script>
<script type="text/javascript" src="<c:url value="/unimgr/desk/WordSegmentWindow.js"/>"></script>
<script type="text/javascript" src="<c:url value="/unimgr/desk/WordTool.js"/>"></script>
<script type="text/javascript" src="<c:url value="/unimgr/desk/SentenceTool.js"/>"></script>
<script type="text/javascript" src="<c:url value="/unimgr/desk/ExportDomainDBTool.js"/>"></script>
<script type="text/javascript">
Ext.Loader.setPath({
	'Ext.ux.desktop' : 'desk/js',
	MyDesktop : 'desk'
});

Ext.require('MyDesktop.App');

var myDesktopApp;
Ext.onReady(function() {
	myDesktopApp = new MyDesktop.App();
});
</script>
<style>
/*	.ux-desktop-shortcut-text {text-shadow:2px 2px 3px #000;} */
.nodeRowClass .x-grid-cell{
  height: 40px !important;
  line-height: 20px;
}
</style>
</head>
<body>


</body>
</html>
