var moduleConfig = isVersionStandard() ? [{
			id : 'kbmgr',
			authName : 'kb',
			title : '知识管理'
		}, {
			id : 'matmgr',
			authName : 'mat',
			title : '素材管理'
		}, {
			id : 'ommgr',
			authName : 'om',
			title : '报表管理'
		}, {
			id : 'botmgr',
			authName : 'bot',
			title : '渠道管理'
		}, {
			id : 'authmgr',
			// authName : 'auth',
			title : '系统管理'
		}] : [{
			id : 'kbmgr',
			authName : 'kb',
			title : '知识管理'
		}, {
			id : 'mktmgr',
			authName : 'mkt',
			title : '服务管理'
		}, {
			id : 'botmgr',
			authName : 'bot',
			title : '渠道管理'
		}, {
			id : 'matmgr',
			authName : 'mat',
			title : '素材管理'
		}, {
			id : 'rsmgr',
			authName : 'rs',
			title : '语音管理'
		}, {
			id : 'ommgr',
			authName : 'om',
			title : '运维管理'
		}, {
			id : 'authmgr',
			authName : is_sso_user?'auth':'',
			title : '系统管理'
		}]
// if (isVersionStandard())
// moduleConfig.splice(1, 1);

getTopWindow().modules = moduleConfig;
if (window.rsmgr_disable) {
	Ext.each(moduleConfig, function(m, i) {
				if (m.id == 'rsmgr') {
					moduleConfig.splice(i, 1)
					return false;
				}
			})
}
Ext.onReady(function() {
	var menuTab = new Ext.ux.TabPanel({
				tabPosition : 'left',
				autoScroll : true,
				// deferredRender:false,
				activeTab : 0,
				enableTabScroll : true,
				border : false,
				region : 'center'
			});
	getTopWindow().vtab = menuTab;
	var r = moduleConfig
	for (var i = r.length - 1; i >= 0; i--) {
		if (r[i].authName && window._check_perm_result) {
			if (_check_perm_result.indexOf(r[i].authName) == -1) {
				r.splice(i, 1);
			}
		}
	}
	var viewport = new Ext.Viewport({
				layout : 'border',
				items : [{
							cls : 'docs-header',
							height : 36,
							region : 'north',
							xtype : 'box',
							el : 'header',
							border : false,
							margins : '0 0 5 0'
						}, menuTab]
			});
	var initModuleIndex = 0;
	Ext.each(moduleConfig, function(m, i) {
		if (window.initModule  && (window.initModule.split('.')[0]+'mgr') == m.id)
			initModuleIndex = i
			
		var p = new Ext.Panel({
			layout : 'fit',
			title : m.title,
			iconCls : 'top-module-' + m.id,
			border : false,
			items : [{
				html : "<iframe id='"
						+ m.id
						+ "' src='"
						+ (m.url ? m.url : '../' + m.id + '/')
						+ "' height='100%' width='100%' frameborder='no' border='0' marginwidth='0' marginheight='0' scrolling='auto' allowtransparency='yes'></iframe>"
			}]
		});
		menuTab.add(p)
	})
	menuTab.setActiveTab(initModuleIndex)
	// extra tab
	Ext.Ajax.request({
		url : '../kbmgr/property!get.action',
		params : {
			key : 'top.layout.extra_tab_config'
		},
		success : function(response) {
			var cfgstr = response && response.responseText
					? response.responseText
					: '';
			// 报表,http://www.baidu.com;
			cfgstr = cfgstr.trim()
			if (cfgstr) {
				var cfgs = cfgstr.split(';');
				var extracfg = [];
				Ext.each(cfgs, function(c, i) {
							var params = c.split(',');
							if (params && params.length)
								extracfg.push({
											id : Ext.id(),
											title : params[0],
											url : params[1]
										});
						})
				Ext.each(extracfg, function(m, i) {
					var p = new Ext.Panel({
						layout : 'fit',
						title : m.title,
						iconCls : 'top-module-x',
						border : false,
						items : [{
							html : "<iframe id='"
									+ m.id
									+ "' src='"
									+ (m.url ? m.url : '../' + m.id + '/')
									+ "' height='100%' width='100%' frameborder='no' border='0' marginwidth='0' marginheight='0' scrolling='auto' allowtransparency='yes'></iframe>"
						}]
					});
					menuTab.add(p)
				})
			}
		},
		failure : function(response) {
		}
	});
	Ext.get('header').on('click', function() {
				viewport.focus();
			});
});
